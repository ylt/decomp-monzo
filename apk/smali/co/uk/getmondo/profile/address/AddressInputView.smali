.class public final Lco/uk/getmondo/profile/address/AddressInputView;
.super Landroid/widget/LinearLayout;
.source "AddressInputView.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u000c\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008R\u0011\u0010\t\u001a\u00020\n8F\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000cR\u0017\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e8F\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\u0012\u001a\u00020\u00138F\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0015R\u0011\u0010\u0016\u001a\u00020\u00178F\u00a2\u0006\u0006\u001a\u0004\u0008\u0018\u0010\u0019R\u0011\u0010\u001a\u001a\u00020\u00138F\u00a2\u0006\u0006\u001a\u0004\u0008\u001b\u0010\u0015R(\u0010\u001d\u001a\u0004\u0018\u00010\n2\u0008\u0010\u001c\u001a\u0004\u0018\u00010\n@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001e\u0010\u000c\"\u0004\u0008\u001f\u0010 R\u0011\u0010!\u001a\u00020\u00138F\u00a2\u0006\u0006\u001a\u0004\u0008\"\u0010\u0015\u00a8\u0006#"
    }
    d2 = {
        "Lco/uk/getmondo/profile/address/AddressInputView;",
        "Landroid/widget/LinearLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyle",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "address",
        "Lco/uk/getmondo/api/model/Address;",
        "getAddress",
        "()Lco/uk/getmondo/api/model/Address;",
        "addressInputValid",
        "Lio/reactivex/Observable;",
        "",
        "getAddressInputValid",
        "()Lio/reactivex/Observable;",
        "city",
        "",
        "getCity",
        "()Ljava/lang/String;",
        "legacyAddress",
        "Lco/uk/getmondo/model/LegacyAddress;",
        "getLegacyAddress",
        "()Lco/uk/getmondo/model/LegacyAddress;",
        "postcode",
        "getPostcode",
        "value",
        "preFilledAddress",
        "getPreFilledAddress",
        "setPreFilledAddress",
        "(Lco/uk/getmondo/api/model/Address;)V",
        "street",
        "getStreet",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private a:Lco/uk/getmondo/api/model/Address;

.field private b:Ljava/util/HashMap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    move-object v0, p0

    move-object v1, p1

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/profile/address/AddressInputView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/profile/address/AddressInputView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    invoke-virtual {p0, v1}, Lco/uk/getmondo/profile/address/AddressInputView;->setOrientation(I)V

    .line 27
    invoke-virtual {p0, v1}, Lco/uk/getmondo/profile/address/AddressInputView;->setFocusable(Z)V

    .line 28
    invoke-virtual {p0, v1}, Lco/uk/getmondo/profile/address/AddressInputView;->setFocusableInTouchMode(Z)V

    .line 29
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f05013b

    check-cast p0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V
    .locals 2

    .prologue
    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_1

    .line 21
    const/4 v0, 0x0

    check-cast v0, Landroid/util/AttributeSet;

    :goto_0
    and-int/lit8 v1, p4, 0x4

    if-eqz v1, :cond_0

    .line 22
    const/4 p3, 0x0

    :cond_0
    invoke-direct {p0, p1, v0, p3}, Lco/uk/getmondo/profile/address/AddressInputView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void

    :cond_1
    move-object v0, p2

    goto :goto_0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/profile/address/AddressInputView;->b:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/profile/address/AddressInputView;->b:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/profile/address/AddressInputView;->b:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/profile/address/AddressInputView;->b:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public final getAddress()Lco/uk/getmondo/api/model/Address;
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 54
    invoke-virtual {p0}, Lco/uk/getmondo/profile/address/AddressInputView;->getStreet()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    new-array v1, v6, [Ljava/lang/String;

    const-string v3, ","

    aput-object v3, v1, v2

    const/4 v4, 0x6

    move v3, v2

    invoke-static/range {v0 .. v5}, Lkotlin/h/j;->b(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-virtual {p0}, Lco/uk/getmondo/profile/address/AddressInputView;->getCity()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/a/m;->a(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 103
    new-instance v1, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v0, v3}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 104
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 105
    check-cast v0, Ljava/lang/String;

    .line 54
    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 106
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 54
    nop

    .line 107
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 108
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v1, v3

    check-cast v1, Ljava/lang/String;

    .line 54
    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lkotlin/h/j;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    move v1, v6

    :goto_2
    if-eqz v1, :cond_2

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2

    :cond_4
    move-object v6, v0

    .line 109
    check-cast v6, Ljava/util/List;

    .line 55
    iget-object v1, p0, Lco/uk/getmondo/profile/address/AddressInputView;->a:Lco/uk/getmondo/api/model/Address;

    .line 56
    if-eqz v1, :cond_5

    .line 57
    instance-of v0, v1, Lco/uk/getmondo/api/model/ApiAddress;

    if-eqz v0, :cond_5

    invoke-interface {v1}, Lco/uk/getmondo/api/model/Address;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v0, v6}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    move-object v0, v1

    check-cast v0, Lco/uk/getmondo/api/model/ApiAddress;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/ApiAddress;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/profile/address/AddressInputView;->getPostcode()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 56
    :goto_3
    return-object v1

    .line 64
    :cond_5
    new-instance v4, Lco/uk/getmondo/api/model/ApiAddress;

    const-string v7, ""

    invoke-virtual {p0}, Lco/uk/getmondo/profile/address/AddressInputView;->getPostcode()Ljava/lang/String;

    move-result-object v8

    sget-object v0, Lco/uk/getmondo/d/i;->UNITED_KINGDOM:Lco/uk/getmondo/d/i;

    invoke-virtual {v0}, Lco/uk/getmondo/d/i;->e()Ljava/lang/String;

    move-result-object v9

    invoke-direct/range {v4 .. v9}, Lco/uk/getmondo/api/model/ApiAddress;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v4, Lco/uk/getmondo/api/model/Address;

    move-object v1, v4

    goto :goto_3
.end method

.method public final getAddressInputValid()Lio/reactivex/n;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    sget v0, Lco/uk/getmondo/c$a;->addressStreetEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressInputView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputEditText;

    .line 100
    invoke-static {v0}, Lcom/b/a/d/e;->c(Landroid/widget/TextView;)Lcom/b/a/a;

    move-result-object v0

    const-string v1, "RxTextView.textChanges(this)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/r;

    .line 35
    sget v1, Lco/uk/getmondo/c$a;->addressPostcodeEditText:I

    invoke-virtual {p0, v1}, Lco/uk/getmondo/profile/address/AddressInputView;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/design/widget/TextInputEditText;

    .line 101
    invoke-static {v1}, Lcom/b/a/d/e;->c(Landroid/widget/TextView;)Lcom/b/a/a;

    move-result-object v1

    const-string v2, "RxTextView.textChanges(this)"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/r;

    .line 36
    sget v2, Lco/uk/getmondo/c$a;->addressCityEditText:I

    invoke-virtual {p0, v2}, Lco/uk/getmondo/profile/address/AddressInputView;->a(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/design/widget/TextInputEditText;

    .line 102
    invoke-static {v2}, Lcom/b/a/d/e;->c(Landroid/widget/TextView;)Lcom/b/a/a;

    move-result-object v2

    const-string v3, "RxTextView.textChanges(this)"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lio/reactivex/r;

    .line 37
    sget-object v3, Lco/uk/getmondo/profile/address/AddressInputView$a;->a:Lco/uk/getmondo/profile/address/AddressInputView$a;

    check-cast v3, Lio/reactivex/c/i;

    .line 33
    invoke-static {v0, v1, v2, v3}, Lio/reactivex/n;->combineLatest(Lio/reactivex/r;Lio/reactivex/r;Lio/reactivex/r;Lio/reactivex/c/i;)Lio/reactivex/n;

    move-result-object v0

    .line 40
    invoke-virtual {v0}, Lio/reactivex/n;->distinctUntilChanged()Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getCity()Ljava/lang/String;
    .locals 2

    .prologue
    .line 47
    sget v0, Lco/uk/getmondo/c$a;->addressCityEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressInputView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputEditText;

    invoke-virtual {v0}, Landroid/support/design/widget/TextInputEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getLegacyAddress()Lco/uk/getmondo/d/s;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 51
    new-instance v0, Lco/uk/getmondo/d/s;

    invoke-virtual {p0}, Lco/uk/getmondo/profile/address/AddressInputView;->getPostcode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lco/uk/getmondo/profile/address/AddressInputView;->getStreet()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lco/uk/getmondo/common/k/a;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lco/uk/getmondo/profile/address/AddressInputView;->getCity()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lco/uk/getmondo/d/i;->UNITED_KINGDOM:Lco/uk/getmondo/d/i;

    invoke-virtual {v4}, Lco/uk/getmondo/d/i;->e()Ljava/lang/String;

    move-result-object v4

    const/16 v6, 0x10

    move-object v7, v5

    invoke-direct/range {v0 .. v7}, Lco/uk/getmondo/d/s;-><init>(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILkotlin/d/b/i;)V

    return-object v0
.end method

.method public final getPostcode()Ljava/lang/String;
    .locals 2

    .prologue
    .line 45
    sget v0, Lco/uk/getmondo/c$a;->addressPostcodeEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressInputView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputEditText;

    invoke-virtual {v0}, Landroid/support/design/widget/TextInputEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getPreFilledAddress()Lco/uk/getmondo/api/model/Address;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lco/uk/getmondo/profile/address/AddressInputView;->a:Lco/uk/getmondo/api/model/Address;

    return-object v0
.end method

.method public final getStreet()Ljava/lang/String;
    .locals 2

    .prologue
    .line 43
    sget v0, Lco/uk/getmondo/c$a;->addressStreetEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressInputView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputEditText;

    invoke-virtual {v0}, Landroid/support/design/widget/TextInputEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final setPreFilledAddress(Lco/uk/getmondo/api/model/Address;)V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 70
    iput-object p1, p0, Lco/uk/getmondo/profile/address/AddressInputView;->a:Lco/uk/getmondo/api/model/Address;

    .line 72
    sget v0, Lco/uk/getmondo/c$a;->addressStreetInputLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressInputView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0, v4}, Landroid/support/design/widget/TextInputLayout;->setHintAnimationEnabled(Z)V

    .line 73
    sget v0, Lco/uk/getmondo/c$a;->addressCityInputLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressInputView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0, v4}, Landroid/support/design/widget/TextInputLayout;->setHintAnimationEnabled(Z)V

    .line 74
    sget v0, Lco/uk/getmondo/c$a;->addressPostcodeInputLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressInputView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0, v4}, Landroid/support/design/widget/TextInputLayout;->setHintAnimationEnabled(Z)V

    .line 78
    instance-of v0, p1, Lco/uk/getmondo/api/model/ApiAddress;

    if-eqz v0, :cond_2

    invoke-interface {p1}, Lco/uk/getmondo/api/model/Address;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/m;->g(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v9, v0

    .line 83
    :goto_0
    instance-of v0, p1, Lco/uk/getmondo/api/model/ApiAddress;

    if-eqz v0, :cond_4

    invoke-interface {p1}, Lco/uk/getmondo/api/model/Address;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v0, v10}, Lkotlin/a/m;->d(Ljava/util/List;I)Ljava/util/List;

    move-result-object v0

    .line 86
    :cond_0
    :goto_1
    check-cast v0, Ljava/lang/Iterable;

    .line 110
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 111
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    .line 86
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v10

    :goto_3
    if-eqz v0, :cond_1

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 79
    :cond_2
    instance-of v0, p1, Lco/uk/getmondo/d/s;

    if-eqz v0, :cond_3

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/d/s;

    invoke-virtual {v0}, Lco/uk/getmondo/d/s;->i()Ljava/lang/String;

    move-result-object v9

    goto :goto_0

    .line 80
    :cond_3
    const-string v9, ""

    goto :goto_0

    .line 84
    :cond_4
    if-eqz p1, :cond_5

    invoke-interface {p1}, Lco/uk/getmondo/api/model/Address;->a()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    :cond_5
    invoke-static {}, Lkotlin/a/m;->a()Ljava/util/List;

    move-result-object v0

    goto :goto_1

    :cond_6
    move v0, v4

    .line 86
    goto :goto_3

    .line 112
    :cond_7
    check-cast v1, Ljava/util/List;

    move-object v0, v1

    check-cast v0, Ljava/lang/Iterable;

    .line 86
    const-string v1, ", "

    check-cast v1, Ljava/lang/CharSequence;

    const/16 v7, 0x3e

    move-object v3, v2

    move-object v5, v2

    move-object v6, v2

    move-object v8, v2

    invoke-static/range {v0 .. v8}, Lkotlin/a/m;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/d/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 88
    sget v0, Lco/uk/getmondo/c$a;->addressStreetEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressInputView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputEditText;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputEditText;->setText(Ljava/lang/CharSequence;)V

    .line 89
    sget v0, Lco/uk/getmondo/c$a;->addressCityEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressInputView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputEditText;

    move-object v1, v9

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputEditText;->setText(Ljava/lang/CharSequence;)V

    .line 90
    sget v0, Lco/uk/getmondo/c$a;->addressPostcodeEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressInputView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputEditText;

    if-eqz p1, :cond_8

    invoke-interface {p1}, Lco/uk/getmondo/api/model/Address;->c()Ljava/lang/String;

    move-result-object v1

    :goto_4
    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputEditText;->setText(Ljava/lang/CharSequence;)V

    .line 93
    sget v0, Lco/uk/getmondo/c$a;->addressStreetInputLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressInputView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0, v10}, Landroid/support/design/widget/TextInputLayout;->setHintAnimationEnabled(Z)V

    .line 94
    sget v0, Lco/uk/getmondo/c$a;->addressCityInputLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressInputView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0, v10}, Landroid/support/design/widget/TextInputLayout;->setHintAnimationEnabled(Z)V

    .line 95
    sget v0, Lco/uk/getmondo/c$a;->addressPostcodeInputLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressInputView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0, v10}, Landroid/support/design/widget/TextInputLayout;->setHintAnimationEnabled(Z)V

    .line 97
    if-eqz p1, :cond_9

    invoke-virtual {p0}, Lco/uk/getmondo/profile/address/AddressInputView;->requestFocus()Z

    .line 98
    :goto_5
    return-void

    :cond_8
    move-object v1, v2

    .line 90
    goto :goto_4

    .line 97
    :cond_9
    sget v0, Lco/uk/getmondo/c$a;->addressStreetEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressInputView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputEditText;

    invoke-virtual {v0}, Landroid/support/design/widget/TextInputEditText;->requestFocus()Z

    goto :goto_5
.end method
