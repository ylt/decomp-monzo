.class public final Lco/uk/getmondo/profile/address/EnterAddressActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "EnterAddressActivity.kt"

# interfaces
.implements Lco/uk/getmondo/profile/address/g$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/profile/address/EnterAddressActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 \u001e2\u00020\u00012\u00020\u0002:\u0001\u001eB\u0005\u00a2\u0006\u0002\u0010\u0003J\u000e\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0012H\u0016J\u000e\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0012H\u0016J\u0012\u0010\u0015\u001a\u00020\u00162\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0014J\u0010\u0010\u0019\u001a\u00020\u00162\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0012\u0010\u001a\u001a\u00020\u00162\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u001bH\u0016J\u0010\u0010\u001c\u001a\u00020\u00162\u0006\u0010\u001d\u001a\u00020\u0013H\u0016R#\u0010\u0004\u001a\n \u0006*\u0004\u0018\u00010\u00050\u00058BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\t\u0010\n\u001a\u0004\u0008\u0007\u0010\u0008R\u001e\u0010\u000b\u001a\u00020\u000c8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\r\u0010\u000e\"\u0004\u0008\u000f\u0010\u0010\u00a8\u0006\u001f"
    }
    d2 = {
        "Lco/uk/getmondo/profile/address/EnterAddressActivity;",
        "Lco/uk/getmondo/common/activities/BaseActivity;",
        "Lco/uk/getmondo/profile/address/EnterAddressPresenter$View;",
        "()V",
        "address",
        "Lco/uk/getmondo/model/LegacyAddress;",
        "kotlin.jvm.PlatformType",
        "getAddress",
        "()Lco/uk/getmondo/model/LegacyAddress;",
        "address$delegate",
        "Lkotlin/Lazy;",
        "presenter",
        "Lco/uk/getmondo/profile/address/EnterAddressPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/profile/address/EnterAddressPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/profile/address/EnterAddressPresenter;)V",
        "onAddressInputValid",
        "Lio/reactivex/Observable;",
        "",
        "onConfirmAddress",
        "onCreate",
        "",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "openPinEntry",
        "setAddress",
        "Lco/uk/getmondo/api/model/Address;",
        "setConfirmEnabled",
        "enabled",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;

.field public static final c:Lco/uk/getmondo/profile/address/EnterAddressActivity$a;


# instance fields
.field public b:Lco/uk/getmondo/profile/address/g;

.field private final e:Lkotlin/c;

.field private f:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/profile/address/EnterAddressActivity;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "address"

    const-string v5, "getAddress()Lco/uk/getmondo/model/LegacyAddress;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lco/uk/getmondo/profile/address/EnterAddressActivity;->a:[Lkotlin/reflect/l;

    new-instance v0, Lco/uk/getmondo/profile/address/EnterAddressActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/profile/address/EnterAddressActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/profile/address/EnterAddressActivity;->c:Lco/uk/getmondo/profile/address/EnterAddressActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 23
    new-instance v0, Lco/uk/getmondo/profile/address/EnterAddressActivity$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/profile/address/EnterAddressActivity$b;-><init>(Lco/uk/getmondo/profile/address/EnterAddressActivity;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/profile/address/EnterAddressActivity;->e:Lkotlin/c;

    return-void
.end method

.method private final c()Lco/uk/getmondo/d/s;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/profile/address/EnterAddressActivity;->e:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/profile/address/EnterAddressActivity;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/s;

    return-object v0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/profile/address/EnterAddressActivity;->f:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/profile/address/EnterAddressActivity;->f:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/profile/address/EnterAddressActivity;->f:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/profile/address/EnterAddressActivity;->f:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public a()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    sget v0, Lco/uk/getmondo/c$a;->profileAddressInputView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/EnterAddressActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/profile/address/AddressInputView;

    invoke-virtual {v0}, Lco/uk/getmondo/profile/address/AddressInputView;->getAddressInputValid()Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public a(Lco/uk/getmondo/api/model/Address;)V
    .locals 1

    .prologue
    .line 47
    sget v0, Lco/uk/getmondo/c$a;->profileAddressInputView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/EnterAddressActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/profile/address/AddressInputView;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/profile/address/AddressInputView;->setPreFilledAddress(Lco/uk/getmondo/api/model/Address;)V

    .line 48
    return-void
.end method

.method public a(Lco/uk/getmondo/d/s;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const-string v0, "address"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    sget-object v1, Lco/uk/getmondo/settings/SettingsActivity;->c:Lco/uk/getmondo/settings/SettingsActivity$a;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/settings/SettingsActivity$a;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 52
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    .line 53
    sget-object v7, Lco/uk/getmondo/common/pin/PinEntryActivity;->c:Lco/uk/getmondo/common/pin/PinEntryActivity$a;

    move-object v6, p0

    check-cast v6, Landroid/content/Context;

    new-instance v0, Lco/uk/getmondo/common/pin/a/a/b$a;

    const-string v2, "confirmationIntent"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x4

    move-object v2, p1

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/common/pin/a/a/b$a;-><init>(Landroid/content/Intent;Lco/uk/getmondo/d/s;Lco/uk/getmondo/api/model/tracking/Impression$PinFrom;ILkotlin/d/b/i;)V

    check-cast v0, Lco/uk/getmondo/common/pin/a/a/b;

    invoke-virtual {v7, v6, v0}, Lco/uk/getmondo/common/pin/PinEntryActivity$a;->a(Landroid/content/Context;Lco/uk/getmondo/common/pin/a/a/b;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/EnterAddressActivity;->startActivity(Landroid/content/Intent;)V

    .line 54
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 43
    sget v0, Lco/uk/getmondo/c$a;->addressConfirmButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/EnterAddressActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 44
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/d/s;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    sget v0, Lco/uk/getmondo/c$a;->addressConfirmButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/EnterAddressActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 66
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    const-string v0, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    new-instance v0, Lco/uk/getmondo/profile/address/EnterAddressActivity$c;

    invoke-direct {v0, p0}, Lco/uk/getmondo/profile/address/EnterAddressActivity$c;-><init>(Lco/uk/getmondo/profile/address/EnterAddressActivity;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "addressConfirmButton.cli\u2026InputView.legacyAddress }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 26
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 27
    const v0, 0x7f050035

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/EnterAddressActivity;->setContentView(I)V

    .line 29
    invoke-virtual {p0}, Lco/uk/getmondo/profile/address/EnterAddressActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v1

    new-instance v2, Lco/uk/getmondo/profile/address/c;

    invoke-direct {p0}, Lco/uk/getmondo/profile/address/EnterAddressActivity;->c()Lco/uk/getmondo/d/s;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/Address;

    invoke-direct {v2, v0}, Lco/uk/getmondo/profile/address/c;-><init>(Lco/uk/getmondo/api/model/Address;)V

    invoke-interface {v1, v2}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/profile/address/c;)Lco/uk/getmondo/profile/address/b;

    move-result-object v0

    .line 30
    invoke-interface {v0, p0}, Lco/uk/getmondo/profile/address/b;->a(Lco/uk/getmondo/profile/address/EnterAddressActivity;)V

    .line 32
    iget-object v0, p0, Lco/uk/getmondo/profile/address/EnterAddressActivity;->b:Lco/uk/getmondo/profile/address/g;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    check-cast p0, Lco/uk/getmondo/profile/address/g$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/profile/address/g;->a(Lco/uk/getmondo/profile/address/g$a;)V

    .line 33
    return-void
.end method
