.class public final Lco/uk/getmondo/profile/address/c;
.super Ljava/lang/Object;
.source "AddressModule.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0007\u0018\u00002\u00020\u0001B\u000f\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004J\n\u0010\u0007\u001a\u0004\u0018\u00010\u0003H\u0007R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0008"
    }
    d2 = {
        "Lco/uk/getmondo/profile/address/AddressModule;",
        "",
        "address",
        "Lco/uk/getmondo/api/model/Address;",
        "(Lco/uk/getmondo/api/model/Address;)V",
        "getAddress",
        "()Lco/uk/getmondo/api/model/Address;",
        "provideAddress",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/api/model/Address;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/api/model/Address;)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/profile/address/c;->a:Lco/uk/getmondo/api/model/Address;

    return-void
.end method


# virtual methods
.method public final a()Lco/uk/getmondo/api/model/Address;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lco/uk/getmondo/profile/address/c;->a:Lco/uk/getmondo/api/model/Address;

    return-object v0
.end method
