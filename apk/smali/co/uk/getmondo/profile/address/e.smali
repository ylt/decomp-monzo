.class public final Lco/uk/getmondo/profile/address/e;
.super Lco/uk/getmondo/common/ui/b;
.source "AddressSelectionPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/profile/address/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/profile/address/e$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0007B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0002H\u0016\u00a8\u0006\u0008"
    }
    d2 = {
        "Lco/uk/getmondo/profile/address/AddressSelectionPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/profile/address/AddressSelectionPresenter$View;",
        "()V",
        "register",
        "",
        "view",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lco/uk/getmondo/profile/address/e$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/profile/address/e;->a(Lco/uk/getmondo/profile/address/e$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/profile/address/e$a;)V
    .locals 3

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 11
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 13
    iget-object v1, p0, Lco/uk/getmondo/profile/address/e;->b:Lio/reactivex/b/a;

    .line 16
    invoke-interface {p1}, Lco/uk/getmondo/profile/address/e$a;->a()Lio/reactivex/n;

    move-result-object v2

    .line 14
    sget-object v0, Lco/uk/getmondo/profile/address/e$b;->a:Lco/uk/getmondo/profile/address/e$b;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 15
    invoke-virtual {v0}, Lio/reactivex/n;->distinctUntilChanged()Lio/reactivex/n;

    move-result-object v2

    .line 16
    new-instance v0, Lco/uk/getmondo/profile/address/e$c;

    invoke-direct {v0, p1}, Lco/uk/getmondo/profile/address/e$c;-><init>(Lco/uk/getmondo/profile/address/e$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onPostcodeChanged()\u2026imaryButtonEnabled = it }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/profile/address/e;->b:Lio/reactivex/b/a;

    .line 18
    iget-object v1, p0, Lco/uk/getmondo/profile/address/e;->b:Lio/reactivex/b/a;

    .line 19
    invoke-interface {p1}, Lco/uk/getmondo/profile/address/e$a;->a()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/profile/address/e$d;

    invoke-direct {v0, p1}, Lco/uk/getmondo/profile/address/e$d;-><init>(Lco/uk/getmondo/profile/address/e$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onPostcodeChanged()\u2026 = true\n                }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/profile/address/e;->b:Lio/reactivex/b/a;

    .line 26
    iget-object v1, p0, Lco/uk/getmondo/profile/address/e;->b:Lio/reactivex/b/a;

    .line 27
    invoke-interface {p1}, Lco/uk/getmondo/profile/address/e$a;->b()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/profile/address/e$e;

    invoke-direct {v0, p1}, Lco/uk/getmondo/profile/address/e$e;-><init>(Lco/uk/getmondo/profile/address/e$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onViewedAddresses()\u2026ddressNotInListButton() }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/profile/address/e;->b:Lio/reactivex/b/a;

    .line 28
    return-void
.end method
