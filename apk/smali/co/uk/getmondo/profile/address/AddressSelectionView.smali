.class public final Lco/uk/getmondo/profile/address/AddressSelectionView;
.super Landroid/widget/LinearLayout;
.source "AddressSelectionView.kt"

# interfaces
.implements Lco/uk/getmondo/profile/address/e$a;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\t\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\r\n\u0002\u0008\u0008\n\u0002\u0010 \n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002B%\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u000c\u0010#\u001a\u0008\u0012\u0004\u0012\u00020%0$J\u000c\u0010&\u001a\u0008\u0012\u0004\u0012\u00020%0$J\u000c\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020(0$J\u0008\u0010)\u001a\u00020%H\u0016J\u0008\u0010*\u001a\u00020%H\u0016J\u0008\u0010+\u001a\u00020%H\u0016J\u0008\u0010,\u001a\u00020%H\u0014J\u0008\u0010-\u001a\u00020%H\u0014J\u000e\u0010.\u001a\u0008\u0012\u0004\u0012\u00020/0$H\u0016J\u000e\u00100\u001a\u0008\u0012\u0004\u0012\u00020%0$H\u0016J\u000c\u00101\u001a\u0008\u0012\u0004\u0012\u00020\u00170$J\u000c\u00102\u001a\u0008\u0012\u0004\u0012\u00020%0$J\u0006\u00103\u001a\u00020%J\u0008\u00104\u001a\u00020%H\u0016J\u0008\u00105\u001a\u00020%H\u0016J\u0014\u00106\u001a\u00020%2\u000c\u00107\u001a\u0008\u0012\u0004\u0012\u00020(08J\u0006\u00109\u001a\u00020%R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u000e\u001a\u00020\r2\u0006\u0010\u000c\u001a\u00020\r8V@VX\u0096\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u000e\u0010\u000f\"\u0004\u0008\u0010\u0010\u0011R$\u0010\u0012\u001a\u00020\r2\u0006\u0010\u000c\u001a\u00020\r8V@VX\u0096\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0012\u0010\u000f\"\u0004\u0008\u0013\u0010\u0011R$\u0010\u0014\u001a\u00020\r2\u0006\u0010\u000c\u001a\u00020\r8V@VX\u0096\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0014\u0010\u000f\"\u0004\u0008\u0015\u0010\u0011R\u0011\u0010\u0016\u001a\u00020\u00178F\u00a2\u0006\u0006\u001a\u0004\u0008\u0018\u0010\u0019R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u001c\u001a\u00020\u00172\u0006\u0010\u000c\u001a\u00020\u00178F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u001d\u0010\u0019\"\u0004\u0008\u001e\u0010\u001fR$\u0010 \u001a\u00020\u00172\u0006\u0010\u000c\u001a\u00020\u00178F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008!\u0010\u0019\"\u0004\u0008\"\u0010\u001f\u00a8\u0006:"
    }
    d2 = {
        "Lco/uk/getmondo/profile/address/AddressSelectionView;",
        "Landroid/widget/LinearLayout;",
        "Lco/uk/getmondo/profile/address/AddressSelectionPresenter$View;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyle",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "addressAdapter",
        "Lco/uk/getmondo/profile/address/AddressAdapter;",
        "value",
        "",
        "isAddressLoading",
        "()Z",
        "setAddressLoading",
        "(Z)V",
        "isPrimaryButtonEnabled",
        "setPrimaryButtonEnabled",
        "isPrimaryButtonVisible",
        "setPrimaryButtonVisible",
        "postcode",
        "",
        "getPostcode",
        "()Ljava/lang/String;",
        "presenter",
        "Lco/uk/getmondo/profile/address/AddressSelectionPresenter;",
        "primaryButtonText",
        "getPrimaryButtonText",
        "setPrimaryButtonText",
        "(Ljava/lang/String;)V",
        "title",
        "getTitle",
        "setTitle",
        "addressNotInListButtonClicks",
        "Lio/reactivex/Observable;",
        "",
        "addressResidenceButtonClicks",
        "addressSelected",
        "Lco/uk/getmondo/api/model/Address;",
        "clearPostcodeError",
        "hideAddressNotInListButton",
        "hideAddressPicker",
        "onAttachedToWindow",
        "onDetachedFromWindow",
        "onPostcodeChanged",
        "",
        "onViewedAddresses",
        "postcodeReady",
        "primaryButtonClicks",
        "showAddressNotFoundError",
        "showAddressNotInListButton",
        "showAddressPicker",
        "showAddresses",
        "addresses",
        "",
        "showInvalidPostcodeError",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/profile/address/a;

.field private final b:Lco/uk/getmondo/profile/address/e;

.field private c:Ljava/util/HashMap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    move-object v0, p0

    move-object v1, p1

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/profile/address/AddressSelectionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/profile/address/AddressSelectionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v3, 0x0

    const/4 v5, 0x0

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    new-instance v0, Lco/uk/getmondo/profile/address/a;

    const/4 v1, 0x3

    invoke-direct {v0, v3, v3, v1, v3}, Lco/uk/getmondo/profile/address/a;-><init>(Ljava/util/List;Lkotlin/d/a/b;ILkotlin/d/b/i;)V

    iput-object v0, p0, Lco/uk/getmondo/profile/address/AddressSelectionView;->a:Lco/uk/getmondo/profile/address/a;

    .line 33
    new-instance v0, Lco/uk/getmondo/profile/address/e;

    invoke-direct {v0}, Lco/uk/getmondo/profile/address/e;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/profile/address/AddressSelectionView;->b:Lco/uk/getmondo/profile/address/e;

    .line 50
    invoke-virtual {p0, v11}, Lco/uk/getmondo/profile/address/AddressSelectionView;->setOrientation(I)V

    .line 51
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f05013c

    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 53
    new-instance v2, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-direct {v2, p1}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 54
    sget v0, Lco/uk/getmondo/c$a;->addressRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-direct {v1, p1}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    check-cast v1, Landroid/support/v7/widget/RecyclerView$h;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$h;)V

    .line 55
    sget v0, Lco/uk/getmondo/c$a;->addressRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lco/uk/getmondo/profile/address/AddressSelectionView;->a:Lco/uk/getmondo/profile/address/a;

    check-cast v1, Landroid/support/v7/widget/RecyclerView$a;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 56
    sget v0, Lco/uk/getmondo/c$a;->addressRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Landroid/support/v7/widget/RecyclerView;

    new-instance v0, Lco/uk/getmondo/common/ui/e;

    .line 58
    invoke-virtual {v2}, Landroid/support/v7/widget/LinearLayoutManager;->h()I

    move-result v2

    .line 59
    invoke-virtual {p0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0b0113

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    const/16 v8, 0x74

    move-object v1, p1

    move v6, v5

    move v7, v5

    move-object v9, v3

    .line 56
    invoke-direct/range {v0 .. v9}, Lco/uk/getmondo/common/ui/e;-><init>(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;IIIIILkotlin/d/b/i;)V

    check-cast v0, Landroid/support/v7/widget/RecyclerView$g;

    invoke-virtual {v10, v0}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$g;)V

    .line 62
    if-eqz p2, :cond_0

    .line 63
    sget-object v0, Lco/uk/getmondo/c$b;->AddressSelectionView:[I

    invoke-virtual {p1, p2, v0, v5, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 64
    invoke-virtual {v1, v11}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    :goto_0
    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->setPrimaryButtonText(Ljava/lang/String;)V

    .line 65
    invoke-virtual {v1, v5}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    :goto_1
    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->setTitle(Ljava/lang/String;)V

    .line 66
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    :cond_0
    return-void

    .line 64
    :cond_1
    const-string v0, ""

    goto :goto_0

    .line 65
    :cond_2
    const-string v0, ""

    goto :goto_1
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/d/b/i;)V
    .locals 2

    .prologue
    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_1

    .line 28
    const/4 v0, 0x0

    check-cast v0, Landroid/util/AttributeSet;

    :goto_0
    and-int/lit8 v1, p4, 0x4

    if-eqz v1, :cond_0

    .line 29
    const/4 p3, 0x0

    :cond_0
    invoke-direct {p0, p1, v0, p3}, Lco/uk/getmondo/profile/address/AddressSelectionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void

    :cond_1
    move-object v0, p2

    goto :goto_0
.end method

.method public static final synthetic a(Lco/uk/getmondo/profile/address/AddressSelectionView;)Lco/uk/getmondo/profile/address/a;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lco/uk/getmondo/profile/address/AddressSelectionView;->a:Lco/uk/getmondo/profile/address/a;

    return-object v0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/profile/address/AddressSelectionView;->c:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/profile/address/AddressSelectionView;->c:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/profile/address/AddressSelectionView;->c:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/profile/address/AddressSelectionView;->c:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .prologue
    .line 116
    sget v0, Lco/uk/getmondo/c$a;->addressPostcodeEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputEditText;

    .line 237
    invoke-static {v0}, Lcom/b/a/d/e;->c(Landroid/widget/TextView;)Lcom/b/a/a;

    move-result-object v0

    const-string v1, "RxTextView.textChanges(this)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    invoke-virtual {v0}, Lcom/b/a/a;->b()Lio/reactivex/n;

    move-result-object v0

    const-string v1, "addressPostcodeEditText.\u2026nges().skipInitialValue()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lco/uk/getmondo/api/model/Address;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "addresses"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    sget v0, Lco/uk/getmondo/c$a;->addressPostcodeEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputEditText;

    invoke-virtual {v0}, Landroid/support/design/widget/TextInputEditText;->clearFocus()V

    .line 152
    sget v0, Lco/uk/getmondo/c$a;->addressPostcodeEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputEditText;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->c(Landroid/view/View;)V

    .line 155
    new-instance v0, Lco/uk/getmondo/profile/address/AddressSelectionView$i;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/profile/address/AddressSelectionView$i;-><init>(Lco/uk/getmondo/profile/address/AddressSelectionView;Ljava/util/List;)V

    check-cast v0, Ljava/lang/Runnable;

    .line 161
    const-wide/16 v2, 0x12c

    .line 155
    invoke-virtual {p0, v0, v2, v3}, Lco/uk/getmondo/profile/address/AddressSelectionView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 162
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    new-instance v0, Lco/uk/getmondo/profile/address/AddressSelectionView$d;

    invoke-direct {v0, p0}, Lco/uk/getmondo/profile/address/AddressSelectionView$d;-><init>(Lco/uk/getmondo/profile/address/AddressSelectionView;)V

    check-cast v0, Lio/reactivex/p;

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v1

    .line 131
    new-instance v0, Lco/uk/getmondo/profile/address/AddressSelectionView$e;

    invoke-direct {v0, p0}, Lco/uk/getmondo/profile/address/AddressSelectionView$e;-><init>(Lco/uk/getmondo/profile/address/AddressSelectionView;)V

    check-cast v0, Lio/reactivex/c/q;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v1

    .line 132
    sget-object v0, Lco/uk/getmondo/profile/address/AddressSelectionView$f;->a:Lco/uk/getmondo/profile/address/AddressSelectionView$f;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.create<Unit> \u2026\n                .map { }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c()V
    .locals 3

    .prologue
    .line 211
    sget v0, Lco/uk/getmondo/c$a;->addressNotInListButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 220
    :goto_0
    return-void

    .line 213
    :cond_0
    invoke-virtual {p0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 214
    instance-of v0, v2, Landroid/app/Activity;

    if-eqz v0, :cond_1

    .line 215
    sget v0, Lco/uk/getmondo/c$a;->addressNotInListButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lco/uk/getmondo/profile/address/AddressSelectionView$h;

    invoke-direct {v1, v2}, Lco/uk/getmondo/profile/address/AddressSelectionView$h;-><init>(Landroid/content/Context;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->post(Ljava/lang/Runnable;)Z

    .line 219
    :cond_1
    sget v0, Lco/uk/getmondo/c$a;->addressNotInListButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method public d()V
    .locals 3

    .prologue
    .line 223
    sget v0, Lco/uk/getmondo/c$a;->addressNotInListButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 232
    :cond_0
    :goto_0
    return-void

    .line 225
    :cond_1
    sget v0, Lco/uk/getmondo/c$a;->addressNotInListButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 226
    invoke-virtual {p0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 227
    instance-of v0, v2, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 228
    sget v0, Lco/uk/getmondo/c$a;->addressNotInListButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lco/uk/getmondo/profile/address/AddressSelectionView$b;

    invoke-direct {v1, v2}, Lco/uk/getmondo/profile/address/AddressSelectionView$b;-><init>(Landroid/content/Context;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public e()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 187
    invoke-virtual {p0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0062

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v2, v0

    .line 189
    sget v0, Lco/uk/getmondo/c$a;->addressPickerLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/ConstraintLayout;

    invoke-virtual {v0}, Landroid/support/constraint/ConstraintLayout;->getTranslationY()F

    move-result v0

    cmpg-float v0, v0, v2

    if-nez v0, :cond_0

    .line 208
    :goto_0
    return-void

    .line 194
    :cond_0
    sget v0, Lco/uk/getmondo/c$a;->addressScrollView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, 0x0

    invoke-direct {v1, v4, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 195
    const/high16 v3, 0x3f800000    # 1.0f

    iput v3, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    check-cast v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 197
    sget v0, Lco/uk/getmondo/c$a;->addressPickerLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/ConstraintLayout;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 199
    const/4 v3, -0x2

    .line 197
    invoke-direct {v1, v4, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    check-cast v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/support/constraint/ConstraintLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 202
    sget v0, Lco/uk/getmondo/c$a;->addressPickerLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/ConstraintLayout;

    move-object v1, v0

    .line 203
    check-cast v1, Landroid/view/View;

    invoke-static {v1}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 204
    invoke-virtual {v0}, Landroid/support/constraint/ConstraintLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 205
    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 206
    new-instance v0, Landroid/support/v4/view/b/b;

    invoke-direct {v0}, Landroid/support/v4/view/b/b;-><init>()V

    check-cast v0, Landroid/animation/TimeInterpolator;

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method public f()V
    .locals 2

    .prologue
    .line 146
    sget v0, Lco/uk/getmondo/c$a;->addressPostcodeInputLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 147
    sget v0, Lco/uk/getmondo/c$a;->addressPostcodeInputLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x0

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 148
    return-void
.end method

.method public final g()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 101
    sget v0, Lco/uk/getmondo/c$a;->addressResidenceEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputEditText;

    .line 234
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    return-object v0
.end method

.method public final getPostcode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    sget v0, Lco/uk/getmondo/c$a;->addressPostcodeEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputEditText;

    invoke-virtual {v0}, Landroid/support/design/widget/TextInputEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getPrimaryButtonText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    sget v0, Lco/uk/getmondo/c$a;->addressPrimaryButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    invoke-virtual {v0}, Lco/uk/getmondo/common/ui/ProgressButton;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    sget v0, Lco/uk/getmondo/c$a;->addressBodyText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    sget v0, Lco/uk/getmondo/c$a;->addressPrimaryButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    .line 235
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    return-object v0
.end method

.method public final i()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    invoke-virtual {p0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->h()Lio/reactivex/n;

    move-result-object v1

    new-instance v0, Lco/uk/getmondo/profile/address/AddressSelectionView$g;

    invoke-direct {v0, p0}, Lco/uk/getmondo/profile/address/AddressSelectionView$g;-><init>(Lco/uk/getmondo/profile/address/AddressSelectionView;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "primaryButtonClicks().map { postcode.trim() }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final j()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    sget v0, Lco/uk/getmondo/c$a;->addressNotInListButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 236
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    return-object v0
.end method

.method public final k()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/api/model/Address;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    new-instance v0, Lco/uk/getmondo/profile/address/AddressSelectionView$a;

    invoke-direct {v0, p0}, Lco/uk/getmondo/profile/address/AddressSelectionView$a;-><init>(Lco/uk/getmondo/profile/address/AddressSelectionView;)V

    check-cast v0, Lio/reactivex/p;

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.create { emit\u2026stener = null }\n        }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final l()V
    .locals 3

    .prologue
    .line 136
    sget v0, Lco/uk/getmondo/c$a;->addressPostcodeInputLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 137
    sget v0, Lco/uk/getmondo/c$a;->addressPostcodeInputLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {p0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a02bf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 138
    return-void
.end method

.method public final m()V
    .locals 3

    .prologue
    .line 141
    sget v0, Lco/uk/getmondo/c$a;->addressPostcodeInputLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 142
    sget v0, Lco/uk/getmondo/c$a;->addressPostcodeInputLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {p0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 143
    return-void
.end method

.method public n()V
    .locals 5

    .prologue
    const/4 v3, -0x1

    const/4 v4, 0x0

    .line 166
    sget v0, Lco/uk/getmondo/c$a;->addressScrollView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 168
    const/4 v2, -0x2

    .line 166
    invoke-direct {v1, v3, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 169
    iput v4, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    check-cast v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 171
    sget v0, Lco/uk/getmondo/c$a;->addressPickerLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/ConstraintLayout;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, 0x0

    invoke-direct {v1, v3, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 173
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 174
    invoke-virtual {p0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0115

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 175
    nop

    .line 172
    check-cast v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/support/constraint/ConstraintLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 178
    sget v0, Lco/uk/getmondo/c$a;->addressPickerLayout:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/ConstraintLayout;

    move-object v1, v0

    .line 179
    check-cast v1, Landroid/view/View;

    invoke-static {v1}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 180
    invoke-virtual {v0}, Landroid/support/constraint/ConstraintLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 181
    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 182
    new-instance v0, Landroid/support/v4/view/b/b;

    invoke-direct {v0}, Landroid/support/v4/view/b/b;-><init>()V

    check-cast v0, Landroid/animation/TimeInterpolator;

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    .line 184
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 71
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 72
    iget-object v1, p0, Lco/uk/getmondo/profile/address/AddressSelectionView;->b:Lco/uk/getmondo/profile/address/e;

    move-object v0, p0

    check-cast v0, Lco/uk/getmondo/profile/address/e$a;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/profile/address/e;->a(Lco/uk/getmondo/profile/address/e$a;)V

    .line 73
    new-instance v0, Lco/uk/getmondo/profile/address/AddressSelectionView$c;

    invoke-direct {v0, p0}, Lco/uk/getmondo/profile/address/AddressSelectionView$c;-><init>(Lco/uk/getmondo/profile/address/AddressSelectionView;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->post(Ljava/lang/Runnable;)Z

    .line 75
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lco/uk/getmondo/profile/address/AddressSelectionView;->b:Lco/uk/getmondo/profile/address/e;

    invoke-virtual {v0}, Lco/uk/getmondo/profile/address/e;->b()V

    .line 79
    invoke-static {p0}, Lco/uk/getmondo/common/ae;->c(Landroid/view/View;)V

    .line 80
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 81
    return-void
.end method

.method public setAddressLoading(Z)V
    .locals 1

    .prologue
    .line 98
    sget v0, Lco/uk/getmondo/c$a;->addressPrimaryButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/ui/ProgressButton;->setLoading(Z)V

    .line 99
    return-void
.end method

.method public setPrimaryButtonEnabled(Z)V
    .locals 1

    .prologue
    .line 86
    sget v0, Lco/uk/getmondo/c$a;->addressPrimaryButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/ui/ProgressButton;->setEnabled(Z)V

    .line 87
    return-void
.end method

.method public final setPrimaryButtonText(Ljava/lang/String;)V
    .locals 1

    .prologue
    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    sget v0, Lco/uk/getmondo/c$a;->addressPrimaryButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/ui/ProgressButton;->setText(Ljava/lang/CharSequence;)V

    .line 42
    return-void
.end method

.method public setPrimaryButtonVisible(Z)V
    .locals 2

    .prologue
    .line 92
    sget v0, Lco/uk/getmondo/c$a;->addressPrimaryButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    if-eqz p1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/ProgressButton;->setVisibility(I)V

    .line 93
    return-void

    .line 92
    :cond_0
    const/16 v1, 0x8

    goto :goto_0
.end method

.method public final setTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    sget v0, Lco/uk/getmondo/c$a;->addressBodyText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    return-void
.end method
