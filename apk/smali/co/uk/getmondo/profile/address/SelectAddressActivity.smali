.class public final Lco/uk/getmondo/profile/address/SelectAddressActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "SelectAddressActivity.kt"

# interfaces
.implements Lco/uk/getmondo/profile/address/k$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/profile/address/SelectAddressActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0010 \n\u0002\u0008\u0004\u0018\u0000 &2\u00020\u00012\u00020\u0002:\u0001&B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\n\u001a\u00020\u000bH\u0016J\u0008\u0010\u000c\u001a\u00020\u000bH\u0016J\u0008\u0010\r\u001a\u00020\u000bH\u0016J\u0008\u0010\u000e\u001a\u00020\u000bH\u0016J\u000e\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0010H\u0016J\u000e\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0010H\u0016J\u000e\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0010H\u0016J\u0012\u0010\u0015\u001a\u00020\u000b2\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0014J\u000e\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0010H\u0016J\u0012\u0010\u0019\u001a\u00020\u000b2\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u0012H\u0016J\u0010\u0010\u001b\u001a\u00020\u000b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0016J\u0008\u0010\u001e\u001a\u00020\u000bH\u0016J\u0008\u0010\u001f\u001a\u00020\u000bH\u0016J\u0008\u0010 \u001a\u00020\u000bH\u0016J\u0016\u0010!\u001a\u00020\u000b2\u000c\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\u00120#H\u0016J\u0008\u0010$\u001a\u00020\u000bH\u0016J\u0008\u0010%\u001a\u00020\u000bH\u0016R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\"\u0004\u0008\u0008\u0010\t\u00a8\u0006\'"
    }
    d2 = {
        "Lco/uk/getmondo/profile/address/SelectAddressActivity;",
        "Lco/uk/getmondo/common/activities/BaseActivity;",
        "Lco/uk/getmondo/profile/address/SelectAddressPresenter$View;",
        "()V",
        "presenter",
        "Lco/uk/getmondo/profile/address/SelectAddressPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/profile/address/SelectAddressPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/profile/address/SelectAddressPresenter;)V",
        "hideAddressLoading",
        "",
        "hideAddressNotFound",
        "hideAddressNotInList",
        "hideContinue",
        "onAddressNotInListClicked",
        "Lio/reactivex/Observable;",
        "onAddressSelected",
        "Lco/uk/getmondo/api/model/Address;",
        "onContinueClicked",
        "",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onResidenceClicked",
        "openConfirmAddress",
        "address",
        "setContinueEnabled",
        "enabled",
        "",
        "showAddressLoading",
        "showAddressNotFound",
        "showAddressNotInList",
        "showAddresses",
        "addresses",
        "",
        "showContinue",
        "showUkOnly",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final b:Lco/uk/getmondo/profile/address/SelectAddressActivity$a;


# instance fields
.field public a:Lco/uk/getmondo/profile/address/k;

.field private c:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/profile/address/SelectAddressActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/profile/address/SelectAddressActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/profile/address/SelectAddressActivity;->b:Lco/uk/getmondo/profile/address/SelectAddressActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/profile/address/SelectAddressActivity;->c:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/profile/address/SelectAddressActivity;->c:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/profile/address/SelectAddressActivity;->c:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/profile/address/SelectAddressActivity;->c:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public a()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    sget v0, Lco/uk/getmondo/c$a;->addressSelectionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/SelectAddressActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/profile/address/AddressSelectionView;

    invoke-virtual {v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->g()Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public a(Lco/uk/getmondo/api/model/Address;)V
    .locals 2

    .prologue
    .line 87
    sget-object v1, Lco/uk/getmondo/profile/address/EnterAddressActivity;->c:Lco/uk/getmondo/profile/address/EnterAddressActivity$a;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0, p1}, Lco/uk/getmondo/profile/address/EnterAddressActivity$a;->a(Landroid/content/Context;Lco/uk/getmondo/api/model/Address;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/SelectAddressActivity;->startActivity(Landroid/content/Intent;)V

    .line 88
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lco/uk/getmondo/api/model/Address;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "addresses"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    sget v0, Lco/uk/getmondo/c$a;->addressSelectionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/SelectAddressActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/profile/address/AddressSelectionView;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/profile/address/AddressSelectionView;->a(Ljava/util/List;)V

    .line 52
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 42
    sget v0, Lco/uk/getmondo/c$a;->addressSelectionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/SelectAddressActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/profile/address/AddressSelectionView;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/profile/address/AddressSelectionView;->setPrimaryButtonEnabled(Z)V

    .line 43
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    sget v0, Lco/uk/getmondo/c$a;->addressSelectionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/SelectAddressActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/profile/address/AddressSelectionView;

    invoke-virtual {v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->h()Lio/reactivex/n;

    move-result-object v1

    .line 34
    new-instance v0, Lco/uk/getmondo/profile/address/SelectAddressActivity$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/profile/address/SelectAddressActivity$b;-><init>(Lco/uk/getmondo/profile/address/SelectAddressActivity;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "addressSelectionView.pri\u2026sSelectionView.postcode }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/api/model/Address;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    sget v0, Lco/uk/getmondo/c$a;->addressSelectionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/SelectAddressActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/profile/address/AddressSelectionView;

    invoke-virtual {v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->k()Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public d()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    sget v0, Lco/uk/getmondo/c$a;->addressSelectionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/SelectAddressActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/profile/address/AddressSelectionView;

    invoke-virtual {v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->j()Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public e()V
    .locals 3

    .prologue
    .line 46
    sget-object v0, Lco/uk/getmondo/profile/address/n;->c:Lco/uk/getmondo/profile/address/n$a;

    invoke-virtual {v0}, Lco/uk/getmondo/profile/address/n$a;->a()Lco/uk/getmondo/profile/address/n;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/profile/address/SelectAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    const-class v2, Lco/uk/getmondo/profile/address/n;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/profile/address/n;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 47
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 55
    sget v0, Lco/uk/getmondo/c$a;->addressSelectionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/SelectAddressActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/profile/address/AddressSelectionView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/profile/address/AddressSelectionView;->setAddressLoading(Z)V

    .line 56
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 59
    sget v0, Lco/uk/getmondo/c$a;->addressSelectionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/SelectAddressActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/profile/address/AddressSelectionView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lco/uk/getmondo/profile/address/AddressSelectionView;->setAddressLoading(Z)V

    .line 60
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 67
    sget v0, Lco/uk/getmondo/c$a;->addressSelectionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/SelectAddressActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/profile/address/AddressSelectionView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lco/uk/getmondo/profile/address/AddressSelectionView;->setPrimaryButtonVisible(Z)V

    .line 68
    return-void
.end method

.method public i()V
    .locals 1

    .prologue
    .line 71
    sget v0, Lco/uk/getmondo/c$a;->addressSelectionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/SelectAddressActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/profile/address/AddressSelectionView;

    invoke-virtual {v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->c()V

    .line 72
    return-void
.end method

.method public j()V
    .locals 1

    .prologue
    .line 79
    sget v0, Lco/uk/getmondo/c$a;->addressSelectionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/SelectAddressActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/profile/address/AddressSelectionView;

    invoke-virtual {v0}, Lco/uk/getmondo/profile/address/AddressSelectionView;->l()V

    .line 80
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 19
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 20
    const v0, 0x7f05005b

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/SelectAddressActivity;->setContentView(I)V

    .line 22
    invoke-virtual {p0}, Lco/uk/getmondo/profile/address/SelectAddressActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/profile/address/SelectAddressActivity;)V

    .line 24
    sget v0, Lco/uk/getmondo/c$a;->selectAddressToolbar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/SelectAddressActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lco/uk/getmondo/profile/address/SelectAddressActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 25
    invoke-virtual {p0}, Lco/uk/getmondo/profile/address/SelectAddressActivity;->getSupportActionBar()Landroid/support/v7/app/a;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->b(Z)V

    .line 27
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/profile/address/SelectAddressActivity;->a:Lco/uk/getmondo/profile/address/k;

    if-nez v0, :cond_1

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_1
    check-cast p0, Lco/uk/getmondo/profile/address/k$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/profile/address/k;->a(Lco/uk/getmondo/profile/address/k$a;)V

    .line 28
    return-void
.end method
