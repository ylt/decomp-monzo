.class public final Lco/uk/getmondo/profile/address/g;
.super Lco/uk/getmondo/common/ui/b;
.source "EnterAddressPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/profile/address/g$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/profile/address/g$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u000bB\u0019\u0008\u0007\u0012\u0008\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lco/uk/getmondo/profile/address/EnterAddressPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/profile/address/EnterAddressPresenter$View;",
        "address",
        "Lco/uk/getmondo/api/model/Address;",
        "analytics",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "(Lco/uk/getmondo/api/model/Address;Lco/uk/getmondo/common/AnalyticsService;)V",
        "register",
        "",
        "view",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Lco/uk/getmondo/api/model/Address;

.field private final d:Lco/uk/getmondo/common/a;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/api/model/Address;Lco/uk/getmondo/common/a;)V
    .locals 1

    .prologue
    const-string v0, "analytics"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/profile/address/g;->c:Lco/uk/getmondo/api/model/Address;

    iput-object p2, p0, Lco/uk/getmondo/profile/address/g;->d:Lco/uk/getmondo/common/a;

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 16
    check-cast p1, Lco/uk/getmondo/profile/address/g$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/profile/address/g;->a(Lco/uk/getmondo/profile/address/g$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/profile/address/g$a;)V
    .locals 5

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 23
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 25
    iget-object v0, p0, Lco/uk/getmondo/profile/address/g;->d:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->aP()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 27
    iget-object v0, p0, Lco/uk/getmondo/profile/address/g;->c:Lco/uk/getmondo/api/model/Address;

    invoke-interface {p1, v0}, Lco/uk/getmondo/profile/address/g$a;->a(Lco/uk/getmondo/api/model/Address;)V

    .line 29
    iget-object v3, p0, Lco/uk/getmondo/profile/address/g;->b:Lio/reactivex/b/a;

    .line 30
    invoke-interface {p1}, Lco/uk/getmondo/profile/address/g$a;->a()Lio/reactivex/n;

    move-result-object v4

    new-instance v0, Lco/uk/getmondo/profile/address/g$b;

    invoke-direct {v0, p1}, Lco/uk/getmondo/profile/address/g$b;-><init>(Lco/uk/getmondo/profile/address/g$a;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/profile/address/g$c;->a:Lco/uk/getmondo/profile/address/g$c;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lco/uk/getmondo/profile/address/h;

    invoke-direct {v2, v1}, Lco/uk/getmondo/profile/address/h;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v4, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onAddressInputValid\u2026Enabled(it) }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/profile/address/g;->b:Lio/reactivex/b/a;

    .line 32
    iget-object v3, p0, Lco/uk/getmondo/profile/address/g;->b:Lio/reactivex/b/a;

    .line 33
    invoke-interface {p1}, Lco/uk/getmondo/profile/address/g$a;->b()Lio/reactivex/n;

    move-result-object v4

    new-instance v0, Lco/uk/getmondo/profile/address/g$d;

    invoke-direct {v0, p1}, Lco/uk/getmondo/profile/address/g$d;-><init>(Lco/uk/getmondo/profile/address/g$a;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/profile/address/g$e;->a:Lco/uk/getmondo/profile/address/g$e;

    check-cast v1, Lkotlin/d/a/b;

    if-eqz v1, :cond_1

    new-instance v2, Lco/uk/getmondo/profile/address/h;

    invoke-direct {v2, v1}, Lco/uk/getmondo/profile/address/h;-><init>(Lkotlin/d/a/b;)V

    move-object v1, v2

    :cond_1
    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v4, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onConfirmAddress()\n\u2026inEntry(it) }, Timber::e)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-static {v3, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/profile/address/g;->b:Lio/reactivex/b/a;

    .line 34
    return-void
.end method
