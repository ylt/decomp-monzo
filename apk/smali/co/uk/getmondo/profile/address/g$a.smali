.class public interface abstract Lco/uk/getmondo/profile/address/g$a;
.super Ljava/lang/Object;
.source "EnterAddressPresenter.kt"

# interfaces
.implements Lco/uk/getmondo/common/ui/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/profile/address/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001J\u000e\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H&J\u000e\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0003H&J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u0006H&J\u0012\u0010\n\u001a\u00020\u00082\u0008\u0010\t\u001a\u0004\u0018\u00010\u000bH&J\u0010\u0010\u000c\u001a\u00020\u00082\u0006\u0010\r\u001a\u00020\u0004H&\u00a8\u0006\u000e"
    }
    d2 = {
        "Lco/uk/getmondo/profile/address/EnterAddressPresenter$View;",
        "Lco/uk/getmondo/common/ui/MvpView;",
        "onAddressInputValid",
        "Lio/reactivex/Observable;",
        "",
        "onConfirmAddress",
        "Lco/uk/getmondo/model/LegacyAddress;",
        "openPinEntry",
        "",
        "address",
        "setAddress",
        "Lco/uk/getmondo/api/model/Address;",
        "setConfirmEnabled",
        "enabled",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# virtual methods
.method public abstract a()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Lco/uk/getmondo/api/model/Address;)V
.end method

.method public abstract a(Lco/uk/getmondo/d/s;)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract b()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/d/s;",
            ">;"
        }
    .end annotation
.end method
