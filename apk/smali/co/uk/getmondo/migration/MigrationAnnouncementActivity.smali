.class public final Lco/uk/getmondo/migration/MigrationAnnouncementActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "MigrationAnnouncementActivity.kt"

# interfaces
.implements Lco/uk/getmondo/migration/b$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/migration/MigrationAnnouncementActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0018\u0000  2\u00020\u00012\u00020\u0002:\u0001 B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0012\u0010\u0017\u001a\u00020\u000c2\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0014J\u0008\u0010\u001a\u001a\u00020\u000cH\u0014J\u0010\u0010\u001b\u001a\u00020\u000c2\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u0008\u0010\u001c\u001a\u00020\u000cH\u0016J\u0008\u0010\u001d\u001a\u00020\u000cH\u0016J\u0008\u0010\u001e\u001a\u00020\u000cH\u0016J\u0008\u0010\u001f\u001a\u00020\u000cH\u0016R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\"\u0004\u0008\u0008\u0010\tR\u001a\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000eR\u001a\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u000eR\u001b\u0010\u0011\u001a\u00020\u00128VX\u0096\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0015\u0010\u0016\u001a\u0004\u0008\u0013\u0010\u0014\u00a8\u0006!"
    }
    d2 = {
        "Lco/uk/getmondo/migration/MigrationAnnouncementActivity;",
        "Lco/uk/getmondo/common/activities/BaseActivity;",
        "Lco/uk/getmondo/migration/MigrationAnnouncementPresenter$View;",
        "()V",
        "presenter",
        "Lco/uk/getmondo/migration/MigrationAnnouncementPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/migration/MigrationAnnouncementPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/migration/MigrationAnnouncementPresenter;)V",
        "primaryButtonClicked",
        "Lio/reactivex/Observable;",
        "",
        "getPrimaryButtonClicked",
        "()Lio/reactivex/Observable;",
        "secondaryButtonClicked",
        "getSecondaryButtonClicked",
        "signupAllowed",
        "",
        "getSignupAllowed",
        "()Z",
        "signupAllowed$delegate",
        "Lkotlin/Lazy;",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onDestroy",
        "openLearnMore",
        "openMigrationTour",
        "showLearnMorePrimaryButton",
        "showLearnMoreSecondaryButton",
        "showStartSignupButton",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;

.field public static final c:Lco/uk/getmondo/migration/MigrationAnnouncementActivity$a;


# instance fields
.field public b:Lco/uk/getmondo/migration/b;

.field private final e:Lkotlin/c;

.field private f:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "signupAllowed"

    const-string v5, "getSignupAllowed()Z"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;->a:[Lkotlin/reflect/l;

    new-instance v0, Lco/uk/getmondo/migration/MigrationAnnouncementActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/migration/MigrationAnnouncementActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;->c:Lco/uk/getmondo/migration/MigrationAnnouncementActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 20
    new-instance v0, Lco/uk/getmondo/migration/MigrationAnnouncementActivity$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/migration/MigrationAnnouncementActivity$b;-><init>(Lco/uk/getmondo/migration/MigrationAnnouncementActivity;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;->e:Lkotlin/c;

    return-void
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;->f:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;->f:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;->f:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;->f:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public a(Z)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 62
    if-eqz p1, :cond_0

    .line 63
    const-string v1, "https://monzo.com/-webviews/account/upgrade"

    .line 62
    :goto_0
    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move v3, v2

    invoke-static/range {v0 .. v5}, Lco/uk/getmondo/common/activities/a;->a(Landroid/app/Activity;Ljava/lang/String;IZILjava/lang/Object;)V

    .line 67
    return-void

    .line 65
    :cond_0
    const-string v1, "https://monzo.com/-webviews/announcements/current-account-migration"

    goto :goto_0
.end method

.method public a()Z
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;->e:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    sget v0, Lco/uk/getmondo/c$a;->migrationPrimaryButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 82
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    return-object v0
.end method

.method public c()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    sget v0, Lco/uk/getmondo/c$a;->migrationSecondaryButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 83
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    return-object v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 45
    sget v0, Lco/uk/getmondo/c$a;->migrationPrimaryButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f0a0274

    invoke-virtual {p0, v1}, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 46
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 49
    sget v0, Lco/uk/getmondo/c$a;->migrationPrimaryButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f0a0273

    invoke-virtual {p0, v1}, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 50
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 53
    sget v0, Lco/uk/getmondo/c$a;->migrationSecondaryButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 54
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 57
    sget-object v1, Lco/uk/getmondo/migration/MigrationTourActivity;->b:Lco/uk/getmondo/migration/MigrationTourActivity$a;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/migration/MigrationTourActivity$a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;->startActivity(Landroid/content/Intent;)V

    .line 58
    invoke-virtual {p0}, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;->finish()V

    .line 59
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 23
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 24
    const v0, 0x7f05004a

    invoke-virtual {p0, v0}, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;->setContentView(I)V

    .line 25
    sget v0, Lco/uk/getmondo/c$a;->migrationTitle:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "KEY_TITLE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 26
    sget v0, Lco/uk/getmondo/c$a;->migrationBody:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "KEY_BODY"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 28
    invoke-virtual {p0}, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;->getSupportActionBar()Landroid/support/v7/app/a;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->c(Z)V

    .line 30
    :cond_0
    invoke-virtual {p0}, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/migration/MigrationAnnouncementActivity;)V

    .line 31
    iget-object v0, p0, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;->b:Lco/uk/getmondo/migration/b;

    if-nez v0, :cond_1

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_1
    check-cast p0, Lco/uk/getmondo/migration/b$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/migration/b;->a(Lco/uk/getmondo/migration/b$a;)V

    .line 32
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Lco/uk/getmondo/migration/MigrationAnnouncementActivity;->b:Lco/uk/getmondo/migration/b;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/migration/b;->b()V

    .line 36
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 37
    return-void
.end method
