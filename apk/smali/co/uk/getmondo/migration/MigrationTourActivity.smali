.class public final Lco/uk/getmondo/migration/MigrationTourActivity;
.super Lco/uk/getmondo/signup/a;
.source "MigrationTourActivity.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/migration/MigrationTourActivity$b;,
        Lco/uk/getmondo/migration/MigrationTourActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 \u00162\u00020\u0001:\u0002\u0016\u0017B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u0012\u0010\u0012\u001a\u00020\u000f2\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0014J\u0008\u0010\u0015\u001a\u00020\u000fH\u0014R\u001c\u0010\u0003\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u00048BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0007R\u001e\u0010\u0008\u001a\u00020\t8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\n\u0010\u000b\"\u0004\u0008\u000c\u0010\r\u00a8\u0006\u0018"
    }
    d2 = {
        "Lco/uk/getmondo/migration/MigrationTourActivity;",
        "Lco/uk/getmondo/signup/BaseSignupActivity;",
        "()V",
        "infoItemBodies",
        "",
        "Landroid/view/View;",
        "getInfoItemBodies",
        "()Ljava/util/List;",
        "pageViewTracker",
        "Lco/uk/getmondo/common/pager/PageViewTracker;",
        "getPageViewTracker",
        "()Lco/uk/getmondo/common/pager/PageViewTracker;",
        "setPageViewTracker",
        "(Lco/uk/getmondo/common/pager/PageViewTracker;)V",
        "makeInfoItemVisible",
        "",
        "index",
        "",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onDestroy",
        "Companion",
        "MigrationInfoPage",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final b:Lco/uk/getmondo/migration/MigrationTourActivity$a;


# instance fields
.field public a:Lco/uk/getmondo/common/pager/h;

.field private g:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/migration/MigrationTourActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/migration/MigrationTourActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/migration/MigrationTourActivity;->b:Lco/uk/getmondo/migration/MigrationTourActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lco/uk/getmondo/signup/a;-><init>()V

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/migration/MigrationTourActivity;)Ljava/util/List;
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lco/uk/getmondo/migration/MigrationTourActivity;->c()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/migration/MigrationTourActivity;I)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lco/uk/getmondo/migration/MigrationTourActivity;->d(I)V

    return-void
.end method

.method private final c()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    const/4 v0, 0x3

    new-array v1, v0, [Landroid/widget/TextView;

    const/4 v2, 0x0

    sget v0, Lco/uk/getmondo/c$a;->migrationItem1Body:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/migration/MigrationTourActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v2

    const/4 v2, 0x1

    sget v0, Lco/uk/getmondo/c$a;->migrationItem2Body:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/migration/MigrationTourActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v2

    const/4 v2, 0x2

    sget v0, Lco/uk/getmondo/c$a;->migrationItem3Body:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/migration/MigrationTourActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v2

    invoke-static {v1}, Lkotlin/a/m;->b([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private final d(I)V
    .locals 2

    .prologue
    .line 70
    invoke-direct {p0}, Lco/uk/getmondo/migration/MigrationTourActivity;->c()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/a/m;->a(Ljava/util/List;)I

    move-result v0

    if-gt p1, v0, :cond_0

    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    const-string v1, "Check failed."

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 72
    :cond_1
    invoke-direct {p0}, Lco/uk/getmondo/migration/MigrationTourActivity;->c()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 103
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 72
    if-eqz v0, :cond_2

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    :cond_2
    nop

    goto :goto_1

    .line 104
    :cond_3
    nop

    .line 73
    invoke-direct {p0}, Lco/uk/getmondo/migration/MigrationTourActivity;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_4

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 74
    :cond_4
    return-void
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/migration/MigrationTourActivity;->g:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/migration/MigrationTourActivity;->g:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/migration/MigrationTourActivity;->g:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/migration/MigrationTourActivity;->g:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 31
    invoke-super {p0, p1}, Lco/uk/getmondo/signup/a;->onCreate(Landroid/os/Bundle;)V

    .line 32
    const v0, 0x7f05004b

    invoke-virtual {p0, v0}, Lco/uk/getmondo/migration/MigrationTourActivity;->setContentView(I)V

    .line 33
    invoke-virtual {p0}, Lco/uk/getmondo/migration/MigrationTourActivity;->getSupportActionBar()Landroid/support/v7/app/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v5}, Landroid/support/v7/app/a;->b(Z)V

    .line 34
    :cond_0
    invoke-virtual {p0}, Lco/uk/getmondo/migration/MigrationTourActivity;->getSupportActionBar()Landroid/support/v7/app/a;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, v5}, Landroid/support/v7/app/a;->d(Z)V

    .line 36
    :cond_1
    invoke-virtual {p0}, Lco/uk/getmondo/migration/MigrationTourActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/migration/MigrationTourActivity;)V

    .line 38
    sget v0, Lco/uk/getmondo/c$a;->migrationTourViewPager:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/migration/MigrationTourActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    new-instance v2, Lco/uk/getmondo/common/pager/GenericPagerAdapter;

    const/4 v1, 0x2

    new-array v3, v1, [Lco/uk/getmondo/common/pager/f;

    .line 39
    new-instance v1, Lco/uk/getmondo/migration/MigrationTourActivity$b;

    sget-object v4, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v4}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->be()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v4

    invoke-direct {v1, p0, v4}, Lco/uk/getmondo/migration/MigrationTourActivity$b;-><init>(Lco/uk/getmondo/migration/MigrationTourActivity;Lco/uk/getmondo/api/model/tracking/Impression;)V

    check-cast v1, Lco/uk/getmondo/common/pager/f;

    aput-object v1, v3, v5

    .line 40
    const/4 v4, 0x1

    new-instance v1, Lco/uk/getmondo/common/pager/b;

    const v5, 0x7f050129

    sget-object v6, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v6}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->bf()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v6

    invoke-direct {v1, v5, v6}, Lco/uk/getmondo/common/pager/b;-><init>(ILco/uk/getmondo/api/model/tracking/Impression;)V

    check-cast v1, Lco/uk/getmondo/common/pager/f;

    aput-object v1, v3, v4

    .line 38
    invoke-direct {v2, v3}, Lco/uk/getmondo/common/pager/GenericPagerAdapter;-><init>([Lco/uk/getmondo/common/pager/f;)V

    move-object v1, v2

    check-cast v1, Landroid/support/v4/view/p;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/p;)V

    .line 42
    iget-object v1, p0, Lco/uk/getmondo/migration/MigrationTourActivity;->a:Lco/uk/getmondo/common/pager/h;

    if-nez v1, :cond_2

    const-string v0, "pageViewTracker"

    invoke-static {v0}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_2
    sget v0, Lco/uk/getmondo/c$a;->migrationTourViewPager:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/migration/MigrationTourActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/pager/h;->a(Landroid/support/v4/view/ViewPager;)V

    .line 44
    sget v0, Lco/uk/getmondo/c$a;->migrationTourButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/migration/MigrationTourActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lco/uk/getmondo/migration/MigrationTourActivity$c;

    invoke-direct {v1, p0}, Lco/uk/getmondo/migration/MigrationTourActivity$c;-><init>(Lco/uk/getmondo/migration/MigrationTourActivity;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lco/uk/getmondo/migration/MigrationTourActivity;->a:Lco/uk/getmondo/common/pager/h;

    if-nez v0, :cond_0

    const-string v1, "pageViewTracker"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/common/pager/h;->a()V

    .line 66
    invoke-super {p0}, Lco/uk/getmondo/signup/a;->onDestroy()V

    .line 67
    return-void
.end method
