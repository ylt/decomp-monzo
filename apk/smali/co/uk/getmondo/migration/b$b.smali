.class final Lco/uk/getmondo/migration/b$b;
.super Ljava/lang/Object;
.source "MigrationAnnouncementPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/migration/b;->a(Lco/uk/getmondo/migration/b$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lkotlin/n;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "kotlin.jvm.PlatformType",
        "accept",
        "(Lkotlin/Unit;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/migration/b;

.field final synthetic b:Lco/uk/getmondo/migration/b$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/migration/b;Lco/uk/getmondo/migration/b$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/migration/b$b;->a:Lco/uk/getmondo/migration/b;

    iput-object p2, p0, Lco/uk/getmondo/migration/b$b;->b:Lco/uk/getmondo/migration/b$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 12
    check-cast p1, Lkotlin/n;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/migration/b$b;->a(Lkotlin/n;)V

    return-void
.end method

.method public final a(Lkotlin/n;)V
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lco/uk/getmondo/migration/b$b;->b:Lco/uk/getmondo/migration/b$a;

    invoke-interface {v0}, Lco/uk/getmondo/migration/b$a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lco/uk/getmondo/migration/b$b;->a:Lco/uk/getmondo/migration/b;

    invoke-static {v0}, Lco/uk/getmondo/migration/b;->a(Lco/uk/getmondo/migration/b;)Lco/uk/getmondo/common/a;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->bd()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 36
    iget-object v0, p0, Lco/uk/getmondo/migration/b$b;->b:Lco/uk/getmondo/migration/b$a;

    invoke-interface {v0}, Lco/uk/getmondo/migration/b$a;->g()V

    .line 40
    :goto_0
    return-void

    .line 38
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/migration/b$b;->b:Lco/uk/getmondo/migration/b$a;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lco/uk/getmondo/migration/b$a;->a(Z)V

    .line 39
    iget-object v0, p0, Lco/uk/getmondo/migration/b$b;->a:Lco/uk/getmondo/migration/b;

    invoke-static {v0}, Lco/uk/getmondo/migration/b;->a(Lco/uk/getmondo/migration/b;)Lco/uk/getmondo/common/a;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->aZ()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    goto :goto_0
.end method
