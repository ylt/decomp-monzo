.class public final Lco/uk/getmondo/migration/b;
.super Lco/uk/getmondo/common/ui/b;
.source "MigrationAnnouncementPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/migration/b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/migration/b$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u000bB\u0017\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lco/uk/getmondo/migration/MigrationAnnouncementPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/migration/MigrationAnnouncementPresenter$View;",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "migrationStorage",
        "Lco/uk/getmondo/migration/MigrationStorage;",
        "(Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/migration/MigrationStorage;)V",
        "register",
        "",
        "view",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Lco/uk/getmondo/common/a;

.field private final d:Lco/uk/getmondo/migration/d;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/common/a;Lco/uk/getmondo/migration/d;)V
    .locals 1

    .prologue
    const-string v0, "analyticsService"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "migrationStorage"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/migration/b;->c:Lco/uk/getmondo/common/a;

    iput-object p2, p0, Lco/uk/getmondo/migration/b;->d:Lco/uk/getmondo/migration/d;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/migration/b;)Lco/uk/getmondo/common/a;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lco/uk/getmondo/migration/b;->c:Lco/uk/getmondo/common/a;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 12
    check-cast p1, Lco/uk/getmondo/migration/b$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/migration/b;->a(Lco/uk/getmondo/migration/b$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/migration/b$a;)V
    .locals 3

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 20
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 22
    invoke-interface {p1}, Lco/uk/getmondo/migration/b$a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 23
    invoke-interface {p1}, Lco/uk/getmondo/migration/b$a;->d()V

    .line 24
    invoke-interface {p1}, Lco/uk/getmondo/migration/b$a;->f()V

    .line 25
    iget-object v0, p0, Lco/uk/getmondo/migration/b;->c:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->bb()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 30
    :goto_0
    iget-object v1, p0, Lco/uk/getmondo/migration/b;->b:Lio/reactivex/b/a;

    .line 33
    invoke-interface {p1}, Lco/uk/getmondo/migration/b$a;->b()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/migration/b$b;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/migration/b$b;-><init>(Lco/uk/getmondo/migration/b;Lco/uk/getmondo/migration/b$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.primaryButtonClicke\u2026      }\n                }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/migration/b;->b:Lio/reactivex/b/a;

    .line 43
    iget-object v1, p0, Lco/uk/getmondo/migration/b;->b:Lio/reactivex/b/a;

    .line 44
    invoke-interface {p1}, Lco/uk/getmondo/migration/b$a;->c()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/migration/b$c;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/migration/b$c;-><init>(Lco/uk/getmondo/migration/b;Lco/uk/getmondo/migration/b$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.secondaryButtonClic\u2026eTap())\n                }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/migration/b;->b:Lio/reactivex/b/a;

    .line 49
    return-void

    .line 27
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/migration/b;->d:Lco/uk/getmondo/migration/d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/migration/d;->a(Z)V

    .line 28
    invoke-interface {p1}, Lco/uk/getmondo/migration/b$a;->e()V

    .line 29
    iget-object v0, p0, Lco/uk/getmondo/migration/b;->c:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->aY()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    goto :goto_0
.end method
