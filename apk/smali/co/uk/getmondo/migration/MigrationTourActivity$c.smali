.class final Lco/uk/getmondo/migration/MigrationTourActivity$c;
.super Ljava/lang/Object;
.source "MigrationTourActivity.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/migration/MigrationTourActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "onClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/migration/MigrationTourActivity;


# direct methods
.method constructor <init>(Lco/uk/getmondo/migration/MigrationTourActivity;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/migration/MigrationTourActivity$c;->a:Lco/uk/getmondo/migration/MigrationTourActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 45
    iget-object v0, p0, Lco/uk/getmondo/migration/MigrationTourActivity$c;->a:Lco/uk/getmondo/migration/MigrationTourActivity;

    sget v1, Lco/uk/getmondo/c$a;->migrationTourViewPager:I

    invoke-virtual {v0, v1}, Lco/uk/getmondo/migration/MigrationTourActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    if-ne v0, v6, :cond_1

    .line 46
    iget-object v1, p0, Lco/uk/getmondo/migration/MigrationTourActivity$c;->a:Lco/uk/getmondo/migration/MigrationTourActivity;

    sget-object v2, Lco/uk/getmondo/signup/status/SignupStatusActivity;->c:Lco/uk/getmondo/signup/status/SignupStatusActivity$a;

    iget-object v0, p0, Lco/uk/getmondo/migration/MigrationTourActivity$c;->a:Lco/uk/getmondo/migration/MigrationTourActivity;

    check-cast v0, Landroid/content/Context;

    sget-object v3, Lco/uk/getmondo/signup/j;->a:Lco/uk/getmondo/signup/j;

    invoke-virtual {v2, v0, v3}, Lco/uk/getmondo/signup/status/SignupStatusActivity$a;->a(Landroid/content/Context;Lco/uk/getmondo/signup/j;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Lco/uk/getmondo/migration/MigrationTourActivity;->startActivity(Landroid/content/Intent;)V

    .line 47
    iget-object v0, p0, Lco/uk/getmondo/migration/MigrationTourActivity$c;->a:Lco/uk/getmondo/migration/MigrationTourActivity;

    invoke-virtual {v0}, Lco/uk/getmondo/migration/MigrationTourActivity;->finish()V

    .line 61
    :cond_0
    :goto_0
    return-void

    .line 51
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/migration/MigrationTourActivity$c;->a:Lco/uk/getmondo/migration/MigrationTourActivity;

    invoke-static {v0}, Lco/uk/getmondo/migration/MigrationTourActivity;->a(Lco/uk/getmondo/migration/MigrationTourActivity;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/a/m;->i(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    move-object v0, v1

    .line 52
    check-cast v0, Ljava/lang/Iterable;

    .line 103
    const/4 v2, 0x0

    .line 104
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    add-int/lit8 v3, v2, 0x1

    check-cast v0, Landroid/view/View;

    .line 53
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v5

    if-nez v5, :cond_2

    if-eqz v2, :cond_2

    .line 54
    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 55
    add-int/lit8 v0, v2, -0x1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    goto :goto_0

    .line 58
    :cond_2
    nop

    move v2, v3

    goto :goto_1

    .line 105
    :cond_3
    nop

    .line 60
    iget-object v0, p0, Lco/uk/getmondo/migration/MigrationTourActivity$c;->a:Lco/uk/getmondo/migration/MigrationTourActivity;

    sget v1, Lco/uk/getmondo/c$a;->migrationTourViewPager:I

    invoke-virtual {v0, v1}, Lco/uk/getmondo/migration/MigrationTourActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v6, v6}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    goto :goto_0
.end method
