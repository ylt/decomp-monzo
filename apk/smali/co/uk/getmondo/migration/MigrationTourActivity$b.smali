.class public final Lco/uk/getmondo/migration/MigrationTourActivity$b;
.super Lco/uk/getmondo/common/pager/c;
.source "MigrationTourActivity.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/migration/MigrationTourActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "b"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0004\u0018\u00002\u00020\u0001B\u0011\u0012\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "Lco/uk/getmondo/migration/MigrationTourActivity$MigrationInfoPage;",
        "Lco/uk/getmondo/common/pager/CustomViewPage;",
        "impression",
        "Lco/uk/getmondo/api/model/tracking/Impression;",
        "(Lco/uk/getmondo/migration/MigrationTourActivity;Lco/uk/getmondo/api/model/tracking/Impression;)V",
        "createView",
        "Landroid/view/View;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/migration/MigrationTourActivity;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/migration/MigrationTourActivity;Lco/uk/getmondo/api/model/tracking/Impression;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/model/tracking/Impression;",
            ")V"
        }
    .end annotation

    .prologue
    .line 78
    iput-object p1, p0, Lco/uk/getmondo/migration/MigrationTourActivity$b;->a:Lco/uk/getmondo/migration/MigrationTourActivity;

    invoke-direct {p0, p2}, Lco/uk/getmondo/common/pager/c;-><init>(Lco/uk/getmondo/api/model/tracking/Impression;)V

    return-void
.end method


# virtual methods
.method public c()Landroid/view/View;
    .locals 4

    .prologue
    .line 80
    iget-object v0, p0, Lco/uk/getmondo/migration/MigrationTourActivity$b;->a:Lco/uk/getmondo/migration/MigrationTourActivity;

    invoke-virtual {v0}, Lco/uk/getmondo/migration/MigrationTourActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f050128

    iget-object v0, p0, Lco/uk/getmondo/migration/MigrationTourActivity$b;->a:Lco/uk/getmondo/migration/MigrationTourActivity;

    sget v3, Lco/uk/getmondo/c$a;->migrationTourViewPager:I

    invoke-virtual {v0, v3}, Lco/uk/getmondo/migration/MigrationTourActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 82
    const v0, 0x7f110466

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/migration/MigrationTourActivity$b$a;

    invoke-direct {v0, p0}, Lco/uk/getmondo/migration/MigrationTourActivity$b$a;-><init>(Lco/uk/getmondo/migration/MigrationTourActivity$b;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    const v0, 0x7f110468

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/migration/MigrationTourActivity$b$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/migration/MigrationTourActivity$b$b;-><init>(Lco/uk/getmondo/migration/MigrationTourActivity$b;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    const v0, 0x7f11046b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/migration/MigrationTourActivity$b$c;

    invoke-direct {v0, p0}, Lco/uk/getmondo/migration/MigrationTourActivity$b$c;-><init>(Lco/uk/getmondo/migration/MigrationTourActivity$b;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    const-string v0, "view"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v1
.end method
