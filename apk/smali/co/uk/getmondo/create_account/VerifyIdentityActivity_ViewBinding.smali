.class public Lco/uk/getmondo/create_account/VerifyIdentityActivity_ViewBinding;
.super Ljava/lang/Object;
.source "VerifyIdentityActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/create_account/VerifyIdentityActivity;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/create_account/VerifyIdentityActivity;Landroid/view/View;)V
    .locals 6

    .prologue
    const v5, 0x7f11026d

    const v4, 0x7f11026c

    const v3, 0x7f110142

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity_ViewBinding;->a:Lco/uk/getmondo/create_account/VerifyIdentityActivity;

    .line 37
    const-string v0, "field \'nameInput\' and method \'onNameFocusChange\'"

    invoke-static {p2, v3, v0}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 38
    const-string v0, "field \'nameInput\'"

    const-class v2, Landroid/widget/EditText;

    invoke-static {v1, v3, v0, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p1, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->nameInput:Landroid/widget/EditText;

    .line 39
    iput-object v1, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity_ViewBinding;->b:Landroid/view/View;

    .line 40
    new-instance v0, Lco/uk/getmondo/create_account/VerifyIdentityActivity_ViewBinding$1;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/create_account/VerifyIdentityActivity_ViewBinding$1;-><init>(Lco/uk/getmondo/create_account/VerifyIdentityActivity_ViewBinding;Lco/uk/getmondo/create_account/VerifyIdentityActivity;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 46
    const v0, 0x7f110141

    const-string v1, "field \'nameWrapper\'"

    const-class v2, Landroid/support/design/widget/TextInputLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    iput-object v0, p1, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->nameWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 47
    const-string v0, "field \'dobInput\', method \'onDateFocusChange\', and method \'onDateOfBirthFocusChange\'"

    invoke-static {p2, v4, v0}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 48
    const-string v0, "field \'dobInput\'"

    const-class v2, Landroid/widget/EditText;

    invoke-static {v1, v4, v0, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p1, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->dobInput:Landroid/widget/EditText;

    .line 49
    iput-object v1, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity_ViewBinding;->c:Landroid/view/View;

    .line 50
    new-instance v0, Lco/uk/getmondo/create_account/VerifyIdentityActivity_ViewBinding$2;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/create_account/VerifyIdentityActivity_ViewBinding$2;-><init>(Lco/uk/getmondo/create_account/VerifyIdentityActivity_ViewBinding;Lco/uk/getmondo/create_account/VerifyIdentityActivity;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 57
    const v0, 0x7f11026b

    const-string v1, "field \'dobWrapper\'"

    const-class v2, Landroid/support/design/widget/TextInputLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    iput-object v0, p1, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->dobWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 58
    const-string v0, "field \'addressInput\' and method \'onAddressFocusChange\'"

    invoke-static {p2, v5, v0}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 59
    const-string v0, "field \'addressInput\'"

    const-class v2, Landroid/widget/EditText;

    invoke-static {v1, v5, v0, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p1, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->addressInput:Landroid/widget/EditText;

    .line 60
    iput-object v1, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity_ViewBinding;->d:Landroid/view/View;

    .line 61
    new-instance v0, Lco/uk/getmondo/create_account/VerifyIdentityActivity_ViewBinding$3;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/create_account/VerifyIdentityActivity_ViewBinding$3;-><init>(Lco/uk/getmondo/create_account/VerifyIdentityActivity_ViewBinding;Lco/uk/getmondo/create_account/VerifyIdentityActivity;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 67
    const v0, 0x7f11026e

    const-string v1, "method \'onDetailsCorrectClick\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 68
    iput-object v0, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity_ViewBinding;->e:Landroid/view/View;

    .line 69
    new-instance v1, Lco/uk/getmondo/create_account/VerifyIdentityActivity_ViewBinding$4;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/create_account/VerifyIdentityActivity_ViewBinding$4;-><init>(Lco/uk/getmondo/create_account/VerifyIdentityActivity_ViewBinding;Lco/uk/getmondo/create_account/VerifyIdentityActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 80
    iget-object v0, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity_ViewBinding;->a:Lco/uk/getmondo/create_account/VerifyIdentityActivity;

    .line 81
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity_ViewBinding;->a:Lco/uk/getmondo/create_account/VerifyIdentityActivity;

    .line 84
    iput-object v1, v0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->nameInput:Landroid/widget/EditText;

    .line 85
    iput-object v1, v0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->nameWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 86
    iput-object v1, v0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->dobInput:Landroid/widget/EditText;

    .line 87
    iput-object v1, v0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->dobWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 88
    iput-object v1, v0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->addressInput:Landroid/widget/EditText;

    .line 90
    iget-object v0, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity_ViewBinding;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 91
    iput-object v1, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity_ViewBinding;->b:Landroid/view/View;

    .line 92
    iget-object v0, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity_ViewBinding;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 93
    iput-object v1, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity_ViewBinding;->c:Landroid/view/View;

    .line 94
    iget-object v0, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity_ViewBinding;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 95
    iput-object v1, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity_ViewBinding;->d:Landroid/view/View;

    .line 96
    iget-object v0, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity_ViewBinding;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    iput-object v1, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity_ViewBinding;->e:Landroid/view/View;

    .line 98
    return-void
.end method
