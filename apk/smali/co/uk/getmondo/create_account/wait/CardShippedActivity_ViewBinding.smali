.class public Lco/uk/getmondo/create_account/wait/CardShippedActivity_ViewBinding;
.super Ljava/lang/Object;
.source "CardShippedActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/create_account/wait/CardShippedActivity;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/create_account/wait/CardShippedActivity;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity_ViewBinding;->a:Lco/uk/getmondo/create_account/wait/CardShippedActivity;

    .line 34
    const v0, 0x7f11012a

    const-string v1, "field \'descriptionView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->descriptionView:Landroid/widget/TextView;

    .line 35
    const v0, 0x7f11012f

    const-string v1, "field \'descriptionOnceArrivedView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->descriptionOnceArrivedView:Landroid/widget/TextView;

    .line 36
    const v0, 0x7f11012b

    const-string v1, "field \'animationView\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->animationView:Landroid/widget/ImageView;

    .line 37
    const v0, 0x7f11012c

    const-string v1, "field \'buttonsContainer\'"

    const-class v2, Landroid/view/ViewGroup;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p1, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->buttonsContainer:Landroid/view/ViewGroup;

    .line 38
    const v0, 0x7f11012d

    const-string v1, "method \'onCardArrivedClick\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 39
    iput-object v0, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity_ViewBinding;->b:Landroid/view/View;

    .line 40
    new-instance v1, Lco/uk/getmondo/create_account/wait/CardShippedActivity_ViewBinding$1;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/create_account/wait/CardShippedActivity_ViewBinding$1;-><init>(Lco/uk/getmondo/create_account/wait/CardShippedActivity_ViewBinding;Lco/uk/getmondo/create_account/wait/CardShippedActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    const v0, 0x7f11012e

    const-string v1, "method \'onCardDidntArriveClick\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 47
    iput-object v0, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity_ViewBinding;->c:Landroid/view/View;

    .line 48
    new-instance v1, Lco/uk/getmondo/create_account/wait/CardShippedActivity_ViewBinding$2;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/create_account/wait/CardShippedActivity_ViewBinding$2;-><init>(Lco/uk/getmondo/create_account/wait/CardShippedActivity_ViewBinding;Lco/uk/getmondo/create_account/wait/CardShippedActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 59
    iget-object v0, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity_ViewBinding;->a:Lco/uk/getmondo/create_account/wait/CardShippedActivity;

    .line 60
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity_ViewBinding;->a:Lco/uk/getmondo/create_account/wait/CardShippedActivity;

    .line 63
    iput-object v1, v0, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->descriptionView:Landroid/widget/TextView;

    .line 64
    iput-object v1, v0, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->descriptionOnceArrivedView:Landroid/widget/TextView;

    .line 65
    iput-object v1, v0, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->animationView:Landroid/widget/ImageView;

    .line 66
    iput-object v1, v0, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->buttonsContainer:Landroid/view/ViewGroup;

    .line 68
    iget-object v0, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity_ViewBinding;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    iput-object v1, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity_ViewBinding;->b:Landroid/view/View;

    .line 70
    iget-object v0, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity_ViewBinding;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    iput-object v1, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity_ViewBinding;->c:Landroid/view/View;

    .line 72
    return-void
.end method
