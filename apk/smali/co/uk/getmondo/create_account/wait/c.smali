.class public Lco/uk/getmondo/create_account/wait/c;
.super Lco/uk/getmondo/common/ui/b;
.source "CardShippedPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/create_account/wait/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/create_account/wait/c$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lco/uk/getmondo/common/q;

.field private final d:Lio/reactivex/u;

.field private final e:Lio/reactivex/u;

.field private final f:Lco/uk/getmondo/common/accounts/d;

.field private final g:Lco/uk/getmondo/common/e/a;

.field private final h:Lco/uk/getmondo/api/MonzoApi;

.field private final i:Lco/uk/getmondo/common/a;

.field private final j:Lco/uk/getmondo/waitlist/i;


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/common/a;Lco/uk/getmondo/common/q;Lco/uk/getmondo/waitlist/i;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 38
    iput-object p1, p0, Lco/uk/getmondo/create_account/wait/c;->d:Lio/reactivex/u;

    .line 39
    iput-object p2, p0, Lco/uk/getmondo/create_account/wait/c;->e:Lio/reactivex/u;

    .line 40
    iput-object p3, p0, Lco/uk/getmondo/create_account/wait/c;->f:Lco/uk/getmondo/common/accounts/d;

    .line 41
    iput-object p4, p0, Lco/uk/getmondo/create_account/wait/c;->g:Lco/uk/getmondo/common/e/a;

    .line 42
    iput-object p5, p0, Lco/uk/getmondo/create_account/wait/c;->h:Lco/uk/getmondo/api/MonzoApi;

    .line 43
    iput-object p6, p0, Lco/uk/getmondo/create_account/wait/c;->i:Lco/uk/getmondo/common/a;

    .line 44
    iput-object p7, p0, Lco/uk/getmondo/create_account/wait/c;->c:Lco/uk/getmondo/common/q;

    .line 45
    iput-object p8, p0, Lco/uk/getmondo/create_account/wait/c;->j:Lco/uk/getmondo/waitlist/i;

    .line 46
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/wait/c;Lco/uk/getmondo/create_account/wait/c$a;Lco/uk/getmondo/api/model/ApiCardDispatchStatus;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 59
    invoke-interface {p1}, Lco/uk/getmondo/create_account/wait/c$a;->d()V

    .line 60
    iget-object v0, p0, Lco/uk/getmondo/create_account/wait/c;->f:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v0

    .line 61
    invoke-virtual {p2}, Lco/uk/getmondo/api/model/ApiCardDispatchStatus;->a()Ljava/util/Date;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 62
    invoke-virtual {v0}, Lco/uk/getmondo/d/ac;->h()Lco/uk/getmondo/d/s;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/k/a;->a(Lco/uk/getmondo/d/s;)Ljava/lang/String;

    move-result-object v0

    .line 63
    invoke-virtual {p2}, Lco/uk/getmondo/api/model/ApiCardDispatchStatus;->a()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Lco/uk/getmondo/common/c/a;->b(J)Ljava/lang/String;

    move-result-object v1

    .line 62
    invoke-interface {p1, v0, v1}, Lco/uk/getmondo/create_account/wait/c$a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    :goto_0
    return-void

    .line 65
    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/d/ac;->h()Lco/uk/getmondo/d/s;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/k/a;->a(Lco/uk/getmondo/d/s;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lco/uk/getmondo/create_account/wait/c$a;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/wait/c;Lco/uk/getmondo/create_account/wait/c$a;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lco/uk/getmondo/create_account/wait/c;->g:Lco/uk/getmondo/common/e/a;

    invoke-virtual {v0, p2, p1}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    return-void
.end method


# virtual methods
.method a()V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lco/uk/getmondo/create_account/wait/c;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/create_account/wait/c$a;

    invoke-interface {v0}, Lco/uk/getmondo/create_account/wait/c$a;->c()V

    .line 78
    return-void
.end method

.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lco/uk/getmondo/create_account/wait/c$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/create_account/wait/c;->a(Lco/uk/getmondo/create_account/wait/c$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/create_account/wait/c$a;)V
    .locals 3

    .prologue
    .line 50
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 52
    iget-object v0, p0, Lco/uk/getmondo/create_account/wait/c;->i:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->A()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 53
    invoke-interface {p1}, Lco/uk/getmondo/create_account/wait/c$a;->e()V

    .line 55
    iget-object v0, p0, Lco/uk/getmondo/create_account/wait/c;->h:Lco/uk/getmondo/api/MonzoApi;

    iget-object v1, p0, Lco/uk/getmondo/create_account/wait/c;->f:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v1}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/d/ak;->c()Lco/uk/getmondo/d/a;

    move-result-object v1

    invoke-interface {v1}, Lco/uk/getmondo/d/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lco/uk/getmondo/api/MonzoApi;->getCardDispatchStatus(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/create_account/wait/c;->e:Lio/reactivex/u;

    .line 56
    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/create_account/wait/c;->d:Lio/reactivex/u;

    .line 57
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/create_account/wait/d;->a(Lco/uk/getmondo/create_account/wait/c;Lco/uk/getmondo/create_account/wait/c$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {p0, p1}, Lco/uk/getmondo/create_account/wait/e;->a(Lco/uk/getmondo/create_account/wait/c;Lco/uk/getmondo/create_account/wait/c$a;)Lio/reactivex/c/g;

    move-result-object v2

    .line 58
    invoke-virtual {v0, v1, v2}, Lio/reactivex/v;->a(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 55
    invoke-virtual {p0, v0}, Lco/uk/getmondo/create_account/wait/c;->a(Lio/reactivex/b/b;)V

    .line 69
    iget-object v0, p0, Lco/uk/getmondo/create_account/wait/c;->j:Lco/uk/getmondo/waitlist/i;

    invoke-virtual {v0}, Lco/uk/getmondo/waitlist/i;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    invoke-interface {p1}, Lco/uk/getmondo/create_account/wait/c$a;->a()V

    .line 74
    :goto_0
    return-void

    .line 72
    :cond_0
    invoke-interface {p1}, Lco/uk/getmondo/create_account/wait/c$a;->b()V

    goto :goto_0
.end method

.method c()V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lco/uk/getmondo/create_account/wait/c;->i:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;->GOOD_TO_GO:Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;

    invoke-static {v1}, Lco/uk/getmondo/api/model/tracking/Impression;->a(Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 82
    iget-object v0, p0, Lco/uk/getmondo/create_account/wait/c;->c:Lco/uk/getmondo/common/q;

    invoke-virtual {v0}, Lco/uk/getmondo/common/q;->a()V

    .line 83
    return-void
.end method
