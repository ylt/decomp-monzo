.class public Lco/uk/getmondo/create_account/wait/CardShippedActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "CardShippedActivity.java"

# interfaces
.implements Lco/uk/getmondo/create_account/wait/c$a;


# instance fields
.field a:Lco/uk/getmondo/create_account/wait/c;

.field animationView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11012b
    .end annotation
.end field

.field private final b:Landroid/os/Handler;

.field buttonsContainer:Landroid/view/ViewGroup;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11012c
    .end annotation
.end field

.field private final c:Ljava/lang/Runnable;

.field descriptionOnceArrivedView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11012f
    .end annotation
.end field

.field descriptionView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11012a
    .end annotation
.end field

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 27
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->b:Landroid/os/Handler;

    .line 28
    invoke-static {p0}, Lco/uk/getmondo/create_account/wait/a;->a(Lco/uk/getmondo/create_account/wait/CardShippedActivity;)Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->c:Ljava/lang/Runnable;

    return-void
.end method

.method public static a(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 40
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/create_account/wait/CardShippedActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 41
    const v1, 0x10018000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 42
    const-string v1, "EXTRA_ACTIVATE_CARD_BUTTON"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 43
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 44
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->descriptionOnceArrivedView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 70
    iget-object v0, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->buttonsContainer:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 71
    iget-object v0, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->animationView:Landroid/widget/ImageView;

    const v1, 0x7f0200b1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 72
    iget-object v0, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->animationView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    .line 73
    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 74
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 130
    iget-object v0, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->descriptionView:Landroid/widget/TextView;

    const v1, 0x7f0a014c

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {p0, v1, v2}, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 99
    iget-object v0, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->descriptionView:Landroid/widget/TextView;

    const v1, 0x7f0a014d

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-virtual {p0, v1, v2}, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->descriptionOnceArrivedView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 79
    iget-object v0, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->buttonsContainer:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 80
    iget-object v0, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->animationView:Landroid/widget/ImageView;

    const v1, 0x7f02007f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 81
    iget-object v0, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->animationView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    .line 82
    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 83
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 114
    invoke-static {p0}, Lco/uk/getmondo/card/activate/ActivateCardActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->startActivity(Landroid/content/Intent;)V

    .line 115
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->b:Landroid/os/Handler;

    iget-object v1, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 120
    invoke-virtual {p0}, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->t()V

    .line 121
    return-void
.end method

.method public e()V
    .locals 4

    .prologue
    .line 125
    iget-object v0, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->b:Landroid/os/Handler;

    iget-object v1, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->c:Ljava/lang/Runnable;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 126
    return-void
.end method

.method onCardArrivedClick()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f11012d
        }
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->a:Lco/uk/getmondo/create_account/wait/c;

    invoke-virtual {v0}, Lco/uk/getmondo/create_account/wait/c;->a()V

    .line 105
    return-void
.end method

.method onCardDidntArriveClick()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f11012e
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->a:Lco/uk/getmondo/create_account/wait/c;

    invoke-virtual {v0}, Lco/uk/getmondo/create_account/wait/c;->c()V

    .line 110
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetTextI18n"
        }
    .end annotation

    .prologue
    .line 49
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 50
    const v0, 0x7f050027

    invoke-virtual {p0, v0}, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->setContentView(I)V

    .line 51
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 52
    invoke-virtual {p0}, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/create_account/wait/CardShippedActivity;)V

    .line 54
    invoke-virtual {p0}, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_ACTIVATE_CARD_BUTTON"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->e:Z

    .line 55
    iget-object v0, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->a:Lco/uk/getmondo/create_account/wait/c;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/create_account/wait/c;->a(Lco/uk/getmondo/create_account/wait/c$a;)V

    .line 56
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 93
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 94
    iget-object v0, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->a:Lco/uk/getmondo/create_account/wait/c;

    invoke-virtual {v0}, Lco/uk/getmondo/create_account/wait/c;->b()V

    .line 95
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 87
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onPause()V

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->e:Z

    .line 89
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 60
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onResume()V

    .line 62
    iget-boolean v0, p0, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->e:Z

    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {p0}, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->a()V

    .line 65
    :cond_0
    return-void
.end method
