.class public Lco/uk/getmondo/create_account/c;
.super Ljava/lang/Object;
.source "ExpiryDateTextFormatter.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    const-string v0, " / "

    return-object v0
.end method

.method private a(Ljava/lang/CharSequence;Landroid/text/TextWatcher;Landroid/widget/EditText;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 39
    invoke-direct {p0, p1}, Lco/uk/getmondo/create_account/c;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v3, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 45
    :goto_0
    invoke-direct {p0, v0, p2, p3}, Lco/uk/getmondo/create_account/c;->c(Ljava/lang/CharSequence;Landroid/text/TextWatcher;Landroid/widget/EditText;)V

    .line 46
    return-void

    .line 42
    :cond_0
    invoke-direct {p0, p1}, Lco/uk/getmondo/create_account/c;->b(Ljava/lang/CharSequence;)C

    move-result v0

    .line 43
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {p1, v3, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lco/uk/getmondo/create_account/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/lang/CharSequence;)Z
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lco/uk/getmondo/create_account/c;->b(Ljava/lang/CharSequence;)C

    move-result v0

    .line 59
    const/16 v1, 0x2f

    if-eq v0, v1, :cond_0

    const/16 v1, 0x2e

    if-eq v0, v1, :cond_0

    const/16 v1, 0x5f

    if-eq v0, v1, :cond_0

    const/16 v1, 0x2d

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/lang/CharSequence;)C
    .locals 1

    .prologue
    .line 63
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    return v0
.end method

.method private b(Ljava/lang/CharSequence;Landroid/text/TextWatcher;Landroid/widget/EditText;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 49
    invoke-direct {p0, p1}, Lco/uk/getmondo/create_account/c;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v2, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 54
    :goto_0
    invoke-direct {p0, v0, p2, p3}, Lco/uk/getmondo/create_account/c;->c(Ljava/lang/CharSequence;Landroid/text/TextWatcher;Landroid/widget/EditText;)V

    .line 55
    return-void

    .line 52
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-interface {p1, v2, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lco/uk/getmondo/create_account/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private c(Ljava/lang/CharSequence;Landroid/text/TextWatcher;Landroid/widget/EditText;)V
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p3, p2}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 76
    invoke-virtual {p3, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 77
    invoke-virtual {p3, p2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 78
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    invoke-virtual {p3, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 79
    return-void
.end method

.method private c(Ljava/lang/CharSequence;)Z
    .locals 2

    .prologue
    .line 67
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(Ljava/lang/CharSequence;)Z
    .locals 2

    .prologue
    .line 71
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/CharSequence;IILandroid/text/TextWatcher;Landroid/widget/EditText;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v3, 0x9

    const/4 v2, 0x0

    .line 11
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 36
    :cond_0
    :goto_0
    return-void

    .line 14
    :cond_1
    if-gt p2, p3, :cond_2

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-le v0, v3, :cond_4

    .line 15
    :cond_2
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 16
    invoke-interface {p1, v2, v4}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 17
    invoke-direct {p0, v0, p4, p5}, Lco/uk/getmondo/create_account/c;->c(Ljava/lang/CharSequence;Landroid/text/TextWatcher;Landroid/widget/EditText;)V

    goto :goto_0

    .line 18
    :cond_3
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-le v0, v3, :cond_0

    .line 19
    invoke-interface {p1, v2, v3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 20
    invoke-direct {p0, v0, p4, p5}, Lco/uk/getmondo/create_account/c;->c(Ljava/lang/CharSequence;Landroid/text/TextWatcher;Landroid/widget/EditText;)V

    goto :goto_0

    .line 24
    :cond_4
    invoke-direct {p0, p1}, Lco/uk/getmondo/create_account/c;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-direct {p0, p1}, Lco/uk/getmondo/create_account/c;->c(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 25
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lco/uk/getmondo/create_account/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p4, p5}, Lco/uk/getmondo/create_account/c;->c(Ljava/lang/CharSequence;Landroid/text/TextWatcher;Landroid/widget/EditText;)V

    .line 27
    :cond_6
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_7

    .line 28
    invoke-direct {p0, p1, p4, p5}, Lco/uk/getmondo/create_account/c;->a(Ljava/lang/CharSequence;Landroid/text/TextWatcher;Landroid/widget/EditText;)V

    goto :goto_0

    .line 29
    :cond_7
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-ne v0, v4, :cond_8

    .line 30
    invoke-direct {p0, p1, p4, p5}, Lco/uk/getmondo/create_account/c;->b(Ljava/lang/CharSequence;Landroid/text/TextWatcher;Landroid/widget/EditText;)V

    goto :goto_0

    .line 31
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, p1}, Lco/uk/getmondo/create_account/c;->b(Ljava/lang/CharSequence;)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isDigitsOnly(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 32
    invoke-direct {p0, p1, p4, p5}, Lco/uk/getmondo/create_account/c;->c(Ljava/lang/CharSequence;Landroid/text/TextWatcher;Landroid/widget/EditText;)V

    goto :goto_0

    .line 34
    :cond_9
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v2, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, v0, p4, p5}, Lco/uk/getmondo/create_account/c;->c(Ljava/lang/CharSequence;Landroid/text/TextWatcher;Landroid/widget/EditText;)V

    goto/16 :goto_0
.end method
