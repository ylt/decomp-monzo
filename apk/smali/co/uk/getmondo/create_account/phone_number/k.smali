.class public Lco/uk/getmondo/create_account/phone_number/k;
.super Lco/uk/getmondo/common/ui/b;
.source "EnterCodePresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/create_account/phone_number/k$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/create_account/phone_number/k$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/common/accounts/d;

.field private final f:Lco/uk/getmondo/api/MonzoApi;

.field private final g:Lco/uk/getmondo/common/a;

.field private final h:Lco/uk/getmondo/common/e/a;


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/common/a;Lco/uk/getmondo/common/e/a;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 34
    iput-object p1, p0, Lco/uk/getmondo/create_account/phone_number/k;->c:Lio/reactivex/u;

    .line 35
    iput-object p2, p0, Lco/uk/getmondo/create_account/phone_number/k;->d:Lio/reactivex/u;

    .line 36
    iput-object p3, p0, Lco/uk/getmondo/create_account/phone_number/k;->e:Lco/uk/getmondo/common/accounts/d;

    .line 37
    iput-object p4, p0, Lco/uk/getmondo/create_account/phone_number/k;->f:Lco/uk/getmondo/api/MonzoApi;

    .line 38
    iput-object p5, p0, Lco/uk/getmondo/create_account/phone_number/k;->g:Lco/uk/getmondo/common/a;

    .line 39
    iput-object p6, p0, Lco/uk/getmondo/create_account/phone_number/k;->h:Lco/uk/getmondo/common/e/a;

    .line 40
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/phone_number/k;Lco/uk/getmondo/d/ak;Lco/uk/getmondo/create_account/phone_number/k$a;Ljava/lang/String;)Lio/reactivex/l;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lco/uk/getmondo/create_account/phone_number/k;->f:Lco/uk/getmondo/api/MonzoApi;

    invoke-virtual {p1}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/d/ac;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p3}, Lco/uk/getmondo/api/MonzoApi;->checkVerificationCode(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lio/reactivex/v;->e()Lio/reactivex/h;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/create_account/phone_number/k;->d:Lio/reactivex/u;

    .line 53
    invoke-virtual {v0, v1}, Lio/reactivex/h;->b(Lio/reactivex/u;)Lio/reactivex/h;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/create_account/phone_number/k;->c:Lio/reactivex/u;

    .line 54
    invoke-virtual {v0, v1}, Lio/reactivex/h;->a(Lio/reactivex/u;)Lio/reactivex/h;

    move-result-object v0

    invoke-static {p2}, Lco/uk/getmondo/create_account/phone_number/o;->a(Lco/uk/getmondo/create_account/phone_number/k$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 55
    invoke-virtual {v0, v1}, Lio/reactivex/h;->b(Lio/reactivex/c/g;)Lio/reactivex/h;

    move-result-object v0

    invoke-static {p0, p2}, Lco/uk/getmondo/create_account/phone_number/p;->a(Lco/uk/getmondo/create_account/phone_number/k;Lco/uk/getmondo/create_account/phone_number/k$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 59
    invoke-virtual {v0, v1}, Lio/reactivex/h;->a(Lio/reactivex/c/g;)Lio/reactivex/h;

    move-result-object v0

    .line 60
    invoke-static {}, Lio/reactivex/h;->a()Lio/reactivex/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/h;->a(Lio/reactivex/l;)Lio/reactivex/h;

    move-result-object v0

    invoke-static {p2}, Lco/uk/getmondo/create_account/phone_number/q;->a(Lco/uk/getmondo/create_account/phone_number/k$a;)Lio/reactivex/c/b;

    move-result-object v1

    .line 61
    invoke-virtual {v0, v1}, Lio/reactivex/h;->a(Lio/reactivex/c/b;)Lio/reactivex/h;

    move-result-object v0

    .line 51
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/phone_number/k$a;Lco/uk/getmondo/api/model/ApiCodeCheckResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 67
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiCodeCheckResult;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 68
    const v0, 0x7f0a0184

    invoke-interface {p0, v0}, Lco/uk/getmondo/create_account/phone_number/k$a;->b(I)V

    .line 72
    :goto_0
    return-void

    .line 71
    :cond_0
    invoke-interface {p0}, Lco/uk/getmondo/create_account/phone_number/k$a;->a()V

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/phone_number/k$a;Lco/uk/getmondo/api/model/ApiCodeCheckResult;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 62
    invoke-interface {p0}, Lco/uk/getmondo/create_account/phone_number/k$a;->t()V

    .line 63
    const/4 v0, 0x1

    invoke-interface {p0, v0}, Lco/uk/getmondo/create_account/phone_number/k$a;->a(Z)V

    .line 64
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/phone_number/k$a;Lio/reactivex/b/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 56
    invoke-interface {p0}, Lco/uk/getmondo/create_account/phone_number/k$a;->s()V

    .line 57
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Lco/uk/getmondo/create_account/phone_number/k$a;->a(Z)V

    .line 58
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/phone_number/k;Lco/uk/getmondo/create_account/phone_number/k$a;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lco/uk/getmondo/create_account/phone_number/k;->h:Lco/uk/getmondo/common/e/a;

    invoke-virtual {v0, p2, p1}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lco/uk/getmondo/create_account/phone_number/k$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/create_account/phone_number/k;->a(Lco/uk/getmondo/create_account/phone_number/k$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/create_account/phone_number/k$a;)V
    .locals 3

    .prologue
    .line 44
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 46
    iget-object v0, p0, Lco/uk/getmondo/create_account/phone_number/k;->g:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->t()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 48
    iget-object v0, p0, Lco/uk/getmondo/create_account/phone_number/k;->e:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    .line 50
    invoke-interface {p1}, Lco/uk/getmondo/create_account/phone_number/k$a;->b()Lio/reactivex/n;

    move-result-object v1

    invoke-static {p0, v0, p1}, Lco/uk/getmondo/create_account/phone_number/l;->a(Lco/uk/getmondo/create_account/phone_number/k;Lco/uk/getmondo/d/ak;Lco/uk/getmondo/create_account/phone_number/k$a;)Lio/reactivex/c/h;

    move-result-object v0

    .line 51
    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/create_account/phone_number/k;->c:Lio/reactivex/u;

    .line 65
    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/create_account/phone_number/m;->a(Lco/uk/getmondo/create_account/phone_number/k$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/create_account/phone_number/n;->a()Lio/reactivex/c/g;

    move-result-object v2

    .line 66
    invoke-virtual {v0, v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 50
    invoke-virtual {p0, v0}, Lco/uk/getmondo/create_account/phone_number/k;->a(Lio/reactivex/b/b;)V

    .line 73
    return-void
.end method
