.class public final Lco/uk/getmondo/create_account/phone_number/ag;
.super Landroid/content/BroadcastReceiver;
.source "SmsCodeReceiver.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/create_account/phone_number/ag$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0011\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u00102\u00020\u0001:\u0001\u0010B\u0005\u00a2\u0006\u0002\u0010\u0002J\u001c\u0010\n\u001a\u00020\u000b2\u0008\u0010\u000c\u001a\u0004\u0018\u00010\r2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0016R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0008X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\t\u00a8\u0006\u0011"
    }
    d2 = {
        "Lco/uk/getmondo/create_account/phone_number/SmsCodeReceiver;",
        "Landroid/content/BroadcastReceiver;",
        "()V",
        "listener",
        "Lco/uk/getmondo/create_account/phone_number/CodeListener;",
        "regex",
        "",
        "sender",
        "",
        "[Ljava/lang/String;",
        "onReceive",
        "",
        "context",
        "Landroid/content/Context;",
        "intent",
        "Landroid/content/Intent;",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/create_account/phone_number/ag$a;


# instance fields
.field private b:Ljava/lang/String;

.field private c:[Ljava/lang/String;

.field private d:Lco/uk/getmondo/create_account/phone_number/a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/create_account/phone_number/ag$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/create_account/phone_number/ag$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/create_account/phone_number/ag;->a:Lco/uk/getmondo/create_account/phone_number/ag$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lco/uk/getmondo/create_account/phone_number/ag;->b:Ljava/lang/String;

    .line 78
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lco/uk/getmondo/create_account/phone_number/ag;->c:[Ljava/lang/String;

    return-void
.end method

.method public static final a(Landroid/content/Context;Lco/uk/getmondo/create_account/phone_number/ag;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "smsCodeReceiver"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/create_account/phone_number/ag;->a:Lco/uk/getmondo/create_account/phone_number/ag$a;

    invoke-virtual {v0, p0, p1}, Lco/uk/getmondo/create_account/phone_number/ag$a;->a(Landroid/content/Context;Lco/uk/getmondo/create_account/phone_number/ag;)V

    return-void
.end method

.method public static final a(Landroid/content/Context;Lco/uk/getmondo/create_account/phone_number/ag;Ljava/lang/String;[Ljava/lang/String;Lco/uk/getmondo/create_account/phone_number/a;)V
    .locals 6

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "smsCodeReceiver"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "regex"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sender"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/create_account/phone_number/ag;->a:Lco/uk/getmondo/create_account/phone_number/ag$a;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lco/uk/getmondo/create_account/phone_number/ag$a;->a(Landroid/content/Context;Lco/uk/getmondo/create_account/phone_number/ag;Ljava/lang/String;[Ljava/lang/String;Lco/uk/getmondo/create_account/phone_number/a;)V

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/create_account/phone_number/ag;Lco/uk/getmondo/create_account/phone_number/a;)V
    .locals 0

    .prologue
    .line 12
    iput-object p1, p0, Lco/uk/getmondo/create_account/phone_number/ag;->d:Lco/uk/getmondo/create_account/phone_number/a;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/create_account/phone_number/ag;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 12
    iput-object p1, p0, Lco/uk/getmondo/create_account/phone_number/ag;->b:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/create_account/phone_number/ag;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 12
    iput-object p1, p0, Lco/uk/getmondo/create_account/phone_number/ag;->c:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12

    .prologue
    const/4 v0, 0x0

    const/4 v9, 0x1

    const/4 v6, 0x0

    .line 19
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 20
    :goto_0
    if-eqz v1, :cond_0

    const-string v0, "pdus"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    :cond_0
    if-nez v0, :cond_2

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Array<*>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move-object v1, v0

    .line 19
    goto :goto_0

    .line 20
    :cond_2
    check-cast v0, [Ljava/lang/Object;

    .line 21
    const-string v2, "format"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 22
    iget-object v1, p0, Lco/uk/getmondo/create_account/phone_number/ag;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v11

    move v5, v6

    .line 24
    :goto_1
    array-length v1, v0

    if-ge v5, v1, :cond_3

    aget-object v1, v0, v5

    .line 25
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    if-lt v2, v3, :cond_4

    .line 27
    check-cast v1, [B

    invoke-static {v1, v10}, Landroid/telephony/SmsMessage;->createFromPdu([BLjava/lang/String;)Landroid/telephony/SmsMessage;

    move-result-object v1

    move-object v7, v1

    .line 32
    :goto_2
    invoke-virtual {v7}, Landroid/telephony/SmsMessage;->getDisplayOriginatingAddress()Ljava/lang/String;

    move-result-object v4

    .line 33
    iget-object v1, p0, Lco/uk/getmondo/create_account/phone_number/ag;->c:[Ljava/lang/String;

    check-cast v1, [Ljava/lang/Object;

    move v8, v6

    .line 76
    :goto_3
    array-length v2, v1

    if-ge v8, v2, :cond_6

    aget-object v2, v1, v8

    check-cast v2, Ljava/lang/String;

    .line 33
    check-cast v2, Ljava/lang/CharSequence;

    const-string v3, "originatingAddress"

    invoke-static {v4, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, v4

    check-cast v3, Ljava/lang/CharSequence;

    invoke-static {v2, v3, v9}, Lkotlin/h/j;->b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Z

    move-result v2

    if-eqz v2, :cond_5

    move v1, v9

    .line 77
    :goto_4
    if-nez v1, :cond_7

    .line 45
    :cond_3
    return-void

    .line 29
    :cond_4
    check-cast v1, [B

    invoke-static {v1}, Landroid/telephony/SmsMessage;->createFromPdu([B)Landroid/telephony/SmsMessage;

    move-result-object v1

    move-object v7, v1

    goto :goto_2

    .line 33
    :cond_5
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto :goto_3

    :cond_6
    move v1, v6

    .line 77
    goto :goto_4

    .line 37
    :cond_7
    invoke-virtual {v7}, Landroid/telephony/SmsMessage;->getDisplayMessageBody()Ljava/lang/String;

    move-result-object v1

    .line 38
    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v11, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 40
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 41
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v1

    .line 42
    iget-object v2, p0, Lco/uk/getmondo/create_account/phone_number/ag;->d:Lco/uk/getmondo/create_account/phone_number/a;

    if-eqz v2, :cond_8

    invoke-interface {v2, v1}, Lco/uk/getmondo/create_account/phone_number/a;->a(Ljava/lang/String;)V

    .line 24
    :cond_8
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_1
.end method
