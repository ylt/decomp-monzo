.class public Lco/uk/getmondo/create_account/phone_number/x;
.super Lco/uk/getmondo/common/ui/b;
.source "EnterPhoneNumberPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/create_account/phone_number/x$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/create_account/phone_number/x$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/common/e/a;

.field private final f:Lco/uk/getmondo/common/accounts/d;

.field private final g:Lco/uk/getmondo/api/MonzoApi;

.field private final h:Lco/uk/getmondo/common/a;


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/common/a;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 34
    iput-object p1, p0, Lco/uk/getmondo/create_account/phone_number/x;->c:Lio/reactivex/u;

    .line 35
    iput-object p2, p0, Lco/uk/getmondo/create_account/phone_number/x;->d:Lio/reactivex/u;

    .line 36
    iput-object p3, p0, Lco/uk/getmondo/create_account/phone_number/x;->e:Lco/uk/getmondo/common/e/a;

    .line 37
    iput-object p4, p0, Lco/uk/getmondo/create_account/phone_number/x;->f:Lco/uk/getmondo/common/accounts/d;

    .line 38
    iput-object p5, p0, Lco/uk/getmondo/create_account/phone_number/x;->g:Lco/uk/getmondo/api/MonzoApi;

    .line 39
    iput-object p6, p0, Lco/uk/getmondo/create_account/phone_number/x;->h:Lco/uk/getmondo/common/a;

    .line 40
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/phone_number/x;Lco/uk/getmondo/create_account/phone_number/x$a;Ljava/lang/String;)Lio/reactivex/l;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lco/uk/getmondo/create_account/phone_number/x;->g:Lco/uk/getmondo/api/MonzoApi;

    invoke-interface {v0, p2}, Lco/uk/getmondo/api/MonzoApi;->sendVerificationCode(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/common/b/a;->a:Lco/uk/getmondo/common/b/a;

    .line 64
    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lio/reactivex/v;->e()Lio/reactivex/h;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/create_account/phone_number/x;->d:Lio/reactivex/u;

    .line 66
    invoke-virtual {v0, v1}, Lio/reactivex/h;->b(Lio/reactivex/u;)Lio/reactivex/h;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/create_account/phone_number/x;->c:Lio/reactivex/u;

    .line 67
    invoke-virtual {v0, v1}, Lio/reactivex/h;->a(Lio/reactivex/u;)Lio/reactivex/h;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/create_account/phone_number/ad;->a(Lco/uk/getmondo/create_account/phone_number/x;Lco/uk/getmondo/create_account/phone_number/x$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 68
    invoke-virtual {v0, v1}, Lio/reactivex/h;->a(Lio/reactivex/c/g;)Lio/reactivex/h;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lio/reactivex/h;->f()Lio/reactivex/h;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/create_account/phone_number/ae;->a(Lco/uk/getmondo/create_account/phone_number/x$a;)Lio/reactivex/c/b;

    move-result-object v1

    .line 70
    invoke-virtual {v0, v1}, Lio/reactivex/h;->a(Lio/reactivex/c/b;)Lio/reactivex/h;

    move-result-object v0

    .line 63
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/phone_number/x$a;Lco/uk/getmondo/common/b/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 71
    invoke-interface {p0}, Lco/uk/getmondo/create_account/phone_number/x$a;->d()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/phone_number/x$a;Lco/uk/getmondo/common/b/a;Ljava/lang/Throwable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 70
    invoke-interface {p0}, Lco/uk/getmondo/create_account/phone_number/x$a;->e()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/phone_number/x$a;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 75
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    invoke-interface {p0}, Lco/uk/getmondo/create_account/phone_number/x$a;->c()V

    .line 78
    :cond_0
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/phone_number/x;Lco/uk/getmondo/create_account/phone_number/x$a;Lco/uk/getmondo/d/ak;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 60
    invoke-interface {p1}, Lco/uk/getmondo/create_account/phone_number/x$a;->f()V

    .line 61
    iget-object v0, p0, Lco/uk/getmondo/create_account/phone_number/x;->f:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {p2}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v1

    invoke-virtual {v1, p3}, Lco/uk/getmondo/d/ac;->a(Ljava/lang/String;)Lco/uk/getmondo/d/ac;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/accounts/d;->a(Lco/uk/getmondo/d/ac;)V

    .line 62
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/phone_number/x;Lco/uk/getmondo/create_account/phone_number/x$a;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lco/uk/getmondo/create_account/phone_number/x;->e:Lco/uk/getmondo/common/e/a;

    invoke-virtual {v0, p2, p1}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lco/uk/getmondo/create_account/phone_number/x$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/create_account/phone_number/x;->a(Lco/uk/getmondo/create_account/phone_number/x$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/create_account/phone_number/x$a;)V
    .locals 3

    .prologue
    .line 44
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 46
    iget-object v0, p0, Lco/uk/getmondo/create_account/phone_number/x;->h:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->s()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 49
    iget-object v0, p0, Lco/uk/getmondo/create_account/phone_number/x;->f:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v1

    .line 50
    const/4 v0, 0x0

    .line 51
    if-eqz v1, :cond_0

    .line 52
    invoke-virtual {v1}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v0

    .line 54
    :cond_0
    if-eqz v0, :cond_1

    .line 55
    invoke-virtual {v0}, Lco/uk/getmondo/d/ac;->f()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lco/uk/getmondo/create_account/phone_number/x$a;->a(Ljava/lang/String;)V

    .line 58
    :cond_1
    invoke-interface {p1}, Lco/uk/getmondo/create_account/phone_number/x$a;->a()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1, v1}, Lco/uk/getmondo/create_account/phone_number/y;->a(Lco/uk/getmondo/create_account/phone_number/x;Lco/uk/getmondo/create_account/phone_number/x$a;Lco/uk/getmondo/d/ak;)Lio/reactivex/c/g;

    move-result-object v1

    .line 59
    invoke-virtual {v0, v1}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/create_account/phone_number/z;->a(Lco/uk/getmondo/create_account/phone_number/x;Lco/uk/getmondo/create_account/phone_number/x$a;)Lio/reactivex/c/h;

    move-result-object v1

    .line 63
    invoke-virtual {v0, v1}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/create_account/phone_number/aa;->a(Lco/uk/getmondo/create_account/phone_number/x$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/create_account/phone_number/ab;->a()Lio/reactivex/c/g;

    move-result-object v2

    .line 71
    invoke-virtual {v0, v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 58
    invoke-virtual {p0, v0}, Lco/uk/getmondo/create_account/phone_number/x;->a(Lio/reactivex/b/b;)V

    .line 73
    invoke-interface {p1}, Lco/uk/getmondo/create_account/phone_number/x$a;->b()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/create_account/phone_number/ac;->a(Lco/uk/getmondo/create_account/phone_number/x$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 74
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 73
    invoke-virtual {p0, v0}, Lco/uk/getmondo/create_account/phone_number/x;->a(Lio/reactivex/b/b;)V

    .line 79
    return-void
.end method
