.class public Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "EnterPhoneNumberActivity.java"

# interfaces
.implements Lco/uk/getmondo/create_account/phone_number/x$a;


# instance fields
.field a:Lco/uk/getmondo/create_account/phone_number/x;

.field private b:Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field phoneNumberInput:Landroid/widget/EditText;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110169
    .end annotation
.end field

.field textCodeButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11016a
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 32
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;->c:Lcom/b/b/c;

    .line 33
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;->e:Lcom/b/b/c;

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;Ljava/lang/Object;)Ljava/lang/Boolean;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 93
    invoke-static {p0}, Lco/uk/getmondo/common/k/k;->a(Landroid/content/Context;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 41
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 42
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 43
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 56
    const/4 v0, 0x4

    if-ne p2, v0, :cond_0

    .line 57
    iget-object v0, p0, Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;->c:Lcom/b/b/c;

    sget-object v1, Lco/uk/getmondo/common/b/a;->a:Lco/uk/getmondo/common/b/a;

    invoke-virtual {v0, v1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 58
    const/4 v0, 0x1

    .line 60
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;->phoneNumberInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 85
    invoke-static {p0}, Lco/uk/getmondo/common/k/k;->a(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;->b:Lio/reactivex/n;

    iget-object v1, p0, Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;->c:Lcom/b/b/c;

    invoke-static {v0, v1}, Lio/reactivex/n;->merge(Lio/reactivex/r;Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/create_account/phone_number/t;->a(Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;)Lio/reactivex/c/q;

    move-result-object v1

    .line 85
    invoke-virtual {v0, v1}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;->e:Lcom/b/b/c;

    .line 86
    invoke-virtual {v0, v1}, Lio/reactivex/n;->mergeWith(Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/create_account/phone_number/u;->a(Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;)Lio/reactivex/c/h;

    move-result-object v1

    .line 87
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 84
    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;->phoneNumberInput:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 121
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;->b:Lio/reactivex/n;

    iget-object v1, p0, Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;->c:Lcom/b/b/c;

    invoke-static {v0, v1}, Lio/reactivex/n;->merge(Lio/reactivex/r;Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/create_account/phone_number/v;->a(Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;)Lio/reactivex/c/h;

    move-result-object v1

    .line 93
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 92
    return-object v0
.end method

.method public c()V
    .locals 3

    .prologue
    .line 98
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.RECEIVE_SMS"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.permission.READ_SMS"

    aput-object v2, v0, v1

    const/16 v1, 0x3e9

    invoke-static {p0, v0, v1}, Landroid/support/v4/app/a;->a(Landroid/app/Activity;[Ljava/lang/String;I)V

    .line 101
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 105
    invoke-static {p0}, Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;->a(Landroid/content/Context;)V

    .line 106
    return-void
.end method

.method public e()V
    .locals 0

    .prologue
    .line 110
    invoke-virtual {p0}, Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;->t()V

    .line 111
    return-void
.end method

.method public f()V
    .locals 0

    .prologue
    .line 115
    invoke-virtual {p0}, Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;->s()V

    .line 116
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetTextI18n"
        }
    .end annotation

    .prologue
    .line 48
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 49
    const v0, 0x7f050038

    invoke-virtual {p0, v0}, Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;->setContentView(I)V

    .line 50
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 51
    invoke-virtual {p0}, Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;)V

    .line 53
    iget-object v0, p0, Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;->textCodeButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/n;->share()Lio/reactivex/n;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;->b:Lio/reactivex/n;

    .line 55
    iget-object v0, p0, Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;->phoneNumberInput:Landroid/widget/EditText;

    invoke-static {p0}, Lco/uk/getmondo/create_account/phone_number/s;->a(Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;)Landroid/widget/TextView$OnEditorActionListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 63
    iget-object v0, p0, Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;->a:Lco/uk/getmondo/create_account/phone_number/x;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/create_account/phone_number/x;->a(Lco/uk/getmondo/create_account/phone_number/x$a;)V

    .line 64
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 69
    iget-object v0, p0, Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;->a:Lco/uk/getmondo/create_account/phone_number/x;

    invoke-virtual {v0}, Lco/uk/getmondo/create_account/phone_number/x;->b()V

    .line 70
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 2

    .prologue
    .line 74
    const/16 v0, 0x3e9

    if-ne p1, v0, :cond_0

    .line 76
    iget-object v0, p0, Lco/uk/getmondo/create_account/phone_number/EnterPhoneNumberActivity;->e:Lcom/b/b/c;

    sget-object v1, Lco/uk/getmondo/common/b/a;->a:Lco/uk/getmondo/common/b/a;

    invoke-virtual {v0, v1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 80
    :goto_0
    return-void

    .line 79
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lco/uk/getmondo/common/activities/b;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    goto :goto_0
.end method
