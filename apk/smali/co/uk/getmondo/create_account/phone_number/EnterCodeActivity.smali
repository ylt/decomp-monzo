.class public Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "EnterCodeActivity.java"

# interfaces
.implements Lco/uk/getmondo/create_account/phone_number/k$a;


# instance fields
.field a:Lco/uk/getmondo/create_account/phone_number/k;

.field submitCodeButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110168
    .end annotation
.end field

.field verificationCodeInput:Landroid/widget/EditText;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110167
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;->verificationCodeInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 34
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 35
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 36
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;->verificationCodeInput:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;Lco/uk/getmondo/create_account/phone_number/ag;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 93
    invoke-static {p0, p1}, Lco/uk/getmondo/create_account/phone_number/ag;->a(Landroid/content/Context;Lco/uk/getmondo/create_account/phone_number/ag;)V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;Lio/reactivex/o;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 90
    new-instance v0, Lco/uk/getmondo/create_account/phone_number/ag;

    invoke-direct {v0}, Lco/uk/getmondo/create_account/phone_number/ag;-><init>()V

    .line 91
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p1}, Lco/uk/getmondo/create_account/phone_number/f;->a(Lio/reactivex/o;)Lco/uk/getmondo/create_account/phone_number/a;

    move-result-object v1

    .line 92
    const-string v2, "\\d{4,8}"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "Monzo"

    aput-object v5, v3, v4

    invoke-static {p0, v0, v2, v3, v1}, Lco/uk/getmondo/create_account/phone_number/ag;->a(Landroid/content/Context;Lco/uk/getmondo/create_account/phone_number/ag;Ljava/lang/String;[Ljava/lang/String;Lco/uk/getmondo/create_account/phone_number/a;)V

    .line 93
    invoke-static {p0, v0}, Lco/uk/getmondo/create_account/phone_number/g;->a(Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;Lco/uk/getmondo/create_account/phone_number/ag;)Lio/reactivex/c/f;

    move-result-object v0

    invoke-interface {p1, v0}, Lio/reactivex/o;->a(Lio/reactivex/c/f;)V

    .line 94
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;->verificationCodeInput:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic a(Lio/reactivex/o;Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x4

    if-ne p2, v0, :cond_0

    .line 79
    sget-object v0, Lco/uk/getmondo/common/b/a;->a:Lco/uk/getmondo/common/b/a;

    invoke-interface {p0, v0}, Lio/reactivex/o;->a(Ljava/lang/Object;)V

    .line 80
    const/4 v0, 0x1

    .line 82
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;Lio/reactivex/o;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;->verificationCodeInput:Landroid/widget/EditText;

    invoke-static {p1}, Lco/uk/getmondo/create_account/phone_number/h;->a(Lio/reactivex/o;)Landroid/widget/TextView$OnEditorActionListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 84
    invoke-static {p0}, Lco/uk/getmondo/create_account/phone_number/i;->a(Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;)Lio/reactivex/c/f;

    move-result-object v0

    invoke-interface {p1, v0}, Lio/reactivex/o;->a(Lio/reactivex/c/f;)V

    .line 85
    return-void
.end method

.method private c()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    invoke-static {p0}, Lco/uk/getmondo/create_account/phone_number/d;->a(Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;)Lio/reactivex/p;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method private d()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    invoke-static {p0}, Lco/uk/getmondo/create_account/phone_number/e;->a(Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;)Lio/reactivex/p;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 56
    invoke-static {p0}, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;->startActivity(Landroid/content/Intent;)V

    .line 57
    invoke-virtual {p0}, Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;->finish()V

    .line 58
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;->submitCodeButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 63
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;->submitCodeButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    .line 68
    invoke-direct {p0}, Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;->c()Lio/reactivex/n;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/n;->mergeWith(Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v1

    .line 69
    invoke-static {p0}, Lco/uk/getmondo/common/k/k;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    invoke-direct {p0}, Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;->d()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/create_account/phone_number/b;->a(Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;)Lio/reactivex/c/g;

    move-result-object v2

    invoke-virtual {v0, v2}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v0

    .line 69
    :goto_0
    invoke-virtual {v1, v0}, Lio/reactivex/n;->mergeWith(Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/create_account/phone_number/c;->a(Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;)Lio/reactivex/c/h;

    move-result-object v1

    .line 72
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 67
    return-object v0

    .line 71
    :cond_0
    invoke-static {}, Lio/reactivex/n;->empty()Lio/reactivex/n;

    move-result-object v0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 40
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 41
    const v0, 0x7f050037

    invoke-virtual {p0, v0}, Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;->setContentView(I)V

    .line 42
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 43
    invoke-virtual {p0}, Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;)V

    .line 45
    iget-object v0, p0, Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;->a:Lco/uk/getmondo/create_account/phone_number/k;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/create_account/phone_number/k;->a(Lco/uk/getmondo/create_account/phone_number/k$a;)V

    .line 46
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lco/uk/getmondo/create_account/phone_number/EnterCodeActivity;->a:Lco/uk/getmondo/create_account/phone_number/k;

    invoke-virtual {v0}, Lco/uk/getmondo/create_account/phone_number/k;->b()V

    .line 51
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 52
    return-void
.end method
