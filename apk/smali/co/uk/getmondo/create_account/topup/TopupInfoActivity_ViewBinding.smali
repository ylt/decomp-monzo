.class public Lco/uk/getmondo/create_account/topup/TopupInfoActivity_ViewBinding;
.super Ljava/lang/Object;
.source "TopupInfoActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/create_account/topup/TopupInfoActivity;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/create_account/topup/TopupInfoActivity;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lco/uk/getmondo/create_account/topup/TopupInfoActivity_ViewBinding;->a:Lco/uk/getmondo/create_account/topup/TopupInfoActivity;

    .line 26
    const v0, 0x7f1101e2

    const-string v1, "field \'titleTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/create_account/topup/TopupInfoActivity;->titleTextView:Landroid/widget/TextView;

    .line 27
    const v0, 0x7f11024d

    const-string v1, "field \'topupButton\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lco/uk/getmondo/create_account/topup/TopupInfoActivity;->topupButton:Landroid/view/View;

    .line 28
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/TopupInfoActivity_ViewBinding;->a:Lco/uk/getmondo/create_account/topup/TopupInfoActivity;

    .line 34
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/create_account/topup/TopupInfoActivity_ViewBinding;->a:Lco/uk/getmondo/create_account/topup/TopupInfoActivity;

    .line 37
    iput-object v1, v0, Lco/uk/getmondo/create_account/topup/TopupInfoActivity;->titleTextView:Landroid/widget/TextView;

    .line 38
    iput-object v1, v0, Lco/uk/getmondo/create_account/topup/TopupInfoActivity;->topupButton:Landroid/view/View;

    .line 39
    return-void
.end method
