.class public Lco/uk/getmondo/create_account/topup/g;
.super Lco/uk/getmondo/common/ui/b;
.source "InitialTopupPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/create_account/topup/g$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/create_account/topup/g$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/common/accounts/d;

.field private final f:Lco/uk/getmondo/common/e/a;

.field private final g:Lco/uk/getmondo/common/q;

.field private final h:Lco/uk/getmondo/create_account/topup/o;

.field private final i:Lco/uk/getmondo/common/a;

.field private final j:Lco/uk/getmondo/common/m;

.field private final k:Lco/uk/getmondo/create_account/topup/c;


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/common/q;Lco/uk/getmondo/create_account/topup/o;Lco/uk/getmondo/common/a;Lco/uk/getmondo/common/m;Lco/uk/getmondo/create_account/topup/c;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 48
    iput-object p1, p0, Lco/uk/getmondo/create_account/topup/g;->c:Lio/reactivex/u;

    .line 49
    iput-object p2, p0, Lco/uk/getmondo/create_account/topup/g;->d:Lio/reactivex/u;

    .line 50
    iput-object p3, p0, Lco/uk/getmondo/create_account/topup/g;->e:Lco/uk/getmondo/common/accounts/d;

    .line 51
    iput-object p4, p0, Lco/uk/getmondo/create_account/topup/g;->f:Lco/uk/getmondo/common/e/a;

    .line 52
    iput-object p5, p0, Lco/uk/getmondo/create_account/topup/g;->g:Lco/uk/getmondo/common/q;

    .line 53
    iput-object p6, p0, Lco/uk/getmondo/create_account/topup/g;->h:Lco/uk/getmondo/create_account/topup/o;

    .line 54
    iput-object p7, p0, Lco/uk/getmondo/create_account/topup/g;->i:Lco/uk/getmondo/common/a;

    .line 55
    iput-object p8, p0, Lco/uk/getmondo/create_account/topup/g;->j:Lco/uk/getmondo/common/m;

    .line 56
    iput-object p9, p0, Lco/uk/getmondo/create_account/topup/g;->k:Lco/uk/getmondo/create_account/topup/c;

    .line 57
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/topup/g;)V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/g;->i:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;->INITIAL_TOPUP:Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;

    invoke-static {v1}, Lco/uk/getmondo/api/model/tracking/Impression;->a(Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 69
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/g;->g:Lco/uk/getmondo/common/q;

    invoke-virtual {v0}, Lco/uk/getmondo/common/q;->a()V

    .line 70
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/topup/g;Lco/uk/getmondo/create_account/topup/g$a;Ljava/lang/Long;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/g;->h:Lco/uk/getmondo/create_account/topup/o;

    invoke-virtual {v0}, Lco/uk/getmondo/create_account/topup/o;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/create_account/topup/g;->h:Lco/uk/getmondo/create_account/topup/o;

    .line 67
    invoke-virtual {v1}, Lco/uk/getmondo/create_account/topup/o;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lco/uk/getmondo/create_account/topup/m;->a(Lco/uk/getmondo/create_account/topup/g;)Lco/uk/getmondo/common/f/c$a;

    move-result-object v2

    .line 66
    invoke-interface {p1, v0, v1, v2}, Lco/uk/getmondo/create_account/topup/g$a;->a(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/common/f/c$a;)V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/topup/g;Ljava/lang/Throwable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 82
    iget-object v1, p0, Lco/uk/getmondo/create_account/topup/g;->f:Lco/uk/getmondo/common/e/a;

    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/g;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/common/e/a$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/topup/g;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lco/uk/getmondo/create_account/topup/g;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    .line 90
    if-eqz p1, :cond_0

    .line 91
    invoke-virtual {p0}, Lco/uk/getmondo/create_account/topup/g;->a()V

    .line 96
    :goto_0
    return-void

    .line 93
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/g;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/create_account/topup/g$a;

    iget-object v1, p0, Lco/uk/getmondo/create_account/topup/g;->h:Lco/uk/getmondo/create_account/topup/o;

    invoke-virtual {v1}, Lco/uk/getmondo/create_account/topup/o;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/create_account/topup/g;->h:Lco/uk/getmondo/create_account/topup/o;

    .line 94
    invoke-virtual {v2}, Lco/uk/getmondo/create_account/topup/o;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/create_account/topup/g;->g:Lco/uk/getmondo/common/q;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v3}, Lco/uk/getmondo/create_account/topup/l;->a(Lco/uk/getmondo/common/q;)Lco/uk/getmondo/common/f/c$a;

    move-result-object v3

    .line 93
    invoke-interface {v0, v1, v2, v3}, Lco/uk/getmondo/create_account/topup/g$a;->a(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/common/f/c$a;)V

    goto :goto_0
.end method


# virtual methods
.method a()V
    .locals 3

    .prologue
    .line 99
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/g;->e:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->c()Lco/uk/getmondo/d/a;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/ab;

    .line 101
    iget-object v1, p0, Lco/uk/getmondo/create_account/topup/g;->e:Lco/uk/getmondo/common/accounts/d;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lco/uk/getmondo/d/ab;->b(Z)Lco/uk/getmondo/d/a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/accounts/d;->a(Lco/uk/getmondo/d/a;)V

    .line 102
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/g;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/create_account/topup/g$a;

    invoke-interface {v0}, Lco/uk/getmondo/create_account/topup/g$a;->a()V

    .line 103
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/g;->j:Lco/uk/getmondo/common/m;

    invoke-virtual {v0}, Lco/uk/getmondo/common/m;->b()V

    .line 104
    return-void
.end method

.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 30
    check-cast p1, Lco/uk/getmondo/create_account/topup/g$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/create_account/topup/g;->a(Lco/uk/getmondo/create_account/topup/g$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/create_account/topup/g$a;)V
    .locals 3

    .prologue
    .line 61
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 63
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/g;->i:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->z()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 64
    const-wide/16 v0, 0x1e

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v1, v2}, Lio/reactivex/n;->timer(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/n;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/create_account/topup/g;->c:Lio/reactivex/u;

    .line 65
    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/create_account/topup/h;->a(Lco/uk/getmondo/create_account/topup/g;Lco/uk/getmondo/create_account/topup/g$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/create_account/topup/i;->a()Lio/reactivex/c/g;

    move-result-object v2

    .line 66
    invoke-virtual {v0, v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 64
    invoke-virtual {p0, v0}, Lco/uk/getmondo/create_account/topup/g;->a(Lio/reactivex/b/b;)V

    .line 71
    return-void
.end method

.method a(Ljava/lang/Throwable;)Z
    .locals 3

    .prologue
    .line 76
    instance-of v0, p1, Lco/uk/getmondo/api/ApiException;

    if-eqz v0, :cond_0

    check-cast p1, Lco/uk/getmondo/api/ApiException;

    invoke-virtual {p1}, Lco/uk/getmondo/api/ApiException;->e()Lco/uk/getmondo/api/model/b;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 77
    invoke-static {}, Lco/uk/getmondo/topup/a/v;->values()[Lco/uk/getmondo/topup/a/v;

    move-result-object v1

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lco/uk/getmondo/common/e/d;->a([Lco/uk/getmondo/common/e/f;Ljava/lang/String;)Lco/uk/getmondo/common/e/f;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/topup/a/v;

    .line 78
    sget-object v1, Lco/uk/getmondo/topup/a/v;->g:Lco/uk/getmondo/topup/a/v;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/topup/a/v;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/g;->k:Lco/uk/getmondo/create_account/topup/c;

    invoke-virtual {v0}, Lco/uk/getmondo/create_account/topup/c;->a()Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/create_account/topup/g;->d:Lio/reactivex/u;

    .line 80
    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/create_account/topup/g;->c:Lio/reactivex/u;

    .line 81
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/create_account/topup/j;->a(Lco/uk/getmondo/create_account/topup/g;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {p0}, Lco/uk/getmondo/create_account/topup/k;->a(Lco/uk/getmondo/create_account/topup/g;)Lio/reactivex/c/g;

    move-result-object v2

    .line 82
    invoke-virtual {v0, v1, v2}, Lio/reactivex/v;->a(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 79
    invoke-virtual {p0, v0}, Lco/uk/getmondo/create_account/topup/g;->a(Lio/reactivex/b/b;)V

    .line 83
    const/4 v0, 0x1

    .line 86
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
