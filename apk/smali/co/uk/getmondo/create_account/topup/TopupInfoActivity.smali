.class public Lco/uk/getmondo/create_account/topup/TopupInfoActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "TopupInfoActivity.java"

# interfaces
.implements Lco/uk/getmondo/create_account/topup/r$a;


# instance fields
.field a:Lco/uk/getmondo/create_account/topup/r;

.field titleTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101e2
    .end annotation
.end field

.field topupButton:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11024d
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/TopupInfoActivity;->topupButton:Landroid/view/View;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public a(Lco/uk/getmondo/d/c;)V
    .locals 0

    .prologue
    .line 61
    invoke-static {p0, p1}, Lco/uk/getmondo/create_account/topup/InitialTopupActivity;->a(Landroid/content/Context;Lco/uk/getmondo/d/c;)V

    .line 62
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/TopupInfoActivity;->topupButton:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 77
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->a(Landroid/content/Context;Z)V

    .line 67
    return-void
.end method

.method public b(Lco/uk/getmondo/d/c;)V
    .locals 5

    .prologue
    .line 81
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/TopupInfoActivity;->titleTextView:Landroid/widget/TextView;

    const v1, 0x7f0a03db

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lco/uk/getmondo/d/c;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lco/uk/getmondo/create_account/topup/TopupInfoActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 71
    invoke-static {p0}, Lco/uk/getmondo/main/HomeActivity;->a(Landroid/content/Context;)V

    .line 72
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetTextI18n"
        }
    .end annotation

    .prologue
    .line 39
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 41
    const v0, 0x7f05006d

    invoke-virtual {p0, v0}, Lco/uk/getmondo/create_account/topup/TopupInfoActivity;->setContentView(I)V

    .line 42
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 43
    invoke-virtual {p0}, Lco/uk/getmondo/create_account/topup/TopupInfoActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/create_account/topup/TopupInfoActivity;)V

    .line 45
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/TopupInfoActivity;->a:Lco/uk/getmondo/create_account/topup/r;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/create_account/topup/r;->a(Lco/uk/getmondo/create_account/topup/r$a;)V

    .line 46
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 50
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 51
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/TopupInfoActivity;->a:Lco/uk/getmondo/create_account/topup/r;

    invoke-virtual {v0}, Lco/uk/getmondo/create_account/topup/r;->b()V

    .line 52
    return-void
.end method
