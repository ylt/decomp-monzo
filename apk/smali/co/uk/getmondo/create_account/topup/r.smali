.class public Lco/uk/getmondo/create_account/topup/r;
.super Lco/uk/getmondo/common/ui/b;
.source "TopupInfoPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/create_account/topup/r$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/create_account/topup/r$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/common/e/a;

.field private final f:Lco/uk/getmondo/common/a;

.field private final g:Lco/uk/getmondo/api/b/a;

.field private final h:Lco/uk/getmondo/create_account/topup/c;

.field private i:Lco/uk/getmondo/d/c;


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/common/a;Lco/uk/getmondo/api/b/a;Lco/uk/getmondo/create_account/topup/c;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 33
    iput-object p1, p0, Lco/uk/getmondo/create_account/topup/r;->c:Lio/reactivex/u;

    .line 34
    iput-object p2, p0, Lco/uk/getmondo/create_account/topup/r;->d:Lio/reactivex/u;

    .line 35
    iput-object p3, p0, Lco/uk/getmondo/create_account/topup/r;->e:Lco/uk/getmondo/common/e/a;

    .line 36
    iput-object p4, p0, Lco/uk/getmondo/create_account/topup/r;->f:Lco/uk/getmondo/common/a;

    .line 37
    iput-object p5, p0, Lco/uk/getmondo/create_account/topup/r;->g:Lco/uk/getmondo/api/b/a;

    .line 38
    iput-object p6, p0, Lco/uk/getmondo/create_account/topup/r;->h:Lco/uk/getmondo/create_account/topup/c;

    .line 39
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/topup/r;Lco/uk/getmondo/create_account/topup/r$a;Lco/uk/getmondo/d/ak;)Lio/reactivex/z;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 55
    sget-object v0, Lco/uk/getmondo/create_account/topup/r$1;->a:[I

    invoke-virtual {p2}, Lco/uk/getmondo/d/ak;->a()Lco/uk/getmondo/d/ak$a;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/d/ak$a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 63
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/r;->h:Lco/uk/getmondo/create_account/topup/c;

    invoke-virtual {v0}, Lco/uk/getmondo/create_account/topup/c;->b()Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/create_account/topup/r;->d:Lio/reactivex/u;

    .line 64
    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/create_account/topup/r;->c:Lio/reactivex/u;

    .line 65
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    .line 63
    :goto_0
    return-object v0

    .line 57
    :pswitch_0
    invoke-interface {p1}, Lco/uk/getmondo/create_account/topup/r$a;->b()V

    .line 58
    invoke-static {}, Lio/reactivex/v;->o_()Lio/reactivex/v;

    move-result-object v0

    goto :goto_0

    .line 60
    :pswitch_1
    invoke-interface {p1}, Lco/uk/getmondo/create_account/topup/r$a;->c()V

    .line 61
    invoke-static {}, Lio/reactivex/v;->o_()Lio/reactivex/v;

    move-result-object v0

    goto :goto_0

    .line 55
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/topup/r$a;Lio/reactivex/b/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Lco/uk/getmondo/create_account/topup/r$a;->a(Z)V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/topup/r;Lco/uk/getmondo/create_account/topup/r$a;Lco/uk/getmondo/d/c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 69
    iput-object p2, p0, Lco/uk/getmondo/create_account/topup/r;->i:Lco/uk/getmondo/d/c;

    .line 70
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/r;->i:Lco/uk/getmondo/d/c;

    invoke-interface {p1, v0}, Lco/uk/getmondo/create_account/topup/r$a;->b(Lco/uk/getmondo/d/c;)V

    .line 71
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lco/uk/getmondo/create_account/topup/r$a;->a(Z)V

    .line 72
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/topup/r;Lco/uk/getmondo/create_account/topup/r$a;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/r;->i:Lco/uk/getmondo/d/c;

    invoke-interface {p1, v0}, Lco/uk/getmondo/create_account/topup/r$a;->a(Lco/uk/getmondo/d/c;)V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/topup/r;Lco/uk/getmondo/create_account/topup/r$a;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/r;->e:Lco/uk/getmondo/common/e/a;

    invoke-virtual {v0, p2, p1}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/topup/r;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/r;->i:Lco/uk/getmondo/d/c;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Lco/uk/getmondo/create_account/topup/r$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/create_account/topup/r;->a(Lco/uk/getmondo/create_account/topup/r$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/create_account/topup/r$a;)V
    .locals 3

    .prologue
    .line 43
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 44
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/r;->f:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->y()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 46
    invoke-interface {p1}, Lco/uk/getmondo/create_account/topup/r$a;->a()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/create_account/topup/s;->a(Lco/uk/getmondo/create_account/topup/r;)Lio/reactivex/c/q;

    move-result-object v1

    .line 47
    invoke-virtual {v0, v1}, Lio/reactivex/n;->skipWhile(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/create_account/topup/t;->a(Lco/uk/getmondo/create_account/topup/r;Lco/uk/getmondo/create_account/topup/r$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 48
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 46
    invoke-virtual {p0, v0}, Lco/uk/getmondo/create_account/topup/r;->a(Lio/reactivex/b/b;)V

    .line 50
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/r;->g:Lco/uk/getmondo/api/b/a;

    invoke-virtual {v0}, Lco/uk/getmondo/api/b/a;->a()Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/create_account/topup/r;->d:Lio/reactivex/u;

    .line 51
    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/create_account/topup/r;->c:Lio/reactivex/u;

    .line 52
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/create_account/topup/u;->a(Lco/uk/getmondo/create_account/topup/r$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 53
    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/create_account/topup/v;->a(Lco/uk/getmondo/create_account/topup/r;Lco/uk/getmondo/create_account/topup/r$a;)Lio/reactivex/c/h;

    move-result-object v1

    .line 54
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/create_account/topup/w;->a(Lco/uk/getmondo/create_account/topup/r;Lco/uk/getmondo/create_account/topup/r$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {p0, p1}, Lco/uk/getmondo/create_account/topup/x;->a(Lco/uk/getmondo/create_account/topup/r;Lco/uk/getmondo/create_account/topup/r$a;)Lio/reactivex/c/g;

    move-result-object v2

    .line 68
    invoke-virtual {v0, v1, v2}, Lio/reactivex/v;->a(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 50
    invoke-virtual {p0, v0}, Lco/uk/getmondo/create_account/topup/r;->a(Lio/reactivex/b/b;)V

    .line 74
    return-void
.end method
