.class public final Lco/uk/getmondo/create_account/topup/n;
.super Ljava/lang/Object;
.source "InitialTopupPresenter_Factory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/create_account/topup/g;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/create_account/topup/g;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/accounts/d;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/e/a;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/q;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/create_account/topup/o;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/m;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/create_account/topup/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lco/uk/getmondo/create_account/topup/n;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/create_account/topup/n;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/create_account/topup/g;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/accounts/d;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/e/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/q;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/create_account/topup/o;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/m;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/create_account/topup/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    sget-boolean v0, Lco/uk/getmondo/create_account/topup/n;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 52
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/create_account/topup/n;->b:Lb/a;

    .line 53
    sget-boolean v0, Lco/uk/getmondo/create_account/topup/n;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 54
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/create_account/topup/n;->c:Ljavax/a/a;

    .line 55
    sget-boolean v0, Lco/uk/getmondo/create_account/topup/n;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 56
    :cond_2
    iput-object p3, p0, Lco/uk/getmondo/create_account/topup/n;->d:Ljavax/a/a;

    .line 57
    sget-boolean v0, Lco/uk/getmondo/create_account/topup/n;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 58
    :cond_3
    iput-object p4, p0, Lco/uk/getmondo/create_account/topup/n;->e:Ljavax/a/a;

    .line 59
    sget-boolean v0, Lco/uk/getmondo/create_account/topup/n;->a:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 60
    :cond_4
    iput-object p5, p0, Lco/uk/getmondo/create_account/topup/n;->f:Ljavax/a/a;

    .line 61
    sget-boolean v0, Lco/uk/getmondo/create_account/topup/n;->a:Z

    if-nez v0, :cond_5

    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 62
    :cond_5
    iput-object p6, p0, Lco/uk/getmondo/create_account/topup/n;->g:Ljavax/a/a;

    .line 63
    sget-boolean v0, Lco/uk/getmondo/create_account/topup/n;->a:Z

    if-nez v0, :cond_6

    if-nez p7, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 64
    :cond_6
    iput-object p7, p0, Lco/uk/getmondo/create_account/topup/n;->h:Ljavax/a/a;

    .line 65
    sget-boolean v0, Lco/uk/getmondo/create_account/topup/n;->a:Z

    if-nez v0, :cond_7

    if-nez p8, :cond_7

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 66
    :cond_7
    iput-object p8, p0, Lco/uk/getmondo/create_account/topup/n;->i:Ljavax/a/a;

    .line 67
    sget-boolean v0, Lco/uk/getmondo/create_account/topup/n;->a:Z

    if-nez v0, :cond_8

    if-nez p9, :cond_8

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 68
    :cond_8
    iput-object p9, p0, Lco/uk/getmondo/create_account/topup/n;->j:Ljavax/a/a;

    .line 69
    sget-boolean v0, Lco/uk/getmondo/create_account/topup/n;->a:Z

    if-nez v0, :cond_9

    if-nez p10, :cond_9

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 70
    :cond_9
    iput-object p10, p0, Lco/uk/getmondo/create_account/topup/n;->k:Ljavax/a/a;

    .line 71
    return-void
.end method

.method public static a(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/create_account/topup/g;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/accounts/d;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/e/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/q;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/create_account/topup/o;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/m;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/create_account/topup/c;",
            ">;)",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/create_account/topup/g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    new-instance v0, Lco/uk/getmondo/create_account/topup/n;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lco/uk/getmondo/create_account/topup/n;-><init>(Lb/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/create_account/topup/g;
    .locals 11

    .prologue
    .line 75
    iget-object v10, p0, Lco/uk/getmondo/create_account/topup/n;->b:Lb/a;

    new-instance v0, Lco/uk/getmondo/create_account/topup/g;

    iget-object v1, p0, Lco/uk/getmondo/create_account/topup/n;->c:Ljavax/a/a;

    .line 78
    invoke-interface {v1}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/reactivex/u;

    iget-object v2, p0, Lco/uk/getmondo/create_account/topup/n;->d:Ljavax/a/a;

    .line 79
    invoke-interface {v2}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/reactivex/u;

    iget-object v3, p0, Lco/uk/getmondo/create_account/topup/n;->e:Ljavax/a/a;

    .line 80
    invoke-interface {v3}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lco/uk/getmondo/common/accounts/d;

    iget-object v4, p0, Lco/uk/getmondo/create_account/topup/n;->f:Ljavax/a/a;

    .line 81
    invoke-interface {v4}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lco/uk/getmondo/common/e/a;

    iget-object v5, p0, Lco/uk/getmondo/create_account/topup/n;->g:Ljavax/a/a;

    .line 82
    invoke-interface {v5}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lco/uk/getmondo/common/q;

    iget-object v6, p0, Lco/uk/getmondo/create_account/topup/n;->h:Ljavax/a/a;

    .line 83
    invoke-interface {v6}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lco/uk/getmondo/create_account/topup/o;

    iget-object v7, p0, Lco/uk/getmondo/create_account/topup/n;->i:Ljavax/a/a;

    .line 84
    invoke-interface {v7}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lco/uk/getmondo/common/a;

    iget-object v8, p0, Lco/uk/getmondo/create_account/topup/n;->j:Ljavax/a/a;

    .line 85
    invoke-interface {v8}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lco/uk/getmondo/common/m;

    iget-object v9, p0, Lco/uk/getmondo/create_account/topup/n;->k:Ljavax/a/a;

    .line 86
    invoke-interface {v9}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lco/uk/getmondo/create_account/topup/c;

    invoke-direct/range {v0 .. v9}, Lco/uk/getmondo/create_account/topup/g;-><init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/common/q;Lco/uk/getmondo/create_account/topup/o;Lco/uk/getmondo/common/a;Lco/uk/getmondo/common/m;Lco/uk/getmondo/create_account/topup/c;)V

    .line 75
    invoke-static {v10, v0}, Lb/a/c;->a(Lb/a;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/create_account/topup/g;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lco/uk/getmondo/create_account/topup/n;->a()Lco/uk/getmondo/create_account/topup/g;

    move-result-object v0

    return-object v0
.end method
