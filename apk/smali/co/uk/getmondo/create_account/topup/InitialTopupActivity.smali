.class public Lco/uk/getmondo/create_account/topup/InitialTopupActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "InitialTopupActivity.java"

# interfaces
.implements Lco/uk/getmondo/create_account/topup/g$a;
.implements Lco/uk/getmondo/topup/card/TopUpWithCardFragment$a;


# instance fields
.field a:Lco/uk/getmondo/create_account/topup/g;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lco/uk/getmondo/d/c;)V
    .locals 2

    .prologue
    .line 22
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/create_account/topup/InitialTopupActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 23
    const-string v1, "EXTRA_TOPUP_AMOUNT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 24
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 25
    return-void
.end method

.method static synthetic a(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/common/f/c$a;Lco/uk/getmondo/common/f/c;)V
    .locals 0

    .prologue
    .line 57
    invoke-virtual {p3, p0, p1, p2}, Lco/uk/getmondo/common/f/c;->a(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/common/f/c$a;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lco/uk/getmondo/create_account/wait/CardShippedActivity;->a(Landroid/content/Context;Z)V

    .line 53
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/common/f/c$a;)V
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/InitialTopupActivity;->d:Lcom/c/b/b;

    invoke-static {p1, p2, p3}, Lco/uk/getmondo/create_account/topup/a;->a(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/common/f/c$a;)Lcom/c/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/c/b/b;->a(Lcom/c/a/a;)Lcom/c/b/b;

    .line 58
    return-void
.end method

.method public a(Ljava/lang/Throwable;)Z
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/InitialTopupActivity;->a:Lco/uk/getmondo/create_account/topup/g;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/create_account/topup/g;->a(Ljava/lang/Throwable;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/InitialTopupActivity;->a:Lco/uk/getmondo/create_account/topup/g;

    invoke-virtual {v0}, Lco/uk/getmondo/create_account/topup/g;->a()V

    .line 63
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const v6, 0x1020002

    const/4 v5, 0x1

    .line 29
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 30
    invoke-virtual {p0}, Lco/uk/getmondo/create_account/topup/InitialTopupActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/create_account/topup/InitialTopupActivity;)V

    .line 31
    invoke-virtual {p0}, Lco/uk/getmondo/create_account/topup/InitialTopupActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_TOPUP_AMOUNT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/c;

    .line 32
    const v1, 0x7f0a03d9

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lco/uk/getmondo/d/c;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lco/uk/getmondo/create_account/topup/InitialTopupActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lco/uk/getmondo/create_account/topup/InitialTopupActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 34
    invoke-virtual {p0}, Lco/uk/getmondo/create_account/topup/InitialTopupActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/support/v4/app/n;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_0

    .line 35
    invoke-virtual {p0}, Lco/uk/getmondo/create_account/topup/InitialTopupActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    .line 36
    invoke-virtual {v1}, Landroid/support/v4/app/n;->a()Landroid/support/v4/app/t;

    move-result-object v1

    .line 37
    invoke-static {v0, v5}, Lco/uk/getmondo/topup/card/TopUpWithCardFragment;->a(Lco/uk/getmondo/d/c;Z)Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {v1, v6, v0}, Landroid/support/v4/app/t;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/t;

    move-result-object v0

    .line 38
    invoke-virtual {v0}, Landroid/support/v4/app/t;->c()I

    .line 41
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/InitialTopupActivity;->a:Lco/uk/getmondo/create_account/topup/g;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/create_account/topup/g;->a(Lco/uk/getmondo/create_account/topup/g$a;)V

    .line 42
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 46
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 47
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/InitialTopupActivity;->a:Lco/uk/getmondo/create_account/topup/g;

    invoke-virtual {v0}, Lco/uk/getmondo/create_account/topup/g;->b()V

    .line 48
    return-void
.end method
