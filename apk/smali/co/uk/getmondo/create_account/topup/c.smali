.class public Lco/uk/getmondo/create_account/topup/c;
.super Ljava/lang/Object;
.source "InitialTopupManager.java"


# static fields
.field private static final a:Lco/uk/getmondo/d/c;


# instance fields
.field private final b:Lco/uk/getmondo/common/accounts/d;

.field private final c:Lco/uk/getmondo/api/MonzoApi;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 17
    new-instance v0, Lco/uk/getmondo/d/c;

    const-wide/16 v2, 0x2710

    sget-object v1, Lco/uk/getmondo/common/i/c;->a:Lco/uk/getmondo/common/i/c;

    invoke-direct {v0, v2, v3, v1}, Lco/uk/getmondo/d/c;-><init>(JLco/uk/getmondo/common/i/c;)V

    sput-object v0, Lco/uk/getmondo/create_account/topup/c;->a:Lco/uk/getmondo/d/c;

    return-void
.end method

.method public constructor <init>(Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/api/MonzoApi;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lco/uk/getmondo/create_account/topup/c;->b:Lco/uk/getmondo/common/accounts/d;

    .line 25
    iput-object p2, p0, Lco/uk/getmondo/create_account/topup/c;->c:Lco/uk/getmondo/api/MonzoApi;

    .line 26
    return-void
.end method

.method private a(Lco/uk/getmondo/api/model/ApiInitialTopupStatus;)Lco/uk/getmondo/d/c;
    .locals 4

    .prologue
    .line 49
    new-instance v0, Lco/uk/getmondo/d/c;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiInitialTopupStatus;->b()J

    move-result-wide v2

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiInitialTopupStatus;->c()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Lco/uk/getmondo/d/c;-><init>(JLjava/lang/String;)V

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/topup/c;Lco/uk/getmondo/api/model/ApiInitialTopupStatus;)Lco/uk/getmondo/d/c;
    .locals 1

    invoke-direct {p0, p1}, Lco/uk/getmondo/create_account/topup/c;->a(Lco/uk/getmondo/api/model/ApiInitialTopupStatus;)Lco/uk/getmondo/d/c;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/c;->b:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->c()Lco/uk/getmondo/d/a;

    move-result-object v0

    .line 30
    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/c;->c:Lco/uk/getmondo/api/MonzoApi;

    iget-object v1, p0, Lco/uk/getmondo/create_account/topup/c;->b:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v1}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/d/ak;->c()Lco/uk/getmondo/d/a;

    move-result-object v1

    invoke-interface {v1}, Lco/uk/getmondo/d/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lco/uk/getmondo/api/MonzoApi;->getInitialTopupStatus(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/create_account/topup/d;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 32
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    .line 35
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/v;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    goto :goto_0
.end method

.method public b()Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/d/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lco/uk/getmondo/create_account/topup/c;->b:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->c()Lco/uk/getmondo/d/a;

    move-result-object v0

    .line 40
    if-eqz v0, :cond_0

    .line 41
    iget-object v1, p0, Lco/uk/getmondo/create_account/topup/c;->c:Lco/uk/getmondo/api/MonzoApi;

    invoke-interface {v0}, Lco/uk/getmondo/d/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lco/uk/getmondo/api/MonzoApi;->getInitialTopupStatus(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/create_account/topup/e;->a(Lco/uk/getmondo/create_account/topup/c;)Lio/reactivex/c/h;

    move-result-object v1

    .line 42
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    .line 45
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lco/uk/getmondo/create_account/topup/c;->a:Lco/uk/getmondo/d/c;

    invoke-static {v0}, Lio/reactivex/v;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    goto :goto_0
.end method
