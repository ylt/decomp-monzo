.class public final Lco/uk/getmondo/create_account/a/a/a;
.super Ljava/lang/Object;
.source "SignUpRequest.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0010$\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0008H\u00c6\u0003J;\u0010\u0016\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008H\u00c6\u0001J\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00030\u0018J\u0013\u0010\u0019\u001a\u00020\u001a2\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001c\u001a\u00020\u001dH\u00d6\u0001J\t\u0010\u001e\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\r\u00a8\u0006\u001f"
    }
    d2 = {
        "Lco/uk/getmondo/create_account/data/model/SignUpRequest;",
        "",
        "email",
        "",
        "name",
        "dateOfBirth",
        "phoneNumber",
        "address",
        "Lco/uk/getmondo/model/LegacyAddress;",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/model/LegacyAddress;)V",
        "getAddress",
        "()Lco/uk/getmondo/model/LegacyAddress;",
        "getDateOfBirth",
        "()Ljava/lang/String;",
        "getEmail",
        "getName",
        "getPhoneNumber",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "createMap",
        "",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Lco/uk/getmondo/d/s;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/s;)V
    .locals 1

    .prologue
    const-string v0, "email"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateOfBirth"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "phoneNumber"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "address"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/create_account/a/a/a;->a:Ljava/lang/String;

    iput-object p2, p0, Lco/uk/getmondo/create_account/a/a/a;->b:Ljava/lang/String;

    iput-object p3, p0, Lco/uk/getmondo/create_account/a/a/a;->c:Ljava/lang/String;

    iput-object p4, p0, Lco/uk/getmondo/create_account/a/a/a;->d:Ljava/lang/String;

    iput-object p5, p0, Lco/uk/getmondo/create_account/a/a/a;->e:Lco/uk/getmondo/d/s;

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 15
    iget-object v0, p0, Lco/uk/getmondo/create_account/a/a/a;->e:Lco/uk/getmondo/d/s;

    invoke-virtual {v0}, Lco/uk/getmondo/d/s;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 16
    :goto_0
    iget-object v1, p0, Lco/uk/getmondo/create_account/a/a/a;->e:Lco/uk/getmondo/d/s;

    invoke-virtual {v1}, Lco/uk/getmondo/d/s;->i()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 17
    :goto_1
    iget-object v2, p0, Lco/uk/getmondo/create_account/a/a/a;->e:Lco/uk/getmondo/d/s;

    invoke-virtual {v2}, Lco/uk/getmondo/d/s;->c()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 18
    :goto_2
    iget-object v3, p0, Lco/uk/getmondo/create_account/a/a/a;->e:Lco/uk/getmondo/d/s;

    invoke-virtual {v3}, Lco/uk/getmondo/d/s;->j()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 19
    :goto_3
    const/16 v4, 0x8

    new-array v4, v4, [Lkotlin/h;

    .line 20
    const/4 v5, 0x0

    const-string v6, "email"

    iget-object v7, p0, Lco/uk/getmondo/create_account/a/a/a;->a:Ljava/lang/String;

    invoke-static {v6, v7}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v6

    aput-object v6, v4, v5

    .line 21
    const/4 v5, 0x1

    const-string v6, "name"

    iget-object v7, p0, Lco/uk/getmondo/create_account/a/a/a;->b:Ljava/lang/String;

    invoke-static {v6, v7}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v6

    aput-object v6, v4, v5

    .line 22
    const/4 v5, 0x2

    const-string v6, "date_of_birth"

    iget-object v7, p0, Lco/uk/getmondo/create_account/a/a/a;->c:Ljava/lang/String;

    invoke-static {v7}, Lco/uk/getmondo/create_account/b;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-static {v6, v7}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v6

    aput-object v6, v4, v5

    .line 23
    const/4 v5, 0x3

    const-string v6, "phone_number"

    iget-object v7, p0, Lco/uk/getmondo/create_account/a/a/a;->d:Ljava/lang/String;

    invoke-static {v6, v7}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v6

    aput-object v6, v4, v5

    .line 24
    const/4 v5, 0x4

    const-string v6, "address[locality]"

    invoke-static {v6, v1}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v1

    aput-object v1, v4, v5

    .line 25
    const/4 v1, 0x5

    const-string v5, "address[administrative_area]"

    invoke-static {v5, v0}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v0

    aput-object v0, v4, v1

    .line 26
    const/4 v0, 0x6

    const-string v1, "address[postal_code]"

    invoke-static {v1, v2}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v1

    aput-object v1, v4, v0

    .line 27
    const/4 v0, 0x7

    const-string v1, "address[country]"

    invoke-static {v1, v3}, Lkotlin/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/h;

    move-result-object v1

    aput-object v1, v4, v0

    .line 19
    invoke-static {v4}, Lkotlin/a/ab;->a([Lkotlin/h;)Ljava/util/Map;

    move-result-object v0

    return-object v0

    .line 15
    :cond_1
    const-string v0, ""

    goto :goto_0

    .line 16
    :cond_2
    const-string v1, ""

    goto :goto_1

    .line 17
    :cond_3
    const-string v2, ""

    goto :goto_2

    .line 18
    :cond_4
    const-string v3, ""

    goto :goto_3
.end method

.method public final b()Lco/uk/getmondo/d/s;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lco/uk/getmondo/create_account/a/a/a;->e:Lco/uk/getmondo/d/s;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lco/uk/getmondo/create_account/a/a/a;

    if-eqz v0, :cond_1

    check-cast p1, Lco/uk/getmondo/create_account/a/a/a;

    iget-object v0, p0, Lco/uk/getmondo/create_account/a/a/a;->a:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/create_account/a/a/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/create_account/a/a/a;->b:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/create_account/a/a/a;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/create_account/a/a/a;->c:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/create_account/a/a/a;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/create_account/a/a/a;->d:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/create_account/a/a/a;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/create_account/a/a/a;->e:Lco/uk/getmondo/d/s;

    iget-object v1, p1, Lco/uk/getmondo/create_account/a/a/a;->e:Lco/uk/getmondo/d/s;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lco/uk/getmondo/create_account/a/a/a;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/create_account/a/a/a;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/create_account/a/a/a;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/create_account/a/a/a;->d:Ljava/lang/String;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/uk/getmondo/create_account/a/a/a;->e:Lco/uk/getmondo/d/s;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_3
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SignUpRequest(email="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/create_account/a/a/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/create_account/a/a/a;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dateOfBirth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/create_account/a/a/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", phoneNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/create_account/a/a/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", address="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/create_account/a/a/a;->e:Lco/uk/getmondo/d/s;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
