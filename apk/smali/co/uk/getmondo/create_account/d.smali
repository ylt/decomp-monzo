.class final enum Lco/uk/getmondo/create_account/d;
.super Ljava/lang/Enum;
.source "SignUpError.java"

# interfaces
.implements Lco/uk/getmondo/common/e/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/create_account/d;",
        ">;",
        "Lco/uk/getmondo/common/e/f;"
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/create_account/d;

.field private static final synthetic c:[Lco/uk/getmondo/create_account/d;


# instance fields
.field private final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 8
    new-instance v0, Lco/uk/getmondo/create_account/d;

    const-string v1, "PHONE_NUMBER_EXISTS"

    const-string v2, "bad_request.phone_number_already_exists"

    invoke-direct {v0, v1, v3, v2}, Lco/uk/getmondo/create_account/d;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/create_account/d;->a:Lco/uk/getmondo/create_account/d;

    .line 7
    const/4 v0, 0x1

    new-array v0, v0, [Lco/uk/getmondo/create_account/d;

    sget-object v1, Lco/uk/getmondo/create_account/d;->a:Lco/uk/getmondo/create_account/d;

    aput-object v1, v0, v3

    sput-object v0, Lco/uk/getmondo/create_account/d;->c:[Lco/uk/getmondo/create_account/d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 13
    iput-object p3, p0, Lco/uk/getmondo/create_account/d;->b:Ljava/lang/String;

    .line 14
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/create_account/d;
    .locals 1

    .prologue
    .line 7
    const-class v0, Lco/uk/getmondo/create_account/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/create_account/d;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/create_account/d;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lco/uk/getmondo/create_account/d;->c:[Lco/uk/getmondo/create_account/d;

    invoke-virtual {v0}, [Lco/uk/getmondo/create_account/d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/create_account/d;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lco/uk/getmondo/create_account/d;->b:Ljava/lang/String;

    return-object v0
.end method
