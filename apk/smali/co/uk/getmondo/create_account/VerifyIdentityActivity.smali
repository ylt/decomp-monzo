.class public Lco/uk/getmondo/create_account/VerifyIdentityActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "VerifyIdentityActivity.java"

# interfaces
.implements Lco/uk/getmondo/create_account/g$a;


# instance fields
.field a:Lco/uk/getmondo/create_account/g;

.field addressInput:Landroid/widget/EditText;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11026d
    .end annotation
.end field

.field private final b:Landroid/os/Handler;

.field private final c:Ljava/lang/Runnable;

.field dobInput:Landroid/widget/EditText;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11026c
    .end annotation
.end field

.field dobWrapper:Landroid/support/design/widget/TextInputLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11026b
    .end annotation
.end field

.field nameInput:Landroid/widget/EditText;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110142
    .end annotation
.end field

.field nameWrapper:Landroid/support/design/widget/TextInputLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110141
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 31
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->b:Landroid/os/Handler;

    .line 32
    invoke-static {p0}, Lco/uk/getmondo/create_account/e;->a(Lco/uk/getmondo/create_account/VerifyIdentityActivity;)Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->c:Ljava/lang/Runnable;

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 46
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/create_account/VerifyIdentityActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const v1, 0x10008000

    .line 47
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 46
    return-object v0
.end method

.method private j()V
    .locals 3

    .prologue
    .line 95
    new-instance v0, Lco/uk/getmondo/signup_old/r;

    invoke-direct {v0}, Lco/uk/getmondo/signup_old/r;-><init>()V

    .line 96
    iget-object v1, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->dobInput:Landroid/widget/EditText;

    new-instance v2, Lco/uk/getmondo/create_account/VerifyIdentityActivity$1;

    invoke-direct {v2, p0, v0}, Lco/uk/getmondo/create_account/VerifyIdentityActivity$1;-><init>(Lco/uk/getmondo/create_account/VerifyIdentityActivity;Lco/uk/getmondo/signup_old/r;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 110
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->dobWrapper:Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 145
    iget-object v0, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->dobWrapper:Landroid/support/design/widget/TextInputLayout;

    const v1, 0x7f0a0457

    invoke-virtual {p0, v1}, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 146
    return-void
.end method

.method public a(Lco/uk/getmondo/d/s;)V
    .locals 2

    .prologue
    .line 127
    const v0, 0x7f0a02be

    .line 128
    invoke-virtual {p0, v0}, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/common/activities/b$a;->a:Lco/uk/getmondo/common/activities/b$a;

    invoke-static {p0, v0, v1, p1}, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->a(Landroid/content/Context;Ljava/lang/String;Lco/uk/getmondo/common/activities/b$a;Lco/uk/getmondo/d/s;)Landroid/content/Intent;

    move-result-object v0

    .line 129
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 130
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->nameInput:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 135
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->dobWrapper:Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 151
    iget-object v0, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->dobWrapper:Landroid/support/design/widget/TextInputLayout;

    const v1, 0x7f0a0164

    invoke-virtual {p0, v1}, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 152
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    .line 156
    const v0, 0x7f0a0434

    invoke-virtual {p0, v0}, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a0172

    invoke-virtual {p0, v1}, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lco/uk/getmondo/common/d/a;->a(Ljava/lang/String;Ljava/lang/String;Z)Lco/uk/getmondo/common/d/a;

    move-result-object v0

    .line 157
    invoke-virtual {p0}, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    const-string v2, "DUPLICATE_NUMBER"

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/common/d/a;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 158
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->nameWrapper:Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 163
    iget-object v0, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->nameWrapper:Landroid/support/design/widget/TextInputLayout;

    const v1, 0x7f0a02af

    invoke-virtual {p0, v1}, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 164
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->dobInput:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 140
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->dobWrapper:Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 169
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->addressInput:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 179
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->nameWrapper:Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 174
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 183
    sget-object v0, Lco/uk/getmondo/signup/identity_verification/a/j;->b:Lco/uk/getmondo/signup/identity_verification/a/j;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;->SIGNUP:Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;

    invoke-static {p0, v0, v1}, Lco/uk/getmondo/signup/identity_verification/VerificationPendingActivity;->a(Landroid/content/Context;Lco/uk/getmondo/signup/identity_verification/a/j;Lco/uk/getmondo/api/model/tracking/Impression$KycFrom;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->startActivity(Landroid/content/Intent;)V

    .line 184
    invoke-virtual {p0}, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->finishAffinity()V

    .line 185
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->b:Landroid/os/Handler;

    iget-object v1, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 190
    invoke-virtual {p0}, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->t()V

    .line 191
    return-void
.end method

.method public i()V
    .locals 4

    .prologue
    .line 195
    iget-object v0, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->b:Landroid/os/Handler;

    iget-object v1, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->c:Ljava/lang/Runnable;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 196
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 200
    invoke-super {p0, p1, p2, p3}, Lco/uk/getmondo/common/activities/b;->onActivityResult(IILandroid/content/Intent;)V

    .line 201
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 202
    invoke-static {p3}, Lco/uk/getmondo/common/address/LegacyEnterAddressActivity;->a(Landroid/content/Intent;)Lco/uk/getmondo/d/s;

    move-result-object v0

    .line 203
    iget-object v1, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->a:Lco/uk/getmondo/create_account/g;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/create_account/g;->a(Lco/uk/getmondo/d/s;)V

    .line 205
    :cond_0
    return-void
.end method

.method onAddressFocusChange(Z)V
    .locals 1
    .annotation build Lbutterknife/OnFocusChange;
        value = {
            0x7f11026d
        }
    .end annotation

    .prologue
    .line 119
    if-eqz p1, :cond_0

    .line 120
    iget-object v0, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->a:Lco/uk/getmondo/create_account/g;

    invoke-virtual {v0}, Lco/uk/getmondo/create_account/g;->a()V

    .line 121
    iget-object v0, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->addressInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 123
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetTextI18n"
        }
    .end annotation

    .prologue
    .line 53
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 54
    const v0, 0x7f050073

    invoke-virtual {p0, v0}, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->setContentView(I)V

    .line 55
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 57
    invoke-virtual {p0}, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/create_account/VerifyIdentityActivity;)V

    .line 58
    invoke-direct {p0}, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->j()V

    .line 61
    invoke-virtual {p0}, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->getSupportActionBar()Landroid/support/v7/app/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->b(Z)V

    .line 62
    const v0, 0x7f0a01d7

    invoke-virtual {p0, v0}, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 63
    iget-object v0, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->a:Lco/uk/getmondo/create_account/g;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/create_account/g;->a(Lco/uk/getmondo/create_account/g$a;)V

    .line 64
    return-void
.end method

.method onDateFocusChange(Z)V
    .locals 2
    .annotation build Lbutterknife/OnFocusChange;
        value = {
            0x7f11026c
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->dobInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 75
    if-eqz p1, :cond_0

    .line 76
    iget-object v1, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->dobInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 78
    :cond_0
    return-void
.end method

.method onDateOfBirthFocusChange(Z)V
    .locals 1
    .annotation build Lbutterknife/OnFocusChange;
        value = {
            0x7f11026c
        }
    .end annotation

    .prologue
    .line 89
    if-eqz p1, :cond_0

    .line 90
    iget-object v0, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->a:Lco/uk/getmondo/create_account/g;

    invoke-virtual {v0}, Lco/uk/getmondo/create_account/g;->d()V

    .line 92
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->a:Lco/uk/getmondo/create_account/g;

    invoke-virtual {v0}, Lco/uk/getmondo/create_account/g;->b()V

    .line 69
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 70
    return-void
.end method

.method onDetailsCorrectClick()V
    .locals 3
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f11026e
        }
    .end annotation

    .prologue
    .line 114
    iget-object v0, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->a:Lco/uk/getmondo/create_account/g;

    iget-object v1, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->nameInput:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->dobInput:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/create_account/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    return-void
.end method

.method onNameFocusChange(Z)V
    .locals 1
    .annotation build Lbutterknife/OnFocusChange;
        value = {
            0x7f110142
        }
    .end annotation

    .prologue
    .line 82
    if-eqz p1, :cond_0

    .line 83
    iget-object v0, p0, Lco/uk/getmondo/create_account/VerifyIdentityActivity;->a:Lco/uk/getmondo/create_account/g;

    invoke-virtual {v0}, Lco/uk/getmondo/create_account/g;->c()V

    .line 85
    :cond_0
    return-void
.end method
