.class public Lco/uk/getmondo/create_account/g;
.super Lco/uk/getmondo/common/ui/b;
.source "VerifyIdentityPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/create_account/g$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/create_account/g$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/common/accounts/d;

.field private final f:Lco/uk/getmondo/common/e/a;

.field private final g:Lco/uk/getmondo/api/b/a;

.field private final h:Lco/uk/getmondo/common/a;


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/api/b/a;Lco/uk/getmondo/common/a;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 41
    iput-object p1, p0, Lco/uk/getmondo/create_account/g;->c:Lio/reactivex/u;

    .line 42
    iput-object p2, p0, Lco/uk/getmondo/create_account/g;->d:Lio/reactivex/u;

    .line 43
    iput-object p3, p0, Lco/uk/getmondo/create_account/g;->e:Lco/uk/getmondo/common/accounts/d;

    .line 44
    iput-object p4, p0, Lco/uk/getmondo/create_account/g;->f:Lco/uk/getmondo/common/e/a;

    .line 45
    iput-object p5, p0, Lco/uk/getmondo/create_account/g;->g:Lco/uk/getmondo/api/b/a;

    .line 46
    iput-object p6, p0, Lco/uk/getmondo/create_account/g;->h:Lco/uk/getmondo/common/a;

    .line 47
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/g;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lco/uk/getmondo/create_account/g;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/create_account/g$a;

    invoke-interface {v0}, Lco/uk/getmondo/create_account/g$a;->g()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/create_account/g;Ljava/lang/Throwable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 105
    instance-of v0, p1, Lco/uk/getmondo/api/ApiException;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 106
    check-cast v0, Lco/uk/getmondo/api/ApiException;

    invoke-virtual {v0}, Lco/uk/getmondo/api/ApiException;->e()Lco/uk/getmondo/api/model/b;

    move-result-object v0

    .line 107
    invoke-static {}, Lco/uk/getmondo/create_account/d;->values()[Lco/uk/getmondo/create_account/d;

    move-result-object v1

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lco/uk/getmondo/common/e/d;->a([Lco/uk/getmondo/common/e/f;Ljava/lang/String;)Lco/uk/getmondo/common/e/f;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/create_account/d;

    .line 108
    sget-object v1, Lco/uk/getmondo/create_account/d;->a:Lco/uk/getmondo/create_account/d;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/create_account/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lco/uk/getmondo/create_account/g;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/create_account/g$a;

    invoke-interface {v0}, Lco/uk/getmondo/create_account/g$a;->c()V

    .line 114
    :goto_0
    return-void

    .line 113
    :cond_0
    iget-object v1, p0, Lco/uk/getmondo/create_account/g;->f:Lco/uk/getmondo/common/e/a;

    iget-object v0, p0, Lco/uk/getmondo/create_account/g;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/common/e/a$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    goto :goto_0
.end method

.method static synthetic b(Lco/uk/getmondo/create_account/g;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 102
    iget-object v0, p0, Lco/uk/getmondo/create_account/g;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/create_account/g$a;

    invoke-interface {v0}, Lco/uk/getmondo/create_account/g$a;->h()V

    return-void
.end method


# virtual methods
.method a()V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lco/uk/getmondo/create_account/g;->h:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->x()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 62
    iget-object v0, p0, Lco/uk/getmondo/create_account/g;->e:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/ac;->h()Lco/uk/getmondo/d/s;

    move-result-object v1

    .line 63
    iget-object v0, p0, Lco/uk/getmondo/create_account/g;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/create_account/g$a;

    invoke-interface {v0, v1}, Lco/uk/getmondo/create_account/g$a;->a(Lco/uk/getmondo/d/s;)V

    .line 64
    return-void
.end method

.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 29
    check-cast p1, Lco/uk/getmondo/create_account/g$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/create_account/g;->a(Lco/uk/getmondo/create_account/g$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/create_account/g$a;)V
    .locals 3

    .prologue
    .line 51
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 52
    iget-object v0, p0, Lco/uk/getmondo/create_account/g;->h:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->u()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 54
    iget-object v0, p0, Lco/uk/getmondo/create_account/g;->e:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v1

    .line 55
    iget-object v0, p0, Lco/uk/getmondo/create_account/g;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/create_account/g$a;

    invoke-virtual {v1}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/d/ac;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lco/uk/getmondo/create_account/g$a;->a(Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lco/uk/getmondo/create_account/g;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/create_account/g$a;

    invoke-virtual {v1}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/d/ac;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lco/uk/getmondo/create_account/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lco/uk/getmondo/create_account/g$a;->d(Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lco/uk/getmondo/create_account/g;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/create_account/g$a;

    invoke-virtual {v1}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/d/ac;->h()Lco/uk/getmondo/d/s;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/common/k/a;->a(Lco/uk/getmondo/d/s;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lco/uk/getmondo/create_account/g$a;->e(Ljava/lang/String;)V

    .line 58
    return-void
.end method

.method a(Lco/uk/getmondo/d/s;)V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lco/uk/getmondo/create_account/g;->e:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    .line 119
    iget-object v1, p0, Lco/uk/getmondo/create_account/g;->e:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v0

    invoke-virtual {v0, p1}, Lco/uk/getmondo/d/ac;->a(Lco/uk/getmondo/d/s;)Lco/uk/getmondo/d/ac;

    move-result-object v0

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/accounts/d;->a(Lco/uk/getmondo/d/ac;)V

    .line 120
    iget-object v0, p0, Lco/uk/getmondo/create_account/g;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/create_account/g$a;

    invoke-static {p1}, Lco/uk/getmondo/common/k/a;->a(Lco/uk/getmondo/d/s;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lco/uk/getmondo/create_account/g$a;->e(Ljava/lang/String;)V

    .line 121
    return-void
.end method

.method a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 67
    iget-object v0, p0, Lco/uk/getmondo/create_account/g;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/create_account/g$a;

    invoke-interface {v0}, Lco/uk/getmondo/create_account/g$a;->e()V

    .line 68
    iget-object v0, p0, Lco/uk/getmondo/create_account/g;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/create_account/g$a;

    invoke-interface {v0}, Lco/uk/getmondo/create_account/g$a;->f()V

    .line 70
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 73
    :try_start_0
    invoke-static {p2}, Lco/uk/getmondo/create_account/b;->a(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 78
    invoke-static {v1, v0}, Lco/uk/getmondo/create_account/b;->b(Ljava/util/Date;Ljava/util/Date;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 79
    iget-object v0, p0, Lco/uk/getmondo/create_account/g;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/create_account/g$a;

    invoke-interface {v0}, Lco/uk/getmondo/create_account/g$a;->b()V

    .line 115
    :goto_0
    return-void

    .line 74
    :catch_0
    move-exception v0

    .line 75
    iget-object v0, p0, Lco/uk/getmondo/create_account/g;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/create_account/g$a;

    invoke-interface {v0}, Lco/uk/getmondo/create_account/g$a;->b()V

    goto :goto_0

    .line 82
    :cond_0
    invoke-static {v1, v0}, Lco/uk/getmondo/create_account/b;->a(Ljava/util/Date;Ljava/util/Date;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 83
    iget-object v0, p0, Lco/uk/getmondo/create_account/g;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/create_account/g$a;

    invoke-interface {v0}, Lco/uk/getmondo/create_account/g$a;->a()V

    goto :goto_0

    .line 86
    :cond_1
    invoke-static {p1}, Lco/uk/getmondo/common/k/p;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 87
    iget-object v0, p0, Lco/uk/getmondo/create_account/g;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/create_account/g$a;

    invoke-interface {v0}, Lco/uk/getmondo/create_account/g$a;->d()V

    goto :goto_0

    .line 91
    :cond_2
    iget-object v0, p0, Lco/uk/getmondo/create_account/g;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/create_account/g$a;

    invoke-interface {v0}, Lco/uk/getmondo/create_account/g$a;->i()V

    .line 92
    iget-object v0, p0, Lco/uk/getmondo/create_account/g;->e:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    .line 93
    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/d/ac;->f()Ljava/lang/String;

    move-result-object v4

    .line 94
    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/d/ac;->d()Ljava/lang/String;

    move-result-object v1

    .line 95
    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/d/ac;->h()Lco/uk/getmondo/d/s;

    move-result-object v5

    .line 96
    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v0

    invoke-static {p2}, Lco/uk/getmondo/create_account/b;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Lco/uk/getmondo/d/ac;->a(Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/d/ac;

    move-result-object v0

    .line 97
    iget-object v2, p0, Lco/uk/getmondo/create_account/g;->e:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v2, v0}, Lco/uk/getmondo/common/accounts/d;->a(Lco/uk/getmondo/d/ac;)V

    .line 99
    iget-object v6, p0, Lco/uk/getmondo/create_account/g;->g:Lco/uk/getmondo/api/b/a;

    new-instance v0, Lco/uk/getmondo/create_account/a/a/a;

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/create_account/a/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/s;)V

    invoke-virtual {v6, v0}, Lco/uk/getmondo/api/b/a;->a(Lco/uk/getmondo/create_account/a/a/a;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/create_account/g;->d:Lio/reactivex/u;

    .line 100
    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/create_account/g;->c:Lio/reactivex/u;

    .line 101
    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/create_account/h;->a(Lco/uk/getmondo/create_account/g;)Lio/reactivex/c/g;

    move-result-object v1

    .line 102
    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/create_account/i;->a(Lco/uk/getmondo/create_account/g;)Lio/reactivex/c/a;

    move-result-object v1

    invoke-static {p0}, Lco/uk/getmondo/create_account/j;->a(Lco/uk/getmondo/create_account/g;)Lio/reactivex/c/g;

    move-result-object v2

    .line 103
    invoke-virtual {v0, v1, v2}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 99
    invoke-virtual {p0, v0}, Lco/uk/getmondo/create_account/g;->a(Lio/reactivex/b/b;)V

    goto/16 :goto_0
.end method

.method c()V
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lco/uk/getmondo/create_account/g;->h:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->v()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 125
    return-void
.end method

.method d()V
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lco/uk/getmondo/create_account/g;->h:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->w()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 129
    return-void
.end method
