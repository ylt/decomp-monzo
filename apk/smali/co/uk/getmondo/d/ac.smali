.class public final Lco/uk/getmondo/d/ac;
.super Ljava/lang/Object;
.source "Profile.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008%\u0008\u0086\u0008\u0018\u00002\u00020\u0001B_\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\u0003\u0012\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0008\u0008\u0002\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\t\u0010!\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\"\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010#\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u0010$\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010%\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010&\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0010\u0010\'\u001a\u0004\u0018\u00010\nH\u00c6\u0003\u00a2\u0006\u0002\u0010\u001fJ\t\u0010(\u001a\u00020\u000cH\u00c6\u0003J\t\u0010)\u001a\u00020\u000eH\u00c6\u0003Jp\u0010*\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00032\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00032\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000c2\u0008\u0008\u0002\u0010\r\u001a\u00020\u000eH\u00c6\u0001\u00a2\u0006\u0002\u0010+J\u0013\u0010,\u001a\u00020\u000e2\u0008\u0010-\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010.\u001a\u00020\nH\u00d6\u0001J\t\u0010/\u001a\u00020\u0003H\u00d6\u0001J\u000e\u00100\u001a\u00020\u00002\u0006\u0010\u000b\u001a\u00020\u000cJ\u0016\u00101\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0003J\u000e\u00102\u001a\u00020\u00002\u0006\u0010\u0008\u001a\u00020\u0003R\u0011\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0015R\u0011\u0010\u0017\u001a\u00020\u000e8F\u00a2\u0006\u0006\u001a\u0004\u0008\u0017\u0010\u0013R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0015R\u0013\u0010\u0008\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u0015R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0015R\u0013\u0010\u001b\u001a\u0004\u0018\u00010\u00038F\u00a2\u0006\u0006\u001a\u0004\u0008\u001c\u0010\u0015R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u0015R\u0015\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\n\n\u0002\u0010 \u001a\u0004\u0008\u001e\u0010\u001f\u00a8\u00063"
    }
    d2 = {
        "Lco/uk/getmondo/model/Profile;",
        "",
        "userId",
        "",
        "name",
        "preferredName",
        "email",
        "dateOfBirth",
        "phoneNumber",
        "userNumber",
        "",
        "address",
        "Lco/uk/getmondo/model/LegacyAddress;",
        "addressUpdatable",
        "",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lco/uk/getmondo/model/LegacyAddress;Z)V",
        "getAddress",
        "()Lco/uk/getmondo/model/LegacyAddress;",
        "getAddressUpdatable",
        "()Z",
        "getDateOfBirth",
        "()Ljava/lang/String;",
        "getEmail",
        "isStudent",
        "getName",
        "getPhoneNumber",
        "getPreferredName",
        "preferredNameOrFullName",
        "getPreferredNameOrFullName",
        "getUserId",
        "getUserNumber",
        "()Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lco/uk/getmondo/model/LegacyAddress;Z)Lco/uk/getmondo/model/Profile;",
        "equals",
        "other",
        "hashCode",
        "toString",
        "withAddress",
        "withNameAndDob",
        "withPhoneNumber",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final address:Lco/uk/getmondo/d/s;

.field private final addressUpdatable:Z

.field private final dateOfBirth:Ljava/lang/String;

.field private final email:Ljava/lang/String;

.field private final name:Ljava/lang/String;

.field private final phoneNumber:Ljava/lang/String;

.field private final preferredName:Ljava/lang/String;

.field private final userId:Ljava/lang/String;

.field private final userNumber:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lco/uk/getmondo/d/s;Z)V
    .locals 1

    .prologue
    const-string v0, "userId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "email"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "address"

    invoke-static {p8, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/d/ac;->userId:Ljava/lang/String;

    iput-object p2, p0, Lco/uk/getmondo/d/ac;->name:Ljava/lang/String;

    iput-object p3, p0, Lco/uk/getmondo/d/ac;->preferredName:Ljava/lang/String;

    iput-object p4, p0, Lco/uk/getmondo/d/ac;->email:Ljava/lang/String;

    iput-object p5, p0, Lco/uk/getmondo/d/ac;->dateOfBirth:Ljava/lang/String;

    iput-object p6, p0, Lco/uk/getmondo/d/ac;->phoneNumber:Ljava/lang/String;

    iput-object p7, p0, Lco/uk/getmondo/d/ac;->userNumber:Ljava/lang/Integer;

    iput-object p8, p0, Lco/uk/getmondo/d/ac;->address:Lco/uk/getmondo/d/s;

    iput-boolean p9, p0, Lco/uk/getmondo/d/ac;->addressUpdatable:Z

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lco/uk/getmondo/d/s;ZILkotlin/d/b/i;)V
    .locals 11

    .prologue
    and-int/lit8 v1, p10, 0x4

    if-eqz v1, :cond_4

    .line 6
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object v4, v1

    :goto_0
    and-int/lit8 v1, p10, 0x10

    if-eqz v1, :cond_3

    .line 8
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object v6, v1

    :goto_1
    and-int/lit8 v1, p10, 0x20

    if-eqz v1, :cond_2

    .line 9
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    move-object v7, v1

    :goto_2
    and-int/lit8 v1, p10, 0x40

    if-eqz v1, :cond_1

    .line 10
    const/4 v1, 0x0

    check-cast v1, Ljava/lang/Integer;

    move-object v8, v1

    :goto_3
    move/from16 v0, p10

    and-int/lit16 v1, v0, 0x100

    if-eqz v1, :cond_0

    .line 12
    const/4 v10, 0x0

    :goto_4
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p4

    move-object/from16 v9, p8

    invoke-direct/range {v1 .. v10}, Lco/uk/getmondo/d/ac;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lco/uk/getmondo/d/s;Z)V

    return-void

    :cond_0
    move/from16 v10, p9

    goto :goto_4

    :cond_1
    move-object/from16 v8, p7

    goto :goto_3

    :cond_2
    move-object/from16 v7, p6

    goto :goto_2

    :cond_3
    move-object/from16 v6, p5

    goto :goto_1

    :cond_4
    move-object v4, p3

    goto :goto_0
.end method

.method public static bridge synthetic a(Lco/uk/getmondo/d/ac;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lco/uk/getmondo/d/s;ZILjava/lang/Object;)Lco/uk/getmondo/d/ac;
    .locals 11

    and-int/lit8 v1, p10, 0x1

    if-eqz v1, :cond_8

    iget-object v2, p0, Lco/uk/getmondo/d/ac;->userId:Ljava/lang/String;

    :goto_0
    and-int/lit8 v1, p10, 0x2

    if-eqz v1, :cond_7

    iget-object v3, p0, Lco/uk/getmondo/d/ac;->name:Ljava/lang/String;

    :goto_1
    and-int/lit8 v1, p10, 0x4

    if-eqz v1, :cond_6

    iget-object v4, p0, Lco/uk/getmondo/d/ac;->preferredName:Ljava/lang/String;

    :goto_2
    and-int/lit8 v1, p10, 0x8

    if-eqz v1, :cond_5

    iget-object v5, p0, Lco/uk/getmondo/d/ac;->email:Ljava/lang/String;

    :goto_3
    and-int/lit8 v1, p10, 0x10

    if-eqz v1, :cond_4

    iget-object v6, p0, Lco/uk/getmondo/d/ac;->dateOfBirth:Ljava/lang/String;

    :goto_4
    and-int/lit8 v1, p10, 0x20

    if-eqz v1, :cond_3

    iget-object v7, p0, Lco/uk/getmondo/d/ac;->phoneNumber:Ljava/lang/String;

    :goto_5
    and-int/lit8 v1, p10, 0x40

    if-eqz v1, :cond_2

    iget-object v8, p0, Lco/uk/getmondo/d/ac;->userNumber:Ljava/lang/Integer;

    :goto_6
    move/from16 v0, p10

    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_1

    iget-object v9, p0, Lco/uk/getmondo/d/ac;->address:Lco/uk/getmondo/d/s;

    :goto_7
    move/from16 v0, p10

    and-int/lit16 v1, v0, 0x100

    if-eqz v1, :cond_0

    iget-boolean v10, p0, Lco/uk/getmondo/d/ac;->addressUpdatable:Z

    :goto_8
    move-object v1, p0

    invoke-virtual/range {v1 .. v10}, Lco/uk/getmondo/d/ac;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lco/uk/getmondo/d/s;Z)Lco/uk/getmondo/d/ac;

    move-result-object v1

    return-object v1

    :cond_0
    move/from16 v10, p9

    goto :goto_8

    :cond_1
    move-object/from16 v9, p8

    goto :goto_7

    :cond_2
    move-object/from16 v8, p7

    goto :goto_6

    :cond_3
    move-object/from16 v7, p6

    goto :goto_5

    :cond_4
    move-object/from16 v6, p5

    goto :goto_4

    :cond_5
    move-object v5, p4

    goto :goto_3

    :cond_6
    move-object v4, p3

    goto :goto_2

    :cond_7
    move-object v3, p2

    goto :goto_1

    :cond_8
    move-object v2, p1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/d/s;)Lco/uk/getmondo/d/ac;
    .locals 12

    .prologue
    const/4 v1, 0x0

    const-string v0, "address"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    const/4 v9, 0x0

    const/16 v10, 0x17f

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v6, v1

    move-object v7, v1

    move-object v8, p1

    move-object v11, v1

    invoke-static/range {v0 .. v11}, Lco/uk/getmondo/d/ac;->a(Lco/uk/getmondo/d/ac;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lco/uk/getmondo/d/s;ZILjava/lang/Object;)Lco/uk/getmondo/d/ac;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lco/uk/getmondo/d/ac;
    .locals 12

    .prologue
    const/4 v1, 0x0

    const-string v0, "phoneNumber"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    const/4 v9, 0x0

    const/16 v10, 0x1df

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v6, p1

    move-object v7, v1

    move-object v8, v1

    move-object v11, v1

    invoke-static/range {v0 .. v11}, Lco/uk/getmondo/d/ac;->a(Lco/uk/getmondo/d/ac;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lco/uk/getmondo/d/s;ZILjava/lang/Object;)Lco/uk/getmondo/d/ac;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/d/ac;
    .locals 12

    .prologue
    const/4 v1, 0x0

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateOfBirth"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    const/4 v9, 0x0

    const/16 v10, 0x1ed

    move-object v0, p0

    move-object v2, p1

    move-object v3, v1

    move-object v4, v1

    move-object v5, p2

    move-object v6, v1

    move-object v7, v1

    move-object v8, v1

    move-object v11, v1

    invoke-static/range {v0 .. v11}, Lco/uk/getmondo/d/ac;->a(Lco/uk/getmondo/d/ac;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lco/uk/getmondo/d/s;ZILjava/lang/Object;)Lco/uk/getmondo/d/ac;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lco/uk/getmondo/d/s;Z)Lco/uk/getmondo/d/ac;
    .locals 11

    const-string v1, "userId"

    invoke-static {p1, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "name"

    invoke-static {p2, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "email"

    invoke-static {p4, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "address"

    move-object/from16 v0, p8

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lco/uk/getmondo/d/ac;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move/from16 v10, p9

    invoke-direct/range {v1 .. v10}, Lco/uk/getmondo/d/ac;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lco/uk/getmondo/d/s;Z)V

    return-object v1
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lco/uk/getmondo/d/ac;->preferredName:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, p0, Lco/uk/getmondo/d/ac;->name:Ljava/lang/String;

    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lco/uk/getmondo/d/ac;->preferredName:Ljava/lang/String;

    goto :goto_1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4
    iget-object v0, p0, Lco/uk/getmondo/d/ac;->userId:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5
    iget-object v0, p0, Lco/uk/getmondo/d/ac;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lco/uk/getmondo/d/ac;->email:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lco/uk/getmondo/d/ac;->dateOfBirth:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eq p0, p1, :cond_0

    instance-of v2, p1, Lco/uk/getmondo/d/ac;

    if-eqz v2, :cond_1

    check-cast p1, Lco/uk/getmondo/d/ac;

    iget-object v2, p0, Lco/uk/getmondo/d/ac;->userId:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/d/ac;->userId:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/d/ac;->name:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/d/ac;->name:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/d/ac;->preferredName:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/d/ac;->preferredName:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/d/ac;->email:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/d/ac;->email:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/d/ac;->dateOfBirth:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/d/ac;->dateOfBirth:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/d/ac;->phoneNumber:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/d/ac;->phoneNumber:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/d/ac;->userNumber:Ljava/lang/Integer;

    iget-object v3, p1, Lco/uk/getmondo/d/ac;->userNumber:Ljava/lang/Integer;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/d/ac;->address:Lco/uk/getmondo/d/s;

    iget-object v3, p1, Lco/uk/getmondo/d/ac;->address:Lco/uk/getmondo/d/s;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lco/uk/getmondo/d/ac;->addressUpdatable:Z

    iget-boolean v3, p1, Lco/uk/getmondo/d/ac;->addressUpdatable:Z

    if-ne v2, v3, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lco/uk/getmondo/d/ac;->phoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lco/uk/getmondo/d/ac;->userNumber:Ljava/lang/Integer;

    return-object v0
.end method

.method public final h()Lco/uk/getmondo/d/s;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lco/uk/getmondo/d/ac;->address:Lco/uk/getmondo/d/s;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lco/uk/getmondo/d/ac;->userId:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/ac;->name:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/ac;->preferredName:Ljava/lang/String;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/ac;->email:Ljava/lang/String;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/ac;->dateOfBirth:Ljava/lang/String;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/ac;->phoneNumber:Ljava/lang/String;

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/ac;->userNumber:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/uk/getmondo/d/ac;->address:Lco/uk/getmondo/d/s;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lco/uk/getmondo/d/ac;->addressUpdatable:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :cond_1
    add-int/2addr v0, v1

    return v0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_3

    :cond_6
    move v0, v1

    goto :goto_4

    :cond_7
    move v0, v1

    goto :goto_5

    :cond_8
    move v0, v1

    goto :goto_6
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 12
    iget-boolean v0, p0, Lco/uk/getmondo/d/ac;->addressUpdatable:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Profile(userId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/ac;->userId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/ac;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", preferredName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/ac;->preferredName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", email="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/ac;->email:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dateOfBirth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/ac;->dateOfBirth:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", phoneNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/ac;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", userNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/ac;->userNumber:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", address="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/ac;->address:Lco/uk/getmondo/d/s;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", addressUpdatable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lco/uk/getmondo/d/ac;->addressUpdatable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
