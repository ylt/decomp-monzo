.class public Lco/uk/getmondo/d/w;
.super Lio/realm/bc;
.source "OverdraftStatus.kt"

# interfaces
.implements Lio/realm/ap;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\t\n\u0002\u0008\u0013\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0016\u0018\u00002\u00020\u0001BW\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\t\u0012\u0006\u0010\u000b\u001a\u00020\t\u0012\u0006\u0010\u000c\u001a\u00020\t\u0012\u0006\u0010\r\u001a\u00020\t\u0012\u0006\u0010\u000e\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u000fB\u0005\u00a2\u0006\u0002\u0010\u0010J\u0013\u00101\u001a\u00020\u00052\u0008\u00102\u001a\u0004\u0018\u000103H\u0096\u0002J\u0008\u00104\u001a\u000205H\u0016J\u0008\u00106\u001a\u00020\u0003H\u0016R\u000e\u0010\u0011\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0002\u001a\u00020\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019\"\u0004\u0008\u001a\u0010\u001bR\u0011\u0010\u001c\u001a\u00020\u001d8F\u00a2\u0006\u0006\u001a\u0004\u0008\u001e\u0010\u001fR\u001a\u0010\u0004\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008 \u0010!\"\u0004\u0008\"\u0010#R\u0011\u0010$\u001a\u00020\u001d8F\u00a2\u0006\u0006\u001a\u0004\u0008%\u0010\u001fR\u0011\u0010\u000e\u001a\u00020&8F\u00a2\u0006\u0006\u001a\u0004\u0008\'\u0010(R\u0011\u0010)\u001a\u00020\u001d8F\u00a2\u0006\u0006\u001a\u0004\u0008*\u0010\u001fR\u001a\u0010\u0006\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008+\u0010!\"\u0004\u0008,\u0010#R\u0011\u0010-\u001a\u00020\u001d8F\u00a2\u0006\u0006\u001a\u0004\u0008.\u0010\u001fR\u0011\u0010/\u001a\u00020\u001d8F\u00a2\u0006\u0006\u001a\u0004\u00080\u0010\u001f\u00a8\u00067"
    }
    d2 = {
        "Lco/uk/getmondo/model/OverdraftStatus;",
        "Lio/realm/RealmObject;",
        "accountId",
        "",
        "active",
        "",
        "eligible",
        "currency",
        "accruedFeesAmount",
        "",
        "bufferAmount",
        "dailyFeeAmount",
        "limitAmount",
        "preapprovedLimitAmount",
        "chargeDate",
        "(Ljava/lang/String;ZZLjava/lang/String;JJJJJLjava/lang/String;)V",
        "()V",
        "_accruedFeesAmount",
        "_bufferAmount",
        "_chargeDate",
        "_currency",
        "_dailyFeeAmount",
        "_limitAmount",
        "_preapprovedLimitAmount",
        "getAccountId",
        "()Ljava/lang/String;",
        "setAccountId",
        "(Ljava/lang/String;)V",
        "accruedFees",
        "Lco/uk/getmondo/model/Amount;",
        "getAccruedFees",
        "()Lco/uk/getmondo/model/Amount;",
        "getActive",
        "()Z",
        "setActive",
        "(Z)V",
        "buffer",
        "getBuffer",
        "Lorg/threeten/bp/LocalDate;",
        "getChargeDate",
        "()Lorg/threeten/bp/LocalDate;",
        "dailyFee",
        "getDailyFee",
        "getEligible",
        "setEligible",
        "limit",
        "getLimit",
        "preapprovedLimit",
        "getPreapprovedLimit",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private _accruedFeesAmount:J

.field private _bufferAmount:J

.field private _chargeDate:Ljava/lang/String;

.field private _currency:Ljava/lang/String;

.field private _dailyFeeAmount:J

.field private _limitAmount:J

.field private _preapprovedLimitAmount:J

.field private accountId:Ljava/lang/String;

.field private active:Z

.field private eligible:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Lio/realm/bc;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->u_()V

    .line 8
    :cond_0
    const-string v0, ""

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/w;->a(Ljava/lang/String;)V

    .line 24
    const-string v0, ""

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/w;->b(Ljava/lang/String;)V

    .line 30
    const-string v0, ""

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/w;->c(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZZLjava/lang/String;JJJJJLjava/lang/String;)V
    .locals 3

    .prologue
    const-string v2, "accountId"

    invoke-static {p1, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "currency"

    invoke-static {p4, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "chargeDate"

    move-object/from16 v0, p15

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-direct {p0}, Lco/uk/getmondo/d/w;-><init>()V

    instance-of v2, p0, Lio/realm/internal/l;

    if-eqz v2, :cond_0

    move-object v2, p0

    check-cast v2, Lio/realm/internal/l;

    invoke-interface {v2}, Lio/realm/internal/l;->u_()V

    .line 43
    :cond_0
    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/w;->a(Ljava/lang/String;)V

    .line 44
    invoke-virtual {p0, p2}, Lco/uk/getmondo/d/w;->a(Z)V

    .line 45
    invoke-virtual {p0, p3}, Lco/uk/getmondo/d/w;->b(Z)V

    .line 46
    invoke-virtual {p0, p4}, Lco/uk/getmondo/d/w;->b(Ljava/lang/String;)V

    .line 47
    invoke-virtual {p0, p5, p6}, Lco/uk/getmondo/d/w;->a(J)V

    .line 48
    invoke-virtual {p0, p7, p8}, Lco/uk/getmondo/d/w;->b(J)V

    .line 49
    invoke-virtual {p0, p9, p10}, Lco/uk/getmondo/d/w;->c(J)V

    .line 50
    invoke-virtual {p0, p11, p12}, Lco/uk/getmondo/d/w;->d(J)V

    .line 51
    move-wide/from16 v0, p13

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/d/w;->e(J)V

    .line 52
    move-object/from16 v0, p15

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/w;->c(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8
    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(J)V
    .locals 1

    iput-wide p1, p0, Lco/uk/getmondo/d/w;->_accruedFeesAmount:J

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/w;->accountId:Ljava/lang/String;

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lco/uk/getmondo/d/w;->active:Z

    return-void
.end method

.method public b(J)V
    .locals 1

    iput-wide p1, p0, Lco/uk/getmondo/d/w;->_bufferAmount:J

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/w;->_currency:Ljava/lang/String;

    return-void
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lco/uk/getmondo/d/w;->eligible:Z

    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->k()Z

    move-result v0

    return v0
.end method

.method public c(J)V
    .locals 1

    iput-wide p1, p0, Lco/uk/getmondo/d/w;->_dailyFeeAmount:J

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/w;->_chargeDate:Ljava/lang/String;

    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->l()Z

    move-result v0

    return v0
.end method

.method public final d()Lco/uk/getmondo/d/c;
    .locals 4

    .prologue
    .line 12
    new-instance v0, Lco/uk/getmondo/d/c;

    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->n()J

    move-result-wide v2

    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->m()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Lco/uk/getmondo/d/c;-><init>(JLjava/lang/String;)V

    return-object v0
.end method

.method public d(J)V
    .locals 1

    iput-wide p1, p0, Lco/uk/getmondo/d/w;->_limitAmount:J

    return-void
.end method

.method public final e()Lco/uk/getmondo/d/c;
    .locals 4

    .prologue
    .line 14
    new-instance v0, Lco/uk/getmondo/d/c;

    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->o()J

    move-result-wide v2

    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->m()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Lco/uk/getmondo/d/c;-><init>(JLjava/lang/String;)V

    return-object v0
.end method

.method public e(J)V
    .locals 1

    iput-wide p1, p0, Lco/uk/getmondo/d/w;->_preapprovedLimitAmount:J

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 57
    move-object v0, p0

    check-cast v0, Lco/uk/getmondo/d/w;

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 73
    :goto_0
    return v0

    .line 58
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    :goto_1
    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 60
    :cond_2
    if-nez p1, :cond_3

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.model.OverdraftStatus"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/d/w;

    .line 62
    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->j()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/d/w;

    invoke-virtual {v0}, Lco/uk/getmondo/d/w;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    move v0, v2

    goto :goto_0

    .line 63
    :cond_4
    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->k()Z

    move-result v3

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/d/w;

    invoke-virtual {v0}, Lco/uk/getmondo/d/w;->k()Z

    move-result v0

    if-eq v3, v0, :cond_5

    move v0, v2

    goto :goto_0

    .line 64
    :cond_5
    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->l()Z

    move-result v3

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/d/w;

    invoke-virtual {v0}, Lco/uk/getmondo/d/w;->l()Z

    move-result v0

    if-eq v3, v0, :cond_6

    move v0, v2

    goto :goto_0

    .line 65
    :cond_6
    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->m()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/d/w;

    invoke-virtual {v0}, Lco/uk/getmondo/d/w;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_7

    move v0, v2

    goto :goto_0

    .line 66
    :cond_7
    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->n()J

    move-result-wide v4

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/d/w;

    invoke-virtual {v0}, Lco/uk/getmondo/d/w;->n()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-eqz v0, :cond_8

    move v0, v2

    goto :goto_0

    .line 67
    :cond_8
    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->o()J

    move-result-wide v4

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/d/w;

    invoke-virtual {v0}, Lco/uk/getmondo/d/w;->o()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-eqz v0, :cond_9

    move v0, v2

    goto/16 :goto_0

    .line 68
    :cond_9
    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->p()J

    move-result-wide v4

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/d/w;

    invoke-virtual {v0}, Lco/uk/getmondo/d/w;->p()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-eqz v0, :cond_a

    move v0, v2

    goto/16 :goto_0

    .line 69
    :cond_a
    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->q()J

    move-result-wide v4

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/d/w;

    invoke-virtual {v0}, Lco/uk/getmondo/d/w;->q()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-eqz v0, :cond_b

    move v0, v2

    goto/16 :goto_0

    .line 70
    :cond_b
    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->r()J

    move-result-wide v4

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/d/w;

    invoke-virtual {v0}, Lco/uk/getmondo/d/w;->r()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-eqz v0, :cond_c

    move v0, v2

    goto/16 :goto_0

    .line 71
    :cond_c
    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->s()Ljava/lang/String;

    move-result-object v0

    check-cast p1, Lco/uk/getmondo/d/w;

    invoke-virtual {p1}, Lco/uk/getmondo/d/w;->s()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_d

    move v0, v2

    goto/16 :goto_0

    :cond_d
    move v0, v1

    .line 73
    goto/16 :goto_0
.end method

.method public final f()Lco/uk/getmondo/d/c;
    .locals 4

    .prologue
    .line 16
    new-instance v0, Lco/uk/getmondo/d/c;

    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->p()J

    move-result-wide v2

    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->m()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Lco/uk/getmondo/d/c;-><init>(JLjava/lang/String;)V

    return-object v0
.end method

.method public final g()Lco/uk/getmondo/d/c;
    .locals 4

    .prologue
    .line 18
    new-instance v0, Lco/uk/getmondo/d/c;

    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->q()J

    move-result-wide v2

    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->m()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Lco/uk/getmondo/d/c;-><init>(JLjava/lang/String;)V

    return-object v0
.end method

.method public final h()Lco/uk/getmondo/d/c;
    .locals 4

    .prologue
    .line 20
    new-instance v0, Lco/uk/getmondo/d/c;

    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->r()J

    move-result-wide v2

    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->m()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Lco/uk/getmondo/d/c;-><init>(JLjava/lang/String;)V

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 77
    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 78
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->k()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 79
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->l()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 80
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 81
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->n()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 82
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->o()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 83
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->p()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 84
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->q()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 85
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->r()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 86
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 87
    return v0
.end method

.method public final i()Lorg/threeten/bp/LocalDate;
    .locals 2

    .prologue
    .line 22
    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->s()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lorg/threeten/bp/LocalDate;->a(Ljava/lang/CharSequence;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    const-string v1, "LocalDate.parse(_chargeDate)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/w;->accountId:Ljava/lang/String;

    return-object v0
.end method

.method public k()Z
    .locals 1

    iget-boolean v0, p0, Lco/uk/getmondo/d/w;->active:Z

    return v0
.end method

.method public l()Z
    .locals 1

    iget-boolean v0, p0, Lco/uk/getmondo/d/w;->eligible:Z

    return v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/w;->_currency:Ljava/lang/String;

    return-object v0
.end method

.method public n()J
    .locals 2

    iget-wide v0, p0, Lco/uk/getmondo/d/w;->_accruedFeesAmount:J

    return-wide v0
.end method

.method public o()J
    .locals 2

    iget-wide v0, p0, Lco/uk/getmondo/d/w;->_bufferAmount:J

    return-wide v0
.end method

.method public p()J
    .locals 2

    iget-wide v0, p0, Lco/uk/getmondo/d/w;->_dailyFeeAmount:J

    return-wide v0
.end method

.method public q()J
    .locals 2

    iget-wide v0, p0, Lco/uk/getmondo/d/w;->_limitAmount:J

    return-wide v0
.end method

.method public r()J
    .locals 2

    iget-wide v0, p0, Lco/uk/getmondo/d/w;->_preapprovedLimitAmount:J

    return-wide v0
.end method

.method public s()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/w;->_chargeDate:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OverdraftStatus(accountId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', active="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->k()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", eligible="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->l()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", _currency=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\',"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " _accruedFeesAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 92
    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->n()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", _bufferAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->o()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", _dailyFeeAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->p()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " _limitAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 93
    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->q()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", _preapprovedLimitAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->r()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", _chargeDate=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/d/w;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
