.class public Lco/uk/getmondo/d/l;
.super Lio/realm/bc;
.source "FeatureFlags.kt"

# interfaces
.implements Lio/realm/r;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\n\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\u0008\u0017\u0018\u00002\u00020\u0001B\u001b\u0008\u0007\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005J\u0013\u0010\u000c\u001a\u00020\u00032\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0096\u0002J\u0008\u0010\u000f\u001a\u00020\u0010H\u0016R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\"\u0004\u0008\u0008\u0010\tR\u001a\u0010\u0004\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\n\u0010\u0007\"\u0004\u0008\u000b\u0010\t\u00a8\u0006\u0011"
    }
    d2 = {
        "Lco/uk/getmondo/model/FeatureFlags;",
        "Lio/realm/RealmObject;",
        "currentAccountP2pEnabled",
        "",
        "potsEnabled",
        "(ZZ)V",
        "getCurrentAccountP2pEnabled",
        "()Z",
        "setCurrentAccountP2pEnabled",
        "(Z)V",
        "getPotsEnabled",
        "setPotsEnabled",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private currentAccountP2pEnabled:Z

.field private potsEnabled:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-direct {p0, v2, v2, v0, v1}, Lco/uk/getmondo/d/l;-><init>(ZZILkotlin/d/b/i;)V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/l;

    invoke-interface {p0}, Lio/realm/internal/l;->u_()V

    :cond_0
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Lco/uk/getmondo/d/l;-><init>(ZZILkotlin/d/b/i;)V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/l;

    invoke-interface {p0}, Lio/realm/internal/l;->u_()V

    :cond_0
    return-void
.end method

.method public constructor <init>(ZZ)V
    .locals 1

    .prologue
    .line 8
    .line 15
    invoke-direct {p0}, Lio/realm/bc;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->u_()V

    :cond_0
    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/l;->a(Z)V

    invoke-virtual {p0, p2}, Lco/uk/getmondo/d/l;->b(Z)V

    return-void
.end method

.method public synthetic constructor <init>(ZZILkotlin/d/b/i;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    and-int/lit8 v1, p3, 0x1

    if-eqz v1, :cond_0

    move p1, v0

    .line 13
    :cond_0
    and-int/lit8 v1, p3, 0x2

    if-eqz v1, :cond_1

    move p2, v0

    .line 14
    :cond_1
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/d/l;-><init>(ZZ)V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_2

    check-cast p0, Lio/realm/internal/l;

    invoke-interface {p0}, Lio/realm/internal/l;->u_()V

    :cond_2
    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lco/uk/getmondo/d/l;->currentAccountP2pEnabled:Z

    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lco/uk/getmondo/d/l;->b()Z

    move-result v0

    return v0
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lco/uk/getmondo/d/l;->potsEnabled:Z

    return-void
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lco/uk/getmondo/d/l;->currentAccountP2pEnabled:Z

    return v0
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lco/uk/getmondo/d/l;->potsEnabled:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 18
    move-object v0, p0

    check-cast v0, Lco/uk/getmondo/d/l;

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 26
    :goto_0
    return v0

    .line 19
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    :goto_1
    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 21
    :cond_2
    if-nez p1, :cond_3

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.model.FeatureFlags"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/d/l;

    .line 23
    invoke-virtual {p0}, Lco/uk/getmondo/d/l;->b()Z

    move-result v3

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/d/l;

    invoke-virtual {v0}, Lco/uk/getmondo/d/l;->b()Z

    move-result v0

    if-eq v3, v0, :cond_4

    move v0, v2

    goto :goto_0

    .line 24
    :cond_4
    invoke-virtual {p0}, Lco/uk/getmondo/d/l;->c()Z

    move-result v0

    check-cast p1, Lco/uk/getmondo/d/l;

    invoke-virtual {p1}, Lco/uk/getmondo/d/l;->c()Z

    move-result v3

    if-eq v0, v3, :cond_5

    move v0, v2

    goto :goto_0

    :cond_5
    move v0, v1

    .line 26
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 30
    invoke-virtual {p0}, Lco/uk/getmondo/d/l;->b()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 31
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/l;->c()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 32
    return v0
.end method
