.class public Lco/uk/getmondo/d/am;
.super Ljava/lang/Object;
.source "UserSettings.java"

# interfaces
.implements Lio/realm/bb;
.implements Lio/realm/bp;


# static fields
.field public static final ID:Ljava/lang/String; = "user_settings_storage_static_id"


# instance fields
.field private id:Ljava/lang/String;

.field private inboundP2P:Lco/uk/getmondo/d/p;

.field private monzoMeUsername:Ljava/lang/String;

.field private prepaidAccountMigrated:Z

.field private statusId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->u_()V

    .line 11
    :cond_0
    const-string v0, "user_settings_storage_static_id"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/am;->a(Ljava/lang/String;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/p;Z)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->u_()V

    .line 11
    :cond_0
    const-string v0, "user_settings_storage_static_id"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/am;->a(Ljava/lang/String;)V

    .line 22
    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/am;->b(Ljava/lang/String;)V

    .line 23
    invoke-virtual {p0, p2}, Lco/uk/getmondo/d/am;->c(Ljava/lang/String;)V

    .line 24
    invoke-virtual {p0, p3}, Lco/uk/getmondo/d/am;->a(Lco/uk/getmondo/d/p;)V

    .line 25
    invoke-virtual {p0, p4}, Lco/uk/getmondo/d/am;->a(Z)V

    .line 26
    return-void
.end method


# virtual methods
.method public a()Lco/uk/getmondo/payments/send/data/a/d;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lco/uk/getmondo/d/am;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/payments/send/data/a/d;->a(Ljava/lang/String;)Lco/uk/getmondo/payments/send/data/a/d;

    move-result-object v0

    return-object v0
.end method

.method public a(Lco/uk/getmondo/d/p;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/am;->inboundP2P:Lco/uk/getmondo/d/p;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/am;->id:Ljava/lang/String;

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lco/uk/getmondo/d/am;->prepaidAccountMigrated:Z

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lco/uk/getmondo/d/am;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/am;->statusId:Ljava/lang/String;

    return-void
.end method

.method public c()Lco/uk/getmondo/d/p;
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0}, Lco/uk/getmondo/d/am;->h()Lco/uk/getmondo/d/p;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/am;->monzoMeUsername:Ljava/lang/String;

    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0}, Lco/uk/getmondo/d/am;->i()Z

    move-result v0

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/am;->id:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 53
    if-ne p0, p1, :cond_1

    move v1, v0

    .line 62
    :cond_0
    :goto_0
    return v1

    .line 54
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    .line 56
    check-cast p1, Lco/uk/getmondo/d/am;

    .line 58
    invoke-virtual {p0}, Lco/uk/getmondo/d/am;->i()Z

    move-result v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/am;->i()Z

    move-result v3

    if-ne v2, v3, :cond_0

    .line 59
    invoke-virtual {p0}, Lco/uk/getmondo/d/am;->e()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-virtual {p0}, Lco/uk/getmondo/d/am;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/am;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 60
    :cond_2
    invoke-virtual {p0}, Lco/uk/getmondo/d/am;->f()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {p0}, Lco/uk/getmondo/d/am;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/am;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 61
    :cond_3
    invoke-virtual {p0}, Lco/uk/getmondo/d/am;->g()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-virtual {p0}, Lco/uk/getmondo/d/am;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/am;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 62
    :cond_4
    invoke-virtual {p0}, Lco/uk/getmondo/d/am;->h()Lco/uk/getmondo/d/p;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-virtual {p0}, Lco/uk/getmondo/d/am;->h()Lco/uk/getmondo/d/p;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/d/am;->h()Lco/uk/getmondo/d/p;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/d/p;->equals(Ljava/lang/Object;)Z

    move-result v0

    :cond_5
    :goto_1
    move v1, v0

    goto :goto_0

    .line 59
    :cond_6
    invoke-virtual {p1}, Lco/uk/getmondo/d/am;->e()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    goto :goto_0

    .line 60
    :cond_7
    invoke-virtual {p1}, Lco/uk/getmondo/d/am;->f()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    goto :goto_0

    .line 61
    :cond_8
    invoke-virtual {p1}, Lco/uk/getmondo/d/am;->g()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    goto :goto_0

    .line 62
    :cond_9
    invoke-virtual {p1}, Lco/uk/getmondo/d/am;->h()Lco/uk/getmondo/d/p;

    move-result-object v2

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_1
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/am;->statusId:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/am;->monzoMeUsername:Ljava/lang/String;

    return-object v0
.end method

.method public h()Lco/uk/getmondo/d/p;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/am;->inboundP2P:Lco/uk/getmondo/d/p;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-virtual {p0}, Lco/uk/getmondo/d/am;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lco/uk/getmondo/d/am;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 67
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/am;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lco/uk/getmondo/d/am;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 68
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/am;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lco/uk/getmondo/d/am;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    .line 69
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/am;->h()Lco/uk/getmondo/d/p;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lco/uk/getmondo/d/am;->h()Lco/uk/getmondo/d/p;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/p;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    .line 70
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/am;->i()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    add-int/2addr v0, v1

    .line 71
    return v0

    :cond_1
    move v0, v1

    .line 66
    goto :goto_0

    :cond_2
    move v0, v1

    .line 67
    goto :goto_1

    :cond_3
    move v0, v1

    .line 68
    goto :goto_2

    :cond_4
    move v0, v1

    .line 69
    goto :goto_3
.end method

.method public i()Z
    .locals 1

    iget-boolean v0, p0, Lco/uk/getmondo/d/am;->prepaidAccountMigrated:Z

    return v0
.end method
