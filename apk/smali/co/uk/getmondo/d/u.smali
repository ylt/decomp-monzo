.class public Lco/uk/getmondo/d/u;
.super Ljava/lang/Object;
.source "Merchant.java"

# interfaces
.implements Lio/realm/ah;
.implements Lio/realm/bb;


# static fields
.field public static final TFL_MERCHANT_NAME:Ljava/lang/String; = "Transport for London"


# instance fields
.field private atm:Z

.field private category:Ljava/lang/String;

.field private emoji:Ljava/lang/String;

.field private formattedAddress:Ljava/lang/String;

.field private groupId:Ljava/lang/String;

.field private id:Ljava/lang/String;

.field private latitude:Ljava/lang/Double;

.field private logoUrl:Ljava/lang/String;

.field private longitude:Ljava/lang/Double;

.field private name:Ljava/lang/String;

.field private online:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/l;

    invoke-interface {p0}, Lio/realm/internal/l;->u_()V

    .line 31
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/Double;Ljava/lang/Double;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->u_()V

    .line 35
    :cond_0
    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/u;->a(Ljava/lang/String;)V

    .line 36
    invoke-virtual {p0, p2}, Lco/uk/getmondo/d/u;->b(Ljava/lang/String;)V

    .line 37
    invoke-virtual {p0, p3}, Lco/uk/getmondo/d/u;->c(Ljava/lang/String;)V

    .line 38
    invoke-virtual {p0, p4}, Lco/uk/getmondo/d/u;->d(Ljava/lang/String;)V

    .line 39
    invoke-virtual {p0, p5}, Lco/uk/getmondo/d/u;->e(Ljava/lang/String;)V

    .line 40
    invoke-virtual {p0, p6}, Lco/uk/getmondo/d/u;->f(Ljava/lang/String;)V

    .line 41
    invoke-virtual {p0, p7}, Lco/uk/getmondo/d/u;->a(Z)V

    .line 42
    invoke-virtual {p0, p8}, Lco/uk/getmondo/d/u;->b(Z)V

    .line 43
    invoke-virtual {p0, p9}, Lco/uk/getmondo/d/u;->g(Ljava/lang/String;)V

    .line 44
    invoke-virtual {p0, p10}, Lco/uk/getmondo/d/u;->a(Ljava/lang/Double;)V

    .line 45
    invoke-virtual {p0, p11}, Lco/uk/getmondo/d/u;->b(Ljava/lang/Double;)V

    .line 46
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Double;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/u;->latitude:Ljava/lang/Double;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/u;->id:Ljava/lang/String;

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lco/uk/getmondo/d/u;->online:Z

    return-void
.end method

.method a()Z
    .locals 2

    .prologue
    .line 54
    const-string v0, "Transport for London"

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public b()Lco/uk/getmondo/d/h;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->r()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/d/h;->a(Ljava/lang/String;)Lco/uk/getmondo/d/h;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Double;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/u;->longitude:Ljava/lang/Double;

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/u;->groupId:Ljava/lang/String;

    return-void
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lco/uk/getmondo/d/u;->atm:Z

    return-void
.end method

.method public c()Lcom/c/b/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/c/b/b",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->u()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/c/b/b;->b(Ljava/lang/Object;)Lcom/c/b/b;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/u;->name:Ljava/lang/String;

    return-void
.end method

.method public d()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 67
    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->v()Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/u;->logoUrl:Ljava/lang/String;

    return-void
.end method

.method public e()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->w()Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public e(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/u;->emoji:Ljava/lang/String;

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 112
    if-ne p0, p1, :cond_1

    move v1, v0

    .line 128
    :cond_0
    :goto_0
    return v1

    .line 113
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    .line 115
    check-cast p1, Lco/uk/getmondo/d/u;

    .line 117
    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->s()Z

    move-result v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/u;->s()Z

    move-result v3

    if-ne v2, v3, :cond_0

    .line 118
    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->t()Z

    move-result v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/u;->t()Z

    move-result v3

    if-ne v2, v3, :cond_0

    .line 119
    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->m()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_b

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/u;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 120
    :cond_2
    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->n()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_c

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/u;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 121
    :cond_3
    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->o()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_d

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/u;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 122
    :cond_4
    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->p()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_e

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/u;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 123
    :cond_5
    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->q()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_f

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/u;->q()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 124
    :cond_6
    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->r()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_10

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->r()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/u;->r()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 125
    :cond_7
    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->u()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_11

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->u()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/u;->u()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 127
    :cond_8
    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->v()Ljava/lang/Double;

    move-result-object v2

    if-eqz v2, :cond_12

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->v()Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/u;->v()Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Double;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 128
    :cond_9
    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->w()Ljava/lang/Double;

    move-result-object v2

    if-eqz v2, :cond_13

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->w()Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/d/u;->w()Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Double;->equals(Ljava/lang/Object;)Z

    move-result v0

    :cond_a
    :goto_1
    move v1, v0

    goto/16 :goto_0

    .line 119
    :cond_b
    invoke-virtual {p1}, Lco/uk/getmondo/d/u;->m()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    goto/16 :goto_0

    .line 120
    :cond_c
    invoke-virtual {p1}, Lco/uk/getmondo/d/u;->n()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    goto/16 :goto_0

    .line 121
    :cond_d
    invoke-virtual {p1}, Lco/uk/getmondo/d/u;->o()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    goto/16 :goto_0

    .line 122
    :cond_e
    invoke-virtual {p1}, Lco/uk/getmondo/d/u;->p()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    goto/16 :goto_0

    .line 123
    :cond_f
    invoke-virtual {p1}, Lco/uk/getmondo/d/u;->q()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    goto/16 :goto_0

    .line 124
    :cond_10
    invoke-virtual {p1}, Lco/uk/getmondo/d/u;->r()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    goto/16 :goto_0

    .line 125
    :cond_11
    invoke-virtual {p1}, Lco/uk/getmondo/d/u;->u()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_8

    goto/16 :goto_0

    .line 127
    :cond_12
    invoke-virtual {p1}, Lco/uk/getmondo/d/u;->v()Ljava/lang/Double;

    move-result-object v2

    if-eqz v2, :cond_9

    goto/16 :goto_0

    .line 128
    :cond_13
    invoke-virtual {p1}, Lco/uk/getmondo/d/u;->w()Ljava/lang/Double;

    move-result-object v2

    if-eqz v2, :cond_a

    move v0, v1

    goto :goto_1
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 76
    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->q()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/k/p;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->o()Ljava/lang/String;

    move-result-object v0

    .line 79
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public f(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/u;->category:Ljava/lang/String;

    return-void
.end method

.method public g(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/u;->formattedAddress:Ljava/lang/String;

    return-void
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 83
    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->t()Z

    move-result v0

    return v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 134
    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 135
    :goto_0
    mul-int/lit8 v3, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    .line 136
    mul-int/lit8 v3, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->o()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v3

    .line 137
    mul-int/lit8 v3, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->p()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v3

    .line 138
    mul-int/lit8 v3, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->q()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->q()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v3

    .line 139
    mul-int/lit8 v3, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->r()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->r()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v3

    .line 140
    mul-int/lit8 v3, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->s()Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v2

    :goto_6
    add-int/2addr v0, v3

    .line 141
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->t()Z

    move-result v3

    if-eqz v3, :cond_8

    :goto_7
    add-int/2addr v0, v2

    .line 142
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->u()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->u()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_8
    add-int/2addr v0, v2

    .line 143
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->v()Ljava/lang/Double;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->v()Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->hashCode()I

    move-result v0

    :goto_9
    add-int/2addr v0, v2

    .line 144
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->w()Ljava/lang/Double;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->w()Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Double;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 145
    return v0

    :cond_1
    move v0, v1

    .line 134
    goto/16 :goto_0

    :cond_2
    move v0, v1

    .line 135
    goto/16 :goto_1

    :cond_3
    move v0, v1

    .line 136
    goto/16 :goto_2

    :cond_4
    move v0, v1

    .line 137
    goto :goto_3

    :cond_5
    move v0, v1

    .line 138
    goto :goto_4

    :cond_6
    move v0, v1

    .line 139
    goto :goto_5

    :cond_7
    move v0, v1

    .line 140
    goto :goto_6

    :cond_8
    move v2, v1

    .line 141
    goto :goto_7

    :cond_9
    move v0, v1

    .line 142
    goto :goto_8

    :cond_a
    move v0, v1

    .line 143
    goto :goto_9
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->s()Z

    move-result v0

    return v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    invoke-virtual {p0}, Lco/uk/getmondo/d/u;->q()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/u;->id:Ljava/lang/String;

    return-object v0
.end method

.method public n()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/u;->groupId:Ljava/lang/String;

    return-object v0
.end method

.method public o()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/u;->name:Ljava/lang/String;

    return-object v0
.end method

.method public p()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/u;->logoUrl:Ljava/lang/String;

    return-object v0
.end method

.method public q()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/u;->emoji:Ljava/lang/String;

    return-object v0
.end method

.method public r()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/u;->category:Ljava/lang/String;

    return-object v0
.end method

.method public s()Z
    .locals 1

    iget-boolean v0, p0, Lco/uk/getmondo/d/u;->online:Z

    return v0
.end method

.method public t()Z
    .locals 1

    iget-boolean v0, p0, Lco/uk/getmondo/d/u;->atm:Z

    return v0
.end method

.method public u()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/u;->formattedAddress:Ljava/lang/String;

    return-object v0
.end method

.method public v()Ljava/lang/Double;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/u;->latitude:Ljava/lang/Double;

    return-object v0
.end method

.method public w()Ljava/lang/Double;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/u;->longitude:Ljava/lang/Double;

    return-object v0
.end method
