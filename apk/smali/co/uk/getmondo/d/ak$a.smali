.class public final enum Lco/uk/getmondo/d/ak$a;
.super Ljava/lang/Enum;
.source "User.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/d/ak;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/d/ak$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\t\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\t\u00a8\u0006\n"
    }
    d2 = {
        "Lco/uk/getmondo/model/User$State;",
        "",
        "(Ljava/lang/String;I)V",
        "NO_PROFILE",
        "ON_WAITLIST",
        "INELIGIBLE",
        "ACCOUNT_CREATION",
        "INITIAL_TOPUP_REQUIRED",
        "NO_CARD",
        "HAS_ACCOUNT",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lco/uk/getmondo/d/ak$a;

.field public static final enum ACCOUNT_CREATION:Lco/uk/getmondo/d/ak$a;

.field public static final enum HAS_ACCOUNT:Lco/uk/getmondo/d/ak$a;

.field public static final enum INELIGIBLE:Lco/uk/getmondo/d/ak$a;

.field public static final enum INITIAL_TOPUP_REQUIRED:Lco/uk/getmondo/d/ak$a;

.field public static final enum NO_CARD:Lco/uk/getmondo/d/ak$a;

.field public static final enum NO_PROFILE:Lco/uk/getmondo/d/ak$a;

.field public static final enum ON_WAITLIST:Lco/uk/getmondo/d/ak$a;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x7

    new-array v0, v0, [Lco/uk/getmondo/d/ak$a;

    new-instance v1, Lco/uk/getmondo/d/ak$a;

    const-string v2, "NO_PROFILE"

    invoke-direct {v1, v2, v3}, Lco/uk/getmondo/d/ak$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/d/ak$a;->NO_PROFILE:Lco/uk/getmondo/d/ak$a;

    aput-object v1, v0, v3

    new-instance v1, Lco/uk/getmondo/d/ak$a;

    const-string v2, "ON_WAITLIST"

    invoke-direct {v1, v2, v4}, Lco/uk/getmondo/d/ak$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/d/ak$a;->ON_WAITLIST:Lco/uk/getmondo/d/ak$a;

    aput-object v1, v0, v4

    new-instance v1, Lco/uk/getmondo/d/ak$a;

    const-string v2, "INELIGIBLE"

    invoke-direct {v1, v2, v5}, Lco/uk/getmondo/d/ak$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/d/ak$a;->INELIGIBLE:Lco/uk/getmondo/d/ak$a;

    aput-object v1, v0, v5

    new-instance v1, Lco/uk/getmondo/d/ak$a;

    const-string v2, "ACCOUNT_CREATION"

    invoke-direct {v1, v2, v6}, Lco/uk/getmondo/d/ak$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/d/ak$a;->ACCOUNT_CREATION:Lco/uk/getmondo/d/ak$a;

    aput-object v1, v0, v6

    new-instance v1, Lco/uk/getmondo/d/ak$a;

    const-string v2, "INITIAL_TOPUP_REQUIRED"

    invoke-direct {v1, v2, v7}, Lco/uk/getmondo/d/ak$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/d/ak$a;->INITIAL_TOPUP_REQUIRED:Lco/uk/getmondo/d/ak$a;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-instance v2, Lco/uk/getmondo/d/ak$a;

    const-string v3, "NO_CARD"

    const/4 v4, 0x5

    invoke-direct {v2, v3, v4}, Lco/uk/getmondo/d/ak$a;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lco/uk/getmondo/d/ak$a;->NO_CARD:Lco/uk/getmondo/d/ak$a;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lco/uk/getmondo/d/ak$a;

    const-string v3, "HAS_ACCOUNT"

    const/4 v4, 0x6

    invoke-direct {v2, v3, v4}, Lco/uk/getmondo/d/ak$a;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lco/uk/getmondo/d/ak$a;->HAS_ACCOUNT:Lco/uk/getmondo/d/ak$a;

    aput-object v2, v0, v1

    sput-object v0, Lco/uk/getmondo/d/ak$a;->$VALUES:[Lco/uk/getmondo/d/ak$a;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/d/ak$a;
    .locals 1

    const-class v0, Lco/uk/getmondo/d/ak$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/ak$a;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/d/ak$a;
    .locals 1

    sget-object v0, Lco/uk/getmondo/d/ak$a;->$VALUES:[Lco/uk/getmondo/d/ak$a;

    invoke-virtual {v0}, [Lco/uk/getmondo/d/ak$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/d/ak$a;

    return-object v0
.end method
