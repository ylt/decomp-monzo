.class public Lco/uk/getmondo/d/v;
.super Ljava/lang/Object;
.source "MonthlySpendingReport.java"

# interfaces
.implements Lio/realm/aj;
.implements Lio/realm/bb;


# instance fields
.field private amountCurrency:Ljava/lang/String;

.field private amountValue:J

.field private id:Ljava/lang/String;

.field private month:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/l;

    invoke-interface {p0}, Lio/realm/internal/l;->u_()V

    .line 17
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->u_()V

    .line 20
    :cond_0
    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/v;->a(Ljava/lang/String;)V

    .line 21
    invoke-virtual {p0, p2, p3}, Lco/uk/getmondo/d/v;->a(J)V

    .line 22
    invoke-virtual {p0, p4}, Lco/uk/getmondo/d/v;->b(Ljava/lang/String;)V

    .line 23
    invoke-virtual {p0, p5}, Lco/uk/getmondo/d/v;->c(Ljava/lang/String;)V

    .line 24
    return-void
.end method


# virtual methods
.method public a()Lco/uk/getmondo/d/c;
    .locals 4

    .prologue
    .line 27
    new-instance v0, Lco/uk/getmondo/d/c;

    invoke-virtual {p0}, Lco/uk/getmondo/d/v;->d()J

    move-result-wide v2

    invoke-virtual {p0}, Lco/uk/getmondo/d/v;->e()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Lco/uk/getmondo/d/c;-><init>(JLjava/lang/String;)V

    return-object v0
.end method

.method public a(J)V
    .locals 1

    iput-wide p1, p0, Lco/uk/getmondo/d/v;->amountValue:J

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/v;->id:Ljava/lang/String;

    return-void
.end method

.method public b()Lorg/threeten/bp/YearMonth;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lco/uk/getmondo/d/v;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/threeten/bp/YearMonth;->a(Ljava/lang/CharSequence;)Lorg/threeten/bp/YearMonth;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/v;->amountCurrency:Ljava/lang/String;

    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/v;->id:Ljava/lang/String;

    return-object v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/v;->month:Ljava/lang/String;

    return-void
.end method

.method public d()J
    .locals 2

    iget-wide v0, p0, Lco/uk/getmondo/d/v;->amountValue:J

    return-wide v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/v;->amountCurrency:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 36
    if-ne p0, p1, :cond_1

    move v1, v0

    .line 43
    :cond_0
    :goto_0
    return v1

    .line 37
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    .line 38
    check-cast p1, Lco/uk/getmondo/d/v;

    .line 40
    invoke-virtual {p0}, Lco/uk/getmondo/d/v;->d()J

    move-result-wide v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/v;->d()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 41
    invoke-virtual {p0}, Lco/uk/getmondo/d/v;->c()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Lco/uk/getmondo/d/v;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/v;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 42
    :cond_2
    invoke-virtual {p0}, Lco/uk/getmondo/d/v;->e()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-virtual {p0}, Lco/uk/getmondo/d/v;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/v;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 43
    :cond_3
    invoke-virtual {p0}, Lco/uk/getmondo/d/v;->f()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {p0}, Lco/uk/getmondo/d/v;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/d/v;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :cond_4
    :goto_1
    move v1, v0

    goto :goto_0

    .line 41
    :cond_5
    invoke-virtual {p1}, Lco/uk/getmondo/d/v;->c()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    goto :goto_0

    .line 42
    :cond_6
    invoke-virtual {p1}, Lco/uk/getmondo/d/v;->e()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    goto :goto_0

    .line 43
    :cond_7
    invoke-virtual {p1}, Lco/uk/getmondo/d/v;->f()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_1
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/v;->month:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-virtual {p0}, Lco/uk/getmondo/d/v;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lco/uk/getmondo/d/v;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 50
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/v;->d()J

    move-result-wide v2

    invoke-virtual {p0}, Lco/uk/getmondo/d/v;->d()J

    move-result-wide v4

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 51
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/v;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lco/uk/getmondo/d/v;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 52
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/v;->f()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/d/v;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 53
    return v0

    :cond_1
    move v0, v1

    .line 49
    goto :goto_0

    :cond_2
    move v0, v1

    .line 51
    goto :goto_1
.end method
