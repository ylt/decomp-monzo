.class public Lco/uk/getmondo/d/p;
.super Ljava/lang/Object;
.source "InboundP2P.java"

# interfaces
.implements Lio/realm/ab;
.implements Lio/realm/bb;


# static fields
.field public static final ID:Ljava/lang/String; = "inbound_p2p_storage_static_id"


# instance fields
.field private enabled:Z

.field private id:Ljava/lang/String;

.field private ineligibilityReason:Ljava/lang/String;

.field private max:I

.field private min:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->u_()V

    .line 12
    :cond_0
    const-string v0, "inbound_p2p_storage_static_id"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/p;->a(Ljava/lang/String;)V

    .line 21
    return-void
.end method

.method public constructor <init>(ZIILjava/lang/String;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->u_()V

    .line 12
    :cond_0
    const-string v0, "inbound_p2p_storage_static_id"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/p;->a(Ljava/lang/String;)V

    .line 24
    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/p;->a(Z)V

    .line 25
    invoke-virtual {p0, p2}, Lco/uk/getmondo/d/p;->a(I)V

    .line 26
    invoke-virtual {p0, p3}, Lco/uk/getmondo/d/p;->b(I)V

    .line 27
    invoke-virtual {p0, p4}, Lco/uk/getmondo/d/p;->b(Ljava/lang/String;)V

    .line 28
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Lco/uk/getmondo/d/p;->e()I

    move-result v0

    return v0
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lco/uk/getmondo/d/p;->max:I

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/p;->id:Ljava/lang/String;

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lco/uk/getmondo/d/p;->enabled:Z

    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lco/uk/getmondo/d/p;->f()I

    move-result v0

    return v0
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, Lco/uk/getmondo/d/p;->min:I

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/p;->ineligibilityReason:Ljava/lang/String;

    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/p;->id:Ljava/lang/String;

    return-object v0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lco/uk/getmondo/d/p;->enabled:Z

    return v0
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lco/uk/getmondo/d/p;->max:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 49
    if-ne p0, p1, :cond_1

    move v1, v0

    .line 57
    :cond_0
    :goto_0
    return v1

    .line 50
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    .line 52
    check-cast p1, Lco/uk/getmondo/d/p;

    .line 54
    invoke-virtual {p0}, Lco/uk/getmondo/d/p;->d()Z

    move-result v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/p;->d()Z

    move-result v3

    if-ne v2, v3, :cond_0

    .line 55
    invoke-virtual {p0}, Lco/uk/getmondo/d/p;->e()I

    move-result v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/p;->e()I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 56
    invoke-virtual {p0}, Lco/uk/getmondo/d/p;->f()I

    move-result v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/p;->f()I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 57
    invoke-virtual {p0}, Lco/uk/getmondo/d/p;->g()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lco/uk/getmondo/d/p;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/d/p;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :cond_2
    :goto_1
    move v1, v0

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lco/uk/getmondo/d/p;->g()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    move v0, v1

    goto :goto_1
.end method

.method public f()I
    .locals 1

    iget v0, p0, Lco/uk/getmondo/d/p;->min:I

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/p;->ineligibilityReason:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 62
    invoke-virtual {p0}, Lco/uk/getmondo/d/p;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 63
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/p;->e()I

    move-result v2

    add-int/2addr v0, v2

    .line 64
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/p;->f()I

    move-result v2

    add-int/2addr v0, v2

    .line 65
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/p;->g()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/d/p;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 66
    return v0

    :cond_1
    move v0, v1

    .line 62
    goto :goto_0
.end method
