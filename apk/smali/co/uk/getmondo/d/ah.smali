.class public final enum Lco/uk/getmondo/d/ah;
.super Ljava/lang/Enum;
.source "SddMigrationType.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/d/ah$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/d/ah;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0008\u0008\u0086\u0001\u0018\u0000 \n2\u0008\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\nB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\t\u00a8\u0006\u000b"
    }
    d2 = {
        "Lco/uk/getmondo/model/SddMigrationType;",
        "",
        "apiValue",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "getApiValue",
        "()Ljava/lang/String;",
        "DISMISSIBLE",
        "PERSISTENT",
        "NONE",
        "Find",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lco/uk/getmondo/d/ah;

.field public static final enum DISMISSIBLE:Lco/uk/getmondo/d/ah;

.field public static final Find:Lco/uk/getmondo/d/ah$a;

.field public static final enum NONE:Lco/uk/getmondo/d/ah;

.field public static final enum PERSISTENT:Lco/uk/getmondo/d/ah;


# instance fields
.field private final apiValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x3

    new-array v0, v0, [Lco/uk/getmondo/d/ah;

    new-instance v1, Lco/uk/getmondo/d/ah;

    const-string v2, "DISMISSIBLE"

    .line 4
    const-string v3, "dismissible"

    invoke-direct {v1, v2, v4, v3}, Lco/uk/getmondo/d/ah;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/d/ah;->DISMISSIBLE:Lco/uk/getmondo/d/ah;

    aput-object v1, v0, v4

    new-instance v1, Lco/uk/getmondo/d/ah;

    const-string v2, "PERSISTENT"

    .line 5
    const-string v3, "persistent"

    invoke-direct {v1, v2, v5, v3}, Lco/uk/getmondo/d/ah;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/d/ah;->PERSISTENT:Lco/uk/getmondo/d/ah;

    aput-object v1, v0, v5

    new-instance v1, Lco/uk/getmondo/d/ah;

    const-string v2, "NONE"

    .line 6
    const-string v3, ""

    invoke-direct {v1, v2, v6, v3}, Lco/uk/getmondo/d/ah;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/d/ah;->NONE:Lco/uk/getmondo/d/ah;

    aput-object v1, v0, v6

    sput-object v0, Lco/uk/getmondo/d/ah;->$VALUES:[Lco/uk/getmondo/d/ah;

    new-instance v0, Lco/uk/getmondo/d/ah$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/d/ah$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/d/ah;->Find:Lco/uk/getmondo/d/ah$a;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "apiValue"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lco/uk/getmondo/d/ah;->apiValue:Ljava/lang/String;

    return-void
.end method

.method public static final a(Ljava/lang/String;)Lco/uk/getmondo/d/ah;
    .locals 1

    sget-object v0, Lco/uk/getmondo/d/ah;->Find:Lco/uk/getmondo/d/ah$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/d/ah$a;->a(Ljava/lang/String;)Lco/uk/getmondo/d/ah;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/d/ah;
    .locals 1

    const-class v0, Lco/uk/getmondo/d/ah;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/ah;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/d/ah;
    .locals 1

    sget-object v0, Lco/uk/getmondo/d/ah;->$VALUES:[Lco/uk/getmondo/d/ah;

    invoke-virtual {v0}, [Lco/uk/getmondo/d/ah;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/d/ah;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3
    iget-object v0, p0, Lco/uk/getmondo/d/ah;->apiValue:Ljava/lang/String;

    return-object v0
.end method
