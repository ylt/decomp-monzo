.class public abstract Lco/uk/getmondo/d/x;
.super Ljava/lang/Object;
.source "PaymentLimits.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/d/x$a;,
        Lco/uk/getmondo/d/x$b;,
        Lco/uk/getmondo/d/x$c;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0003\u0006\u0007\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lco/uk/getmondo/model/PaymentLimit;",
        "",
        "()V",
        "AmountLimit",
        "CountLimit",
        "VirtualLimit",
        "Lco/uk/getmondo/model/PaymentLimit$AmountLimit;",
        "Lco/uk/getmondo/model/PaymentLimit$CountLimit;",
        "Lco/uk/getmondo/model/PaymentLimit$VirtualLimit;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lco/uk/getmondo/d/x;-><init>()V

    return-void
.end method
