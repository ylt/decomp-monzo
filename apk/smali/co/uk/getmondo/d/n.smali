.class public Lco/uk/getmondo/d/n;
.super Ljava/lang/Object;
.source "FeedMetadata.java"

# interfaces
.implements Lio/realm/bb;
.implements Lio/realm/v;


# instance fields
.field private id:Ljava/lang/String;

.field private lastFetched:Ljava/util/Date;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->u_()V

    .line 11
    :cond_0
    const-string v0, "feed_metadata_id"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/n;->a(Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public constructor <init>(Ljava/util/Date;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->u_()V

    .line 11
    :cond_0
    const-string v0, "feed_metadata_id"

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/n;->a(Ljava/lang/String;)V

    .line 19
    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/n;->a(Ljava/util/Date;)V

    .line 20
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/n;->id:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/n;->id:Ljava/lang/String;

    return-void
.end method

.method public a(Ljava/util/Date;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/n;->lastFetched:Ljava/util/Date;

    return-void
.end method

.method public b()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/n;->lastFetched:Ljava/util/Date;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 24
    if-ne p0, p1, :cond_1

    .line 30
    :cond_0
    :goto_0
    return v0

    .line 25
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 27
    :cond_3
    check-cast p1, Lco/uk/getmondo/d/n;

    .line 29
    invoke-virtual {p0}, Lco/uk/getmondo/d/n;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Lco/uk/getmondo/d/n;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/n;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    invoke-virtual {p1}, Lco/uk/getmondo/d/n;->a()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_4

    .line 30
    :cond_6
    invoke-virtual {p0}, Lco/uk/getmondo/d/n;->b()Ljava/util/Date;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {p0}, Lco/uk/getmondo/d/n;->b()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/d/n;->b()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_7
    invoke-virtual {p1}, Lco/uk/getmondo/d/n;->b()Ljava/util/Date;

    move-result-object v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-virtual {p0}, Lco/uk/getmondo/d/n;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lco/uk/getmondo/d/n;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 36
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/n;->b()Ljava/util/Date;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/d/n;->b()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Date;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 37
    return v0

    :cond_1
    move v0, v1

    .line 35
    goto :goto_0
.end method
