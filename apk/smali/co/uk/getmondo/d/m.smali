.class public Lco/uk/getmondo/d/m;
.super Ljava/lang/Object;
.source "FeedItem.java"

# interfaces
.implements Lio/realm/bb;
.implements Lio/realm/t;


# instance fields
.field private accountId:Ljava/lang/String;

.field private appUri:Ljava/lang/String;

.field private basicItemInfo:Lco/uk/getmondo/d/f;

.field private created:Ljava/util/Date;

.field private goldenTicket:Lco/uk/getmondo/d/o;

.field private id:Ljava/lang/String;

.field private isDismissable:Z

.field private kycRejected:Lco/uk/getmondo/d/r;

.field private monthlySpendingReport:Lco/uk/getmondo/d/v;

.field private sddRejectedReason:Ljava/lang/String;

.field private transaction:Lco/uk/getmondo/d/aj;

.field private type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/l;

    invoke-interface {p0}, Lio/realm/internal/l;->u_()V

    .line 31
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/o;Lco/uk/getmondo/d/v;Lco/uk/getmondo/d/aj;Lco/uk/getmondo/d/r;Ljava/lang/String;Lco/uk/getmondo/d/f;Z)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->u_()V

    .line 35
    :cond_0
    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/m;->a(Ljava/lang/String;)V

    .line 36
    invoke-virtual {p0, p2}, Lco/uk/getmondo/d/m;->b(Ljava/lang/String;)V

    .line 37
    invoke-virtual {p0, p3}, Lco/uk/getmondo/d/m;->a(Ljava/util/Date;)V

    .line 38
    invoke-virtual {p0, p4}, Lco/uk/getmondo/d/m;->c(Ljava/lang/String;)V

    .line 39
    invoke-virtual {p0, p5}, Lco/uk/getmondo/d/m;->d(Ljava/lang/String;)V

    .line 40
    invoke-virtual {p0, p6}, Lco/uk/getmondo/d/m;->a(Lco/uk/getmondo/d/o;)V

    .line 41
    invoke-virtual {p0, p7}, Lco/uk/getmondo/d/m;->a(Lco/uk/getmondo/d/v;)V

    .line 42
    invoke-virtual {p0, p8}, Lco/uk/getmondo/d/m;->a(Lco/uk/getmondo/d/aj;)V

    .line 43
    invoke-virtual {p0, p9}, Lco/uk/getmondo/d/m;->a(Lco/uk/getmondo/d/r;)V

    .line 44
    invoke-virtual {p0, p10}, Lco/uk/getmondo/d/m;->e(Ljava/lang/String;)V

    .line 45
    invoke-virtual {p0, p11}, Lco/uk/getmondo/d/m;->a(Lco/uk/getmondo/d/f;)V

    .line 46
    invoke-virtual {p0, p12}, Lco/uk/getmondo/d/m;->a(Z)V

    .line 47
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lco/uk/getmondo/d/aj;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/m;->transaction:Lco/uk/getmondo/d/aj;

    return-void
.end method

.method public a(Lco/uk/getmondo/d/f;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/m;->basicItemInfo:Lco/uk/getmondo/d/f;

    return-void
.end method

.method public a(Lco/uk/getmondo/d/o;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/m;->goldenTicket:Lco/uk/getmondo/d/o;

    return-void
.end method

.method public a(Lco/uk/getmondo/d/r;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/m;->kycRejected:Lco/uk/getmondo/d/r;

    return-void
.end method

.method public a(Lco/uk/getmondo/d/v;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/m;->monthlySpendingReport:Lco/uk/getmondo/d/v;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/m;->id:Ljava/lang/String;

    return-void
.end method

.method public a(Ljava/util/Date;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/m;->created:Ljava/util/Date;

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lco/uk/getmondo/d/m;->isDismissable:Z

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/m;->accountId:Ljava/lang/String;

    return-void
.end method

.method public c()Ljava/util/Date;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->n()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/m;->type:Ljava/lang/String;

    return-void
.end method

.method public d()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->p()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public d(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/m;->appUri:Ljava/lang/String;

    return-void
.end method

.method public e()Lcom/c/b/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/c/b/b",
            "<",
            "Lco/uk/getmondo/d/aj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->s()Lco/uk/getmondo/d/aj;

    move-result-object v0

    invoke-static {v0}, Lcom/c/b/b;->b(Ljava/lang/Object;)Lcom/c/b/b;

    move-result-object v0

    return-object v0
.end method

.method public e(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/m;->sddRejectedReason:Ljava/lang/String;

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 105
    if-ne p0, p1, :cond_1

    move v1, v0

    .line 123
    :cond_0
    :goto_0
    return v1

    .line 106
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    .line 108
    check-cast p1, Lco/uk/getmondo/d/m;

    .line 110
    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->w()Z

    move-result v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->w()Z

    move-result v3

    if-ne v2, v3, :cond_0

    .line 111
    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->l()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_d

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 112
    :cond_2
    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->m()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_e

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 113
    :cond_3
    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->n()Ljava/util/Date;

    move-result-object v2

    if-eqz v2, :cond_f

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->n()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->n()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 114
    :cond_4
    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->o()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_10

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 115
    :cond_5
    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->p()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_11

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 116
    :cond_6
    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->q()Lco/uk/getmondo/d/o;

    move-result-object v2

    if-eqz v2, :cond_12

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->q()Lco/uk/getmondo/d/o;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->q()Lco/uk/getmondo/d/o;

    move-result-object v3

    invoke-virtual {v2, v3}, Lco/uk/getmondo/d/o;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 117
    :cond_7
    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->r()Lco/uk/getmondo/d/v;

    move-result-object v2

    if-eqz v2, :cond_13

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->r()Lco/uk/getmondo/d/v;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->r()Lco/uk/getmondo/d/v;

    move-result-object v3

    invoke-virtual {v2, v3}, Lco/uk/getmondo/d/v;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 119
    :cond_8
    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->s()Lco/uk/getmondo/d/aj;

    move-result-object v2

    if-eqz v2, :cond_14

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->s()Lco/uk/getmondo/d/aj;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->s()Lco/uk/getmondo/d/aj;

    move-result-object v3

    invoke-virtual {v2, v3}, Lco/uk/getmondo/d/aj;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 120
    :cond_9
    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->t()Lco/uk/getmondo/d/r;

    move-result-object v2

    if-eqz v2, :cond_15

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->t()Lco/uk/getmondo/d/r;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->t()Lco/uk/getmondo/d/r;

    move-result-object v3

    invoke-virtual {v2, v3}, Lco/uk/getmondo/d/r;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 121
    :cond_a
    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->u()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_16

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->u()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->u()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 123
    :cond_b
    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->v()Lco/uk/getmondo/d/f;

    move-result-object v2

    if-eqz v2, :cond_17

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->v()Lco/uk/getmondo/d/f;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->v()Lco/uk/getmondo/d/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/d/f;->equals(Ljava/lang/Object;)Z

    move-result v0

    :cond_c
    :goto_1
    move v1, v0

    goto/16 :goto_0

    .line 111
    :cond_d
    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->l()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    goto/16 :goto_0

    .line 112
    :cond_e
    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->m()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    goto/16 :goto_0

    .line 113
    :cond_f
    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->n()Ljava/util/Date;

    move-result-object v2

    if-eqz v2, :cond_4

    goto/16 :goto_0

    .line 114
    :cond_10
    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->o()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    goto/16 :goto_0

    .line 115
    :cond_11
    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->p()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    goto/16 :goto_0

    .line 116
    :cond_12
    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->q()Lco/uk/getmondo/d/o;

    move-result-object v2

    if-eqz v2, :cond_7

    goto/16 :goto_0

    .line 117
    :cond_13
    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->r()Lco/uk/getmondo/d/v;

    move-result-object v2

    if-eqz v2, :cond_8

    goto/16 :goto_0

    .line 119
    :cond_14
    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->s()Lco/uk/getmondo/d/aj;

    move-result-object v2

    if-eqz v2, :cond_9

    goto/16 :goto_0

    .line 120
    :cond_15
    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->t()Lco/uk/getmondo/d/r;

    move-result-object v2

    if-eqz v2, :cond_a

    goto/16 :goto_0

    .line 121
    :cond_16
    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->u()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_b

    goto/16 :goto_0

    .line 123
    :cond_17
    invoke-virtual {p1}, Lco/uk/getmondo/d/m;->v()Lco/uk/getmondo/d/f;

    move-result-object v2

    if-eqz v2, :cond_c

    move v0, v1

    goto :goto_1
.end method

.method public f()Lco/uk/getmondo/d/o;
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->q()Lco/uk/getmondo/d/o;

    move-result-object v0

    return-object v0
.end method

.method public g()Lco/uk/getmondo/d/v;
    .locals 1

    .prologue
    .line 77
    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->r()Lco/uk/getmondo/d/v;

    move-result-object v0

    return-object v0
.end method

.method public h()Lco/uk/getmondo/d/r;
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->t()Lco/uk/getmondo/d/r;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 129
    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->l()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 130
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 131
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->n()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->n()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    .line 132
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->o()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    .line 133
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->p()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    .line 134
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->q()Lco/uk/getmondo/d/o;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->q()Lco/uk/getmondo/d/o;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/o;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v2

    .line 135
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->r()Lco/uk/getmondo/d/v;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->r()Lco/uk/getmondo/d/v;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/v;->hashCode()I

    move-result v0

    :goto_6
    add-int/2addr v0, v2

    .line 136
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->s()Lco/uk/getmondo/d/aj;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->s()Lco/uk/getmondo/d/aj;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/aj;->hashCode()I

    move-result v0

    :goto_7
    add-int/2addr v0, v2

    .line 137
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->t()Lco/uk/getmondo/d/r;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->t()Lco/uk/getmondo/d/r;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/r;->hashCode()I

    move-result v0

    :goto_8
    add-int/2addr v0, v2

    .line 138
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->u()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->u()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_9
    add-int/2addr v0, v2

    .line 139
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->v()Lco/uk/getmondo/d/f;

    move-result-object v0

    if-eqz v0, :cond_b

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->v()Lco/uk/getmondo/d/f;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/f;->hashCode()I

    move-result v0

    :goto_a
    add-int/2addr v0, v2

    .line 140
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->w()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    add-int/2addr v0, v1

    .line 141
    return v0

    :cond_1
    move v0, v1

    .line 129
    goto/16 :goto_0

    :cond_2
    move v0, v1

    .line 130
    goto/16 :goto_1

    :cond_3
    move v0, v1

    .line 131
    goto/16 :goto_2

    :cond_4
    move v0, v1

    .line 132
    goto/16 :goto_3

    :cond_5
    move v0, v1

    .line 133
    goto :goto_4

    :cond_6
    move v0, v1

    .line 134
    goto :goto_5

    :cond_7
    move v0, v1

    .line 135
    goto :goto_6

    :cond_8
    move v0, v1

    .line 136
    goto :goto_7

    :cond_9
    move v0, v1

    .line 137
    goto :goto_8

    :cond_a
    move v0, v1

    .line 138
    goto :goto_9

    :cond_b
    move v0, v1

    .line 139
    goto :goto_a
.end method

.method public i()Lco/uk/getmondo/d/f;
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->v()Lco/uk/getmondo/d/f;

    move-result-object v0

    return-object v0
.end method

.method public j()Lco/uk/getmondo/feed/a/a/a;
    .locals 2

    .prologue
    .line 96
    sget-object v0, Lco/uk/getmondo/feed/a/a/a;->o:Lco/uk/getmondo/feed/a/a/a$a;

    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/feed/a/a/a$a;->a(Ljava/lang/String;)Lco/uk/getmondo/feed/a/a/a;

    move-result-object v0

    return-object v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 100
    invoke-virtual {p0}, Lco/uk/getmondo/d/m;->w()Z

    move-result v0

    return v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/m;->id:Ljava/lang/String;

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/m;->accountId:Ljava/lang/String;

    return-object v0
.end method

.method public n()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/m;->created:Ljava/util/Date;

    return-object v0
.end method

.method public o()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/m;->type:Ljava/lang/String;

    return-object v0
.end method

.method public p()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/m;->appUri:Ljava/lang/String;

    return-object v0
.end method

.method public q()Lco/uk/getmondo/d/o;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/m;->goldenTicket:Lco/uk/getmondo/d/o;

    return-object v0
.end method

.method public r()Lco/uk/getmondo/d/v;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/m;->monthlySpendingReport:Lco/uk/getmondo/d/v;

    return-object v0
.end method

.method public s()Lco/uk/getmondo/d/aj;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/m;->transaction:Lco/uk/getmondo/d/aj;

    return-object v0
.end method

.method public t()Lco/uk/getmondo/d/r;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/m;->kycRejected:Lco/uk/getmondo/d/r;

    return-object v0
.end method

.method public u()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/m;->sddRejectedReason:Ljava/lang/String;

    return-object v0
.end method

.method public v()Lco/uk/getmondo/d/f;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/m;->basicItemInfo:Lco/uk/getmondo/d/f;

    return-object v0
.end method

.method public w()Z
    .locals 1

    iget-boolean v0, p0, Lco/uk/getmondo/d/m;->isDismissable:Z

    return v0
.end method
