.class public final enum Lco/uk/getmondo/d/h;
.super Ljava/lang/Enum;
.source "Category.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/d/h;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lco/uk/getmondo/d/h;

.field public static final enum BILLS:Lco/uk/getmondo/d/h;

.field public static final enum CASH:Lco/uk/getmondo/d/h;

.field public static final enum EATING_OUT:Lco/uk/getmondo/d/h;

.field public static final enum ENTERTAINMENT:Lco/uk/getmondo/d/h;

.field public static final enum EXPENSES:Lco/uk/getmondo/d/h;

.field public static final enum GENERAL:Lco/uk/getmondo/d/h;

.field public static final enum GROCERIES:Lco/uk/getmondo/d/h;

.field public static final enum HOLIDAYS:Lco/uk/getmondo/d/h;

.field public static final enum SHOPPING:Lco/uk/getmondo/d/h;

.field public static final enum TRANSPORT:Lco/uk/getmondo/d/h;


# instance fields
.field private final apiValue:Ljava/lang/String;

.field private final colorResource:I

.field private final iconInverseLargeResource:I

.field private final iconInverseResource:I

.field private final iconResource:I

.field private final iconRound:I

.field private final iconRoundFull:I

.field private final nameResource:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    .line 11
    new-instance v0, Lco/uk/getmondo/d/h;

    const-string v1, "TRANSPORT"

    const/4 v2, 0x0

    const-string v3, "transport"

    const v4, 0x7f0a011b

    const v5, 0x7f020145

    const v6, 0x7f020146

    const v7, 0x7f020147

    const v8, 0x7f020148

    const v9, 0x7f020149

    const v10, 0x7f0f0035

    invoke-direct/range {v0 .. v10}, Lco/uk/getmondo/d/h;-><init>(Ljava/lang/String;ILjava/lang/String;IIIIIII)V

    sput-object v0, Lco/uk/getmondo/d/h;->TRANSPORT:Lco/uk/getmondo/d/h;

    .line 19
    new-instance v0, Lco/uk/getmondo/d/h;

    const-string v1, "GROCERIES"

    const/4 v2, 0x1

    const-string v3, "groceries"

    const v4, 0x7f0a0118

    const v5, 0x7f020136

    const v6, 0x7f020137

    const v7, 0x7f020138

    const v8, 0x7f020139

    const v9, 0x7f02013a

    const v10, 0x7f0f0032

    invoke-direct/range {v0 .. v10}, Lco/uk/getmondo/d/h;-><init>(Ljava/lang/String;ILjava/lang/String;IIIIIII)V

    sput-object v0, Lco/uk/getmondo/d/h;->GROCERIES:Lco/uk/getmondo/d/h;

    .line 27
    new-instance v0, Lco/uk/getmondo/d/h;

    const-string v1, "EATING_OUT"

    const/4 v2, 0x2

    const-string v3, "eating_out"

    const v4, 0x7f0a0114

    const v5, 0x7f020122

    const v6, 0x7f020123

    const v7, 0x7f020124

    const v8, 0x7f020125

    const v9, 0x7f020126

    const v10, 0x7f0f002e

    invoke-direct/range {v0 .. v10}, Lco/uk/getmondo/d/h;-><init>(Ljava/lang/String;ILjava/lang/String;IIIIIII)V

    sput-object v0, Lco/uk/getmondo/d/h;->EATING_OUT:Lco/uk/getmondo/d/h;

    .line 35
    new-instance v0, Lco/uk/getmondo/d/h;

    const-string v1, "CASH"

    const/4 v2, 0x3

    const-string v3, "cash"

    const v4, 0x7f0a0113

    const v5, 0x7f02011c

    const v6, 0x7f02011e

    const v7, 0x7f02011f

    const v8, 0x7f020120

    const v9, 0x7f020121

    const v10, 0x7f0f002d

    invoke-direct/range {v0 .. v10}, Lco/uk/getmondo/d/h;-><init>(Ljava/lang/String;ILjava/lang/String;IIIIIII)V

    sput-object v0, Lco/uk/getmondo/d/h;->CASH:Lco/uk/getmondo/d/h;

    .line 43
    new-instance v0, Lco/uk/getmondo/d/h;

    const-string v1, "BILLS"

    const/4 v2, 0x4

    const-string v3, "bills"

    const v4, 0x7f0a0112

    const v5, 0x7f020117

    const v6, 0x7f020118

    const v7, 0x7f020119

    const v8, 0x7f02011a

    const v9, 0x7f02011b

    const v10, 0x7f0f002c

    invoke-direct/range {v0 .. v10}, Lco/uk/getmondo/d/h;-><init>(Ljava/lang/String;ILjava/lang/String;IIIIIII)V

    sput-object v0, Lco/uk/getmondo/d/h;->BILLS:Lco/uk/getmondo/d/h;

    .line 51
    new-instance v0, Lco/uk/getmondo/d/h;

    const-string v1, "ENTERTAINMENT"

    const/4 v2, 0x5

    const-string v3, "entertainment"

    const v4, 0x7f0a0115

    const v5, 0x7f020127

    const v6, 0x7f020128

    const v7, 0x7f020129

    const v8, 0x7f02012a

    const v9, 0x7f02012b

    const v10, 0x7f0f002f

    invoke-direct/range {v0 .. v10}, Lco/uk/getmondo/d/h;-><init>(Ljava/lang/String;ILjava/lang/String;IIIIIII)V

    sput-object v0, Lco/uk/getmondo/d/h;->ENTERTAINMENT:Lco/uk/getmondo/d/h;

    .line 59
    new-instance v0, Lco/uk/getmondo/d/h;

    const-string v1, "SHOPPING"

    const/4 v2, 0x6

    const-string v3, "shopping"

    const v4, 0x7f0a011a

    const v5, 0x7f020140

    const v6, 0x7f020141

    const v7, 0x7f020142

    const v8, 0x7f020143

    const v9, 0x7f020144

    const v10, 0x7f0f0034

    invoke-direct/range {v0 .. v10}, Lco/uk/getmondo/d/h;-><init>(Ljava/lang/String;ILjava/lang/String;IIIIIII)V

    sput-object v0, Lco/uk/getmondo/d/h;->SHOPPING:Lco/uk/getmondo/d/h;

    .line 67
    new-instance v0, Lco/uk/getmondo/d/h;

    const-string v1, "HOLIDAYS"

    const/4 v2, 0x7

    const-string v3, "holidays"

    const v4, 0x7f0a0119

    const v5, 0x7f02013b

    const v6, 0x7f02013c

    const v7, 0x7f02013d

    const v8, 0x7f02013e

    const v9, 0x7f02013f

    const v10, 0x7f0f0033

    invoke-direct/range {v0 .. v10}, Lco/uk/getmondo/d/h;-><init>(Ljava/lang/String;ILjava/lang/String;IIIIIII)V

    sput-object v0, Lco/uk/getmondo/d/h;->HOLIDAYS:Lco/uk/getmondo/d/h;

    .line 75
    new-instance v0, Lco/uk/getmondo/d/h;

    const-string v1, "GENERAL"

    const/16 v2, 0x8

    const-string v3, "general"

    const v4, 0x7f0a0117

    const v5, 0x7f020131

    const v6, 0x7f020132

    const v7, 0x7f020133

    const v8, 0x7f020134

    const v9, 0x7f020135

    const v10, 0x7f0f0031

    invoke-direct/range {v0 .. v10}, Lco/uk/getmondo/d/h;-><init>(Ljava/lang/String;ILjava/lang/String;IIIIIII)V

    sput-object v0, Lco/uk/getmondo/d/h;->GENERAL:Lco/uk/getmondo/d/h;

    .line 83
    new-instance v0, Lco/uk/getmondo/d/h;

    const-string v1, "EXPENSES"

    const/16 v2, 0x9

    const-string v3, "expenses"

    const v4, 0x7f0a0116

    const v5, 0x7f02012c

    const v6, 0x7f02012d

    const v7, 0x7f02012e

    const v8, 0x7f02012f

    const v9, 0x7f020130

    const v10, 0x7f0f0030

    invoke-direct/range {v0 .. v10}, Lco/uk/getmondo/d/h;-><init>(Ljava/lang/String;ILjava/lang/String;IIIIIII)V

    sput-object v0, Lco/uk/getmondo/d/h;->EXPENSES:Lco/uk/getmondo/d/h;

    .line 9
    const/16 v0, 0xa

    new-array v0, v0, [Lco/uk/getmondo/d/h;

    const/4 v1, 0x0

    sget-object v2, Lco/uk/getmondo/d/h;->TRANSPORT:Lco/uk/getmondo/d/h;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lco/uk/getmondo/d/h;->GROCERIES:Lco/uk/getmondo/d/h;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lco/uk/getmondo/d/h;->EATING_OUT:Lco/uk/getmondo/d/h;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lco/uk/getmondo/d/h;->CASH:Lco/uk/getmondo/d/h;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lco/uk/getmondo/d/h;->BILLS:Lco/uk/getmondo/d/h;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lco/uk/getmondo/d/h;->ENTERTAINMENT:Lco/uk/getmondo/d/h;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lco/uk/getmondo/d/h;->SHOPPING:Lco/uk/getmondo/d/h;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lco/uk/getmondo/d/h;->HOLIDAYS:Lco/uk/getmondo/d/h;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lco/uk/getmondo/d/h;->GENERAL:Lco/uk/getmondo/d/h;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lco/uk/getmondo/d/h;->EXPENSES:Lco/uk/getmondo/d/h;

    aput-object v2, v0, v1

    sput-object v0, Lco/uk/getmondo/d/h;->$VALUES:[Lco/uk/getmondo/d/h;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;IIIIIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IIIIIII)V"
        }
    .end annotation

    .prologue
    .line 104
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 105
    iput-object p3, p0, Lco/uk/getmondo/d/h;->apiValue:Ljava/lang/String;

    .line 106
    iput p4, p0, Lco/uk/getmondo/d/h;->nameResource:I

    .line 107
    iput p5, p0, Lco/uk/getmondo/d/h;->iconResource:I

    .line 108
    iput p6, p0, Lco/uk/getmondo/d/h;->iconInverseResource:I

    .line 109
    iput p7, p0, Lco/uk/getmondo/d/h;->iconInverseLargeResource:I

    .line 110
    iput p8, p0, Lco/uk/getmondo/d/h;->iconRound:I

    .line 111
    iput p9, p0, Lco/uk/getmondo/d/h;->iconRoundFull:I

    .line 112
    iput p10, p0, Lco/uk/getmondo/d/h;->colorResource:I

    .line 113
    return-void
.end method

.method public static a(Ljava/lang/String;)Lco/uk/getmondo/d/h;
    .locals 5

    .prologue
    .line 116
    invoke-static {}, Lco/uk/getmondo/d/h;->values()[Lco/uk/getmondo/d/h;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 117
    iget-object v4, v0, Lco/uk/getmondo/d/h;->apiValue:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 122
    :goto_1
    return-object v0

    .line 116
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 122
    :cond_1
    sget-object v0, Lco/uk/getmondo/d/h;->GENERAL:Lco/uk/getmondo/d/h;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/d/h;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lco/uk/getmondo/d/h;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/h;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/d/h;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lco/uk/getmondo/d/h;->$VALUES:[Lco/uk/getmondo/d/h;

    invoke-virtual {v0}, [Lco/uk/getmondo/d/h;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/d/h;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lco/uk/getmondo/d/h;->nameResource:I

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 137
    iget v0, p0, Lco/uk/getmondo/d/h;->colorResource:I

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Lco/uk/getmondo/d/h;->iconInverseResource:I

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Lco/uk/getmondo/d/h;->iconInverseLargeResource:I

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lco/uk/getmondo/d/h;->iconRoundFull:I

    return v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lco/uk/getmondo/d/h;->apiValue:Ljava/lang/String;

    return-object v0
.end method
