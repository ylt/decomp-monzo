.class public Lco/uk/getmondo/d/f;
.super Lio/realm/bc;
.source "BasicItemInfo.kt"

# interfaces
.implements Lio/realm/i;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\u0008\u0016\u0018\u00002\u00020\u0001B)\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0007B\u0005\u00a2\u0006\u0002\u0010\u0008J\u0013\u0010\u0011\u001a\u00020\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0096\u0002J\u0008\u0010\u0015\u001a\u00020\u0016H\u0016R\u0012\u0010\u0002\u001a\u00020\u00038\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0006\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\t\u0010\n\"\u0004\u0008\u000b\u0010\u000cR\u001a\u0010\u0005\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\r\u0010\n\"\u0004\u0008\u000e\u0010\u000cR\u001a\u0010\u0004\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000f\u0010\n\"\u0004\u0008\u0010\u0010\u000c\u00a8\u0006\u0017"
    }
    d2 = {
        "Lco/uk/getmondo/model/BasicItemInfo;",
        "Lio/realm/RealmObject;",
        "id",
        "",
        "title",
        "subtitle",
        "imageUrl",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V",
        "()V",
        "getImageUrl",
        "()Ljava/lang/String;",
        "setImageUrl",
        "(Ljava/lang/String;)V",
        "getSubtitle",
        "setSubtitle",
        "getTitle",
        "setTitle",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private id:Ljava/lang/String;

.field private imageUrl:Ljava/lang/String;

.field private subtitle:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Lio/realm/bc;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->u_()V

    .line 9
    :cond_0
    const-string v0, ""

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/f;->a(Ljava/lang/String;)V

    .line 10
    const-string v0, ""

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/f;->b(Ljava/lang/String;)V

    .line 11
    const-string v0, ""

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/f;->c(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "title"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "subtitle"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Lco/uk/getmondo/d/f;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->u_()V

    .line 15
    :cond_0
    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/f;->a(Ljava/lang/String;)V

    .line 16
    invoke-virtual {p0, p2}, Lco/uk/getmondo/d/f;->b(Ljava/lang/String;)V

    .line 17
    invoke-virtual {p0, p3}, Lco/uk/getmondo/d/f;->c(Ljava/lang/String;)V

    .line 18
    invoke-virtual {p0, p4}, Lco/uk/getmondo/d/f;->d(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0}, Lco/uk/getmondo/d/f;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/f;->id:Ljava/lang/String;

    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lco/uk/getmondo/d/f;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/f;->title:Ljava/lang/String;

    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lco/uk/getmondo/d/f;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/f;->subtitle:Ljava/lang/String;

    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/f;->id:Ljava/lang/String;

    return-object v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/f;->imageUrl:Ljava/lang/String;

    return-void
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/f;->title:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 22
    move-object v0, p0

    check-cast v0, Lco/uk/getmondo/d/f;

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 32
    :goto_0
    return v0

    .line 23
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    :goto_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 25
    :cond_2
    if-nez p1, :cond_3

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.model.BasicItemInfo"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/d/f;

    .line 27
    invoke-virtual {p0}, Lco/uk/getmondo/d/f;->d()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/d/f;

    invoke-virtual {v0}, Lco/uk/getmondo/d/f;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    move v0, v2

    goto :goto_0

    .line 28
    :cond_4
    invoke-virtual {p0}, Lco/uk/getmondo/d/f;->e()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/d/f;

    invoke-virtual {v0}, Lco/uk/getmondo/d/f;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_5

    move v0, v2

    goto :goto_0

    .line 29
    :cond_5
    invoke-virtual {p0}, Lco/uk/getmondo/d/f;->f()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/d/f;

    invoke-virtual {v0}, Lco/uk/getmondo/d/f;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_6

    move v0, v2

    goto :goto_0

    .line 30
    :cond_6
    invoke-virtual {p0}, Lco/uk/getmondo/d/f;->g()Ljava/lang/String;

    move-result-object v0

    check-cast p1, Lco/uk/getmondo/d/f;

    invoke-virtual {p1}, Lco/uk/getmondo/d/f;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_7

    move v0, v2

    goto :goto_0

    :cond_7
    move v0, v1

    .line 32
    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/f;->subtitle:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/f;->imageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 36
    invoke-virtual {p0}, Lco/uk/getmondo/d/f;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 37
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/f;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 38
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/f;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 39
    mul-int/lit8 v1, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/f;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v1

    .line 40
    return v0

    .line 39
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
