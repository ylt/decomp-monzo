.class public final Lco/uk/getmondo/d/e;
.super Ljava/lang/Object;
.source "BalanceLimit.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008B\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u00ad\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\u0006\u0010\u0008\u001a\u00020\u0005\u0012\u0006\u0010\t\u001a\u00020\u0005\u0012\u0006\u0010\n\u001a\u00020\u0005\u0012\u0006\u0010\u000b\u001a\u00020\u0005\u0012\u0006\u0010\u000c\u001a\u00020\u0005\u0012\u0006\u0010\r\u001a\u00020\u0005\u0012\u0006\u0010\u000e\u001a\u00020\u0005\u0012\u0006\u0010\u000f\u001a\u00020\u0005\u0012\u0006\u0010\u0010\u001a\u00020\u0005\u0012\u0006\u0010\u0011\u001a\u00020\u0005\u0012\u0006\u0010\u0012\u001a\u00020\u0005\u0012\u0006\u0010\u0013\u001a\u00020\u0005\u0012\u0006\u0010\u0014\u001a\u00020\u0005\u0012\u0006\u0010\u0015\u001a\u00020\u0005\u0012\u0006\u0010\u0016\u001a\u00020\u0005\u0012\u0006\u0010\u0017\u001a\u00020\u0005\u0012\u0006\u0010\u0018\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0019J\t\u00101\u001a\u00020\u0003H\u00c6\u0003J\t\u00102\u001a\u00020\u0005H\u00c6\u0003J\t\u00103\u001a\u00020\u0005H\u00c6\u0003J\t\u00104\u001a\u00020\u0005H\u00c6\u0003J\t\u00105\u001a\u00020\u0005H\u00c6\u0003J\t\u00106\u001a\u00020\u0005H\u00c6\u0003J\t\u00107\u001a\u00020\u0005H\u00c6\u0003J\t\u00108\u001a\u00020\u0005H\u00c6\u0003J\t\u00109\u001a\u00020\u0005H\u00c6\u0003J\t\u0010:\u001a\u00020\u0005H\u00c6\u0003J\t\u0010;\u001a\u00020\u0005H\u00c6\u0003J\t\u0010<\u001a\u00020\u0005H\u00c6\u0003J\t\u0010=\u001a\u00020\u0005H\u00c6\u0003J\t\u0010>\u001a\u00020\u0005H\u00c6\u0003J\t\u0010?\u001a\u00020\u0005H\u00c6\u0003J\t\u0010@\u001a\u00020\u0005H\u00c6\u0003J\t\u0010A\u001a\u00020\u0005H\u00c6\u0003J\t\u0010B\u001a\u00020\u0005H\u00c6\u0003J\t\u0010C\u001a\u00020\u0005H\u00c6\u0003J\t\u0010D\u001a\u00020\u0005H\u00c6\u0003J\t\u0010E\u001a\u00020\u0005H\u00c6\u0003J\u00db\u0001\u0010F\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00052\u0008\u0008\u0002\u0010\t\u001a\u00020\u00052\u0008\u0008\u0002\u0010\n\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u000c\u001a\u00020\u00052\u0008\u0008\u0002\u0010\r\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u000f\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0010\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0011\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0012\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0013\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0014\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0015\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0016\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0017\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0018\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010G\u001a\u00020H2\u0008\u0010I\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010J\u001a\u00020KH\u00d6\u0001J\t\u0010L\u001a\u00020MH\u00d6\u0001R\u0011\u0010\u0011\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001bR\u0011\u0010\u0012\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u001bR\u0011\u0010\r\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001bR\u0011\u0010\u000e\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u001bR\u0011\u0010\u000b\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010\u001bR\u0011\u0010\u000c\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010\u001bR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008!\u0010\u001bR\u0011\u0010\u0007\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\"\u0010\u001bR\u0011\u0010\u0008\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008#\u0010\u001bR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008$\u0010\u001bR\u0011\u0010\t\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008%\u0010\u001bR\u0011\u0010\n\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008&\u0010\u001bR\u0011\u0010\u0017\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\'\u0010\u001bR\u0011\u0010\u0018\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008(\u0010\u001bR\u0011\u0010\u0016\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008)\u0010\u001bR\u0011\u0010\u0014\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008*\u0010\u001bR\u0011\u0010\u0015\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008+\u0010\u001bR\u0011\u0010\u0013\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008,\u0010\u001bR\u0011\u0010\u000f\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008-\u0010\u001bR\u0011\u0010\u0010\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008.\u0010\u001bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008/\u00100\u00a8\u0006N"
    }
    d2 = {
        "Lco/uk/getmondo/model/BalanceLimit;",
        "",
        "verificationType",
        "Lco/uk/getmondo/api/model/VerificationType;",
        "maxSingleCardPayment",
        "Lco/uk/getmondo/model/Amount;",
        "maxBalance",
        "maxDailyTopUp",
        "maxDailyTopUpRemaining",
        "maxTopUpOver30Days",
        "maxTopUpOver30DaysRemaining",
        "maxAnnualTopUp",
        "maxAnnualTopUpRemaining",
        "dailyWithdrawalLimit",
        "dailyWithdrawalLimitRemaining",
        "rolling30DayLimit",
        "rolling30DayLimitRemaining",
        "annualWithdrawalLimit",
        "annualWithdrawalLimitRemaining",
        "paymentsMaxSinglePayment",
        "paymentsMaxReceivedOver30Days",
        "paymentsMaxReceivedOver30DaysRemaining",
        "monzoMeMaxSinglePayment",
        "monzoMeMaxReceivedOver30Days",
        "monzoMeMaxReceivedOver30DaysRemaining",
        "(Lco/uk/getmondo/api/model/VerificationType;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/model/Amount;)V",
        "getAnnualWithdrawalLimit",
        "()Lco/uk/getmondo/model/Amount;",
        "getAnnualWithdrawalLimitRemaining",
        "getDailyWithdrawalLimit",
        "getDailyWithdrawalLimitRemaining",
        "getMaxAnnualTopUp",
        "getMaxAnnualTopUpRemaining",
        "getMaxBalance",
        "getMaxDailyTopUp",
        "getMaxDailyTopUpRemaining",
        "getMaxSingleCardPayment",
        "getMaxTopUpOver30Days",
        "getMaxTopUpOver30DaysRemaining",
        "getMonzoMeMaxReceivedOver30Days",
        "getMonzoMeMaxReceivedOver30DaysRemaining",
        "getMonzoMeMaxSinglePayment",
        "getPaymentsMaxReceivedOver30Days",
        "getPaymentsMaxReceivedOver30DaysRemaining",
        "getPaymentsMaxSinglePayment",
        "getRolling30DayLimit",
        "getRolling30DayLimitRemaining",
        "getVerificationType",
        "()Lco/uk/getmondo/api/model/VerificationType;",
        "component1",
        "component10",
        "component11",
        "component12",
        "component13",
        "component14",
        "component15",
        "component16",
        "component17",
        "component18",
        "component19",
        "component2",
        "component20",
        "component21",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final annualWithdrawalLimit:Lco/uk/getmondo/d/c;

.field private final annualWithdrawalLimitRemaining:Lco/uk/getmondo/d/c;

.field private final dailyWithdrawalLimit:Lco/uk/getmondo/d/c;

.field private final dailyWithdrawalLimitRemaining:Lco/uk/getmondo/d/c;

.field private final maxAnnualTopUp:Lco/uk/getmondo/d/c;

.field private final maxAnnualTopUpRemaining:Lco/uk/getmondo/d/c;

.field private final maxBalance:Lco/uk/getmondo/d/c;

.field private final maxDailyTopUp:Lco/uk/getmondo/d/c;

.field private final maxDailyTopUpRemaining:Lco/uk/getmondo/d/c;

.field private final maxSingleCardPayment:Lco/uk/getmondo/d/c;

.field private final maxTopUpOver30Days:Lco/uk/getmondo/d/c;

.field private final maxTopUpOver30DaysRemaining:Lco/uk/getmondo/d/c;

.field private final monzoMeMaxReceivedOver30Days:Lco/uk/getmondo/d/c;

.field private final monzoMeMaxReceivedOver30DaysRemaining:Lco/uk/getmondo/d/c;

.field private final monzoMeMaxSinglePayment:Lco/uk/getmondo/d/c;

.field private final paymentsMaxReceivedOver30Days:Lco/uk/getmondo/d/c;

.field private final paymentsMaxReceivedOver30DaysRemaining:Lco/uk/getmondo/d/c;

.field private final paymentsMaxSinglePayment:Lco/uk/getmondo/d/c;

.field private final rolling30DayLimit:Lco/uk/getmondo/d/c;

.field private final rolling30DayLimitRemaining:Lco/uk/getmondo/d/c;

.field private final verificationType:Lco/uk/getmondo/api/model/VerificationType;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/api/model/VerificationType;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;)V
    .locals 2

    .prologue
    const-string v1, "verificationType"

    invoke-static {p1, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "maxSingleCardPayment"

    invoke-static {p2, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "maxBalance"

    invoke-static {p3, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "maxDailyTopUp"

    invoke-static {p4, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "maxDailyTopUpRemaining"

    invoke-static {p5, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "maxTopUpOver30Days"

    invoke-static {p6, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "maxTopUpOver30DaysRemaining"

    invoke-static {p7, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "maxAnnualTopUp"

    invoke-static {p8, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "maxAnnualTopUpRemaining"

    invoke-static {p9, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "dailyWithdrawalLimit"

    invoke-static {p10, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "dailyWithdrawalLimitRemaining"

    invoke-static {p11, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "rolling30DayLimit"

    invoke-static {p12, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "rolling30DayLimitRemaining"

    invoke-static {p13, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "annualWithdrawalLimit"

    move-object/from16 v0, p14

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "annualWithdrawalLimitRemaining"

    move-object/from16 v0, p15

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "paymentsMaxSinglePayment"

    move-object/from16 v0, p16

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "paymentsMaxReceivedOver30Days"

    move-object/from16 v0, p17

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "paymentsMaxReceivedOver30DaysRemaining"

    move-object/from16 v0, p18

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "monzoMeMaxSinglePayment"

    move-object/from16 v0, p19

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "monzoMeMaxReceivedOver30Days"

    move-object/from16 v0, p20

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "monzoMeMaxReceivedOver30DaysRemaining"

    move-object/from16 v0, p21

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/d/e;->verificationType:Lco/uk/getmondo/api/model/VerificationType;

    iput-object p2, p0, Lco/uk/getmondo/d/e;->maxSingleCardPayment:Lco/uk/getmondo/d/c;

    iput-object p3, p0, Lco/uk/getmondo/d/e;->maxBalance:Lco/uk/getmondo/d/c;

    iput-object p4, p0, Lco/uk/getmondo/d/e;->maxDailyTopUp:Lco/uk/getmondo/d/c;

    iput-object p5, p0, Lco/uk/getmondo/d/e;->maxDailyTopUpRemaining:Lco/uk/getmondo/d/c;

    iput-object p6, p0, Lco/uk/getmondo/d/e;->maxTopUpOver30Days:Lco/uk/getmondo/d/c;

    iput-object p7, p0, Lco/uk/getmondo/d/e;->maxTopUpOver30DaysRemaining:Lco/uk/getmondo/d/c;

    iput-object p8, p0, Lco/uk/getmondo/d/e;->maxAnnualTopUp:Lco/uk/getmondo/d/c;

    iput-object p9, p0, Lco/uk/getmondo/d/e;->maxAnnualTopUpRemaining:Lco/uk/getmondo/d/c;

    iput-object p10, p0, Lco/uk/getmondo/d/e;->dailyWithdrawalLimit:Lco/uk/getmondo/d/c;

    iput-object p11, p0, Lco/uk/getmondo/d/e;->dailyWithdrawalLimitRemaining:Lco/uk/getmondo/d/c;

    iput-object p12, p0, Lco/uk/getmondo/d/e;->rolling30DayLimit:Lco/uk/getmondo/d/c;

    iput-object p13, p0, Lco/uk/getmondo/d/e;->rolling30DayLimitRemaining:Lco/uk/getmondo/d/c;

    move-object/from16 v0, p14

    iput-object v0, p0, Lco/uk/getmondo/d/e;->annualWithdrawalLimit:Lco/uk/getmondo/d/c;

    move-object/from16 v0, p15

    iput-object v0, p0, Lco/uk/getmondo/d/e;->annualWithdrawalLimitRemaining:Lco/uk/getmondo/d/c;

    move-object/from16 v0, p16

    iput-object v0, p0, Lco/uk/getmondo/d/e;->paymentsMaxSinglePayment:Lco/uk/getmondo/d/c;

    move-object/from16 v0, p17

    iput-object v0, p0, Lco/uk/getmondo/d/e;->paymentsMaxReceivedOver30Days:Lco/uk/getmondo/d/c;

    move-object/from16 v0, p18

    iput-object v0, p0, Lco/uk/getmondo/d/e;->paymentsMaxReceivedOver30DaysRemaining:Lco/uk/getmondo/d/c;

    move-object/from16 v0, p19

    iput-object v0, p0, Lco/uk/getmondo/d/e;->monzoMeMaxSinglePayment:Lco/uk/getmondo/d/c;

    move-object/from16 v0, p20

    iput-object v0, p0, Lco/uk/getmondo/d/e;->monzoMeMaxReceivedOver30Days:Lco/uk/getmondo/d/c;

    move-object/from16 v0, p21

    iput-object v0, p0, Lco/uk/getmondo/d/e;->monzoMeMaxReceivedOver30DaysRemaining:Lco/uk/getmondo/d/c;

    return-void
.end method


# virtual methods
.method public final a()Lco/uk/getmondo/api/model/VerificationType;
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lco/uk/getmondo/d/e;->verificationType:Lco/uk/getmondo/api/model/VerificationType;

    return-object v0
.end method

.method public final b()Lco/uk/getmondo/d/c;
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lco/uk/getmondo/d/e;->maxSingleCardPayment:Lco/uk/getmondo/d/c;

    return-object v0
.end method

.method public final c()Lco/uk/getmondo/d/c;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lco/uk/getmondo/d/e;->maxBalance:Lco/uk/getmondo/d/c;

    return-object v0
.end method

.method public final d()Lco/uk/getmondo/d/c;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lco/uk/getmondo/d/e;->maxDailyTopUp:Lco/uk/getmondo/d/c;

    return-object v0
.end method

.method public final e()Lco/uk/getmondo/d/c;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lco/uk/getmondo/d/e;->maxDailyTopUpRemaining:Lco/uk/getmondo/d/c;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lco/uk/getmondo/d/e;

    if-eqz v0, :cond_1

    check-cast p1, Lco/uk/getmondo/d/e;

    iget-object v0, p0, Lco/uk/getmondo/d/e;->verificationType:Lco/uk/getmondo/api/model/VerificationType;

    iget-object v1, p1, Lco/uk/getmondo/d/e;->verificationType:Lco/uk/getmondo/api/model/VerificationType;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/d/e;->maxSingleCardPayment:Lco/uk/getmondo/d/c;

    iget-object v1, p1, Lco/uk/getmondo/d/e;->maxSingleCardPayment:Lco/uk/getmondo/d/c;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/d/e;->maxBalance:Lco/uk/getmondo/d/c;

    iget-object v1, p1, Lco/uk/getmondo/d/e;->maxBalance:Lco/uk/getmondo/d/c;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/d/e;->maxDailyTopUp:Lco/uk/getmondo/d/c;

    iget-object v1, p1, Lco/uk/getmondo/d/e;->maxDailyTopUp:Lco/uk/getmondo/d/c;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/d/e;->maxDailyTopUpRemaining:Lco/uk/getmondo/d/c;

    iget-object v1, p1, Lco/uk/getmondo/d/e;->maxDailyTopUpRemaining:Lco/uk/getmondo/d/c;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/d/e;->maxTopUpOver30Days:Lco/uk/getmondo/d/c;

    iget-object v1, p1, Lco/uk/getmondo/d/e;->maxTopUpOver30Days:Lco/uk/getmondo/d/c;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/d/e;->maxTopUpOver30DaysRemaining:Lco/uk/getmondo/d/c;

    iget-object v1, p1, Lco/uk/getmondo/d/e;->maxTopUpOver30DaysRemaining:Lco/uk/getmondo/d/c;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/d/e;->maxAnnualTopUp:Lco/uk/getmondo/d/c;

    iget-object v1, p1, Lco/uk/getmondo/d/e;->maxAnnualTopUp:Lco/uk/getmondo/d/c;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/d/e;->maxAnnualTopUpRemaining:Lco/uk/getmondo/d/c;

    iget-object v1, p1, Lco/uk/getmondo/d/e;->maxAnnualTopUpRemaining:Lco/uk/getmondo/d/c;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/d/e;->dailyWithdrawalLimit:Lco/uk/getmondo/d/c;

    iget-object v1, p1, Lco/uk/getmondo/d/e;->dailyWithdrawalLimit:Lco/uk/getmondo/d/c;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/d/e;->dailyWithdrawalLimitRemaining:Lco/uk/getmondo/d/c;

    iget-object v1, p1, Lco/uk/getmondo/d/e;->dailyWithdrawalLimitRemaining:Lco/uk/getmondo/d/c;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/d/e;->rolling30DayLimit:Lco/uk/getmondo/d/c;

    iget-object v1, p1, Lco/uk/getmondo/d/e;->rolling30DayLimit:Lco/uk/getmondo/d/c;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/d/e;->rolling30DayLimitRemaining:Lco/uk/getmondo/d/c;

    iget-object v1, p1, Lco/uk/getmondo/d/e;->rolling30DayLimitRemaining:Lco/uk/getmondo/d/c;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/d/e;->annualWithdrawalLimit:Lco/uk/getmondo/d/c;

    iget-object v1, p1, Lco/uk/getmondo/d/e;->annualWithdrawalLimit:Lco/uk/getmondo/d/c;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/d/e;->annualWithdrawalLimitRemaining:Lco/uk/getmondo/d/c;

    iget-object v1, p1, Lco/uk/getmondo/d/e;->annualWithdrawalLimitRemaining:Lco/uk/getmondo/d/c;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/d/e;->paymentsMaxSinglePayment:Lco/uk/getmondo/d/c;

    iget-object v1, p1, Lco/uk/getmondo/d/e;->paymentsMaxSinglePayment:Lco/uk/getmondo/d/c;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/d/e;->paymentsMaxReceivedOver30Days:Lco/uk/getmondo/d/c;

    iget-object v1, p1, Lco/uk/getmondo/d/e;->paymentsMaxReceivedOver30Days:Lco/uk/getmondo/d/c;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/d/e;->paymentsMaxReceivedOver30DaysRemaining:Lco/uk/getmondo/d/c;

    iget-object v1, p1, Lco/uk/getmondo/d/e;->paymentsMaxReceivedOver30DaysRemaining:Lco/uk/getmondo/d/c;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/d/e;->monzoMeMaxSinglePayment:Lco/uk/getmondo/d/c;

    iget-object v1, p1, Lco/uk/getmondo/d/e;->monzoMeMaxSinglePayment:Lco/uk/getmondo/d/c;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/d/e;->monzoMeMaxReceivedOver30Days:Lco/uk/getmondo/d/c;

    iget-object v1, p1, Lco/uk/getmondo/d/e;->monzoMeMaxReceivedOver30Days:Lco/uk/getmondo/d/c;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/d/e;->monzoMeMaxReceivedOver30DaysRemaining:Lco/uk/getmondo/d/c;

    iget-object v1, p1, Lco/uk/getmondo/d/e;->monzoMeMaxReceivedOver30DaysRemaining:Lco/uk/getmondo/d/c;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lco/uk/getmondo/d/c;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lco/uk/getmondo/d/e;->maxTopUpOver30Days:Lco/uk/getmondo/d/c;

    return-object v0
.end method

.method public final g()Lco/uk/getmondo/d/c;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lco/uk/getmondo/d/e;->maxTopUpOver30DaysRemaining:Lco/uk/getmondo/d/c;

    return-object v0
.end method

.method public final h()Lco/uk/getmondo/d/c;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lco/uk/getmondo/d/e;->maxAnnualTopUp:Lco/uk/getmondo/d/c;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lco/uk/getmondo/d/e;->verificationType:Lco/uk/getmondo/api/model/VerificationType;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/e;->maxSingleCardPayment:Lco/uk/getmondo/d/c;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/e;->maxBalance:Lco/uk/getmondo/d/c;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/e;->maxDailyTopUp:Lco/uk/getmondo/d/c;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/e;->maxDailyTopUpRemaining:Lco/uk/getmondo/d/c;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/e;->maxTopUpOver30Days:Lco/uk/getmondo/d/c;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/e;->maxTopUpOver30DaysRemaining:Lco/uk/getmondo/d/c;

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/e;->maxAnnualTopUp:Lco/uk/getmondo/d/c;

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/e;->maxAnnualTopUpRemaining:Lco/uk/getmondo/d/c;

    if-eqz v0, :cond_9

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/e;->dailyWithdrawalLimit:Lco/uk/getmondo/d/c;

    if-eqz v0, :cond_a

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_9
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/e;->dailyWithdrawalLimitRemaining:Lco/uk/getmondo/d/c;

    if-eqz v0, :cond_b

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_a
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/e;->rolling30DayLimit:Lco/uk/getmondo/d/c;

    if-eqz v0, :cond_c

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_b
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/e;->rolling30DayLimitRemaining:Lco/uk/getmondo/d/c;

    if-eqz v0, :cond_d

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_c
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/e;->annualWithdrawalLimit:Lco/uk/getmondo/d/c;

    if-eqz v0, :cond_e

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_d
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/e;->annualWithdrawalLimitRemaining:Lco/uk/getmondo/d/c;

    if-eqz v0, :cond_f

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_e
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/e;->paymentsMaxSinglePayment:Lco/uk/getmondo/d/c;

    if-eqz v0, :cond_10

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_f
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/e;->paymentsMaxReceivedOver30Days:Lco/uk/getmondo/d/c;

    if-eqz v0, :cond_11

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_10
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/e;->paymentsMaxReceivedOver30DaysRemaining:Lco/uk/getmondo/d/c;

    if-eqz v0, :cond_12

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_11
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/e;->monzoMeMaxSinglePayment:Lco/uk/getmondo/d/c;

    if-eqz v0, :cond_13

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_12
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/e;->monzoMeMaxReceivedOver30Days:Lco/uk/getmondo/d/c;

    if-eqz v0, :cond_14

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_13
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/uk/getmondo/d/e;->monzoMeMaxReceivedOver30DaysRemaining:Lco/uk/getmondo/d/c;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto/16 :goto_0

    :cond_2
    move v0, v1

    goto/16 :goto_1

    :cond_3
    move v0, v1

    goto/16 :goto_2

    :cond_4
    move v0, v1

    goto/16 :goto_3

    :cond_5
    move v0, v1

    goto/16 :goto_4

    :cond_6
    move v0, v1

    goto/16 :goto_5

    :cond_7
    move v0, v1

    goto/16 :goto_6

    :cond_8
    move v0, v1

    goto/16 :goto_7

    :cond_9
    move v0, v1

    goto/16 :goto_8

    :cond_a
    move v0, v1

    goto/16 :goto_9

    :cond_b
    move v0, v1

    goto/16 :goto_a

    :cond_c
    move v0, v1

    goto/16 :goto_b

    :cond_d
    move v0, v1

    goto :goto_c

    :cond_e
    move v0, v1

    goto :goto_d

    :cond_f
    move v0, v1

    goto :goto_e

    :cond_10
    move v0, v1

    goto :goto_f

    :cond_11
    move v0, v1

    goto :goto_10

    :cond_12
    move v0, v1

    goto :goto_11

    :cond_13
    move v0, v1

    goto :goto_12

    :cond_14
    move v0, v1

    goto :goto_13
.end method

.method public final i()Lco/uk/getmondo/d/c;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lco/uk/getmondo/d/e;->maxAnnualTopUpRemaining:Lco/uk/getmondo/d/c;

    return-object v0
.end method

.method public final j()Lco/uk/getmondo/d/c;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lco/uk/getmondo/d/e;->dailyWithdrawalLimit:Lco/uk/getmondo/d/c;

    return-object v0
.end method

.method public final k()Lco/uk/getmondo/d/c;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lco/uk/getmondo/d/e;->dailyWithdrawalLimitRemaining:Lco/uk/getmondo/d/c;

    return-object v0
.end method

.method public final l()Lco/uk/getmondo/d/c;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lco/uk/getmondo/d/e;->rolling30DayLimit:Lco/uk/getmondo/d/c;

    return-object v0
.end method

.method public final m()Lco/uk/getmondo/d/c;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lco/uk/getmondo/d/e;->rolling30DayLimitRemaining:Lco/uk/getmondo/d/c;

    return-object v0
.end method

.method public final n()Lco/uk/getmondo/d/c;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lco/uk/getmondo/d/e;->annualWithdrawalLimit:Lco/uk/getmondo/d/c;

    return-object v0
.end method

.method public final o()Lco/uk/getmondo/d/c;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/d/e;->annualWithdrawalLimitRemaining:Lco/uk/getmondo/d/c;

    return-object v0
.end method

.method public final p()Lco/uk/getmondo/d/c;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lco/uk/getmondo/d/e;->paymentsMaxSinglePayment:Lco/uk/getmondo/d/c;

    return-object v0
.end method

.method public final q()Lco/uk/getmondo/d/c;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lco/uk/getmondo/d/e;->paymentsMaxReceivedOver30Days:Lco/uk/getmondo/d/c;

    return-object v0
.end method

.method public final r()Lco/uk/getmondo/d/c;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lco/uk/getmondo/d/e;->paymentsMaxReceivedOver30DaysRemaining:Lco/uk/getmondo/d/c;

    return-object v0
.end method

.method public final s()Lco/uk/getmondo/d/c;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lco/uk/getmondo/d/e;->monzoMeMaxSinglePayment:Lco/uk/getmondo/d/c;

    return-object v0
.end method

.method public final t()Lco/uk/getmondo/d/c;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lco/uk/getmondo/d/e;->monzoMeMaxReceivedOver30Days:Lco/uk/getmondo/d/c;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BalanceLimit(verificationType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/e;->verificationType:Lco/uk/getmondo/api/model/VerificationType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxSingleCardPayment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/e;->maxSingleCardPayment:Lco/uk/getmondo/d/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxBalance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/e;->maxBalance:Lco/uk/getmondo/d/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxDailyTopUp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/e;->maxDailyTopUp:Lco/uk/getmondo/d/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxDailyTopUpRemaining="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/e;->maxDailyTopUpRemaining:Lco/uk/getmondo/d/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxTopUpOver30Days="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/e;->maxTopUpOver30Days:Lco/uk/getmondo/d/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxTopUpOver30DaysRemaining="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/e;->maxTopUpOver30DaysRemaining:Lco/uk/getmondo/d/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxAnnualTopUp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/e;->maxAnnualTopUp:Lco/uk/getmondo/d/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxAnnualTopUpRemaining="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/e;->maxAnnualTopUpRemaining:Lco/uk/getmondo/d/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dailyWithdrawalLimit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/e;->dailyWithdrawalLimit:Lco/uk/getmondo/d/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dailyWithdrawalLimitRemaining="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/e;->dailyWithdrawalLimitRemaining:Lco/uk/getmondo/d/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", rolling30DayLimit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/e;->rolling30DayLimit:Lco/uk/getmondo/d/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", rolling30DayLimitRemaining="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/e;->rolling30DayLimitRemaining:Lco/uk/getmondo/d/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", annualWithdrawalLimit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/e;->annualWithdrawalLimit:Lco/uk/getmondo/d/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", annualWithdrawalLimitRemaining="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/e;->annualWithdrawalLimitRemaining:Lco/uk/getmondo/d/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", paymentsMaxSinglePayment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/e;->paymentsMaxSinglePayment:Lco/uk/getmondo/d/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", paymentsMaxReceivedOver30Days="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/e;->paymentsMaxReceivedOver30Days:Lco/uk/getmondo/d/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", paymentsMaxReceivedOver30DaysRemaining="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/e;->paymentsMaxReceivedOver30DaysRemaining:Lco/uk/getmondo/d/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", monzoMeMaxSinglePayment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/e;->monzoMeMaxSinglePayment:Lco/uk/getmondo/d/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", monzoMeMaxReceivedOver30Days="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/e;->monzoMeMaxReceivedOver30Days:Lco/uk/getmondo/d/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", monzoMeMaxReceivedOver30DaysRemaining="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/e;->monzoMeMaxReceivedOver30DaysRemaining:Lco/uk/getmondo/d/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()Lco/uk/getmondo/d/c;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/d/e;->monzoMeMaxReceivedOver30DaysRemaining:Lco/uk/getmondo/d/c;

    return-object v0
.end method
