.class public final Lco/uk/getmondo/d/c;
.super Ljava/lang/Object;
.source "Amount.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/d/c$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0006\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\r\n\u0002\u0010\u000b\n\u0002\u0008\u000b\n\u0002\u0010\u0000\n\u0002\u0008\u000c\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0008\u0086\u0008\u0018\u0000 ?2\u00020\u0001:\u0001?B\u0017\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006B\u0017\u0008\u0016\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bB\u000f\u0008\u0014\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000fJ\u0006\u0010\'\u001a\u00020\u0000J\u0006\u0010(\u001a\u00020\u0000J\t\u0010)\u001a\u00020\u0003H\u00c6\u0003J\t\u0010*\u001a\u00020\nH\u00c6\u0003J\u001d\u0010+\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\t\u001a\u00020\nH\u00c6\u0001J\u0008\u0010,\u001a\u00020\u0015H\u0016J\u0013\u0010-\u001a\u00020#2\u0008\u0010.\u001a\u0004\u0018\u00010/H\u00d6\u0003J\t\u00100\u001a\u00020\u0015H\u00d6\u0001J\u0011\u00101\u001a\u00020\u00002\u0006\u00102\u001a\u00020\u0003H\u0086\u0002J\u0011\u00103\u001a\u00020\u00002\u0006\u00104\u001a\u00020\u0003H\u0086\u0002J\u0006\u00105\u001a\u00020\u0000J\u000e\u00106\u001a\u00020\u00002\u0006\u00107\u001a\u00020\u0015J\u0011\u00108\u001a\u00020\u00002\u0006\u00109\u001a\u00020\u0003H\u0086\u0002J\u0008\u0010:\u001a\u00020\u0005H\u0016J\u0018\u0010;\u001a\u00020<2\u0006\u0010=\u001a\u00020\r2\u0006\u0010>\u001a\u00020\u0015H\u0016R\u0011\u0010\u0010\u001a\u00020\u00118F\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\u0014\u001a\u00020\u00158F\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\u0017R\u0011\u0010\u0018\u001a\u00020\u00038F\u00a2\u0006\u0006\u001a\u0004\u0008\u0019\u0010\u001aR\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001cR\u0011\u0010\u001d\u001a\u00020\u00058F\u00a2\u0006\u0006\u001a\u0004\u0008\u001e\u0010\u001fR\u0011\u0010 \u001a\u00020\u00118F\u00a2\u0006\u0006\u001a\u0004\u0008!\u0010\u0013R\u0011\u0010\"\u001a\u00020#8F\u00a2\u0006\u0006\u001a\u0004\u0008\"\u0010$R\u0011\u0010%\u001a\u00020#8F\u00a2\u0006\u0006\u001a\u0004\u0008%\u0010$R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008&\u0010\u001a\u00a8\u0006@"
    }
    d2 = {
        "Lco/uk/getmondo/model/Amount;",
        "Landroid/os/Parcelable;",
        "value",
        "",
        "currencyCode",
        "",
        "(JLjava/lang/String;)V",
        "bigDecimal",
        "Ljava/math/BigDecimal;",
        "currency",
        "Lco/uk/getmondo/common/money/Currency;",
        "(Ljava/math/BigDecimal;Lco/uk/getmondo/common/money/Currency;)V",
        "source",
        "Landroid/os/Parcel;",
        "(Landroid/os/Parcel;)V",
        "(JLco/uk/getmondo/common/money/Currency;)V",
        "absoluteDecimal",
        "",
        "getAbsoluteDecimal",
        "()D",
        "absoluteFractionalPart",
        "",
        "getAbsoluteFractionalPart",
        "()I",
        "absoluteIntegerPart",
        "getAbsoluteIntegerPart",
        "()J",
        "getCurrency",
        "()Lco/uk/getmondo/common/money/Currency;",
        "currencySymbol",
        "getCurrencySymbol",
        "()Ljava/lang/String;",
        "decimal",
        "getDecimal",
        "isDebit",
        "",
        "()Z",
        "isZero",
        "getValue",
        "abs",
        "ceil",
        "component1",
        "component2",
        "copy",
        "describeContents",
        "equals",
        "other",
        "",
        "hashCode",
        "minus",
        "valueToSubtract",
        "plus",
        "valueToAdd",
        "round",
        "split",
        "shares",
        "times",
        "valueToMultiply",
        "toString",
        "writeToParcel",
        "",
        "dest",
        "flags",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lco/uk/getmondo/d/c;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lco/uk/getmondo/d/c$a;

.field private static final defaultFormatter:Lco/uk/getmondo/common/i/b;


# instance fields
.field private final currency:Lco/uk/getmondo/common/i/c;

.field private final value:J


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    new-instance v0, Lco/uk/getmondo/d/c$a;

    invoke-direct {v0, v5}, Lco/uk/getmondo/d/c$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/d/c;->Companion:Lco/uk/getmondo/d/c$a;

    .line 94
    new-instance v0, Lco/uk/getmondo/common/i/b;

    const/4 v2, 0x1

    const/4 v4, 0x5

    move v3, v1

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/common/i/b;-><init>(ZZZILkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/d/c;->defaultFormatter:Lco/uk/getmondo/common/i/b;

    .line 108
    new-instance v0, Lco/uk/getmondo/d/c$b;

    invoke-direct {v0}, Lco/uk/getmondo/d/c$b;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lco/uk/getmondo/d/c;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLco/uk/getmondo/common/i/c;)V
    .locals 1

    .prologue
    const-string v0, "currency"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lco/uk/getmondo/d/c;->value:J

    iput-object p3, p0, Lco/uk/getmondo/d/c;->currency:Lco/uk/getmondo/common/i/c;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;)V
    .locals 1

    .prologue
    const-string v0, "currencyCode"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    new-instance v0, Lco/uk/getmondo/common/i/c;

    invoke-direct {v0, p3}, Lco/uk/getmondo/common/i/c;-><init>(Ljava/lang/String;)V

    .line 12
    invoke-direct {p0, p1, p2, v0}, Lco/uk/getmondo/d/c;-><init>(JLco/uk/getmondo/common/i/c;)V

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 24
    const-class v0, Lco/uk/getmondo/common/i/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    const-string v1, "source.readParcelable<Cu\u2026::class.java.classLoader)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lco/uk/getmondo/common/i/c;

    .line 22
    invoke-direct {p0, v2, v3, v0}, Lco/uk/getmondo/d/c;-><init>(JLco/uk/getmondo/common/i/c;)V

    return-void
.end method

.method public constructor <init>(Ljava/math/BigDecimal;Lco/uk/getmondo/common/i/c;)V
    .locals 2

    .prologue
    const-string v0, "bigDecimal"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currency"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-virtual {p2}, Lco/uk/getmondo/common/i/c;->a()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->scaleByPowerOfTen(I)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v0

    .line 17
    invoke-direct {p0, v0, v1, p2}, Lco/uk/getmondo/d/c;-><init>(JLco/uk/getmondo/common/i/c;)V

    return-void
.end method

.method public static final a(Ljava/lang/String;Lco/uk/getmondo/common/i/c;)Lco/uk/getmondo/d/c;
    .locals 1

    const-string v0, "currency"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lco/uk/getmondo/d/c;->Companion:Lco/uk/getmondo/d/c$a;

    invoke-virtual {v0, p0, p1}, Lco/uk/getmondo/d/c$a;->a(Ljava/lang/String;Lco/uk/getmondo/common/i/c;)Lco/uk/getmondo/d/c;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic m()Lco/uk/getmondo/common/i/b;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lco/uk/getmondo/d/c;->defaultFormatter:Lco/uk/getmondo/common/i/b;

    return-object v0
.end method


# virtual methods
.method public final a(I)Lco/uk/getmondo/d/c;
    .locals 4

    .prologue
    .line 70
    iget-wide v0, p0, Lco/uk/getmondo/d/c;->value:J

    long-to-double v0, v0

    int-to-double v2, p1

    div-double/2addr v0, v2

    .line 71
    invoke-virtual {p0}, Lco/uk/getmondo/d/c;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    :goto_0
    double-to-long v0, v0

    .line 72
    new-instance v2, Lco/uk/getmondo/d/c;

    iget-object v3, p0, Lco/uk/getmondo/d/c;->currency:Lco/uk/getmondo/common/i/c;

    invoke-direct {v2, v0, v1, v3}, Lco/uk/getmondo/d/c;-><init>(JLco/uk/getmondo/common/i/c;)V

    return-object v2

    .line 71
    :cond_0
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    goto :goto_0
.end method

.method public final a(J)Lco/uk/getmondo/d/c;
    .locals 3

    .prologue
    .line 58
    iget-wide v0, p0, Lco/uk/getmondo/d/c;->value:J

    add-long/2addr v0, p1

    iget-object v2, p0, Lco/uk/getmondo/d/c;->currency:Lco/uk/getmondo/common/i/c;

    invoke-virtual {p0, v0, v1, v2}, Lco/uk/getmondo/d/c;->a(JLco/uk/getmondo/common/i/c;)Lco/uk/getmondo/d/c;

    move-result-object v0

    return-object v0
.end method

.method public final a(JLco/uk/getmondo/common/i/c;)Lco/uk/getmondo/d/c;
    .locals 1

    const-string v0, "currency"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lco/uk/getmondo/d/c;

    invoke-direct {v0, p1, p2, p3}, Lco/uk/getmondo/d/c;-><init>(JLco/uk/getmondo/common/i/c;)V

    return-object v0
.end method

.method public final a()Z
    .locals 4

    .prologue
    .line 37
    iget-wide v0, p0, Lco/uk/getmondo/d/c;->value:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(J)Lco/uk/getmondo/d/c;
    .locals 3

    .prologue
    .line 62
    iget-wide v0, p0, Lco/uk/getmondo/d/c;->value:J

    sub-long/2addr v0, p1

    iget-object v2, p0, Lco/uk/getmondo/d/c;->currency:Lco/uk/getmondo/common/i/c;

    invoke-virtual {p0, v0, v1, v2}, Lco/uk/getmondo/d/c;->a(JLco/uk/getmondo/common/i/c;)Lco/uk/getmondo/d/c;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    .line 40
    iget-wide v0, p0, Lco/uk/getmondo/d/c;->value:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()J
    .locals 3

    .prologue
    .line 43
    iget-wide v0, p0, Lco/uk/getmondo/d/c;->value:J

    iget-object v2, p0, Lco/uk/getmondo/d/c;->currency:Lco/uk/getmondo/common/i/c;

    invoke-virtual {v2}, Lco/uk/getmondo/common/i/c;->a()I

    move-result v2

    invoke-static {v0, v1, v2}, Lco/uk/getmondo/common/i/d;->b(JI)J

    move-result-wide v0

    return-wide v0
.end method

.method public final c(J)Lco/uk/getmondo/d/c;
    .locals 3

    .prologue
    .line 66
    iget-wide v0, p0, Lco/uk/getmondo/d/c;->value:J

    mul-long/2addr v0, p1

    iget-object v2, p0, Lco/uk/getmondo/d/c;->currency:Lco/uk/getmondo/common/i/c;

    invoke-virtual {p0, v0, v1, v2}, Lco/uk/getmondo/d/c;->a(JLco/uk/getmondo/common/i/c;)Lco/uk/getmondo/d/c;

    move-result-object v0

    return-object v0
.end method

.method public final d()I
    .locals 3

    .prologue
    .line 46
    iget-wide v0, p0, Lco/uk/getmondo/d/c;->value:J

    iget-object v2, p0, Lco/uk/getmondo/d/c;->currency:Lco/uk/getmondo/common/i/c;

    invoke-virtual {v2}, Lco/uk/getmondo/common/i/c;->a()I

    move-result v2

    invoke-static {v0, v1, v2}, Lco/uk/getmondo/common/i/d;->a(JI)I

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    return v0
.end method

.method public final e()D
    .locals 2

    .prologue
    .line 49
    invoke-virtual {p0}, Lco/uk/getmondo/d/c;->f()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eq p0, p1, :cond_0

    instance-of v2, p1, Lco/uk/getmondo/d/c;

    if-eqz v2, :cond_1

    check-cast p1, Lco/uk/getmondo/d/c;

    iget-wide v2, p0, Lco/uk/getmondo/d/c;->value:J

    iget-wide v4, p1, Lco/uk/getmondo/d/c;->value:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/d/c;->currency:Lco/uk/getmondo/common/i/c;

    iget-object v3, p1, Lco/uk/getmondo/d/c;->currency:Lco/uk/getmondo/common/i/c;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0
.end method

.method public final f()D
    .locals 3

    .prologue
    .line 52
    iget-wide v0, p0, Lco/uk/getmondo/d/c;->value:J

    iget-object v2, p0, Lco/uk/getmondo/d/c;->currency:Lco/uk/getmondo/common/i/c;

    invoke-virtual {v2}, Lco/uk/getmondo/common/i/c;->a()I

    move-result v2

    invoke-static {v0, v1, v2}, Lco/uk/getmondo/common/i/d;->c(JI)D

    move-result-wide v0

    return-wide v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lco/uk/getmondo/d/c;->currency:Lco/uk/getmondo/common/i/c;

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/i/c;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "currency.getSymbol(Locale.ENGLISH)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final h()Lco/uk/getmondo/d/c;
    .locals 4

    .prologue
    .line 76
    iget-wide v0, p0, Lco/uk/getmondo/d/c;->value:J

    iget-object v2, p0, Lco/uk/getmondo/d/c;->currency:Lco/uk/getmondo/common/i/c;

    invoke-virtual {v2}, Lco/uk/getmondo/common/i/c;->a()I

    move-result v2

    invoke-static {v0, v1, v2}, Lco/uk/getmondo/common/i/d;->d(JI)J

    move-result-wide v0

    .line 77
    iget-wide v2, p0, Lco/uk/getmondo/d/c;->value:J

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lco/uk/getmondo/d/c;->currency:Lco/uk/getmondo/common/i/c;

    invoke-virtual {p0, v0, v1, v2}, Lco/uk/getmondo/d/c;->a(JLco/uk/getmondo/common/i/c;)Lco/uk/getmondo/d/c;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method public hashCode()I
    .locals 4

    iget-wide v0, p0, Lco/uk/getmondo/d/c;->value:J

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v0, v0

    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/c;->currency:Lco/uk/getmondo/common/i/c;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Lco/uk/getmondo/d/c;
    .locals 4

    .prologue
    .line 81
    iget-wide v0, p0, Lco/uk/getmondo/d/c;->value:J

    iget-object v2, p0, Lco/uk/getmondo/d/c;->currency:Lco/uk/getmondo/common/i/c;

    invoke-virtual {v2}, Lco/uk/getmondo/common/i/c;->a()I

    move-result v2

    invoke-static {v0, v1, v2}, Lco/uk/getmondo/common/i/d;->e(JI)J

    move-result-wide v0

    .line 82
    iget-wide v2, p0, Lco/uk/getmondo/d/c;->value:J

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lco/uk/getmondo/d/c;->currency:Lco/uk/getmondo/common/i/c;

    invoke-virtual {p0, v0, v1, v2}, Lco/uk/getmondo/d/c;->a(JLco/uk/getmondo/common/i/c;)Lco/uk/getmondo/d/c;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method public final j()Lco/uk/getmondo/d/c;
    .locals 3

    .prologue
    .line 86
    iget-wide v0, p0, Lco/uk/getmondo/d/c;->value:J

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    iget-object v2, p0, Lco/uk/getmondo/d/c;->currency:Lco/uk/getmondo/common/i/c;

    invoke-virtual {p0, v0, v1, v2}, Lco/uk/getmondo/d/c;->a(JLco/uk/getmondo/common/i/c;)Lco/uk/getmondo/d/c;

    move-result-object v0

    return-object v0
.end method

.method public final k()J
    .locals 2

    .prologue
    .line 11
    iget-wide v0, p0, Lco/uk/getmondo/d/c;->value:J

    return-wide v0
.end method

.method public final l()Lco/uk/getmondo/common/i/c;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lco/uk/getmondo/d/c;->currency:Lco/uk/getmondo/common/i/c;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lco/uk/getmondo/d/c;->Companion:Lco/uk/getmondo/d/c$a;

    invoke-static {v0}, Lco/uk/getmondo/d/c$a;->a(Lco/uk/getmondo/d/c$a;)Lco/uk/getmondo/common/i/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lco/uk/getmondo/common/i/b;->a(Lco/uk/getmondo/d/c;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const-string v0, "dest"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    iget-wide v0, p0, Lco/uk/getmondo/d/c;->value:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 29
    iget-object v0, p0, Lco/uk/getmondo/d/c;->currency:Lco/uk/getmondo/common/i/c;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 30
    return-void
.end method
