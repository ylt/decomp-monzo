.class public interface abstract Lco/uk/getmondo/d/a;
.super Ljava/lang/Object;
.source "Account.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/d/a$b;,
        Lco/uk/getmondo/d/a$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008f\u0018\u00002\u00020\u0001:\u0001\u0018J\u0010\u0010\u0016\u001a\u00020\u00002\u0006\u0010\u0017\u001a\u00020\u0003H&R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\tR\u0012\u0010\n\u001a\u00020\u000bX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\rR\u0012\u0010\u000e\u001a\u00020\u000bX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000f\u0010\rR\u0014\u0010\u0010\u001a\u00020\u00038VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0005R\u0014\u0010\u0011\u001a\u00020\u00038VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0011\u0010\u0005R\u0014\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0015\u00a8\u0006\u0019"
    }
    d2 = {
        "Lco/uk/getmondo/model/Account;",
        "",
        "cardActivated",
        "",
        "getCardActivated",
        "()Z",
        "created",
        "Lorg/threeten/bp/LocalDateTime;",
        "getCreated",
        "()Lorg/threeten/bp/LocalDateTime;",
        "description",
        "",
        "getDescription",
        "()Ljava/lang/String;",
        "id",
        "getId",
        "isPrepaidWithCard",
        "isRetail",
        "type",
        "Lco/uk/getmondo/model/Account$Type;",
        "getType",
        "()Lco/uk/getmondo/model/Account$Type;",
        "withCardActivated",
        "isActivated",
        "Type",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# virtual methods
.method public abstract a(Z)Lco/uk/getmondo/d/a;
.end method

.method public abstract a()Ljava/lang/String;
.end method

.method public abstract b()Lorg/threeten/bp/LocalDateTime;
.end method

.method public abstract c()Z
.end method

.method public abstract d()Lco/uk/getmondo/d/a$b;
.end method

.method public abstract e()Z
.end method

.method public abstract f()Z
.end method
