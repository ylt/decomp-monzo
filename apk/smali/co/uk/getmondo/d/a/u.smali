.class public final Lco/uk/getmondo/d/a/u;
.super Ljava/lang/Object;
.source "TransactionMapper_Factory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/d/a/t;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final accountManagerProvider:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/accounts/b;",
            ">;"
        }
    .end annotation
.end field

.field private final declineReasonStringProvider:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/feed/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-class v0, Lco/uk/getmondo/d/a/u;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/d/a/u;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/a/a;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/feed/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/accounts/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    sget-boolean v0, Lco/uk/getmondo/d/a/u;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 22
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/d/a/u;->declineReasonStringProvider:Ljavax/a/a;

    .line 23
    sget-boolean v0, Lco/uk/getmondo/d/a/u;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 24
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/d/a/u;->accountManagerProvider:Ljavax/a/a;

    .line 25
    return-void
.end method

.method public static a(Ljavax/a/a;Ljavax/a/a;)Lb/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/feed/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/accounts/b;",
            ">;)",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/d/a/t;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    new-instance v0, Lco/uk/getmondo/d/a/u;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/d/a/u;-><init>(Ljavax/a/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/d/a/t;
    .locals 3

    .prologue
    .line 29
    new-instance v2, Lco/uk/getmondo/d/a/t;

    iget-object v0, p0, Lco/uk/getmondo/d/a/u;->declineReasonStringProvider:Ljavax/a/a;

    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/feed/a;

    iget-object v1, p0, Lco/uk/getmondo/d/a/u;->accountManagerProvider:Ljavax/a/a;

    invoke-interface {v1}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/common/accounts/b;

    invoke-direct {v2, v0, v1}, Lco/uk/getmondo/d/a/t;-><init>(Lco/uk/getmondo/feed/a;Lco/uk/getmondo/common/accounts/b;)V

    return-object v2
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0}, Lco/uk/getmondo/d/a/u;->a()Lco/uk/getmondo/d/a/t;

    move-result-object v0

    return-object v0
.end method
