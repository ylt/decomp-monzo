.class public Lco/uk/getmondo/d/a/d;
.super Ljava/lang/Object;
.source "BalanceMapper.java"

# interfaces
.implements Lco/uk/getmondo/d/a/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lco/uk/getmondo/d/a/j",
        "<",
        "Lco/uk/getmondo/api/model/ApiBalance;",
        "Lco/uk/getmondo/d/b;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lco/uk/getmondo/d/a/d;->accountId:Ljava/lang/String;

    .line 16
    return-void
.end method

.method private a(Ljava/util/List;)Lio/realm/az;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/api/model/ApiLocalSpend;",
            ">;)",
            "Lio/realm/az",
            "<",
            "Lco/uk/getmondo/d/t;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    new-instance v1, Lio/realm/az;

    invoke-direct {v1}, Lio/realm/az;-><init>()V

    .line 26
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/ApiLocalSpend;

    .line 27
    new-instance v3, Lco/uk/getmondo/d/t;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/ApiLocalSpend;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/ApiLocalSpend;->a()J

    move-result-wide v6

    invoke-direct {v3, v4, v6, v7}, Lco/uk/getmondo/d/t;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v1, v3}, Lio/realm/az;->a(Lio/realm/bb;)Z

    goto :goto_0

    .line 29
    :cond_0
    return-object v1
.end method


# virtual methods
.method public a(Lco/uk/getmondo/api/model/ApiBalance;)Lco/uk/getmondo/d/b;
    .locals 9

    .prologue
    .line 20
    new-instance v0, Lco/uk/getmondo/d/b;

    iget-object v1, p0, Lco/uk/getmondo/d/a/d;->accountId:Ljava/lang/String;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiBalance;->a()J

    move-result-wide v2

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiBalance;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiBalance;->b()J

    move-result-wide v5

    .line 21
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiBalance;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiBalance;->d()Ljava/util/List;

    move-result-object v8

    invoke-direct {p0, v8}, Lco/uk/getmondo/d/a/d;->a(Ljava/util/List;)Lio/realm/az;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lco/uk/getmondo/d/b;-><init>(Ljava/lang/String;JLjava/lang/String;JLjava/lang/String;Lio/realm/az;)V

    .line 20
    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    check-cast p1, Lco/uk/getmondo/api/model/ApiBalance;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/a/d;->a(Lco/uk/getmondo/api/model/ApiBalance;)Lco/uk/getmondo/d/b;

    move-result-object v0

    return-object v0
.end method
