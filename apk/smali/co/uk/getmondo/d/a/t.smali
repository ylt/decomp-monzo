.class public Lco/uk/getmondo/d/a/t;
.super Ljava/lang/Object;
.source "TransactionMapper.java"


# instance fields
.field private final accountManager:Lco/uk/getmondo/common/accounts/b;

.field private final attachmentMapper:Lco/uk/getmondo/d/a/b;

.field private final declineReasonStringProvider:Lco/uk/getmondo/feed/a;

.field private final merchantMapper:Lco/uk/getmondo/d/a/k;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/feed/a;Lco/uk/getmondo/common/accounts/b;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lco/uk/getmondo/d/a/t;->declineReasonStringProvider:Lco/uk/getmondo/feed/a;

    .line 33
    iput-object p2, p0, Lco/uk/getmondo/d/a/t;->accountManager:Lco/uk/getmondo/common/accounts/b;

    .line 34
    new-instance v0, Lco/uk/getmondo/d/a/k;

    invoke-direct {v0}, Lco/uk/getmondo/d/a/k;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/d/a/t;->merchantMapper:Lco/uk/getmondo/d/a/k;

    .line 35
    new-instance v0, Lco/uk/getmondo/d/a/b;

    invoke-direct {v0}, Lco/uk/getmondo/d/a/b;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/d/a/t;->attachmentMapper:Lco/uk/getmondo/d/a/b;

    .line 36
    return-void
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 100
    invoke-static {p1}, Lco/uk/getmondo/common/k/p;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private b(Lco/uk/getmondo/api/model/feed/ApiTransaction;)Lco/uk/getmondo/payments/send/data/a/a;
    .locals 4

    .prologue
    .line 70
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->q()Lco/uk/getmondo/api/model/feed/ApiCounterparty;

    move-result-object v1

    .line 71
    iget-object v0, p0, Lco/uk/getmondo/d/a/t;->accountManager:Lco/uk/getmondo/common/accounts/b;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/feed/ApiCounterparty;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/feed/ApiCounterparty;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {v1}, Lco/uk/getmondo/api/model/feed/ApiCounterparty;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 73
    new-instance v0, Lco/uk/getmondo/payments/send/data/a/a;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/feed/ApiCounterparty;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/feed/ApiCounterparty;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/feed/ApiCounterparty;->f()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Lco/uk/getmondo/payments/send/data/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Lco/uk/getmondo/api/model/feed/ApiTransaction;)Lco/uk/getmondo/d/aa;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 80
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->q()Lco/uk/getmondo/api/model/feed/ApiCounterparty;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->q()Lco/uk/getmondo/api/model/feed/ApiCounterparty;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/feed/ApiCounterparty;->d()Ljava/lang/String;

    move-result-object v1

    .line 82
    new-instance v0, Lco/uk/getmondo/d/aa;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->q()Lco/uk/getmondo/api/model/feed/ApiCounterparty;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/api/model/feed/ApiCounterparty;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->q()Lco/uk/getmondo/api/model/feed/ApiCounterparty;

    move-result-object v3

    invoke-virtual {v3}, Lco/uk/getmondo/api/model/feed/ApiCounterparty;->c()Ljava/lang/String;

    move-result-object v3

    const/4 v7, 0x0

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v7}, Lco/uk/getmondo/d/aa;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 86
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v4

    goto :goto_0
.end method

.method private d(Lco/uk/getmondo/api/model/feed/ApiTransaction;)Lio/realm/az;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/model/feed/ApiTransaction;",
            ")",
            "Lio/realm/az",
            "<",
            "Lco/uk/getmondo/d/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    new-instance v1, Lio/realm/az;

    invoke-direct {v1}, Lio/realm/az;-><init>()V

    .line 91
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->u()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 92
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->u()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/feed/ApiAttachment;

    .line 93
    iget-object v3, p0, Lco/uk/getmondo/d/a/t;->attachmentMapper:Lco/uk/getmondo/d/a/b;

    invoke-virtual {v3, v0}, Lco/uk/getmondo/d/a/b;->a(Lco/uk/getmondo/api/model/feed/ApiAttachment;)Lco/uk/getmondo/d/d;

    move-result-object v0

    invoke-virtual {v1, v0}, Lio/realm/az;->a(Lio/realm/bb;)Z

    goto :goto_0

    .line 96
    :cond_0
    return-object v1
.end method

.method private e(Lco/uk/getmondo/api/model/feed/ApiTransaction;)Lco/uk/getmondo/d/u;
    .locals 2

    .prologue
    .line 104
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lco/uk/getmondo/d/a/t;->merchantMapper:Lco/uk/getmondo/d/a/k;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->h()Lco/uk/getmondo/api/model/feed/ApiMerchant;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/d/a/k;->a(Lco/uk/getmondo/api/model/feed/ApiMerchant;)Lco/uk/getmondo/d/u;

    move-result-object v0

    .line 107
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f(Lco/uk/getmondo/api/model/feed/ApiTransaction;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 112
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/d/j;->a(Ljava/lang/String;)Lco/uk/getmondo/d/j;

    move-result-object v0

    .line 113
    iget-object v1, p0, Lco/uk/getmondo/d/a/t;->declineReasonStringProvider:Lco/uk/getmondo/feed/a;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/feed/a;->a(Lco/uk/getmondo/d/j;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private g(Lco/uk/getmondo/api/model/feed/ApiTransaction;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->s()Lco/uk/getmondo/api/model/feed/ApiMetadata;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/feed/ApiMetadata;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->s()Lco/uk/getmondo/api/model/feed/ApiMetadata;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/feed/ApiMetadata;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->s()Lco/uk/getmondo/api/model/feed/ApiMetadata;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/feed/ApiMetadata;->b()Ljava/lang/String;

    move-result-object v0

    .line 122
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h(Lco/uk/getmondo/api/model/feed/ApiTransaction;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->q()Lco/uk/getmondo/api/model/feed/ApiCounterparty;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 127
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->q()Lco/uk/getmondo/api/model/feed/ApiCounterparty;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/feed/ApiCounterparty;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->q()Lco/uk/getmondo/api/model/feed/ApiCounterparty;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/feed/ApiCounterparty;->b()Ljava/lang/String;

    move-result-object v0

    .line 136
    :goto_0
    return-object v0

    .line 130
    :cond_0
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->q()Lco/uk/getmondo/api/model/feed/ApiCounterparty;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/feed/ApiCounterparty;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 133
    :cond_1
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 134
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->h()Lco/uk/getmondo/api/model/feed/ApiMerchant;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/feed/ApiMerchant;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 136
    :cond_2
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->o()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private i(Lco/uk/getmondo/api/model/feed/ApiTransaction;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 146
    const/4 v0, 0x0

    .line 147
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->e()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 148
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->s()Lco/uk/getmondo/api/model/feed/ApiMetadata;

    move-result-object v0

    .line 151
    :cond_0
    if-eqz v0, :cond_4

    .line 152
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/feed/ApiMetadata;->c()Ljava/lang/Boolean;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/feed/ApiMetadata;->c()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 154
    :goto_0
    if-nez v0, :cond_1

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v2, v1

    :cond_2
    return v2

    :cond_3
    move v0, v2

    .line 152
    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method private j(Lco/uk/getmondo/api/model/feed/ApiTransaction;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->s()Lco/uk/getmondo/api/model/feed/ApiMetadata;

    move-result-object v0

    .line 159
    if-eqz v0, :cond_0

    .line 160
    invoke-virtual {v0}, Lco/uk/getmondo/api/model/feed/ApiMetadata;->f()Ljava/lang/String;

    move-result-object v0

    .line 162
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/api/model/feed/ApiTransaction;)Lco/uk/getmondo/d/aj;
    .locals 31

    .prologue
    .line 39
    new-instance v2, Lco/uk/getmondo/d/aj;

    .line 40
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->g()Ljava/lang/String;

    move-result-object v3

    .line 41
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->i()J

    move-result-wide v4

    .line 42
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->m()Ljava/lang/String;

    move-result-object v6

    .line 43
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->j()J

    move-result-wide v7

    .line 44
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->n()Ljava/lang/String;

    move-result-object v9

    .line 45
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->k()Ljava/util/Date;

    move-result-object v10

    .line 46
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->k()Ljava/util/Date;

    move-result-object v11

    invoke-static {v11}, Lco/uk/getmondo/common/c/a;->d(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v11

    .line 47
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->l()Ljava/util/Date;

    move-result-object v12

    .line 48
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->o()Ljava/lang/String;

    move-result-object v13

    .line 49
    invoke-direct/range {p0 .. p1}, Lco/uk/getmondo/d/a/t;->h(Lco/uk/getmondo/api/model/feed/ApiTransaction;)Ljava/lang/String;

    move-result-object v14

    .line 50
    invoke-direct/range {p0 .. p1}, Lco/uk/getmondo/d/a/t;->f(Lco/uk/getmondo/api/model/feed/ApiTransaction;)Ljava/lang/String;

    move-result-object v15

    .line 51
    invoke-direct/range {p0 .. p1}, Lco/uk/getmondo/d/a/t;->g(Lco/uk/getmondo/api/model/feed/ApiTransaction;)Ljava/lang/String;

    move-result-object v16

    .line 52
    invoke-direct/range {p0 .. p1}, Lco/uk/getmondo/d/a/t;->i(Lco/uk/getmondo/api/model/feed/ApiTransaction;)Z

    move-result v17

    .line 53
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->r()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lco/uk/getmondo/d/a/t;->a(Ljava/lang/String;)Z

    move-result v18

    .line 54
    invoke-direct/range {p0 .. p1}, Lco/uk/getmondo/d/a/t;->e(Lco/uk/getmondo/api/model/feed/ApiTransaction;)Lco/uk/getmondo/d/u;

    move-result-object v19

    .line 55
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->t()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lco/uk/getmondo/d/h;->a(Ljava/lang/String;)Lco/uk/getmondo/d/h;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lco/uk/getmondo/d/h;->f()Ljava/lang/String;

    move-result-object v20

    .line 56
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->a()Z

    move-result v21

    .line 57
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->d()Z

    move-result v22

    .line 58
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->c()Z

    move-result v23

    .line 59
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->v()Z

    move-result v24

    .line 60
    invoke-direct/range {p0 .. p1}, Lco/uk/getmondo/d/a/t;->d(Lco/uk/getmondo/api/model/feed/ApiTransaction;)Lio/realm/az;

    move-result-object v25

    .line 61
    invoke-direct/range {p0 .. p1}, Lco/uk/getmondo/d/a/t;->c(Lco/uk/getmondo/api/model/feed/ApiTransaction;)Lco/uk/getmondo/d/aa;

    move-result-object v26

    .line 62
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->s()Lco/uk/getmondo/api/model/feed/ApiMetadata;

    move-result-object v27

    if-eqz v27, :cond_0

    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->s()Lco/uk/getmondo/api/model/feed/ApiMetadata;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lco/uk/getmondo/api/model/feed/ApiMetadata;->d()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lco/uk/getmondo/common/k/p;->c(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_0

    const/16 v27, 0x1

    .line 63
    :goto_0
    invoke-direct/range {p0 .. p1}, Lco/uk/getmondo/d/a/t;->b(Lco/uk/getmondo/api/model/feed/ApiTransaction;)Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v28

    .line 64
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->w()Ljava/lang/String;

    move-result-object v29

    .line 65
    invoke-direct/range {p0 .. p1}, Lco/uk/getmondo/d/a/t;->j(Lco/uk/getmondo/api/model/feed/ApiTransaction;)Ljava/lang/String;

    move-result-object v30

    invoke-direct/range {v2 .. v30}, Lco/uk/getmondo/d/aj;-><init>(Ljava/lang/String;JLjava/lang/String;JLjava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLco/uk/getmondo/d/u;Ljava/lang/String;ZZZZLio/realm/az;Lco/uk/getmondo/d/aa;ZLco/uk/getmondo/payments/send/data/a/a;Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    return-object v2

    .line 62
    :cond_0
    const/16 v27, 0x0

    goto :goto_0
.end method
