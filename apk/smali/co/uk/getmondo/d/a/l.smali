.class public final Lco/uk/getmondo/d/a/l;
.super Ljava/lang/Object;
.source "OverdraftMapper.kt"

# interfaces
.implements Lco/uk/getmondo/d/a/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lco/uk/getmondo/d/a/j",
        "<",
        "Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;",
        "Lco/uk/getmondo/d/w;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u00c6\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0002H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "Lco/uk/getmondo/model/mapper/OverdraftMapper;",
        "Lco/uk/getmondo/model/mapper/Mapper;",
        "Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;",
        "Lco/uk/getmondo/model/OverdraftStatus;",
        "()V",
        "apply",
        "apiValue",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final INSTANCE:Lco/uk/getmondo/d/a/l;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6
    new-instance v0, Lco/uk/getmondo/d/a/l;

    invoke-direct {v0}, Lco/uk/getmondo/d/a/l;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lco/uk/getmondo/d/a/l;

    sput-object p0, Lco/uk/getmondo/d/a/l;->INSTANCE:Lco/uk/getmondo/d/a/l;

    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;)Lco/uk/getmondo/d/w;
    .locals 19

    .prologue
    const-string v2, "apiValue"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    new-instance v3, Lco/uk/getmondo/d/w;

    .line 9
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->a()Ljava/lang/String;

    move-result-object v4

    .line 10
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->b()Z

    move-result v5

    .line 11
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->c()Z

    move-result v6

    .line 12
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->d()Ljava/lang/String;

    move-result-object v7

    .line 13
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->e()J

    move-result-wide v8

    .line 14
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->f()J

    move-result-wide v10

    .line 15
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->g()J

    move-result-wide v12

    .line 16
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->h()J

    move-result-wide v14

    .line 17
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->i()J

    move-result-wide v16

    .line 18
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;->j()Ljava/lang/String;

    move-result-object v18

    .line 8
    invoke-direct/range {v3 .. v18}, Lco/uk/getmondo/d/w;-><init>(Ljava/lang/String;ZZLjava/lang/String;JJJJJLjava/lang/String;)V

    .line 18
    return-object v3
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 6
    check-cast p1, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/a/l;->a(Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;)Lco/uk/getmondo/d/w;

    move-result-object v0

    return-object v0
.end method
