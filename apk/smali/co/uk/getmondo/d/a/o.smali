.class public final Lco/uk/getmondo/d/a/o;
.super Lco/uk/getmondo/d/a/a;
.source "PrepaidAccountMapper.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/d/a/a",
        "<",
        "Lco/uk/getmondo/d/ab;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0017\u0012\u0008\u0008\u0002\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0008\u001a\u00020\tH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lco/uk/getmondo/model/mapper/PrepaidAccountMapper;",
        "Lco/uk/getmondo/model/mapper/AccountMapper;",
        "Lco/uk/getmondo/model/PrepaidAccount;",
        "initialTopupCompleted",
        "",
        "cardActivated",
        "(ZZ)V",
        "apply",
        "apiValue",
        "Lco/uk/getmondo/api/model/ApiAccount;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final initialTopupCompleted:Z


# direct methods
.method public constructor <init>(ZZ)V
    .locals 0

    .prologue
    .line 7
    .line 10
    invoke-direct {p0, p2}, Lco/uk/getmondo/d/a/a;-><init>(Z)V

    iput-boolean p1, p0, Lco/uk/getmondo/d/a/o;->initialTopupCompleted:Z

    return-void
.end method

.method public synthetic constructor <init>(ZZILkotlin/d/b/i;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_0

    .line 8
    const/4 p1, 0x0

    :cond_0
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/d/a/o;-><init>(ZZ)V

    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/api/model/ApiAccount;)Lco/uk/getmondo/d/ab;
    .locals 8

    .prologue
    const-string v0, "apiValue"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    new-instance v0, Lco/uk/getmondo/d/ab;

    .line 14
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiAccount;->a()Ljava/lang/String;

    move-result-object v1

    .line 15
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiAccount;->c()Ljava/lang/String;

    move-result-object v2

    .line 16
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiAccount;->b()Lorg/threeten/bp/LocalDateTime;

    move-result-object v3

    .line 17
    iget-boolean v6, p0, Lco/uk/getmondo/d/a/o;->initialTopupCompleted:Z

    .line 18
    invoke-virtual {p0}, Lco/uk/getmondo/d/a/o;->a()Z

    move-result v4

    .line 19
    sget-object v5, Lco/uk/getmondo/d/a$b;->Find:Lco/uk/getmondo/d/a$b$a;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiAccount;->f()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lco/uk/getmondo/d/a$b$a;->a(Ljava/lang/String;)Lco/uk/getmondo/d/a$b;

    move-result-object v5

    .line 13
    invoke-direct/range {v0 .. v6}, Lco/uk/getmondo/d/ab;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDateTime;ZLco/uk/getmondo/d/a$b;Z)V

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 7
    check-cast p1, Lco/uk/getmondo/api/model/ApiAccount;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/a/o;->a(Lco/uk/getmondo/api/model/ApiAccount;)Lco/uk/getmondo/d/ab;

    move-result-object v0

    return-object v0
.end method
