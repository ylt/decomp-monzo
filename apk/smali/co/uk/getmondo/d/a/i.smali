.class public final Lco/uk/getmondo/d/a/i;
.super Ljava/lang/Object;
.source "LegacyAddressMapper.kt"

# interfaces
.implements Lco/uk/getmondo/d/a/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lco/uk/getmondo/d/a/j",
        "<",
        "Lco/uk/getmondo/api/model/LegacyApiAddress;",
        "Lco/uk/getmondo/d/s;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0002H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "Lco/uk/getmondo/model/mapper/LegacyAddressMapper;",
        "Lco/uk/getmondo/model/mapper/Mapper;",
        "Lco/uk/getmondo/api/model/LegacyApiAddress;",
        "Lco/uk/getmondo/model/LegacyAddress;",
        "()V",
        "apply",
        "apiValue",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/api/model/LegacyApiAddress;)Lco/uk/getmondo/d/s;
    .locals 7

    .prologue
    const-string v0, "apiValue"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    new-instance v3, Lco/uk/getmondo/d/s;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/LegacyApiAddress;->c()Ljava/lang/String;

    move-result-object v1

    .line 10
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/LegacyApiAddress;->a()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    check-cast v0, Ljava/util/Collection;

    .line 17
    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.util.Collection<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 18
    :cond_0
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, [Ljava/lang/String;

    move-object v2, v0

    move-object v6, v3

    move-object v0, v3

    .line 11
    :goto_0
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/LegacyApiAddress;->b()Ljava/lang/String;

    move-result-object v3

    .line 12
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/LegacyApiAddress;->d()Ljava/lang/String;

    move-result-object v4

    .line 13
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/LegacyApiAddress;->e()Ljava/lang/String;

    move-result-object v5

    .line 9
    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/d/s;-><init>(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v6

    .line 18
    :cond_2
    const/4 v2, 0x0

    move-object v0, v3

    move-object v6, v3

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 6
    check-cast p1, Lco/uk/getmondo/api/model/LegacyApiAddress;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/a/i;->a(Lco/uk/getmondo/api/model/LegacyApiAddress;)Lco/uk/getmondo/d/s;

    move-result-object v0

    return-object v0
.end method
