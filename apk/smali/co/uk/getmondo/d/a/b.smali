.class public Lco/uk/getmondo/d/a/b;
.super Ljava/lang/Object;
.source "AttachmentMapper.java"

# interfaces
.implements Lco/uk/getmondo/d/a/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lco/uk/getmondo/d/a/j",
        "<",
        "Lco/uk/getmondo/api/model/feed/ApiAttachment;",
        "Lco/uk/getmondo/d/d;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/api/model/feed/ApiAttachment;)Lco/uk/getmondo/d/d;
    .locals 5

    .prologue
    .line 12
    if-nez p1, :cond_0

    .line 13
    const/4 v0, 0x0

    .line 15
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lco/uk/getmondo/d/d;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiAttachment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiAttachment;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiAttachment;->b()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiAttachment;->d()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lco/uk/getmondo/d/d;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 6
    check-cast p1, Lco/uk/getmondo/api/model/feed/ApiAttachment;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/a/b;->a(Lco/uk/getmondo/api/model/feed/ApiAttachment;)Lco/uk/getmondo/d/d;

    move-result-object v0

    return-object v0
.end method
