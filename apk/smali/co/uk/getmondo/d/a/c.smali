.class public Lco/uk/getmondo/d/a/c;
.super Ljava/lang/Object;
.source "BalanceLimitsMapper.java"

# interfaces
.implements Lco/uk/getmondo/d/a/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lco/uk/getmondo/d/a/j",
        "<",
        "Lco/uk/getmondo/api/model/ApiBalanceLimits;",
        "Lco/uk/getmondo/d/e;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(D)Lco/uk/getmondo/d/c;
    .locals 5

    .prologue
    .line 37
    new-instance v0, Lco/uk/getmondo/d/c;

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    mul-double/2addr v2, p1

    double-to-long v2, v2

    sget-object v1, Lco/uk/getmondo/common/i/c;->a:Lco/uk/getmondo/common/i/c;

    invoke-direct {v0, v2, v3, v1}, Lco/uk/getmondo/d/c;-><init>(JLco/uk/getmondo/common/i/c;)V

    return-object v0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/api/model/ApiBalanceLimits;)Lco/uk/getmondo/d/e;
    .locals 28

    .prologue
    .line 12
    new-instance v4, Lco/uk/getmondo/d/e;

    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/ApiBalanceLimits;->u()Lco/uk/getmondo/api/model/VerificationType;

    move-result-object v5

    .line 13
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/ApiBalanceLimits;->b()D

    move-result-wide v6

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lco/uk/getmondo/d/a/c;->a(D)Lco/uk/getmondo/d/c;

    move-result-object v6

    .line 14
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/ApiBalanceLimits;->a()D

    move-result-wide v8

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lco/uk/getmondo/d/a/c;->a(D)Lco/uk/getmondo/d/c;

    move-result-object v7

    .line 15
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/ApiBalanceLimits;->c()D

    move-result-wide v8

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lco/uk/getmondo/d/a/c;->a(D)Lco/uk/getmondo/d/c;

    move-result-object v8

    .line 16
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/ApiBalanceLimits;->d()D

    move-result-wide v10

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11}, Lco/uk/getmondo/d/a/c;->a(D)Lco/uk/getmondo/d/c;

    move-result-object v9

    .line 17
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/ApiBalanceLimits;->e()D

    move-result-wide v10

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11}, Lco/uk/getmondo/d/a/c;->a(D)Lco/uk/getmondo/d/c;

    move-result-object v10

    .line 18
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/ApiBalanceLimits;->f()D

    move-result-wide v12

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lco/uk/getmondo/d/a/c;->a(D)Lco/uk/getmondo/d/c;

    move-result-object v11

    .line 19
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/ApiBalanceLimits;->g()D

    move-result-wide v12

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lco/uk/getmondo/d/a/c;->a(D)Lco/uk/getmondo/d/c;

    move-result-object v12

    .line 20
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/ApiBalanceLimits;->h()D

    move-result-wide v14

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lco/uk/getmondo/d/a/c;->a(D)Lco/uk/getmondo/d/c;

    move-result-object v13

    .line 21
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/ApiBalanceLimits;->i()D

    move-result-wide v14

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lco/uk/getmondo/d/a/c;->a(D)Lco/uk/getmondo/d/c;

    move-result-object v14

    .line 22
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/ApiBalanceLimits;->j()D

    move-result-wide v16

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/d/a/c;->a(D)Lco/uk/getmondo/d/c;

    move-result-object v15

    .line 23
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/ApiBalanceLimits;->k()D

    move-result-wide v16

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/d/a/c;->a(D)Lco/uk/getmondo/d/c;

    move-result-object v16

    .line 24
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/ApiBalanceLimits;->l()D

    move-result-wide v18

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/d/a/c;->a(D)Lco/uk/getmondo/d/c;

    move-result-object v17

    .line 25
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/ApiBalanceLimits;->m()D

    move-result-wide v18

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/d/a/c;->a(D)Lco/uk/getmondo/d/c;

    move-result-object v18

    .line 26
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/ApiBalanceLimits;->n()D

    move-result-wide v20

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/d/a/c;->a(D)Lco/uk/getmondo/d/c;

    move-result-object v19

    .line 27
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/ApiBalanceLimits;->o()D

    move-result-wide v20

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/d/a/c;->a(D)Lco/uk/getmondo/d/c;

    move-result-object v20

    .line 28
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/ApiBalanceLimits;->p()D

    move-result-wide v22

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/d/a/c;->a(D)Lco/uk/getmondo/d/c;

    move-result-object v21

    .line 29
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/ApiBalanceLimits;->q()D

    move-result-wide v22

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/d/a/c;->a(D)Lco/uk/getmondo/d/c;

    move-result-object v22

    .line 30
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/ApiBalanceLimits;->r()D

    move-result-wide v24

    move-object/from16 v0, p0

    move-wide/from16 v1, v24

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/d/a/c;->a(D)Lco/uk/getmondo/d/c;

    move-result-object v23

    .line 31
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/ApiBalanceLimits;->s()D

    move-result-wide v24

    move-object/from16 v0, p0

    move-wide/from16 v1, v24

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/d/a/c;->a(D)Lco/uk/getmondo/d/c;

    move-result-object v24

    .line 32
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/api/model/ApiBalanceLimits;->t()D

    move-result-wide v26

    move-object/from16 v0, p0

    move-wide/from16 v1, v26

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/d/a/c;->a(D)Lco/uk/getmondo/d/c;

    move-result-object v25

    invoke-direct/range {v4 .. v25}, Lco/uk/getmondo/d/e;-><init>(Lco/uk/getmondo/api/model/VerificationType;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;)V

    .line 12
    return-object v4
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8
    check-cast p1, Lco/uk/getmondo/api/model/ApiBalanceLimits;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/a/c;->a(Lco/uk/getmondo/api/model/ApiBalanceLimits;)Lco/uk/getmondo/d/e;

    move-result-object v0

    return-object v0
.end method
