.class public final synthetic Lco/uk/getmondo/d/a/n;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 3

    invoke-static {}, Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;->values()[Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lco/uk/getmondo/d/a/n;->$EnumSwitchMapping$0:[I

    sget-object v0, Lco/uk/getmondo/d/a/n;->$EnumSwitchMapping$0:[I

    sget-object v1, Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;->AMOUNT:Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lco/uk/getmondo/d/a/n;->$EnumSwitchMapping$0:[I

    sget-object v1, Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;->COUNT:Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1

    sget-object v0, Lco/uk/getmondo/d/a/n;->$EnumSwitchMapping$0:[I

    sget-object v1, Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;->VIRTUAL:Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1

    return-void
.end method
