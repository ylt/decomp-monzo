.class Lco/uk/getmondo/d/a/k;
.super Ljava/lang/Object;
.source "MerchantMapper.java"

# interfaces
.implements Lco/uk/getmondo/d/a/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lco/uk/getmondo/d/a/j",
        "<",
        "Lco/uk/getmondo/api/model/feed/ApiMerchant;",
        "Lco/uk/getmondo/d/u;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/api/model/feed/ApiMerchant;)Lco/uk/getmondo/d/u;
    .locals 14

    .prologue
    const/4 v11, 0x0

    .line 11
    if-nez p1, :cond_0

    .line 14
    :goto_0
    return-object v11

    :cond_0
    new-instance v0, Lco/uk/getmondo/d/u;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiMerchant;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiMerchant;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiMerchant;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiMerchant;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiMerchant;->e()Ljava/lang/String;

    move-result-object v5

    .line 15
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiMerchant;->c()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lco/uk/getmondo/d/h;->a(Ljava/lang/String;)Lco/uk/getmondo/d/h;

    move-result-object v6

    invoke-virtual {v6}, Lco/uk/getmondo/d/h;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiMerchant;->i()Z

    move-result v7

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiMerchant;->f()Z

    move-result v8

    .line 16
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiMerchant;->h()Lco/uk/getmondo/api/model/feed/ApiMerchantAddress;

    move-result-object v9

    if-nez v9, :cond_1

    move-object v9, v11

    .line 17
    :goto_1
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiMerchant;->h()Lco/uk/getmondo/api/model/feed/ApiMerchantAddress;

    move-result-object v10

    if-nez v10, :cond_2

    move-object v10, v11

    .line 18
    :goto_2
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiMerchant;->h()Lco/uk/getmondo/api/model/feed/ApiMerchantAddress;

    move-result-object v12

    if-nez v12, :cond_3

    :goto_3
    invoke-direct/range {v0 .. v11}, Lco/uk/getmondo/d/u;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/Double;Ljava/lang/Double;)V

    move-object v11, v0

    .line 14
    goto :goto_0

    .line 16
    :cond_1
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiMerchant;->h()Lco/uk/getmondo/api/model/feed/ApiMerchantAddress;

    move-result-object v9

    invoke-virtual {v9}, Lco/uk/getmondo/api/model/feed/ApiMerchantAddress;->a()Ljava/lang/String;

    move-result-object v9

    goto :goto_1

    .line 17
    :cond_2
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiMerchant;->h()Lco/uk/getmondo/api/model/feed/ApiMerchantAddress;

    move-result-object v10

    invoke-virtual {v10}, Lco/uk/getmondo/api/model/feed/ApiMerchantAddress;->b()D

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    goto :goto_2

    .line 18
    :cond_3
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiMerchant;->h()Lco/uk/getmondo/api/model/feed/ApiMerchantAddress;

    move-result-object v11

    invoke-virtual {v11}, Lco/uk/getmondo/api/model/feed/ApiMerchantAddress;->c()D

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v11

    goto :goto_3
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 7
    check-cast p1, Lco/uk/getmondo/api/model/feed/ApiMerchant;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/a/k;->a(Lco/uk/getmondo/api/model/feed/ApiMerchant;)Lco/uk/getmondo/d/u;

    move-result-object v0

    return-object v0
.end method
