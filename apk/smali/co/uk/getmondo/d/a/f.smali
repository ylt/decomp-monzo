.class public Lco/uk/getmondo/d/a/f;
.super Ljava/lang/Object;
.source "FeedItemMapper.java"

# interfaces
.implements Lco/uk/getmondo/d/a/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lco/uk/getmondo/d/a/j",
        "<",
        "Lco/uk/getmondo/api/model/feed/ApiFeedItem;",
        "Lco/uk/getmondo/d/m;",
        ">;"
    }
.end annotation


# instance fields
.field private final transactionMapper:Lco/uk/getmondo/d/a/t;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/d/a/t;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lco/uk/getmondo/d/a/f;->transactionMapper:Lco/uk/getmondo/d/a/t;

    .line 26
    return-void
.end method

.method private b(Lco/uk/getmondo/api/model/feed/ApiFeedItem;)Lco/uk/getmondo/d/aj;
    .locals 2

    .prologue
    .line 37
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->f()Lco/uk/getmondo/api/model/feed/ApiTransaction;

    move-result-object v0

    if-nez v0, :cond_0

    .line 38
    const/4 v0, 0x0

    .line 40
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/d/a/f;->transactionMapper:Lco/uk/getmondo/d/a/t;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->f()Lco/uk/getmondo/api/model/feed/ApiTransaction;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/d/a/t;->a(Lco/uk/getmondo/api/model/feed/ApiTransaction;)Lco/uk/getmondo/d/aj;

    move-result-object v0

    goto :goto_0
.end method

.method private c(Lco/uk/getmondo/api/model/feed/ApiFeedItem;)Lco/uk/getmondo/d/o;
    .locals 2

    .prologue
    .line 45
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->g()Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->g()Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/k/p;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 46
    :cond_0
    const/4 v0, 0x0

    .line 48
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lco/uk/getmondo/d/o;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->g()Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lco/uk/getmondo/d/o;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private d(Lco/uk/getmondo/api/model/feed/ApiFeedItem;)Lco/uk/getmondo/d/v;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 53
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->g()Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->g()Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/common/k/p;->d(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 66
    :cond_0
    :goto_0
    return-object v0

    .line 57
    :cond_1
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->g()Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "month"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 58
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->g()Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;->d()Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    .line 59
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->g()Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;->e()Lorg/threeten/bp/LocalDateTime;

    move-result-object v1

    .line 61
    invoke-static {v0, v1}, Lorg/threeten/bp/Duration;->a(Lorg/threeten/bp/temporal/Temporal;Lorg/threeten/bp/temporal/Temporal;)Lorg/threeten/bp/Duration;

    move-result-object v1

    .line 62
    invoke-virtual {v1}, Lorg/threeten/bp/Duration;->b()J

    move-result-wide v2

    const-wide/16 v4, 0x2

    div-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lorg/threeten/bp/LocalDateTime;->c(J)Lorg/threeten/bp/LocalDateTime;

    move-result-object v5

    .line 63
    new-instance v0, Lco/uk/getmondo/d/v;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->g()Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;->c()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 64
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->g()Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;

    move-result-object v4

    invoke-virtual {v4}, Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5}, Lorg/threeten/bp/YearMonth;->a(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/YearMonth;

    move-result-object v5

    invoke-virtual {v5}, Lorg/threeten/bp/YearMonth;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/d/v;-><init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private e(Lco/uk/getmondo/api/model/feed/ApiFeedItem;)Lco/uk/getmondo/d/r;
    .locals 4

    .prologue
    .line 71
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->g()Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;

    move-result-object v0

    if-nez v0, :cond_0

    .line 72
    const/4 v0, 0x0

    .line 75
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lco/uk/getmondo/d/r;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->g()Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;->g()Ljava/lang/String;

    move-result-object v2

    .line 76
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->g()Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;

    move-result-object v3

    invoke-virtual {v3}, Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;->h()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/d/r;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private f(Lco/uk/getmondo/api/model/feed/ApiFeedItem;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->g()Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;

    move-result-object v0

    if-nez v0, :cond_0

    .line 82
    const/4 v0, 0x0

    .line 85
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->g()Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;->i()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private g(Lco/uk/getmondo/api/model/feed/ApiFeedItem;)Lco/uk/getmondo/d/f;
    .locals 5

    .prologue
    .line 90
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->g()Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;

    move-result-object v3

    .line 91
    if-nez v3, :cond_0

    .line 92
    const/4 v0, 0x0

    .line 96
    :goto_0
    return-object v0

    .line 94
    :cond_0
    invoke-virtual {v3}, Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v3}, Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;->j()Ljava/lang/String;

    move-result-object v0

    .line 95
    :goto_1
    invoke-virtual {v3}, Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;->k()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v3}, Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;->k()Ljava/lang/String;

    move-result-object v1

    .line 96
    :goto_2
    new-instance v2, Lco/uk/getmondo/d/f;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;->l()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v4, v0, v1, v3}, Lco/uk/getmondo/d/f;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    goto :goto_0

    .line 94
    :cond_1
    const-string v0, ""

    goto :goto_1

    .line 95
    :cond_2
    const-string v1, ""

    goto :goto_2
.end method


# virtual methods
.method public a(Lco/uk/getmondo/api/model/feed/ApiFeedItem;)Lco/uk/getmondo/d/m;
    .locals 13

    .prologue
    .line 30
    new-instance v0, Lco/uk/getmondo/d/m;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->e()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->h()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, p1}, Lco/uk/getmondo/d/a/f;->c(Lco/uk/getmondo/api/model/feed/ApiFeedItem;)Lco/uk/getmondo/d/o;

    move-result-object v6

    .line 31
    invoke-direct {p0, p1}, Lco/uk/getmondo/d/a/f;->d(Lco/uk/getmondo/api/model/feed/ApiFeedItem;)Lco/uk/getmondo/d/v;

    move-result-object v7

    invoke-direct {p0, p1}, Lco/uk/getmondo/d/a/f;->b(Lco/uk/getmondo/api/model/feed/ApiFeedItem;)Lco/uk/getmondo/d/aj;

    move-result-object v8

    invoke-direct {p0, p1}, Lco/uk/getmondo/d/a/f;->e(Lco/uk/getmondo/api/model/feed/ApiFeedItem;)Lco/uk/getmondo/d/r;

    move-result-object v9

    .line 32
    invoke-direct {p0, p1}, Lco/uk/getmondo/d/a/f;->f(Lco/uk/getmondo/api/model/feed/ApiFeedItem;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, p1}, Lco/uk/getmondo/d/a/f;->g(Lco/uk/getmondo/api/model/feed/ApiFeedItem;)Lco/uk/getmondo/d/f;

    move-result-object v11

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->i()Z

    move-result v12

    invoke-direct/range {v0 .. v12}, Lco/uk/getmondo/d/m;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/o;Lco/uk/getmondo/d/v;Lco/uk/getmondo/d/aj;Lco/uk/getmondo/d/r;Ljava/lang/String;Lco/uk/getmondo/d/f;Z)V

    .line 30
    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    check-cast p1, Lco/uk/getmondo/api/model/feed/ApiFeedItem;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/a/f;->a(Lco/uk/getmondo/api/model/feed/ApiFeedItem;)Lco/uk/getmondo/d/m;

    move-result-object v0

    return-object v0
.end method
