.class public final Lco/uk/getmondo/d/a/p;
.super Ljava/lang/Object;
.source "ProfileMapper.kt"

# interfaces
.implements Lco/uk/getmondo/d/a/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lco/uk/getmondo/d/a/j",
        "<",
        "Lco/uk/getmondo/api/model/ApiProfile;",
        "Lco/uk/getmondo/d/ac;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u00032\u0006\u0010\u0008\u001a\u00020\u0002H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lco/uk/getmondo/model/mapper/ProfileMapper;",
        "Lco/uk/getmondo/model/mapper/Mapper;",
        "Lco/uk/getmondo/api/model/ApiProfile;",
        "Lco/uk/getmondo/model/Profile;",
        "()V",
        "addressMapper",
        "Lco/uk/getmondo/model/mapper/LegacyAddressMapper;",
        "apply",
        "apiProfile",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final addressMapper:Lco/uk/getmondo/d/a/i;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    new-instance v0, Lco/uk/getmondo/d/a/i;

    invoke-direct {v0}, Lco/uk/getmondo/d/a/i;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/d/a/p;->addressMapper:Lco/uk/getmondo/d/a/i;

    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/api/model/ApiProfile;)Lco/uk/getmondo/d/ac;
    .locals 10

    .prologue
    const-string v0, "apiProfile"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    new-instance v0, Lco/uk/getmondo/d/ac;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiProfile;->a()Ljava/lang/String;

    move-result-object v1

    .line 12
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiProfile;->b()Ljava/lang/String;

    move-result-object v2

    .line 13
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiProfile;->c()Ljava/lang/String;

    move-result-object v3

    .line 14
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiProfile;->d()Ljava/lang/String;

    move-result-object v4

    .line 15
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiProfile;->g()Ljava/lang/String;

    move-result-object v5

    .line 16
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiProfile;->h()Ljava/lang/String;

    move-result-object v6

    .line 17
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiProfile;->i()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 18
    iget-object v8, p0, Lco/uk/getmondo/d/a/p;->addressMapper:Lco/uk/getmondo/d/a/i;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiProfile;->e()Lco/uk/getmondo/api/model/LegacyApiAddress;

    move-result-object v9

    invoke-virtual {v8, v9}, Lco/uk/getmondo/d/a/i;->a(Lco/uk/getmondo/api/model/LegacyApiAddress;)Lco/uk/getmondo/d/s;

    move-result-object v8

    .line 19
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiProfile;->f()Z

    move-result v9

    .line 11
    invoke-direct/range {v0 .. v9}, Lco/uk/getmondo/d/ac;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lco/uk/getmondo/d/s;Z)V

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 6
    check-cast p1, Lco/uk/getmondo/api/model/ApiProfile;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/a/p;->a(Lco/uk/getmondo/api/model/ApiProfile;)Lco/uk/getmondo/d/ac;

    move-result-object v0

    return-object v0
.end method
