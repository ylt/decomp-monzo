.class public Lco/uk/getmondo/d/a/e;
.super Ljava/lang/Object;
.source "CardMapper.java"

# interfaces
.implements Lco/uk/getmondo/d/a/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lco/uk/getmondo/d/a/j",
        "<",
        "Lco/uk/getmondo/api/model/ApiCard;",
        "Lco/uk/getmondo/d/g;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/api/model/ApiCard;)Lco/uk/getmondo/d/g;
    .locals 8

    .prologue
    .line 10
    new-instance v0, Lco/uk/getmondo/d/g;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiCard;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiCard;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiCard;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiCard;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiCard;->c()Ljava/lang/String;

    move-result-object v5

    .line 11
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiCard;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiCard;->g()Ljava/lang/Long;

    move-result-object v7

    if-eqz v7, :cond_0

    const/4 v7, 0x1

    :goto_0
    invoke-direct/range {v0 .. v7}, Lco/uk/getmondo/d/g;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 10
    return-object v0

    .line 11
    :cond_0
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 6
    check-cast p1, Lco/uk/getmondo/api/model/ApiCard;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/a/e;->a(Lco/uk/getmondo/api/model/ApiCard;)Lco/uk/getmondo/d/g;

    move-result-object v0

    return-object v0
.end method
