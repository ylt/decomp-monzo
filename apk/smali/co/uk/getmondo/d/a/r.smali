.class public Lco/uk/getmondo/d/a/r;
.super Ljava/lang/Object;
.source "StripeCardMapper.java"

# interfaces
.implements Lco/uk/getmondo/d/a/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lco/uk/getmondo/d/a/j",
        "<",
        "Lco/uk/getmondo/api/model/topup/ApiStripeCard;",
        "Lco/uk/getmondo/d/ae;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/api/model/topup/ApiStripeCard;)Lco/uk/getmondo/d/ae;
    .locals 5

    .prologue
    .line 11
    if-nez p1, :cond_0

    .line 12
    const/4 v0, 0x0

    .line 15
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lco/uk/getmondo/d/ae;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/topup/ApiStripeCard;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/topup/ApiStripeCard;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/topup/ApiStripeCard;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/topup/ApiStripeCard;->d()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lco/uk/getmondo/d/ae;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 7
    check-cast p1, Lco/uk/getmondo/api/model/topup/ApiStripeCard;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/a/r;->a(Lco/uk/getmondo/api/model/topup/ApiStripeCard;)Lco/uk/getmondo/d/ae;

    move-result-object v0

    return-object v0
.end method
