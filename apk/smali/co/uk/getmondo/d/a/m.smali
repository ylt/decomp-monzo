.class public final Lco/uk/getmondo/d/a/m;
.super Ljava/lang/Object;
.source "PaymentLimitsMapper.kt"

# interfaces
.implements Lco/uk/getmondo/d/a/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lco/uk/getmondo/d/a/j",
        "<",
        "Lco/uk/getmondo/api/model/ApiPaymentLimits;",
        "Lco/uk/getmondo/d/y;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0002H\u0016J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0002\u00a8\u0006\u000b"
    }
    d2 = {
        "Lco/uk/getmondo/model/mapper/PaymentLimitsMapper;",
        "Lco/uk/getmondo/model/mapper/Mapper;",
        "Lco/uk/getmondo/api/model/ApiPaymentLimits;",
        "Lco/uk/getmondo/model/PaymentLimits;",
        "()V",
        "apply",
        "apiValue",
        "mapLimit",
        "Lco/uk/getmondo/model/PaymentLimit;",
        "limit",
        "Lco/uk/getmondo/api/model/ApiPaymentLimit;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final INSTANCE:Lco/uk/getmondo/d/a/m;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    new-instance v0, Lco/uk/getmondo/d/a/m;

    invoke-direct {v0}, Lco/uk/getmondo/d/a/m;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lco/uk/getmondo/d/a/m;

    sput-object p0, Lco/uk/getmondo/d/a/m;->INSTANCE:Lco/uk/getmondo/d/a/m;

    return-void
.end method

.method private final a(Lco/uk/getmondo/api/model/ApiPaymentLimit;)Lco/uk/getmondo/d/x;
    .locals 8

    .prologue
    .line 19
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiPaymentLimit;->c()Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/d/a/n;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/ApiPaymentLimit$LimitType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 33
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 21
    :pswitch_0
    new-instance v0, Lco/uk/getmondo/d/x$a;

    .line 22
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiPaymentLimit;->a()Ljava/lang/String;

    move-result-object v1

    .line 23
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiPaymentLimit;->b()Ljava/lang/String;

    move-result-object v2

    .line 24
    new-instance v3, Lco/uk/getmondo/d/c;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiPaymentLimit;->e()Ljava/lang/Long;

    move-result-object v4

    if-nez v4, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiPaymentLimit;->d()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_1

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_1
    invoke-direct {v3, v4, v5, v6}, Lco/uk/getmondo/d/c;-><init>(JLjava/lang/String;)V

    .line 25
    new-instance v4, Lco/uk/getmondo/d/c;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiPaymentLimit;->f()Ljava/lang/Long;

    move-result-object v5

    if-nez v5, :cond_2

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_2
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiPaymentLimit;->d()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_3

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_3
    invoke-direct {v4, v6, v7, v5}, Lco/uk/getmondo/d/c;-><init>(JLjava/lang/String;)V

    .line 21
    invoke-direct {v0, v1, v2, v3, v4}, Lco/uk/getmondo/d/x$a;-><init>(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/c;Lco/uk/getmondo/d/c;)V

    check-cast v0, Lco/uk/getmondo/d/x;

    .line 33
    :goto_0
    return-object v0

    .line 27
    :pswitch_1
    new-instance v1, Lco/uk/getmondo/d/x$b;

    .line 28
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiPaymentLimit;->a()Ljava/lang/String;

    move-result-object v2

    .line 29
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiPaymentLimit;->b()Ljava/lang/String;

    move-result-object v3

    .line 30
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiPaymentLimit;->e()Ljava/lang/Long;

    move-result-object v0

    if-nez v0, :cond_4

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_4
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 31
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiPaymentLimit;->f()Ljava/lang/Long;

    move-result-object v0

    if-nez v0, :cond_5

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_5
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 27
    invoke-direct/range {v1 .. v7}, Lco/uk/getmondo/d/x$b;-><init>(Ljava/lang/String;Ljava/lang/String;JJ)V

    check-cast v1, Lco/uk/getmondo/d/x;

    move-object v0, v1

    goto :goto_0

    .line 33
    :pswitch_2
    new-instance v0, Lco/uk/getmondo/d/x$c;

    .line 34
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiPaymentLimit;->a()Ljava/lang/String;

    move-result-object v1

    .line 35
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiPaymentLimit;->b()Ljava/lang/String;

    move-result-object v2

    .line 33
    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/d/x$c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lco/uk/getmondo/d/x;

    goto :goto_0

    .line 19
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public a(Lco/uk/getmondo/api/model/ApiPaymentLimits;)Lco/uk/getmondo/d/y;
    .locals 11

    .prologue
    const/16 v10, 0xa

    const-string v0, "apiValue"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    new-instance v3, Lco/uk/getmondo/d/y;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiPaymentLimits;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 40
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0, v10}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 41
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 42
    check-cast v0, Lco/uk/getmondo/api/model/ApiPaymentLimitsSection;

    .line 14
    new-instance v5, Lco/uk/getmondo/d/z;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/ApiPaymentLimitsSection;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/ApiPaymentLimitsSection;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/ApiPaymentLimitsSection;->c()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 43
    new-instance v2, Ljava/util/ArrayList;

    invoke-static {v0, v10}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v8

    invoke-direct {v2, v8}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 44
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 45
    check-cast v0, Lco/uk/getmondo/api/model/ApiPaymentLimit;

    .line 14
    sget-object v9, Lco/uk/getmondo/d/a/m;->INSTANCE:Lco/uk/getmondo/d/a/m;

    invoke-direct {v9, v0}, Lco/uk/getmondo/d/a/m;->a(Lco/uk/getmondo/api/model/ApiPaymentLimit;)Lco/uk/getmondo/d/x;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 46
    :cond_0
    check-cast v2, Ljava/util/List;

    .line 14
    invoke-direct {v5, v6, v7, v2}, Lco/uk/getmondo/d/z;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    invoke-interface {v1, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 47
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 13
    invoke-direct {v3, v1}, Lco/uk/getmondo/d/y;-><init>(Ljava/util/List;)V

    return-object v3
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    check-cast p1, Lco/uk/getmondo/api/model/ApiPaymentLimits;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/a/m;->a(Lco/uk/getmondo/api/model/ApiPaymentLimits;)Lco/uk/getmondo/d/y;

    move-result-object v0

    return-object v0
.end method
