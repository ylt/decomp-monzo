.class public Lco/uk/getmondo/d/a/v;
.super Ljava/lang/Object;
.source "WaitlistMapper.java"

# interfaces
.implements Lco/uk/getmondo/d/a/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lco/uk/getmondo/d/a/j",
        "<",
        "Lco/uk/getmondo/api/model/ApiWaitlistPosition;",
        "Lco/uk/getmondo/d/an;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/api/model/c;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/d/k;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 30
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/c;

    .line 31
    new-instance v3, Lco/uk/getmondo/d/k;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/c;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/c;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/c;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/c;->d()I

    move-result v0

    invoke-direct {v3, v4, v5, v6, v0}, Lco/uk/getmondo/d/k;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 33
    :cond_0
    return-object v1
.end method


# virtual methods
.method public a(Lco/uk/getmondo/api/model/ApiWaitlistPosition;)Lco/uk/getmondo/d/an;
    .locals 10

    .prologue
    .line 16
    new-instance v0, Lco/uk/getmondo/d/an;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiWaitlistPosition;->a()Ljava/lang/String;

    move-result-object v1

    .line 17
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiWaitlistPosition;->b()I

    move-result v2

    .line 18
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiWaitlistPosition;->c()I

    move-result v3

    .line 19
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiWaitlistPosition;->d()Z

    move-result v4

    .line 20
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiWaitlistPosition;->e()Ljava/util/List;

    move-result-object v5

    invoke-direct {p0, v5}, Lco/uk/getmondo/d/a/v;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    .line 21
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiWaitlistPosition;->f()Ljava/lang/String;

    move-result-object v6

    .line 22
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiWaitlistPosition;->g()I

    move-result v7

    .line 23
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiWaitlistPosition;->h()Z

    move-result v8

    .line 24
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiWaitlistPosition;->i()Ljava/lang/Boolean;

    move-result-object v9

    invoke-direct/range {v0 .. v9}, Lco/uk/getmondo/d/an;-><init>(Ljava/lang/String;IIZLjava/util/List;Ljava/lang/String;IZLjava/lang/Boolean;)V

    .line 16
    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    check-cast p1, Lco/uk/getmondo/api/model/ApiWaitlistPosition;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/a/v;->a(Lco/uk/getmondo/api/model/ApiWaitlistPosition;)Lco/uk/getmondo/d/an;

    move-result-object v0

    return-object v0
.end method
