.class public final Lco/uk/getmondo/d/a/q;
.super Lco/uk/getmondo/d/a/a;
.source "RetailAccountMapper.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/d/a/a",
        "<",
        "Lco/uk/getmondo/d/ad;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0008H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "Lco/uk/getmondo/model/mapper/RetailAccountMapper;",
        "Lco/uk/getmondo/model/mapper/AccountMapper;",
        "Lco/uk/getmondo/model/RetailAccount;",
        "cardActivated",
        "",
        "(Z)V",
        "apply",
        "apiValue",
        "Lco/uk/getmondo/api/model/ApiAccount;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0, p1}, Lco/uk/getmondo/d/a/a;-><init>(Z)V

    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/api/model/ApiAccount;)Lco/uk/getmondo/d/ad;
    .locals 9

    .prologue
    const-string v0, "apiValue"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    new-instance v0, Lco/uk/getmondo/d/ad;

    .line 11
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiAccount;->a()Ljava/lang/String;

    move-result-object v1

    .line 12
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiAccount;->c()Ljava/lang/String;

    move-result-object v2

    .line 13
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiAccount;->b()Lorg/threeten/bp/LocalDateTime;

    move-result-object v3

    .line 14
    invoke-virtual {p0}, Lco/uk/getmondo/d/a/q;->a()Z

    move-result v4

    .line 15
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiAccount;->e()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    .line 16
    :cond_0
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiAccount;->d()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_1

    invoke-static {}, Lkotlin/d/b/l;->a()V

    .line 17
    :cond_1
    sget-object v5, Lco/uk/getmondo/d/a$b;->Find:Lco/uk/getmondo/d/a$b$a;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiAccount;->f()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lco/uk/getmondo/d/a$b$a;->a(Ljava/lang/String;)Lco/uk/getmondo/d/a$b;

    move-result-object v5

    .line 10
    invoke-direct/range {v0 .. v7}, Lco/uk/getmondo/d/ad;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDateTime;ZLco/uk/getmondo/d/a$b;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 7
    check-cast p1, Lco/uk/getmondo/api/model/ApiAccount;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/a/q;->a(Lco/uk/getmondo/api/model/ApiAccount;)Lco/uk/getmondo/d/ad;

    move-result-object v0

    return-object v0
.end method
