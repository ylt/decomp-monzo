.class public final Lco/uk/getmondo/d/ab;
.super Ljava/lang/Object;
.source "PrepaidAccount.kt"

# interfaces
.implements Lco/uk/getmondo/d/a;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0015\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0006\u0008\u0086\u0008\u0018\u00002\u00020\u0001B7\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0008\u0010\t\u001a\u0004\u0018\u00010\n\u0012\u0006\u0010\u000b\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\u000cJ\t\u0010\u0017\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0008H\u00c6\u0003J\u000b\u0010\u001b\u001a\u0004\u0018\u00010\nH\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u0008H\u00c6\u0003JG\u0010\u001d\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u0008H\u00c6\u0001J\u0013\u0010\u001e\u001a\u00020\u00082\u0008\u0010\u001f\u001a\u0004\u0018\u00010 H\u00d6\u0003J\t\u0010!\u001a\u00020\"H\u00d6\u0001J\t\u0010#\u001a\u00020\u0003H\u00d6\u0001J\u0010\u0010$\u001a\u00020\u00012\u0006\u0010%\u001a\u00020\u0008H\u0016J\u000e\u0010&\u001a\u00020\u00012\u0006\u0010\'\u001a\u00020\u0008R\u0014\u0010\u0007\u001a\u00020\u0008X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0014\u0010\u0004\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0012R\u0011\u0010\u000b\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u000eR\u0016\u0010\t\u001a\u0004\u0018\u00010\nX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016\u00a8\u0006("
    }
    d2 = {
        "Lco/uk/getmondo/model/PrepaidAccount;",
        "Lco/uk/getmondo/model/Account;",
        "id",
        "",
        "description",
        "created",
        "Lorg/threeten/bp/LocalDateTime;",
        "cardActivated",
        "",
        "type",
        "Lco/uk/getmondo/model/Account$Type;",
        "initialTopupCompleted",
        "(Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDateTime;ZLco/uk/getmondo/model/Account$Type;Z)V",
        "getCardActivated",
        "()Z",
        "getCreated",
        "()Lorg/threeten/bp/LocalDateTime;",
        "getDescription",
        "()Ljava/lang/String;",
        "getId",
        "getInitialTopupCompleted",
        "getType",
        "()Lco/uk/getmondo/model/Account$Type;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "withCardActivated",
        "isActivated",
        "withInitialTopUpCompleted",
        "isCompleted",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final cardActivated:Z

.field private final created:Lorg/threeten/bp/LocalDateTime;

.field private final description:Ljava/lang/String;

.field private final id:Ljava/lang/String;

.field private final initialTopupCompleted:Z

.field private final type:Lco/uk/getmondo/d/a$b;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDateTime;ZLco/uk/getmondo/d/a$b;Z)V
    .locals 1

    .prologue
    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "description"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "created"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/d/ab;->id:Ljava/lang/String;

    iput-object p2, p0, Lco/uk/getmondo/d/ab;->description:Ljava/lang/String;

    iput-object p3, p0, Lco/uk/getmondo/d/ab;->created:Lorg/threeten/bp/LocalDateTime;

    iput-boolean p4, p0, Lco/uk/getmondo/d/ab;->cardActivated:Z

    iput-object p5, p0, Lco/uk/getmondo/d/ab;->type:Lco/uk/getmondo/d/a$b;

    iput-boolean p6, p0, Lco/uk/getmondo/d/ab;->initialTopupCompleted:Z

    return-void
.end method

.method public static synthetic a(Lco/uk/getmondo/d/ab;Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDateTime;ZLco/uk/getmondo/d/a$b;ZILjava/lang/Object;)Lco/uk/getmondo/d/ab;
    .locals 7

    and-int/lit8 v0, p7, 0x1

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lco/uk/getmondo/d/ab;->a()Ljava/lang/String;

    move-result-object v1

    :goto_0
    and-int/lit8 v0, p7, 0x2

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lco/uk/getmondo/d/ab;->g()Ljava/lang/String;

    move-result-object v2

    :goto_1
    and-int/lit8 v0, p7, 0x4

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lco/uk/getmondo/d/ab;->b()Lorg/threeten/bp/LocalDateTime;

    move-result-object v3

    :goto_2
    and-int/lit8 v0, p7, 0x8

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lco/uk/getmondo/d/ab;->c()Z

    move-result v4

    :goto_3
    and-int/lit8 v0, p7, 0x10

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lco/uk/getmondo/d/ab;->d()Lco/uk/getmondo/d/a$b;

    move-result-object v5

    :goto_4
    and-int/lit8 v0, p7, 0x20

    if-eqz v0, :cond_0

    iget-boolean v6, p0, Lco/uk/getmondo/d/ab;->initialTopupCompleted:Z

    :goto_5
    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lco/uk/getmondo/d/ab;->a(Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDateTime;ZLco/uk/getmondo/d/a$b;Z)Lco/uk/getmondo/d/ab;

    move-result-object v0

    return-object v0

    :cond_0
    move v6, p6

    goto :goto_5

    :cond_1
    move-object v5, p5

    goto :goto_4

    :cond_2
    move v4, p4

    goto :goto_3

    :cond_3
    move-object v3, p3

    goto :goto_2

    :cond_4
    move-object v2, p2

    goto :goto_1

    :cond_5
    move-object v1, p1

    goto :goto_0
.end method


# virtual methods
.method public a(Z)Lco/uk/getmondo/d/a;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 17
    const/4 v6, 0x0

    const/16 v7, 0x37

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move v4, p1

    move-object v5, v1

    move-object v8, v1

    invoke-static/range {v0 .. v8}, Lco/uk/getmondo/d/ab;->a(Lco/uk/getmondo/d/ab;Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDateTime;ZLco/uk/getmondo/d/a$b;ZILjava/lang/Object;)Lco/uk/getmondo/d/ab;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/a;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDateTime;ZLco/uk/getmondo/d/a$b;Z)Lco/uk/getmondo/d/ab;
    .locals 7

    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "description"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "created"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lco/uk/getmondo/d/ab;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lco/uk/getmondo/d/ab;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDateTime;ZLco/uk/getmondo/d/a$b;Z)V

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lco/uk/getmondo/d/ab;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Z)Lco/uk/getmondo/d/a;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 21
    const/4 v4, 0x0

    const/16 v7, 0x1f

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v5, v1

    move v6, p1

    move-object v8, v1

    invoke-static/range {v0 .. v8}, Lco/uk/getmondo/d/ab;->a(Lco/uk/getmondo/d/ab;Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDateTime;ZLco/uk/getmondo/d/a$b;ZILjava/lang/Object;)Lco/uk/getmondo/d/ab;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/a;

    return-object v0
.end method

.method public b()Lorg/threeten/bp/LocalDateTime;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lco/uk/getmondo/d/ab;->created:Lorg/threeten/bp/LocalDateTime;

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 9
    iget-boolean v0, p0, Lco/uk/getmondo/d/ab;->cardActivated:Z

    return v0
.end method

.method public d()Lco/uk/getmondo/d/a$b;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lco/uk/getmondo/d/ab;->type:Lco/uk/getmondo/d/a$b;

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 5
    invoke-static {p0}, Lco/uk/getmondo/d/a$a;->a(Lco/uk/getmondo/d/a;)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eq p0, p1, :cond_0

    instance-of v2, p1, Lco/uk/getmondo/d/ab;

    if-eqz v2, :cond_1

    check-cast p1, Lco/uk/getmondo/d/ab;

    invoke-virtual {p0}, Lco/uk/getmondo/d/ab;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/ab;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lco/uk/getmondo/d/ab;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/ab;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lco/uk/getmondo/d/ab;->b()Lorg/threeten/bp/LocalDateTime;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/ab;->b()Lorg/threeten/bp/LocalDateTime;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lco/uk/getmondo/d/ab;->c()Z

    move-result v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/ab;->c()Z

    move-result v3

    if-ne v2, v3, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lco/uk/getmondo/d/ab;->d()Lco/uk/getmondo/d/a$b;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/ab;->d()Lco/uk/getmondo/d/a$b;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lco/uk/getmondo/d/ab;->initialTopupCompleted:Z

    iget-boolean v3, p1, Lco/uk/getmondo/d/ab;->initialTopupCompleted:Z

    if-ne v2, v3, :cond_3

    move v2, v1

    :goto_1
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 5
    invoke-static {p0}, Lco/uk/getmondo/d/a$a;->b(Lco/uk/getmondo/d/a;)Z

    move-result v0

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lco/uk/getmondo/d/ab;->description:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 13
    iget-boolean v0, p0, Lco/uk/getmondo/d/ab;->initialTopupCompleted:Z

    return v0
.end method

.method public hashCode()I
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lco/uk/getmondo/d/ab;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v3, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/ab;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/ab;->b()Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/ab;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    :cond_0
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/ab;->d()Lco/uk/getmondo/d/a$b;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lco/uk/getmondo/d/ab;->initialTopupCompleted:Z

    if-eqz v0, :cond_5

    :goto_3
    add-int v0, v1, v2

    return v0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2

    :cond_5
    move v2, v0

    goto :goto_3
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PrepaidAccount(id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/d/ab;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/d/ab;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", created="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/d/ab;->b()Lorg/threeten/bp/LocalDateTime;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", cardActivated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/d/ab;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/d/ab;->d()Lco/uk/getmondo/d/a$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", initialTopupCompleted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lco/uk/getmondo/d/ab;->initialTopupCompleted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
