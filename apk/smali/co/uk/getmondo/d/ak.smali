.class public final Lco/uk/getmondo/d/ak;
.super Ljava/lang/Object;
.source "User.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/d/ak$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0001.BA\u0008\u0007\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t\u0012\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u000bJ\u000b\u0010\u001c\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\u001d\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u000b\u0010\u001e\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J\u000b\u0010\u001f\u001a\u0004\u0018\u00010\tH\u00c6\u0003J\u000b\u0010 \u001a\u0004\u0018\u00010\u0003H\u00c6\u0003JE\u0010!\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t2\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010\"\u001a\u00020\u000f2\u0008\u0010#\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010$\u001a\u00020%H\u00d6\u0001J\t\u0010&\u001a\u00020\u0003H\u00d6\u0001J\u000e\u0010\'\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005J\u000e\u0010(\u001a\u00020\u00002\u0006\u0010\u0006\u001a\u00020\u0007J\u000e\u0010)\u001a\u00020\u00002\u0006\u0010*\u001a\u00020+J\u0018\u0010,\u001a\u00020\u00002\u0006\u0010\u0006\u001a\u00020\u00072\u0008\u0010\u0008\u001a\u0004\u0018\u00010\tJ\u0010\u0010-\u001a\u00020\u00002\u0008\u0010\u0008\u001a\u0004\u0018\u00010\tR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0014\u0010\u000e\u001a\u00020\u000f8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u0010R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0013\u0010\n\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\u0015\u001a\u00020\u00168F\u00a2\u0006\u0006\u001a\u0004\u0008\u0017\u0010\u0018R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u0014R\u0013\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001b\u00a8\u0006/"
    }
    d2 = {
        "Lco/uk/getmondo/model/User;",
        "",
        "userId",
        "",
        "account",
        "Lco/uk/getmondo/model/Account;",
        "profile",
        "Lco/uk/getmondo/model/Profile;",
        "waitlistProfile",
        "Lco/uk/getmondo/model/WaitlistProfile;",
        "secondaryAccountId",
        "(Ljava/lang/String;Lco/uk/getmondo/model/Account;Lco/uk/getmondo/model/Profile;Lco/uk/getmondo/model/WaitlistProfile;Ljava/lang/String;)V",
        "getAccount",
        "()Lco/uk/getmondo/model/Account;",
        "isGoogleTestUser",
        "",
        "()Z",
        "getProfile",
        "()Lco/uk/getmondo/model/Profile;",
        "getSecondaryAccountId",
        "()Ljava/lang/String;",
        "state",
        "Lco/uk/getmondo/model/User$State;",
        "getState",
        "()Lco/uk/getmondo/model/User$State;",
        "getUserId",
        "getWaitlistProfile",
        "()Lco/uk/getmondo/model/WaitlistProfile;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "withAccount",
        "withProfile",
        "withProfileAndAccountInfo",
        "info",
        "Lco/uk/getmondo/common/accounts/ProfileAndAccountInfo;",
        "withProfileAndWaitlistProfile",
        "withWaitlistProfile",
        "State",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final account:Lco/uk/getmondo/d/a;

.field private final profile:Lco/uk/getmondo/d/ac;

.field private final secondaryAccountId:Ljava/lang/String;

.field private final userId:Ljava/lang/String;

.field private final waitlistProfile:Lco/uk/getmondo/d/an;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 8

    const/4 v2, 0x0

    const/16 v6, 0x1e

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v7, v2

    invoke-direct/range {v0 .. v7}, Lco/uk/getmondo/d/ak;-><init>(Ljava/lang/String;Lco/uk/getmondo/d/a;Lco/uk/getmondo/d/ac;Lco/uk/getmondo/d/an;Ljava/lang/String;ILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lco/uk/getmondo/d/a;)V
    .locals 8

    const/4 v3, 0x0

    const/16 v6, 0x1c

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    move-object v7, v3

    invoke-direct/range {v0 .. v7}, Lco/uk/getmondo/d/ak;-><init>(Ljava/lang/String;Lco/uk/getmondo/d/a;Lco/uk/getmondo/d/ac;Lco/uk/getmondo/d/an;Ljava/lang/String;ILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lco/uk/getmondo/d/a;Lco/uk/getmondo/d/ac;)V
    .locals 8

    const/4 v4, 0x0

    const/16 v6, 0x18

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v0 .. v7}, Lco/uk/getmondo/d/ak;-><init>(Ljava/lang/String;Lco/uk/getmondo/d/a;Lco/uk/getmondo/d/ac;Lco/uk/getmondo/d/an;Ljava/lang/String;ILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lco/uk/getmondo/d/a;Lco/uk/getmondo/d/ac;Lco/uk/getmondo/d/an;)V
    .locals 8

    const/4 v5, 0x0

    const/16 v6, 0x10

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v7, v5

    invoke-direct/range {v0 .. v7}, Lco/uk/getmondo/d/ak;-><init>(Ljava/lang/String;Lco/uk/getmondo/d/a;Lco/uk/getmondo/d/ac;Lco/uk/getmondo/d/an;Ljava/lang/String;ILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lco/uk/getmondo/d/a;Lco/uk/getmondo/d/ac;Lco/uk/getmondo/d/an;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/d/ak;->userId:Ljava/lang/String;

    iput-object p2, p0, Lco/uk/getmondo/d/ak;->account:Lco/uk/getmondo/d/a;

    iput-object p3, p0, Lco/uk/getmondo/d/ak;->profile:Lco/uk/getmondo/d/ac;

    iput-object p4, p0, Lco/uk/getmondo/d/ak;->waitlistProfile:Lco/uk/getmondo/d/an;

    iput-object p5, p0, Lco/uk/getmondo/d/ak;->secondaryAccountId:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lco/uk/getmondo/d/a;Lco/uk/getmondo/d/ac;Lco/uk/getmondo/d/an;Ljava/lang/String;ILkotlin/d/b/i;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    and-int/lit8 v0, p6, 0x2

    if-eqz v0, :cond_3

    move-object v0, v1

    .line 11
    check-cast v0, Lco/uk/getmondo/d/a;

    move-object v2, v0

    :goto_0
    and-int/lit8 v0, p6, 0x4

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 12
    check-cast v0, Lco/uk/getmondo/d/ac;

    move-object v3, v0

    :goto_1
    and-int/lit8 v0, p6, 0x8

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 13
    check-cast v0, Lco/uk/getmondo/d/an;

    move-object v4, v0

    :goto_2
    and-int/lit8 v0, p6, 0x10

    if-eqz v0, :cond_0

    .line 14
    check-cast v1, Ljava/lang/String;

    move-object v5, v1

    :goto_3
    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/d/ak;-><init>(Ljava/lang/String;Lco/uk/getmondo/d/a;Lco/uk/getmondo/d/ac;Lco/uk/getmondo/d/an;Ljava/lang/String;)V

    return-void

    :cond_0
    move-object v5, p5

    goto :goto_3

    :cond_1
    move-object v4, p4

    goto :goto_2

    :cond_2
    move-object v3, p3

    goto :goto_1

    :cond_3
    move-object v2, p2

    goto :goto_0
.end method

.method public static bridge synthetic a(Lco/uk/getmondo/d/ak;Ljava/lang/String;Lco/uk/getmondo/d/a;Lco/uk/getmondo/d/ac;Lco/uk/getmondo/d/an;Ljava/lang/String;ILjava/lang/Object;)Lco/uk/getmondo/d/ak;
    .locals 6

    and-int/lit8 v0, p6, 0x1

    if-eqz v0, :cond_4

    iget-object v1, p0, Lco/uk/getmondo/d/ak;->userId:Ljava/lang/String;

    :goto_0
    and-int/lit8 v0, p6, 0x2

    if-eqz v0, :cond_3

    iget-object v2, p0, Lco/uk/getmondo/d/ak;->account:Lco/uk/getmondo/d/a;

    :goto_1
    and-int/lit8 v0, p6, 0x4

    if-eqz v0, :cond_2

    iget-object v3, p0, Lco/uk/getmondo/d/ak;->profile:Lco/uk/getmondo/d/ac;

    :goto_2
    and-int/lit8 v0, p6, 0x8

    if-eqz v0, :cond_1

    iget-object v4, p0, Lco/uk/getmondo/d/ak;->waitlistProfile:Lco/uk/getmondo/d/an;

    :goto_3
    and-int/lit8 v0, p6, 0x10

    if-eqz v0, :cond_0

    iget-object v5, p0, Lco/uk/getmondo/d/ak;->secondaryAccountId:Ljava/lang/String;

    :goto_4
    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lco/uk/getmondo/d/ak;->a(Ljava/lang/String;Lco/uk/getmondo/d/a;Lco/uk/getmondo/d/ac;Lco/uk/getmondo/d/an;Ljava/lang/String;)Lco/uk/getmondo/d/ak;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v5, p5

    goto :goto_4

    :cond_1
    move-object v4, p4

    goto :goto_3

    :cond_2
    move-object v3, p3

    goto :goto_2

    :cond_3
    move-object v2, p2

    goto :goto_1

    :cond_4
    move-object v1, p1

    goto :goto_0
.end method

.method private final g()Z
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lco/uk/getmondo/d/ak;->profile:Lco/uk/getmondo/d/ac;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/d/ak;->profile:Lco/uk/getmondo/d/ac;

    invoke-virtual {v0}, Lco/uk/getmondo/d/ac;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "apmonzotest@gmail.com"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lco/uk/getmondo/d/ak$a;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 29
    sget-object v0, Lco/uk/getmondo/a;->c:Ljava/lang/Boolean;

    const-string v2, "BuildConfig.BANK"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 30
    iget-object v0, p0, Lco/uk/getmondo/d/ak;->profile:Lco/uk/getmondo/d/ac;

    if-nez v0, :cond_0

    sget-object v0, Lco/uk/getmondo/d/ak$a;->NO_PROFILE:Lco/uk/getmondo/d/ak$a;

    .line 37
    :goto_0
    return-object v0

    .line 32
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/d/ak;->account:Lco/uk/getmondo/d/a;

    if-nez v0, :cond_1

    sget-object v0, Lco/uk/getmondo/d/ak$a;->ACCOUNT_CREATION:Lco/uk/getmondo/d/ak$a;

    goto :goto_0

    .line 33
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/d/ak;->account:Lco/uk/getmondo/d/a;

    invoke-interface {v0}, Lco/uk/getmondo/d/a;->c()Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lco/uk/getmondo/d/ak$a;->NO_CARD:Lco/uk/getmondo/d/ak$a;

    goto :goto_0

    .line 34
    :cond_2
    sget-object v0, Lco/uk/getmondo/d/ak$a;->HAS_ACCOUNT:Lco/uk/getmondo/d/ak$a;

    goto :goto_0

    .line 37
    :cond_3
    iget-object v0, p0, Lco/uk/getmondo/d/ak;->account:Lco/uk/getmondo/d/a;

    instance-of v0, v0, Lco/uk/getmondo/d/ab;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lco/uk/getmondo/d/ak;->account:Lco/uk/getmondo/d/a;

    check-cast v0, Lco/uk/getmondo/d/ab;

    invoke-virtual {v0}, Lco/uk/getmondo/d/ab;->h()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-direct {p0}, Lco/uk/getmondo/d/ak;->g()Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Lco/uk/getmondo/d/ak$a;->INITIAL_TOPUP_REQUIRED:Lco/uk/getmondo/d/ak$a;

    goto :goto_0

    .line 39
    :cond_4
    iget-object v0, p0, Lco/uk/getmondo/d/ak;->account:Lco/uk/getmondo/d/a;

    if-eqz v0, :cond_5

    invoke-interface {v0}, Lco/uk/getmondo/d/a;->c()Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lco/uk/getmondo/d/ak$a;->NO_CARD:Lco/uk/getmondo/d/ak$a;

    goto :goto_0

    .line 40
    :cond_5
    iget-object v0, p0, Lco/uk/getmondo/d/ak;->account:Lco/uk/getmondo/d/a;

    if-eqz v0, :cond_6

    sget-object v0, Lco/uk/getmondo/d/ak$a;->HAS_ACCOUNT:Lco/uk/getmondo/d/ak$a;

    goto :goto_0

    .line 41
    :cond_6
    iget-object v0, p0, Lco/uk/getmondo/d/ak;->waitlistProfile:Lco/uk/getmondo/d/an;

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lco/uk/getmondo/d/an;->a()Z

    move-result v0

    :goto_1
    if-eqz v0, :cond_8

    sget-object v0, Lco/uk/getmondo/d/ak$a;->INELIGIBLE:Lco/uk/getmondo/d/ak$a;

    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_1

    .line 42
    :cond_8
    iget-object v0, p0, Lco/uk/getmondo/d/ak;->waitlistProfile:Lco/uk/getmondo/d/an;

    if-eqz v0, :cond_9

    invoke-virtual {v0}, Lco/uk/getmondo/d/an;->f()Z

    move-result v0

    :goto_2
    if-eqz v0, :cond_a

    sget-object v0, Lco/uk/getmondo/d/ak$a;->ACCOUNT_CREATION:Lco/uk/getmondo/d/ak$a;

    goto :goto_0

    :cond_9
    move v0, v1

    goto :goto_2

    .line 43
    :cond_a
    iget-object v0, p0, Lco/uk/getmondo/d/ak;->profile:Lco/uk/getmondo/d/ac;

    if-eqz v0, :cond_b

    sget-object v0, Lco/uk/getmondo/d/ak$a;->ON_WAITLIST:Lco/uk/getmondo/d/ak$a;

    goto :goto_0

    .line 44
    :cond_b
    sget-object v0, Lco/uk/getmondo/d/ak$a;->NO_PROFILE:Lco/uk/getmondo/d/ak$a;

    goto :goto_0
.end method

.method public final a(Lco/uk/getmondo/common/accounts/l;)Lco/uk/getmondo/d/ak;
    .locals 10

    .prologue
    const/4 v1, 0x0

    const-string v0, "info"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-virtual {p1}, Lco/uk/getmondo/common/accounts/l;->a()Lco/uk/getmondo/d/ac;

    move-result-object v0

    if-nez v0, :cond_0

    .line 57
    invoke-virtual {p1}, Lco/uk/getmondo/common/accounts/l;->a()Lco/uk/getmondo/d/ac;

    move-result-object v3

    invoke-virtual {p1}, Lco/uk/getmondo/common/accounts/l;->a()Lco/uk/getmondo/d/ac;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/d/a;

    invoke-virtual {p1}, Lco/uk/getmondo/common/accounts/l;->c()Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x9

    move-object v0, p0

    move-object v4, v1

    move-object v7, v1

    invoke-static/range {v0 .. v7}, Lco/uk/getmondo/d/ak;->a(Lco/uk/getmondo/d/ak;Ljava/lang/String;Lco/uk/getmondo/d/a;Lco/uk/getmondo/d/ac;Lco/uk/getmondo/d/an;Ljava/lang/String;ILjava/lang/Object;)Lco/uk/getmondo/d/ak;

    move-result-object v0

    .line 59
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lco/uk/getmondo/common/accounts/l;->a()Lco/uk/getmondo/d/ac;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/ac;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lco/uk/getmondo/common/accounts/l;->a()Lco/uk/getmondo/d/ac;

    move-result-object v5

    invoke-virtual {p1}, Lco/uk/getmondo/common/accounts/l;->b()Lco/uk/getmondo/d/a;

    move-result-object v4

    invoke-virtual {p1}, Lco/uk/getmondo/common/accounts/l;->c()Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x8

    move-object v2, p0

    move-object v6, v1

    move-object v9, v1

    invoke-static/range {v2 .. v9}, Lco/uk/getmondo/d/ak;->a(Lco/uk/getmondo/d/ak;Ljava/lang/String;Lco/uk/getmondo/d/a;Lco/uk/getmondo/d/ac;Lco/uk/getmondo/d/an;Ljava/lang/String;ILjava/lang/Object;)Lco/uk/getmondo/d/ak;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lco/uk/getmondo/d/a;)Lco/uk/getmondo/d/ak;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const-string v0, "account"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    const/16 v6, 0x1d

    move-object v0, p0

    move-object v2, p1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v7, v1

    invoke-static/range {v0 .. v7}, Lco/uk/getmondo/d/ak;->a(Lco/uk/getmondo/d/ak;Ljava/lang/String;Lco/uk/getmondo/d/a;Lco/uk/getmondo/d/ac;Lco/uk/getmondo/d/an;Ljava/lang/String;ILjava/lang/Object;)Lco/uk/getmondo/d/ak;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/d/ac;)Lco/uk/getmondo/d/ak;
    .locals 8

    .prologue
    const/4 v2, 0x0

    const-string v0, "profile"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-virtual {p1}, Lco/uk/getmondo/d/ac;->b()Ljava/lang/String;

    move-result-object v1

    const/16 v6, 0x1a

    move-object v0, p0

    move-object v3, p1

    move-object v4, v2

    move-object v5, v2

    move-object v7, v2

    invoke-static/range {v0 .. v7}, Lco/uk/getmondo/d/ak;->a(Lco/uk/getmondo/d/ak;Ljava/lang/String;Lco/uk/getmondo/d/a;Lco/uk/getmondo/d/ac;Lco/uk/getmondo/d/an;Ljava/lang/String;ILjava/lang/Object;)Lco/uk/getmondo/d/ak;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/d/ac;Lco/uk/getmondo/d/an;)Lco/uk/getmondo/d/ak;
    .locals 8

    .prologue
    const/4 v2, 0x0

    const-string v0, "profile"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-virtual {p1}, Lco/uk/getmondo/d/ac;->b()Ljava/lang/String;

    move-result-object v1

    const/16 v6, 0x12

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, v2

    move-object v7, v2

    invoke-static/range {v0 .. v7}, Lco/uk/getmondo/d/ak;->a(Lco/uk/getmondo/d/ak;Ljava/lang/String;Lco/uk/getmondo/d/a;Lco/uk/getmondo/d/ac;Lco/uk/getmondo/d/an;Ljava/lang/String;ILjava/lang/Object;)Lco/uk/getmondo/d/ak;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/d/an;)Lco/uk/getmondo/d/ak;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 53
    const/16 v6, 0x17

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, p1

    move-object v5, v1

    move-object v7, v1

    invoke-static/range {v0 .. v7}, Lco/uk/getmondo/d/ak;->a(Lco/uk/getmondo/d/ak;Ljava/lang/String;Lco/uk/getmondo/d/a;Lco/uk/getmondo/d/ac;Lco/uk/getmondo/d/an;Ljava/lang/String;ILjava/lang/Object;)Lco/uk/getmondo/d/ak;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lco/uk/getmondo/d/a;Lco/uk/getmondo/d/ac;Lco/uk/getmondo/d/an;Ljava/lang/String;)Lco/uk/getmondo/d/ak;
    .locals 6

    new-instance v0, Lco/uk/getmondo/d/ak;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/d/ak;-><init>(Ljava/lang/String;Lco/uk/getmondo/d/a;Lco/uk/getmondo/d/ac;Lco/uk/getmondo/d/an;Ljava/lang/String;)V

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lco/uk/getmondo/d/ak;->userId:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lco/uk/getmondo/d/a;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lco/uk/getmondo/d/ak;->account:Lco/uk/getmondo/d/a;

    return-object v0
.end method

.method public final d()Lco/uk/getmondo/d/ac;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lco/uk/getmondo/d/ak;->profile:Lco/uk/getmondo/d/ac;

    return-object v0
.end method

.method public final e()Lco/uk/getmondo/d/an;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lco/uk/getmondo/d/ak;->waitlistProfile:Lco/uk/getmondo/d/an;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lco/uk/getmondo/d/ak;

    if-eqz v0, :cond_1

    check-cast p1, Lco/uk/getmondo/d/ak;

    iget-object v0, p0, Lco/uk/getmondo/d/ak;->userId:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/d/ak;->userId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/d/ak;->account:Lco/uk/getmondo/d/a;

    iget-object v1, p1, Lco/uk/getmondo/d/ak;->account:Lco/uk/getmondo/d/a;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/d/ak;->profile:Lco/uk/getmondo/d/ac;

    iget-object v1, p1, Lco/uk/getmondo/d/ak;->profile:Lco/uk/getmondo/d/ac;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/d/ak;->waitlistProfile:Lco/uk/getmondo/d/an;

    iget-object v1, p1, Lco/uk/getmondo/d/ak;->waitlistProfile:Lco/uk/getmondo/d/an;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/d/ak;->secondaryAccountId:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/d/ak;->secondaryAccountId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lco/uk/getmondo/d/ak;->secondaryAccountId:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lco/uk/getmondo/d/ak;->userId:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/ak;->account:Lco/uk/getmondo/d/a;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/ak;->profile:Lco/uk/getmondo/d/ac;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/ak;->waitlistProfile:Lco/uk/getmondo/d/an;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/uk/getmondo/d/ak;->secondaryAccountId:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_3
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "User(userId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/ak;->userId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", account="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/ak;->account:Lco/uk/getmondo/d/a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", profile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/ak;->profile:Lco/uk/getmondo/d/ac;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", waitlistProfile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/ak;->waitlistProfile:Lco/uk/getmondo/d/an;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", secondaryAccountId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/ak;->secondaryAccountId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
