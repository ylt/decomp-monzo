.class public final enum Lco/uk/getmondo/d/af;
.super Ljava/lang/Enum;
.source "Scheme.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/d/af$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/d/af;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u000c\u0008\u0086\u0001\u0018\u0000 \u000e2\u0008\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u000eB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\r\u00a8\u0006\u000f"
    }
    d2 = {
        "Lco/uk/getmondo/model/Scheme;",
        "",
        "apiValue",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "getApiValue",
        "()Ljava/lang/String;",
        "GPS_MASTERCARD",
        "BACS",
        "FASTER_PAYMENT",
        "MASTERCARD",
        "OVERDRAFT",
        "CHAPS",
        "UNKNOWN",
        "Find",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lco/uk/getmondo/d/af;

.field public static final enum BACS:Lco/uk/getmondo/d/af;

.field public static final enum CHAPS:Lco/uk/getmondo/d/af;

.field public static final enum FASTER_PAYMENT:Lco/uk/getmondo/d/af;

.field public static final Find:Lco/uk/getmondo/d/af$a;

.field public static final enum GPS_MASTERCARD:Lco/uk/getmondo/d/af;

.field public static final enum MASTERCARD:Lco/uk/getmondo/d/af;

.field public static final enum OVERDRAFT:Lco/uk/getmondo/d/af;

.field public static final enum UNKNOWN:Lco/uk/getmondo/d/af;


# instance fields
.field private final apiValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x7

    new-array v0, v0, [Lco/uk/getmondo/d/af;

    new-instance v1, Lco/uk/getmondo/d/af;

    const-string v2, "GPS_MASTERCARD"

    .line 4
    const-string v3, "gps_mastercard"

    invoke-direct {v1, v2, v4, v3}, Lco/uk/getmondo/d/af;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/d/af;->GPS_MASTERCARD:Lco/uk/getmondo/d/af;

    aput-object v1, v0, v4

    new-instance v1, Lco/uk/getmondo/d/af;

    const-string v2, "BACS"

    .line 5
    const-string v3, "bacs"

    invoke-direct {v1, v2, v5, v3}, Lco/uk/getmondo/d/af;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/d/af;->BACS:Lco/uk/getmondo/d/af;

    aput-object v1, v0, v5

    new-instance v1, Lco/uk/getmondo/d/af;

    const-string v2, "FASTER_PAYMENT"

    .line 6
    const-string v3, "payport_faster_payments"

    invoke-direct {v1, v2, v6, v3}, Lco/uk/getmondo/d/af;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/d/af;->FASTER_PAYMENT:Lco/uk/getmondo/d/af;

    aput-object v1, v0, v6

    new-instance v1, Lco/uk/getmondo/d/af;

    const-string v2, "MASTERCARD"

    .line 7
    const-string v3, "mastercard"

    invoke-direct {v1, v2, v7, v3}, Lco/uk/getmondo/d/af;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/d/af;->MASTERCARD:Lco/uk/getmondo/d/af;

    aput-object v1, v0, v7

    new-instance v1, Lco/uk/getmondo/d/af;

    const-string v2, "OVERDRAFT"

    .line 8
    const-string v3, "overdraft"

    invoke-direct {v1, v2, v8, v3}, Lco/uk/getmondo/d/af;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/d/af;->OVERDRAFT:Lco/uk/getmondo/d/af;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    new-instance v2, Lco/uk/getmondo/d/af;

    const-string v3, "CHAPS"

    const/4 v4, 0x5

    .line 9
    const-string v5, "chaps"

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/d/af;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lco/uk/getmondo/d/af;->CHAPS:Lco/uk/getmondo/d/af;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lco/uk/getmondo/d/af;

    const-string v3, "UNKNOWN"

    const/4 v4, 0x6

    .line 10
    const-string v5, ""

    invoke-direct {v2, v3, v4, v5}, Lco/uk/getmondo/d/af;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lco/uk/getmondo/d/af;->UNKNOWN:Lco/uk/getmondo/d/af;

    aput-object v2, v0, v1

    sput-object v0, Lco/uk/getmondo/d/af;->$VALUES:[Lco/uk/getmondo/d/af;

    new-instance v0, Lco/uk/getmondo/d/af$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/d/af$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/d/af;->Find:Lco/uk/getmondo/d/af$a;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "apiValue"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lco/uk/getmondo/d/af;->apiValue:Ljava/lang/String;

    return-void
.end method

.method public static final a(Ljava/lang/String;)Lco/uk/getmondo/d/af;
    .locals 1

    sget-object v0, Lco/uk/getmondo/d/af;->Find:Lco/uk/getmondo/d/af$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/d/af$a;->a(Ljava/lang/String;)Lco/uk/getmondo/d/af;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/d/af;
    .locals 1

    const-class v0, Lco/uk/getmondo/d/af;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/af;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/d/af;
    .locals 1

    sget-object v0, Lco/uk/getmondo/d/af;->$VALUES:[Lco/uk/getmondo/d/af;

    invoke-virtual {v0}, [Lco/uk/getmondo/d/af;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/d/af;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3
    iget-object v0, p0, Lco/uk/getmondo/d/af;->apiValue:Ljava/lang/String;

    return-object v0
.end method
