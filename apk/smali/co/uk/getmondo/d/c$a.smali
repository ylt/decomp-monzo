.class public final Lco/uk/getmondo/d/c$a;
.super Ljava/lang/Object;
.source "Amount.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/d/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u001c\u0010\n\u001a\u0004\u0018\u00010\u00052\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0007R\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\u000f"
    }
    d2 = {
        "Lco/uk/getmondo/model/Amount$Companion;",
        "",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "Lco/uk/getmondo/model/Amount;",
        "defaultFormatter",
        "Lco/uk/getmondo/common/money/AmountFormatter;",
        "getDefaultFormatter",
        "()Lco/uk/getmondo/common/money/AmountFormatter;",
        "parseDecimal",
        "decimalString",
        "",
        "currency",
        "Lco/uk/getmondo/common/money/Currency;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Lco/uk/getmondo/d/c$a;-><init>()V

    return-void
.end method

.method private final a()Lco/uk/getmondo/common/i/b;
    .locals 1

    .prologue
    .line 94
    invoke-static {}, Lco/uk/getmondo/d/c;->m()Lco/uk/getmondo/common/i/b;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/d/c$a;)Lco/uk/getmondo/common/i/b;
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Lco/uk/getmondo/d/c$a;->a()Lco/uk/getmondo/common/i/b;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lco/uk/getmondo/common/i/c;)Lco/uk/getmondo/d/c;
    .locals 3

    .prologue
    const/4 v1, 0x0

    const-string v0, "currency"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    if-eqz p1, :cond_0

    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    :cond_0
    move-object v0, v1

    .line 104
    :goto_1
    return-object v0

    .line 97
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 101
    :cond_2
    nop

    .line 102
    :try_start_0
    new-instance v0, Lco/uk/getmondo/d/c;

    new-instance v2, Ljava/math/BigDecimal;

    invoke-direct {v2, p1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v2, p2}, Lco/uk/getmondo/d/c;-><init>(Ljava/math/BigDecimal;Lco/uk/getmondo/common/i/c;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 103
    :catch_0
    move-exception v0

    move-object v0, v1

    .line 104
    goto :goto_1
.end method
