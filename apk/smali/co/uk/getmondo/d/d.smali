.class public Lco/uk/getmondo/d/d;
.super Ljava/lang/Object;
.source "Attachment.java"

# interfaces
.implements Lio/realm/bb;
.implements Lio/realm/d;


# instance fields
.field private created:Ljava/util/Date;

.field private externalId:Ljava/lang/String;

.field private id:Ljava/lang/String;

.field private url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/l;

    invoke-interface {p0}, Lio/realm/internal/l;->u_()V

    .line 17
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->u_()V

    .line 20
    :cond_0
    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/d;->a(Ljava/lang/String;)V

    .line 21
    invoke-virtual {p0, p2}, Lco/uk/getmondo/d/d;->b(Ljava/lang/String;)V

    .line 22
    invoke-virtual {p0, p3}, Lco/uk/getmondo/d/d;->a(Ljava/util/Date;)V

    .line 23
    invoke-virtual {p0, p4}, Lco/uk/getmondo/d/d;->c(Ljava/lang/String;)V

    .line 24
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lco/uk/getmondo/d/d;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/d;->id:Ljava/lang/String;

    return-void
.end method

.method public a(Ljava/util/Date;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/d;->created:Ljava/util/Date;

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Lco/uk/getmondo/d/d;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/d;->externalId:Ljava/lang/String;

    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/d;->id:Ljava/lang/String;

    return-object v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/d;->url:Ljava/lang/String;

    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/d;->externalId:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/d;->created:Ljava/util/Date;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 40
    if-ne p0, p1, :cond_1

    move v1, v0

    .line 48
    :cond_0
    :goto_0
    return v1

    .line 41
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    .line 43
    check-cast p1, Lco/uk/getmondo/d/d;

    .line 45
    invoke-virtual {p0}, Lco/uk/getmondo/d/d;->c()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-virtual {p0}, Lco/uk/getmondo/d/d;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/d;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 46
    :cond_2
    invoke-virtual {p0}, Lco/uk/getmondo/d/d;->d()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {p0}, Lco/uk/getmondo/d/d;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/d;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 47
    :cond_3
    invoke-virtual {p0}, Lco/uk/getmondo/d/d;->e()Ljava/util/Date;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-virtual {p0}, Lco/uk/getmondo/d/d;->e()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/d;->e()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 48
    :cond_4
    invoke-virtual {p0}, Lco/uk/getmondo/d/d;->f()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-virtual {p0}, Lco/uk/getmondo/d/d;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/d/d;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :cond_5
    :goto_1
    move v1, v0

    goto :goto_0

    .line 45
    :cond_6
    invoke-virtual {p1}, Lco/uk/getmondo/d/d;->c()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    goto :goto_0

    .line 46
    :cond_7
    invoke-virtual {p1}, Lco/uk/getmondo/d/d;->d()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    goto :goto_0

    .line 47
    :cond_8
    invoke-virtual {p1}, Lco/uk/getmondo/d/d;->e()Ljava/util/Date;

    move-result-object v2

    if-eqz v2, :cond_4

    goto :goto_0

    .line 48
    :cond_9
    invoke-virtual {p1}, Lco/uk/getmondo/d/d;->f()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_1
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/d;->url:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 53
    invoke-virtual {p0}, Lco/uk/getmondo/d/d;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lco/uk/getmondo/d/d;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 54
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/d;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lco/uk/getmondo/d/d;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 55
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/d;->e()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lco/uk/getmondo/d/d;->e()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    .line 56
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/d;->f()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/d/d;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 57
    return v0

    :cond_1
    move v0, v1

    .line 53
    goto :goto_0

    :cond_2
    move v0, v1

    .line 54
    goto :goto_1

    :cond_3
    move v0, v1

    .line 55
    goto :goto_2
.end method
