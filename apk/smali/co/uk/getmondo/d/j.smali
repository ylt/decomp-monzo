.class public final enum Lco/uk/getmondo/d/j;
.super Ljava/lang/Enum;
.source "DeclineReason.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/d/j;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lco/uk/getmondo/d/j;

.field public static final enum CARD_BLOCKED:Lco/uk/getmondo/d/j;

.field public static final enum CARD_INACTIVE:Lco/uk/getmondo/d/j;

.field public static final enum CONTACTLESS_FAILURE:Lco/uk/getmondo/d/j;

.field public static final enum EXCEEDS_TRANSACTION_AMOUNT_LIMIT:Lco/uk/getmondo/d/j;

.field public static final enum EXCEEDS_TRANSACTION_AMOUNT_LIMIT_KYC_REQUIRED:Lco/uk/getmondo/d/j;

.field public static final enum EXCEEDS_WITHDRAWAL_AMOUNT_LIMIT:Lco/uk/getmondo/d/j;

.field public static final enum EXCEEDS_WITHDRAWAL_AMOUNT_LIMIT_KYC_REQUIRED:Lco/uk/getmondo/d/j;

.field public static final enum EXCEEDS_WITHDRAWAL_COUNT_LIMIT:Lco/uk/getmondo/d/j;

.field public static final enum EXCEEDS_WITHDRAWAL_COUNT_LIMIT_KYC_REQUIRED:Lco/uk/getmondo/d/j;

.field public static final enum INSUFFICIENT_FUNDS:Lco/uk/getmondo/d/j;

.field public static final enum INVALID_CVC:Lco/uk/getmondo/d/j;

.field public static final enum INVALID_EXPIRY_DATE:Lco/uk/getmondo/d/j;

.field public static final enum INVALID_MERCHANT:Lco/uk/getmondo/d/j;

.field public static final enum INVALID_PIN:Lco/uk/getmondo/d/j;

.field public static final enum KYC_REQUIRED:Lco/uk/getmondo/d/j;

.field public static final enum MAGNETIC_STRIP_ATM:Lco/uk/getmondo/d/j;

.field public static final enum NOT_DECLINED:Lco/uk/getmondo/d/j;

.field public static final enum OTHER:Lco/uk/getmondo/d/j;

.field public static final enum PIN_RETRY_COUNT_EXCEEDED:Lco/uk/getmondo/d/j;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 5
    new-instance v0, Lco/uk/getmondo/d/j;

    const-string v1, "NOT_DECLINED"

    invoke-direct {v0, v1, v3}, Lco/uk/getmondo/d/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/d/j;->NOT_DECLINED:Lco/uk/getmondo/d/j;

    .line 6
    new-instance v0, Lco/uk/getmondo/d/j;

    const-string v1, "OTHER"

    invoke-direct {v0, v1, v4}, Lco/uk/getmondo/d/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/d/j;->OTHER:Lco/uk/getmondo/d/j;

    .line 7
    new-instance v0, Lco/uk/getmondo/d/j;

    const-string v1, "INSUFFICIENT_FUNDS"

    invoke-direct {v0, v1, v5}, Lco/uk/getmondo/d/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/d/j;->INSUFFICIENT_FUNDS:Lco/uk/getmondo/d/j;

    .line 8
    new-instance v0, Lco/uk/getmondo/d/j;

    const-string v1, "CARD_BLOCKED"

    invoke-direct {v0, v1, v6}, Lco/uk/getmondo/d/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/d/j;->CARD_BLOCKED:Lco/uk/getmondo/d/j;

    .line 9
    new-instance v0, Lco/uk/getmondo/d/j;

    const-string v1, "CARD_INACTIVE"

    invoke-direct {v0, v1, v7}, Lco/uk/getmondo/d/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/d/j;->CARD_INACTIVE:Lco/uk/getmondo/d/j;

    .line 10
    new-instance v0, Lco/uk/getmondo/d/j;

    const-string v1, "INVALID_MERCHANT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/d/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/d/j;->INVALID_MERCHANT:Lco/uk/getmondo/d/j;

    .line 11
    new-instance v0, Lco/uk/getmondo/d/j;

    const-string v1, "MAGNETIC_STRIP_ATM"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/d/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/d/j;->MAGNETIC_STRIP_ATM:Lco/uk/getmondo/d/j;

    .line 12
    new-instance v0, Lco/uk/getmondo/d/j;

    const-string v1, "INVALID_EXPIRY_DATE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/d/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/d/j;->INVALID_EXPIRY_DATE:Lco/uk/getmondo/d/j;

    .line 13
    new-instance v0, Lco/uk/getmondo/d/j;

    const-string v1, "INVALID_CVC"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/d/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/d/j;->INVALID_CVC:Lco/uk/getmondo/d/j;

    .line 14
    new-instance v0, Lco/uk/getmondo/d/j;

    const-string v1, "CONTACTLESS_FAILURE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/d/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/d/j;->CONTACTLESS_FAILURE:Lco/uk/getmondo/d/j;

    .line 15
    new-instance v0, Lco/uk/getmondo/d/j;

    const-string v1, "KYC_REQUIRED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/d/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/d/j;->KYC_REQUIRED:Lco/uk/getmondo/d/j;

    .line 16
    new-instance v0, Lco/uk/getmondo/d/j;

    const-string v1, "EXCEEDS_TRANSACTION_AMOUNT_LIMIT"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/d/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/d/j;->EXCEEDS_TRANSACTION_AMOUNT_LIMIT:Lco/uk/getmondo/d/j;

    .line 17
    new-instance v0, Lco/uk/getmondo/d/j;

    const-string v1, "EXCEEDS_WITHDRAWAL_AMOUNT_LIMIT"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/d/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/d/j;->EXCEEDS_WITHDRAWAL_AMOUNT_LIMIT:Lco/uk/getmondo/d/j;

    .line 18
    new-instance v0, Lco/uk/getmondo/d/j;

    const-string v1, "EXCEEDS_WITHDRAWAL_COUNT_LIMIT"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/d/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/d/j;->EXCEEDS_WITHDRAWAL_COUNT_LIMIT:Lco/uk/getmondo/d/j;

    .line 19
    new-instance v0, Lco/uk/getmondo/d/j;

    const-string v1, "EXCEEDS_TRANSACTION_AMOUNT_LIMIT_KYC_REQUIRED"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/d/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/d/j;->EXCEEDS_TRANSACTION_AMOUNT_LIMIT_KYC_REQUIRED:Lco/uk/getmondo/d/j;

    .line 20
    new-instance v0, Lco/uk/getmondo/d/j;

    const-string v1, "EXCEEDS_WITHDRAWAL_AMOUNT_LIMIT_KYC_REQUIRED"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/d/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/d/j;->EXCEEDS_WITHDRAWAL_AMOUNT_LIMIT_KYC_REQUIRED:Lco/uk/getmondo/d/j;

    .line 21
    new-instance v0, Lco/uk/getmondo/d/j;

    const-string v1, "EXCEEDS_WITHDRAWAL_COUNT_LIMIT_KYC_REQUIRED"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/d/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/d/j;->EXCEEDS_WITHDRAWAL_COUNT_LIMIT_KYC_REQUIRED:Lco/uk/getmondo/d/j;

    .line 22
    new-instance v0, Lco/uk/getmondo/d/j;

    const-string v1, "PIN_RETRY_COUNT_EXCEEDED"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/d/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/d/j;->PIN_RETRY_COUNT_EXCEEDED:Lco/uk/getmondo/d/j;

    .line 23
    new-instance v0, Lco/uk/getmondo/d/j;

    const-string v1, "INVALID_PIN"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/d/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/d/j;->INVALID_PIN:Lco/uk/getmondo/d/j;

    .line 3
    const/16 v0, 0x13

    new-array v0, v0, [Lco/uk/getmondo/d/j;

    sget-object v1, Lco/uk/getmondo/d/j;->NOT_DECLINED:Lco/uk/getmondo/d/j;

    aput-object v1, v0, v3

    sget-object v1, Lco/uk/getmondo/d/j;->OTHER:Lco/uk/getmondo/d/j;

    aput-object v1, v0, v4

    sget-object v1, Lco/uk/getmondo/d/j;->INSUFFICIENT_FUNDS:Lco/uk/getmondo/d/j;

    aput-object v1, v0, v5

    sget-object v1, Lco/uk/getmondo/d/j;->CARD_BLOCKED:Lco/uk/getmondo/d/j;

    aput-object v1, v0, v6

    sget-object v1, Lco/uk/getmondo/d/j;->CARD_INACTIVE:Lco/uk/getmondo/d/j;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lco/uk/getmondo/d/j;->INVALID_MERCHANT:Lco/uk/getmondo/d/j;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lco/uk/getmondo/d/j;->MAGNETIC_STRIP_ATM:Lco/uk/getmondo/d/j;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lco/uk/getmondo/d/j;->INVALID_EXPIRY_DATE:Lco/uk/getmondo/d/j;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lco/uk/getmondo/d/j;->INVALID_CVC:Lco/uk/getmondo/d/j;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lco/uk/getmondo/d/j;->CONTACTLESS_FAILURE:Lco/uk/getmondo/d/j;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lco/uk/getmondo/d/j;->KYC_REQUIRED:Lco/uk/getmondo/d/j;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lco/uk/getmondo/d/j;->EXCEEDS_TRANSACTION_AMOUNT_LIMIT:Lco/uk/getmondo/d/j;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lco/uk/getmondo/d/j;->EXCEEDS_WITHDRAWAL_AMOUNT_LIMIT:Lco/uk/getmondo/d/j;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lco/uk/getmondo/d/j;->EXCEEDS_WITHDRAWAL_COUNT_LIMIT:Lco/uk/getmondo/d/j;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lco/uk/getmondo/d/j;->EXCEEDS_TRANSACTION_AMOUNT_LIMIT_KYC_REQUIRED:Lco/uk/getmondo/d/j;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lco/uk/getmondo/d/j;->EXCEEDS_WITHDRAWAL_AMOUNT_LIMIT_KYC_REQUIRED:Lco/uk/getmondo/d/j;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lco/uk/getmondo/d/j;->EXCEEDS_WITHDRAWAL_COUNT_LIMIT_KYC_REQUIRED:Lco/uk/getmondo/d/j;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lco/uk/getmondo/d/j;->PIN_RETRY_COUNT_EXCEEDED:Lco/uk/getmondo/d/j;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lco/uk/getmondo/d/j;->INVALID_PIN:Lco/uk/getmondo/d/j;

    aput-object v2, v0, v1

    sput-object v0, Lco/uk/getmondo/d/j;->$VALUES:[Lco/uk/getmondo/d/j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lco/uk/getmondo/d/j;
    .locals 2

    .prologue
    .line 26
    if-nez p0, :cond_0

    .line 27
    sget-object v0, Lco/uk/getmondo/d/j;->NOT_DECLINED:Lco/uk/getmondo/d/j;

    .line 68
    :goto_0
    return-object v0

    .line 30
    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 68
    sget-object v0, Lco/uk/getmondo/d/j;->OTHER:Lco/uk/getmondo/d/j;

    goto :goto_0

    .line 30
    :sswitch_0
    const-string v1, "OTHER"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string v1, "INSUFFICIENT_FUNDS"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_2
    const-string v1, "CARD_BLOCKED"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_3
    const-string v1, "CARD_INACTIVE"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_4
    const-string v1, "INVALID_MERCHANT"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x4

    goto :goto_1

    :sswitch_5
    const-string v1, "MAGNETIC_STRIP_ATM"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x5

    goto :goto_1

    :sswitch_6
    const-string v1, "INVALID_EXPIRY_DATE"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x6

    goto :goto_1

    :sswitch_7
    const-string v1, "INVALID_CVC"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x7

    goto :goto_1

    :sswitch_8
    const-string v1, "CONTACTLESS_FAILURE"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x8

    goto :goto_1

    :sswitch_9
    const-string v1, "KYC_REQUIRED"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x9

    goto :goto_1

    :sswitch_a
    const-string v1, "EXCEEDS_TRANSACTION_AMOUNT_LIMIT"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0xa

    goto :goto_1

    :sswitch_b
    const-string v1, "EXCEEDS_WITHDRAWAL_AMOUNT_LIMIT"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0xb

    goto/16 :goto_1

    :sswitch_c
    const-string v1, "EXCEEDS_WITHDRAWAL_COUNT_LIMIT"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0xc

    goto/16 :goto_1

    :sswitch_d
    const-string v1, "EXCEEDS_TRANSACTION_AMOUNT_LIMIT_KYC_REQUIRED"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0xd

    goto/16 :goto_1

    :sswitch_e
    const-string v1, "EXCEEDS_WITHDRAWAL_AMOUNT_LIMIT_KYC_REQUIRED"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0xe

    goto/16 :goto_1

    :sswitch_f
    const-string v1, "EXCEEDS_WITHDRAWAL_COUNT_LIMIT_KYC_REQUIRED"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0xf

    goto/16 :goto_1

    :sswitch_10
    const-string v1, "PIN_RETRY_COUNT_EXCEEDED"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x10

    goto/16 :goto_1

    :sswitch_11
    const-string v1, "INVALID_PIN"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x11

    goto/16 :goto_1

    .line 32
    :pswitch_0
    sget-object v0, Lco/uk/getmondo/d/j;->OTHER:Lco/uk/getmondo/d/j;

    goto/16 :goto_0

    .line 34
    :pswitch_1
    sget-object v0, Lco/uk/getmondo/d/j;->INSUFFICIENT_FUNDS:Lco/uk/getmondo/d/j;

    goto/16 :goto_0

    .line 36
    :pswitch_2
    sget-object v0, Lco/uk/getmondo/d/j;->CARD_BLOCKED:Lco/uk/getmondo/d/j;

    goto/16 :goto_0

    .line 38
    :pswitch_3
    sget-object v0, Lco/uk/getmondo/d/j;->CARD_INACTIVE:Lco/uk/getmondo/d/j;

    goto/16 :goto_0

    .line 40
    :pswitch_4
    sget-object v0, Lco/uk/getmondo/d/j;->INVALID_MERCHANT:Lco/uk/getmondo/d/j;

    goto/16 :goto_0

    .line 42
    :pswitch_5
    sget-object v0, Lco/uk/getmondo/d/j;->MAGNETIC_STRIP_ATM:Lco/uk/getmondo/d/j;

    goto/16 :goto_0

    .line 44
    :pswitch_6
    sget-object v0, Lco/uk/getmondo/d/j;->INVALID_EXPIRY_DATE:Lco/uk/getmondo/d/j;

    goto/16 :goto_0

    .line 46
    :pswitch_7
    sget-object v0, Lco/uk/getmondo/d/j;->INVALID_CVC:Lco/uk/getmondo/d/j;

    goto/16 :goto_0

    .line 48
    :pswitch_8
    sget-object v0, Lco/uk/getmondo/d/j;->CONTACTLESS_FAILURE:Lco/uk/getmondo/d/j;

    goto/16 :goto_0

    .line 50
    :pswitch_9
    sget-object v0, Lco/uk/getmondo/d/j;->KYC_REQUIRED:Lco/uk/getmondo/d/j;

    goto/16 :goto_0

    .line 52
    :pswitch_a
    sget-object v0, Lco/uk/getmondo/d/j;->EXCEEDS_TRANSACTION_AMOUNT_LIMIT:Lco/uk/getmondo/d/j;

    goto/16 :goto_0

    .line 54
    :pswitch_b
    sget-object v0, Lco/uk/getmondo/d/j;->EXCEEDS_WITHDRAWAL_AMOUNT_LIMIT:Lco/uk/getmondo/d/j;

    goto/16 :goto_0

    .line 56
    :pswitch_c
    sget-object v0, Lco/uk/getmondo/d/j;->EXCEEDS_WITHDRAWAL_COUNT_LIMIT:Lco/uk/getmondo/d/j;

    goto/16 :goto_0

    .line 58
    :pswitch_d
    sget-object v0, Lco/uk/getmondo/d/j;->EXCEEDS_TRANSACTION_AMOUNT_LIMIT_KYC_REQUIRED:Lco/uk/getmondo/d/j;

    goto/16 :goto_0

    .line 60
    :pswitch_e
    sget-object v0, Lco/uk/getmondo/d/j;->EXCEEDS_WITHDRAWAL_AMOUNT_LIMIT_KYC_REQUIRED:Lco/uk/getmondo/d/j;

    goto/16 :goto_0

    .line 62
    :pswitch_f
    sget-object v0, Lco/uk/getmondo/d/j;->EXCEEDS_WITHDRAWAL_COUNT_LIMIT_KYC_REQUIRED:Lco/uk/getmondo/d/j;

    goto/16 :goto_0

    .line 64
    :pswitch_10
    sget-object v0, Lco/uk/getmondo/d/j;->PIN_RETRY_COUNT_EXCEEDED:Lco/uk/getmondo/d/j;

    goto/16 :goto_0

    .line 66
    :pswitch_11
    sget-object v0, Lco/uk/getmondo/d/j;->INVALID_PIN:Lco/uk/getmondo/d/j;

    goto/16 :goto_0

    .line 30
    :sswitch_data_0
    .sparse-switch
        -0x69eb6d59 -> :sswitch_f
        -0x47d75476 -> :sswitch_e
        -0x3cf5743c -> :sswitch_10
        -0x2d4cb72e -> :sswitch_6
        -0x2563f0d0 -> :sswitch_4
        -0x1e6ac0e2 -> :sswitch_b
        -0x550f31f -> :sswitch_c
        0x48086f0 -> :sswitch_0
        0x5251af2 -> :sswitch_1
        0x10628009 -> :sswitch_9
        0x10fa45f5 -> :sswitch_a
        0x1ad1ac38 -> :sswitch_5
        0x2957779d -> :sswitch_2
        0x2aac7cba -> :sswitch_3
        0x4796efc8 -> :sswitch_7
        0x47971f0d -> :sswitch_11
        0x58e1ba13 -> :sswitch_d
        0x6bf304c4 -> :sswitch_8
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/d/j;
    .locals 1

    .prologue
    .line 3
    const-class v0, Lco/uk/getmondo/d/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/j;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/d/j;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lco/uk/getmondo/d/j;->$VALUES:[Lco/uk/getmondo/d/j;

    invoke-virtual {v0}, [Lco/uk/getmondo/d/j;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/d/j;

    return-object v0
.end method
