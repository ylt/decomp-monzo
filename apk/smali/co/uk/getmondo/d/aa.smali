.class public Lco/uk/getmondo/d/aa;
.super Ljava/lang/Object;
.source "Peer.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lco/uk/getmondo/payments/send/data/a/e;
.implements Lio/realm/at;
.implements Lio/realm/bb;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lco/uk/getmondo/d/aa;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private contactName:Ljava/lang/String;

.field private contactPhoto:Ljava/lang/String;

.field private isEnriched:Z

.field private peerName:Ljava/lang/String;

.field private phoneNumber:Ljava/lang/String;

.field private userId:Ljava/lang/String;

.field private username:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 172
    new-instance v0, Lco/uk/getmondo/d/aa$1;

    invoke-direct {v0}, Lco/uk/getmondo/d/aa$1;-><init>()V

    sput-object v0, Lco/uk/getmondo/d/aa;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/l;

    invoke-interface {p0}, Lio/realm/internal/l;->u_()V

    .line 32
    :cond_0
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->u_()V

    .line 46
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/aa;->c(Ljava/lang/String;)V

    .line 47
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/aa;->d(Ljava/lang/String;)V

    .line 48
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/aa;->e(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/aa;->f(Ljava/lang/String;)V

    .line 50
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/aa;->g(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/aa;->h(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/aa;->b(Z)V

    .line 53
    return-void

    .line 52
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->u_()V

    .line 36
    :cond_0
    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/aa;->c(Ljava/lang/String;)V

    .line 37
    invoke-virtual {p0, p2}, Lco/uk/getmondo/d/aa;->d(Ljava/lang/String;)V

    .line 38
    invoke-virtual {p0, p3}, Lco/uk/getmondo/d/aa;->e(Ljava/lang/String;)V

    .line 39
    invoke-virtual {p0, p4}, Lco/uk/getmondo/d/aa;->f(Ljava/lang/String;)V

    .line 40
    invoke-virtual {p0, p5}, Lco/uk/getmondo/d/aa;->g(Ljava/lang/String;)V

    .line 41
    invoke-virtual {p0, p6}, Lco/uk/getmondo/d/aa;->h(Ljava/lang/String;)V

    .line 42
    invoke-virtual {p0, p7}, Lco/uk/getmondo/d/aa;->b(Z)V

    .line 43
    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/payments/send/a/b;)Lco/uk/getmondo/d/aa;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 56
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/a/b;->b()Ljava/lang/String;

    move-result-object v3

    .line 57
    :goto_0
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/a/b;->e()Ljava/lang/String;

    move-result-object v0

    .line 58
    :cond_0
    const-string v4, "Enriching peer %s(phone: %s) | Contact name: %s photo: %s"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->d()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    const/4 v6, 0x2

    aput-object v3, v5, v6

    const/4 v6, 0x3

    aput-object v0, v5, v6

    invoke-static {v4, v5}, Ld/a/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 60
    invoke-virtual {p0, v3}, Lco/uk/getmondo/d/aa;->a(Ljava/lang/String;)V

    .line 61
    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/aa;->b(Ljava/lang/String;)V

    .line 62
    if-eqz p1, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/aa;->a(Z)V

    .line 63
    return-object p0

    :cond_1
    move-object v3, v0

    .line 56
    goto :goto_0

    :cond_2
    move v0, v2

    .line 62
    goto :goto_1
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 108
    if-nez p1, :cond_1

    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->n()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 115
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 114
    :cond_2
    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/aa;->g(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 153
    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/aa;->b(Z)V

    .line 154
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 73
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->n()Ljava/lang/String;

    move-result-object v0

    .line 81
    :goto_0
    return-object v0

    .line 75
    :cond_0
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->k()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 76
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->k()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 78
    :cond_1
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->l()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 79
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->l()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 81
    :cond_2
    const-string v0, ""

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 123
    if-nez p1, :cond_1

    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->o()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 130
    :cond_0
    :goto_0
    return-void

    .line 126
    :cond_1
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->o()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 129
    :cond_2
    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/aa;->h(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lco/uk/getmondo/d/aa;->isEnriched:Z

    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 85
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->b()Ljava/lang/String;

    move-result-object v0

    .line 86
    invoke-static {v0}, Lco/uk/getmondo/common/k/p;->d(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 87
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->l()Ljava/lang/String;

    move-result-object v0

    .line 89
    :goto_0
    return-object v0

    :cond_0
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/aa;->userId:Ljava/lang/String;

    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/aa;->peerName:Ljava/lang/String;

    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 169
    const/4 v0, 0x0

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/aa;->phoneNumber:Ljava/lang/String;

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 186
    if-ne p0, p1, :cond_1

    move v1, v0

    .line 197
    :cond_0
    :goto_0
    return v1

    .line 187
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    .line 189
    check-cast p1, Lco/uk/getmondo/d/aa;

    .line 191
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->p()Z

    move-result v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aa;->p()Z

    move-result v3

    if-ne v2, v3, :cond_0

    .line 192
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->j()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aa;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 193
    :cond_2
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->k()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aa;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 194
    :cond_3
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->l()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_a

    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aa;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 195
    :cond_4
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->m()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_b

    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aa;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 196
    :cond_5
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->n()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_c

    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aa;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 197
    :cond_6
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->o()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_d

    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/d/aa;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :cond_7
    :goto_1
    move v1, v0

    goto/16 :goto_0

    .line 192
    :cond_8
    invoke-virtual {p1}, Lco/uk/getmondo/d/aa;->j()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    goto/16 :goto_0

    .line 193
    :cond_9
    invoke-virtual {p1}, Lco/uk/getmondo/d/aa;->k()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    goto/16 :goto_0

    .line 194
    :cond_a
    invoke-virtual {p1}, Lco/uk/getmondo/d/aa;->l()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    goto/16 :goto_0

    .line 195
    :cond_b
    invoke-virtual {p1}, Lco/uk/getmondo/d/aa;->m()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    goto/16 :goto_0

    .line 196
    :cond_c
    invoke-virtual {p1}, Lco/uk/getmondo/d/aa;->n()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    goto/16 :goto_0

    .line 197
    :cond_d
    invoke-virtual {p1}, Lco/uk/getmondo/d/aa;->o()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    move v0, v1

    goto :goto_1
.end method

.method public f(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/aa;->username:Ljava/lang/String;

    return-void
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 133
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->o()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public g(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/aa;->contactName:Ljava/lang/String;

    return-void
.end method

.method public h(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/aa;->contactPhoto:Ljava/lang/String;

    return-void
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 141
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 202
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 203
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->k()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 204
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->l()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    .line 205
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    .line 206
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    .line 207
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->o()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v2

    .line 208
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->p()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    add-int/2addr v0, v1

    .line 209
    return v0

    :cond_1
    move v0, v1

    .line 202
    goto :goto_0

    :cond_2
    move v0, v1

    .line 203
    goto :goto_1

    :cond_3
    move v0, v1

    .line 204
    goto :goto_2

    :cond_4
    move v0, v1

    .line 205
    goto :goto_3

    :cond_5
    move v0, v1

    .line 206
    goto :goto_4

    :cond_6
    move v0, v1

    .line 207
    goto :goto_5
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/k/p;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/aa;->userId:Ljava/lang/String;

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/aa;->peerName:Ljava/lang/String;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/aa;->phoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/aa;->username:Ljava/lang/String;

    return-object v0
.end method

.method public n()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/aa;->contactName:Ljava/lang/String;

    return-object v0
.end method

.method public o()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/aa;->contactPhoto:Ljava/lang/String;

    return-object v0
.end method

.method public p()Z
    .locals 1

    iget-boolean v0, p0, Lco/uk/getmondo/d/aa;->isEnriched:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 158
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 159
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 160
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 161
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 162
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 163
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 164
    invoke-virtual {p0}, Lco/uk/getmondo/d/aa;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 165
    return-void

    .line 164
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
