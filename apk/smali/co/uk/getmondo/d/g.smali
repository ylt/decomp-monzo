.class public Lco/uk/getmondo/d/g;
.super Ljava/lang/Object;
.source "Card.java"

# interfaces
.implements Lio/realm/bb;
.implements Lio/realm/k;


# instance fields
.field private accountId:Ljava/lang/String;

.field private cardStatus:Ljava/lang/String;

.field private expires:Ljava/lang/String;

.field private id:Ljava/lang/String;

.field private lastDigits:Ljava/lang/String;

.field private processorToken:Ljava/lang/String;

.field private replacementOrdered:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/l;

    invoke-interface {p0}, Lio/realm/internal/l;->u_()V

    .line 19
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->u_()V

    .line 22
    :cond_0
    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/g;->a(Ljava/lang/String;)V

    .line 23
    invoke-virtual {p0, p2}, Lco/uk/getmondo/d/g;->b(Ljava/lang/String;)V

    .line 24
    invoke-virtual {p0, p3}, Lco/uk/getmondo/d/g;->c(Ljava/lang/String;)V

    .line 25
    invoke-virtual {p0, p4}, Lco/uk/getmondo/d/g;->d(Ljava/lang/String;)V

    .line 26
    invoke-virtual {p0, p5}, Lco/uk/getmondo/d/g;->e(Ljava/lang/String;)V

    .line 27
    invoke-virtual {p0, p6}, Lco/uk/getmondo/d/g;->f(Ljava/lang/String;)V

    .line 28
    invoke-virtual {p0, p7}, Lco/uk/getmondo/d/g;->a(Z)V

    .line 29
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/g;->id:Ljava/lang/String;

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lco/uk/getmondo/d/g;->replacementOrdered:Z

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/g;->accountId:Ljava/lang/String;

    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->r_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/g;->expires:Ljava/lang/String;

    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/g;->lastDigits:Ljava/lang/String;

    return-void
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/g;->processorToken:Ljava/lang/String;

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 64
    if-ne p0, p1, :cond_1

    move v1, v0

    .line 75
    :cond_0
    :goto_0
    return v1

    .line 65
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    .line 67
    check-cast p1, Lco/uk/getmondo/d/g;

    .line 69
    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->o()Z

    move-result v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/g;->o()Z

    move-result v3

    if-ne v2, v3, :cond_0

    .line 70
    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->i()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/g;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 71
    :cond_2
    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->j()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/g;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 72
    :cond_3
    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->r_()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_a

    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->r_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/g;->r_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 73
    :cond_4
    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->l()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_b

    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/g;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 74
    :cond_5
    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->m()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_c

    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/g;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 75
    :cond_6
    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->n()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_d

    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/d/g;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :cond_7
    :goto_1
    move v1, v0

    goto/16 :goto_0

    .line 70
    :cond_8
    invoke-virtual {p1}, Lco/uk/getmondo/d/g;->i()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    goto/16 :goto_0

    .line 71
    :cond_9
    invoke-virtual {p1}, Lco/uk/getmondo/d/g;->j()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    goto/16 :goto_0

    .line 72
    :cond_a
    invoke-virtual {p1}, Lco/uk/getmondo/d/g;->r_()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    goto/16 :goto_0

    .line 73
    :cond_b
    invoke-virtual {p1}, Lco/uk/getmondo/d/g;->l()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    goto/16 :goto_0

    .line 74
    :cond_c
    invoke-virtual {p1}, Lco/uk/getmondo/d/g;->m()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    goto/16 :goto_0

    .line 75
    :cond_d
    invoke-virtual {p1}, Lco/uk/getmondo/d/g;->n()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    move v0, v1

    goto :goto_1
.end method

.method public f(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/g;->cardStatus:Ljava/lang/String;

    return-void
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 52
    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->o()Z

    move-result v0

    return v0
.end method

.method public g()Lco/uk/getmondo/api/model/a;
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->n()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/api/model/a;->valueOf(Ljava/lang/String;)Lco/uk/getmondo/api/model/a;

    move-result-object v0

    return-object v0
.end method

.method public h()Z
    .locals 2

    .prologue
    .line 60
    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->g()Lco/uk/getmondo/api/model/a;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/a;->ACTIVE:Lco/uk/getmondo/api/model/a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 79
    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->i()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 80
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 81
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->r_()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->r_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    .line 82
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->l()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    .line 83
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    .line 84
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v2

    .line 85
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/g;->o()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    add-int/2addr v0, v1

    .line 86
    return v0

    :cond_1
    move v0, v1

    .line 79
    goto :goto_0

    :cond_2
    move v0, v1

    .line 80
    goto :goto_1

    :cond_3
    move v0, v1

    .line 81
    goto :goto_2

    :cond_4
    move v0, v1

    .line 82
    goto :goto_3

    :cond_5
    move v0, v1

    .line 83
    goto :goto_4

    :cond_6
    move v0, v1

    .line 84
    goto :goto_5
.end method

.method public i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/g;->id:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/g;->accountId:Ljava/lang/String;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/g;->lastDigits:Ljava/lang/String;

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/g;->processorToken:Ljava/lang/String;

    return-object v0
.end method

.method public n()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/g;->cardStatus:Ljava/lang/String;

    return-object v0
.end method

.method public o()Z
    .locals 1

    iget-boolean v0, p0, Lco/uk/getmondo/d/g;->replacementOrdered:Z

    return v0
.end method

.method public r_()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/g;->expires:Ljava/lang/String;

    return-object v0
.end method
