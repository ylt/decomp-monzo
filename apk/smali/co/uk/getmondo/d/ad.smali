.class public final Lco/uk/getmondo/d/ad;
.super Ljava/lang/Object;
.source "RetailAccount.kt"

# interfaces
.implements Lco/uk/getmondo/d/a;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u001a\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0008\u0086\u0008\u0018\u00002\u00020\u0001B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u0003\u0012\u0006\u0010\u000c\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\rJ\t\u0010\u001b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001d\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u001e\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\u001f\u001a\u00020\nH\u00c6\u0003J\t\u0010 \u001a\u00020\u0003H\u00c6\u0003J\t\u0010!\u001a\u00020\u0003H\u00c6\u0003JO\u0010\"\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u000c\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010#\u001a\u00020\u00082\u0008\u0010$\u001a\u0004\u0018\u00010%H\u00d6\u0003J\t\u0010&\u001a\u00020\'H\u00d6\u0001J\t\u0010(\u001a\u00020\u0003H\u00d6\u0001J\u0010\u0010)\u001a\u00020\u00002\u0006\u0010*\u001a\u00020\u0008H\u0016R\u0011\u0010\u000c\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0014\u0010\u0007\u001a\u00020\u0008X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0014\u0010\u0004\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u000fR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u000fR\u0011\u0010\u000b\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u000fR\u0011\u0010\u0017\u001a\u00020\u00038F\u00a2\u0006\u0006\u001a\u0004\u0008\u0018\u0010\u000fR\u0014\u0010\t\u001a\u00020\nX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001a\u00a8\u0006+"
    }
    d2 = {
        "Lco/uk/getmondo/model/RetailAccount;",
        "Lco/uk/getmondo/model/Account;",
        "id",
        "",
        "description",
        "created",
        "Lorg/threeten/bp/LocalDateTime;",
        "cardActivated",
        "",
        "type",
        "Lco/uk/getmondo/model/Account$Type;",
        "sortCode",
        "accountNumber",
        "(Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDateTime;ZLco/uk/getmondo/model/Account$Type;Ljava/lang/String;Ljava/lang/String;)V",
        "getAccountNumber",
        "()Ljava/lang/String;",
        "getCardActivated",
        "()Z",
        "getCreated",
        "()Lorg/threeten/bp/LocalDateTime;",
        "getDescription",
        "getId",
        "getSortCode",
        "sortCodeWithDashes",
        "getSortCodeWithDashes",
        "getType",
        "()Lco/uk/getmondo/model/Account$Type;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "withCardActivated",
        "isActivated",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final accountNumber:Ljava/lang/String;

.field private final cardActivated:Z

.field private final created:Lorg/threeten/bp/LocalDateTime;

.field private final description:Ljava/lang/String;

.field private final id:Ljava/lang/String;

.field private final sortCode:Ljava/lang/String;

.field private final type:Lco/uk/getmondo/d/a$b;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDateTime;ZLco/uk/getmondo/d/a$b;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "description"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "created"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sortCode"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountNumber"

    invoke-static {p7, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/d/ad;->id:Ljava/lang/String;

    iput-object p2, p0, Lco/uk/getmondo/d/ad;->description:Ljava/lang/String;

    iput-object p3, p0, Lco/uk/getmondo/d/ad;->created:Lorg/threeten/bp/LocalDateTime;

    iput-boolean p4, p0, Lco/uk/getmondo/d/ad;->cardActivated:Z

    iput-object p5, p0, Lco/uk/getmondo/d/ad;->type:Lco/uk/getmondo/d/a$b;

    iput-object p6, p0, Lco/uk/getmondo/d/ad;->sortCode:Ljava/lang/String;

    iput-object p7, p0, Lco/uk/getmondo/d/ad;->accountNumber:Ljava/lang/String;

    return-void
.end method

.method public static synthetic a(Lco/uk/getmondo/d/ad;Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDateTime;ZLco/uk/getmondo/d/a$b;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lco/uk/getmondo/d/ad;
    .locals 8

    and-int/lit8 v0, p8, 0x1

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lco/uk/getmondo/d/ad;->a()Ljava/lang/String;

    move-result-object v1

    :goto_0
    and-int/lit8 v0, p8, 0x2

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lco/uk/getmondo/d/ad;->h()Ljava/lang/String;

    move-result-object v2

    :goto_1
    and-int/lit8 v0, p8, 0x4

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lco/uk/getmondo/d/ad;->b()Lorg/threeten/bp/LocalDateTime;

    move-result-object v3

    :goto_2
    and-int/lit8 v0, p8, 0x8

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lco/uk/getmondo/d/ad;->c()Z

    move-result v4

    :goto_3
    and-int/lit8 v0, p8, 0x10

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lco/uk/getmondo/d/ad;->d()Lco/uk/getmondo/d/a$b;

    move-result-object v5

    :goto_4
    and-int/lit8 v0, p8, 0x20

    if-eqz v0, :cond_1

    iget-object v6, p0, Lco/uk/getmondo/d/ad;->sortCode:Ljava/lang/String;

    :goto_5
    and-int/lit8 v0, p8, 0x40

    if-eqz v0, :cond_0

    iget-object v7, p0, Lco/uk/getmondo/d/ad;->accountNumber:Ljava/lang/String;

    :goto_6
    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Lco/uk/getmondo/d/ad;->a(Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDateTime;ZLco/uk/getmondo/d/a$b;Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/d/ad;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v7, p7

    goto :goto_6

    :cond_1
    move-object v6, p6

    goto :goto_5

    :cond_2
    move-object v5, p5

    goto :goto_4

    :cond_3
    move v4, p4

    goto :goto_3

    :cond_4
    move-object v3, p3

    goto :goto_2

    :cond_5
    move-object v2, p2

    goto :goto_1

    :cond_6
    move-object v1, p1

    goto :goto_0
.end method


# virtual methods
.method public synthetic a(Z)Lco/uk/getmondo/d/a;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/ad;->b(Z)Lco/uk/getmondo/d/ad;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/a;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDateTime;ZLco/uk/getmondo/d/a$b;Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/d/ad;
    .locals 8

    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "description"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "created"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sortCode"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountNumber"

    invoke-static {p7, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lco/uk/getmondo/d/ad;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lco/uk/getmondo/d/ad;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDateTime;ZLco/uk/getmondo/d/a$b;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lco/uk/getmondo/d/ad;->id:Ljava/lang/String;

    return-object v0
.end method

.method public b(Z)Lco/uk/getmondo/d/ad;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 15
    const/16 v8, 0x77

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move v4, p1

    move-object v5, v1

    move-object v6, v1

    move-object v7, v1

    move-object v9, v1

    invoke-static/range {v0 .. v9}, Lco/uk/getmondo/d/ad;->a(Lco/uk/getmondo/d/ad;Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDateTime;ZLco/uk/getmondo/d/a$b;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lco/uk/getmondo/d/ad;

    move-result-object v0

    return-object v0
.end method

.method public b()Lorg/threeten/bp/LocalDateTime;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lco/uk/getmondo/d/ad;->created:Lorg/threeten/bp/LocalDateTime;

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 9
    iget-boolean v0, p0, Lco/uk/getmondo/d/ad;->cardActivated:Z

    return v0
.end method

.method public d()Lco/uk/getmondo/d/a$b;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lco/uk/getmondo/d/ad;->type:Lco/uk/getmondo/d/a$b;

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 6
    invoke-static {p0}, Lco/uk/getmondo/d/a$a;->a(Lco/uk/getmondo/d/a;)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eq p0, p1, :cond_0

    instance-of v2, p1, Lco/uk/getmondo/d/ad;

    if-eqz v2, :cond_1

    check-cast p1, Lco/uk/getmondo/d/ad;

    invoke-virtual {p0}, Lco/uk/getmondo/d/ad;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/ad;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lco/uk/getmondo/d/ad;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/ad;->h()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lco/uk/getmondo/d/ad;->b()Lorg/threeten/bp/LocalDateTime;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/ad;->b()Lorg/threeten/bp/LocalDateTime;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lco/uk/getmondo/d/ad;->c()Z

    move-result v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/ad;->c()Z

    move-result v3

    if-ne v2, v3, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lco/uk/getmondo/d/ad;->d()Lco/uk/getmondo/d/a$b;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/ad;->d()Lco/uk/getmondo/d/a$b;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/d/ad;->sortCode:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/d/ad;->sortCode:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lco/uk/getmondo/d/ad;->accountNumber:Ljava/lang/String;

    iget-object v3, p1, Lco/uk/getmondo/d/ad;->accountNumber:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 6
    invoke-static {p0}, Lco/uk/getmondo/d/a$a;->b(Lco/uk/getmondo/d/a;)Z

    move-result v0

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lco/uk/getmondo/d/ad;->sortCode:Ljava/lang/String;

    invoke-static {v0}, Lco/uk/getmondo/common/k/o;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lco/uk/getmondo/d/ad;->description:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lco/uk/getmondo/d/ad;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/ad;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/ad;->b()Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/ad;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/ad;->d()Lco/uk/getmondo/d/a$b;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/d/ad;->sortCode:Ljava/lang/String;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/uk/getmondo/d/ad;->accountNumber:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_3

    :cond_6
    move v0, v1

    goto :goto_4
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lco/uk/getmondo/d/ad;->sortCode:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lco/uk/getmondo/d/ad;->accountNumber:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RetailAccount(id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/d/ad;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/d/ad;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", created="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/d/ad;->b()Lorg/threeten/bp/LocalDateTime;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", cardActivated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/d/ad;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/d/ad;->d()Lco/uk/getmondo/d/a$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sortCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/ad;->sortCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", accountNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/d/ad;->accountNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
