.class public Lco/uk/getmondo/d/t;
.super Ljava/lang/Object;
.source "LocalSpend.java"

# interfaces
.implements Lio/realm/af;
.implements Lio/realm/bb;


# instance fields
.field private amountValue:J

.field private currency:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/l;

    invoke-interface {p0}, Lio/realm/internal/l;->u_()V

    .line 13
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->u_()V

    .line 16
    :cond_0
    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/t;->a(Ljava/lang/String;)V

    .line 17
    invoke-virtual {p0, p2, p3}, Lco/uk/getmondo/d/t;->a(J)V

    .line 18
    return-void
.end method


# virtual methods
.method public a()Lco/uk/getmondo/d/c;
    .locals 4

    .prologue
    .line 21
    new-instance v0, Lco/uk/getmondo/d/c;

    invoke-virtual {p0}, Lco/uk/getmondo/d/t;->c()J

    move-result-wide v2

    invoke-virtual {p0}, Lco/uk/getmondo/d/t;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Lco/uk/getmondo/d/c;-><init>(JLjava/lang/String;)V

    return-object v0
.end method

.method public a(J)V
    .locals 1

    iput-wide p1, p0, Lco/uk/getmondo/d/t;->amountValue:J

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/t;->currency:Ljava/lang/String;

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/t;->currency:Ljava/lang/String;

    return-object v0
.end method

.method public c()J
    .locals 2

    iget-wide v0, p0, Lco/uk/getmondo/d/t;->amountValue:J

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 26
    if-ne p0, p1, :cond_1

    .line 32
    :cond_0
    :goto_0
    return v0

    .line 27
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 29
    :cond_3
    check-cast p1, Lco/uk/getmondo/d/t;

    .line 31
    invoke-virtual {p0}, Lco/uk/getmondo/d/t;->c()J

    move-result-wide v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/t;->c()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    .line 32
    :cond_4
    invoke-virtual {p0}, Lco/uk/getmondo/d/t;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Lco/uk/getmondo/d/t;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/d/t;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_5
    invoke-virtual {p1}, Lco/uk/getmondo/d/t;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 37
    invoke-virtual {p0}, Lco/uk/getmondo/d/t;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/d/t;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 38
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/t;->c()J

    move-result-wide v2

    invoke-virtual {p0}, Lco/uk/getmondo/d/t;->c()J

    move-result-wide v4

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 39
    return v0

    .line 37
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
