.class public final enum Lco/uk/getmondo/d/a$b;
.super Ljava/lang/Enum;
.source "Account.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/d/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/d/a$b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/d/a$b;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0007\u0008\u0086\u0001\u0018\u0000 \t2\u0008\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\tB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008\u00a8\u0006\n"
    }
    d2 = {
        "Lco/uk/getmondo/model/Account$Type;",
        "",
        "value",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "getValue",
        "()Ljava/lang/String;",
        "PREPAID",
        "RETAIL",
        "Find",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lco/uk/getmondo/d/a$b;

.field public static final Find:Lco/uk/getmondo/d/a$b$a;

.field public static final enum PREPAID:Lco/uk/getmondo/d/a$b;

.field public static final enum RETAIL:Lco/uk/getmondo/d/a$b;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x2

    new-array v0, v0, [Lco/uk/getmondo/d/a$b;

    new-instance v1, Lco/uk/getmondo/d/a$b;

    const-string v2, "PREPAID"

    .line 8
    const-string v3, "uk_prepaid"

    invoke-direct {v1, v2, v4, v3}, Lco/uk/getmondo/d/a$b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/d/a$b;->PREPAID:Lco/uk/getmondo/d/a$b;

    aput-object v1, v0, v4

    new-instance v1, Lco/uk/getmondo/d/a$b;

    const-string v2, "RETAIL"

    .line 9
    const-string v3, "uk_retail"

    invoke-direct {v1, v2, v5, v3}, Lco/uk/getmondo/d/a$b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/d/a$b;->RETAIL:Lco/uk/getmondo/d/a$b;

    aput-object v1, v0, v5

    sput-object v0, Lco/uk/getmondo/d/a$b;->$VALUES:[Lco/uk/getmondo/d/a$b;

    new-instance v0, Lco/uk/getmondo/d/a$b$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/d/a$b$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/d/a$b;->Find:Lco/uk/getmondo/d/a$b$a;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "value"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lco/uk/getmondo/d/a$b;->value:Ljava/lang/String;

    return-void
.end method

.method public static final a(Ljava/lang/String;)Lco/uk/getmondo/d/a$b;
    .locals 1

    sget-object v0, Lco/uk/getmondo/d/a$b;->Find:Lco/uk/getmondo/d/a$b$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/d/a$b$a;->a(Ljava/lang/String;)Lco/uk/getmondo/d/a$b;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/d/a$b;
    .locals 1

    const-class v0, Lco/uk/getmondo/d/a$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/a$b;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/d/a$b;
    .locals 1

    sget-object v0, Lco/uk/getmondo/d/a$b;->$VALUES:[Lco/uk/getmondo/d/a$b;

    invoke-virtual {v0}, [Lco/uk/getmondo/d/a$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/d/a$b;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lco/uk/getmondo/d/a$b;->value:Ljava/lang/String;

    return-object v0
.end method
