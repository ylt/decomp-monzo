.class public final Lco/uk/getmondo/d/i$a;
.super Ljava/lang/Object;
.source "Country.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/d/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00050\tH\u0002J\u0014\u0010\n\u001a\u0004\u0018\u00010\u00052\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u0007J\u0008\u0010\r\u001a\u00020\u0005H\u0007J\u000e\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00050\tH\u0007R\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lco/uk/getmondo/model/Country$Companion;",
        "",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "Lco/uk/getmondo/model/Country;",
        "UNITED_KINGDOM",
        "UNITED_STATES",
        "allCountries",
        "",
        "fromCode",
        "code",
        "",
        "getDefaultCountry",
        "prioritisedCountries",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lco/uk/getmondo/d/i$a;-><init>()V

    return-void
.end method

.method private final c()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/d/i;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    const/16 v0, 0xf9

    new-array v0, v0, [Lco/uk/getmondo/d/i;

    .line 42
    const/4 v1, 0x0

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Afghanistan"

    const-string v4, "Afghanistan"

    const-string v5, "AF"

    const-string v6, "AFG"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 43
    const/4 v1, 0x1

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "\u00c5land Islands"

    const-string v4, "\u00c5land Islands"

    const-string v5, "AX"

    const-string v6, "ALA"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 44
    const/4 v1, 0x2

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Albania"

    const-string v4, "Albania"

    const-string v5, "AL"

    const-string v6, "ALB"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 45
    const/4 v1, 0x3

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Algeria"

    const-string v4, "Algeria"

    const-string v5, "DZ"

    const-string v6, "DZA"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 46
    const/4 v1, 0x4

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "American Samoa"

    const-string v4, "American Samoa"

    const-string v5, "AS"

    const-string v6, "ASM"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 47
    const/4 v1, 0x5

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Andorra"

    const-string v4, "Andorra"

    const-string v5, "AD"

    const-string v6, "AND"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 48
    const/4 v1, 0x6

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Angola"

    const-string v4, "Angola"

    const-string v5, "AO"

    const-string v6, "AGO"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 49
    const/4 v1, 0x7

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Anguilla"

    const-string v4, "Anguilla"

    const-string v5, "AI"

    const-string v6, "AIA"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 50
    const/16 v1, 0x8

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Antarctica"

    const-string v4, "Antarctica"

    const-string v5, "AQ"

    const-string v6, "ATA"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 51
    const/16 v1, 0x9

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Antigua and Barbuda"

    const-string v4, "Antigua and Barbuda"

    const-string v5, "AG"

    const-string v6, "ATG"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 52
    const/16 v1, 0xa

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Argentina"

    const-string v4, "Argentina"

    const-string v5, "AR"

    const-string v6, "ARG"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 53
    const/16 v1, 0xb

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Armenia"

    const-string v4, "Armenia"

    const-string v5, "AM"

    const-string v6, "ARM"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 54
    const/16 v1, 0xc

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Aruba"

    const-string v4, "Aruba"

    const-string v5, "AW"

    const-string v6, "ABW"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 55
    const/16 v1, 0xd

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Australia"

    const-string v4, "Australia"

    const-string v5, "AU"

    const-string v6, "AUS"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 56
    const/16 v1, 0xe

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Austria"

    const-string v4, "Austria"

    const-string v5, "AT"

    const-string v6, "AUT"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 57
    const/16 v1, 0xf

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Azerbaijan"

    const-string v4, "Azerbaijan"

    const-string v5, "AZ"

    const-string v6, "AZE"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 58
    const/16 v1, 0x10

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Bahamas"

    const-string v4, "the Bahamas"

    const-string v5, "BS"

    const-string v6, "BHS"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 59
    const/16 v1, 0x11

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Bahrain"

    const-string v4, "Bahrain"

    const-string v5, "BH"

    const-string v6, "BHR"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 60
    const/16 v1, 0x12

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Bangladesh"

    const-string v4, "Bangladesh"

    const-string v5, "BD"

    const-string v6, "BGD"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 61
    const/16 v1, 0x13

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Barbados"

    const-string v4, "Barbados"

    const-string v5, "BB"

    const-string v6, "BRB"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 62
    const/16 v1, 0x14

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Belarus"

    const-string v4, "Belarus"

    const-string v5, "BY"

    const-string v6, "BLR"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 63
    const/16 v1, 0x15

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Belgium"

    const-string v4, "Belgium"

    const-string v5, "BE"

    const-string v6, "BEL"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 64
    const/16 v1, 0x16

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Belize"

    const-string v4, "Belize"

    const-string v5, "BZ"

    const-string v6, "BLZ"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 65
    const/16 v1, 0x17

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Benin"

    const-string v4, "Benin"

    const-string v5, "BJ"

    const-string v6, "BEN"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 66
    const/16 v1, 0x18

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Bermuda"

    const-string v4, "Bermuda"

    const-string v5, "BM"

    const-string v6, "BMU"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 67
    const/16 v1, 0x19

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Bhutan"

    const-string v4, "Bhutan"

    const-string v5, "BT"

    const-string v6, "BTN"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 68
    const/16 v1, 0x1a

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Bolivia (Plurinational State of)"

    const-string v4, "Bolivia"

    const-string v5, "BO"

    const-string v6, "BOL"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 69
    const/16 v1, 0x1b

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Bonaire"

    const-string v4, "Bonaire"

    const-string v5, "BQ"

    const-string v6, "BES"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 70
    const/16 v1, 0x1c

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Bosnia and Herzegovina"

    const-string v4, "Bosnia and Herzegovina"

    const-string v5, "BA"

    const-string v6, "BIH"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 71
    const/16 v1, 0x1d

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Botswana"

    const-string v4, "Botswana"

    const-string v5, "BW"

    const-string v6, "BWA"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 72
    const/16 v1, 0x1e

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Bouvet Island"

    const-string v4, "Bouvet Island"

    const-string v5, "BV"

    const-string v6, "BVT"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 73
    const/16 v1, 0x1f

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Brazil"

    const-string v4, "Brazil"

    const-string v5, "BR"

    const-string v6, "BRA"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 74
    const/16 v1, 0x20

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "British Indian Ocean Territory"

    const-string v4, "British Indian Ocean Territory"

    const-string v5, "IO"

    const-string v6, "IOT"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 75
    const/16 v1, 0x21

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Brunei Darussalam"

    const-string v4, "Brunei Darussalam"

    const-string v5, "BN"

    const-string v6, "BRN"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 76
    const/16 v1, 0x22

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Bulgaria"

    const-string v4, "Bulgaria"

    const-string v5, "BG"

    const-string v6, "BGR"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 77
    const/16 v1, 0x23

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Burkina Faso"

    const-string v4, "Burkina Faso"

    const-string v5, "BF"

    const-string v6, "BFA"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 78
    const/16 v1, 0x24

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Burundi"

    const-string v4, "Burundi"

    const-string v5, "BI"

    const-string v6, "BDI"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 79
    const/16 v1, 0x25

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Cambodia"

    const-string v4, "Cambodia"

    const-string v5, "KH"

    const-string v6, "KHM"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 80
    const/16 v1, 0x26

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Cameroon"

    const-string v4, "Cameroon"

    const-string v5, "CM"

    const-string v6, "CMR"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 81
    const/16 v1, 0x27

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Canada"

    const-string v4, "Canada"

    const-string v5, "CA"

    const-string v6, "CAN"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 82
    const/16 v1, 0x28

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Cabo Verde"

    const-string v4, "Cabo Verde"

    const-string v5, "CV"

    const-string v6, "CPV"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 83
    const/16 v1, 0x29

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Cayman Islands"

    const-string v4, "the Cayman Islands"

    const-string v5, "KY"

    const-string v6, "CYM"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 84
    const/16 v1, 0x2a

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Central African Republic"

    const-string v4, "the Central African Republic"

    const-string v5, "CF"

    const-string v6, "CAF"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 85
    const/16 v1, 0x2b

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Chad"

    const-string v4, "Chad"

    const-string v5, "TD"

    const-string v6, "TCD"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 86
    const/16 v1, 0x2c

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Chile"

    const-string v4, "Chile"

    const-string v5, "CL"

    const-string v6, "CHL"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 87
    const/16 v1, 0x2d

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "China"

    const-string v4, "China"

    const-string v5, "CN"

    const-string v6, "CHN"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 88
    const/16 v1, 0x2e

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Christmas Island"

    const-string v4, "Christmas Island"

    const-string v5, "CX"

    const-string v6, "CXR"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 89
    const/16 v1, 0x2f

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Cocos (Keeling) Islands"

    const-string v4, "the Cocos (Keeling) Islands"

    const-string v5, "CC"

    const-string v6, "CCK"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 90
    const/16 v1, 0x30

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Colombia"

    const-string v4, "Colombia"

    const-string v5, "CO"

    const-string v6, "COL"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 91
    const/16 v1, 0x31

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Comoros"

    const-string v4, "the Comoros"

    const-string v5, "KM"

    const-string v6, "COM"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 92
    const/16 v1, 0x32

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Congo"

    const-string v4, "Congo"

    const-string v5, "CG"

    const-string v6, "COG"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 93
    const/16 v1, 0x33

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Congo (Democratic Republic of the)"

    const-string v4, "the Democratic Republic of the Congo"

    const-string v5, "CD"

    const-string v6, "COD"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 94
    const/16 v1, 0x34

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Cook Islands"

    const-string v4, "the Cook Islands"

    const-string v5, "CK"

    const-string v6, "COK"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 95
    const/16 v1, 0x35

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Costa Rica"

    const-string v4, "Costa Rica"

    const-string v5, "CR"

    const-string v6, "CRI"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 96
    const/16 v1, 0x36

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "C\u00f4te d\'Ivoire"

    const-string v4, "C\u00f4te d\'Ivoire"

    const-string v5, "CI"

    const-string v6, "CIV"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 97
    const/16 v1, 0x37

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Croatia"

    const-string v4, "Croatia"

    const-string v5, "HR"

    const-string v6, "HRV"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 98
    const/16 v1, 0x38

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Cuba"

    const-string v4, "Cuba"

    const-string v5, "CU"

    const-string v6, "CUB"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 99
    const/16 v1, 0x39

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Cura\u00e7ao"

    const-string v4, "Cura\u00e7ao"

    const-string v5, "CW"

    const-string v6, "CUW"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 100
    const/16 v1, 0x3a

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Cyprus"

    const-string v4, "Cyprus"

    const-string v5, "CY"

    const-string v6, "CYP"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 101
    const/16 v1, 0x3b

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Czech Republic"

    const-string v4, "the Czech Republic"

    const-string v5, "CZ"

    const-string v6, "CZE"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 102
    const/16 v1, 0x3c

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Denmark"

    const-string v4, "Denmark"

    const-string v5, "DK"

    const-string v6, "DNK"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 103
    const/16 v1, 0x3d

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Djibouti"

    const-string v4, "Djibouti"

    const-string v5, "DJ"

    const-string v6, "DJI"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 104
    const/16 v1, 0x3e

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Dominica"

    const-string v4, "Dominica"

    const-string v5, "DM"

    const-string v6, "DMA"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 105
    const/16 v1, 0x3f

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Dominican Republic"

    const-string v4, "the Dominican Republic"

    const-string v5, "DO"

    const-string v6, "DOM"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 106
    const/16 v1, 0x40

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Ecuador"

    const-string v4, "Ecuador"

    const-string v5, "EC"

    const-string v6, "ECU"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 107
    const/16 v1, 0x41

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Egypt"

    const-string v4, "Egypt"

    const-string v5, "EG"

    const-string v6, "EGY"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 108
    const/16 v1, 0x42

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "El Salvador"

    const-string v4, "El Salvador"

    const-string v5, "SV"

    const-string v6, "SLV"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 109
    const/16 v1, 0x43

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Equatorial Guinea"

    const-string v4, "Equatorial Guinea"

    const-string v5, "GQ"

    const-string v6, "GNQ"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 110
    const/16 v1, 0x44

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Eritrea"

    const-string v4, "Eritrea"

    const-string v5, "ER"

    const-string v6, "ERI"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 111
    const/16 v1, 0x45

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Estonia"

    const-string v4, "Estonia"

    const-string v5, "EE"

    const-string v6, "EST"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 112
    const/16 v1, 0x46

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Ethiopia"

    const-string v4, "Ethiopia"

    const-string v5, "ET"

    const-string v6, "ETH"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 113
    const/16 v1, 0x47

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Falkland Islands (Malvinas)"

    const-string v4, "the Falkland Islands"

    const-string v5, "FK"

    const-string v6, "FLK"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 114
    const/16 v1, 0x48

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Faroe Islands"

    const-string v4, "Faroe Islands"

    const-string v5, "FO"

    const-string v6, "FRO"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 115
    const/16 v1, 0x49

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Fiji"

    const-string v4, "Fiji"

    const-string v5, "FJ"

    const-string v6, "FJI"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 116
    const/16 v1, 0x4a

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Finland"

    const-string v4, "Finland"

    const-string v5, "FI"

    const-string v6, "FIN"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 117
    const/16 v1, 0x4b

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "France"

    const-string v4, "France"

    const-string v5, "FR"

    const-string v6, "FRA"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 118
    const/16 v1, 0x4c

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "French Guiana"

    const-string v4, "French Guiana"

    const-string v5, "GF"

    const-string v6, "GUF"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 119
    const/16 v1, 0x4d

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "French Polynesia"

    const-string v4, "French Polynesia"

    const-string v5, "PF"

    const-string v6, "PYF"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 120
    const/16 v1, 0x4e

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "French Southern Territories"

    const-string v4, "the French Southern Territories"

    const-string v5, "TF"

    const-string v6, "ATF"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 121
    const/16 v1, 0x4f

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Gabon"

    const-string v4, "Gabon"

    const-string v5, "GA"

    const-string v6, "GAB"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 122
    const/16 v1, 0x50

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Gambia"

    const-string v4, "the Gambia"

    const-string v5, "GM"

    const-string v6, "GMB"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 123
    const/16 v1, 0x51

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Georgia"

    const-string v4, "Georgia"

    const-string v5, "GE"

    const-string v6, "GEO"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 124
    const/16 v1, 0x52

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Germany"

    const-string v4, "Germany"

    const-string v5, "DE"

    const-string v6, "DEU"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 125
    const/16 v1, 0x53

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Ghana"

    const-string v4, "Ghana"

    const-string v5, "GH"

    const-string v6, "GHA"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 126
    const/16 v1, 0x54

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Gibraltar"

    const-string v4, "Gibraltar"

    const-string v5, "GI"

    const-string v6, "GIB"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 127
    const/16 v1, 0x55

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Greece"

    const-string v4, "Greece"

    const-string v5, "GR"

    const-string v6, "GRC"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 128
    const/16 v1, 0x56

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Greenland"

    const-string v4, "Greenland"

    const-string v5, "GL"

    const-string v6, "GRL"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 129
    const/16 v1, 0x57

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Grenada"

    const-string v4, "Grenada"

    const-string v5, "GD"

    const-string v6, "GRD"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 130
    const/16 v1, 0x58

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Guadeloupe"

    const-string v4, "Guadeloupe"

    const-string v5, "GP"

    const-string v6, "GLP"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 131
    const/16 v1, 0x59

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Guam"

    const-string v4, "Guam"

    const-string v5, "GU"

    const-string v6, "GUM"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 132
    const/16 v1, 0x5a

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Guatemala"

    const-string v4, "Guatemala"

    const-string v5, "GT"

    const-string v6, "GTM"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 133
    const/16 v1, 0x5b

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Guernsey"

    const-string v4, "Guernsey"

    const-string v5, "GG"

    const-string v6, "GGY"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 134
    const/16 v1, 0x5c

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Guinea"

    const-string v4, "Guinea"

    const-string v5, "GN"

    const-string v6, "GIN"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 135
    const/16 v1, 0x5d

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Guinea-Bissau"

    const-string v4, "Guinea-Bissau"

    const-string v5, "GW"

    const-string v6, "GNB"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 136
    const/16 v1, 0x5e

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Guyana"

    const-string v4, "Guyana"

    const-string v5, "GY"

    const-string v6, "GUY"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 137
    const/16 v1, 0x5f

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Haiti"

    const-string v4, "Haiti"

    const-string v5, "HT"

    const-string v6, "HTI"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 138
    const/16 v1, 0x60

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Heard Island and McDonald Islands"

    const-string v4, "Heard Island and McDonald Islands"

    const-string v5, "HM"

    const-string v6, "HMD"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 139
    const/16 v1, 0x61

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Holy See"

    const-string v4, "Holy See"

    const-string v5, "VA"

    const-string v6, "VAT"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 140
    const/16 v1, 0x62

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Honduras"

    const-string v4, "Honduras"

    const-string v5, "HN"

    const-string v6, "HND"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 141
    const/16 v1, 0x63

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Hong Kong"

    const-string v4, "Hong Kong"

    const-string v5, "HK"

    const-string v6, "HKG"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 142
    const/16 v1, 0x64

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Hungary"

    const-string v4, "Hungary"

    const-string v5, "HU"

    const-string v6, "HUN"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 143
    const/16 v1, 0x65

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Iceland"

    const-string v4, "Iceland"

    const-string v5, "IS"

    const-string v6, "ISL"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 144
    const/16 v1, 0x66

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "India"

    const-string v4, "India"

    const-string v5, "IN"

    const-string v6, "IND"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 145
    const/16 v1, 0x67

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Indonesia"

    const-string v4, "Indonesia"

    const-string v5, "ID"

    const-string v6, "IDN"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 146
    const/16 v1, 0x68

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Iran (Islamic Republic of)"

    const-string v4, "Iran"

    const-string v5, "IR"

    const-string v6, "IRN"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 147
    const/16 v1, 0x69

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Iraq"

    const-string v4, "Iraq"

    const-string v5, "IQ"

    const-string v6, "IRQ"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 148
    const/16 v1, 0x6a

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Ireland"

    const-string v4, "Ireland"

    const-string v5, "IE"

    const-string v6, "IRL"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 149
    const/16 v1, 0x6b

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Isle of Man"

    const-string v4, "the Isle of Man"

    const-string v5, "IM"

    const-string v6, "IMN"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 150
    const/16 v1, 0x6c

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Israel"

    const-string v4, "Israel"

    const-string v5, "IL"

    const-string v6, "ISR"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 151
    const/16 v1, 0x6d

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Italy"

    const-string v4, "Italy"

    const-string v5, "IT"

    const-string v6, "ITA"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 152
    const/16 v1, 0x6e

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Jamaica"

    const-string v4, "Jamaica"

    const-string v5, "JM"

    const-string v6, "JAM"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 153
    const/16 v1, 0x6f

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Japan"

    const-string v4, "Japan"

    const-string v5, "JP"

    const-string v6, "JPN"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 154
    const/16 v1, 0x70

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Jersey"

    const-string v4, "Jersey"

    const-string v5, "JE"

    const-string v6, "JEY"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 155
    const/16 v1, 0x71

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Jordan"

    const-string v4, "Jordan"

    const-string v5, "JO"

    const-string v6, "JOR"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 156
    const/16 v1, 0x72

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Kazakhstan"

    const-string v4, "Kazakhstan"

    const-string v5, "KZ"

    const-string v6, "KAZ"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 157
    const/16 v1, 0x73

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Kenya"

    const-string v4, "Kenya"

    const-string v5, "KE"

    const-string v6, "KEN"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 158
    const/16 v1, 0x74

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Kiribati"

    const-string v4, "Kiribati"

    const-string v5, "KI"

    const-string v6, "KIR"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 159
    const/16 v1, 0x75

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Korea (Democratic People\'s Republic of)"

    const-string v4, "North Korea"

    const-string v5, "KP"

    const-string v6, "PRK"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 160
    const/16 v1, 0x76

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Korea (Republic of)"

    const-string v4, "South Korea"

    const-string v5, "KR"

    const-string v6, "KOR"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 161
    const/16 v1, 0x77

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Kuwait"

    const-string v4, "Kuwait"

    const-string v5, "KW"

    const-string v6, "KWT"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 162
    const/16 v1, 0x78

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Kyrgyzstan"

    const-string v4, "Kyrgyzstan"

    const-string v5, "KG"

    const-string v6, "KGZ"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 163
    const/16 v1, 0x79

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Lao People\'s Democratic Republic"

    const-string v4, "Laos"

    const-string v5, "LA"

    const-string v6, "LAO"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 164
    const/16 v1, 0x7a

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Latvia"

    const-string v4, "Latvia"

    const-string v5, "LV"

    const-string v6, "LVA"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 165
    const/16 v1, 0x7b

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Lebanon"

    const-string v4, "Lebanon"

    const-string v5, "LB"

    const-string v6, "LBN"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 166
    const/16 v1, 0x7c

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Lesotho"

    const-string v4, "Lesotho"

    const-string v5, "LS"

    const-string v6, "LSO"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 167
    const/16 v1, 0x7d

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Liberia"

    const-string v4, "Liberia"

    const-string v5, "LR"

    const-string v6, "LBR"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 168
    const/16 v1, 0x7e

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Libya"

    const-string v4, "Libya"

    const-string v5, "LY"

    const-string v6, "LBY"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 169
    const/16 v1, 0x7f

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Liechtenstein"

    const-string v4, "Liechtenstein"

    const-string v5, "LI"

    const-string v6, "LIE"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 170
    const/16 v1, 0x80

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Lithuania"

    const-string v4, "Lithuania"

    const-string v5, "LT"

    const-string v6, "LTU"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 171
    const/16 v1, 0x81

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Luxembourg"

    const-string v4, "Luxembourg"

    const-string v5, "LU"

    const-string v6, "LUX"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 172
    const/16 v1, 0x82

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Macao"

    const-string v4, "Macao"

    const-string v5, "MO"

    const-string v6, "MAC"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 173
    const/16 v1, 0x83

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Macedonia (the former Yugoslav Republic of)"

    const-string v4, "Macedonia"

    const-string v5, "MK"

    const-string v6, "MKD"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 174
    const/16 v1, 0x84

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Madagascar"

    const-string v4, "Madagascar"

    const-string v5, "MG"

    const-string v6, "MDG"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 175
    const/16 v1, 0x85

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Malawi"

    const-string v4, "Malawi"

    const-string v5, "MW"

    const-string v6, "MWI"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 176
    const/16 v1, 0x86

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Malaysia"

    const-string v4, "Malaysia"

    const-string v5, "MY"

    const-string v6, "MYS"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 177
    const/16 v1, 0x87

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Maldives"

    const-string v4, "the Maldives"

    const-string v5, "MV"

    const-string v6, "MDV"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 178
    const/16 v1, 0x88

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Mali"

    const-string v4, "Mali"

    const-string v5, "ML"

    const-string v6, "MLI"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 179
    const/16 v1, 0x89

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Malta"

    const-string v4, "Malta"

    const-string v5, "MT"

    const-string v6, "MLT"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 180
    const/16 v1, 0x8a

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Marshall Islands"

    const-string v4, "the Marshall Islands"

    const-string v5, "MH"

    const-string v6, "MHL"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 181
    const/16 v1, 0x8b

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Martinique"

    const-string v4, "Martinique"

    const-string v5, "MQ"

    const-string v6, "MTQ"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 182
    const/16 v1, 0x8c

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Mauritania"

    const-string v4, "Mauritania"

    const-string v5, "MR"

    const-string v6, "MRT"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 183
    const/16 v1, 0x8d

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Mauritius"

    const-string v4, "Mauritius"

    const-string v5, "MU"

    const-string v6, "MUS"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 184
    const/16 v1, 0x8e

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Mayotte"

    const-string v4, "Mayotte"

    const-string v5, "YT"

    const-string v6, "MYT"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 185
    const/16 v1, 0x8f

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Mexico"

    const-string v4, "Mexico"

    const-string v5, "MX"

    const-string v6, "MEX"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 186
    const/16 v1, 0x90

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Micronesia (Federated States of)"

    const-string v4, "Micronesia"

    const-string v5, "FM"

    const-string v6, "FSM"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 187
    const/16 v1, 0x91

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Moldova (Republic of)"

    const-string v4, "Moldova"

    const-string v5, "MD"

    const-string v6, "MDA"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 188
    const/16 v1, 0x92

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Monaco"

    const-string v4, "Monaco"

    const-string v5, "MC"

    const-string v6, "MCO"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 189
    const/16 v1, 0x93

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Mongolia"

    const-string v4, "Mongolia"

    const-string v5, "MN"

    const-string v6, "MNG"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 190
    const/16 v1, 0x94

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Montenegro"

    const-string v4, "Montenegro"

    const-string v5, "ME"

    const-string v6, "MNE"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 191
    const/16 v1, 0x95

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Montserrat"

    const-string v4, "Montserrat"

    const-string v5, "MS"

    const-string v6, "MSR"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 192
    const/16 v1, 0x96

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Morocco"

    const-string v4, "Morocco"

    const-string v5, "MA"

    const-string v6, "MAR"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 193
    const/16 v1, 0x97

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Mozambique"

    const-string v4, "Mozambique"

    const-string v5, "MZ"

    const-string v6, "MOZ"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 194
    const/16 v1, 0x98

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Myanmar"

    const-string v4, "Myanmar"

    const-string v5, "MM"

    const-string v6, "MMR"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 195
    const/16 v1, 0x99

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Namibia"

    const-string v4, "Namibia"

    const-string v5, "NA"

    const-string v6, "NAM"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 196
    const/16 v1, 0x9a

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Nauru"

    const-string v4, "Nauru"

    const-string v5, "NR"

    const-string v6, "NRU"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 197
    const/16 v1, 0x9b

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Nepal"

    const-string v4, "Nepal"

    const-string v5, "NP"

    const-string v6, "NPL"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 198
    const/16 v1, 0x9c

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Netherlands"

    const-string v4, "the Netherlands"

    const-string v5, "NL"

    const-string v6, "NLD"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 199
    const/16 v1, 0x9d

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "New Caledonia"

    const-string v4, "New Caledonia"

    const-string v5, "NC"

    const-string v6, "NCL"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 200
    const/16 v1, 0x9e

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "New Zealand"

    const-string v4, "New Zealand"

    const-string v5, "NZ"

    const-string v6, "NZL"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 201
    const/16 v1, 0x9f

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Nicaragua"

    const-string v4, "Nicaragua"

    const-string v5, "NI"

    const-string v6, "NIC"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 202
    const/16 v1, 0xa0

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Niger"

    const-string v4, "Niger"

    const-string v5, "NE"

    const-string v6, "NER"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 203
    const/16 v1, 0xa1

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Nigeria"

    const-string v4, "Nigeria"

    const-string v5, "NG"

    const-string v6, "NGA"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 204
    const/16 v1, 0xa2

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Niue"

    const-string v4, "Niue"

    const-string v5, "NU"

    const-string v6, "NIU"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 205
    const/16 v1, 0xa3

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Norfolk Island"

    const-string v4, "Norfolk Island"

    const-string v5, "NF"

    const-string v6, "NFK"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 206
    const/16 v1, 0xa4

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Northern Mariana Islands"

    const-string v4, "Northern Mariana Islands"

    const-string v5, "MP"

    const-string v6, "MNP"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 207
    const/16 v1, 0xa5

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Norway"

    const-string v4, "Norway"

    const-string v5, "NO"

    const-string v6, "NOR"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 208
    const/16 v1, 0xa6

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Oman"

    const-string v4, "Oman"

    const-string v5, "OM"

    const-string v6, "OMN"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 209
    const/16 v1, 0xa7

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Pakistan"

    const-string v4, "Pakistan"

    const-string v5, "PK"

    const-string v6, "PAK"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 210
    const/16 v1, 0xa8

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Palau"

    const-string v4, "Palau"

    const-string v5, "PW"

    const-string v6, "PLW"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 211
    const/16 v1, 0xa9

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Palestine"

    const-string v4, "Palestine"

    const-string v5, "PS"

    const-string v6, "PSE"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 212
    const/16 v1, 0xaa

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Panama"

    const-string v4, "Panama"

    const-string v5, "PA"

    const-string v6, "PAN"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 213
    const/16 v1, 0xab

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Papua New Guinea"

    const-string v4, "Papua New Guinea"

    const-string v5, "PG"

    const-string v6, "PNG"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 214
    const/16 v1, 0xac

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Paraguay"

    const-string v4, "Paraguay"

    const-string v5, "PY"

    const-string v6, "PRY"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 215
    const/16 v1, 0xad

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Peru"

    const-string v4, "Peru"

    const-string v5, "PE"

    const-string v6, "PER"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 216
    const/16 v1, 0xae

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Philippines"

    const-string v4, "the Philippines"

    const-string v5, "PH"

    const-string v6, "PHL"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 217
    const/16 v1, 0xaf

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Pitcairn"

    const-string v4, "Pitcairn"

    const-string v5, "PN"

    const-string v6, "PCN"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 218
    const/16 v1, 0xb0

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Poland"

    const-string v4, "Poland"

    const-string v5, "PL"

    const-string v6, "POL"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 219
    const/16 v1, 0xb1

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Portugal"

    const-string v4, "Portugal"

    const-string v5, "PT"

    const-string v6, "PRT"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 220
    const/16 v1, 0xb2

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Puerto Rico"

    const-string v4, "Puerto Rico"

    const-string v5, "PR"

    const-string v6, "PRI"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 221
    const/16 v1, 0xb3

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Qatar"

    const-string v4, "Qatar"

    const-string v5, "QA"

    const-string v6, "QAT"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 222
    const/16 v1, 0xb4

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "R\u00e9union"

    const-string v4, "R\u00e9union"

    const-string v5, "RE"

    const-string v6, "REU"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 223
    const/16 v1, 0xb5

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Romania"

    const-string v4, "Romania"

    const-string v5, "RO"

    const-string v6, "ROU"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 224
    const/16 v1, 0xb6

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Russian Federation"

    const-string v4, "Russia"

    const-string v5, "RU"

    const-string v6, "RUS"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 225
    const/16 v1, 0xb7

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Rwanda"

    const-string v4, "Rwanda"

    const-string v5, "RW"

    const-string v6, "RWA"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 226
    const/16 v1, 0xb8

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Saint Barth\u00e9lemy"

    const-string v4, "Saint Barth\u00e9lemy"

    const-string v5, "BL"

    const-string v6, "BLM"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 227
    const/16 v1, 0xb9

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Saint Helena"

    const-string v4, "Saint Helena, Ascension and Tristan da Cunha"

    const-string v5, "SH"

    const-string v6, "SHN"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 228
    const/16 v1, 0xba

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Saint Kitts and Nevis"

    const-string v4, "Saint Kitts and Nevis"

    const-string v5, "KN"

    const-string v6, "KNA"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 229
    const/16 v1, 0xbb

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Saint Lucia"

    const-string v4, "Saint Lucia"

    const-string v5, "LC"

    const-string v6, "LCA"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 230
    const/16 v1, 0xbc

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Saint Martin (French part)"

    const-string v4, "Saint Martin (French part)"

    const-string v5, "MF"

    const-string v6, "MAF"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 231
    const/16 v1, 0xbd

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Saint Pierre and Miquelon"

    const-string v4, "Saint Pierre and Miquelon"

    const-string v5, "PM"

    const-string v6, "SPM"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 232
    const/16 v1, 0xbe

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Saint Vincent and the Grenadines"

    const-string v4, "Saint Vincent and the Grenadines"

    const-string v5, "VC"

    const-string v6, "VCT"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 233
    const/16 v1, 0xbf

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Samoa"

    const-string v4, "Samoa"

    const-string v5, "WS"

    const-string v6, "WSM"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 234
    const/16 v1, 0xc0

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "San Marino"

    const-string v4, "San Marino"

    const-string v5, "SM"

    const-string v6, "SMR"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 235
    const/16 v1, 0xc1

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Sao Tome and Principe"

    const-string v4, "Sao Tome and Principe"

    const-string v5, "ST"

    const-string v6, "STP"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 236
    const/16 v1, 0xc2

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Saudi Arabia"

    const-string v4, "Saudi Arabia"

    const-string v5, "SA"

    const-string v6, "SAU"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 237
    const/16 v1, 0xc3

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Senegal"

    const-string v4, "Senegal"

    const-string v5, "SN"

    const-string v6, "SEN"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 238
    const/16 v1, 0xc4

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Serbia"

    const-string v4, "Serbia"

    const-string v5, "RS"

    const-string v6, "SRB"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 239
    const/16 v1, 0xc5

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Seychelles"

    const-string v4, "Seychelles"

    const-string v5, "SC"

    const-string v6, "SYC"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 240
    const/16 v1, 0xc6

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Sierra Leone"

    const-string v4, "Sierra Leone"

    const-string v5, "SL"

    const-string v6, "SLE"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 241
    const/16 v1, 0xc7

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Singapore"

    const-string v4, "Singapore"

    const-string v5, "SG"

    const-string v6, "SGP"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 242
    const/16 v1, 0xc8

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Sint Maarten (Dutch part)"

    const-string v4, "Sint Maarten (Dutch part)"

    const-string v5, "SX"

    const-string v6, "SXM"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 243
    const/16 v1, 0xc9

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Slovakia"

    const-string v4, "Slovakia"

    const-string v5, "SK"

    const-string v6, "SVK"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 244
    const/16 v1, 0xca

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Slovenia"

    const-string v4, "Slovenia"

    const-string v5, "SI"

    const-string v6, "SVN"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 245
    const/16 v1, 0xcb

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Solomon Islands"

    const-string v4, "the Solomon Islands"

    const-string v5, "SB"

    const-string v6, "SLB"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 246
    const/16 v1, 0xcc

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Somalia"

    const-string v4, "Somalia"

    const-string v5, "SO"

    const-string v6, "SOM"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 247
    const/16 v1, 0xcd

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "South Africa"

    const-string v4, "South Africa"

    const-string v5, "ZA"

    const-string v6, "ZAF"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 248
    const/16 v1, 0xce

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "South Georgia and the South Sandwich Islands"

    const-string v4, "South Georgia and the South Sandwich Islands"

    const-string v5, "GS"

    const-string v6, "SGS"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 249
    const/16 v1, 0xcf

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "South Sudan"

    const-string v4, "South Sudan"

    const-string v5, "SS"

    const-string v6, "SSD"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 250
    const/16 v1, 0xd0

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Spain"

    const-string v4, "Spain"

    const-string v5, "ES"

    const-string v6, "ESP"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 251
    const/16 v1, 0xd1

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Sri Lanka"

    const-string v4, "Sri Lanka"

    const-string v5, "LK"

    const-string v6, "LKA"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 252
    const/16 v1, 0xd2

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Sudan"

    const-string v4, "Sudan"

    const-string v5, "SD"

    const-string v6, "SDN"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 253
    const/16 v1, 0xd3

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Suriname"

    const-string v4, "SuricommonName"

    const-string v5, "SR"

    const-string v6, "SUR"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 254
    const/16 v1, 0xd4

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Svalbard and Jan Mayen"

    const-string v4, "Svalbard and Jan Mayen"

    const-string v5, "SJ"

    const-string v6, "SJM"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 255
    const/16 v1, 0xd5

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Swaziland"

    const-string v4, "Swaziland"

    const-string v5, "SZ"

    const-string v6, "SWZ"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 256
    const/16 v1, 0xd6

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Sweden"

    const-string v4, "Sweden"

    const-string v5, "SE"

    const-string v6, "SWE"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 257
    const/16 v1, 0xd7

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Switzerland"

    const-string v4, "Switzerland"

    const-string v5, "CH"

    const-string v6, "CHE"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 258
    const/16 v1, 0xd8

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Syrian Arab Republic"

    const-string v4, "Syria"

    const-string v5, "SY"

    const-string v6, "SYR"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 259
    const/16 v1, 0xd9

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Taiwan"

    const-string v4, "Taiwan"

    const-string v5, "TW"

    const-string v6, "TWN"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 260
    const/16 v1, 0xda

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Tajikistan"

    const-string v4, "Tajikistan"

    const-string v5, "TJ"

    const-string v6, "TJK"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 261
    const/16 v1, 0xdb

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Tanzania"

    const-string v4, "Tanzania"

    const-string v5, "TZ"

    const-string v6, "TZA"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 262
    const/16 v1, 0xdc

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Thailand"

    const-string v4, "Thailand"

    const-string v5, "TH"

    const-string v6, "THA"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 263
    const/16 v1, 0xdd

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Timor-Leste"

    const-string v4, "Timor-Leste"

    const-string v5, "TL"

    const-string v6, "TLS"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 264
    const/16 v1, 0xde

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Togo"

    const-string v4, "Togo"

    const-string v5, "TG"

    const-string v6, "TGO"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 265
    const/16 v1, 0xdf

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Tokelau"

    const-string v4, "Tokelau"

    const-string v5, "TK"

    const-string v6, "TKL"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 266
    const/16 v1, 0xe0

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Tonga"

    const-string v4, "Tonga"

    const-string v5, "TO"

    const-string v6, "TON"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 267
    const/16 v1, 0xe1

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Trinidad and Tobago"

    const-string v4, "Trinidad and Tobago"

    const-string v5, "TT"

    const-string v6, "TTO"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 268
    const/16 v1, 0xe2

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Tunisia"

    const-string v4, "Tunisia"

    const-string v5, "TN"

    const-string v6, "TUN"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 269
    const/16 v1, 0xe3

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Turkey"

    const-string v4, "Turkey"

    const-string v5, "TR"

    const-string v6, "TUR"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 270
    const/16 v1, 0xe4

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Turkmenistan"

    const-string v4, "Turkmenistan"

    const-string v5, "TM"

    const-string v6, "TKM"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 271
    const/16 v1, 0xe5

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Turks and Caicos Islands"

    const-string v4, "the Turks and Caicos Islands"

    const-string v5, "TC"

    const-string v6, "TCA"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 272
    const/16 v1, 0xe6

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Tuvalu"

    const-string v4, "Tuvalu"

    const-string v5, "TV"

    const-string v6, "TUV"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 273
    const/16 v1, 0xe7

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Uganda"

    const-string v4, "Uganda"

    const-string v5, "UG"

    const-string v6, "UGA"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 274
    const/16 v1, 0xe8

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Ukraine"

    const-string v4, "Ukraine"

    const-string v5, "UA"

    const-string v6, "UKR"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 275
    const/16 v1, 0xe9

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "United Arab Emirates"

    const-string v4, "the UAE"

    const-string v5, "AE"

    const-string v6, "ARE"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 276
    const/16 v1, 0xea

    sget-object v2, Lco/uk/getmondo/d/i;->UNITED_KINGDOM:Lco/uk/getmondo/d/i;

    aput-object v2, v0, v1

    .line 277
    const/16 v1, 0xeb

    sget-object v2, Lco/uk/getmondo/d/i;->UNITED_STATES:Lco/uk/getmondo/d/i;

    aput-object v2, v0, v1

    .line 278
    const/16 v1, 0xec

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "United States Minor Outlying Islands"

    const-string v4, "the United States Minor Outlying Islands"

    const-string v5, "UM"

    const-string v6, "UMI"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 279
    const/16 v1, 0xed

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Uruguay"

    const-string v4, "Uruguay"

    const-string v5, "UY"

    const-string v6, "URY"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 280
    const/16 v1, 0xee

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Uzbekistan"

    const-string v4, "Uzbekistan"

    const-string v5, "UZ"

    const-string v6, "UZB"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 281
    const/16 v1, 0xef

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Vanuatu"

    const-string v4, "Vanuatu"

    const-string v5, "VU"

    const-string v6, "VUT"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 282
    const/16 v1, 0xf0

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Venezuela (Bolivarian Republic of)"

    const-string v4, "Venezuela"

    const-string v5, "VE"

    const-string v6, "VEN"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 283
    const/16 v1, 0xf1

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Viet Nam"

    const-string v4, "Vietnam"

    const-string v5, "VN"

    const-string v6, "VNM"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 284
    const/16 v1, 0xf2

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Virgin Islands (British)"

    const-string v4, "the Virgin Islands"

    const-string v5, "VG"

    const-string v6, "VGB"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 285
    const/16 v1, 0xf3

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Virgin Islands (U.S.)"

    const-string v4, "the U.S. Virgin Islands"

    const-string v5, "VI"

    const-string v6, "VIR"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 286
    const/16 v1, 0xf4

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Wallis and Futuna"

    const-string v4, "Wallis and Futuna"

    const-string v5, "WF"

    const-string v6, "WLF"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 287
    const/16 v1, 0xf5

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Western Sahara"

    const-string v4, "Western Sahara"

    const-string v5, "EH"

    const-string v6, "ESH"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 288
    const/16 v1, 0xf6

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Yemen"

    const-string v4, "Yemen"

    const-string v5, "YE"

    const-string v6, "YEM"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 289
    const/16 v1, 0xf7

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Zambia"

    const-string v4, "Zambia"

    const-string v5, "ZM"

    const-string v6, "ZMB"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 290
    const/16 v1, 0xf8

    new-instance v2, Lco/uk/getmondo/d/i;

    const-string v3, "Zimbabwe"

    const-string v4, "Zimbabwe"

    const-string v5, "ZW"

    const-string v6, "ZWE"

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/d/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 41
    invoke-static {v0}, Lkotlin/a/m;->b([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lco/uk/getmondo/d/i;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lco/uk/getmondo/d/i;->UNITED_KINGDOM:Lco/uk/getmondo/d/i;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lco/uk/getmondo/d/i;
    .locals 4

    .prologue
    .line 23
    check-cast p0, Lco/uk/getmondo/d/i$a;

    invoke-direct {p0}, Lco/uk/getmondo/d/i$a;->c()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lco/uk/getmondo/d/i;

    invoke-virtual {v0}, Lco/uk/getmondo/d/i;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v0}, Lco/uk/getmondo/d/i;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    move-object v0, v1

    :goto_1
    check-cast v0, Lco/uk/getmondo/d/i;

    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/d/i;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 29
    sget-object v1, Lco/uk/getmondo/d/i;->UNITED_KINGDOM:Lco/uk/getmondo/d/i;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 30
    sget-object v1, Lco/uk/getmondo/d/i;->UNITED_STATES:Lco/uk/getmondo/d/i;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 31
    check-cast p0, Lco/uk/getmondo/d/i$a;

    invoke-direct {p0}, Lco/uk/getmondo/d/i$a;->c()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 316
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/d/i;

    .line 32
    invoke-virtual {v1}, Lco/uk/getmondo/d/i;->e()Ljava/lang/String;

    move-result-object v3

    const-string v4, "GBR"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lco/uk/getmondo/d/i;->e()Ljava/lang/String;

    move-result-object v3

    const-string v4, "USA"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    .line 33
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 35
    :cond_0
    nop

    goto :goto_0

    .line 317
    :cond_1
    nop

    .line 37
    return-object v0
.end method
