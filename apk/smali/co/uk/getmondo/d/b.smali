.class public Lco/uk/getmondo/d/b;
.super Ljava/lang/Object;
.source "AccountBalance.java"

# interfaces
.implements Lio/realm/b;
.implements Lio/realm/bb;


# instance fields
.field private accountId:Ljava/lang/String;

.field private balanceAmountValue:J

.field private balanceCurrency:Ljava/lang/String;

.field private localSpend:Lio/realm/az;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/az",
            "<",
            "Lco/uk/getmondo/d/t;",
            ">;"
        }
    .end annotation
.end field

.field private spentTodayAmountValue:J

.field private spentTodayCurrency:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/l;

    invoke-interface {p0}, Lio/realm/internal/l;->u_()V

    .line 20
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JLjava/lang/String;JLjava/lang/String;Lio/realm/az;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Lio/realm/az",
            "<",
            "Lco/uk/getmondo/d/t;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->u_()V

    .line 24
    :cond_0
    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/b;->a(Ljava/lang/String;)V

    .line 25
    invoke-virtual {p0, p2, p3}, Lco/uk/getmondo/d/b;->a(J)V

    .line 26
    invoke-virtual {p0, p4}, Lco/uk/getmondo/d/b;->b(Ljava/lang/String;)V

    .line 27
    invoke-virtual {p0, p5, p6}, Lco/uk/getmondo/d/b;->b(J)V

    .line 28
    invoke-virtual {p0, p7}, Lco/uk/getmondo/d/b;->c(Ljava/lang/String;)V

    .line 29
    invoke-virtual {p0, p8}, Lco/uk/getmondo/d/b;->a(Lio/realm/az;)V

    .line 30
    return-void
.end method


# virtual methods
.method public a()Lco/uk/getmondo/d/c;
    .locals 4

    .prologue
    .line 33
    new-instance v0, Lco/uk/getmondo/d/c;

    invoke-virtual {p0}, Lco/uk/getmondo/d/b;->f()J

    move-result-wide v2

    invoke-virtual {p0}, Lco/uk/getmondo/d/b;->g()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Lco/uk/getmondo/d/c;-><init>(JLjava/lang/String;)V

    return-object v0
.end method

.method public a(J)V
    .locals 1

    iput-wide p1, p0, Lco/uk/getmondo/d/b;->balanceAmountValue:J

    return-void
.end method

.method public a(Lio/realm/az;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/b;->localSpend:Lio/realm/az;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/b;->accountId:Ljava/lang/String;

    return-void
.end method

.method public b()Lco/uk/getmondo/d/c;
    .locals 4

    .prologue
    .line 37
    new-instance v0, Lco/uk/getmondo/d/c;

    invoke-virtual {p0}, Lco/uk/getmondo/d/b;->h()J

    move-result-wide v2

    invoke-virtual {p0}, Lco/uk/getmondo/d/b;->i()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Lco/uk/getmondo/d/c;-><init>(JLjava/lang/String;)V

    return-object v0
.end method

.method public b(J)V
    .locals 1

    iput-wide p1, p0, Lco/uk/getmondo/d/b;->spentTodayAmountValue:J

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/b;->balanceCurrency:Ljava/lang/String;

    return-void
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/d/t;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    invoke-virtual {p0}, Lco/uk/getmondo/d/b;->j()Lio/realm/az;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/b;->spentTodayCurrency:Ljava/lang/String;

    return-void
.end method

.method public d()Z
    .locals 3

    .prologue
    .line 45
    invoke-virtual {p0}, Lco/uk/getmondo/d/b;->j()Lio/realm/az;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/az;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/t;

    .line 46
    invoke-virtual {v0}, Lco/uk/getmondo/d/t;->a()Lco/uk/getmondo/d/c;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/c;->l()Lco/uk/getmondo/common/i/c;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/common/i/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/d/b;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 47
    const/4 v0, 0x1

    .line 51
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/b;->accountId:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 56
    if-ne p0, p1, :cond_1

    move v1, v0

    .line 67
    :cond_0
    :goto_0
    return v1

    .line 57
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    .line 59
    check-cast p1, Lco/uk/getmondo/d/b;

    .line 61
    invoke-virtual {p0}, Lco/uk/getmondo/d/b;->f()J

    move-result-wide v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/b;->f()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 62
    invoke-virtual {p0}, Lco/uk/getmondo/d/b;->h()J

    move-result-wide v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/b;->h()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 63
    invoke-virtual {p0}, Lco/uk/getmondo/d/b;->e()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-virtual {p0}, Lco/uk/getmondo/d/b;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/b;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 64
    :cond_2
    invoke-virtual {p0}, Lco/uk/getmondo/d/b;->g()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {p0}, Lco/uk/getmondo/d/b;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/b;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 65
    :cond_3
    invoke-virtual {p0}, Lco/uk/getmondo/d/b;->i()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-virtual {p0}, Lco/uk/getmondo/d/b;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/b;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 67
    :cond_4
    invoke-virtual {p0}, Lco/uk/getmondo/d/b;->j()Lio/realm/az;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-virtual {p0}, Lco/uk/getmondo/d/b;->j()Lio/realm/az;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/d/b;->j()Lio/realm/az;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/realm/az;->equals(Ljava/lang/Object;)Z

    move-result v0

    :cond_5
    :goto_1
    move v1, v0

    goto :goto_0

    .line 63
    :cond_6
    invoke-virtual {p1}, Lco/uk/getmondo/d/b;->e()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    goto :goto_0

    .line 64
    :cond_7
    invoke-virtual {p1}, Lco/uk/getmondo/d/b;->g()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    goto/16 :goto_0

    .line 65
    :cond_8
    invoke-virtual {p1}, Lco/uk/getmondo/d/b;->i()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    goto/16 :goto_0

    .line 67
    :cond_9
    invoke-virtual {p1}, Lco/uk/getmondo/d/b;->j()Lio/realm/az;

    move-result-object v2

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_1
.end method

.method public f()J
    .locals 2

    iget-wide v0, p0, Lco/uk/getmondo/d/b;->balanceAmountValue:J

    return-wide v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/b;->balanceCurrency:Ljava/lang/String;

    return-object v0
.end method

.method public h()J
    .locals 2

    iget-wide v0, p0, Lco/uk/getmondo/d/b;->spentTodayAmountValue:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/4 v1, 0x0

    .line 72
    invoke-virtual {p0}, Lco/uk/getmondo/d/b;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lco/uk/getmondo/d/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 73
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/b;->f()J

    move-result-wide v2

    invoke-virtual {p0}, Lco/uk/getmondo/d/b;->f()J

    move-result-wide v4

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 74
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/b;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lco/uk/getmondo/d/b;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 75
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/b;->h()J

    move-result-wide v2

    invoke-virtual {p0}, Lco/uk/getmondo/d/b;->h()J

    move-result-wide v4

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 76
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/b;->i()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lco/uk/getmondo/d/b;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    .line 77
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/b;->j()Lio/realm/az;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/d/b;->j()Lio/realm/az;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/az;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 78
    return v0

    :cond_1
    move v0, v1

    .line 72
    goto :goto_0

    :cond_2
    move v0, v1

    .line 74
    goto :goto_1

    :cond_3
    move v0, v1

    .line 76
    goto :goto_2
.end method

.method public i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/b;->spentTodayCurrency:Ljava/lang/String;

    return-object v0
.end method

.method public j()Lio/realm/az;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/b;->localSpend:Lio/realm/az;

    return-object v0
.end method
