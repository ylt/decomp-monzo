.class public Lco/uk/getmondo/d/aj;
.super Ljava/lang/Object;
.source "Transaction.java"

# interfaces
.implements Lio/realm/bb;
.implements Lio/realm/bn;


# instance fields
.field private amountCurrency:Ljava/lang/String;

.field private amountLocalCurrency:Ljava/lang/String;

.field private amountLocalValue:J

.field private amountValue:J

.field private attachments:Lio/realm/az;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/az",
            "<",
            "Lco/uk/getmondo/d/d;",
            ">;"
        }
    .end annotation
.end field

.field private bacsDirectDebitInstructionId:Ljava/lang/String;

.field private bankDetails:Lco/uk/getmondo/payments/send/data/a/a;

.field private category:Ljava/lang/String;

.field private created:Ljava/util/Date;

.field private createdDateFormatted:Ljava/lang/String;

.field private declineReason:Ljava/lang/String;

.field private description:Ljava/lang/String;

.field private fromAtm:Z

.field private fromMonzoMe:Z

.field private hideAmount:Z

.field private id:Ljava/lang/String;

.field private includeInSpending:Z

.field private merchant:Lco/uk/getmondo/d/u;

.field private merchantDescription:Ljava/lang/String;

.field private notes:Ljava/lang/String;

.field private peer:Lco/uk/getmondo/d/aa;

.field private peerToPeer:Z

.field private scheme:Ljava/lang/String;

.field private settled:Z

.field private topUp:Z

.field private updated:Ljava/util/Date;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/l;

    invoke-interface {p0}, Lio/realm/internal/l;->u_()V

    .line 47
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JLjava/lang/String;JLjava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLco/uk/getmondo/d/u;Ljava/lang/String;ZZZZLio/realm/az;Lco/uk/getmondo/d/aa;ZLco/uk/getmondo/payments/send/data/a/a;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZZ",
            "Lco/uk/getmondo/d/u;",
            "Ljava/lang/String;",
            "ZZZZ",
            "Lio/realm/az",
            "<",
            "Lco/uk/getmondo/d/d;",
            ">;",
            "Lco/uk/getmondo/d/aa;",
            "Z",
            "Lco/uk/getmondo/payments/send/data/a/a;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    instance-of v1, p0, Lio/realm/internal/l;

    if-eqz v1, :cond_0

    move-object v1, p0

    check-cast v1, Lio/realm/internal/l;

    invoke-interface {v1}, Lio/realm/internal/l;->u_()V

    .line 54
    :cond_0
    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/aj;->c(Ljava/lang/String;)V

    .line 55
    invoke-virtual {p0, p2, p3}, Lco/uk/getmondo/d/aj;->a(J)V

    .line 56
    invoke-virtual {p0, p4}, Lco/uk/getmondo/d/aj;->d(Ljava/lang/String;)V

    .line 57
    invoke-virtual {p0, p5, p6}, Lco/uk/getmondo/d/aj;->b(J)V

    .line 58
    invoke-virtual {p0, p7}, Lco/uk/getmondo/d/aj;->e(Ljava/lang/String;)V

    .line 59
    invoke-virtual {p0, p8}, Lco/uk/getmondo/d/aj;->a(Ljava/util/Date;)V

    .line 60
    invoke-virtual {p0, p9}, Lco/uk/getmondo/d/aj;->f(Ljava/lang/String;)V

    .line 61
    invoke-virtual {p0, p10}, Lco/uk/getmondo/d/aj;->b(Ljava/util/Date;)V

    .line 62
    invoke-virtual {p0, p11}, Lco/uk/getmondo/d/aj;->g(Ljava/lang/String;)V

    .line 63
    invoke-virtual {p0, p12}, Lco/uk/getmondo/d/aj;->h(Ljava/lang/String;)V

    .line 64
    invoke-virtual {p0, p13}, Lco/uk/getmondo/d/aj;->i(Ljava/lang/String;)V

    .line 65
    move-object/from16 v0, p14

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/aj;->j(Ljava/lang/String;)V

    .line 66
    move/from16 v0, p15

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/aj;->a(Z)V

    .line 67
    move/from16 v0, p16

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/aj;->b(Z)V

    .line 68
    move-object/from16 v0, p17

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/aj;->a(Lco/uk/getmondo/d/u;)V

    .line 69
    move-object/from16 v0, p18

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/aj;->k(Ljava/lang/String;)V

    .line 70
    move/from16 v0, p19

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/aj;->c(Z)V

    .line 71
    move/from16 v0, p20

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/aj;->d(Z)V

    .line 72
    move/from16 v0, p21

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/aj;->e(Z)V

    .line 73
    move/from16 v0, p22

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/aj;->f(Z)V

    .line 74
    move-object/from16 v0, p23

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/aj;->a(Lio/realm/az;)V

    .line 75
    move-object/from16 v0, p24

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/aj;->a(Lco/uk/getmondo/d/aa;)V

    .line 76
    move/from16 v0, p25

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/aj;->g(Z)V

    .line 77
    move-object/from16 v0, p26

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/aj;->a(Lco/uk/getmondo/payments/send/data/a/a;)V

    .line 78
    move-object/from16 v0, p27

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/aj;->l(Ljava/lang/String;)V

    .line 79
    move-object/from16 v0, p28

    invoke-virtual {p0, v0}, Lco/uk/getmondo/d/aj;->m(Ljava/lang/String;)V

    .line 80
    return-void
.end method


# virtual methods
.method public A()Lio/realm/az;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/az",
            "<",
            "Lco/uk/getmondo/d/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 214
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->ab()Lio/realm/az;

    move-result-object v0

    return-object v0
.end method

.method public B()Lco/uk/getmondo/d/aa;
    .locals 1

    .prologue
    .line 218
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->ac()Lco/uk/getmondo/d/aa;

    move-result-object v0

    return-object v0
.end method

.method public C()Lco/uk/getmondo/payments/send/data/a/a;
    .locals 1

    .prologue
    .line 223
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->ad()Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v0

    return-object v0
.end method

.method public D()Lco/uk/getmondo/d/af;
    .locals 1

    .prologue
    .line 227
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->af()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/d/af;->a(Ljava/lang/String;)Lco/uk/getmondo/d/af;

    move-result-object v0

    return-object v0
.end method

.method public E()Z
    .locals 1

    .prologue
    .line 231
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->ae()Z

    move-result v0

    return v0
.end method

.method public F()Ljava/lang/String;
    .locals 1

    .prologue
    .line 244
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->ab()Lio/realm/az;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->ab()Lio/realm/az;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/az;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->ab()Lio/realm/az;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/az;->b()Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/d;

    invoke-virtual {v0}, Lco/uk/getmondo/d/d;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public G()Ljava/lang/String;
    .locals 1

    .prologue
    .line 248
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->ag()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public H()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/aj;->id:Ljava/lang/String;

    return-object v0
.end method

.method public I()J
    .locals 2

    iget-wide v0, p0, Lco/uk/getmondo/d/aj;->amountValue:J

    return-wide v0
.end method

.method public J()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/aj;->amountCurrency:Ljava/lang/String;

    return-object v0
.end method

.method public K()J
    .locals 2

    iget-wide v0, p0, Lco/uk/getmondo/d/aj;->amountLocalValue:J

    return-wide v0
.end method

.method public L()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/aj;->amountLocalCurrency:Ljava/lang/String;

    return-object v0
.end method

.method public M()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/aj;->created:Ljava/util/Date;

    return-object v0
.end method

.method public N()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/aj;->createdDateFormatted:Ljava/lang/String;

    return-object v0
.end method

.method public O()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/aj;->updated:Ljava/util/Date;

    return-object v0
.end method

.method public P()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/aj;->merchantDescription:Ljava/lang/String;

    return-object v0
.end method

.method public Q()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/aj;->description:Ljava/lang/String;

    return-object v0
.end method

.method public R()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/aj;->declineReason:Ljava/lang/String;

    return-object v0
.end method

.method public S()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/aj;->notes:Ljava/lang/String;

    return-object v0
.end method

.method public T()Z
    .locals 1

    iget-boolean v0, p0, Lco/uk/getmondo/d/aj;->hideAmount:Z

    return v0
.end method

.method public U()Z
    .locals 1

    iget-boolean v0, p0, Lco/uk/getmondo/d/aj;->settled:Z

    return v0
.end method

.method public V()Lco/uk/getmondo/d/u;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/aj;->merchant:Lco/uk/getmondo/d/u;

    return-object v0
.end method

.method public W()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/aj;->category:Ljava/lang/String;

    return-object v0
.end method

.method public X()Z
    .locals 1

    iget-boolean v0, p0, Lco/uk/getmondo/d/aj;->fromAtm:Z

    return v0
.end method

.method public Y()Z
    .locals 1

    iget-boolean v0, p0, Lco/uk/getmondo/d/aj;->peerToPeer:Z

    return v0
.end method

.method public Z()Z
    .locals 1

    iget-boolean v0, p0, Lco/uk/getmondo/d/aj;->topUp:Z

    return v0
.end method

.method public a(J)V
    .locals 1

    iput-wide p1, p0, Lco/uk/getmondo/d/aj;->amountValue:J

    return-void
.end method

.method public a(Lco/uk/getmondo/d/aa;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/aj;->peer:Lco/uk/getmondo/d/aa;

    return-void
.end method

.method public a(Lco/uk/getmondo/d/u;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/aj;->merchant:Lco/uk/getmondo/d/u;

    return-void
.end method

.method public a(Lco/uk/getmondo/payments/send/data/a/a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/aj;->bankDetails:Lco/uk/getmondo/payments/send/data/a/a;

    return-void
.end method

.method public a(Lio/realm/az;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/aj;->attachments:Lio/realm/az;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 235
    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/aj;->k(Ljava/lang/String;)V

    .line 236
    return-void
.end method

.method public a(Ljava/util/Date;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/aj;->created:Ljava/util/Date;

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lco/uk/getmondo/d/aj;->hideAmount:Z

    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 83
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->V()Lco/uk/getmondo/d/u;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->V()Lco/uk/getmondo/d/u;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/u;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aa()Z
    .locals 1

    iget-boolean v0, p0, Lco/uk/getmondo/d/aj;->includeInSpending:Z

    return v0
.end method

.method public ab()Lio/realm/az;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/aj;->attachments:Lio/realm/az;

    return-object v0
.end method

.method public ac()Lco/uk/getmondo/d/aa;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/aj;->peer:Lco/uk/getmondo/d/aa;

    return-object v0
.end method

.method public ad()Lco/uk/getmondo/payments/send/data/a/a;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/aj;->bankDetails:Lco/uk/getmondo/payments/send/data/a/a;

    return-object v0
.end method

.method public ae()Z
    .locals 1

    iget-boolean v0, p0, Lco/uk/getmondo/d/aj;->fromMonzoMe:Z

    return v0
.end method

.method public af()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/aj;->scheme:Ljava/lang/String;

    return-object v0
.end method

.method public ag()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/d/aj;->bacsDirectDebitInstructionId:Ljava/lang/String;

    return-object v0
.end method

.method public b(J)V
    .locals 1

    iput-wide p1, p0, Lco/uk/getmondo/d/aj;->amountLocalValue:J

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 239
    invoke-virtual {p0, p1}, Lco/uk/getmondo/d/aj;->j(Ljava/lang/String;)V

    .line 240
    return-void
.end method

.method public b(Ljava/util/Date;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/aj;->updated:Ljava/util/Date;

    return-void
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lco/uk/getmondo/d/aj;->settled:Z

    return-void
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 87
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->V()Lco/uk/getmondo/d/u;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->c()Lco/uk/getmondo/d/h;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/d/h;->EATING_OUT:Lco/uk/getmondo/d/h;

    if-ne v0, v1, :cond_0

    const-string v0, "USD"

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->L()Ljava/lang/String;

    move-result-object v1

    .line 89
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 87
    :goto_0
    return v0

    .line 89
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Lco/uk/getmondo/d/h;
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->W()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/d/h;->a(Ljava/lang/String;)Lco/uk/getmondo/d/h;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/aj;->id:Ljava/lang/String;

    return-void
.end method

.method public c(Z)V
    .locals 0

    iput-boolean p1, p0, Lco/uk/getmondo/d/aj;->fromAtm:Z

    return-void
.end method

.method public d()Lcom/c/b/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/c/b/b",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->R()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/c/b/b;->b(Ljava/lang/Object;)Lcom/c/b/b;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/aj;->amountCurrency:Ljava/lang/String;

    return-void
.end method

.method public d(Z)V
    .locals 0

    iput-boolean p1, p0, Lco/uk/getmondo/d/aj;->peerToPeer:Z

    return-void
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->S()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/aj;->amountLocalCurrency:Ljava/lang/String;

    return-void
.end method

.method public e(Z)V
    .locals 0

    iput-boolean p1, p0, Lco/uk/getmondo/d/aj;->topUp:Z

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 253
    if-ne p0, p1, :cond_1

    move v1, v0

    .line 286
    :cond_0
    :goto_0
    return v1

    .line 254
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    .line 256
    check-cast p1, Lco/uk/getmondo/d/aj;

    .line 258
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->I()J

    move-result-wide v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->I()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 259
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->K()J

    move-result-wide v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->K()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 260
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->T()Z

    move-result v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->T()Z

    move-result v3

    if-ne v2, v3, :cond_0

    .line 261
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->U()Z

    move-result v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->U()Z

    move-result v3

    if-ne v2, v3, :cond_0

    .line 262
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->X()Z

    move-result v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->X()Z

    move-result v3

    if-ne v2, v3, :cond_0

    .line 263
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->Y()Z

    move-result v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->Y()Z

    move-result v3

    if-ne v2, v3, :cond_0

    .line 264
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->Z()Z

    move-result v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->Z()Z

    move-result v3

    if-ne v2, v3, :cond_0

    .line 265
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->aa()Z

    move-result v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->aa()Z

    move-result v3

    if-ne v2, v3, :cond_0

    .line 266
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->ae()Z

    move-result v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->ae()Z

    move-result v3

    if-ne v2, v3, :cond_0

    .line 267
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->H()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_13

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->H()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->H()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 268
    :cond_2
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->J()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_14

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->J()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->J()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 269
    :cond_3
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->L()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_15

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->L()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->L()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 271
    :cond_4
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->M()Ljava/util/Date;

    move-result-object v2

    if-eqz v2, :cond_16

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->M()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->M()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 272
    :cond_5
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->N()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_17

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->N()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->N()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 274
    :cond_6
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->O()Ljava/util/Date;

    move-result-object v2

    if-eqz v2, :cond_18

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->O()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->O()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 275
    :cond_7
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->P()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_19

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->P()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->P()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 277
    :cond_8
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->Q()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1a

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->Q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->Q()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 278
    :cond_9
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->R()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1b

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->R()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->R()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 279
    :cond_a
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->S()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1c

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->S()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->S()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 280
    :cond_b
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->V()Lco/uk/getmondo/d/u;

    move-result-object v2

    if-eqz v2, :cond_1d

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->V()Lco/uk/getmondo/d/u;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->V()Lco/uk/getmondo/d/u;

    move-result-object v3

    invoke-virtual {v2, v3}, Lco/uk/getmondo/d/u;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 281
    :cond_c
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->W()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1e

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->W()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->W()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 282
    :cond_d
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->ab()Lio/realm/az;

    move-result-object v2

    if-eqz v2, :cond_1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->ab()Lio/realm/az;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->ab()Lio/realm/az;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/realm/az;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 283
    :cond_e
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->ac()Lco/uk/getmondo/d/aa;

    move-result-object v2

    if-eqz v2, :cond_20

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->ac()Lco/uk/getmondo/d/aa;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->ac()Lco/uk/getmondo/d/aa;

    move-result-object v3

    invoke-virtual {v2, v3}, Lco/uk/getmondo/d/aa;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 284
    :cond_f
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->ad()Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v2

    if-eqz v2, :cond_21

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->ad()Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->ad()Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v3

    invoke-virtual {v2, v3}, Lco/uk/getmondo/payments/send/data/a/a;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 285
    :cond_10
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->af()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_22

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->af()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->af()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 286
    :cond_11
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->ag()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_23

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->ag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :cond_12
    :goto_1
    move v1, v0

    goto/16 :goto_0

    .line 267
    :cond_13
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->H()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    goto/16 :goto_0

    .line 268
    :cond_14
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->J()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    goto/16 :goto_0

    .line 269
    :cond_15
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->L()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    goto/16 :goto_0

    .line 271
    :cond_16
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->M()Ljava/util/Date;

    move-result-object v2

    if-eqz v2, :cond_5

    goto/16 :goto_0

    .line 272
    :cond_17
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->N()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    goto/16 :goto_0

    .line 274
    :cond_18
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->O()Ljava/util/Date;

    move-result-object v2

    if-eqz v2, :cond_7

    goto/16 :goto_0

    .line 275
    :cond_19
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->P()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_8

    goto/16 :goto_0

    .line 277
    :cond_1a
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->Q()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_9

    goto/16 :goto_0

    .line 278
    :cond_1b
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->R()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_a

    goto/16 :goto_0

    .line 279
    :cond_1c
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->S()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_b

    goto/16 :goto_0

    .line 280
    :cond_1d
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->V()Lco/uk/getmondo/d/u;

    move-result-object v2

    if-eqz v2, :cond_c

    goto/16 :goto_0

    .line 281
    :cond_1e
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->W()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_d

    goto/16 :goto_0

    .line 282
    :cond_1f
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->ab()Lio/realm/az;

    move-result-object v2

    if-eqz v2, :cond_e

    goto/16 :goto_0

    .line 283
    :cond_20
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->ac()Lco/uk/getmondo/d/aa;

    move-result-object v2

    if-eqz v2, :cond_f

    goto/16 :goto_0

    .line 284
    :cond_21
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->ad()Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v2

    if-eqz v2, :cond_10

    goto/16 :goto_0

    .line 285
    :cond_22
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->af()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_11

    goto/16 :goto_0

    .line 286
    :cond_23
    invoke-virtual {p1}, Lco/uk/getmondo/d/aj;->ag()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_12

    move v0, v1

    goto/16 :goto_1
.end method

.method public f()Lco/uk/getmondo/d/u;
    .locals 1

    .prologue
    .line 107
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->V()Lco/uk/getmondo/d/u;

    move-result-object v0

    return-object v0
.end method

.method public f(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/aj;->createdDateFormatted:Ljava/lang/String;

    return-void
.end method

.method public f(Z)V
    .locals 0

    iput-boolean p1, p0, Lco/uk/getmondo/d/aj;->includeInSpending:Z

    return-void
.end method

.method public g()Lco/uk/getmondo/d/c;
    .locals 4

    .prologue
    .line 111
    new-instance v0, Lco/uk/getmondo/d/c;

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->I()J

    move-result-wide v2

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->J()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Lco/uk/getmondo/d/c;-><init>(JLjava/lang/String;)V

    return-object v0
.end method

.method public g(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/aj;->merchantDescription:Ljava/lang/String;

    return-void
.end method

.method public g(Z)V
    .locals 0

    iput-boolean p1, p0, Lco/uk/getmondo/d/aj;->fromMonzoMe:Z

    return-void
.end method

.method public h()Lco/uk/getmondo/d/c;
    .locals 4

    .prologue
    .line 115
    new-instance v0, Lco/uk/getmondo/d/c;

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->K()J

    move-result-wide v2

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->L()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Lco/uk/getmondo/d/c;-><init>(JLjava/lang/String;)V

    return-object v0
.end method

.method public h(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/aj;->description:Ljava/lang/String;

    return-void
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const/16 v8, 0x20

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 291
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->H()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->H()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 292
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->I()J

    move-result-wide v4

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->I()J

    move-result-wide v6

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int/2addr v0, v3

    .line 293
    mul-int/lit8 v3, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->J()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->J()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    .line 294
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->K()J

    move-result-wide v4

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->K()J

    move-result-wide v6

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int/2addr v0, v3

    .line 295
    mul-int/lit8 v3, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->L()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->L()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v3

    .line 296
    mul-int/lit8 v3, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->M()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->M()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v3

    .line 297
    mul-int/lit8 v3, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->N()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->N()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v3

    .line 298
    mul-int/lit8 v3, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->O()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->O()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v3

    .line 299
    mul-int/lit8 v3, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->P()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->P()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_6
    add-int/2addr v0, v3

    .line 300
    mul-int/lit8 v3, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->Q()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->Q()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_7
    add-int/2addr v0, v3

    .line 301
    mul-int/lit8 v3, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->R()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->R()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_8
    add-int/2addr v0, v3

    .line 302
    mul-int/lit8 v3, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->S()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->S()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_9
    add-int/2addr v0, v3

    .line 303
    mul-int/lit8 v3, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->T()Z

    move-result v0

    if-eqz v0, :cond_b

    move v0, v2

    :goto_a
    add-int/2addr v0, v3

    .line 304
    mul-int/lit8 v3, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->U()Z

    move-result v0

    if-eqz v0, :cond_c

    move v0, v2

    :goto_b
    add-int/2addr v0, v3

    .line 305
    mul-int/lit8 v3, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->V()Lco/uk/getmondo/d/u;

    move-result-object v0

    if-eqz v0, :cond_d

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->V()Lco/uk/getmondo/d/u;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/u;->hashCode()I

    move-result v0

    :goto_c
    add-int/2addr v0, v3

    .line 306
    mul-int/lit8 v3, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->W()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_e

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->W()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_d
    add-int/2addr v0, v3

    .line 307
    mul-int/lit8 v3, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->X()Z

    move-result v0

    if-eqz v0, :cond_f

    move v0, v2

    :goto_e
    add-int/2addr v0, v3

    .line 308
    mul-int/lit8 v3, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->Y()Z

    move-result v0

    if-eqz v0, :cond_10

    move v0, v2

    :goto_f
    add-int/2addr v0, v3

    .line 309
    mul-int/lit8 v3, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->Z()Z

    move-result v0

    if-eqz v0, :cond_11

    move v0, v2

    :goto_10
    add-int/2addr v0, v3

    .line 310
    mul-int/lit8 v3, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->aa()Z

    move-result v0

    if-eqz v0, :cond_12

    move v0, v2

    :goto_11
    add-int/2addr v0, v3

    .line 311
    mul-int/lit8 v3, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->ab()Lio/realm/az;

    move-result-object v0

    if-eqz v0, :cond_13

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->ab()Lio/realm/az;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/az;->hashCode()I

    move-result v0

    :goto_12
    add-int/2addr v0, v3

    .line 312
    mul-int/lit8 v3, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->ac()Lco/uk/getmondo/d/aa;

    move-result-object v0

    if-eqz v0, :cond_14

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->ac()Lco/uk/getmondo/d/aa;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->hashCode()I

    move-result v0

    :goto_13
    add-int/2addr v0, v3

    .line 313
    mul-int/lit8 v3, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->ad()Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v0

    if-eqz v0, :cond_15

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->ad()Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/data/a/a;->hashCode()I

    move-result v0

    :goto_14
    add-int/2addr v0, v3

    .line 314
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->ae()Z

    move-result v3

    if-eqz v3, :cond_16

    :goto_15
    add-int/2addr v0, v2

    .line 315
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->af()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_17

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->af()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_16
    add-int/2addr v0, v2

    .line 316
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->ag()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 317
    return v0

    :cond_1
    move v0, v1

    .line 291
    goto/16 :goto_0

    :cond_2
    move v0, v1

    .line 293
    goto/16 :goto_1

    :cond_3
    move v0, v1

    .line 295
    goto/16 :goto_2

    :cond_4
    move v0, v1

    .line 296
    goto/16 :goto_3

    :cond_5
    move v0, v1

    .line 297
    goto/16 :goto_4

    :cond_6
    move v0, v1

    .line 298
    goto/16 :goto_5

    :cond_7
    move v0, v1

    .line 299
    goto/16 :goto_6

    :cond_8
    move v0, v1

    .line 300
    goto/16 :goto_7

    :cond_9
    move v0, v1

    .line 301
    goto/16 :goto_8

    :cond_a
    move v0, v1

    .line 302
    goto/16 :goto_9

    :cond_b
    move v0, v1

    .line 303
    goto/16 :goto_a

    :cond_c
    move v0, v1

    .line 304
    goto/16 :goto_b

    :cond_d
    move v0, v1

    .line 305
    goto/16 :goto_c

    :cond_e
    move v0, v1

    .line 306
    goto/16 :goto_d

    :cond_f
    move v0, v1

    .line 307
    goto/16 :goto_e

    :cond_10
    move v0, v1

    .line 308
    goto/16 :goto_f

    :cond_11
    move v0, v1

    .line 309
    goto/16 :goto_10

    :cond_12
    move v0, v1

    .line 310
    goto/16 :goto_11

    :cond_13
    move v0, v1

    .line 311
    goto/16 :goto_12

    :cond_14
    move v0, v1

    .line 312
    goto :goto_13

    :cond_15
    move v0, v1

    .line 313
    goto :goto_14

    :cond_16
    move v2, v1

    .line 314
    goto :goto_15

    :cond_17
    move v0, v1

    .line 315
    goto :goto_16
.end method

.method public i(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/aj;->declineReason:Ljava/lang/String;

    return-void
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 119
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->J()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->L()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()D
    .locals 5

    .prologue
    const-wide/16 v0, 0x0

    .line 123
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->g()Lco/uk/getmondo/d/c;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/d/c;->f()D

    move-result-wide v2

    .line 124
    cmpl-double v4, v2, v0

    if-nez v4, :cond_0

    .line 127
    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->h()Lco/uk/getmondo/d/c;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/c;->f()D

    move-result-wide v0

    div-double/2addr v0, v2

    goto :goto_0
.end method

.method public j(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/aj;->notes:Ljava/lang/String;

    return-void
.end method

.method public k(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/aj;->category:Ljava/lang/String;

    return-void
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 131
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->U()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->d()Lcom/c/b/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/c/b/b;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->T()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->g()Lco/uk/getmondo/d/c;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Ljava/lang/String;
    .locals 3

    .prologue
    .line 135
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->P()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, " +"

    const-string v2, " "

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public l(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/aj;->scheme:Ljava/lang/String;

    return-void
.end method

.method public m(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/d/aj;->bacsDirectDebitInstructionId:Ljava/lang/String;

    return-void
.end method

.method public m()Z
    .locals 2

    .prologue
    .line 139
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->S()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->S()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 143
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->f()Lco/uk/getmondo/d/u;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->f()Lco/uk/getmondo/d/u;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/u;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/k/p;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->v()Ljava/util/Date;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/c/a;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public p()Z
    .locals 1

    .prologue
    .line 152
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->d()Lcom/c/b/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/c/b/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->d()Lcom/c/b/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/c/b/b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lco/uk/getmondo/common/k/p;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()Z
    .locals 1

    .prologue
    .line 166
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->Y()Z

    move-result v0

    return v0
.end method

.method public r()Z
    .locals 2

    .prologue
    .line 170
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->D()Lco/uk/getmondo/d/af;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/d/af;->FASTER_PAYMENT:Lco/uk/getmondo/d/af;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->ad()Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public s()Z
    .locals 2

    .prologue
    .line 174
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->D()Lco/uk/getmondo/d/af;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/d/af;->BACS:Lco/uk/getmondo/d/af;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public t()Z
    .locals 2

    .prologue
    .line 178
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->D()Lco/uk/getmondo/d/af;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/d/af;->OVERDRAFT:Lco/uk/getmondo/d/af;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public u()Z
    .locals 1

    .prologue
    .line 182
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->G()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public v()Ljava/util/Date;
    .locals 1

    .prologue
    .line 186
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->M()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public w()Ljava/lang/String;
    .locals 1

    .prologue
    .line 190
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->H()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public x()Ljava/lang/String;
    .locals 1

    .prologue
    .line 202
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->Q()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public y()Z
    .locals 1

    .prologue
    .line 206
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->T()Z

    move-result v0

    return v0
.end method

.method public z()Z
    .locals 1

    .prologue
    .line 210
    invoke-virtual {p0}, Lco/uk/getmondo/d/aj;->Z()Z

    move-result v0

    return v0
.end method
