.class public Lco/uk/getmondo/bump_up/BumpUpActivity;
.super Lco/uk/getmondo/common/activities/h;
.source "BumpUpActivity.java"


# instance fields
.field a:Lco/uk/getmondo/common/accounts/d;

.field private b:Lco/uk/getmondo/d/k;

.field eventBumpText:Lco/uk/getmondo/common/ui/AnimatedNumberView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11011d
    .end annotation
.end field

.field eventMessageText:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11011c
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/h;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Intent;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 38
    if-nez p0, :cond_0

    .line 44
    :goto_0
    return v0

    .line 42
    :cond_0
    :try_start_0
    const-string v1, "BUMP_EXTRA"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 43
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;Lco/uk/getmondo/d/k;)V
    .locals 2

    .prologue
    .line 32
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/bump_up/BumpUpActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 33
    const-class v1, Lco/uk/getmondo/d/k;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 34
    const/16 v1, 0x2c3b

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 35
    return-void
.end method

.method public static a(I)Z
    .locals 1

    .prologue
    .line 49
    const/16 v0, 0x2c3b

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public finish()V
    .locals 3

    .prologue
    .line 76
    iget-object v0, p0, Lco/uk/getmondo/bump_up/BumpUpActivity;->b:Lco/uk/getmondo/d/k;

    if-eqz v0, :cond_0

    .line 77
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 78
    const-string v1, "BUMP_EXTRA"

    iget-object v2, p0, Lco/uk/getmondo/bump_up/BumpUpActivity;->b:Lco/uk/getmondo/d/k;

    invoke-virtual {v2}, Lco/uk/getmondo/d/k;->d()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 79
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lco/uk/getmondo/bump_up/BumpUpActivity;->setResult(ILandroid/content/Intent;)V

    .line 81
    :cond_0
    invoke-super {p0}, Lco/uk/getmondo/common/activities/h;->finish()V

    .line 82
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 54
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/h;->onCreate(Landroid/os/Bundle;)V

    .line 55
    const v0, 0x7f050024

    invoke-virtual {p0, v0}, Lco/uk/getmondo/bump_up/BumpUpActivity;->setContentView(I)V

    .line 56
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 57
    invoke-virtual {p0}, Lco/uk/getmondo/bump_up/BumpUpActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/bump_up/BumpUpActivity;)V

    .line 58
    invoke-virtual {p0}, Lco/uk/getmondo/bump_up/BumpUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-class v1, Lco/uk/getmondo/d/k;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    invoke-virtual {p0}, Lco/uk/getmondo/bump_up/BumpUpActivity;->finish()V

    .line 65
    :goto_0
    return-void

    .line 62
    :cond_0
    invoke-virtual {p0}, Lco/uk/getmondo/bump_up/BumpUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-class v1, Lco/uk/getmondo/d/k;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/k;

    iput-object v0, p0, Lco/uk/getmondo/bump_up/BumpUpActivity;->b:Lco/uk/getmondo/d/k;

    .line 63
    iget-object v0, p0, Lco/uk/getmondo/bump_up/BumpUpActivity;->eventBumpText:Lco/uk/getmondo/common/ui/AnimatedNumberView;

    iget-object v1, p0, Lco/uk/getmondo/bump_up/BumpUpActivity;->b:Lco/uk/getmondo/d/k;

    invoke-virtual {v1}, Lco/uk/getmondo/d/k;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/AnimatedNumberView;->setNumber(I)V

    .line 64
    iget-object v0, p0, Lco/uk/getmondo/bump_up/BumpUpActivity;->eventMessageText:Landroid/widget/TextView;

    iget-object v1, p0, Lco/uk/getmondo/bump_up/BumpUpActivity;->b:Lco/uk/getmondo/d/k;

    invoke-virtual {v1}, Lco/uk/getmondo/d/k;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onShareMoreInvites()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f11011e
        }
    .end annotation

    .prologue
    .line 69
    const v0, 0x7f0a00dc

    invoke-virtual {p0, v0}, Lco/uk/getmondo/bump_up/BumpUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 70
    iget-object v1, p0, Lco/uk/getmondo/bump_up/BumpUpActivity;->a:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v1}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/d/ak;->e()Lco/uk/getmondo/d/an;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/d/an;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/common/k/j;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 71
    invoke-static {v1, v0}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/bump_up/BumpUpActivity;->startActivity(Landroid/content/Intent;)V

    .line 72
    return-void
.end method
