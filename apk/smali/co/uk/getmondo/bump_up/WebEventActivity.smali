.class public Lco/uk/getmondo/bump_up/WebEventActivity;
.super Lco/uk/getmondo/common/activities/h;
.source "WebEventActivity.java"

# interfaces
.implements Lco/uk/getmondo/bump_up/d$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/bump_up/WebEventActivity$a;
    }
.end annotation


# instance fields
.field a:Lco/uk/getmondo/common/a;

.field b:Lco/uk/getmondo/bump_up/d;

.field c:Lco/uk/getmondo/common/k;

.field private final e:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field eventWebView:Landroid/webkit/WebView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11027b
    .end annotation
.end field

.field private f:Lco/uk/getmondo/d/k;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/h;-><init>()V

    .line 30
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/bump_up/WebEventActivity;->e:Lcom/b/b/c;

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/bump_up/WebEventActivity;)Lcom/b/b/c;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lco/uk/getmondo/bump_up/WebEventActivity;->e:Lcom/b/b/c;

    return-object v0
.end method

.method public static a(Landroid/app/Activity;Lco/uk/getmondo/d/k;)V
    .locals 2

    .prologue
    .line 41
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/bump_up/WebEventActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 42
    const-class v1, Lco/uk/getmondo/d/k;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 43
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 44
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lco/uk/getmondo/bump_up/WebEventActivity;->f:Lco/uk/getmondo/d/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/bump_up/WebEventActivity;->f:Lco/uk/getmondo/d/k;

    invoke-virtual {v0}, Lco/uk/getmondo/d/k;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lco/uk/getmondo/bump_up/WebEventActivity;->a:Lco/uk/getmondo/common/a;

    iget-object v1, p0, Lco/uk/getmondo/bump_up/WebEventActivity;->f:Lco/uk/getmondo/d/k;

    invoke-virtual {v1}, Lco/uk/getmondo/d/k;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/api/model/tracking/Impression;->b(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 71
    :cond_0
    invoke-super {p0}, Lco/uk/getmondo/common/activities/h;->a()V

    .line 72
    return-void
.end method

.method public a(Lco/uk/getmondo/bump_up/b;)V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lco/uk/getmondo/bump_up/WebEventActivity;->a:Lco/uk/getmondo/common/a;

    invoke-virtual {p1}, Lco/uk/getmondo/bump_up/b;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/api/model/tracking/Impression;->d(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 88
    iget-object v0, p0, Lco/uk/getmondo/bump_up/WebEventActivity;->c:Lco/uk/getmondo/common/k;

    invoke-virtual {p1}, Lco/uk/getmondo/bump_up/b;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lco/uk/getmondo/common/k;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 89
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lco/uk/getmondo/bump_up/WebEventActivity;->e:Lcom/b/b/c;

    return-object v0
.end method

.method public b(Lco/uk/getmondo/bump_up/b;)V
    .locals 3

    .prologue
    .line 93
    iget-object v0, p0, Lco/uk/getmondo/bump_up/WebEventActivity;->a:Lco/uk/getmondo/common/a;

    invoke-virtual {p1}, Lco/uk/getmondo/bump_up/b;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/api/model/tracking/Impression;->c(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 94
    invoke-virtual {p1}, Lco/uk/getmondo/bump_up/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/bump_up/b;->b()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lco/uk/getmondo/api/model/tracking/a;->WAITING_LIST_WEB_EVENT:Lco/uk/getmondo/api/model/tracking/a;

    invoke-static {p0, v0, v1, v2}, Lco/uk/getmondo/common/k/j;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/a;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/bump_up/WebEventActivity;->startActivity(Landroid/content/Intent;)V

    .line 96
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 48
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/h;->onCreate(Landroid/os/Bundle;)V

    .line 49
    const v0, 0x7f050076

    invoke-virtual {p0, v0}, Lco/uk/getmondo/bump_up/WebEventActivity;->setContentView(I)V

    .line 50
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 51
    invoke-virtual {p0}, Lco/uk/getmondo/bump_up/WebEventActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/bump_up/WebEventActivity;)V

    .line 52
    invoke-virtual {p0}, Lco/uk/getmondo/bump_up/WebEventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-class v1, Lco/uk/getmondo/d/k;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    invoke-virtual {p0}, Lco/uk/getmondo/bump_up/WebEventActivity;->finish()V

    .line 64
    :goto_0
    return-void

    .line 56
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/bump_up/WebEventActivity;->b:Lco/uk/getmondo/bump_up/d;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/bump_up/d;->a(Lco/uk/getmondo/bump_up/d$a;)V

    .line 57
    invoke-virtual {p0}, Lco/uk/getmondo/bump_up/WebEventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-class v1, Lco/uk/getmondo/d/k;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/k;

    iput-object v0, p0, Lco/uk/getmondo/bump_up/WebEventActivity;->f:Lco/uk/getmondo/d/k;

    .line 58
    iget-object v0, p0, Lco/uk/getmondo/bump_up/WebEventActivity;->eventWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 59
    iget-object v0, p0, Lco/uk/getmondo/bump_up/WebEventActivity;->eventWebView:Landroid/webkit/WebView;

    new-instance v1, Landroid/webkit/WebViewClient;

    invoke-direct {v1}, Landroid/webkit/WebViewClient;-><init>()V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 60
    iget-object v0, p0, Lco/uk/getmondo/bump_up/WebEventActivity;->eventWebView:Landroid/webkit/WebView;

    new-instance v1, Lco/uk/getmondo/bump_up/WebEventActivity$a;

    invoke-direct {v1, p0}, Lco/uk/getmondo/bump_up/WebEventActivity$a;-><init>(Lco/uk/getmondo/bump_up/WebEventActivity;)V

    const-string v2, "mondo"

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    iget-object v0, p0, Lco/uk/getmondo/bump_up/WebEventActivity;->eventWebView:Landroid/webkit/WebView;

    iget-object v1, p0, Lco/uk/getmondo/bump_up/WebEventActivity;->f:Lco/uk/getmondo/d/k;

    invoke-virtual {v1}, Lco/uk/getmondo/d/k;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lco/uk/getmondo/bump_up/WebEventActivity;->eventWebView:Landroid/webkit/WebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    .line 63
    iget-object v0, p0, Lco/uk/getmondo/bump_up/WebEventActivity;->a:Lco/uk/getmondo/common/a;

    iget-object v1, p0, Lco/uk/getmondo/bump_up/WebEventActivity;->f:Lco/uk/getmondo/d/k;

    invoke-virtual {v1}, Lco/uk/getmondo/d/k;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lco/uk/getmondo/api/model/tracking/Impression;->a(Ljava/lang/String;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lco/uk/getmondo/bump_up/WebEventActivity;->b:Lco/uk/getmondo/bump_up/d;

    invoke-virtual {v0}, Lco/uk/getmondo/bump_up/d;->b()V

    .line 77
    invoke-super {p0}, Lco/uk/getmondo/common/activities/h;->onDestroy()V

    .line 78
    return-void
.end method
