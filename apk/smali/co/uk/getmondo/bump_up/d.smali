.class Lco/uk/getmondo/bump_up/d;
.super Lco/uk/getmondo/common/ui/b;
.source "WebEventPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/bump_up/d$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/bump_up/d$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lco/uk/getmondo/common/accounts/d;

.field private final d:Lcom/google/gson/f;


# direct methods
.method constructor <init>(Lco/uk/getmondo/common/accounts/d;Lcom/google/gson/f;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 24
    iput-object p1, p0, Lco/uk/getmondo/bump_up/d;->c:Lco/uk/getmondo/common/accounts/d;

    .line 25
    iput-object p2, p0, Lco/uk/getmondo/bump_up/d;->d:Lcom/google/gson/f;

    .line 26
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/bump_up/b;Lco/uk/getmondo/d/ak;)Lco/uk/getmondo/bump_up/b;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 38
    invoke-virtual {p0}, Lco/uk/getmondo/bump_up/b;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lco/uk/getmondo/d/ak;->e()Lco/uk/getmondo/d/an;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 39
    invoke-virtual {p1}, Lco/uk/getmondo/d/ak;->e()Lco/uk/getmondo/d/an;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/an;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/bump_up/b;->a(Ljava/lang/String;)V

    .line 41
    :cond_0
    return-object p0
.end method

.method static synthetic a(Lco/uk/getmondo/bump_up/d;Ljava/lang/String;)Lco/uk/getmondo/bump_up/b;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lco/uk/getmondo/bump_up/d;->d:Lcom/google/gson/f;

    const-class v1, Lco/uk/getmondo/bump_up/b;

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/f;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/bump_up/b;

    return-object v0
.end method

.method private a(Ljava/lang/String;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/bump_up/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    invoke-static {p0, p1}, Lco/uk/getmondo/bump_up/i;->a(Lco/uk/getmondo/bump_up/d;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/v;->c(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/bump_up/d;Lco/uk/getmondo/bump_up/b;)Lio/reactivex/z;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lco/uk/getmondo/bump_up/d;->c:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->c()Lio/reactivex/v;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/bump_up/j;->a(Lco/uk/getmondo/bump_up/b;)Lio/reactivex/c/h;

    move-result-object v1

    .line 37
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    .line 36
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/bump_up/d$a;Lco/uk/getmondo/bump_up/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 44
    invoke-virtual {p1}, Lco/uk/getmondo/bump_up/b;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 45
    invoke-interface {p0, p1}, Lco/uk/getmondo/bump_up/d$a;->a(Lco/uk/getmondo/bump_up/b;)V

    .line 49
    :cond_0
    :goto_0
    return-void

    .line 46
    :cond_1
    invoke-virtual {p1}, Lco/uk/getmondo/bump_up/b;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    invoke-interface {p0, p1}, Lco/uk/getmondo/bump_up/d$a;->b(Lco/uk/getmondo/bump_up/b;)V

    goto :goto_0
.end method

.method static synthetic b(Lco/uk/getmondo/bump_up/d;Ljava/lang/String;)Lio/reactivex/l;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lco/uk/getmondo/bump_up/d;->a(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Lio/reactivex/v;->e()Lio/reactivex/h;

    move-result-object v0

    .line 35
    invoke-static {}, Lio/reactivex/h;->a()Lio/reactivex/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/h;->a(Lio/reactivex/l;)Lio/reactivex/h;

    move-result-object v0

    .line 33
    return-object v0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/bump_up/d$a;)V
    .locals 3

    .prologue
    .line 30
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 32
    invoke-interface {p1}, Lco/uk/getmondo/bump_up/d$a;->b()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/bump_up/e;->a(Lco/uk/getmondo/bump_up/d;)Lio/reactivex/c/h;

    move-result-object v1

    .line 33
    invoke-virtual {v0, v1}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/bump_up/f;->a(Lco/uk/getmondo/bump_up/d;)Lio/reactivex/c/h;

    move-result-object v1

    .line 36
    invoke-virtual {v0, v1}, Lio/reactivex/n;->flatMapSingle(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/bump_up/g;->a(Lco/uk/getmondo/bump_up/d$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/bump_up/h;->a()Lio/reactivex/c/g;

    move-result-object v2

    .line 43
    invoke-virtual {v0, v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 32
    invoke-virtual {p0, v0}, Lco/uk/getmondo/bump_up/d;->a(Lio/reactivex/b/b;)V

    .line 50
    return-void
.end method

.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 16
    check-cast p1, Lco/uk/getmondo/bump_up/d$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/bump_up/d;->a(Lco/uk/getmondo/bump_up/d$a;)V

    return-void
.end method
