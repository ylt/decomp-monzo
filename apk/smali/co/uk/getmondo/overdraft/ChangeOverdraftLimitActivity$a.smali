.class public final Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity$a;
.super Ljava/lang/Object;
.source "ChangeOverdraftLimitActivity.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eR\u0014\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0008X\u0082D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u000f"
    }
    d2 = {
        "Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity$Companion;",
        "",
        "()V",
        "CONFIRMATION_FRAGMENT_TAG",
        "",
        "getCONFIRMATION_FRAGMENT_TAG",
        "()Ljava/lang/String;",
        "CONFIRMATION_REQUEST_CODE",
        "",
        "getCONFIRMATION_REQUEST_CODE",
        "()I",
        "buildIntent",
        "Landroid/content/Intent;",
        "context",
        "Landroid/content/Context;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity$a;-><init>()V

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity$a;)I
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity$a;->b()I

    move-result v0

    return v0
.end method

.method private final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    invoke-static {}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final b()I
    .locals 1

    .prologue
    .line 111
    invoke-static {}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->j()I

    move-result v0

    return v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity$a;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method
