.class final Lco/uk/getmondo/overdraft/b$d;
.super Ljava/lang/Object;
.source "ChangeOverdraftLimitPresenter.kt"

# interfaces
.implements Lio/reactivex/c/q;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/overdraft/b;->a(Lco/uk/getmondo/overdraft/b$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/q",
        "<",
        "Lkotlin/n;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "test",
        "(Lkotlin/Unit;)Z"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/overdraft/b;


# direct methods
.method constructor <init>(Lco/uk/getmondo/overdraft/b;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/overdraft/b$d;->a:Lco/uk/getmondo/overdraft/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 19
    check-cast p1, Lkotlin/n;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/overdraft/b$d;->a(Lkotlin/n;)Z

    move-result v0

    return v0
.end method

.method public final a(Lkotlin/n;)Z
    .locals 4

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lco/uk/getmondo/overdraft/b$d;->a:Lco/uk/getmondo/overdraft/b;

    invoke-static {v0}, Lco/uk/getmondo/overdraft/b;->d(Lco/uk/getmondo/overdraft/b;)Lco/uk/getmondo/d/c;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/c;->k()J

    move-result-wide v0

    iget-object v2, p0, Lco/uk/getmondo/overdraft/b$d;->a:Lco/uk/getmondo/overdraft/b;

    invoke-static {v2}, Lco/uk/getmondo/overdraft/b;->e(Lco/uk/getmondo/overdraft/b;)J

    move-result-wide v2

    add-long/2addr v0, v2

    iget-object v2, p0, Lco/uk/getmondo/overdraft/b$d;->a:Lco/uk/getmondo/overdraft/b;

    invoke-static {v2}, Lco/uk/getmondo/overdraft/b;->b(Lco/uk/getmondo/overdraft/b;)Lco/uk/getmondo/d/w;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/d/w;->h()Lco/uk/getmondo/d/c;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/d/c;->k()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
