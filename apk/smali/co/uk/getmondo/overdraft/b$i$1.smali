.class final Lco/uk/getmondo/overdraft/b$i$1;
.super Ljava/lang/Object;
.source "ChangeOverdraftLimitPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/overdraft/b$i;->a(Lkotlin/n;)Lio/reactivex/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<",
        "Ljava/lang/String;",
        "Lio/reactivex/d;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Completable;",
        "accountId",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/overdraft/b$i;


# direct methods
.method constructor <init>(Lco/uk/getmondo/overdraft/b$i;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/overdraft/b$i$1;->a:Lco/uk/getmondo/overdraft/b$i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lio/reactivex/b;
    .locals 2

    .prologue
    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lco/uk/getmondo/overdraft/b$i$1;->a:Lco/uk/getmondo/overdraft/b$i;

    iget-object v0, v0, Lco/uk/getmondo/overdraft/b$i;->a:Lco/uk/getmondo/overdraft/b;

    invoke-static {v0}, Lco/uk/getmondo/overdraft/b;->a(Lco/uk/getmondo/overdraft/b;)Lco/uk/getmondo/overdraft/a/c;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/overdraft/b$i$1;->a:Lco/uk/getmondo/overdraft/b$i;

    iget-object v1, v1, Lco/uk/getmondo/overdraft/b$i;->a:Lco/uk/getmondo/overdraft/b;

    invoke-static {v1}, Lco/uk/getmondo/overdraft/b;->d(Lco/uk/getmondo/overdraft/b;)Lco/uk/getmondo/d/c;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lco/uk/getmondo/overdraft/a/c;->a(Ljava/lang/String;Lco/uk/getmondo/d/c;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/overdraft/b$i$1;->a(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method
