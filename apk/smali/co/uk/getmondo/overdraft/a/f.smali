.class public final Lco/uk/getmondo/overdraft/a/f;
.super Ljava/lang/Object;
.source "OverdraftStorage.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00082\u0006\u0010\t\u001a\u00020\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lco/uk/getmondo/overdraft/data/OverdraftStorage;",
        "",
        "()V",
        "save",
        "Lio/reactivex/Completable;",
        "newStatus",
        "Lco/uk/getmondo/model/OverdraftStatus;",
        "status",
        "Lio/reactivex/Observable;",
        "accountId",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/d/w;)Lio/reactivex/b;
    .locals 1

    .prologue
    const-string v0, "newStatus"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    new-instance v0, Lco/uk/getmondo/overdraft/a/f$a;

    invoke-direct {v0, p1}, Lco/uk/getmondo/overdraft/a/f$a;-><init>(Lco/uk/getmondo/d/w;)V

    check-cast v0, Lio/realm/av$a;

    invoke-static {v0}, Lco/uk/getmondo/common/j/g;->a(Lio/realm/av$a;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/d/w;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    new-instance v0, Lco/uk/getmondo/overdraft/a/f$b;

    invoke-direct {v0, p1}, Lco/uk/getmondo/overdraft/a/f$b;-><init>(Ljava/lang/String;)V

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {v0}, Lco/uk/getmondo/common/j/g;->a(Lkotlin/d/a/b;)Lio/reactivex/n;

    move-result-object v1

    .line 23
    sget-object v0, Lco/uk/getmondo/overdraft/a/f$c;->a:Lco/uk/getmondo/overdraft/a/f$c;

    check-cast v0, Lio/reactivex/c/q;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v1

    .line 24
    sget-object v0, Lco/uk/getmondo/overdraft/a/f$d;->a:Lco/uk/getmondo/overdraft/a/f$d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lio/reactivex/n;->distinctUntilChanged()Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxRealm.asObservable { r\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
