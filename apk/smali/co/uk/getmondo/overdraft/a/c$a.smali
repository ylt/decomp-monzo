.class final Lco/uk/getmondo/overdraft/a/c$a;
.super Lkotlin/d/b/k;
.source "OverdraftManager.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/overdraft/a/c;->a(Lio/reactivex/v;)Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/k;",
        "Lkotlin/d/a/b",
        "<",
        "Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;",
        "Lco/uk/getmondo/d/w;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u0015\u0010\u0002\u001a\u00110\u0003\u00a2\u0006\u000c\u0008\u0004\u0012\u0008\u0008\u0005\u0012\u0004\u0008\u0008(\u0006\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lco/uk/getmondo/model/OverdraftStatus;",
        "p1",
        "Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;",
        "Lkotlin/ParameterName;",
        "name",
        "apiValue",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method constructor <init>(Lco/uk/getmondo/d/a/l;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lkotlin/d/b/k;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;)Lco/uk/getmondo/d/w;
    .locals 1

    .prologue
    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lco/uk/getmondo/overdraft/a/c$a;->b:Ljava/lang/Object;

    check-cast v0, Lco/uk/getmondo/d/a/l;

    .line 34
    invoke-virtual {v0, p1}, Lco/uk/getmondo/d/a/l;->a(Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;)Lco/uk/getmondo/d/w;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    check-cast p1, Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/overdraft/a/c$a;->a(Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;)Lco/uk/getmondo/d/w;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lkotlin/reflect/e;
    .locals 1

    const-class v0, Lco/uk/getmondo/d/a/l;

    invoke-static {v0}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "apply"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "apply(Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;)Lco/uk/getmondo/model/OverdraftStatus;"

    return-object v0
.end method
