.class public final Lco/uk/getmondo/overdraft/a/e;
.super Ljava/lang/Object;
.source "OverdraftManager_Factory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/overdraft/a/c;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/OverdraftApi;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/overdraft/a/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-class v0, Lco/uk/getmondo/overdraft/a/e;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/overdraft/a/e;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/a/a;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/OverdraftApi;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/overdraft/a/f;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    sget-boolean v0, Lco/uk/getmondo/overdraft/a/e;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 21
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/overdraft/a/e;->b:Ljavax/a/a;

    .line 22
    sget-boolean v0, Lco/uk/getmondo/overdraft/a/e;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 23
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/overdraft/a/e;->c:Ljavax/a/a;

    .line 24
    return-void
.end method

.method public static a(Ljavax/a/a;Ljavax/a/a;)Lb/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/OverdraftApi;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/overdraft/a/f;",
            ">;)",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/overdraft/a/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    new-instance v0, Lco/uk/getmondo/overdraft/a/e;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/overdraft/a/e;-><init>(Ljavax/a/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/overdraft/a/c;
    .locals 3

    .prologue
    .line 28
    new-instance v2, Lco/uk/getmondo/overdraft/a/c;

    iget-object v0, p0, Lco/uk/getmondo/overdraft/a/e;->b:Ljavax/a/a;

    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/OverdraftApi;

    iget-object v1, p0, Lco/uk/getmondo/overdraft/a/e;->c:Ljavax/a/a;

    invoke-interface {v1}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/overdraft/a/f;

    invoke-direct {v2, v0, v1}, Lco/uk/getmondo/overdraft/a/c;-><init>(Lco/uk/getmondo/api/OverdraftApi;Lco/uk/getmondo/overdraft/a/f;)V

    return-object v2
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8
    invoke-virtual {p0}, Lco/uk/getmondo/overdraft/a/e;->a()Lco/uk/getmondo/overdraft/a/c;

    move-result-object v0

    return-object v0
.end method
