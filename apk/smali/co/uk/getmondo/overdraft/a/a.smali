.class public final Lco/uk/getmondo/overdraft/a/a;
.super Ljava/lang/Object;
.source "OverdraftFeedItemMapper.kt"

# interfaces
.implements Lco/uk/getmondo/d/a/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lco/uk/getmondo/d/a/j",
        "<",
        "Lco/uk/getmondo/d/w;",
        "Lco/uk/getmondo/d/m;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0011\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\u00032\u0006\u0010\u0008\u001a\u00020\u0002H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lco/uk/getmondo/overdraft/data/OverdraftFeedItemMapper;",
        "Lco/uk/getmondo/model/mapper/Mapper;",
        "Lco/uk/getmondo/model/OverdraftStatus;",
        "Lco/uk/getmondo/model/FeedItem;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "apply",
        "inputValue",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/overdraft/a/a;->a:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/d/w;)Lco/uk/getmondo/d/m;
    .locals 37

    .prologue
    const-string v1, "inputValue"

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/w;->i()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/LocalDate;->m()Lorg/threeten/bp/LocalDateTime;

    move-result-object v1

    .line 28
    invoke-static {v1}, Lco/uk/getmondo/common/c/a;->a(Lorg/threeten/bp/LocalDateTime;)Ljava/util/Date;

    move-result-object v9

    .line 29
    move-object/from16 v0, p0

    iget-object v2, v0, Lco/uk/getmondo/overdraft/a/a;->a:Landroid/content/Context;

    const v3, 0x7f0a02dd

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {}, Lorg/threeten/bp/LocalDate;->a()Lorg/threeten/bp/LocalDate;

    move-result-object v6

    invoke-virtual {v6}, Lorg/threeten/bp/LocalDate;->f()Lorg/threeten/bp/Month;

    move-result-object v6

    sget-object v7, Lorg/threeten/bp/format/TextStyle;->a:Lorg/threeten/bp/format/TextStyle;

    sget-object v8, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v6, v7, v8}, Lorg/threeten/bp/Month;->a(Lorg/threeten/bp/format/TextStyle;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    .line 30
    move-object/from16 v0, p0

    iget-object v2, v0, Lco/uk/getmondo/overdraft/a/a;->a:Landroid/content/Context;

    const v3, 0x7f0a02dc

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lorg/threeten/bp/LocalDateTime;->e()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Lorg/threeten/bp/LocalDateTime;->e()I

    move-result v7

    invoke-static {v7}, Lco/uk/getmondo/common/c/a;->a(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 31
    const/4 v5, 0x1

    invoke-virtual {v1}, Lorg/threeten/bp/LocalDateTime;->d()Lorg/threeten/bp/Month;

    move-result-object v1

    sget-object v6, Lorg/threeten/bp/format/TextStyle;->a:Lorg/threeten/bp/format/TextStyle;

    sget-object v7, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v6, v7}, Lorg/threeten/bp/Month;->a(Lorg/threeten/bp/format/TextStyle;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    .line 30
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    .line 33
    new-instance v30, Lco/uk/getmondo/d/m;

    const-string v31, "overdraft_charges_item_id"

    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/w;->a()Ljava/lang/String;

    move-result-object v32

    sget-object v1, Lco/uk/getmondo/feed/a/a/a;->a:Lco/uk/getmondo/feed/a/a/a;

    invoke-virtual {v1}, Lco/uk/getmondo/feed/a/a/a;->a()Ljava/lang/String;

    move-result-object v33

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    .line 34
    new-instance v1, Lco/uk/getmondo/d/aj;

    const-string v2, "overdraft_charges_transaction_id"

    .line 35
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/w;->d()Lco/uk/getmondo/d/c;

    move-result-object v3

    invoke-virtual {v3}, Lco/uk/getmondo/d/c;->k()J

    move-result-wide v3

    .line 36
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/w;->d()Lco/uk/getmondo/d/c;

    move-result-object v5

    invoke-virtual {v5}, Lco/uk/getmondo/d/c;->l()Lco/uk/getmondo/common/i/c;

    move-result-object v5

    invoke-virtual {v5}, Lco/uk/getmondo/common/i/c;->b()Ljava/lang/String;

    move-result-object v5

    .line 37
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/w;->d()Lco/uk/getmondo/d/c;

    move-result-object v6

    invoke-virtual {v6}, Lco/uk/getmondo/d/c;->k()J

    move-result-wide v6

    .line 38
    invoke-virtual/range {p1 .. p1}, Lco/uk/getmondo/d/w;->d()Lco/uk/getmondo/d/c;

    move-result-object v8

    invoke-virtual {v8}, Lco/uk/getmondo/d/c;->l()Lco/uk/getmondo/common/i/c;

    move-result-object v8

    invoke-virtual {v8}, Lco/uk/getmondo/common/i/c;->b()Ljava/lang/String;

    move-result-object v8

    .line 40
    invoke-static {v9}, Lco/uk/getmondo/common/c/a;->d(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v10

    .line 42
    const/4 v12, 0x0

    .line 44
    const/4 v14, 0x0

    .line 46
    const/16 v16, 0x0

    .line 47
    const/16 v17, 0x0

    .line 48
    const/16 v18, 0x0

    .line 49
    const/16 v19, 0x0

    .line 50
    const/16 v20, 0x0

    .line 51
    const/16 v21, 0x0

    .line 52
    const/16 v22, 0x0

    .line 53
    const/16 v23, 0x0

    .line 54
    const/16 v24, 0x0

    .line 55
    const/16 v25, 0x0

    .line 56
    const/16 v26, 0x0

    .line 57
    const/16 v27, 0x0

    .line 58
    sget-object v11, Lco/uk/getmondo/d/af;->OVERDRAFT:Lco/uk/getmondo/d/af;

    invoke-virtual {v11}, Lco/uk/getmondo/d/af;->a()Ljava/lang/String;

    move-result-object v28

    .line 59
    const/16 v29, 0x0

    move-object v11, v9

    .line 34
    invoke-direct/range {v1 .. v29}, Lco/uk/getmondo/d/aj;-><init>(Ljava/lang/String;JLjava/lang/String;JLjava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLco/uk/getmondo/d/u;Ljava/lang/String;ZZZZLio/realm/az;Lco/uk/getmondo/d/aa;ZLco/uk/getmondo/payments/send/data/a/a;Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object/from16 v6, v30

    move-object/from16 v7, v31

    move-object/from16 v8, v32

    move-object/from16 v10, v33

    move-object/from16 v11, v34

    move-object/from16 v12, v35

    move-object/from16 v13, v36

    move-object v14, v1

    .line 33
    invoke-direct/range {v6 .. v18}, Lco/uk/getmondo/d/m;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/o;Lco/uk/getmondo/d/v;Lco/uk/getmondo/d/aj;Lco/uk/getmondo/d/r;Ljava/lang/String;Lco/uk/getmondo/d/f;Z)V

    return-object v30
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    check-cast p1, Lco/uk/getmondo/d/w;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/overdraft/a/a;->a(Lco/uk/getmondo/d/w;)Lco/uk/getmondo/d/m;

    move-result-object v0

    return-object v0
.end method
