.class public final Lco/uk/getmondo/overdraft/a/c;
.super Ljava/lang/Object;
.source "OverdraftManager.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nJ\u0016\u0010\u000b\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000c\u001a\u00020\rJ\u0014\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f2\u0006\u0010\t\u001a\u00020\nJ\u0012\u0010\u0011\u001a\u00020\u0008*\u0008\u0012\u0004\u0012\u00020\u00130\u0012H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lco/uk/getmondo/overdraft/data/OverdraftManager;",
        "",
        "overdraftApi",
        "Lco/uk/getmondo/api/OverdraftApi;",
        "overdraftStorage",
        "Lco/uk/getmondo/overdraft/data/OverdraftStorage;",
        "(Lco/uk/getmondo/api/OverdraftApi;Lco/uk/getmondo/overdraft/data/OverdraftStorage;)V",
        "refreshStatus",
        "Lio/reactivex/Completable;",
        "accountId",
        "",
        "setLimit",
        "limit",
        "Lco/uk/getmondo/model/Amount;",
        "status",
        "Lio/reactivex/Observable;",
        "Lco/uk/getmondo/model/OverdraftStatus;",
        "storeOverdraftStatus",
        "Lio/reactivex/Single;",
        "Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/api/OverdraftApi;

.field private final b:Lco/uk/getmondo/overdraft/a/f;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/api/OverdraftApi;Lco/uk/getmondo/overdraft/a/f;)V
    .locals 1

    .prologue
    const-string v0, "overdraftApi"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "overdraftStorage"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/overdraft/a/c;->a:Lco/uk/getmondo/api/OverdraftApi;

    iput-object p2, p0, Lco/uk/getmondo/overdraft/a/c;->b:Lco/uk/getmondo/overdraft/a/f;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/overdraft/a/c;)Lco/uk/getmondo/overdraft/a/f;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lco/uk/getmondo/overdraft/a/c;->b:Lco/uk/getmondo/overdraft/a/f;

    return-object v0
.end method

.method private final a(Lio/reactivex/v;)Lio/reactivex/b;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/overdraft/ApiOverdraftStatus;",
            ">;)",
            "Lio/reactivex/b;"
        }
    .end annotation

    .prologue
    .line 34
    new-instance v0, Lco/uk/getmondo/overdraft/a/c$a;

    sget-object v1, Lco/uk/getmondo/d/a/l;->INSTANCE:Lco/uk/getmondo/d/a/l;

    invoke-direct {v0, v1}, Lco/uk/getmondo/overdraft/a/c$a;-><init>(Lco/uk/getmondo/d/a/l;)V

    check-cast v0, Lkotlin/d/a/b;

    new-instance v1, Lco/uk/getmondo/overdraft/a/d;

    invoke-direct {v1, v0}, Lco/uk/getmondo/overdraft/a/d;-><init>(Lkotlin/d/a/b;)V

    move-object v0, v1

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v1

    .line 35
    new-instance v0, Lco/uk/getmondo/overdraft/a/c$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/overdraft/a/c$b;-><init>(Lco/uk/getmondo/overdraft/a/c;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "map(OverdraftMapper::app\u2026aftStorage.save(status) }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lio/reactivex/b;
    .locals 1

    .prologue
    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    iget-object v0, p0, Lco/uk/getmondo/overdraft/a/c;->a:Lco/uk/getmondo/api/OverdraftApi;

    invoke-interface {v0, p1}, Lco/uk/getmondo/api/OverdraftApi;->overdraftStatus(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    .line 21
    invoke-direct {p0, v0}, Lco/uk/getmondo/overdraft/a/c;->a(Lio/reactivex/v;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lco/uk/getmondo/d/c;)Lio/reactivex/b;
    .locals 5

    .prologue
    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "limit"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    iget-object v0, p0, Lco/uk/getmondo/overdraft/a/c;->a:Lco/uk/getmondo/api/OverdraftApi;

    invoke-virtual {p2}, Lco/uk/getmondo/d/c;->k()J

    move-result-wide v2

    invoke-virtual {p2}, Lco/uk/getmondo/d/c;->l()Lco/uk/getmondo/common/i/c;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/common/i/c;->b()Ljava/lang/String;

    move-result-object v1

    const-string v4, "limit.currency.currencyCode"

    invoke-static {v1, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1, v2, v3, v1}, Lco/uk/getmondo/api/OverdraftApi;->setOverdraftLimit(Ljava/lang/String;JLjava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    .line 26
    invoke-direct {p0, v0}, Lco/uk/getmondo/overdraft/a/c;->a(Lio/reactivex/v;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/d/w;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    iget-object v0, p0, Lco/uk/getmondo/overdraft/a/c;->b:Lco/uk/getmondo/overdraft/a/f;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/overdraft/a/f;->a(Ljava/lang/String;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method
