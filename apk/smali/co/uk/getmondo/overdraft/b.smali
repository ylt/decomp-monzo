.class public final Lco/uk/getmondo/overdraft/b;
.super Lco/uk/getmondo/common/ui/b;
.source "ChangeOverdraftLimitPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/overdraft/b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/overdraft/b$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0005\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0019B3\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0011H\u0002J\u0010\u0010\u0017\u001a\u00020\u00152\u0006\u0010\u0018\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u000eX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lco/uk/getmondo/overdraft/ChangeOverdraftLimitPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/overdraft/ChangeOverdraftLimitPresenter$View;",
        "ioScheduler",
        "Lio/reactivex/Scheduler;",
        "uiScheduler",
        "accountManager",
        "Lco/uk/getmondo/common/accounts/AccountManager;",
        "overdraftManager",
        "Lco/uk/getmondo/overdraft/data/OverdraftManager;",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/accounts/AccountManager;Lco/uk/getmondo/overdraft/data/OverdraftManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;)V",
        "limitIncrementAmount",
        "",
        "maxDaysInMonth",
        "selectedLimit",
        "Lco/uk/getmondo/model/Amount;",
        "status",
        "Lco/uk/getmondo/model/OverdraftStatus;",
        "handleNewLimit",
        "",
        "newLimit",
        "register",
        "view",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:J

.field private final d:J

.field private e:Lco/uk/getmondo/d/w;

.field private f:Lco/uk/getmondo/d/c;

.field private final g:Lio/reactivex/u;

.field private final h:Lio/reactivex/u;

.field private final i:Lco/uk/getmondo/common/accounts/b;

.field private final j:Lco/uk/getmondo/overdraft/a/c;

.field private final k:Lco/uk/getmondo/common/e/a;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/accounts/b;Lco/uk/getmondo/overdraft/a/c;Lco/uk/getmondo/common/e/a;)V
    .locals 2

    .prologue
    const-string v0, "ioScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "uiScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountManager"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "overdraftManager"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiErrorHandler"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/overdraft/b;->g:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/overdraft/b;->h:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/overdraft/b;->i:Lco/uk/getmondo/common/accounts/b;

    iput-object p4, p0, Lco/uk/getmondo/overdraft/b;->j:Lco/uk/getmondo/overdraft/a/c;

    iput-object p5, p0, Lco/uk/getmondo/overdraft/b;->k:Lco/uk/getmondo/common/e/a;

    .line 28
    const-wide/16 v0, 0x2710

    iput-wide v0, p0, Lco/uk/getmondo/overdraft/b;->c:J

    .line 29
    const-wide/16 v0, 0x1f

    iput-wide v0, p0, Lco/uk/getmondo/overdraft/b;->d:J

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/overdraft/b;)Lco/uk/getmondo/overdraft/a/c;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lco/uk/getmondo/overdraft/b;->j:Lco/uk/getmondo/overdraft/a/c;

    return-object v0
.end method

.method private final a(Lco/uk/getmondo/d/c;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 69
    iput-object p1, p0, Lco/uk/getmondo/overdraft/b;->f:Lco/uk/getmondo/d/c;

    .line 70
    iget-object v0, p0, Lco/uk/getmondo/overdraft/b;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/overdraft/b$a;

    invoke-interface {v0, p1}, Lco/uk/getmondo/overdraft/b$a;->a(Lco/uk/getmondo/d/c;)V

    .line 71
    iget-object v0, p0, Lco/uk/getmondo/overdraft/b;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/overdraft/b$a;

    invoke-virtual {p1}, Lco/uk/getmondo/d/c;->k()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_2

    move v1, v2

    :goto_0
    invoke-interface {v0, v1}, Lco/uk/getmondo/overdraft/b$a;->b(Z)V

    .line 72
    iget-object v0, p0, Lco/uk/getmondo/overdraft/b;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/overdraft/b$a;

    invoke-virtual {p1}, Lco/uk/getmondo/d/c;->k()J

    move-result-wide v4

    iget-object v1, p0, Lco/uk/getmondo/overdraft/b;->e:Lco/uk/getmondo/d/w;

    if-nez v1, :cond_0

    const-string v6, "status"

    invoke-static {v6}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Lco/uk/getmondo/d/w;->h()Lco/uk/getmondo/d/c;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/d/c;->k()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-gez v1, :cond_3

    :goto_1
    invoke-interface {v0, v2}, Lco/uk/getmondo/overdraft/b$a;->c(Z)V

    .line 73
    iget-object v0, p0, Lco/uk/getmondo/overdraft/b;->a:Lco/uk/getmondo/common/ui/f;

    check-cast v0, Lco/uk/getmondo/overdraft/b$a;

    iget-object v1, p0, Lco/uk/getmondo/overdraft/b;->e:Lco/uk/getmondo/d/w;

    if-nez v1, :cond_1

    const-string v2, "status"

    invoke-static {v2}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v1}, Lco/uk/getmondo/d/w;->g()Lco/uk/getmondo/d/c;

    move-result-object v1

    invoke-static {p1, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Lco/uk/getmondo/overdraft/b$a;->a(Z)V

    .line 75
    return-void

    :cond_2
    move v1, v3

    .line 71
    goto :goto_0

    :cond_3
    move v2, v3

    .line 72
    goto :goto_1
.end method

.method public static final synthetic a(Lco/uk/getmondo/overdraft/b;Lco/uk/getmondo/d/c;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lco/uk/getmondo/overdraft/b;->a(Lco/uk/getmondo/d/c;)V

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/overdraft/b;Lco/uk/getmondo/d/w;)V
    .locals 0

    .prologue
    .line 19
    iput-object p1, p0, Lco/uk/getmondo/overdraft/b;->e:Lco/uk/getmondo/d/w;

    return-void
.end method

.method public static final synthetic b(Lco/uk/getmondo/overdraft/b;)Lco/uk/getmondo/d/w;
    .locals 2

    .prologue
    .line 19
    iget-object v0, p0, Lco/uk/getmondo/overdraft/b;->e:Lco/uk/getmondo/d/w;

    if-nez v0, :cond_0

    const-string v1, "status"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/overdraft/b;)J
    .locals 2

    .prologue
    .line 19
    iget-wide v0, p0, Lco/uk/getmondo/overdraft/b;->d:J

    return-wide v0
.end method

.method public static final synthetic d(Lco/uk/getmondo/overdraft/b;)Lco/uk/getmondo/d/c;
    .locals 2

    .prologue
    .line 19
    iget-object v0, p0, Lco/uk/getmondo/overdraft/b;->f:Lco/uk/getmondo/d/c;

    if-nez v0, :cond_0

    const-string v1, "selectedLimit"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public static final synthetic e(Lco/uk/getmondo/overdraft/b;)J
    .locals 2

    .prologue
    .line 19
    iget-wide v0, p0, Lco/uk/getmondo/overdraft/b;->c:J

    return-wide v0
.end method

.method public static final synthetic f(Lco/uk/getmondo/overdraft/b;)Lco/uk/getmondo/common/accounts/b;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lco/uk/getmondo/overdraft/b;->i:Lco/uk/getmondo/common/accounts/b;

    return-object v0
.end method

.method public static final synthetic g(Lco/uk/getmondo/overdraft/b;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lco/uk/getmondo/overdraft/b;->g:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic h(Lco/uk/getmondo/overdraft/b;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lco/uk/getmondo/overdraft/b;->h:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic i(Lco/uk/getmondo/overdraft/b;)Lco/uk/getmondo/common/e/a;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lco/uk/getmondo/overdraft/b;->k:Lco/uk/getmondo/common/e/a;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Lco/uk/getmondo/overdraft/b$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/overdraft/b;->a(Lco/uk/getmondo/overdraft/b$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/overdraft/b$a;)V
    .locals 3

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 34
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 35
    iget-object v1, p0, Lco/uk/getmondo/overdraft/b;->b:Lio/reactivex/b/a;

    .line 37
    iget-object v0, p0, Lco/uk/getmondo/overdraft/b;->i:Lco/uk/getmondo/common/accounts/b;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/b;->e()Lio/reactivex/n;

    move-result-object v2

    .line 36
    new-instance v0, Lco/uk/getmondo/overdraft/b$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/overdraft/b$b;-><init>(Lco/uk/getmondo/overdraft/b;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->flatMap(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v2

    .line 37
    new-instance v0, Lco/uk/getmondo/overdraft/b$c;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/overdraft/b$c;-><init>(Lco/uk/getmondo/overdraft/b;Lco/uk/getmondo/overdraft/b$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "accountManager.accountId\u2026.limit)\n                }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/overdraft/b;->b:Lio/reactivex/b/a;

    .line 43
    iget-object v1, p0, Lco/uk/getmondo/overdraft/b;->b:Lio/reactivex/b/a;

    .line 45
    invoke-interface {p1}, Lco/uk/getmondo/overdraft/b$a;->a()Lio/reactivex/n;

    move-result-object v2

    .line 44
    new-instance v0, Lco/uk/getmondo/overdraft/b$d;

    invoke-direct {v0, p0}, Lco/uk/getmondo/overdraft/b$d;-><init>(Lco/uk/getmondo/overdraft/b;)V

    check-cast v0, Lio/reactivex/c/q;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v2

    .line 45
    new-instance v0, Lco/uk/getmondo/overdraft/b$e;

    invoke-direct {v0, p0}, Lco/uk/getmondo/overdraft/b$e;-><init>(Lco/uk/getmondo/overdraft/b;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.plusButtonClicks\n  \u2026+ limitIncrementAmount) }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/overdraft/b;->b:Lio/reactivex/b/a;

    .line 47
    iget-object v1, p0, Lco/uk/getmondo/overdraft/b;->b:Lio/reactivex/b/a;

    .line 49
    invoke-interface {p1}, Lco/uk/getmondo/overdraft/b$a;->b()Lio/reactivex/n;

    move-result-object v2

    .line 48
    new-instance v0, Lco/uk/getmondo/overdraft/b$f;

    invoke-direct {v0, p0}, Lco/uk/getmondo/overdraft/b$f;-><init>(Lco/uk/getmondo/overdraft/b;)V

    check-cast v0, Lio/reactivex/c/q;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v2

    .line 49
    new-instance v0, Lco/uk/getmondo/overdraft/b$g;

    invoke-direct {v0, p0}, Lco/uk/getmondo/overdraft/b$g;-><init>(Lco/uk/getmondo/overdraft/b;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.minusButtonClicks\n \u2026- limitIncrementAmount) }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/overdraft/b;->b:Lio/reactivex/b/a;

    .line 51
    iget-object v1, p0, Lco/uk/getmondo/overdraft/b;->b:Lio/reactivex/b/a;

    .line 52
    invoke-interface {p1}, Lco/uk/getmondo/overdraft/b$a;->c()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/overdraft/b$h;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/overdraft/b$h;-><init>(Lco/uk/getmondo/overdraft/b;Lco/uk/getmondo/overdraft/b$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.buttonConfirmationC\u2026onPrompt(selectedLimit) }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/overdraft/b;->b:Lio/reactivex/b/a;

    .line 54
    iget-object v1, p0, Lco/uk/getmondo/overdraft/b;->b:Lio/reactivex/b/a;

    .line 65
    invoke-interface {p1}, Lco/uk/getmondo/overdraft/b$a;->e()Lio/reactivex/n;

    move-result-object v2

    .line 55
    new-instance v0, Lco/uk/getmondo/overdraft/b$i;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/overdraft/b$i;-><init>(Lco/uk/getmondo/overdraft/b;Lco/uk/getmondo/overdraft/b$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v2

    .line 65
    new-instance v0, Lco/uk/getmondo/overdraft/b$j;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/overdraft/b$j;-><init>(Lco/uk/getmondo/overdraft/b;Lco/uk/getmondo/overdraft/b$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.dialogConfirmationC\u2026onScreen(selectedLimit) }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/overdraft/b;->b:Lio/reactivex/b/a;

    .line 66
    return-void
.end method
