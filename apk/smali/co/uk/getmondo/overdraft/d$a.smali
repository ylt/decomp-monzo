.class public final Lco/uk/getmondo/overdraft/d$a;
.super Ljava/lang/Object;
.source "ConfirmNewLimitDialogFragment.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/overdraft/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lco/uk/getmondo/overdraft/ConfirmNewLimitDialogFragment$Companion;",
        "",
        "()V",
        "ARG_LIMIT",
        "",
        "newInstance",
        "Landroid/support/v4/app/DialogFragment;",
        "newLimit",
        "Lco/uk/getmondo/model/Amount;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lco/uk/getmondo/overdraft/d$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/d/c;)Landroid/support/v4/app/i;
    .locals 3

    .prologue
    const-string v0, "newLimit"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    new-instance v0, Lco/uk/getmondo/overdraft/d;

    invoke-direct {v0}, Lco/uk/getmondo/overdraft/d;-><init>()V

    .line 37
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 38
    const-string v2, "ARG_LIMIT"

    check-cast p1, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 39
    invoke-virtual {v0, v1}, Lco/uk/getmondo/overdraft/d;->setArguments(Landroid/os/Bundle;)V

    .line 40
    check-cast v0, Landroid/support/v4/app/i;

    return-object v0
.end method
