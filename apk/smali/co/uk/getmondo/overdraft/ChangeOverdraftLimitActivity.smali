.class public final Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "ChangeOverdraftLimitActivity.kt"

# interfaces
.implements Lco/uk/getmondo/overdraft/b$a;
.implements Lco/uk/getmondo/overdraft/d$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000e\n\u0002\u0008\u0007\u0018\u0000 :2\u00020\u00012\u00020\u00022\u00020\u0003:\u0001:B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0018\u001a\u00020\u0007H\u0016J\"\u0010\u0019\u001a\u00020\u00072\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001b2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0014J\u0012\u0010\u001f\u001a\u00020\u00072\u0008\u0010 \u001a\u0004\u0018\u00010!H\u0014J\u0012\u0010\"\u001a\u00020#2\u0008\u0010$\u001a\u0004\u0018\u00010%H\u0016J\u0008\u0010&\u001a\u00020\u0007H\u0014J\u0008\u0010\'\u001a\u00020\u0007H\u0016J\u0010\u0010(\u001a\u00020#2\u0006\u0010)\u001a\u00020*H\u0016J\u0010\u0010+\u001a\u00020\u00072\u0006\u0010,\u001a\u00020-H\u0016J\u0010\u0010.\u001a\u00020\u00072\u0006\u0010/\u001a\u00020#H\u0016J\u0010\u00100\u001a\u00020\u00072\u0006\u0010/\u001a\u00020#H\u0016J\u0010\u00101\u001a\u00020\u00072\u0006\u0010/\u001a\u00020#H\u0016J \u00102\u001a\u00020\u00072\u0006\u00103\u001a\u0002042\u0006\u00105\u001a\u0002042\u0006\u00106\u001a\u000204H\u0016J\u0008\u00107\u001a\u00020\u0007H\u0016J\u0010\u00108\u001a\u00020\u00072\u0006\u0010,\u001a\u00020-H\u0016J\u0010\u00109\u001a\u00020\u00072\u0006\u0010,\u001a\u00020-H\u0016R\u001a\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\tR\u001e\u0010\n\u001a\u00020\u000b8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000c\u0010\r\"\u0004\u0008\u000e\u0010\u000fR\u001a\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0011X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u001a\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0015\u0010\tR\u001a\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0017\u0010\t\u00a8\u0006;"
    }
    d2 = {
        "Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;",
        "Lco/uk/getmondo/common/activities/BaseActivity;",
        "Lco/uk/getmondo/overdraft/ChangeOverdraftLimitPresenter$View;",
        "Lco/uk/getmondo/overdraft/ConfirmNewLimitDialogFragment$NewLimitConfirmationListener;",
        "()V",
        "buttonConfirmationClicks",
        "Lio/reactivex/Observable;",
        "",
        "getButtonConfirmationClicks",
        "()Lio/reactivex/Observable;",
        "changeOverdraftLimitPresenter",
        "Lco/uk/getmondo/overdraft/ChangeOverdraftLimitPresenter;",
        "getChangeOverdraftLimitPresenter",
        "()Lco/uk/getmondo/overdraft/ChangeOverdraftLimitPresenter;",
        "setChangeOverdraftLimitPresenter",
        "(Lco/uk/getmondo/overdraft/ChangeOverdraftLimitPresenter;)V",
        "dialogConfirmationClicks",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "getDialogConfirmationClicks",
        "()Lcom/jakewharton/rxrelay2/PublishRelay;",
        "minusButtonClicks",
        "getMinusButtonClicks",
        "plusButtonClicks",
        "getPlusButtonClicks",
        "hideLoading",
        "onActivityResult",
        "requestCode",
        "",
        "resultCode",
        "data",
        "Landroid/content/Intent;",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onCreateOptionsMenu",
        "",
        "menu",
        "Landroid/view/Menu;",
        "onDestroy",
        "onNewLimitConfirmed",
        "onOptionsItemSelected",
        "item",
        "Landroid/view/MenuItem;",
        "openOverdraftConfirmationScreen",
        "newLimit",
        "Lco/uk/getmondo/model/Amount;",
        "setConfirmationButtonEnabled",
        "enabled",
        "setMinusButtonEnabled",
        "setPlusButtonEnabled",
        "showDescription",
        "dailyFee",
        "",
        "buffer",
        "maxCharge",
        "showLoading",
        "showNewLimit",
        "showOverdraftConfirmationPrompt",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final b:Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity$a;

# The value of this static final field might be set in the static constructor
.field private static final e:Ljava/lang/String; = "CONFIRMATION_FRAGMENT_TAG"

# The value of this static final field might be set in the static constructor
.field private static final f:I = 0x1


# instance fields
.field public a:Lco/uk/getmondo/overdraft/b;

.field private final c:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    new-instance v0, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->b:Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity$a;

    .line 110
    const-string v0, "CONFIRMATION_FRAGMENT_TAG"

    sput-object v0, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->e:Ljava/lang/String;

    .line 111
    const/4 v0, 0x1

    sput v0, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->f:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 20
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 66
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    const-string v1, "PublishRelay.create<Unit>()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->c:Lcom/b/b/c;

    return-void
.end method

.method public static final synthetic i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->e:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic j()I
    .locals 1

    .prologue
    .line 20
    sget v0, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->f:I

    return v0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->g:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->g:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->g:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->g:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    sget v0, Lco/uk/getmondo/c$a;->plusButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 118
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    return-object v0
.end method

.method public a(Lco/uk/getmondo/d/c;)V
    .locals 1

    .prologue
    const-string v0, "newLimit"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    sget v0, Lco/uk/getmondo/c$a;->currentLimitView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/AmountView;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/ui/AmountView;->setAmount(Lco/uk/getmondo/d/c;)V

    .line 95
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const-string v0, "dailyFee"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buffer"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "maxCharge"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    sget v0, Lco/uk/getmondo/c$a;->changeLimitDescriptionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 78
    sget v0, Lco/uk/getmondo/c$a;->changeLimitDescriptionView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0a02da

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v3, 0x2

    aput-object p3, v2, v3

    invoke-virtual {p0, v1, v2}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 82
    sget v0, Lco/uk/getmondo/c$a;->confirmLimitButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/ui/ProgressButton;->setEnabled(Z)V

    .line 83
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    sget v0, Lco/uk/getmondo/c$a;->minusButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 119
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    return-object v0
.end method

.method public b(Lco/uk/getmondo/d/c;)V
    .locals 3

    .prologue
    const-string v0, "newLimit"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    sget-object v0, Lco/uk/getmondo/overdraft/d;->a:Lco/uk/getmondo/overdraft/d$a;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/overdraft/d$a;->a(Lco/uk/getmondo/d/c;)Landroid/support/v4/app/i;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    sget-object v2, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->b:Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity$a;

    invoke-static {v2}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity$a;->b(Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity$a;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/i;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 99
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 86
    sget v0, Lco/uk/getmondo/c$a;->minusButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 87
    return-void
.end method

.method public c()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    sget v0, Lco/uk/getmondo/c$a;->confirmLimitButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    .line 120
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    return-object v0
.end method

.method public c(Lco/uk/getmondo/d/c;)V
    .locals 6

    .prologue
    const-string v0, "newLimit"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    .line 102
    check-cast v0, Landroid/content/Context;

    const v1, 0x7f0a02d8

    invoke-virtual {p0, v1}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a02d9

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lco/uk/getmondo/d/c;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lco/uk/getmondo/common/activities/ConfirmationActivity;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->b:Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity$a;

    invoke-static {v1}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity$a;->a(Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity$a;)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 103
    return-void
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 90
    sget v0, Lco/uk/getmondo/c$a;->plusButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 91
    return-void
.end method

.method public d()Lcom/b/b/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/b/b/c",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->c:Lcom/b/b/c;

    return-object v0
.end method

.method public synthetic e()Lio/reactivex/n;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->d()Lcom/b/b/c;

    move-result-object v0

    check-cast v0, Lio/reactivex/n;

    return-object v0
.end method

.method public f()V
    .locals 2

    .prologue
    .line 69
    sget v0, Lco/uk/getmondo/c$a;->confirmLimitButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/ProgressButton;->setLoading(Z)V

    .line 70
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 73
    sget v0, Lco/uk/getmondo/c$a;->confirmLimitButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/ProgressButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/ProgressButton;->setLoading(Z)V

    .line 74
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 106
    invoke-virtual {p0}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->d()Lcom/b/b/c;

    move-result-object v0

    sget-object v1, Lkotlin/n;->a:Lkotlin/n;

    invoke-virtual {v0, v1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 107
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 47
    sget-object v0, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->b:Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity$a;

    invoke-static {v0}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity$a;->a(Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity$a;)I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 48
    invoke-virtual {p0}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->finish()V

    .line 49
    invoke-virtual {p0, v1, v1}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->overridePendingTransition(II)V

    .line 52
    :goto_0
    return-void

    .line 51
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lco/uk/getmondo/common/activities/b;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 25
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 26
    const v0, 0x7f050028

    invoke-virtual {p0, v0}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->setContentView(I)V

    .line 27
    invoke-virtual {p0}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;)V

    .line 28
    iget-object v0, p0, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->a:Lco/uk/getmondo/overdraft/b;

    if-nez v0, :cond_0

    const-string v1, "changeOverdraftLimitPresenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    check-cast p0, Lco/uk/getmondo/overdraft/b$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/overdraft/b;->a(Lco/uk/getmondo/overdraft/b$a;)V

    .line 29
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 32
    invoke-virtual {p0}, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f130004

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 33
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lco/uk/getmondo/overdraft/ChangeOverdraftLimitActivity;->a:Lco/uk/getmondo/overdraft/b;

    if-nez v0, :cond_0

    const-string v1, "changeOverdraftLimitPresenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/overdraft/b;->b()V

    .line 57
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 58
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f1104d2

    if-ne v0, v1, :cond_0

    .line 40
    const/4 v0, 0x1

    .line 37
    :goto_0
    return v0

    .line 42
    :cond_0
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
