.class public final Lco/uk/getmondo/payments/send/n;
.super Ljava/lang/Object;
.source "SendMoneyFragment_MembersInjector.java"

# interfaces
.implements Lb/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a",
        "<",
        "Lco/uk/getmondo/payments/send/SendMoneyFragment;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/o;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/SendMoneyAdapter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-class v0, Lco/uk/getmondo/payments/send/n;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/payments/send/n;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/o;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/SendMoneyAdapter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    sget-boolean v0, Lco/uk/getmondo/payments/send/n;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 24
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/payments/send/n;->b:Ljavax/a/a;

    .line 25
    sget-boolean v0, Lco/uk/getmondo/payments/send/n;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 26
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/payments/send/n;->c:Ljavax/a/a;

    .line 27
    sget-boolean v0, Lco/uk/getmondo/payments/send/n;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 28
    :cond_2
    iput-object p3, p0, Lco/uk/getmondo/payments/send/n;->d:Ljavax/a/a;

    .line 29
    return-void
.end method

.method public static a(Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/o;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/common/a;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/send/SendMoneyAdapter;",
            ">;)",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/payments/send/SendMoneyFragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    new-instance v0, Lco/uk/getmondo/payments/send/n;

    invoke-direct {v0, p0, p1, p2}, Lco/uk/getmondo/payments/send/n;-><init>(Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/payments/send/SendMoneyFragment;)V
    .locals 2

    .prologue
    .line 41
    if-nez p1, :cond_0

    .line 42
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Cannot inject members into a null reference"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/payments/send/n;->b:Ljavax/a/a;

    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/send/o;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/SendMoneyFragment;->a:Lco/uk/getmondo/payments/send/o;

    .line 45
    iget-object v0, p0, Lco/uk/getmondo/payments/send/n;->c:Ljavax/a/a;

    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/a;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/SendMoneyFragment;->c:Lco/uk/getmondo/common/a;

    .line 46
    iget-object v0, p0, Lco/uk/getmondo/payments/send/n;->d:Ljavax/a/a;

    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/SendMoneyFragment;->d:Lco/uk/getmondo/payments/send/SendMoneyAdapter;

    .line 47
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lco/uk/getmondo/payments/send/SendMoneyFragment;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/send/n;->a(Lco/uk/getmondo/payments/send/SendMoneyFragment;)V

    return-void
.end method
