.class Lco/uk/getmondo/payments/send/peer/j;
.super Lco/uk/getmondo/common/ui/b;
.source "PeerPaymentPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/payments/send/peer/j$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/payments/send/peer/j$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/common/e/a;

.field private final f:Lco/uk/getmondo/common/accounts/d;

.field private final g:Lco/uk/getmondo/d/aa;

.field private final h:Lco/uk/getmondo/d/c;

.field private final i:Ljava/lang/String;

.field private final j:Lco/uk/getmondo/a/a;

.field private k:Lco/uk/getmondo/d/c;


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/d/aa;Lco/uk/getmondo/d/c;Ljava/lang/String;Lco/uk/getmondo/a/a;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 42
    iput-object p1, p0, Lco/uk/getmondo/payments/send/peer/j;->c:Lio/reactivex/u;

    .line 43
    iput-object p2, p0, Lco/uk/getmondo/payments/send/peer/j;->d:Lio/reactivex/u;

    .line 44
    iput-object p3, p0, Lco/uk/getmondo/payments/send/peer/j;->e:Lco/uk/getmondo/common/e/a;

    .line 45
    iput-object p4, p0, Lco/uk/getmondo/payments/send/peer/j;->f:Lco/uk/getmondo/common/accounts/d;

    .line 46
    iput-object p5, p0, Lco/uk/getmondo/payments/send/peer/j;->g:Lco/uk/getmondo/d/aa;

    .line 47
    iput-object p6, p0, Lco/uk/getmondo/payments/send/peer/j;->h:Lco/uk/getmondo/d/c;

    .line 48
    iput-object p7, p0, Lco/uk/getmondo/payments/send/peer/j;->i:Ljava/lang/String;

    .line 49
    iput-object p8, p0, Lco/uk/getmondo/payments/send/peer/j;->j:Lco/uk/getmondo/a/a;

    .line 50
    return-void
.end method

.method static synthetic a()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 72
    const-string v0, "Balance synced"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ld/a/a;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/peer/j;Lco/uk/getmondo/payments/send/peer/j$a;Landroid/support/v4/g/j;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 83
    iget-object v0, p2, Landroid/support/v4/g/j;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    sget-object v1, Lco/uk/getmondo/common/i/c;->a:Lco/uk/getmondo/common/i/c;

    invoke-static {v0, v1}, Lco/uk/getmondo/d/c;->a(Ljava/lang/String;Lco/uk/getmondo/common/i/c;)Lco/uk/getmondo/d/c;

    move-result-object v1

    .line 84
    invoke-direct {p0, v1}, Lco/uk/getmondo/payments/send/peer/j;->a(Lco/uk/getmondo/d/c;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 86
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Balance string is not a valid number"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ld/a/a;->a(Ljava/lang/Throwable;)V

    .line 96
    :goto_0
    return-void

    .line 90
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/payments/send/peer/j;->k:Lco/uk/getmondo/d/c;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lco/uk/getmondo/payments/send/peer/j;->k:Lco/uk/getmondo/d/c;

    .line 91
    invoke-virtual {v0}, Lco/uk/getmondo/d/c;->a()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v1}, Lco/uk/getmondo/d/c;->k()J

    move-result-wide v2

    iget-object v0, p0, Lco/uk/getmondo/payments/send/peer/j;->k:Lco/uk/getmondo/d/c;

    invoke-virtual {v0}, Lco/uk/getmondo/d/c;->k()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    .line 92
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/payments/send/peer/j;->k:Lco/uk/getmondo/d/c;

    invoke-interface {p1, v0}, Lco/uk/getmondo/payments/send/peer/j$a;->b(Lco/uk/getmondo/d/c;)V

    goto :goto_0

    .line 94
    :cond_2
    new-instance v2, Lco/uk/getmondo/payments/send/data/a/g;

    iget-object v0, p2, Landroid/support/v4/g/j;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v3, p0, Lco/uk/getmondo/payments/send/peer/j;->g:Lco/uk/getmondo/d/aa;

    invoke-direct {v2, v1, v0, v3}, Lco/uk/getmondo/payments/send/data/a/g;-><init>(Lco/uk/getmondo/d/c;Ljava/lang/String;Lco/uk/getmondo/d/aa;)V

    invoke-interface {p1, v2}, Lco/uk/getmondo/payments/send/peer/j$a;->a(Lco/uk/getmondo/payments/send/data/a/g;)V

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/peer/j;Lco/uk/getmondo/payments/send/peer/j$a;Lco/uk/getmondo/d/c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 65
    iput-object p2, p0, Lco/uk/getmondo/payments/send/peer/j;->k:Lco/uk/getmondo/d/c;

    .line 66
    invoke-interface {p1, p2}, Lco/uk/getmondo/payments/send/peer/j$a;->a(Lco/uk/getmondo/d/c;)V

    .line 67
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/peer/j;Lco/uk/getmondo/payments/send/peer/j$a;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 77
    sget-object v0, Lco/uk/getmondo/common/i/c;->a:Lco/uk/getmondo/common/i/c;

    invoke-static {p2, v0}, Lco/uk/getmondo/d/c;->a(Ljava/lang/String;Lco/uk/getmondo/common/i/c;)Lco/uk/getmondo/d/c;

    move-result-object v0

    .line 78
    invoke-direct {p0, v0}, Lco/uk/getmondo/payments/send/peer/j;->a(Lco/uk/getmondo/d/c;)Z

    move-result v0

    invoke-interface {p1, v0}, Lco/uk/getmondo/payments/send/peer/j$a;->a(Z)V

    .line 79
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/peer/j;Lco/uk/getmondo/payments/send/peer/j$a;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lco/uk/getmondo/payments/send/peer/j;->e:Lco/uk/getmondo/common/e/a;

    invoke-virtual {v0, p2, p1}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    return-void
.end method

.method private a(Lco/uk/getmondo/d/c;)Z
    .locals 4

    .prologue
    .line 100
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lco/uk/getmondo/d/c;->f()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 25
    check-cast p1, Lco/uk/getmondo/payments/send/peer/j$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/send/peer/j;->a(Lco/uk/getmondo/payments/send/peer/j$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/payments/send/peer/j$a;)V
    .locals 4

    .prologue
    .line 54
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 56
    iget-object v0, p0, Lco/uk/getmondo/payments/send/peer/j;->g:Lco/uk/getmondo/d/aa;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lco/uk/getmondo/payments/send/peer/j$a;->a(Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lco/uk/getmondo/payments/send/peer/j;->h:Lco/uk/getmondo/d/c;

    iget-object v1, p0, Lco/uk/getmondo/payments/send/peer/j;->i:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lco/uk/getmondo/payments/send/peer/j$a;->a(Lco/uk/getmondo/d/c;Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lco/uk/getmondo/payments/send/peer/j;->f:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->c()Lco/uk/getmondo/d/a;

    move-result-object v0

    invoke-interface {v0}, Lco/uk/getmondo/d/a;->a()Ljava/lang/String;

    move-result-object v0

    .line 61
    iget-object v1, p0, Lco/uk/getmondo/payments/send/peer/j;->j:Lco/uk/getmondo/a/a;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/a/a;->a(Ljava/lang/String;)Lio/reactivex/n;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/payments/send/peer/j;->c:Lio/reactivex/u;

    .line 62
    invoke-virtual {v1, v2}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/payments/send/peer/k;->a()Lio/reactivex/c/h;

    move-result-object v2

    .line 63
    invoke-virtual {v1, v2}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    invoke-static {p0, p1}, Lco/uk/getmondo/payments/send/peer/l;->a(Lco/uk/getmondo/payments/send/peer/j;Lco/uk/getmondo/payments/send/peer/j$a;)Lio/reactivex/c/g;

    move-result-object v2

    invoke-static {}, Lco/uk/getmondo/payments/send/peer/m;->a()Lio/reactivex/c/g;

    move-result-object v3

    .line 64
    invoke-virtual {v1, v2, v3}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v1

    .line 61
    invoke-virtual {p0, v1}, Lco/uk/getmondo/payments/send/peer/j;->a(Lio/reactivex/b/b;)V

    .line 69
    iget-object v1, p0, Lco/uk/getmondo/payments/send/peer/j;->j:Lco/uk/getmondo/a/a;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/a/a;->b(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/payments/send/peer/j;->d:Lio/reactivex/u;

    .line 70
    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/payments/send/peer/j;->c:Lio/reactivex/u;

    .line 71
    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/payments/send/peer/n;->b()Lio/reactivex/c/a;

    move-result-object v1

    invoke-static {p0, p1}, Lco/uk/getmondo/payments/send/peer/o;->a(Lco/uk/getmondo/payments/send/peer/j;Lco/uk/getmondo/payments/send/peer/j$a;)Lio/reactivex/c/g;

    move-result-object v2

    .line 72
    invoke-virtual {v0, v1, v2}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 69
    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/peer/j;->a(Lio/reactivex/b/b;)V

    .line 75
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/peer/j$a;->a()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/payments/send/peer/p;->a(Lco/uk/getmondo/payments/send/peer/j;Lco/uk/getmondo/payments/send/peer/j$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 76
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 75
    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/peer/j;->a(Lio/reactivex/b/b;)V

    .line 81
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/peer/j$a;->b()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/payments/send/peer/q;->a(Lco/uk/getmondo/payments/send/peer/j;Lco/uk/getmondo/payments/send/peer/j$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 82
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 81
    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/peer/j;->a(Lio/reactivex/b/b;)V

    .line 97
    return-void
.end method
