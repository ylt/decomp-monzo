.class public Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "PeerPaymentActivity.java"

# interfaces
.implements Lco/uk/getmondo/payments/send/peer/j$a;


# instance fields
.field a:Lco/uk/getmondo/payments/send/peer/j;

.field amountInputView:Lco/uk/getmondo/common/ui/AmountInputView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101df
    .end annotation
.end field

.field b:Lco/uk/getmondo/common/a;

.field private final c:Landroid/os/Handler;

.field notesEditText:Landroid/widget/EditText;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101e0
    .end annotation
.end field

.field sendMoneyButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101e1
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 41
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;->c:Landroid/os/Handler;

    return-void
.end method

.method public static a(Landroid/content/Context;Lco/uk/getmondo/d/aa;Lco/uk/getmondo/api/model/tracking/Impression$PaymentFlowFrom;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 59
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "key_peer"

    .line 60
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "key_entry_point"

    .line 61
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    .line 59
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lco/uk/getmondo/payments/send/data/a/g;Lco/uk/getmondo/api/model/tracking/Impression$PaymentFlowFrom;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 51
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "key_peer"

    .line 52
    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/data/a/g;->c()Lco/uk/getmondo/d/aa;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "key_notes"

    .line 53
    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/data/a/g;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "key_amount"

    .line 54
    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/data/a/g;->a()Lco/uk/getmondo/d/c;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "key_entry_point"

    .line 55
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    .line 51
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;Ljava/lang/Object;)Landroid/support/v4/g/j;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 132
    iget-object v0, p0, Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;->amountInputView:Lco/uk/getmondo/common/ui/AmountInputView;

    invoke-virtual {v0}, Lco/uk/getmondo/common/ui/AmountInputView;->getAmountText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;->notesEditText:Landroid/widget/EditText;

    .line 133
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 132
    invoke-static {v0, v1}, Landroid/support/v4/g/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Landroid/support/v4/g/j;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;Landroid/support/v7/app/a;Lco/uk/getmondo/d/c;)V
    .locals 4

    .prologue
    .line 94
    const v0, 0x7f0a00e5

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p2}, Lco/uk/getmondo/d/c;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v7/app/a;->b(Ljava/lang/CharSequence;)V

    .line 95
    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    iget-object v0, p0, Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;->amountInputView:Lco/uk/getmondo/common/ui/AmountInputView;

    invoke-virtual {v0}, Lco/uk/getmondo/common/ui/AmountInputView;->a()Lio/reactivex/n;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/payments/send/peer/b;->a()Lio/reactivex/c/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public a(Lco/uk/getmondo/d/c;)V
    .locals 2

    .prologue
    .line 90
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;->getSupportActionBar()Landroid/support/v7/app/a;

    move-result-object v0

    .line 91
    if-eqz v0, :cond_0

    .line 93
    iget-object v1, p0, Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;->c:Landroid/os/Handler;

    invoke-static {p0, v0, p1}, Lco/uk/getmondo/payments/send/peer/a;->a(Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;Landroid/support/v7/app/a;Lco/uk/getmondo/d/c;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 97
    :cond_0
    return-void
.end method

.method public a(Lco/uk/getmondo/d/c;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 101
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lco/uk/getmondo/d/c;->k()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;->amountInputView:Lco/uk/getmondo/common/ui/AmountInputView;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/ui/AmountInputView;->setDefaultAmount(Lco/uk/getmondo/d/c;)V

    .line 104
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;->notesEditText:Landroid/widget/EditText;

    invoke-virtual {v0, p2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 105
    return-void
.end method

.method public a(Lco/uk/getmondo/payments/send/data/a/g;)V
    .locals 1

    .prologue
    .line 109
    invoke-static {p0, p1}, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->a(Landroid/content/Context;Lco/uk/getmondo/payments/send/data/a/f;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;->startActivity(Landroid/content/Intent;)V

    .line 110
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 138
    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 139
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;->sendMoneyButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 122
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Landroid/support/v4/g/j",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 131
    iget-object v0, p0, Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;->sendMoneyButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/payments/send/peer/c;->a(Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;)Lio/reactivex/c/h;

    move-result-object v1

    .line 132
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 131
    return-object v0
.end method

.method public b(Lco/uk/getmondo/d/c;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 114
    const v0, 0x7f0a02e7

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a02e8

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    .line 115
    invoke-virtual {p1}, Lco/uk/getmondo/d/c;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 114
    invoke-static {v0, v1, v4}, Lco/uk/getmondo/common/d/a;->a(Ljava/lang/String;Ljava/lang/String;Z)Lco/uk/getmondo/common/d/a;

    move-result-object v0

    .line 116
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    const-string v2, "ERROR_FRAGMENT_TAG"

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/common/d/a;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 117
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 66
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 68
    const v0, 0x7f050052

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;->setContentView(I)V

    .line 69
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 71
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "key_peer"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/aa;

    .line 72
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "key_amount"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/d/c;

    .line 73
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "key_notes"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 74
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v3

    new-instance v4, Lco/uk/getmondo/payments/send/peer/f;

    invoke-direct {v4, v0, v1, v2}, Lco/uk/getmondo/payments/send/peer/f;-><init>(Lco/uk/getmondo/d/aa;Lco/uk/getmondo/d/c;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/payments/send/peer/f;)Lco/uk/getmondo/payments/send/peer/e;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/payments/send/peer/e;->a(Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;)V

    .line 75
    iget-object v0, p0, Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;->a:Lco/uk/getmondo/payments/send/peer/j;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/payments/send/peer/j;->a(Lco/uk/getmondo/payments/send/peer/j$a;)V

    .line 77
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "key_entry_point"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/tracking/Impression$PaymentFlowFrom;

    .line 78
    iget-object v1, p0, Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;->b:Lco/uk/getmondo/common/a;

    invoke-static {v0}, Lco/uk/getmondo/api/model/tracking/Impression;->a(Lco/uk/getmondo/api/model/tracking/Impression$PaymentFlowFrom;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 79
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;->a:Lco/uk/getmondo/payments/send/peer/j;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/peer/j;->b()V

    .line 85
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 86
    return-void
.end method
