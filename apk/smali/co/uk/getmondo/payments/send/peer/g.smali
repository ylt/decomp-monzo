.class public final Lco/uk/getmondo/payments/send/peer/g;
.super Ljava/lang/Object;
.source "PeerPaymentModule_ProvideAmountFactory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/d/c;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lco/uk/getmondo/payments/send/peer/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-class v0, Lco/uk/getmondo/payments/send/peer/g;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/payments/send/peer/g;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lco/uk/getmondo/payments/send/peer/f;)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    sget-boolean v0, Lco/uk/getmondo/payments/send/peer/g;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 17
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/payments/send/peer/g;->b:Lco/uk/getmondo/payments/send/peer/f;

    .line 18
    return-void
.end method

.method public static a(Lco/uk/getmondo/payments/send/peer/f;)Lb/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/payments/send/peer/f;",
            ")",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/d/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    new-instance v0, Lco/uk/getmondo/payments/send/peer/g;

    invoke-direct {v0, p0}, Lco/uk/getmondo/payments/send/peer/g;-><init>(Lco/uk/getmondo/payments/send/peer/f;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/d/c;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lco/uk/getmondo/payments/send/peer/g;->b:Lco/uk/getmondo/payments/send/peer/f;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/peer/f;->b()Lco/uk/getmondo/d/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/peer/g;->a()Lco/uk/getmondo/d/c;

    move-result-object v0

    return-object v0
.end method
