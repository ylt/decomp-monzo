.class final Lco/uk/getmondo/payments/send/o$q$2;
.super Ljava/lang/Object;
.source "SendMoneyPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/payments/send/o$q;->a(Ljava/util/List;)Lio/reactivex/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/z",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010!\n\u0002\u0010\u000e\n\u0002\u0010 \n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012(\u0010\u0004\u001a$\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00060\u0006 \u0003*\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00060\u00060\u00070\u0005H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lco/uk/getmondo/api/model/ApiContactDiscovery;",
        "kotlin.jvm.PlatformType",
        "it",
        "",
        "",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/payments/send/o$q;


# direct methods
.method constructor <init>(Lco/uk/getmondo/payments/send/o$q;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/send/o$q$2;->a:Lco/uk/getmondo/payments/send/o$q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/ApiContactDiscovery;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lco/uk/getmondo/payments/send/o$q$2;->a:Lco/uk/getmondo/payments/send/o$q;

    iget-object v0, v0, Lco/uk/getmondo/payments/send/o$q;->a:Lco/uk/getmondo/payments/send/o;

    invoke-static {v0}, Lco/uk/getmondo/payments/send/o;->c(Lco/uk/getmondo/payments/send/o;)Lco/uk/getmondo/api/MonzoApi;

    move-result-object v0

    invoke-interface {v0, p1}, Lco/uk/getmondo/api/MonzoApi;->queryContacts(Ljava/util/List;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 35
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/send/o$q$2;->a(Ljava/util/List;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method
