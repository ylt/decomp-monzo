.class public final Lco/uk/getmondo/payments/send/o;
.super Lco/uk/getmondo/common/ui/b;
.source "SendMoneyPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/payments/send/o$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/payments/send/o$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0084\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001.Bk\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u00a2\u0006\u0002\u0010\u001aJB\u0010\u001d\u001a\u001a\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020 0\u001f\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020!0\u001f0\u001e2\u0018\u0010\"\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u00020$0\u001e0#2\u0006\u0010\u0010\u001a\u00020%H\u0002J\u0010\u0010&\u001a\u00020$2\u0006\u0010\'\u001a\u00020$H\u0002J\u0012\u0010(\u001a\u00020$2\u0008\u0010)\u001a\u0004\u0018\u00010$H\u0002J\u0010\u0010*\u001a\u00020$2\u0006\u0010\'\u001a\u00020$H\u0002J\u0010\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020\u0002H\u0016R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006/"
    }
    d2 = {
        "Lco/uk/getmondo/payments/send/SendMoneyPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/payments/send/SendMoneyPresenter$View;",
        "uiScheduler",
        "Lio/reactivex/Scheduler;",
        "ioScheduler",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "accountService",
        "Lco/uk/getmondo/common/accounts/AccountService;",
        "hashSHA256",
        "Lco/uk/getmondo/common/utils/HashSHA256;",
        "analyticsService",
        "Lco/uk/getmondo/common/AnalyticsService;",
        "monzoApi",
        "Lco/uk/getmondo/api/MonzoApi;",
        "contacts",
        "Lco/uk/getmondo/payments/send/contacts/RxContacts;",
        "peerToPeerRepository",
        "Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;",
        "paymentsManager",
        "Lco/uk/getmondo/payments/send/data/PaymentsManager;",
        "accountManager",
        "Lco/uk/getmondo/common/accounts/AccountManager;",
        "featureFlagsStorage",
        "Lco/uk/getmondo/common/FeatureFlagsStorage;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/common/accounts/AccountService;Lco/uk/getmondo/common/utils/HashSHA256;Lco/uk/getmondo/common/AnalyticsService;Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/payments/send/contacts/RxContacts;Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;Lco/uk/getmondo/payments/send/data/PaymentsManager;Lco/uk/getmondo/common/accounts/AccountManager;Lco/uk/getmondo/common/FeatureFlagsStorage;)V",
        "contactNameComparator",
        "Lco/uk/getmondo/payments/send/contacts/ContactNameComparator;",
        "findContactsOnMonzo",
        "Lkotlin/Pair;",
        "",
        "Lco/uk/getmondo/payments/send/contacts/Contact;",
        "Lco/uk/getmondo/payments/send/contacts/ContactWithMultipleNumbers;",
        "pairs",
        "",
        "",
        "Lco/uk/getmondo/api/model/ApiContactDiscovery;",
        "firstPartOfHashedNumber",
        "number",
        "hash",
        "toHash",
        "nextDigitsOfHashedNumber",
        "register",
        "",
        "view",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Lco/uk/getmondo/payments/send/a/c;

.field private final d:Lio/reactivex/u;

.field private final e:Lio/reactivex/u;

.field private final f:Lco/uk/getmondo/common/e/a;

.field private final g:Lco/uk/getmondo/common/accounts/d;

.field private final h:Lco/uk/getmondo/common/k/f;

.field private final i:Lco/uk/getmondo/common/a;

.field private final j:Lco/uk/getmondo/api/MonzoApi;

.field private final k:Lco/uk/getmondo/payments/send/a/e;

.field private final l:Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;

.field private final m:Lco/uk/getmondo/payments/send/data/a;

.field private final n:Lco/uk/getmondo/common/accounts/b;

.field private final o:Lco/uk/getmondo/common/o;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/common/k/f;Lco/uk/getmondo/common/a;Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/payments/send/a/e;Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;Lco/uk/getmondo/payments/send/data/a;Lco/uk/getmondo/common/accounts/b;Lco/uk/getmondo/common/o;)V
    .locals 1

    .prologue
    const-string v0, "uiScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ioScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiErrorHandler"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountService"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hashSHA256"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsService"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "monzoApi"

    invoke-static {p7, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contacts"

    invoke-static {p8, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "peerToPeerRepository"

    invoke-static {p9, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentsManager"

    invoke-static {p10, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountManager"

    invoke-static {p11, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "featureFlagsStorage"

    invoke-static {p12, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/payments/send/o;->d:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/payments/send/o;->e:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/payments/send/o;->f:Lco/uk/getmondo/common/e/a;

    iput-object p4, p0, Lco/uk/getmondo/payments/send/o;->g:Lco/uk/getmondo/common/accounts/d;

    iput-object p5, p0, Lco/uk/getmondo/payments/send/o;->h:Lco/uk/getmondo/common/k/f;

    iput-object p6, p0, Lco/uk/getmondo/payments/send/o;->i:Lco/uk/getmondo/common/a;

    iput-object p7, p0, Lco/uk/getmondo/payments/send/o;->j:Lco/uk/getmondo/api/MonzoApi;

    iput-object p8, p0, Lco/uk/getmondo/payments/send/o;->k:Lco/uk/getmondo/payments/send/a/e;

    iput-object p9, p0, Lco/uk/getmondo/payments/send/o;->l:Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;

    iput-object p10, p0, Lco/uk/getmondo/payments/send/o;->m:Lco/uk/getmondo/payments/send/data/a;

    iput-object p11, p0, Lco/uk/getmondo/payments/send/o;->n:Lco/uk/getmondo/common/accounts/b;

    iput-object p12, p0, Lco/uk/getmondo/payments/send/o;->o:Lco/uk/getmondo/common/o;

    .line 49
    new-instance v0, Lco/uk/getmondo/payments/send/a/c;

    invoke-direct {v0}, Lco/uk/getmondo/payments/send/a/c;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/payments/send/o;->c:Lco/uk/getmondo/payments/send/a/c;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/payments/send/o;)Lco/uk/getmondo/payments/send/a/e;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lco/uk/getmondo/payments/send/o;->k:Lco/uk/getmondo/payments/send/a/e;

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/payments/send/o;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lco/uk/getmondo/payments/send/o;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 209
    const/4 v0, 0x5

    const/16 v1, 0xc

    if-nez p1, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/payments/send/o;Ljava/util/List;Lco/uk/getmondo/api/model/ApiContactDiscovery;)Lkotlin/h;
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/payments/send/o;->a(Ljava/util/List;Lco/uk/getmondo/api/model/ApiContactDiscovery;)Lkotlin/h;

    move-result-object v0

    return-object v0
.end method

.method private final a(Ljava/util/List;Lco/uk/getmondo/api/model/ApiContactDiscovery;)Lkotlin/h;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lkotlin/h",
            "<",
            "Lco/uk/getmondo/payments/send/a/b;",
            "Ljava/lang/String;",
            ">;>;",
            "Lco/uk/getmondo/api/model/ApiContactDiscovery;",
            ")",
            "Lkotlin/h",
            "<",
            "Ljava/util/Collection",
            "<",
            "Lco/uk/getmondo/payments/send/a/b;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lco/uk/getmondo/payments/send/a/d;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 167
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 168
    new-instance v8, Landroid/support/v4/g/a;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v8, v0}, Landroid/support/v4/g/a;-><init>(I)V

    .line 169
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/h;

    .line 170
    const/4 v3, 0x0

    .line 171
    invoke-virtual {v0}, Lkotlin/h;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {p0, v1}, Lco/uk/getmondo/payments/send/o;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 175
    invoke-virtual {p2}, Lco/uk/getmondo/api/model/ApiContactDiscovery;->a()Ljava/util/Map;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-virtual {p2}, Lco/uk/getmondo/api/model/ApiContactDiscovery;->a()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 176
    invoke-virtual {p2}, Lco/uk/getmondo/api/model/ApiContactDiscovery;->a()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_1
    check-cast v1, Ljava/util/List;

    .line 177
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 178
    invoke-virtual {v0}, Lkotlin/h;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {p0, v2}, Lco/uk/getmondo/payments/send/o;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 179
    invoke-static {v2, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 180
    const/4 v1, 0x1

    .line 186
    :goto_1
    invoke-virtual {v0}, Lkotlin/h;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/send/a/b;

    .line 187
    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/a/b;->a()J

    move-result-wide v2

    .line 188
    if-eqz v1, :cond_3

    .line 189
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 191
    :cond_3
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/support/v4/g/a;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 192
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/support/v4/g/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/payments/send/a/d;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lco/uk/getmondo/payments/send/a/d;->f()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/a/b;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_4
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 194
    :cond_5
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 195
    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/a/b;->c()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_6

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_6
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 196
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    new-instance v1, Lco/uk/getmondo/payments/send/a/d;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/a/b;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/a/b;->e()Ljava/lang/String;

    move-result-object v5

    check-cast v6, Ljava/util/List;

    invoke-direct/range {v1 .. v6}, Lco/uk/getmondo/payments/send/a/d;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {v8, v10, v1}, Landroid/support/v4/g/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_7
    move-object v0, v7

    .line 200
    check-cast v0, Ljava/util/List;

    iget-object v1, p0, Lco/uk/getmondo/payments/send/o;->c:Lco/uk/getmondo/payments/send/a/c;

    check-cast v1, Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 202
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v8}, Landroid/support/v4/g/a;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object v0, v1

    .line 203
    check-cast v0, Ljava/util/List;

    iget-object v2, p0, Lco/uk/getmondo/payments/send/o;->c:Lco/uk/getmondo/payments/send/a/c;

    check-cast v2, Ljava/util/Comparator;

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 205
    new-instance v0, Lkotlin/h;

    invoke-direct {v0, v7, v1}, Lkotlin/h;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    :cond_8
    move v1, v3

    goto/16 :goto_1
.end method

.method public static final synthetic b(Lco/uk/getmondo/payments/send/o;)Lco/uk/getmondo/common/accounts/d;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lco/uk/getmondo/payments/send/o;->g:Lco/uk/getmondo/common/accounts/d;

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/payments/send/o;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lco/uk/getmondo/payments/send/o;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 213
    const/4 v0, 0x0

    const/4 v1, 0x5

    if-nez p1, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/payments/send/o;)Lco/uk/getmondo/api/MonzoApi;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lco/uk/getmondo/payments/send/o;->j:Lco/uk/getmondo/api/MonzoApi;

    return-object v0
.end method

.method private final c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 217
    nop

    .line 218
    :try_start_0
    iget-object v0, p0, Lco/uk/getmondo/payments/send/o;->h:Lco/uk/getmondo/common/k/f;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/k/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "hashSHA256.hash(toHash)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    .line 217
    :goto_0
    return-object v0

    .line 219
    :catch_0
    move-exception v0

    .line 220
    const-string v0, ""

    goto :goto_0
.end method

.method public static final synthetic d(Lco/uk/getmondo/payments/send/o;)Lco/uk/getmondo/common/a;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lco/uk/getmondo/payments/send/o;->i:Lco/uk/getmondo/common/a;

    return-object v0
.end method

.method public static final synthetic e(Lco/uk/getmondo/payments/send/o;)Lco/uk/getmondo/common/e/a;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lco/uk/getmondo/payments/send/o;->f:Lco/uk/getmondo/common/e/a;

    return-object v0
.end method

.method public static final synthetic f(Lco/uk/getmondo/payments/send/o;)Lco/uk/getmondo/common/accounts/b;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lco/uk/getmondo/payments/send/o;->n:Lco/uk/getmondo/common/accounts/b;

    return-object v0
.end method

.method public static final synthetic g(Lco/uk/getmondo/payments/send/o;)Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lco/uk/getmondo/payments/send/o;->l:Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;

    return-object v0
.end method

.method public static final synthetic h(Lco/uk/getmondo/payments/send/o;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lco/uk/getmondo/payments/send/o;->e:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic i(Lco/uk/getmondo/payments/send/o;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lco/uk/getmondo/payments/send/o;->d:Lio/reactivex/u;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 35
    check-cast p1, Lco/uk/getmondo/payments/send/o$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/send/o;->a(Lco/uk/getmondo/payments/send/o$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/payments/send/o$a;)V
    .locals 4

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 52
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 54
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/o$a;->b()Lio/reactivex/n;

    move-result-object v1

    .line 55
    new-instance v0, Lco/uk/getmondo/payments/send/o$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/payments/send/o$b;-><init>(Lco/uk/getmondo/payments/send/o;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    .line 57
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/o$a;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    sget-object v0, Lkotlin/n;->a:Lkotlin/n;

    invoke-static {v0}, Lio/reactivex/n;->just(Ljava/lang/Object;)Lio/reactivex/n;

    move-result-object v0

    .line 63
    :goto_0
    iget-object v2, p0, Lco/uk/getmondo/payments/send/o;->b:Lio/reactivex/b/a;

    .line 87
    check-cast v0, Lio/reactivex/r;

    invoke-interface {p1}, Lco/uk/getmondo/payments/send/o$a;->g()Lio/reactivex/n;

    move-result-object v1

    check-cast v1, Lio/reactivex/r;

    invoke-static {v0, v1}, Lio/reactivex/n;->merge(Lio/reactivex/r;Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v1

    .line 64
    new-instance v0, Lco/uk/getmondo/payments/send/o$m;

    invoke-direct {v0, p0}, Lco/uk/getmondo/payments/send/o$m;-><init>(Lco/uk/getmondo/payments/send/o;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->switchMap(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 65
    iget-object v1, p0, Lco/uk/getmondo/payments/send/o;->d:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v1

    .line 66
    new-instance v0, Lco/uk/getmondo/payments/send/o$n;

    invoke-direct {v0, p1}, Lco/uk/getmondo/payments/send/o$n;-><init>(Lco/uk/getmondo/payments/send/o$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v0

    .line 67
    iget-object v1, p0, Lco/uk/getmondo/payments/send/o;->e:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v1

    .line 68
    new-instance v0, Lco/uk/getmondo/payments/send/o$o;

    invoke-direct {v0, p0}, Lco/uk/getmondo/payments/send/o$o;-><init>(Lco/uk/getmondo/payments/send/o;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v1

    .line 73
    new-instance v0, Lco/uk/getmondo/payments/send/o$p;

    invoke-direct {v0, p0}, Lco/uk/getmondo/payments/send/o$p;-><init>(Lco/uk/getmondo/payments/send/o;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapSingle(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    .line 78
    new-instance v0, Lco/uk/getmondo/payments/send/o$q;

    invoke-direct {v0, p0}, Lco/uk/getmondo/payments/send/o$q;-><init>(Lco/uk/getmondo/payments/send/o;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapSingle(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 86
    iget-object v1, p0, Lco/uk/getmondo/payments/send/o;->d:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v3

    .line 87
    new-instance v0, Lco/uk/getmondo/payments/send/o$r;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/payments/send/o$r;-><init>(Lco/uk/getmondo/payments/send/o;Lco/uk/getmondo/payments/send/o$a;)V

    check-cast v0, Lio/reactivex/c/g;

    .line 91
    new-instance v1, Lco/uk/getmondo/payments/send/o$s;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/payments/send/o$s;-><init>(Lco/uk/getmondo/payments/send/o;Lco/uk/getmondo/payments/send/o$a;)V

    check-cast v1, Lio/reactivex/c/g;

    .line 87
    invoke-virtual {v3, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "Observable.merge(permiss\u2026 view)\n                })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/o;->b:Lio/reactivex/b/a;

    .line 96
    iget-object v0, p0, Lco/uk/getmondo/payments/send/o;->n:Lco/uk/getmondo/common/accounts/b;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/b;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 97
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/o$a;->h()V

    .line 98
    iget-object v2, p0, Lco/uk/getmondo/payments/send/o;->b:Lio/reactivex/b/a;

    .line 102
    iget-object v0, p0, Lco/uk/getmondo/payments/send/o;->m:Lco/uk/getmondo/payments/send/data/a;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/data/a;->a()Lio/reactivex/n;

    move-result-object v1

    .line 99
    new-instance v0, Lco/uk/getmondo/payments/send/o$t;

    invoke-direct {v0, p0}, Lco/uk/getmondo/payments/send/o$t;-><init>(Lco/uk/getmondo/payments/send/o;)V

    check-cast v0, Lio/reactivex/c/q;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v0

    .line 100
    iget-object v1, p0, Lco/uk/getmondo/payments/send/o;->e:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v0

    .line 101
    iget-object v1, p0, Lco/uk/getmondo/payments/send/o;->d:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v3

    .line 102
    new-instance v0, Lco/uk/getmondo/payments/send/o$c;

    invoke-direct {v0, p1}, Lco/uk/getmondo/payments/send/o$c;-><init>(Lco/uk/getmondo/payments/send/o$a;)V

    check-cast v0, Lio/reactivex/c/g;

    new-instance v1, Lco/uk/getmondo/payments/send/o$d;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/payments/send/o$d;-><init>(Lco/uk/getmondo/payments/send/o;Lco/uk/getmondo/payments/send/o$a;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v3, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "paymentsManager.recentPa\u2026ndleError(error, view) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/o;->b:Lio/reactivex/b/a;

    .line 104
    iget-object v1, p0, Lco/uk/getmondo/payments/send/o;->b:Lio/reactivex/b/a;

    .line 105
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/o$a;->f()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/payments/send/o$e;

    invoke-direct {v0, p1}, Lco/uk/getmondo/payments/send/o$e;-><init>(Lco/uk/getmondo/payments/send/o$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onRecentPayeeClicke\u2026  }\n                    }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/o;->b:Lio/reactivex/b/a;

    .line 113
    iget-object v1, p0, Lco/uk/getmondo/payments/send/o;->b:Lio/reactivex/b/a;

    .line 114
    iget-object v0, p0, Lco/uk/getmondo/payments/send/o;->o:Lco/uk/getmondo/common/o;

    invoke-virtual {v0}, Lco/uk/getmondo/common/o;->a()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/payments/send/o$f;

    invoke-direct {v0, p1}, Lco/uk/getmondo/payments/send/o$f;-><init>(Lco/uk/getmondo/payments/send/o$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "featureFlagsStorage.feat\u2026  }\n                    }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/o;->b:Lio/reactivex/b/a;

    .line 127
    :goto_1
    iget-object v1, p0, Lco/uk/getmondo/payments/send/o;->b:Lio/reactivex/b/a;

    .line 152
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/o$a;->c()Lio/reactivex/n;

    move-result-object v2

    .line 130
    new-instance v0, Lco/uk/getmondo/payments/send/o$g;

    invoke-direct {v0, p1}, Lco/uk/getmondo/payments/send/o$g;-><init>(Lco/uk/getmondo/payments/send/o$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v2

    .line 131
    new-instance v0, Lco/uk/getmondo/payments/send/o$h;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/payments/send/o$h;-><init>(Lco/uk/getmondo/payments/send/o;Lco/uk/getmondo/payments/send/o$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->switchMap(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v2

    .line 152
    new-instance v0, Lco/uk/getmondo/payments/send/o$i;

    invoke-direct {v0, p1}, Lco/uk/getmondo/payments/send/o$i;-><init>(Lco/uk/getmondo/payments/send/o$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onContactWithMonzoC\u2026 view.openSendMoney(it) }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/o;->b:Lio/reactivex/b/a;

    .line 154
    iget-object v1, p0, Lco/uk/getmondo/payments/send/o;->b:Lio/reactivex/b/a;

    .line 156
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/o$a;->d()Lio/reactivex/n;

    move-result-object v2

    .line 155
    new-instance v0, Lco/uk/getmondo/payments/send/o$j;

    invoke-direct {v0, p0}, Lco/uk/getmondo/payments/send/o$j;-><init>(Lco/uk/getmondo/payments/send/o;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v2

    .line 156
    new-instance v0, Lco/uk/getmondo/payments/send/o$k;

    invoke-direct {v0, p1}, Lco/uk/getmondo/payments/send/o$k;-><init>(Lco/uk/getmondo/payments/send/o$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onContactNotOnMonzo\u2026 view.inviteContact(it) }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/o;->b:Lio/reactivex/b/a;

    .line 158
    iget-object v1, p0, Lco/uk/getmondo/payments/send/o;->b:Lio/reactivex/b/a;

    .line 159
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/o$a;->e()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/payments/send/o$l;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/payments/send/o$l;-><init>(Lco/uk/getmondo/payments/send/o;Lco/uk/getmondo/payments/send/o$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onAddContactClicked\u2026tact())\n                }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/o;->b:Lio/reactivex/b/a;

    .line 163
    return-void

    .line 60
    :cond_0
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/o$a;->b()Lio/reactivex/n;

    move-result-object v0

    goto/16 :goto_0

    .line 123
    :cond_1
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/o$a;->i()Z

    move-result v0

    if-nez v0, :cond_2

    .line 124
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/o$a;->j()V

    .line 126
    :cond_2
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/o$a;->k()V

    goto/16 :goto_1
.end method
