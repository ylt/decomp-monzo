.class Lco/uk/getmondo/payments/send/SendMoneyAdapter;
.super Landroid/support/v7/widget/RecyclerView$a;
.source "SendMoneyAdapter.java"

# interfaces
.implements La/a/a/a/a/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/payments/send/SendMoneyAdapter$d;,
        Lco/uk/getmondo/payments/send/SendMoneyAdapter$RecentPayeeViewHolder;,
        Lco/uk/getmondo/payments/send/SendMoneyAdapter$b;,
        Lco/uk/getmondo/payments/send/SendMoneyAdapter$e;,
        Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;,
        Lco/uk/getmondo/payments/send/SendMoneyAdapter$StickyHeaderViewHolder;,
        Lco/uk/getmondo/payments/send/SendMoneyAdapter$a;,
        Lco/uk/getmondo/payments/send/SendMoneyAdapter$f;,
        Lco/uk/getmondo/payments/send/SendMoneyAdapter$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$a",
        "<",
        "Landroid/support/v7/widget/RecyclerView$w;",
        ">;",
        "La/a/a/a/a/a",
        "<",
        "Lco/uk/getmondo/payments/send/SendMoneyAdapter$StickyHeaderViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/payments/send/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lco/uk/getmondo/payments/send/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/payments/send/data/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private d:Z

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lco/uk/getmondo/payments/send/SendMoneyAdapter$c;

.field private g:Lco/uk/getmondo/payments/send/SendMoneyAdapter$a;

.field private h:Lco/uk/getmondo/payments/send/SendMoneyAdapter$f;

.field private i:Z

.field private j:Lco/uk/getmondo/common/ui/a;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 69
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$a;-><init>()V

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->a:Ljava/util/List;

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->b:Ljava/util/Collection;

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->c:Ljava/util/List;

    .line 60
    iput-boolean v1, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->d:Z

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->e:Ljava/util/List;

    .line 66
    iput-boolean v1, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->i:Z

    .line 70
    invoke-static {p1}, Lco/uk/getmondo/common/ui/a;->a(Landroid/content/Context;)Lco/uk/getmondo/common/ui/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->j:Lco/uk/getmondo/common/ui/a;

    .line 71
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/SendMoneyAdapter;)Lco/uk/getmondo/common/ui/a;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->j:Lco/uk/getmondo/common/ui/a;

    return-object v0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 106
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 107
    iget-boolean v1, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->i:Z

    if-eqz v1, :cond_0

    .line 108
    const-string v1, "BANK_PAYMENTS_ITEM"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    iget-object v1, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 111
    :cond_0
    iget-object v1, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 112
    iget-object v1, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->b:Ljava/util/Collection;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 113
    iget-boolean v1, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->d:Z

    if-eqz v1, :cond_1

    .line 114
    const-string v1, "FOOTER_ITEM"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    :cond_1
    new-instance v1, Lco/uk/getmondo/payments/send/SendMoneyAdapter$d;

    iget-object v2, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->e:Ljava/util/List;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v0, v3}, Lco/uk/getmondo/payments/send/SendMoneyAdapter$d;-><init>(Ljava/util/List;Ljava/util/List;Lco/uk/getmondo/payments/send/SendMoneyAdapter$1;)V

    invoke-static {v1}, Landroid/support/v7/f/b;->a(Landroid/support/v7/f/b$a;)Landroid/support/v7/f/b$b;

    move-result-object v1

    .line 118
    iget-object v2, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 119
    iget-object v2, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->e:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 120
    invoke-virtual {v1, p0}, Landroid/support/v7/f/b$b;->a(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 121
    return-void
.end method

.method static synthetic b(Lco/uk/getmondo/payments/send/SendMoneyAdapter;)Ljava/util/List;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->e:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public a(I)J
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 174
    const-string v1, "BANK_PAYMENTS_ITEM"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "FOOTER_ITEM"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 175
    :cond_0
    const-wide/16 v0, -0x1

    .line 181
    :goto_0
    return-wide v0

    .line 176
    :cond_1
    instance-of v1, v0, Lco/uk/getmondo/payments/send/a/b;

    if-eqz v1, :cond_2

    .line 177
    const-wide/16 v0, 0xa

    goto :goto_0

    .line 178
    :cond_2
    instance-of v0, v0, Lco/uk/getmondo/payments/send/data/a/e;

    if-eqz v0, :cond_3

    .line 179
    const-wide/16 v0, 0xb

    goto :goto_0

    .line 181
    :cond_3
    const-wide/16 v0, 0xc

    goto :goto_0
.end method

.method public synthetic a(Landroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$w;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->b(Landroid/view/ViewGroup;)Lco/uk/getmondo/payments/send/SendMoneyAdapter$StickyHeaderViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Landroid/support/v7/widget/RecyclerView$w;I)V
    .locals 0

    .prologue
    .line 42
    check-cast p1, Lco/uk/getmondo/payments/send/SendMoneyAdapter$StickyHeaderViewHolder;

    invoke-virtual {p0, p1, p2}, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->a(Lco/uk/getmondo/payments/send/SendMoneyAdapter$StickyHeaderViewHolder;I)V

    return-void
.end method

.method public a(Lco/uk/getmondo/payments/send/SendMoneyAdapter$StickyHeaderViewHolder;I)V
    .locals 4

    .prologue
    .line 193
    iget-object v0, p1, Lco/uk/getmondo/payments/send/SendMoneyAdapter$StickyHeaderViewHolder;->headerTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 195
    invoke-virtual {p0, p2}, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->a(I)J

    move-result-wide v2

    long-to-int v1, v2

    .line 196
    packed-switch v1, :pswitch_data_0

    .line 204
    const v1, 0x7f0a0349

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 207
    :goto_0
    invoke-virtual {p1, v0}, Lco/uk/getmondo/payments/send/SendMoneyAdapter$StickyHeaderViewHolder;->a(Ljava/lang/String;)V

    .line 208
    return-void

    .line 198
    :pswitch_0
    const v1, 0x7f0a034e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 201
    :pswitch_1
    const v1, 0x7f0a035c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 196
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method a(Lco/uk/getmondo/payments/send/SendMoneyAdapter$a;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->g:Lco/uk/getmondo/payments/send/SendMoneyAdapter$a;

    .line 79
    return-void
.end method

.method a(Lco/uk/getmondo/payments/send/SendMoneyAdapter$c;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->f:Lco/uk/getmondo/payments/send/SendMoneyAdapter$c;

    .line 75
    return-void
.end method

.method a(Lco/uk/getmondo/payments/send/SendMoneyAdapter$f;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->h:Lco/uk/getmondo/payments/send/SendMoneyAdapter$f;

    .line 83
    return-void
.end method

.method a(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lco/uk/getmondo/payments/send/data/a/e;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 101
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 102
    invoke-direct {p0}, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->a()V

    .line 103
    return-void
.end method

.method a(Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lco/uk/getmondo/payments/send/a/b;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lco/uk/getmondo/payments/send/a/d;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 91
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 92
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->b:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 93
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 94
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->b:Ljava/util/Collection;

    invoke-interface {v0, p2}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 95
    const/4 v0, 0x1

    iput-boolean v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->d:Z

    .line 96
    invoke-direct {p0}, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->a()V

    .line 97
    return-void
.end method

.method a(Z)V
    .locals 0

    .prologue
    .line 86
    iput-boolean p1, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->i:Z

    .line 87
    invoke-direct {p0}, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->a()V

    .line 88
    return-void
.end method

.method public b(Landroid/view/ViewGroup;)Lco/uk/getmondo/payments/send/SendMoneyAdapter$StickyHeaderViewHolder;
    .locals 3

    .prologue
    .line 187
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f050103

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 188
    new-instance v1, Lco/uk/getmondo/payments/send/SendMoneyAdapter$StickyHeaderViewHolder;

    invoke-direct {v1, v0}, Lco/uk/getmondo/payments/send/SendMoneyAdapter$StickyHeaderViewHolder;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    .line 160
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 161
    const-string v1, "BANK_PAYMENTS_ITEM"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 162
    const/4 v0, 0x2

    .line 168
    :goto_0
    return v0

    .line 163
    :cond_0
    const-string v1, "FOOTER_ITEM"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 164
    const/4 v0, 0x1

    goto :goto_0

    .line 165
    :cond_1
    instance-of v0, v0, Lco/uk/getmondo/payments/send/data/a/e;

    if-eqz v0, :cond_2

    .line 166
    const/4 v0, 0x3

    goto :goto_0

    .line 168
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$w;I)V
    .locals 2

    .prologue
    .line 143
    instance-of v0, p1, Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 144
    check-cast v0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;

    .line 145
    iget-object v1, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->e:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/payments/send/a/a;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;->a(Lco/uk/getmondo/payments/send/a/a;)V

    .line 147
    :cond_0
    instance-of v0, p1, Lco/uk/getmondo/payments/send/SendMoneyAdapter$RecentPayeeViewHolder;

    if-eqz v0, :cond_1

    .line 148
    check-cast p1, Lco/uk/getmondo/payments/send/SendMoneyAdapter$RecentPayeeViewHolder;

    .line 149
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->e:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/send/data/a/e;

    invoke-virtual {p1, v0}, Lco/uk/getmondo/payments/send/SendMoneyAdapter$RecentPayeeViewHolder;->a(Lco/uk/getmondo/payments/send/data/a/e;)V

    .line 151
    :cond_1
    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$w;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 125
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 126
    packed-switch p2, :pswitch_data_0

    .line 137
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected viewType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :pswitch_0
    new-instance v0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$b;

    const v2, 0x7f0500fb

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->g:Lco/uk/getmondo/payments/send/SendMoneyAdapter$a;

    invoke-direct {v0, p0, v1, v2}, Lco/uk/getmondo/payments/send/SendMoneyAdapter$b;-><init>(Lco/uk/getmondo/payments/send/SendMoneyAdapter;Landroid/view/View;Lco/uk/getmondo/payments/send/SendMoneyAdapter$a;)V

    .line 135
    :goto_0
    return-object v0

    .line 131
    :pswitch_1
    new-instance v0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;

    const v2, 0x7f0500ff

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->f:Lco/uk/getmondo/payments/send/SendMoneyAdapter$c;

    invoke-direct {v0, p0, v1, v2}, Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;-><init>(Lco/uk/getmondo/payments/send/SendMoneyAdapter;Landroid/view/View;Lco/uk/getmondo/payments/send/SendMoneyAdapter$c;)V

    goto :goto_0

    .line 133
    :pswitch_2
    new-instance v0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$e;

    const v2, 0x7f050100

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lco/uk/getmondo/payments/send/SendMoneyAdapter$e;-><init>(Lco/uk/getmondo/payments/send/SendMoneyAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 135
    :pswitch_3
    new-instance v0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$RecentPayeeViewHolder;

    const v2, 0x7f050107

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->h:Lco/uk/getmondo/payments/send/SendMoneyAdapter$f;

    invoke-direct {v0, p0, v1, v2}, Lco/uk/getmondo/payments/send/SendMoneyAdapter$RecentPayeeViewHolder;-><init>(Lco/uk/getmondo/payments/send/SendMoneyAdapter;Landroid/view/View;Lco/uk/getmondo/payments/send/SendMoneyAdapter$f;)V

    goto :goto_0

    .line 126
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
