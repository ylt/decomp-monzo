.class public Lco/uk/getmondo/payments/send/bank/payee/g;
.super Lco/uk/getmondo/common/ui/b;
.source "UkBankPayeeDetailsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/payments/send/bank/payee/g$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/payments/send/bank/payee/g$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lco/uk/getmondo/payments/send/data/a;

.field private final d:Lco/uk/getmondo/common/e/a;

.field private final e:Lio/reactivex/u;

.field private final f:Lio/reactivex/u;


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/payments/send/data/a;Lco/uk/getmondo/common/e/a;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 37
    iput-object p3, p0, Lco/uk/getmondo/payments/send/bank/payee/g;->c:Lco/uk/getmondo/payments/send/data/a;

    .line 38
    iput-object p4, p0, Lco/uk/getmondo/payments/send/bank/payee/g;->d:Lco/uk/getmondo/common/e/a;

    .line 39
    iput-object p2, p0, Lco/uk/getmondo/payments/send/bank/payee/g;->e:Lio/reactivex/u;

    .line 40
    iput-object p1, p0, Lco/uk/getmondo/payments/send/bank/payee/g;->f:Lio/reactivex/u;

    .line 41
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/bank/payee/g;Lco/uk/getmondo/payments/send/bank/payee/g$a;Lco/uk/getmondo/payments/send/data/a/b;)Lio/reactivex/z;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payee/g;->c:Lco/uk/getmondo/payments/send/data/a;

    invoke-virtual {v0, p2}, Lco/uk/getmondo/payments/send/data/a;->a(Lco/uk/getmondo/payments/send/data/a/b;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/payments/send/bank/payee/g;->e:Lio/reactivex/u;

    .line 87
    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/payments/send/bank/payee/g;->f:Lio/reactivex/u;

    .line 88
    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/payments/send/bank/payee/k;->a(Lco/uk/getmondo/payments/send/bank/payee/g$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 89
    invoke-virtual {v0, v1}, Lio/reactivex/b;->c(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/payments/send/bank/payee/l;->a(Lco/uk/getmondo/payments/send/bank/payee/g$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 90
    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/payments/send/bank/payee/m;->a(Lco/uk/getmondo/payments/send/bank/payee/g;Lco/uk/getmondo/payments/send/bank/payee/g$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 91
    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v0

    .line 92
    invoke-virtual {v0, p2}, Lio/reactivex/b;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    .line 93
    invoke-static {}, Lio/reactivex/v;->o_()Lio/reactivex/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/v;)Lio/reactivex/v;

    move-result-object v0

    .line 86
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/bank/payee/g$a;Landroid/support/v4/g/j;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p1, Landroid/support/v4/g/j;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 77
    :goto_0
    if-eqz v0, :cond_1

    sget-object v1, Lco/uk/getmondo/payments/send/data/a/b;->a:Lco/uk/getmondo/payments/send/data/a/b$a;

    iget-object v0, p1, Landroid/support/v4/g/j;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/payments/send/data/a/b$a;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 78
    invoke-interface {p0}, Lco/uk/getmondo/payments/send/bank/payee/g$a;->b()V

    .line 82
    :goto_1
    return-void

    .line 76
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 80
    :cond_1
    invoke-interface {p0}, Lco/uk/getmondo/payments/send/bank/payee/g$a;->n()V

    goto :goto_1
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/bank/payee/g$a;Lco/uk/getmondo/payments/send/data/a/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 49
    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/data/a/b;->c()Z

    move-result v0

    invoke-interface {p0, v0}, Lco/uk/getmondo/payments/send/bank/payee/g$a;->a(Z)V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/bank/payee/g$a;Lio/reactivex/b/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 89
    invoke-interface {p0}, Lco/uk/getmondo/payments/send/bank/payee/g$a;->o()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/bank/payee/g$a;Ljava/lang/Throwable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 90
    invoke-interface {p0}, Lco/uk/getmondo/payments/send/bank/payee/g$a;->p()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/bank/payee/g;Lco/uk/getmondo/payments/send/bank/payee/g$a;Ljava/lang/Throwable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 91
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/payments/send/bank/payee/g;->b(Lco/uk/getmondo/payments/send/bank/payee/g$a;Ljava/lang/Throwable;)V

    return-void
.end method

.method static synthetic b(Lco/uk/getmondo/payments/send/bank/payee/g$a;Landroid/support/v4/g/j;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p1, Landroid/support/v4/g/j;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 66
    :goto_0
    if-eqz v0, :cond_1

    sget-object v1, Lco/uk/getmondo/payments/send/data/a/b;->a:Lco/uk/getmondo/payments/send/data/a/b$a;

    iget-object v0, p1, Landroid/support/v4/g/j;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/payments/send/data/a/b$a;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 67
    invoke-interface {p0}, Lco/uk/getmondo/payments/send/bank/payee/g$a;->a()V

    .line 71
    :goto_1
    return-void

    .line 65
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 69
    :cond_1
    invoke-interface {p0}, Lco/uk/getmondo/payments/send/bank/payee/g$a;->m()V

    goto :goto_1
.end method

.method private b(Lco/uk/getmondo/payments/send/bank/payee/g$a;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 98
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/bank/payee/g$a;->n()V

    .line 99
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/bank/payee/g$a;->m()V

    .line 102
    instance-of v0, p2, Lco/uk/getmondo/api/ApiException;

    if-eqz v0, :cond_1

    move-object v0, p2

    check-cast v0, Lco/uk/getmondo/api/ApiException;

    invoke-virtual {v0}, Lco/uk/getmondo/api/ApiException;->e()Lco/uk/getmondo/api/model/b;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 103
    invoke-static {}, Lco/uk/getmondo/payments/a/a/c;->values()[Lco/uk/getmondo/payments/a/a/c;

    move-result-object v1

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lco/uk/getmondo/common/e/d;->a([Lco/uk/getmondo/common/e/f;Ljava/lang/String;)Lco/uk/getmondo/common/e/f;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/a/a/c;

    .line 104
    if-eqz v0, :cond_1

    .line 105
    invoke-virtual {v0, p1}, Lco/uk/getmondo/payments/a/a/c;->a(Lco/uk/getmondo/payments/send/bank/payee/a;)V

    .line 113
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payee/g;->d:Lco/uk/getmondo/common/e/a;

    invoke-virtual {v0, p2, p1}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 111
    const v0, 0x7f0a0196

    invoke-interface {p1, v0}, Lco/uk/getmondo/payments/send/bank/payee/g$a;->b(I)V

    goto :goto_0
.end method

.method static synthetic c(Lco/uk/getmondo/payments/send/bank/payee/g$a;Landroid/support/v4/g/j;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p1, Landroid/support/v4/g/j;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 55
    :goto_0
    if-eqz v0, :cond_1

    sget-object v1, Lco/uk/getmondo/payments/send/data/a/b;->a:Lco/uk/getmondo/payments/send/data/a/b$a;

    iget-object v0, p1, Landroid/support/v4/g/j;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/payments/send/data/a/b$a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 56
    invoke-interface {p0}, Lco/uk/getmondo/payments/send/bank/payee/g$a;->k()V

    .line 60
    :goto_1
    return-void

    .line 54
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 58
    :cond_1
    invoke-interface {p0}, Lco/uk/getmondo/payments/send/bank/payee/g$a;->l()V

    goto :goto_1
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lco/uk/getmondo/payments/send/bank/payee/g$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/send/bank/payee/g;->a(Lco/uk/getmondo/payments/send/bank/payee/g$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/payments/send/bank/payee/g$a;)V
    .locals 4

    .prologue
    .line 45
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 47
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/bank/payee/g$a;->d()Lio/reactivex/n;

    move-result-object v0

    invoke-interface {p1}, Lco/uk/getmondo/payments/send/bank/payee/g$a;->e()Lio/reactivex/n;

    move-result-object v1

    invoke-interface {p1}, Lco/uk/getmondo/payments/send/bank/payee/g$a;->f()Lio/reactivex/n;

    move-result-object v2

    invoke-static {}, Lco/uk/getmondo/payments/send/bank/payee/h;->a()Lio/reactivex/c/i;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lio/reactivex/n;->combineLatest(Lio/reactivex/r;Lio/reactivex/r;Lio/reactivex/r;Lio/reactivex/c/i;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/payments/send/bank/payee/n;->a(Lco/uk/getmondo/payments/send/bank/payee/g$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 49
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 47
    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/bank/payee/g;->a(Lio/reactivex/b/b;)V

    .line 51
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/bank/payee/g$a;->g()Lio/reactivex/n;

    move-result-object v0

    .line 52
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/bank/payee/g$a;->d()Lio/reactivex/n;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/payments/send/bank/payee/o;->a()Lio/reactivex/c/c;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/reactivex/n;->withLatestFrom(Lio/reactivex/r;Lio/reactivex/c/c;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/payments/send/bank/payee/p;->a(Lco/uk/getmondo/payments/send/bank/payee/g$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 53
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 51
    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/bank/payee/g;->a(Lio/reactivex/b/b;)V

    .line 62
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/bank/payee/g$a;->h()Lio/reactivex/n;

    move-result-object v0

    .line 63
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/bank/payee/g$a;->e()Lio/reactivex/n;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/payments/send/bank/payee/q;->a()Lio/reactivex/c/c;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/reactivex/n;->withLatestFrom(Lio/reactivex/r;Lio/reactivex/c/c;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/payments/send/bank/payee/r;->a(Lco/uk/getmondo/payments/send/bank/payee/g$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 64
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 62
    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/bank/payee/g;->a(Lio/reactivex/b/b;)V

    .line 73
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/bank/payee/g$a;->i()Lio/reactivex/n;

    move-result-object v0

    .line 74
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/bank/payee/g$a;->f()Lio/reactivex/n;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/payments/send/bank/payee/s;->a()Lio/reactivex/c/c;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/reactivex/n;->withLatestFrom(Lio/reactivex/r;Lio/reactivex/c/c;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/payments/send/bank/payee/t;->a(Lco/uk/getmondo/payments/send/bank/payee/g$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 75
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 73
    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/bank/payee/g;->a(Lio/reactivex/b/b;)V

    .line 84
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/bank/payee/g$a;->j()Lio/reactivex/n;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/payments/send/bank/payee/u;->a()Lio/reactivex/c/q;

    move-result-object v1

    .line 85
    invoke-virtual {v0, v1}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/payments/send/bank/payee/i;->a(Lco/uk/getmondo/payments/send/bank/payee/g;Lco/uk/getmondo/payments/send/bank/payee/g$a;)Lio/reactivex/c/h;

    move-result-object v1

    .line 86
    invoke-virtual {v0, v1}, Lio/reactivex/n;->flatMapSingle(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p1}, Lco/uk/getmondo/payments/send/bank/payee/j;->a(Lco/uk/getmondo/payments/send/bank/payee/g$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 94
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 84
    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/bank/payee/g;->a(Lio/reactivex/b/b;)V

    .line 95
    return-void
.end method
