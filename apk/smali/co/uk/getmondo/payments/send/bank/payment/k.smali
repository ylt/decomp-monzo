.class public final Lco/uk/getmondo/payments/send/bank/payment/k;
.super Lco/uk/getmondo/common/ui/b;
.source "BankPaymentDetailsPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/payments/send/bank/payment/k$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/payments/send/bank/payment/k$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001fB;\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0012\u0010\u0011\u001a\u00020\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0002J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0002H\u0016J\u0018\u0010\u0018\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J \u0010\u001b\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001dH\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsPresenter$View;",
        "uiScheduler",
        "Lio/reactivex/Scheduler;",
        "ioScheduler",
        "balanceRepository",
        "Lco/uk/getmondo/account_balance/BalanceRepository;",
        "accountService",
        "Lco/uk/getmondo/common/accounts/AccountService;",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "bankPayee",
        "Lco/uk/getmondo/payments/send/data/model/BankPayee;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/account_balance/BalanceRepository;Lco/uk/getmondo/common/accounts/AccountService;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/payments/send/data/model/BankPayee;)V",
        "referenceRegex",
        "Lkotlin/text/Regex;",
        "isValidBankPaymentAmount",
        "",
        "amount",
        "Lco/uk/getmondo/model/Amount;",
        "register",
        "",
        "view",
        "validateFinal",
        "formData",
        "Lco/uk/getmondo/payments/send/bank/payment/PaymentDetailsFormData;",
        "validateWhileTyping",
        "amountString",
        "",
        "reference",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Lkotlin/h/g;

.field private final d:Lio/reactivex/u;

.field private final e:Lio/reactivex/u;

.field private final f:Lco/uk/getmondo/a/a;

.field private final g:Lco/uk/getmondo/common/accounts/d;

.field private final h:Lco/uk/getmondo/common/e/a;

.field private final i:Lco/uk/getmondo/payments/send/data/a/b;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/a/a;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/payments/send/data/a/b;)V
    .locals 2

    .prologue
    const-string v0, "uiScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ioScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "balanceRepository"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountService"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiErrorHandler"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bankPayee"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->d:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->e:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->f:Lco/uk/getmondo/a/a;

    iput-object p4, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->g:Lco/uk/getmondo/common/accounts/d;

    iput-object p5, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->h:Lco/uk/getmondo/common/e/a;

    iput-object p6, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->i:Lco/uk/getmondo/payments/send/data/a/b;

    .line 36
    new-instance v0, Lkotlin/h/g;

    const-string v1, "^[a-zA-Z0-9 /?:().,+#=!\"%&*<>;{}@_\\-]+$"

    invoke-direct {v0, v1}, Lkotlin/h/g;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->c:Lkotlin/h/g;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/payments/send/bank/payment/k;)Lco/uk/getmondo/a/a;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->f:Lco/uk/getmondo/a/a;

    return-object v0
.end method

.method private final a(Lco/uk/getmondo/payments/send/bank/payment/k$a;Lco/uk/getmondo/payments/send/bank/payment/s;)V
    .locals 11

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 90
    sget-object v0, Lco/uk/getmondo/d/c;->Companion:Lco/uk/getmondo/d/c$a;

    invoke-virtual {p2}, Lco/uk/getmondo/payments/send/bank/payment/s;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lco/uk/getmondo/common/i/c;->a:Lco/uk/getmondo/common/i/c;

    const-string v3, "Currency.GBP"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/d/c$a;->a(Ljava/lang/String;Lco/uk/getmondo/common/i/c;)Lco/uk/getmondo/d/c;

    move-result-object v8

    .line 91
    invoke-direct {p0, v8}, Lco/uk/getmondo/payments/send/bank/payment/k;->a(Lco/uk/getmondo/d/c;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 93
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Balance string is not a valid number"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    invoke-static {v0}, Ld/a/a;->a(Ljava/lang/Throwable;)V

    .line 105
    :goto_0
    return-void

    .line 97
    :cond_0
    invoke-virtual {p2}, Lco/uk/getmondo/payments/send/bank/payment/s;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 99
    invoke-virtual {p2}, Lco/uk/getmondo/payments/send/bank/payment/s;->e()Lco/uk/getmondo/payments/a/a/d$c;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/payments/a/a/d$c;->a:Lco/uk/getmondo/payments/a/a/d$c;

    if-eq v0, v1, :cond_2

    invoke-virtual {p2}, Lco/uk/getmondo/payments/send/bank/payment/s;->f()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    .line 100
    :goto_1
    new-instance v7, Lco/uk/getmondo/payments/send/data/a/c;

    if-nez v8, :cond_1

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_1
    invoke-virtual {p2}, Lco/uk/getmondo/payments/send/bank/payment/s;->b()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->i:Lco/uk/getmondo/payments/send/data/a/b;

    new-instance v0, Lco/uk/getmondo/payments/a/a/d;

    invoke-virtual {p2}, Lco/uk/getmondo/payments/send/bank/payment/s;->d()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-virtual {p2}, Lco/uk/getmondo/payments/send/bank/payment/s;->e()Lco/uk/getmondo/payments/a/a/d$c;

    move-result-object v2

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lco/uk/getmondo/payments/a/a/d;-><init>(Lorg/threeten/bp/LocalDate;Lco/uk/getmondo/payments/a/a/d$c;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;ILkotlin/d/b/i;)V

    invoke-direct {v7, v8, v9, v10, v0}, Lco/uk/getmondo/payments/send/data/a/c;-><init>(Lco/uk/getmondo/d/c;Ljava/lang/String;Lco/uk/getmondo/payments/send/data/a/b;Lco/uk/getmondo/payments/a/a/d;)V

    move-object v0, v7

    .line 103
    :goto_2
    invoke-interface {p1, v0}, Lco/uk/getmondo/payments/send/bank/payment/k$a;->a(Lco/uk/getmondo/payments/send/data/a/c;)V

    goto :goto_0

    :cond_2
    move-object v3, v4

    .line 99
    goto :goto_1

    .line 102
    :cond_3
    new-instance v0, Lco/uk/getmondo/payments/send/data/a/c;

    if-nez v8, :cond_4

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_4
    invoke-virtual {p2}, Lco/uk/getmondo/payments/send/bank/payment/s;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->i:Lco/uk/getmondo/payments/send/data/a/b;

    move-object v1, v8

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lco/uk/getmondo/payments/send/data/a/c;-><init>(Lco/uk/getmondo/d/c;Ljava/lang/String;Lco/uk/getmondo/payments/send/data/a/b;Lco/uk/getmondo/payments/a/a/d;ILkotlin/d/b/i;)V

    goto :goto_2
.end method

.method private final a(Lco/uk/getmondo/payments/send/bank/payment/k$a;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 108
    sget-object v0, Lco/uk/getmondo/d/c;->Companion:Lco/uk/getmondo/d/c$a;

    sget-object v3, Lco/uk/getmondo/common/i/c;->a:Lco/uk/getmondo/common/i/c;

    const-string v4, "Currency.GBP"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p2, v3}, Lco/uk/getmondo/d/c$a;->a(Ljava/lang/String;Lco/uk/getmondo/common/i/c;)Lco/uk/getmondo/d/c;

    move-result-object v3

    .line 109
    iget-object v4, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->c:Lkotlin/h/g;

    if-nez p3, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, p3

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/h/j;->b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v4, v0}, Lkotlin/h/g;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    .line 110
    if-nez v4, :cond_1

    check-cast p3, Ljava/lang/CharSequence;

    invoke-interface {p3}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 111
    :cond_1
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/bank/payment/k$a;->v()V

    .line 114
    :goto_1
    invoke-direct {p0, v3}, Lco/uk/getmondo/payments/send/bank/payment/k;->a(Lco/uk/getmondo/d/c;)Z

    move-result v0

    if-eqz v0, :cond_4

    if-eqz v4, :cond_4

    move v0, v1

    :goto_2
    invoke-interface {p1, v0}, Lco/uk/getmondo/payments/send/bank/payment/k$a;->b(Z)V

    .line 116
    return-void

    :cond_2
    move v0, v2

    .line 110
    goto :goto_0

    .line 113
    :cond_3
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/bank/payment/k$a;->k()V

    goto :goto_1

    :cond_4
    move v0, v2

    .line 114
    goto :goto_2
.end method

.method public static final synthetic a(Lco/uk/getmondo/payments/send/bank/payment/k;Lco/uk/getmondo/payments/send/bank/payment/k$a;Lco/uk/getmondo/payments/send/bank/payment/s;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/payments/send/bank/payment/k;->a(Lco/uk/getmondo/payments/send/bank/payment/k$a;Lco/uk/getmondo/payments/send/bank/payment/s;)V

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/payments/send/bank/payment/k;Lco/uk/getmondo/payments/send/bank/payment/k$a;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lco/uk/getmondo/payments/send/bank/payment/k;->a(Lco/uk/getmondo/payments/send/bank/payment/k$a;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private final a(Lco/uk/getmondo/d/c;)Z
    .locals 4

    .prologue
    .line 119
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lco/uk/getmondo/d/c;->k()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final synthetic b(Lco/uk/getmondo/payments/send/bank/payment/k;)Lco/uk/getmondo/common/e/a;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->h:Lco/uk/getmondo/common/e/a;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lco/uk/getmondo/payments/send/bank/payment/k$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/send/bank/payment/k;->a(Lco/uk/getmondo/payments/send/bank/payment/k$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/payments/send/bank/payment/k$a;)V
    .locals 4

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 39
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 41
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->i:Lco/uk/getmondo/payments/send/data/a/b;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/data/a/b;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lco/uk/getmondo/payments/send/bank/payment/k$a;->a(Ljava/lang/String;)V

    .line 42
    iget-object v2, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->b:Lio/reactivex/b/a;

    .line 46
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->g:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->d()Lio/reactivex/v;

    move-result-object v1

    .line 43
    new-instance v0, Lco/uk/getmondo/payments/send/bank/payment/k$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/payments/send/bank/payment/k$b;-><init>(Lco/uk/getmondo/payments/send/bank/payment/k;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    .line 44
    iget-object v1, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->e:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    .line 45
    iget-object v1, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->d:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v3

    .line 46
    sget-object v0, Lco/uk/getmondo/payments/send/bank/payment/k$h;->a:Lco/uk/getmondo/payments/send/bank/payment/k$h;

    check-cast v0, Lio/reactivex/c/a;

    new-instance v1, Lco/uk/getmondo/payments/send/bank/payment/k$i;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/payments/send/bank/payment/k$i;-><init>(Lco/uk/getmondo/payments/send/bank/payment/k;Lco/uk/getmondo/payments/send/bank/payment/k$a;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v3, v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "accountService.accountId\u2026ndleError(error, view) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->b:Lio/reactivex/b/a;

    .line 48
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->g:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->d()Lio/reactivex/v;

    move-result-object v1

    .line 49
    new-instance v0, Lco/uk/getmondo/payments/send/bank/payment/k$p;

    invoke-direct {v0, p0}, Lco/uk/getmondo/payments/send/bank/payment/k$p;-><init>(Lco/uk/getmondo/payments/send/bank/payment/k;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->b(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    .line 50
    sget-object v0, Lco/uk/getmondo/payments/send/bank/payment/k$q;->a:Lco/uk/getmondo/payments/send/bank/payment/k$q;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Lio/reactivex/n;->distinctUntilChanged()Lio/reactivex/n;

    move-result-object v0

    .line 53
    iget-object v2, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->b:Lio/reactivex/b/a;

    .line 54
    iget-object v1, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->d:Lio/reactivex/u;

    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v3

    new-instance v0, Lco/uk/getmondo/payments/send/bank/payment/k$j;

    invoke-direct {v0, p1}, Lco/uk/getmondo/payments/send/bank/payment/k$j;-><init>(Lco/uk/getmondo/payments/send/bank/payment/k$a;)V

    check-cast v0, Lio/reactivex/c/g;

    sget-object v1, Lco/uk/getmondo/payments/send/bank/payment/k$k;->a:Lco/uk/getmondo/payments/send/bank/payment/k$k;

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v3, v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "balance.observeOn(uiSche\u2026(it) }, { Timber.e(it) })"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->b:Lio/reactivex/b/a;

    .line 56
    iget-object v1, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->b:Lio/reactivex/b/a;

    .line 58
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/bank/payment/k$a;->b()Lio/reactivex/n;

    move-result-object v0

    invoke-interface {p1}, Lco/uk/getmondo/payments/send/bank/payment/k$a;->a()Lio/reactivex/n;

    move-result-object v2

    invoke-static {v0, v2}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/n;Lio/reactivex/n;)Lio/reactivex/n;

    move-result-object v0

    .line 57
    iget-object v2, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->d:Lio/reactivex/u;

    invoke-virtual {v0, v2}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v2

    .line 58
    new-instance v0, Lco/uk/getmondo/payments/send/bank/payment/k$l;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/payments/send/bank/payment/k$l;-><init>(Lco/uk/getmondo/payments/send/bank/payment/k;Lco/uk/getmondo/payments/send/bank/payment/k$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "combineLatest(view.onAmo\u2026ng(view, first, second) }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->b:Lio/reactivex/b/a;

    .line 60
    iget-object v1, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->b:Lio/reactivex/b/a;

    .line 62
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/bank/payment/k$a;->c()Lio/reactivex/n;

    move-result-object v0

    .line 61
    iget-object v2, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->d:Lio/reactivex/u;

    invoke-virtual {v0, v2}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v2

    .line 62
    new-instance v0, Lco/uk/getmondo/payments/send/bank/payment/k$m;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/payments/send/bank/payment/k$m;-><init>(Lco/uk/getmondo/payments/send/bank/payment/k;Lco/uk/getmondo/payments/send/bank/payment/k$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onNextStepClicked()\u2026teFinal(view, formData) }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->b:Lio/reactivex/b/a;

    .line 64
    iget-object v1, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->b:Lio/reactivex/b/a;

    .line 65
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/bank/payment/k$a;->d()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/payments/send/bank/payment/k$n;

    invoke-direct {v0, p1}, Lco/uk/getmondo/payments/send/bank/payment/k$n;-><init>(Lco/uk/getmondo/payments/send/bank/payment/k$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onScheduleToggled()\u2026eduleOptionsVisible(it) }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->b:Lio/reactivex/b/a;

    .line 67
    iget-object v1, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->b:Lio/reactivex/b/a;

    .line 68
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/bank/payment/k$a;->e()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/payments/send/bank/payment/k$o;

    invoke-direct {v0, p1}, Lco/uk/getmondo/payments/send/bank/payment/k$o;-><init>(Lco/uk/getmondo/payments/send/bank/payment/k$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onStartDateClicked(\u2026openStartDatePicker(it) }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->b:Lio/reactivex/b/a;

    .line 70
    iget-object v1, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->b:Lio/reactivex/b/a;

    .line 71
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/bank/payment/k$a;->f()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/payments/send/bank/payment/k$c;

    invoke-direct {v0, p1}, Lco/uk/getmondo/payments/send/bank/payment/k$c;-><init>(Lco/uk/getmondo/payments/send/bank/payment/k$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onStartDateSelected\u2026owSelectedStartDate(it) }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->b:Lio/reactivex/b/a;

    .line 73
    iget-object v1, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->b:Lio/reactivex/b/a;

    .line 74
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/bank/payment/k$a;->g()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/payments/send/bank/payment/k$d;

    invoke-direct {v0, p1}, Lco/uk/getmondo/payments/send/bank/payment/k$d;-><init>(Lco/uk/getmondo/payments/send/bank/payment/k$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onIntervalClicked()\u2026.openIntervalPicker(it) }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->b:Lio/reactivex/b/a;

    .line 76
    iget-object v1, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->b:Lio/reactivex/b/a;

    .line 77
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/bank/payment/k$a;->h()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/payments/send/bank/payment/k$e;

    invoke-direct {v0, p1}, Lco/uk/getmondo/payments/send/bank/payment/k$e;-><init>(Lco/uk/getmondo/payments/send/bank/payment/k$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onIntervalSelected(\u2026terval)\n                }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->b:Lio/reactivex/b/a;

    .line 82
    iget-object v1, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->b:Lio/reactivex/b/a;

    .line 83
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/bank/payment/k$a;->i()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/payments/send/bank/payment/k$f;

    invoke-direct {v0, p1}, Lco/uk/getmondo/payments/send/bank/payment/k$f;-><init>(Lco/uk/getmondo/payments/send/bank/payment/k$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onEndDateClicked()\n\u2026w.openEndDatePicker(it) }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->b:Lio/reactivex/b/a;

    .line 85
    iget-object v1, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->b:Lio/reactivex/b/a;

    .line 86
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/bank/payment/k$a;->j()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/payments/send/bank/payment/k$g;

    invoke-direct {v0, p1}, Lco/uk/getmondo/payments/send/bank/payment/k$g;-><init>(Lco/uk/getmondo/payments/send/bank/payment/k$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onEndDateSelected()\u2026showSelectedEndDate(it) }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/k;->b:Lio/reactivex/b/a;

    .line 87
    return-void
.end method
