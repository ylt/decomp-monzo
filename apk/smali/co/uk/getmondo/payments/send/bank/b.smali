.class public Lco/uk/getmondo/payments/send/bank/b;
.super Lco/uk/getmondo/common/ui/b;
.source "BankPaymentPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/payments/send/bank/b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/payments/send/bank/b$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lco/uk/getmondo/a/a;

.field private final e:Lco/uk/getmondo/common/accounts/d;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lco/uk/getmondo/a/a;Lco/uk/getmondo/common/accounts/d;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 25
    iput-object p1, p0, Lco/uk/getmondo/payments/send/bank/b;->c:Lio/reactivex/u;

    .line 26
    iput-object p2, p0, Lco/uk/getmondo/payments/send/bank/b;->d:Lco/uk/getmondo/a/a;

    .line 27
    iput-object p3, p0, Lco/uk/getmondo/payments/send/bank/b;->e:Lco/uk/getmondo/common/accounts/d;

    .line 28
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 16
    check-cast p1, Lco/uk/getmondo/payments/send/bank/b$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/send/bank/b;->a(Lco/uk/getmondo/payments/send/bank/b$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/payments/send/bank/b$a;)V
    .locals 3

    .prologue
    .line 32
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 34
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/b;->e:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->d()Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/payments/send/bank/b;->d:Lco/uk/getmondo/a/a;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lco/uk/getmondo/payments/send/bank/c;->a(Lco/uk/getmondo/a/a;)Lio/reactivex/c/h;

    move-result-object v1

    .line 35
    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/payments/send/bank/d;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 36
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/payments/send/bank/b;->c:Lio/reactivex/u;

    .line 37
    invoke-virtual {v0, v1}, Lio/reactivex/n;->observeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p1}, Lco/uk/getmondo/payments/send/bank/e;->a(Lco/uk/getmondo/payments/send/bank/b$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/payments/send/bank/f;->a()Lio/reactivex/c/g;

    move-result-object v2

    .line 38
    invoke-virtual {v0, v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 34
    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/bank/b;->a(Lio/reactivex/b/b;)V

    .line 39
    return-void
.end method
