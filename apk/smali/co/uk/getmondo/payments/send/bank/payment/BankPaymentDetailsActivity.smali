.class public Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "BankPaymentDetailsActivity.java"

# interfaces
.implements Lco/uk/getmondo/payments/send/bank/payment/k$a;
.implements Lco/uk/getmondo/payments/send/bank/payment/m$a;
.implements Lco/uk/getmondo/payments/send/bank/payment/p$a;


# instance fields
.field a:Lco/uk/getmondo/payments/send/bank/payment/k;

.field amountInputView:Lco/uk/getmondo/common/ui/AmountInputView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110111
    .end annotation
.end field

.field private final b:Lcom/b/b/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/b",
            "<",
            "Lorg/threeten/bp/LocalDate;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/b/b/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/b",
            "<",
            "Lcom/c/b/b",
            "<",
            "Lorg/threeten/bp/LocalDate;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:Lcom/b/b/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/b",
            "<",
            "Lco/uk/getmondo/payments/a/a/d$c;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lorg/threeten/bp/format/DateTimeFormatter;

.field nextStepButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11011b
    .end annotation
.end field

.field referenceEditText:Landroid/widget/EditText;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110113
    .end annotation
.end field

.field referenceInputLayout:Landroid/support/design/widget/TextInputLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110112
    .end annotation
.end field

.field schedulePaymentEndRow:Landroid/view/ViewGroup;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110119
    .end annotation
.end field

.field schedulePaymentEndTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11011a
    .end annotation
.end field

.field schedulePaymentIntervalRow:Landroid/view/ViewGroup;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110117
    .end annotation
.end field

.field schedulePaymentIntervalTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110118
    .end annotation
.end field

.field schedulePaymentStartRow:Landroid/view/ViewGroup;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110115
    .end annotation
.end field

.field schedulePaymentStartTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110116
    .end annotation
.end field

.field schedulePaymentSwitch:Landroid/widget/Switch;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110114
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 64
    invoke-static {}, Lorg/threeten/bp/LocalDate;->a()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-static {v0}, Lcom/b/b/b;->b(Ljava/lang/Object;)Lcom/b/b/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->b:Lcom/b/b/b;

    .line 66
    invoke-static {}, Lcom/c/b/b;->c()Lcom/c/b/b;

    move-result-object v0

    invoke-static {v0}, Lcom/b/b/b;->b(Ljava/lang/Object;)Lcom/b/b/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->c:Lcom/b/b/b;

    .line 67
    sget-object v0, Lco/uk/getmondo/payments/a/a/d$c;->a:Lco/uk/getmondo/payments/a/a/d$c;

    .line 68
    invoke-static {v0}, Lcom/b/b/b;->b(Ljava/lang/Object;)Lcom/b/b/b;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->e:Lcom/b/b/b;

    .line 70
    const-string v0, "EEE, LLL d"

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-static {v0, v1}, Lorg/threeten/bp/format/DateTimeFormatter;->a(Ljava/lang/String;Ljava/util/Locale;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->f:Lorg/threeten/bp/format/DateTimeFormatter;

    return-void
.end method

.method public static a(Landroid/content/Context;Lco/uk/getmondo/payments/send/data/a/b;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 87
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "KEY_RECIPIENT_BANK_ACCOUNT"

    .line 88
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 87
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;Ljava/lang/Object;)Lcom/c/b/b;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 192
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->c:Lcom/b/b/b;

    invoke-virtual {v0}, Lcom/b/b/b;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/c/b/b;

    return-object v0
.end method

.method static synthetic b(Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;Ljava/lang/Object;)Lco/uk/getmondo/payments/a/a/d$c;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 181
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->e:Lcom/b/b/b;

    invoke-virtual {v0}, Lcom/b/b/b;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/a/a/d$c;

    return-object v0
.end method

.method static synthetic c(Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;Ljava/lang/Object;)Lorg/threeten/bp/LocalDate;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 170
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->b:Lcom/b/b/b;

    invoke-virtual {v0}, Lcom/b/b/b;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method static synthetic d(Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;Ljava/lang/Object;)Lco/uk/getmondo/payments/send/bank/payment/s;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 154
    new-instance v0, Lco/uk/getmondo/payments/send/bank/payment/s;

    iget-object v1, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->amountInputView:Lco/uk/getmondo/common/ui/AmountInputView;

    invoke-virtual {v1}, Lco/uk/getmondo/common/ui/AmountInputView;->getAmountText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->referenceEditText:Landroid/widget/EditText;

    .line 155
    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentSwitch:Landroid/widget/Switch;

    .line 156
    invoke-virtual {v3}, Landroid/widget/Switch;->isChecked()Z

    move-result v3

    iget-object v4, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->b:Lcom/b/b/b;

    .line 157
    invoke-virtual {v4}, Lcom/b/b/b;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/threeten/bp/LocalDate;

    iget-object v5, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->e:Lcom/b/b/b;

    .line 158
    invoke-virtual {v5}, Lcom/b/b/b;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lco/uk/getmondo/payments/a/a/d$c;

    iget-object v6, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->c:Lcom/b/b/b;

    .line 159
    invoke-virtual {v6}, Lcom/b/b/b;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/c/b/b;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/c/b/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/threeten/bp/LocalDate;

    invoke-direct/range {v0 .. v6}, Lco/uk/getmondo/payments/send/bank/payment/s;-><init>(Ljava/lang/String;Ljava/lang/String;ZLorg/threeten/bp/LocalDate;Lco/uk/getmondo/payments/a/a/d$c;Lorg/threeten/bp/LocalDate;)V

    .line 154
    return-object v0
.end method

.method private w()V
    .locals 8

    .prologue
    const/16 v7, 0x22

    .line 300
    const v0, 0x7f0a033d

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 301
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 302
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    .line 303
    const v3, 0x7f0f0036

    invoke-static {p0, v3}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v3

    .line 304
    const v4, 0x7f0f0037

    invoke-static {p0, v4}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v4

    .line 305
    new-instance v5, Landroid/text/SpannableString;

    invoke-direct {v5, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 306
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v0, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v3, 0x0

    const/16 v6, 0x11

    invoke-virtual {v5, v0, v3, v1, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 307
    new-instance v0, Landroid/text/style/AbsoluteSizeSpan;

    const/16 v3, 0xc

    invoke-static {v3}, Lco/uk/getmondo/common/k/n;->b(I)I

    move-result v3

    invoke-direct {v0, v3}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-virtual {v5, v0, v1, v2, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 308
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v0, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v5, v0, v1, v2, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 309
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, v5}, Landroid/widget/Switch;->setText(Ljava/lang/CharSequence;)V

    .line 310
    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 143
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->referenceEditText:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/b/a/d/e;->c(Landroid/widget/TextView;)Lcom/b/a/a;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/payments/send/bank/payment/a;->a()Lio/reactivex/c/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/b/a/a;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 129
    const/16 v0, 0x65

    if-ne p1, v0, :cond_0

    .line 130
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->c:Lcom/b/b/b;

    invoke-static {}, Lcom/c/b/b;->c()Lcom/c/b/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/b/b/b;->a(Ljava/lang/Object;)V

    .line 132
    :cond_0
    return-void
.end method

.method public a(Lco/uk/getmondo/d/c;)V
    .locals 5

    .prologue
    .line 207
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->getSupportActionBar()Landroid/support/v7/app/a;

    move-result-object v0

    .line 208
    if-eqz v0, :cond_0

    .line 209
    const v1, 0x7f0a00e5

    invoke-virtual {p0, v1}, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lco/uk/getmondo/d/c;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->b(Ljava/lang/CharSequence;)V

    .line 211
    :cond_0
    return-void
.end method

.method public a(Lco/uk/getmondo/payments/a/a/d$c;)V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->e:Lcom/b/b/b;

    invoke-virtual {v0, p1}, Lcom/b/b/b;->a(Ljava/lang/Object;)V

    .line 138
    return-void
.end method

.method public a(Lco/uk/getmondo/payments/send/data/a/c;)V
    .locals 1

    .prologue
    .line 286
    invoke-static {p0, p1}, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->a(Landroid/content/Context;Lco/uk/getmondo/payments/send/data/a/f;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->startActivity(Landroid/content/Intent;)V

    .line 287
    return-void
.end method

.method public a(Lcom/c/b/b;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/c/b/b",
            "<",
            "Lorg/threeten/bp/LocalDate;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 240
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->b:Lcom/b/b/b;

    invoke-virtual {v0}, Lcom/b/b/b;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/LocalDate;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Lorg/threeten/bp/LocalDate;->e(J)Lorg/threeten/bp/LocalDate;

    move-result-object v1

    .line 241
    invoke-virtual {p1, v1}, Lcom/c/b/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/LocalDate;

    .line 242
    const/16 v2, 0x65

    .line 243
    invoke-virtual {p1}, Lcom/c/b/b;->b()Z

    move-result v3

    invoke-static {v2, v0, v1, v3}, Lco/uk/getmondo/payments/send/bank/payment/m;->a(ILorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Z)Lco/uk/getmondo/payments/send/bank/payment/m;

    move-result-object v0

    .line 244
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    const-string v2, "DATE_PICKER_FRAGMENT_TAG"

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/payments/send/bank/payment/m;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 245
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 202
    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 203
    return-void
.end method

.method public a(Lorg/threeten/bp/LocalDate;)V
    .locals 2

    .prologue
    .line 215
    invoke-static {}, Lorg/threeten/bp/LocalDate;->a()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/threeten/bp/LocalDate;->d(Lorg/threeten/bp/chrono/ChronoLocalDate;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0a033f

    .line 216
    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 217
    :goto_0
    iget-object v1, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentStartTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 218
    return-void

    .line 216
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->f:Lorg/threeten/bp/format/DateTimeFormatter;

    invoke-virtual {p1, v0}, Lorg/threeten/bp/LocalDate;->a(Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lorg/threeten/bp/LocalDate;I)V
    .locals 2

    .prologue
    .line 117
    packed-switch p2, :pswitch_data_0

    .line 125
    :goto_0
    return-void

    .line 119
    :pswitch_0
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->c:Lcom/b/b/b;

    invoke-static {p1}, Lcom/c/b/b;->b(Ljava/lang/Object;)Lcom/c/b/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/b/b/b;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 122
    :pswitch_1
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->b:Lcom/b/b/b;

    invoke-virtual {v0, p1}, Lcom/b/b/b;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 117
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 258
    iget-object v1, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentEndRow:Landroid/view/ViewGroup;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 259
    return-void

    .line 258
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 148
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->amountInputView:Lco/uk/getmondo/common/ui/AmountInputView;

    invoke-virtual {v0}, Lco/uk/getmondo/common/ui/AmountInputView;->a()Lio/reactivex/n;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/payments/send/bank/payment/b;->a()Lio/reactivex/c/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public b(Lco/uk/getmondo/payments/a/a/d$c;)V
    .locals 3

    .prologue
    .line 229
    invoke-static {p1}, Lco/uk/getmondo/payments/send/bank/payment/p;->a(Lco/uk/getmondo/payments/a/a/d$c;)Lco/uk/getmondo/payments/send/bank/payment/p;

    move-result-object v0

    .line 230
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    const-string v2, "INTERVAL_PICKER_FRAGMENT_TAG"

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/payments/send/bank/payment/p;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 231
    return-void
.end method

.method public b(Lcom/c/b/b;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/c/b/b",
            "<",
            "Lorg/threeten/bp/LocalDate;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 249
    invoke-virtual {p1}, Lcom/c/b/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250
    iget-object v1, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentEndTextView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/c/b/b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/LocalDate;

    iget-object v2, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->f:Lorg/threeten/bp/format/DateTimeFormatter;

    invoke-virtual {v0, v2}, Lorg/threeten/bp/LocalDate;->a(Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 254
    :goto_0
    return-void

    .line 252
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentEndTextView:Landroid/widget/TextView;

    const v1, 0x7f0a033b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public b(Lorg/threeten/bp/LocalDate;)V
    .locals 3

    .prologue
    .line 222
    const/16 v0, 0x64

    .line 223
    invoke-static {}, Lorg/threeten/bp/LocalDate;->a()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    const/4 v2, 0x0

    .line 222
    invoke-static {v0, p1, v1, v2}, Lco/uk/getmondo/payments/send/bank/payment/m;->a(ILorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Z)Lco/uk/getmondo/payments/send/bank/payment/m;

    move-result-object v0

    .line 224
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    const-string v2, "DATE_PICKER_FRAGMENT_TAG"

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/payments/send/bank/payment/m;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 225
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->nextStepButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 264
    return-void
.end method

.method public c()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/payments/send/bank/payment/s;",
            ">;"
        }
    .end annotation

    .prologue
    .line 153
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->nextStepButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/payments/send/bank/payment/c;->a(Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;)Lio/reactivex/c/h;

    move-result-object v1

    .line 154
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 153
    return-object v0
.end method

.method public c(Lco/uk/getmondo/payments/a/a/d$c;)V
    .locals 2

    .prologue
    .line 235
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentIntervalTextView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lco/uk/getmondo/payments/a/a/d$c;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 236
    return-void
.end method

.method public c(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 268
    if-eqz p1, :cond_2

    move v0, v1

    .line 269
    :goto_0
    iget-object v3, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentIntervalRow:Landroid/view/ViewGroup;

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 270
    iget-object v3, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentStartRow:Landroid/view/ViewGroup;

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 272
    if-eqz p1, :cond_3

    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->e:Lcom/b/b/b;

    invoke-virtual {v0}, Lcom/b/b/b;->b()Ljava/lang/Object;

    move-result-object v0

    sget-object v3, Lco/uk/getmondo/payments/a/a/d$c;->a:Lco/uk/getmondo/payments/a/a/d$c;

    if-eq v0, v3, :cond_3

    .line 273
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentEndRow:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 279
    :cond_0
    :goto_1
    if-eqz p1, :cond_1

    .line 280
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentSwitch:Landroid/widget/Switch;

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->a(Landroid/view/View;)V

    .line 282
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 268
    goto :goto_0

    .line 274
    :cond_3
    if-nez p1, :cond_0

    .line 275
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentEndRow:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1
.end method

.method public d()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 164
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentSwitch:Landroid/widget/Switch;

    invoke-static {v0}, Lcom/b/a/d/d;->a(Landroid/widget/CompoundButton;)Lcom/b/a/a;

    move-result-object v0

    return-object v0
.end method

.method public e()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lorg/threeten/bp/LocalDate;",
            ">;"
        }
    .end annotation

    .prologue
    .line 169
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentStartRow:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/payments/send/bank/payment/d;->a(Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;)Lio/reactivex/c/h;

    move-result-object v1

    .line 170
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 169
    return-object v0
.end method

.method public f()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lorg/threeten/bp/LocalDate;",
            ">;"
        }
    .end annotation

    .prologue
    .line 175
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->b:Lcom/b/b/b;

    return-object v0
.end method

.method public g()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/payments/a/a/d$c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 180
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentIntervalRow:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/payments/send/bank/payment/e;->a(Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;)Lio/reactivex/c/h;

    move-result-object v1

    .line 181
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 180
    return-object v0
.end method

.method public h()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/payments/a/a/d$c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->e:Lcom/b/b/b;

    return-object v0
.end method

.method public i()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lcom/c/b/b",
            "<",
            "Lorg/threeten/bp/LocalDate;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 191
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentEndRow:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/payments/send/bank/payment/f;->a(Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;)Lio/reactivex/c/h;

    move-result-object v1

    .line 192
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 191
    return-object v0
.end method

.method public j()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lcom/c/b/b",
            "<",
            "Lorg/threeten/bp/LocalDate;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 197
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->c:Lcom/b/b/b;

    return-object v0
.end method

.method public k()V
    .locals 2

    .prologue
    .line 291
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->referenceInputLayout:Landroid/support/design/widget/TextInputLayout;

    const v1, 0x7f0a00eb

    invoke-virtual {p0, v1}, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 292
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 93
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 94
    const v0, 0x7f050023

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->setContentView(I)V

    .line 95
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 97
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "KEY_RECIPIENT_BANK_ACCOUNT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/send/data/a/b;

    .line 98
    if-nez v0, :cond_0

    .line 99
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "This activity requires a Payee"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 101
    :cond_0
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v1

    new-instance v2, Lco/uk/getmondo/payments/send/bank/payment/i;

    invoke-direct {v2, v0}, Lco/uk/getmondo/payments/send/bank/payment/i;-><init>(Lco/uk/getmondo/payments/send/data/a/b;)V

    invoke-interface {v1, v2}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/payments/send/bank/payment/i;)Lco/uk/getmondo/payments/send/bank/payment/h;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/payments/send/bank/payment/h;->a(Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;)V

    .line 103
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->a:Lco/uk/getmondo/payments/send/bank/payment/k;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/payments/send/bank/payment/k;->a(Lco/uk/getmondo/payments/send/bank/payment/k$a;)V

    .line 105
    invoke-direct {p0}, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->w()V

    .line 106
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->a:Lco/uk/getmondo/payments/send/bank/payment/k;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/bank/payment/k;->b()V

    .line 111
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 112
    return-void
.end method

.method public v()V
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->referenceInputLayout:Landroid/support/design/widget/TextInputLayout;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 297
    return-void
.end method
