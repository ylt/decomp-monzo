.class public Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity_ViewBinding;
.super Ljava/lang/Object;
.source "BankPaymentDetailsActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity_ViewBinding;->a:Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;

    .line 32
    const v0, 0x7f110111

    const-string v1, "field \'amountInputView\'"

    const-class v2, Lco/uk/getmondo/common/ui/AmountInputView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/AmountInputView;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->amountInputView:Lco/uk/getmondo/common/ui/AmountInputView;

    .line 33
    const v0, 0x7f110112

    const-string v1, "field \'referenceInputLayout\'"

    const-class v2, Landroid/support/design/widget/TextInputLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->referenceInputLayout:Landroid/support/design/widget/TextInputLayout;

    .line 34
    const v0, 0x7f110113

    const-string v1, "field \'referenceEditText\'"

    const-class v2, Landroid/widget/EditText;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->referenceEditText:Landroid/widget/EditText;

    .line 35
    const v0, 0x7f11011b

    const-string v1, "field \'nextStepButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->nextStepButton:Landroid/widget/Button;

    .line 36
    const v0, 0x7f110114

    const-string v1, "field \'schedulePaymentSwitch\'"

    const-class v2, Landroid/widget/Switch;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentSwitch:Landroid/widget/Switch;

    .line 37
    const v0, 0x7f110117

    const-string v1, "field \'schedulePaymentIntervalRow\'"

    const-class v2, Landroid/view/ViewGroup;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentIntervalRow:Landroid/view/ViewGroup;

    .line 38
    const v0, 0x7f110115

    const-string v1, "field \'schedulePaymentStartRow\'"

    const-class v2, Landroid/view/ViewGroup;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentStartRow:Landroid/view/ViewGroup;

    .line 39
    const v0, 0x7f110116

    const-string v1, "field \'schedulePaymentStartTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentStartTextView:Landroid/widget/TextView;

    .line 40
    const v0, 0x7f110118

    const-string v1, "field \'schedulePaymentIntervalTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentIntervalTextView:Landroid/widget/TextView;

    .line 41
    const v0, 0x7f110119

    const-string v1, "field \'schedulePaymentEndRow\'"

    const-class v2, Landroid/view/ViewGroup;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentEndRow:Landroid/view/ViewGroup;

    .line 42
    const v0, 0x7f11011a

    const-string v1, "field \'schedulePaymentEndTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentEndTextView:Landroid/widget/TextView;

    .line 43
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 48
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity_ViewBinding;->a:Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;

    .line 49
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity_ViewBinding;->a:Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;

    .line 52
    iput-object v1, v0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->amountInputView:Lco/uk/getmondo/common/ui/AmountInputView;

    .line 53
    iput-object v1, v0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->referenceInputLayout:Landroid/support/design/widget/TextInputLayout;

    .line 54
    iput-object v1, v0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->referenceEditText:Landroid/widget/EditText;

    .line 55
    iput-object v1, v0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->nextStepButton:Landroid/widget/Button;

    .line 56
    iput-object v1, v0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentSwitch:Landroid/widget/Switch;

    .line 57
    iput-object v1, v0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentIntervalRow:Landroid/view/ViewGroup;

    .line 58
    iput-object v1, v0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentStartRow:Landroid/view/ViewGroup;

    .line 59
    iput-object v1, v0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentStartTextView:Landroid/widget/TextView;

    .line 60
    iput-object v1, v0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentIntervalTextView:Landroid/widget/TextView;

    .line 61
    iput-object v1, v0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentEndRow:Landroid/view/ViewGroup;

    .line 62
    iput-object v1, v0, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->schedulePaymentEndTextView:Landroid/widget/TextView;

    .line 63
    return-void
.end method
