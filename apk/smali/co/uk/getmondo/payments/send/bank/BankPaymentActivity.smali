.class public Lco/uk/getmondo/payments/send/bank/BankPaymentActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "BankPaymentActivity.java"

# interfaces
.implements Lco/uk/getmondo/payments/send/bank/b$a;


# instance fields
.field a:Lco/uk/getmondo/payments/send/bank/b;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 20
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/payments/send/bank/BankPaymentActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/d/c;)V
    .locals 5

    .prologue
    .line 39
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/bank/BankPaymentActivity;->getSupportActionBar()Landroid/support/v7/app/a;

    move-result-object v0

    .line 40
    if-eqz v0, :cond_0

    .line 41
    const v1, 0x7f0a00e5

    invoke-virtual {p0, v1}, Lco/uk/getmondo/payments/send/bank/BankPaymentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lco/uk/getmondo/d/c;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->b(Ljava/lang/CharSequence;)V

    .line 43
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 26
    const v0, 0x7f050022

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/bank/BankPaymentActivity;->setContentView(I)V

    .line 27
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/bank/BankPaymentActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/payments/send/bank/BankPaymentActivity;)V

    .line 28
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/BankPaymentActivity;->a:Lco/uk/getmondo/payments/send/bank/b;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/payments/send/bank/b;->a(Lco/uk/getmondo/payments/send/bank/b$a;)V

    .line 29
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/BankPaymentActivity;->a:Lco/uk/getmondo/payments/send/bank/b;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/bank/b;->b()V

    .line 34
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 35
    return-void
.end method
