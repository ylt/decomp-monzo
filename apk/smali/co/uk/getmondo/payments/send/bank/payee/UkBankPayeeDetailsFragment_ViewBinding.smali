.class public Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment_ViewBinding;
.super Ljava/lang/Object;
.source "UkBankPayeeDetailsFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment_ViewBinding;->a:Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;

    .line 24
    const v0, 0x7f110356

    const-string v1, "field \'recipientInputLayout\'"

    const-class v2, Landroid/support/design/widget/TextInputLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->recipientInputLayout:Landroid/support/design/widget/TextInputLayout;

    .line 25
    const v0, 0x7f110357

    const-string v1, "field \'recipientEditText\'"

    const-class v2, Landroid/widget/EditText;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->recipientEditText:Landroid/widget/EditText;

    .line 26
    const v0, 0x7f110358

    const-string v1, "field \'sortCodeInputLayout\'"

    const-class v2, Landroid/support/design/widget/TextInputLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->sortCodeInputLayout:Landroid/support/design/widget/TextInputLayout;

    .line 27
    const v0, 0x7f110359

    const-string v1, "field \'sortCodeEditText\'"

    const-class v2, Landroid/widget/EditText;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->sortCodeEditText:Landroid/widget/EditText;

    .line 28
    const v0, 0x7f11035a

    const-string v1, "field \'accountNumberInputLayout\'"

    const-class v2, Landroid/support/design/widget/TextInputLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->accountNumberInputLayout:Landroid/support/design/widget/TextInputLayout;

    .line 29
    const v0, 0x7f11035b

    const-string v1, "field \'accountNumberEditText\'"

    const-class v2, Landroid/widget/EditText;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->accountNumberEditText:Landroid/widget/EditText;

    .line 30
    const v0, 0x7f11035c

    const-string v1, "field \'nextButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->nextButton:Landroid/widget/Button;

    .line 31
    const v0, 0x7f11035d

    const-string v1, "field \'progress\'"

    const-class v2, Landroid/widget/ProgressBar;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->progress:Landroid/widget/ProgressBar;

    .line 32
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment_ViewBinding;->a:Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;

    .line 38
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment_ViewBinding;->a:Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;

    .line 41
    iput-object v1, v0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->recipientInputLayout:Landroid/support/design/widget/TextInputLayout;

    .line 42
    iput-object v1, v0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->recipientEditText:Landroid/widget/EditText;

    .line 43
    iput-object v1, v0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->sortCodeInputLayout:Landroid/support/design/widget/TextInputLayout;

    .line 44
    iput-object v1, v0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->sortCodeEditText:Landroid/widget/EditText;

    .line 45
    iput-object v1, v0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->accountNumberInputLayout:Landroid/support/design/widget/TextInputLayout;

    .line 46
    iput-object v1, v0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->accountNumberEditText:Landroid/widget/EditText;

    .line 47
    iput-object v1, v0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->nextButton:Landroid/widget/Button;

    .line 48
    iput-object v1, v0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->progress:Landroid/widget/ProgressBar;

    .line 49
    return-void
.end method
