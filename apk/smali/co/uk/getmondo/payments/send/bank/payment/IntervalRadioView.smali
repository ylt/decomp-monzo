.class public Lco/uk/getmondo/payments/send/bank/payment/IntervalRadioView;
.super Landroid/widget/LinearLayout;
.source "IntervalRadioView.java"

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field private a:Lco/uk/getmondo/payments/a/a/d$c;

.field descriptionTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1104b0
    .end annotation
.end field

.field radioButton:Landroid/widget/RadioButton;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1104af
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 30
    invoke-direct {p0}, Lco/uk/getmondo/payments/send/bank/payment/IntervalRadioView;->a()V

    .line 31
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 44
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/bank/payment/IntervalRadioView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 45
    const v1, 0x7f050141

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 46
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 47
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 76
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/bank/payment/IntervalRadioView;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/IntervalRadioView;->a:Lco/uk/getmondo/payments/a/a/d$c;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/d$c;->d()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/IntervalRadioView;->descriptionTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 81
    :goto_0
    return-void

    .line 79
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/IntervalRadioView;->descriptionTextView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/IntervalRadioView;->radioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    return v0
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/IntervalRadioView;->radioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, p1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 61
    invoke-direct {p0}, Lco/uk/getmondo/payments/send/bank/payment/IntervalRadioView;->b()V

    .line 62
    return-void
.end method

.method public setInterval(Lco/uk/getmondo/payments/a/a/d$c;)V
    .locals 2

    .prologue
    .line 50
    iput-object p1, p0, Lco/uk/getmondo/payments/send/bank/payment/IntervalRadioView;->a:Lco/uk/getmondo/payments/a/a/d$c;

    .line 51
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/IntervalRadioView;->radioButton:Landroid/widget/RadioButton;

    invoke-virtual {p1}, Lco/uk/getmondo/payments/a/a/d$c;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(I)V

    .line 52
    invoke-virtual {p1}, Lco/uk/getmondo/payments/a/a/d$c;->d()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/IntervalRadioView;->descriptionTextView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lco/uk/getmondo/payments/a/a/d$c;->d()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 55
    :cond_0
    invoke-direct {p0}, Lco/uk/getmondo/payments/send/bank/payment/IntervalRadioView;->b()V

    .line 56
    return-void
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/IntervalRadioView;->radioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->toggle()V

    .line 72
    invoke-direct {p0}, Lco/uk/getmondo/payments/send/bank/payment/IntervalRadioView;->b()V

    .line 73
    return-void
.end method
