.class public Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;
.super Lco/uk/getmondo/common/f/a;
.source "UkBankPayeeDetailsFragment.java"

# interfaces
.implements Lco/uk/getmondo/payments/send/bank/payee/g$a;


# instance fields
.field a:Lco/uk/getmondo/payments/send/bank/payee/g;

.field accountNumberEditText:Landroid/widget/EditText;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11035b
    .end annotation
.end field

.field accountNumberInputLayout:Landroid/support/design/widget/TextInputLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11035a
    .end annotation
.end field

.field private c:Lbutterknife/Unbinder;

.field nextButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11035c
    .end annotation
.end field

.field progress:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11035d
    .end annotation
.end field

.field recipientEditText:Landroid/widget/EditText;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110357
    .end annotation
.end field

.field recipientInputLayout:Landroid/support/design/widget/TextInputLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110356
    .end annotation
.end field

.field sortCodeEditText:Landroid/widget/EditText;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110359
    .end annotation
.end field

.field sortCodeInputLayout:Landroid/support/design/widget/TextInputLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110358
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lco/uk/getmondo/common/f/a;-><init>()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;Ljava/lang/Object;)Lco/uk/getmondo/payments/send/data/a/b;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 105
    new-instance v0, Lco/uk/getmondo/payments/send/data/a/b;

    iget-object v1, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->recipientEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->sortCodeEditText:Landroid/widget/EditText;

    .line 106
    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "-"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->accountNumberEditText:Landroid/widget/EditText;

    .line 107
    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lco/uk/getmondo/payments/send/data/a/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    return-object v0
.end method

.method static synthetic a(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 79
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "-"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->sortCodeInputLayout:Landroid/support/design/widget/TextInputLayout;

    const v1, 0x7f0a0425

    invoke-virtual {p0, v1}, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 133
    return-void
.end method

.method public a(Lco/uk/getmondo/payments/send/data/a/b;)V
    .locals 1

    .prologue
    .line 117
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    invoke-static {v0, p1}, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->a(Landroid/content/Context;Lco/uk/getmondo/payments/send/data/a/b;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->startActivity(Landroid/content/Intent;)V

    .line 118
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->nextButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 113
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->accountNumberInputLayout:Landroid/support/design/widget/TextInputLayout;

    const v1, 0x7f0a0422

    invoke-virtual {p0, v1}, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 143
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 164
    const v0, 0x7f0a0423

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->b(I)V

    .line 165
    return-void
.end method

.method public d()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->recipientEditText:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/b/a/d/e;->c(Landroid/widget/TextView;)Lcom/b/a/a;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/payments/send/bank/payee/b;->a()Lio/reactivex/c/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/b/a/a;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public e()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->sortCodeEditText:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/b/a/d/e;->c(Landroid/widget/TextView;)Lcom/b/a/a;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/payments/send/bank/payee/c;->a()Lio/reactivex/c/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/b/a/a;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public f()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->accountNumberEditText:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/b/a/d/e;->c(Landroid/widget/TextView;)Lcom/b/a/a;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/payments/send/bank/payee/d;->a()Lio/reactivex/c/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/b/a/a;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public g()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->recipientEditText:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/b/a/c/c;->b(Landroid/view/View;)Lcom/b/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/b/a/a;->b()Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public h()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->sortCodeEditText:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/b/a/c/c;->b(Landroid/view/View;)Lcom/b/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/b/a/a;->b()Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public i()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->accountNumberEditText:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/b/a/c/c;->b(Landroid/view/View;)Lcom/b/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/b/a/a;->b()Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public j()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/payments/send/data/a/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->nextButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->accountNumberEditText:Landroid/widget/EditText;

    invoke-static {v1}, Lcom/b/a/d/e;->b(Landroid/widget/TextView;)Lio/reactivex/n;

    move-result-object v1

    invoke-static {v0, v1}, Lio/reactivex/n;->merge(Lio/reactivex/r;Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/payments/send/bank/payee/e;->a(Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;)Lio/reactivex/c/h;

    move-result-object v1

    .line 105
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 104
    return-object v0
.end method

.method public k()V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->recipientInputLayout:Landroid/support/design/widget/TextInputLayout;

    const v1, 0x7f0a0424

    invoke-virtual {p0, v1}, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 123
    return-void
.end method

.method public l()V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->recipientInputLayout:Landroid/support/design/widget/TextInputLayout;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 128
    return-void
.end method

.method public m()V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->sortCodeInputLayout:Landroid/support/design/widget/TextInputLayout;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 138
    return-void
.end method

.method public n()V
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->accountNumberInputLayout:Landroid/support/design/widget/TextInputLayout;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 148
    return-void
.end method

.method public o()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 152
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->nextButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 153
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->progress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 154
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 45
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onCreate(Landroid/os/Bundle;)V

    .line 46
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->B()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;)V

    .line 47
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 52
    const v0, 0x7f0500b1

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->c:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 68
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->a:Lco/uk/getmondo/payments/send/bank/payee/g;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/bank/payee/g;->b()V

    .line 69
    invoke-super {p0}, Lco/uk/getmondo/common/f/a;->onDestroyView()V

    .line 70
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 57
    invoke-super {p0, p1, p2}, Lco/uk/getmondo/common/f/a;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 58
    invoke-static {p0, p1}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->c:Lbutterknife/Unbinder;

    .line 59
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->a:Lco/uk/getmondo/payments/send/bank/payee/g;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/payments/send/bank/payee/g;->a(Lco/uk/getmondo/payments/send/bank/payee/g$a;)V

    .line 61
    const/16 v0, 0x2d

    invoke-static {v0}, Lco/uk/getmondo/common/k/i;->a(C)Lco/uk/getmondo/common/k/i;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/k/i;->a(I)Lco/uk/getmondo/common/k/i;

    move-result-object v0

    .line 62
    iget-object v1, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->sortCodeEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Lco/uk/getmondo/common/k/i;->a()Landroid/text/TextWatcher;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 63
    return-void
.end method

.method public p()V
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->nextButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 159
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payee/UkBankPayeeDetailsFragment;->progress:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 160
    return-void
.end method
