.class public interface abstract Lco/uk/getmondo/payments/send/bank/payment/k$a;
.super Ljava/lang/Object;
.source "BankPaymentDetailsPresenter.kt"

# interfaces
.implements Lco/uk/getmondo/common/e/a$a;
.implements Lco/uk/getmondo/common/ui/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/payments/send/bank/payment/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\n\u0008`\u0018\u00002\u00020\u00012\u00020\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H&J\u000e\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H&J\u0014\u0010\u0008\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\n0\t0\u0006H&J\u0014\u0010\u000b\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\n0\t0\u0006H&J\u000e\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\r0\u0006H&J\u000e\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\r0\u0006H&J\u000e\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u0006H&J\u000e\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H&J\u000e\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0006H&J\u000e\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\n0\u0006H&J\u000e\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\n0\u0006H&J\u0016\u0010\u0016\u001a\u00020\u00042\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\n0\tH&J\u0010\u0010\u0018\u001a\u00020\u00042\u0006\u0010\u0019\u001a\u00020\rH&J\u0010\u0010\u001a\u001a\u00020\u00042\u0006\u0010\u001b\u001a\u00020\u001cH&J\u0010\u0010\u001d\u001a\u00020\u00042\u0006\u0010\u001e\u001a\u00020\nH&J\u0010\u0010\u001f\u001a\u00020\u00042\u0006\u0010 \u001a\u00020\u0013H&J\u0010\u0010!\u001a\u00020\u00042\u0006\u0010\"\u001a\u00020\u0013H&J\u0010\u0010#\u001a\u00020\u00042\u0006\u0010 \u001a\u00020\u0013H&J\u0010\u0010$\u001a\u00020\u00042\u0006\u0010%\u001a\u00020&H&J\u0008\u0010\'\u001a\u00020\u0004H&J\u0010\u0010(\u001a\u00020\u00042\u0006\u0010)\u001a\u00020\u0007H&J\u0016\u0010*\u001a\u00020\u00042\u000c\u0010+\u001a\u0008\u0012\u0004\u0012\u00020\n0\tH&J\u0010\u0010,\u001a\u00020\u00042\u0006\u0010-\u001a\u00020\rH&J\u0010\u0010.\u001a\u00020\u00042\u0006\u0010/\u001a\u00020\nH&\u00a8\u00060"
    }
    d2 = {
        "Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsPresenter$View;",
        "Lco/uk/getmondo/common/ui/MvpView;",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;",
        "clearInvalidReferenceError",
        "",
        "onAmountChanged",
        "Lio/reactivex/Observable;",
        "",
        "onEndDateClicked",
        "Lcom/memoizrlabs/poweroptional/Optional;",
        "Lorg/threeten/bp/LocalDate;",
        "onEndDateSelected",
        "onIntervalClicked",
        "Lco/uk/getmondo/payments/data/model/PaymentSchedule$Interval;",
        "onIntervalSelected",
        "onNextStepClicked",
        "Lco/uk/getmondo/payments/send/bank/payment/PaymentDetailsFormData;",
        "onReferenceChanged",
        "onScheduleToggled",
        "",
        "onStartDateClicked",
        "onStartDateSelected",
        "openEndDatePicker",
        "optionalDefaultDate",
        "openIntervalPicker",
        "defaultInterval",
        "openPaymentAuthentication",
        "bankPayment",
        "Lco/uk/getmondo/payments/send/data/model/BankPayment;",
        "openStartDatePicker",
        "defaultDate",
        "setEndDateVisible",
        "visible",
        "setNextStepButtonEnabled",
        "enabled",
        "setScheduleOptionsVisible",
        "showBalance",
        "balance",
        "Lco/uk/getmondo/model/Amount;",
        "showInvalidReference",
        "showRecipientName",
        "name",
        "showSelectedEndDate",
        "optionalEndDate",
        "showSelectedInterval",
        "interval",
        "showSelectedStartDate",
        "startDate",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# virtual methods
.method public abstract a()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Lco/uk/getmondo/d/c;)V
.end method

.method public abstract a(Lco/uk/getmondo/payments/send/data/a/c;)V
.end method

.method public abstract a(Lcom/c/b/b;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/c/b/b",
            "<",
            "Lorg/threeten/bp/LocalDate;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract a(Lorg/threeten/bp/LocalDate;)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract b()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract b(Lco/uk/getmondo/payments/a/a/d$c;)V
.end method

.method public abstract b(Lcom/c/b/b;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/c/b/b",
            "<",
            "Lorg/threeten/bp/LocalDate;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract b(Lorg/threeten/bp/LocalDate;)V
.end method

.method public abstract b(Z)V
.end method

.method public abstract c()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/payments/send/bank/payment/s;",
            ">;"
        }
    .end annotation
.end method

.method public abstract c(Lco/uk/getmondo/payments/a/a/d$c;)V
.end method

.method public abstract c(Z)V
.end method

.method public abstract d()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract e()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lorg/threeten/bp/LocalDate;",
            ">;"
        }
    .end annotation
.end method

.method public abstract f()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lorg/threeten/bp/LocalDate;",
            ">;"
        }
    .end annotation
.end method

.method public abstract g()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/payments/a/a/d$c;",
            ">;"
        }
    .end annotation
.end method

.method public abstract h()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/payments/a/a/d$c;",
            ">;"
        }
    .end annotation
.end method

.method public abstract i()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lcom/c/b/b",
            "<",
            "Lorg/threeten/bp/LocalDate;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract j()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lcom/c/b/b",
            "<",
            "Lorg/threeten/bp/LocalDate;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract k()V
.end method

.method public abstract v()V
.end method
