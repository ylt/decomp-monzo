.class public Lco/uk/getmondo/payments/send/bank/payment/m;
.super Landroid/support/v4/app/i;
.source "DatePickerDialogFragment.java"

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/payments/send/bank/payment/m$a;
    }
.end annotation


# instance fields
.field private a:Lco/uk/getmondo/payments/send/bank/payment/m$a;

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v4/app/i;-><init>()V

    return-void
.end method

.method public static a(ILorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Z)Lco/uk/getmondo/payments/send/bank/payment/m;
    .locals 2

    .prologue
    .line 31
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 32
    const-string v1, "KEY_REQUEST_CODE"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 33
    const-string v1, "KEY_DEFAULT_DATE"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 34
    const-string v1, "KEY_MIN_DATE"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 35
    const-string v1, "KEY_CAN_REMOVE_DATE"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 36
    new-instance v1, Lco/uk/getmondo/payments/send/bank/payment/m;

    invoke-direct {v1}, Lco/uk/getmondo/payments/send/bank/payment/m;-><init>()V

    .line 37
    invoke-virtual {v1, v0}, Lco/uk/getmondo/payments/send/bank/payment/m;->setArguments(Landroid/os/Bundle;)V

    .line 38
    return-object v1
.end method

.method static synthetic a(Landroid/app/DatePickerDialog;Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 65
    const/4 v0, -0x3

    invoke-virtual {p0, v0}, Landroid/app/DatePickerDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 66
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/bank/payment/m;Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/m;->a:Lco/uk/getmondo/payments/send/bank/payment/m$a;

    iget v1, p0, Lco/uk/getmondo/payments/send/bank/payment/m;->b:I

    invoke-interface {v0, v1}, Lco/uk/getmondo/payments/send/bank/payment/m$a;->a(I)V

    return-void
.end method


# virtual methods
.method public onAttach(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 43
    invoke-super {p0, p1}, Landroid/support/v4/app/i;->onAttach(Landroid/content/Context;)V

    .line 44
    instance-of v0, p1, Lco/uk/getmondo/payments/send/bank/payment/m$a;

    if-eqz v0, :cond_0

    .line 45
    check-cast p1, Lco/uk/getmondo/payments/send/bank/payment/m$a;

    iput-object p1, p0, Lco/uk/getmondo/payments/send/bank/payment/m;->a:Lco/uk/getmondo/payments/send/bank/payment/m$a;

    .line 49
    return-void

    .line 47
    :cond_0
    new-instance v0, Ljava/lang/ClassCastException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement DateListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 54
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/bank/payment/m;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "KEY_REQUEST_CODE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lco/uk/getmondo/payments/send/bank/payment/m;->b:I

    .line 55
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/bank/payment/m;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "KEY_MIN_DATE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lorg/threeten/bp/LocalDate;

    .line 56
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/bank/payment/m;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "KEY_CAN_REMOVE_DATE"

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    .line 57
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/bank/payment/m;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "KEY_DEFAULT_DATE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lorg/threeten/bp/LocalDate;

    .line 59
    new-instance v0, Landroid/app/DatePickerDialog;

    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/bank/payment/m;->getActivity()Landroid/support/v4/app/j;

    move-result-object v1

    invoke-virtual {v2}, Lorg/threeten/bp/LocalDate;->d()I

    move-result v3

    .line 60
    invoke-virtual {v2}, Lorg/threeten/bp/LocalDate;->e()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v2}, Lorg/threeten/bp/LocalDate;->g()I

    move-result v5

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    .line 61
    if-eqz v7, :cond_0

    .line 62
    const/4 v1, -0x3

    const v2, 0x7f0a0134

    invoke-virtual {p0, v2}, Lco/uk/getmondo/payments/send/bank/payment/m;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0}, Lco/uk/getmondo/payments/send/bank/payment/n;->a(Lco/uk/getmondo/payments/send/bank/payment/m;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/DatePickerDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 64
    invoke-static {v0}, Lco/uk/getmondo/payments/send/bank/payment/o;->a(Landroid/app/DatePickerDialog;)Landroid/content/DialogInterface$OnShowListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/DatePickerDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 69
    :cond_0
    invoke-static {v8, v8}, Lorg/threeten/bp/LocalTime;->a(II)Lorg/threeten/bp/LocalTime;

    move-result-object v1

    invoke-static {}, Lorg/threeten/bp/ZoneId;->a()Lorg/threeten/bp/ZoneId;

    move-result-object v2

    invoke-static {v6, v1, v2}, Lorg/threeten/bp/ZonedDateTime;->a(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/ZonedDateTime;->j()Lorg/threeten/bp/Instant;

    move-result-object v1

    .line 70
    invoke-virtual {v0}, Landroid/app/DatePickerDialog;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v2

    invoke-virtual {v1}, Lorg/threeten/bp/Instant;->c()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/widget/DatePicker;->setMinDate(J)V

    .line 71
    return-object v0
.end method

.method public onDateSet(Landroid/widget/DatePicker;III)V
    .locals 3

    .prologue
    .line 76
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/m;->a:Lco/uk/getmondo/payments/send/bank/payment/m$a;

    add-int/lit8 v1, p3, 0x1

    invoke-static {p2, v1, p4}, Lorg/threeten/bp/LocalDate;->a(III)Lorg/threeten/bp/LocalDate;

    move-result-object v1

    iget v2, p0, Lco/uk/getmondo/payments/send/bank/payment/m;->b:I

    invoke-interface {v0, v1, v2}, Lco/uk/getmondo/payments/send/bank/payment/m$a;->a(Lorg/threeten/bp/LocalDate;I)V

    .line 77
    return-void
.end method
