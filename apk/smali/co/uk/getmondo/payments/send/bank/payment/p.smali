.class public Lco/uk/getmondo/payments/send/bank/payment/p;
.super Landroid/support/v4/app/i;
.source "IntervalPickerDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/payments/send/bank/payment/p$a;
    }
.end annotation


# instance fields
.field private a:Lco/uk/getmondo/payments/send/bank/payment/p$a;

.field private b:Lco/uk/getmondo/payments/a/a/d$c;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v4/app/i;-><init>()V

    .line 25
    sget-object v0, Lco/uk/getmondo/payments/a/a/d$c;->a:Lco/uk/getmondo/payments/a/a/d$c;

    iput-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/p;->b:Lco/uk/getmondo/payments/a/a/d$c;

    return-void
.end method

.method public static a(Lco/uk/getmondo/payments/a/a/d$c;)Lco/uk/getmondo/payments/send/bank/payment/p;
    .locals 2

    .prologue
    .line 28
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 29
    const-string v1, "KEY_DEFAULT_INTERVAL"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 30
    new-instance v1, Lco/uk/getmondo/payments/send/bank/payment/p;

    invoke-direct {v1}, Lco/uk/getmondo/payments/send/bank/payment/p;-><init>()V

    .line 31
    invoke-virtual {v1, v0}, Lco/uk/getmondo/payments/send/bank/payment/p;->setArguments(Landroid/os/Bundle;)V

    .line 32
    return-object v1
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/bank/payment/p;Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/p;->a:Lco/uk/getmondo/payments/send/bank/payment/p$a;

    iget-object v1, p0, Lco/uk/getmondo/payments/send/bank/payment/p;->b:Lco/uk/getmondo/payments/a/a/d$c;

    invoke-interface {v0, v1}, Lco/uk/getmondo/payments/send/bank/payment/p$a;->a(Lco/uk/getmondo/payments/a/a/d$c;)V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/bank/payment/p;Landroid/widget/LinearLayout;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 63
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/a/a/d$c;

    iput-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/p;->b:Lco/uk/getmondo/payments/a/a/d$c;

    .line 64
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 65
    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Checkable;

    .line 66
    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-interface {v0, v2}, Landroid/widget/Checkable;->setChecked(Z)V

    .line 64
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 68
    :cond_0
    return-void
.end method


# virtual methods
.method public onAttach(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 45
    invoke-super {p0, p1}, Landroid/support/v4/app/i;->onAttach(Landroid/content/Context;)V

    .line 46
    instance-of v0, p1, Lco/uk/getmondo/payments/send/bank/payment/p$a;

    if-eqz v0, :cond_0

    .line 47
    check-cast p1, Lco/uk/getmondo/payments/send/bank/payment/p$a;

    iput-object p1, p0, Lco/uk/getmondo/payments/send/bank/payment/p;->a:Lco/uk/getmondo/payments/send/bank/payment/p$a;

    .line 51
    return-void

    .line 49
    :cond_0
    new-instance v0, Ljava/lang/ClassCastException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement IntervalSelectedListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 37
    invoke-super {p0, p1}, Landroid/support/v4/app/i;->onCreate(Landroid/os/Bundle;)V

    .line 38
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/bank/payment/p;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/bank/payment/p;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "KEY_DEFAULT_INTERVAL"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/bank/payment/p;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "KEY_DEFAULT_INTERVAL"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/a/a/d$c;

    iput-object v0, p0, Lco/uk/getmondo/payments/send/bank/payment/p;->b:Lco/uk/getmondo/payments/a/a/d$c;

    .line 41
    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 56
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/bank/payment/p;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0113

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 57
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/bank/payment/p;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0115

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 58
    new-instance v3, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/bank/payment/p;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 59
    invoke-virtual {v3, v10}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 60
    invoke-virtual {v3, v1, v1, v1, v2}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 62
    invoke-static {p0, v3}, Lco/uk/getmondo/payments/send/bank/payment/q;->a(Lco/uk/getmondo/payments/send/bank/payment/p;Landroid/widget/LinearLayout;)Landroid/view/View$OnClickListener;

    move-result-object v4

    .line 70
    invoke-static {}, Lco/uk/getmondo/payments/a/a/d$c;->values()[Lco/uk/getmondo/payments/a/a/d$c;

    move-result-object v5

    array-length v6, v5

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v6, :cond_1

    aget-object v7, v5, v0

    .line 71
    new-instance v8, Lco/uk/getmondo/payments/send/bank/payment/IntervalRadioView;

    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/bank/payment/p;->getActivity()Landroid/support/v4/app/j;

    move-result-object v9

    invoke-direct {v8, v9}, Lco/uk/getmondo/payments/send/bank/payment/IntervalRadioView;-><init>(Landroid/content/Context;)V

    .line 72
    invoke-virtual {v8, v1, v2, v1, v2}, Lco/uk/getmondo/payments/send/bank/payment/IntervalRadioView;->setPaddingRelative(IIII)V

    .line 73
    invoke-virtual {v8, v7}, Lco/uk/getmondo/payments/send/bank/payment/IntervalRadioView;->setInterval(Lco/uk/getmondo/payments/a/a/d$c;)V

    .line 74
    invoke-virtual {v8, v7}, Lco/uk/getmondo/payments/send/bank/payment/IntervalRadioView;->setTag(Ljava/lang/Object;)V

    .line 75
    invoke-virtual {v8, v4}, Lco/uk/getmondo/payments/send/bank/payment/IntervalRadioView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    invoke-virtual {v3, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 78
    iget-object v9, p0, Lco/uk/getmondo/payments/send/bank/payment/p;->b:Lco/uk/getmondo/payments/a/a/d$c;

    if-ne v7, v9, :cond_0

    .line 79
    invoke-virtual {v8, v10}, Lco/uk/getmondo/payments/send/bank/payment/IntervalRadioView;->setChecked(Z)V

    .line 70
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 83
    :cond_1
    new-instance v0, Landroid/widget/ScrollView;

    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/bank/payment/p;->getActivity()Landroid/support/v4/app/j;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 84
    invoke-virtual {v0, v3}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 85
    new-instance v1, Landroid/support/v7/app/d$a;

    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/bank/payment/p;->getActivity()Landroid/support/v4/app/j;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v7/app/d$a;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0a01f4

    .line 86
    invoke-virtual {v1, v2}, Landroid/support/v7/app/d$a;->a(I)Landroid/support/v7/app/d$a;

    move-result-object v1

    .line 87
    invoke-virtual {v1, v0}, Landroid/support/v7/app/d$a;->b(Landroid/view/View;)Landroid/support/v7/app/d$a;

    move-result-object v0

    const v1, 0x7f0a01f3

    invoke-static {p0}, Lco/uk/getmondo/payments/send/bank/payment/r;->a(Lco/uk/getmondo/payments/send/bank/payment/p;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    .line 88
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/d$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/d$a;

    move-result-object v0

    .line 90
    invoke-virtual {v0}, Landroid/support/v7/app/d$a;->b()Landroid/support/v7/app/d;

    move-result-object v0

    .line 85
    return-object v0
.end method
