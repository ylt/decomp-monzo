.class Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;
.super Landroid/support/v7/widget/RecyclerView$w;
.source "SendMoneyAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/payments/send/SendMoneyAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ContactViewHolder"
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/payments/send/SendMoneyAdapter;

.field actionTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110419
    .end annotation
.end field

.field private b:I

.field contactImageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101d3
    .end annotation
.end field

.field nameTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101d4
    .end annotation
.end field

.field numberTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110418
    .end annotation
.end field


# direct methods
.method constructor <init>(Lco/uk/getmondo/payments/send/SendMoneyAdapter;Landroid/view/View;Lco/uk/getmondo/payments/send/SendMoneyAdapter$c;)V
    .locals 3

    .prologue
    .line 242
    iput-object p1, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;->a:Lco/uk/getmondo/payments/send/SendMoneyAdapter;

    .line 243
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$w;-><init>(Landroid/view/View;)V

    .line 244
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 246
    const/4 v0, 0x2

    const/high16 v1, 0x41a00000    # 20.0f

    iget-object v2, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;->nameTextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;->b:I

    .line 248
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;->itemView:Landroid/view/View;

    invoke-static {p0, p3}, Lco/uk/getmondo/payments/send/e;->a(Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;Lco/uk/getmondo/payments/send/SendMoneyAdapter$c;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 254
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;Lco/uk/getmondo/payments/send/SendMoneyAdapter$c;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 249
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;->a:Lco/uk/getmondo/payments/send/SendMoneyAdapter;

    invoke-static {v0}, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->b(Lco/uk/getmondo/payments/send/SendMoneyAdapter;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;->getAdapterPosition()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 250
    if-eqz p1, :cond_0

    if-eqz v1, :cond_0

    instance-of v0, v1, Lco/uk/getmondo/payments/send/a/a;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 251
    check-cast v0, Lco/uk/getmondo/payments/send/a/a;

    instance-of v1, v1, Lco/uk/getmondo/payments/send/a/b;

    invoke-interface {p1, v0, v1}, Lco/uk/getmondo/payments/send/SendMoneyAdapter$c;->a(Lco/uk/getmondo/payments/send/a/a;Z)V

    .line 253
    :cond_0
    return-void
.end method


# virtual methods
.method a(Lco/uk/getmondo/payments/send/a/a;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 257
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;->nameTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 258
    instance-of v0, p1, Lco/uk/getmondo/payments/send/a/b;

    if-eqz v0, :cond_1

    .line 259
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;->actionTextView:Landroid/widget/TextView;

    const v1, 0x7f0a0348

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 260
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;->actionTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;->nameTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0f0005

    invoke-static {v1, v2}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 261
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;->numberTextView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 274
    :goto_0
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;->nameTextView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/a/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 275
    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 276
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;->contactImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bumptech/glide/g;->b(Landroid/content/Context;)Lcom/bumptech/glide/j;

    move-result-object v0

    .line 277
    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/a/a;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bumptech/glide/j;->a(Landroid/net/Uri;)Lcom/bumptech/glide/d;

    move-result-object v0

    .line 278
    invoke-virtual {v0}, Lcom/bumptech/glide/d;->h()Lcom/bumptech/glide/b;

    move-result-object v0

    sget-object v1, Lcom/bumptech/glide/load/engine/b;->b:Lcom/bumptech/glide/load/engine/b;

    .line 280
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/b;->a(Lcom/bumptech/glide/load/engine/b;)Lcom/bumptech/glide/a;

    move-result-object v0

    .line 281
    invoke-virtual {v0}, Lcom/bumptech/glide/a;->a()Lcom/bumptech/glide/a;

    move-result-object v0

    new-instance v1, Lco/uk/getmondo/common/ui/c;

    iget-object v2, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;->contactImageView:Landroid/widget/ImageView;

    invoke-direct {v1, v2}, Lco/uk/getmondo/common/ui/c;-><init>(Landroid/widget/ImageView;)V

    .line 282
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/a;->a(Lcom/bumptech/glide/g/b/j;)Lcom/bumptech/glide/g/b/j;

    .line 286
    :cond_0
    :goto_1
    return-void

    .line 263
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;->actionTextView:Landroid/widget/TextView;

    const v2, 0x7f0a0347

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 264
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;->actionTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;->nameTextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0f00b1

    invoke-static {v2, v3}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 265
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;->numberTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    move-object v0, p1

    .line 267
    check-cast v0, Lco/uk/getmondo/payments/send/a/d;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/a/d;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v6, :cond_2

    .line 268
    iget-object v2, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;->numberTextView:Landroid/widget/TextView;

    const v3, 0x7f0a035a

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/Object;

    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/a/a;->c()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/payments/send/a/d;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/a/d;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 270
    :cond_2
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;->numberTextView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/a/a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 283
    :cond_3
    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/a/a;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;->contactImageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;->a:Lco/uk/getmondo/payments/send/SendMoneyAdapter;

    invoke-static {v1}, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->a(Lco/uk/getmondo/payments/send/SendMoneyAdapter;)Lco/uk/getmondo/common/ui/a;

    move-result-object v1

    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/a/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lco/uk/getmondo/common/ui/a;->a(Ljava/lang/String;)Lco/uk/getmondo/common/ui/a$b;

    move-result-object v1

    iget v2, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$ContactViewHolder;->b:I

    invoke-virtual {v1, v2}, Lco/uk/getmondo/common/ui/a$b;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1
.end method
