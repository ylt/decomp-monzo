.class final Lco/uk/getmondo/payments/send/o$e;
.super Ljava/lang/Object;
.source "SendMoneyPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/payments/send/o;->a(Lco/uk/getmondo/payments/send/o$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lco/uk/getmondo/payments/send/data/a/e;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "payee",
        "Lco/uk/getmondo/payments/send/data/model/Payee;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/payments/send/o$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/payments/send/o$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/send/o$e;->a:Lco/uk/getmondo/payments/send/o$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/payments/send/data/a/e;)V
    .locals 2

    .prologue
    .line 106
    .line 107
    instance-of v0, p1, Lco/uk/getmondo/payments/send/data/a/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/payments/send/o$e;->a:Lco/uk/getmondo/payments/send/o$a;

    check-cast p1, Lco/uk/getmondo/payments/send/data/a/b;

    invoke-interface {v0, p1}, Lco/uk/getmondo/payments/send/o$a;->a(Lco/uk/getmondo/payments/send/data/a/b;)V

    .line 110
    :goto_0
    return-void

    .line 108
    :cond_0
    instance-of v0, p1, Lco/uk/getmondo/d/aa;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/payments/send/o$e;->a:Lco/uk/getmondo/payments/send/o$a;

    check-cast p1, Lco/uk/getmondo/d/aa;

    invoke-interface {v0, p1}, Lco/uk/getmondo/payments/send/o$a;->a(Lco/uk/getmondo/d/aa;)V

    goto :goto_0

    .line 109
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected payee type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 35
    check-cast p1, Lco/uk/getmondo/payments/send/data/a/e;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/send/o$e;->a(Lco/uk/getmondo/payments/send/data/a/e;)V

    return-void
.end method
