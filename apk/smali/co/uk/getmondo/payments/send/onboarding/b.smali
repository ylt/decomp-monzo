.class Lco/uk/getmondo/payments/send/onboarding/b;
.super Lco/uk/getmondo/common/ui/b;
.source "PeerToPeerIntroPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/payments/send/onboarding/b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/payments/send/onboarding/b$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/common/e/a;

.field private final f:Lco/uk/getmondo/payments/send/data/h;

.field private final g:Lco/uk/getmondo/common/a;


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/payments/send/data/h;Lco/uk/getmondo/common/a;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 30
    iput-object p1, p0, Lco/uk/getmondo/payments/send/onboarding/b;->c:Lio/reactivex/u;

    .line 31
    iput-object p2, p0, Lco/uk/getmondo/payments/send/onboarding/b;->d:Lio/reactivex/u;

    .line 32
    iput-object p3, p0, Lco/uk/getmondo/payments/send/onboarding/b;->e:Lco/uk/getmondo/common/e/a;

    .line 33
    iput-object p4, p0, Lco/uk/getmondo/payments/send/onboarding/b;->f:Lco/uk/getmondo/payments/send/data/h;

    .line 34
    iput-object p5, p0, Lco/uk/getmondo/payments/send/onboarding/b;->g:Lco/uk/getmondo/common/a;

    .line 35
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/onboarding/b;Lco/uk/getmondo/payments/send/onboarding/b$a;Ljava/lang/Object;)Lio/reactivex/z;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lco/uk/getmondo/payments/send/onboarding/b;->f:Lco/uk/getmondo/payments/send/data/h;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/payments/send/data/h;->a(Z)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/payments/send/onboarding/b;->d:Lio/reactivex/u;

    .line 45
    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/payments/send/onboarding/b;->c:Lio/reactivex/u;

    .line 46
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/payments/send/onboarding/i;->a(Lco/uk/getmondo/payments/send/onboarding/b$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 47
    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/payments/send/onboarding/j;->a(Lco/uk/getmondo/payments/send/onboarding/b$a;)Lio/reactivex/c/b;

    move-result-object v1

    .line 48
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/c/b;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/payments/send/onboarding/k;->a(Lco/uk/getmondo/payments/send/onboarding/b;Lco/uk/getmondo/payments/send/onboarding/b$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 49
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    const/4 v1, 0x0

    .line 50
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    .line 44
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/onboarding/b$a;Lio/reactivex/b/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Lco/uk/getmondo/payments/send/onboarding/b$a;->b(Z)V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/onboarding/b$a;Ljava/lang/Boolean;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 48
    const/4 v0, 0x1

    invoke-interface {p0, v0}, Lco/uk/getmondo/payments/send/onboarding/b$a;->b(Z)V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/onboarding/b$a;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 54
    invoke-interface {p0}, Lco/uk/getmondo/payments/send/onboarding/b$a;->d()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/onboarding/b;Lco/uk/getmondo/payments/send/onboarding/b$a;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lco/uk/getmondo/payments/send/onboarding/b;->e:Lco/uk/getmondo/common/e/a;

    invoke-virtual {v0, p2, p1}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    return-void
.end method

.method static synthetic a(Ljava/lang/Boolean;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 51
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lco/uk/getmondo/payments/send/onboarding/b$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/send/onboarding/b;->a(Lco/uk/getmondo/payments/send/onboarding/b$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/payments/send/onboarding/b$a;)V
    .locals 3

    .prologue
    .line 39
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 41
    iget-object v0, p0, Lco/uk/getmondo/payments/send/onboarding/b;->g:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->J()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 43
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/onboarding/b$a;->a()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/payments/send/onboarding/c;->a(Lco/uk/getmondo/payments/send/onboarding/b;Lco/uk/getmondo/payments/send/onboarding/b$a;)Lio/reactivex/c/h;

    move-result-object v1

    .line 44
    invoke-virtual {v0, v1}, Lio/reactivex/n;->switchMapSingle(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/payments/send/onboarding/d;->a()Lio/reactivex/c/q;

    move-result-object v1

    .line 51
    invoke-virtual {v0, v1}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p1}, Lco/uk/getmondo/payments/send/onboarding/e;->a(Lco/uk/getmondo/payments/send/onboarding/b$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/payments/send/onboarding/f;->a()Lio/reactivex/c/g;

    move-result-object v2

    .line 52
    invoke-virtual {v0, v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 43
    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/onboarding/b;->a(Lio/reactivex/b/b;)V

    .line 54
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/onboarding/b$a;->b()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/payments/send/onboarding/g;->a(Lco/uk/getmondo/payments/send/onboarding/b$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/onboarding/b;->a(Lio/reactivex/b/b;)V

    .line 56
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/onboarding/b$a;->c()Lio/reactivex/n;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p1}, Lco/uk/getmondo/payments/send/onboarding/h;->a(Lco/uk/getmondo/payments/send/onboarding/b$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/onboarding/b;->a(Lio/reactivex/b/b;)V

    .line 57
    return-void
.end method
