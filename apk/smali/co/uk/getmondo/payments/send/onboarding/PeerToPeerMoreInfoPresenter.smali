.class Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter;
.super Lco/uk/getmondo/common/ui/b;
.source "PeerToPeerMoreInfoPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$MessageFromWeb;,
        Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/common/e/a;

.field private final f:Lco/uk/getmondo/payments/send/data/h;

.field private final g:Lco/uk/getmondo/common/a;


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/payments/send/data/h;Lco/uk/getmondo/common/a;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 33
    iput-object p1, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter;->c:Lio/reactivex/u;

    .line 34
    iput-object p2, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter;->d:Lio/reactivex/u;

    .line 35
    iput-object p3, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter;->e:Lco/uk/getmondo/common/e/a;

    .line 36
    iput-object p4, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter;->f:Lco/uk/getmondo/payments/send/data/h;

    .line 37
    iput-object p5, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter;->g:Lco/uk/getmondo/common/a;

    .line 38
    return-void
.end method

.method static synthetic a(Ljava/lang/String;)Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$MessageFromWeb;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 47
    new-instance v0, Lcom/google/gson/f;

    invoke-direct {v0}, Lcom/google/gson/f;-><init>()V

    const-class v1, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$MessageFromWeb;

    invoke-virtual {v0, p0, v1}, Lcom/google/gson/f;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$MessageFromWeb;

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$a;Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$MessageFromWeb;)Lio/reactivex/r;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 49
    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$MessageFromWeb;->proceed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    invoke-static {p1}, Lio/reactivex/n;->just(Ljava/lang/Object;)Lio/reactivex/n;

    move-result-object v0

    .line 55
    :goto_0
    return-object v0

    .line 51
    :cond_0
    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$MessageFromWeb;->dismiss()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    invoke-interface {p0}, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$a;->b()V

    .line 53
    invoke-static {}, Lio/reactivex/n;->empty()Lio/reactivex/n;

    move-result-object v0

    goto :goto_0

    .line 55
    :cond_1
    invoke-static {}, Lio/reactivex/n;->empty()Lio/reactivex/n;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter;Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$a;Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$MessageFromWeb;)Lio/reactivex/z;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter;->f:Lco/uk/getmondo/payments/send/data/h;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/payments/send/data/h;->a(Z)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter;->d:Lio/reactivex/u;

    .line 59
    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter;->c:Lio/reactivex/u;

    .line 60
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/payments/send/onboarding/t;->a(Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter;Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 61
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    const/4 v1, 0x0

    .line 62
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    .line 58
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$a;Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 64
    invoke-interface {p0}, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$a;->c()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter;Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$a;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter;->e:Lco/uk/getmondo/common/e/a;

    invoke-virtual {v0, p2, p1}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    return-void
.end method

.method static synthetic a(Ljava/lang/Boolean;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 63
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter;->a(Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$a;)V
    .locals 3

    .prologue
    .line 42
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 44
    iget-object v0, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter;->g:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->K()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 46
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$a;->a()Lio/reactivex/n;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/payments/send/onboarding/n;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 47
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/payments/send/onboarding/o;->a(Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$a;)Lio/reactivex/c/h;

    move-result-object v1

    .line 48
    invoke-virtual {v0, v1}, Lio/reactivex/n;->switchMap(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/payments/send/onboarding/p;->a(Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter;Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$a;)Lio/reactivex/c/h;

    move-result-object v1

    .line 58
    invoke-virtual {v0, v1}, Lio/reactivex/n;->switchMapSingle(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/payments/send/onboarding/q;->a()Lio/reactivex/c/q;

    move-result-object v1

    .line 63
    invoke-virtual {v0, v1}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/payments/send/onboarding/r;->a(Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/payments/send/onboarding/s;->a()Lio/reactivex/c/g;

    move-result-object v2

    .line 64
    invoke-virtual {v0, v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 46
    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter;->a(Lio/reactivex/b/b;)V

    .line 65
    return-void
.end method
