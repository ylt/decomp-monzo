.class public Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity_ViewBinding;
.super Ljava/lang/Object;
.source "PeerToPeerIntroActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity_ViewBinding;->a:Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;

    .line 27
    const v0, 0x7f1101e3

    const-string v1, "field \'enablePeerToPeerButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;->enablePeerToPeerButton:Landroid/widget/Button;

    .line 28
    const v0, 0x7f1101e4

    const-string v1, "field \'moreInfoButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;->moreInfoButton:Landroid/widget/Button;

    .line 29
    const v0, 0x7f1100fe

    const-string v1, "field \'toolbar\'"

    const-class v2, Landroid/support/v7/widget/Toolbar;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 30
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    iget-object v0, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity_ViewBinding;->a:Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;

    .line 36
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity_ViewBinding;->a:Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;

    .line 39
    iput-object v1, v0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;->enablePeerToPeerButton:Landroid/widget/Button;

    .line 40
    iput-object v1, v0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;->moreInfoButton:Landroid/widget/Button;

    .line 41
    iput-object v1, v0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 42
    return-void
.end method
