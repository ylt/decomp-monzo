.class public Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "PeerToPeerIntroActivity.java"

# interfaces
.implements Lco/uk/getmondo/payments/send/onboarding/b$a;


# instance fields
.field a:Lco/uk/getmondo/payments/send/onboarding/b;

.field private final b:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field enablePeerToPeerButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101e3
    .end annotation
.end field

.field moreInfoButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101e4
    .end annotation
.end field

.field toolbar:Landroid/support/v7/widget/Toolbar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1100fe
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 38
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;->b:Lcom/b/b/c;

    return-void
.end method

.method public static a(Landroid/content/Context;Lco/uk/getmondo/common/t;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 41
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "KEY_NAV_FLOW"

    .line 42
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    .line 41
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lco/uk/getmondo/common/t;Lco/uk/getmondo/payments/send/data/a/g;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 46
    invoke-static {p0, p1}, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;->a(Landroid/content/Context;Lco/uk/getmondo/common/t;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "KEY_PENDING_PAYMENT"

    .line 47
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 46
    return-object v0
.end method

.method public static a(Landroid/content/Intent;)Lco/uk/getmondo/common/t;
    .locals 1

    .prologue
    .line 51
    const-string v0, "KEY_NAV_FLOW"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/t;

    return-object v0
.end method

.method public static b(Landroid/content/Intent;)Lco/uk/getmondo/payments/send/data/a/g;
    .locals 1

    .prologue
    .line 56
    const-string v0, "KEY_PENDING_PAYMENT"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/send/data/a/g;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;->enablePeerToPeerButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 109
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 110
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 111
    if-eqz p1, :cond_0

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;->setResult(ILandroid/content/Intent;)V

    .line 112
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;->finish()V

    .line 113
    return-void

    .line 111
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;->moreInfoButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;->enablePeerToPeerButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 118
    return-void
.end method

.method public c()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;->b:Lcom/b/b/c;

    return-object v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoActivity;->a(Landroid/app/Activity;I)V

    .line 105
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 80
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 81
    iget-object v0, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;->b:Lcom/b/b/c;

    invoke-static {p3}, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoActivity;->a(Landroid/content/Intent;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 85
    :goto_0
    return-void

    .line 84
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lco/uk/getmondo/common/activities/b;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 61
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 63
    const v0, 0x7f050053

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;->setContentView(I)V

    .line 65
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;)V

    .line 66
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 68
    iget-object v0, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f02014d

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(I)V

    .line 69
    iget-object v0, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;->a:Lco/uk/getmondo/payments/send/onboarding/b;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/payments/send/onboarding/b;->a(Lco/uk/getmondo/payments/send/onboarding/b$a;)V

    .line 70
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerIntroActivity;->a:Lco/uk/getmondo/payments/send/onboarding/b;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/onboarding/b;->b()V

    .line 75
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 76
    return-void
.end method
