.class Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$MessageFromWeb;
.super Ljava/lang/Object;
.source "PeerToPeerMoreInfoPresenter.java"


# annotations
.annotation build Landroid/support/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MessageFromWeb"
.end annotation


# instance fields
.field private final action:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object p1, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$MessageFromWeb;->action:Ljava/lang/String;

    .line 80
    return-void
.end method


# virtual methods
.method dismiss()Z
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$MessageFromWeb;->action:Ljava/lang/String;

    const-string v1, "dismiss"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method proceed()Z
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$MessageFromWeb;->action:Ljava/lang/String;

    const-string v1, "request_permission"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
