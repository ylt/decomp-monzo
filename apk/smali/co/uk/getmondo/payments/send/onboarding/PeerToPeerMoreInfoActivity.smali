.class public Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "PeerToPeerMoreInfoActivity.java"

# interfaces
.implements Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoActivity$a;
    }
.end annotation


# instance fields
.field a:Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter;

.field private final b:Lio/reactivex/i/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/i/a",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field toolbar:Landroid/support/v7/widget/Toolbar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1100fe
    .end annotation
.end field

.field webView:Landroid/webkit/WebView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101e5
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 25
    invoke-static {}, Lio/reactivex/i/a;->a()Lio/reactivex/i/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoActivity;->b:Lio/reactivex/i/a;

    return-void
.end method

.method public static a(Landroid/app/Activity;I)V
    .locals 2

    .prologue
    .line 33
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0, p1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 34
    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 80
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 81
    const-string v1, "KEY_RESULT_ENABLED"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 82
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoActivity;->setResult(ILandroid/content/Intent;)V

    .line 83
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoActivity;->finish()V

    .line 84
    return-void
.end method

.method public static a(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 37
    const-string v0, "KEY_RESULT_ENABLED"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoActivity;->b:Lio/reactivex/i/a;

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoActivity;->a(Z)V

    .line 72
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoActivity;->a(Z)V

    .line 77
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 42
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 44
    const v0, 0x7f050054

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoActivity;->setContentView(I)V

    .line 46
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoActivity;)V

    .line 47
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 48
    iget-object v0, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f02014d

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(I)V

    .line 50
    iget-object v0, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoActivity;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 51
    iget-object v0, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoActivity;->webView:Landroid/webkit/WebView;

    new-instance v1, Landroid/webkit/WebViewClient;

    invoke-direct {v1}, Landroid/webkit/WebViewClient;-><init>()V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 52
    iget-object v0, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoActivity;->webView:Landroid/webkit/WebView;

    new-instance v1, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoActivity$a;

    iget-object v2, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoActivity;->b:Lio/reactivex/i/a;

    invoke-direct {v1, v2}, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoActivity$a;-><init>(Lio/reactivex/i/a;)V

    const-string v2, "mondo"

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    iget-object v0, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoActivity;->webView:Landroid/webkit/WebView;

    const-string v1, "https://monzo.com/content/p2p/android"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 55
    iget-object v0, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoActivity;->a:Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter;->a(Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter$a;)V

    .line 56
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoActivity;->a:Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/onboarding/PeerToPeerMoreInfoPresenter;->b()V

    .line 61
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 62
    return-void
.end method
