.class final Lco/uk/getmondo/payments/send/o$r;
.super Ljava/lang/Object;
.source "SendMoneyPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/payments/send/o;->a(Lco/uk/getmondo/payments/send/o$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lkotlin/h",
        "<+",
        "Ljava/util/Collection",
        "<+",
        "Lco/uk/getmondo/payments/send/a/b;",
        ">;+",
        "Ljava/util/Collection",
        "<+",
        "Lco/uk/getmondo/payments/send/a/d;",
        ">;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012>\u0010\u0002\u001a:\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00060\u0004 \u0007*\u001c\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00060\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "",
        "Lco/uk/getmondo/payments/send/contacts/Contact;",
        "Lco/uk/getmondo/payments/send/contacts/ContactWithMultipleNumbers;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/payments/send/o;

.field final synthetic b:Lco/uk/getmondo/payments/send/o$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/payments/send/o;Lco/uk/getmondo/payments/send/o$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/send/o$r;->a:Lco/uk/getmondo/payments/send/o;

    iput-object p2, p0, Lco/uk/getmondo/payments/send/o$r;->b:Lco/uk/getmondo/payments/send/o$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 35
    check-cast p1, Lkotlin/h;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/send/o$r;->a(Lkotlin/h;)V

    return-void
.end method

.method public final a(Lkotlin/h;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/h",
            "<+",
            "Ljava/util/Collection",
            "<",
            "Lco/uk/getmondo/payments/send/a/b;",
            ">;+",
            "Ljava/util/Collection",
            "<",
            "Lco/uk/getmondo/payments/send/a/d;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    invoke-virtual {p1}, Lkotlin/h;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-virtual {p1}, Lkotlin/h;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    .line 88
    iget-object v2, p0, Lco/uk/getmondo/payments/send/o$r;->b:Lco/uk/getmondo/payments/send/o$a;

    invoke-interface {v2}, Lco/uk/getmondo/payments/send/o$a;->m()V

    .line 89
    iget-object v2, p0, Lco/uk/getmondo/payments/send/o$r;->b:Lco/uk/getmondo/payments/send/o$a;

    invoke-interface {v2, v0, v1}, Lco/uk/getmondo/payments/send/o$a;->a(Ljava/util/Collection;Ljava/util/Collection;)V

    .line 90
    iget-object v2, p0, Lco/uk/getmondo/payments/send/o$r;->a:Lco/uk/getmondo/payments/send/o;

    invoke-static {v2}, Lco/uk/getmondo/payments/send/o;->d(Lco/uk/getmondo/payments/send/o;)Lco/uk/getmondo/common/a;

    move-result-object v2

    sget-object v3, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-virtual {v3, v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->a(II)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    invoke-virtual {v2, v0}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 91
    return-void
.end method
