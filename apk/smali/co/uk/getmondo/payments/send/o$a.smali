.class public interface abstract Lco/uk/getmondo/payments/send/o$a;
.super Ljava/lang/Object;
.source "SendMoneyPresenter.kt"

# interfaces
.implements Lco/uk/getmondo/common/e/a$a;
.implements Lco/uk/getmondo/common/ui/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/payments/send/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008f\u0018\u00002\u00020\u00012\u00020\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H&J\u000e\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H&J\u0008\u0010\u0008\u001a\u00020\tH&J\u0008\u0010\n\u001a\u00020\tH&J\u0008\u0010\u000b\u001a\u00020\tH&J\u0008\u0010\u000c\u001a\u00020\tH&J\u0012\u0010\r\u001a\u00020\t2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000fH&J\u000e\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H&J\u000e\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u0006H&J\u000e\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0006H&J\u000e\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0006H&J\u000e\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H&J\u0008\u0010\u0017\u001a\u00020\tH&J\u0010\u0010\u0018\u001a\u00020\t2\u0006\u0010\u0019\u001a\u00020\u001aH&J\u0010\u0010\u001b\u001a\u00020\t2\u0006\u0010\u001c\u001a\u00020\u001dH&J\u0008\u0010\u001e\u001a\u00020\tH&J\u0008\u0010\u001f\u001a\u00020\tH&J$\u0010 \u001a\u00020\t2\u000c\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u00130\"2\u000c\u0010#\u001a\u0008\u0012\u0004\u0012\u00020$0\"H&J\u0008\u0010%\u001a\u00020\tH&J\u0016\u0010&\u001a\u00020\t2\u000c\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020\u00150\"H&\u00a8\u0006("
    }
    d2 = {
        "Lco/uk/getmondo/payments/send/SendMoneyPresenter$View;",
        "Lco/uk/getmondo/common/ui/MvpView;",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler$ApiErrorView;",
        "checkContactsPermission",
        "",
        "contactsPermissionAcceptClicks",
        "Lio/reactivex/Observable;",
        "",
        "enableBankPayments",
        "",
        "enableContactsRefresh",
        "hideContactLoading",
        "hideListLoading",
        "inviteContact",
        "name",
        "",
        "onAddContactClicked",
        "onContactNotOnMonzoClicked",
        "onContactWithMonzoClicked",
        "Lco/uk/getmondo/payments/send/contacts/Contact;",
        "onRecentPayeeClicked",
        "Lco/uk/getmondo/payments/send/data/model/Payee;",
        "onRefreshAction",
        "openAddContact",
        "openBankTransfer",
        "bankPayee",
        "Lco/uk/getmondo/payments/send/data/model/BankPayee;",
        "openSendMoney",
        "peer",
        "Lco/uk/getmondo/model/Peer;",
        "requestContactsPermission",
        "showContactLoading",
        "showContacts",
        "contactsOnMonzo",
        "",
        "allContacts",
        "Lco/uk/getmondo/payments/send/contacts/ContactWithMultipleNumbers;",
        "showListLoading",
        "showRecentPayees",
        "recentPayees",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# virtual methods
.method public abstract a(Lco/uk/getmondo/d/aa;)V
.end method

.method public abstract a(Lco/uk/getmondo/payments/send/data/a/b;)V
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract a(Ljava/util/Collection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lco/uk/getmondo/payments/send/data/a/e;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract a(Ljava/util/Collection;Ljava/util/Collection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lco/uk/getmondo/payments/send/a/b;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lco/uk/getmondo/payments/send/a/d;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract b()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method public abstract c()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/payments/send/a/b;",
            ">;"
        }
    .end annotation
.end method

.method public abstract d()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract e()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method public abstract f()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/payments/send/data/a/e;",
            ">;"
        }
    .end annotation
.end method

.method public abstract g()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method public abstract h()V
.end method

.method public abstract i()Z
.end method

.method public abstract j()V
.end method

.method public abstract k()V
.end method

.method public abstract l()V
.end method

.method public abstract m()V
.end method

.method public abstract n()V
.end method

.method public abstract o()V
.end method

.method public abstract p()V
.end method
