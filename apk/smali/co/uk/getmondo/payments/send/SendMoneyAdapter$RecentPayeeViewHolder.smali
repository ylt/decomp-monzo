.class Lco/uk/getmondo/payments/send/SendMoneyAdapter$RecentPayeeViewHolder;
.super Landroid/support/v7/widget/RecyclerView$w;
.source "SendMoneyAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/payments/send/SendMoneyAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RecentPayeeViewHolder"
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/payments/send/SendMoneyAdapter;

.field private b:I

.field payeeIdentifierView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110428
    .end annotation
.end field

.field payeeImageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110426
    .end annotation
.end field

.field payeeNameView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110427
    .end annotation
.end field


# direct methods
.method constructor <init>(Lco/uk/getmondo/payments/send/SendMoneyAdapter;Landroid/view/View;Lco/uk/getmondo/payments/send/SendMoneyAdapter$f;)V
    .locals 3

    .prologue
    .line 309
    iput-object p1, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$RecentPayeeViewHolder;->a:Lco/uk/getmondo/payments/send/SendMoneyAdapter;

    .line 310
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$w;-><init>(Landroid/view/View;)V

    .line 311
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 313
    const/4 v0, 0x2

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$RecentPayeeViewHolder;->b:I

    .line 315
    invoke-static {p0, p3}, Lco/uk/getmondo/payments/send/f;->a(Lco/uk/getmondo/payments/send/SendMoneyAdapter$RecentPayeeViewHolder;Lco/uk/getmondo/payments/send/SendMoneyAdapter$f;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 322
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/SendMoneyAdapter$RecentPayeeViewHolder;Lco/uk/getmondo/payments/send/SendMoneyAdapter$f;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 316
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/SendMoneyAdapter$RecentPayeeViewHolder;->getAdapterPosition()I

    move-result v0

    .line 317
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 318
    iget-object v1, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$RecentPayeeViewHolder;->a:Lco/uk/getmondo/payments/send/SendMoneyAdapter;

    invoke-static {v1}, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->b(Lco/uk/getmondo/payments/send/SendMoneyAdapter;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/send/data/a/e;

    .line 319
    invoke-interface {p1, v0}, Lco/uk/getmondo/payments/send/SendMoneyAdapter$f;->a(Lco/uk/getmondo/payments/send/data/a/e;)V

    .line 321
    :cond_0
    return-void
.end method


# virtual methods
.method a(Lco/uk/getmondo/payments/send/data/a/e;)V
    .locals 6

    .prologue
    .line 326
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/data/a/e;->b()Ljava/lang/String;

    move-result-object v1

    .line 329
    instance-of v0, p1, Lco/uk/getmondo/payments/send/data/a/b;

    if-eqz v0, :cond_0

    .line 330
    check-cast p1, Lco/uk/getmondo/payments/send/data/a/b;

    .line 331
    const-string v0, "%s  \u2022  %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/data/a/b;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/data/a/b;->e()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 332
    const/4 v0, 0x0

    move-object v5, v0

    move-object v0, v2

    move-object v2, v5

    .line 340
    :goto_0
    if-nez v0, :cond_2

    .line 341
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Payee is missing information"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 333
    :cond_0
    instance-of v0, p1, Lco/uk/getmondo/d/aa;

    if-eqz v0, :cond_1

    .line 334
    check-cast p1, Lco/uk/getmondo/d/aa;

    .line 335
    invoke-virtual {p1}, Lco/uk/getmondo/d/aa;->d()Ljava/lang/String;

    move-result-object v2

    .line 336
    invoke-virtual {p1}, Lco/uk/getmondo/d/aa;->g()Ljava/lang/String;

    move-result-object v0

    move-object v5, v0

    move-object v0, v2

    move-object v2, v5

    .line 337
    goto :goto_0

    .line 338
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected payee type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 343
    :cond_2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 345
    iget-object v1, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$RecentPayeeViewHolder;->payeeIdentifierView:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 350
    :goto_1
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 351
    iget-object v1, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$RecentPayeeViewHolder;->payeeImageView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/bumptech/glide/g;->b(Landroid/content/Context;)Lcom/bumptech/glide/j;

    move-result-object v1

    .line 352
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bumptech/glide/j;->a(Landroid/net/Uri;)Lcom/bumptech/glide/d;

    move-result-object v1

    .line 353
    invoke-virtual {v1}, Lcom/bumptech/glide/d;->h()Lcom/bumptech/glide/b;

    move-result-object v1

    sget-object v2, Lcom/bumptech/glide/load/engine/b;->b:Lcom/bumptech/glide/load/engine/b;

    .line 355
    invoke-virtual {v1, v2}, Lcom/bumptech/glide/b;->a(Lcom/bumptech/glide/load/engine/b;)Lcom/bumptech/glide/a;

    move-result-object v1

    .line 356
    invoke-virtual {v1}, Lcom/bumptech/glide/a;->a()Lcom/bumptech/glide/a;

    move-result-object v1

    new-instance v2, Lco/uk/getmondo/common/ui/c;

    iget-object v3, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$RecentPayeeViewHolder;->payeeImageView:Landroid/widget/ImageView;

    invoke-direct {v2, v3}, Lco/uk/getmondo/common/ui/c;-><init>(Landroid/widget/ImageView;)V

    .line 357
    invoke-virtual {v1, v2}, Lcom/bumptech/glide/a;->a(Lcom/bumptech/glide/g/b/j;)Lcom/bumptech/glide/g/b/j;

    .line 361
    :goto_2
    iget-object v1, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$RecentPayeeViewHolder;->payeeNameView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 362
    return-void

    .line 348
    :cond_3
    iget-object v3, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$RecentPayeeViewHolder;->payeeIdentifierView:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, v1

    goto :goto_1

    .line 359
    :cond_4
    iget-object v1, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$RecentPayeeViewHolder;->payeeImageView:Landroid/widget/ImageView;

    iget-object v2, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$RecentPayeeViewHolder;->a:Lco/uk/getmondo/payments/send/SendMoneyAdapter;

    invoke-static {v2}, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->a(Lco/uk/getmondo/payments/send/SendMoneyAdapter;)Lco/uk/getmondo/common/ui/a;

    move-result-object v2

    invoke-virtual {v2, v0}, Lco/uk/getmondo/common/ui/a;->a(Ljava/lang/String;)Lco/uk/getmondo/common/ui/a$b;

    move-result-object v2

    iget v3, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$RecentPayeeViewHolder;->b:I

    invoke-virtual {v2, v3}, Lco/uk/getmondo/common/ui/a$b;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2
.end method
