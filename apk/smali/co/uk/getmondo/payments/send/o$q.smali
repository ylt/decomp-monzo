.class final Lco/uk/getmondo/payments/send/o$q;
.super Ljava/lang/Object;
.source "SendMoneyPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/payments/send/o;->a(Lco/uk/getmondo/payments/send/o$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/z",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000(\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010!\n\u0002\u0010\u000e\n\u0002\u0010 \n\u0000\u0010\u0000\u001a\u0086\u0001\u0012<\u0012:\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u0003 \u0006*\u001c\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u0003\u0018\u00010\u00020\u0002 \u0006*B\u0012<\u0012:\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u0003 \u0006*\u001c\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u0003\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012X\u0010\u0007\u001aT\u0012$\u0012\"\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\t \u0006*\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\t\u0018\u00010\u00020\u0002 \u0006*(\u0012$\u0012\"\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\t \u0006*\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\t\u0018\u00010\u00020\u00020\n0\u0008H\n\u00a2\u0006\u0002\u0008\u000b"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lkotlin/Pair;",
        "",
        "Lco/uk/getmondo/payments/send/contacts/Contact;",
        "Lco/uk/getmondo/payments/send/contacts/ContactWithMultipleNumbers;",
        "kotlin.jvm.PlatformType",
        "pairs",
        "",
        "",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/payments/send/o;


# direct methods
.method constructor <init>(Lco/uk/getmondo/payments/send/o;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/send/o$q;->a:Lco/uk/getmondo/payments/send/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lkotlin/h",
            "<",
            "Lco/uk/getmondo/payments/send/a/b;",
            "Ljava/lang/String;",
            ">;>;)",
            "Lio/reactivex/v",
            "<",
            "Lkotlin/h",
            "<",
            "Ljava/util/Collection",
            "<",
            "Lco/uk/getmondo/payments/send/a/b;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lco/uk/getmondo/payments/send/a/d;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    const-string v0, "pairs"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 79
    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lio/reactivex/n;->fromIterable(Ljava/lang/Iterable;)Lio/reactivex/n;

    move-result-object v1

    .line 80
    new-instance v0, Lco/uk/getmondo/payments/send/o$q$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/payments/send/o$q$1;-><init>(Lco/uk/getmondo/payments/send/o$q;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 81
    invoke-virtual {v0}, Lio/reactivex/n;->distinct()Lio/reactivex/n;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Lio/reactivex/n;->toList()Lio/reactivex/v;

    move-result-object v1

    .line 83
    new-instance v0, Lco/uk/getmondo/payments/send/o$q$2;

    invoke-direct {v0, p0}, Lco/uk/getmondo/payments/send/o$q$2;-><init>(Lco/uk/getmondo/payments/send/o$q;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->a(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v1

    .line 84
    new-instance v0, Lco/uk/getmondo/payments/send/o$q$3;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/payments/send/o$q$3;-><init>(Lco/uk/getmondo/payments/send/o$q;Ljava/util/List;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 35
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/send/o$q;->a(Ljava/util/List;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method
