.class final Lco/uk/getmondo/payments/send/o$h$2;
.super Ljava/lang/Object;
.source "SendMoneyPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/payments/send/o$h;->a(Lco/uk/getmondo/payments/send/a/b;)Lio/reactivex/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "error",
        "",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/payments/send/o$h;


# direct methods
.method constructor <init>(Lco/uk/getmondo/payments/send/o$h;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/send/o$h$2;->a:Lco/uk/getmondo/payments/send/o$h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 35
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/send/o$h$2;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lco/uk/getmondo/payments/send/o$h$2;->a:Lco/uk/getmondo/payments/send/o$h;

    iget-object v0, v0, Lco/uk/getmondo/payments/send/o$h;->b:Lco/uk/getmondo/payments/send/o$a;

    invoke-interface {v0}, Lco/uk/getmondo/payments/send/o$a;->o()V

    .line 138
    instance-of v0, p1, Lco/uk/getmondo/payments/send/data/PeerToPeerRepository$a;

    if-eqz v0, :cond_1

    .line 139
    iget-object v0, p0, Lco/uk/getmondo/payments/send/o$h$2;->a:Lco/uk/getmondo/payments/send/o$h;

    iget-object v0, v0, Lco/uk/getmondo/payments/send/o$h;->b:Lco/uk/getmondo/payments/send/o$a;

    const v1, 0x7f0a034d

    invoke-interface {v0, v1}, Lco/uk/getmondo/payments/send/o$a;->b(I)V

    .line 140
    iget-object v0, p0, Lco/uk/getmondo/payments/send/o$h$2;->a:Lco/uk/getmondo/payments/send/o$h;

    iget-object v0, v0, Lco/uk/getmondo/payments/send/o$h;->b:Lco/uk/getmondo/payments/send/o$a;

    check-cast p1, Lco/uk/getmondo/payments/send/data/PeerToPeerRepository$a;

    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/data/PeerToPeerRepository$a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lco/uk/getmondo/payments/send/o$a;->a(Ljava/lang/String;)V

    .line 141
    iget-object v0, p0, Lco/uk/getmondo/payments/send/o$h$2;->a:Lco/uk/getmondo/payments/send/o$h;

    iget-object v0, v0, Lco/uk/getmondo/payments/send/o$h;->a:Lco/uk/getmondo/payments/send/o;

    invoke-static {v0}, Lco/uk/getmondo/payments/send/o;->d(Lco/uk/getmondo/payments/send/o;)Lco/uk/getmondo/common/a;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->Z()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 148
    :cond_0
    :goto_0
    return-void

    .line 142
    :cond_1
    instance-of v0, p1, Lco/uk/getmondo/payments/send/data/PeerToPeerRepository$UserHasP2pDisabledException;

    if-eqz v0, :cond_2

    .line 143
    iget-object v0, p0, Lco/uk/getmondo/payments/send/o$h$2;->a:Lco/uk/getmondo/payments/send/o$h;

    iget-object v0, v0, Lco/uk/getmondo/payments/send/o$h;->b:Lco/uk/getmondo/payments/send/o$a;

    const v1, 0x7f0a034c

    invoke-interface {v0, v1}, Lco/uk/getmondo/payments/send/o$a;->b(I)V

    goto :goto_0

    .line 145
    :cond_2
    iget-object v0, p0, Lco/uk/getmondo/payments/send/o$h$2;->a:Lco/uk/getmondo/payments/send/o$h;

    iget-object v0, v0, Lco/uk/getmondo/payments/send/o$h;->a:Lco/uk/getmondo/payments/send/o;

    invoke-static {v0}, Lco/uk/getmondo/payments/send/o;->e(Lco/uk/getmondo/payments/send/o;)Lco/uk/getmondo/common/e/a;

    move-result-object v1

    const-string v0, "error"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lco/uk/getmondo/payments/send/o$h$2;->a:Lco/uk/getmondo/payments/send/o$h;

    iget-object v0, v0, Lco/uk/getmondo/payments/send/o$h;->b:Lco/uk/getmondo/payments/send/o$a;

    check-cast v0, Lco/uk/getmondo/common/e/a$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 146
    iget-object v0, p0, Lco/uk/getmondo/payments/send/o$h$2;->a:Lco/uk/getmondo/payments/send/o$h;

    iget-object v0, v0, Lco/uk/getmondo/payments/send/o$h;->b:Lco/uk/getmondo/payments/send/o$a;

    const v1, 0x7f0a0352

    invoke-interface {v0, v1}, Lco/uk/getmondo/payments/send/o$a;->b(I)V

    goto :goto_0
.end method
