.class Lco/uk/getmondo/payments/send/authentication/h;
.super Lco/uk/getmondo/common/ui/b;
.source "PaymentAuthenticationPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/payments/send/authentication/h$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/payments/send/authentication/h$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/common/e/a;

.field private final f:Lco/uk/getmondo/payments/send/data/a;

.field private final g:Lco/uk/getmondo/common/a;

.field private final h:Lco/uk/getmondo/payments/send/data/a/f;

.field private final i:Lco/uk/getmondo/common/accounts/b;


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/payments/send/data/a;Lco/uk/getmondo/common/a;Lco/uk/getmondo/payments/send/data/a/f;Lco/uk/getmondo/common/accounts/b;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 41
    iput-object p1, p0, Lco/uk/getmondo/payments/send/authentication/h;->c:Lio/reactivex/u;

    .line 42
    iput-object p2, p0, Lco/uk/getmondo/payments/send/authentication/h;->d:Lio/reactivex/u;

    .line 43
    iput-object p3, p0, Lco/uk/getmondo/payments/send/authentication/h;->e:Lco/uk/getmondo/common/e/a;

    .line 44
    iput-object p4, p0, Lco/uk/getmondo/payments/send/authentication/h;->f:Lco/uk/getmondo/payments/send/data/a;

    .line 45
    iput-object p5, p0, Lco/uk/getmondo/payments/send/authentication/h;->g:Lco/uk/getmondo/common/a;

    .line 46
    iput-object p6, p0, Lco/uk/getmondo/payments/send/authentication/h;->h:Lco/uk/getmondo/payments/send/data/a/f;

    .line 47
    iput-object p7, p0, Lco/uk/getmondo/payments/send/authentication/h;->i:Lco/uk/getmondo/common/accounts/b;

    .line 48
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/authentication/h;Ljava/lang/String;Lco/uk/getmondo/payments/send/authentication/h$a;Ljava/lang/String;)Lio/reactivex/z;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lco/uk/getmondo/payments/send/authentication/h;->f:Lco/uk/getmondo/payments/send/data/a;

    iget-object v1, p0, Lco/uk/getmondo/payments/send/authentication/h;->i:Lco/uk/getmondo/common/accounts/b;

    invoke-virtual {v1}, Lco/uk/getmondo/common/accounts/b;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/payments/send/authentication/h;->h:Lco/uk/getmondo/payments/send/data/a/f;

    invoke-virtual {v0, v1, v2, p3, p1}, Lco/uk/getmondo/payments/send/data/a;->a(Ljava/lang/String;Lco/uk/getmondo/payments/send/data/a/f;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/payments/send/authentication/h;->d:Lio/reactivex/u;

    .line 70
    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/payments/send/authentication/h;->c:Lio/reactivex/u;

    .line 71
    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    invoke-static {p0, p2}, Lco/uk/getmondo/payments/send/authentication/n;->a(Lco/uk/getmondo/payments/send/authentication/h;Lco/uk/getmondo/payments/send/authentication/h$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 72
    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v0

    const/4 v1, 0x1

    .line 73
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    const/4 v1, 0x0

    .line 74
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/v;->b(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    .line 69
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/authentication/h$a;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 66
    invoke-interface {p0}, Lco/uk/getmondo/payments/send/authentication/h$a;->f()V

    .line 67
    invoke-interface {p0}, Lco/uk/getmondo/payments/send/authentication/h$a;->c()V

    .line 68
    return-void
.end method

.method private a(Lco/uk/getmondo/payments/send/authentication/h$a;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 96
    instance-of v0, p2, Lco/uk/getmondo/api/ApiException;

    if-eqz v0, :cond_1

    move-object v0, p2

    check-cast v0, Lco/uk/getmondo/api/ApiException;

    invoke-virtual {v0}, Lco/uk/getmondo/api/ApiException;->e()Lco/uk/getmondo/api/model/b;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 97
    invoke-static {}, Lco/uk/getmondo/payments/send/authentication/p;->values()[Lco/uk/getmondo/payments/send/authentication/p;

    move-result-object v1

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lco/uk/getmondo/common/e/d;->a([Lco/uk/getmondo/common/e/f;Ljava/lang/String;)Lco/uk/getmondo/common/e/f;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/send/authentication/p;

    .line 98
    if-eqz v0, :cond_1

    .line 99
    iget-object v1, p0, Lco/uk/getmondo/payments/send/authentication/h;->h:Lco/uk/getmondo/payments/send/data/a/f;

    invoke-interface {p1, v0, v1}, Lco/uk/getmondo/payments/send/authentication/h$a;->a(Lco/uk/getmondo/payments/send/authentication/p;Lco/uk/getmondo/payments/send/data/a/f;)V

    .line 100
    sget-object v1, Lco/uk/getmondo/payments/send/authentication/p;->d:Lco/uk/getmondo/payments/send/authentication/p;

    if-ne v0, v1, :cond_0

    .line 101
    iget-object v0, p0, Lco/uk/getmondo/payments/send/authentication/h;->g:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->R()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/payments/send/authentication/h;->e:Lco/uk/getmondo/common/e/a;

    invoke-virtual {v0, p2, p1}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 108
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/authentication/h$a;->b()V

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/authentication/h;Lco/uk/getmondo/payments/send/authentication/h$a;Ljava/lang/Boolean;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lco/uk/getmondo/payments/send/authentication/h;->h:Lco/uk/getmondo/payments/send/data/a/f;

    instance-of v0, v0, Lco/uk/getmondo/payments/send/data/a/g;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lco/uk/getmondo/payments/send/authentication/h;->g:Lco/uk/getmondo/common/a;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v1}, Lco/uk/getmondo/api/model/tracking/Impression;->h(Z)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 82
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 83
    iget-object v0, p0, Lco/uk/getmondo/payments/send/authentication/h;->h:Lco/uk/getmondo/payments/send/data/a/f;

    invoke-interface {p1, v0}, Lco/uk/getmondo/payments/send/authentication/h$a;->a(Lco/uk/getmondo/payments/send/data/a/f;)V

    .line 90
    :goto_0
    return-void

    .line 86
    :cond_1
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/authentication/h$a;->e()V

    .line 87
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/authentication/h$a;->g()V

    .line 88
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/authentication/h$a;->d()V

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/authentication/h;Lco/uk/getmondo/payments/send/authentication/h$a;Ljava/lang/Throwable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/payments/send/authentication/h;->a(Lco/uk/getmondo/payments/send/authentication/h$a;Ljava/lang/Throwable;)V

    return-void
.end method

.method static synthetic a(Ljava/lang/String;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 64
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lco/uk/getmondo/payments/send/authentication/h$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/send/authentication/h;->a(Lco/uk/getmondo/payments/send/authentication/h$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/payments/send/authentication/h$a;)V
    .locals 3

    .prologue
    .line 52
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 54
    iget-object v0, p0, Lco/uk/getmondo/payments/send/authentication/h;->h:Lco/uk/getmondo/payments/send/data/a/f;

    instance-of v0, v0, Lco/uk/getmondo/payments/send/data/a/g;

    if-eqz v0, :cond_1

    .line 55
    iget-object v0, p0, Lco/uk/getmondo/payments/send/authentication/h;->h:Lco/uk/getmondo/payments/send/data/a/f;

    check-cast v0, Lco/uk/getmondo/payments/send/data/a/g;

    invoke-interface {p1, v0}, Lco/uk/getmondo/payments/send/authentication/h$a;->a(Lco/uk/getmondo/payments/send/data/a/g;)V

    .line 61
    :cond_0
    :goto_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 63
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/authentication/h$a;->a()Lio/reactivex/n;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/payments/send/authentication/i;->a()Lio/reactivex/c/q;

    move-result-object v2

    .line 64
    invoke-virtual {v1, v2}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v1

    invoke-static {p1}, Lco/uk/getmondo/payments/send/authentication/j;->a(Lco/uk/getmondo/payments/send/authentication/h$a;)Lio/reactivex/c/g;

    move-result-object v2

    .line 65
    invoke-virtual {v1, v2}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v1

    invoke-static {p0, v0, p1}, Lco/uk/getmondo/payments/send/authentication/k;->a(Lco/uk/getmondo/payments/send/authentication/h;Ljava/lang/String;Lco/uk/getmondo/payments/send/authentication/h$a;)Lio/reactivex/c/h;

    move-result-object v0

    .line 69
    invoke-virtual {v1, v0}, Lio/reactivex/n;->flatMapSingle(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/payments/send/authentication/l;->a(Lco/uk/getmondo/payments/send/authentication/h;Lco/uk/getmondo/payments/send/authentication/h$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/payments/send/authentication/m;->a()Lio/reactivex/c/g;

    move-result-object v2

    .line 75
    invoke-virtual {v0, v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 63
    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/authentication/h;->a(Lio/reactivex/b/b;)V

    .line 91
    return-void

    .line 56
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/payments/send/authentication/h;->h:Lco/uk/getmondo/payments/send/data/a/f;

    instance-of v0, v0, Lco/uk/getmondo/payments/send/data/a/c;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lco/uk/getmondo/payments/send/authentication/h;->h:Lco/uk/getmondo/payments/send/data/a/f;

    check-cast v0, Lco/uk/getmondo/payments/send/data/a/c;

    invoke-interface {p1, v0}, Lco/uk/getmondo/payments/send/authentication/h$a;->a(Lco/uk/getmondo/payments/send/data/a/c;)V

    goto :goto_0
.end method
