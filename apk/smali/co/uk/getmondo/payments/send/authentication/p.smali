.class public abstract enum Lco/uk/getmondo/payments/send/authentication/p;
.super Ljava/lang/Enum;
.source "PaymentError.kt"

# interfaces
.implements Lco/uk/getmondo/common/e/f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/payments/send/authentication/p$a;,
        Lco/uk/getmondo/payments/send/authentication/p$c;,
        Lco/uk/getmondo/payments/send/authentication/p$b;,
        Lco/uk/getmondo/payments/send/authentication/p$e;,
        Lco/uk/getmondo/payments/send/authentication/p$d;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/payments/send/authentication/p;",
        ">;",
        "Lco/uk/getmondo/common/e/f;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002B\u000f\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0018\u0010\u0008\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH&R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007j\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011\u00a8\u0006\u0012"
    }
    d2 = {
        "Lco/uk/getmondo/payments/send/authentication/PaymentError;",
        "",
        "Lco/uk/getmondo/common/errors/MatchableError;",
        "prefix",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "getPrefix",
        "()Ljava/lang/String;",
        "getDisplayMessage",
        "resources",
        "Landroid/content/res/Resources;",
        "payment",
        "Lco/uk/getmondo/payments/send/data/model/Payment;",
        "BLOCKED",
        "PIN_INCORRECT",
        "INSUFFICIENT_FUNDS",
        "RECIPIENT_NOT_ENABLED",
        "RECIPIENT_NOT_ACTIVE",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/payments/send/authentication/p;

.field public static final enum b:Lco/uk/getmondo/payments/send/authentication/p;

.field public static final enum c:Lco/uk/getmondo/payments/send/authentication/p;

.field public static final enum d:Lco/uk/getmondo/payments/send/authentication/p;

.field public static final enum e:Lco/uk/getmondo/payments/send/authentication/p;

.field private static final synthetic f:[Lco/uk/getmondo/payments/send/authentication/p;


# instance fields
.field private final g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x5

    new-array v0, v0, [Lco/uk/getmondo/payments/send/authentication/p;

    new-instance v1, Lco/uk/getmondo/payments/send/authentication/p$a;

    const-string v2, "BLOCKED"

    invoke-direct {v1, v2, v3}, Lco/uk/getmondo/payments/send/authentication/p$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/payments/send/authentication/p;->a:Lco/uk/getmondo/payments/send/authentication/p;

    aput-object v1, v0, v3

    new-instance v1, Lco/uk/getmondo/payments/send/authentication/p$c;

    const-string v2, "PIN_INCORRECT"

    invoke-direct {v1, v2, v4}, Lco/uk/getmondo/payments/send/authentication/p$c;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/payments/send/authentication/p;->b:Lco/uk/getmondo/payments/send/authentication/p;

    aput-object v1, v0, v4

    new-instance v1, Lco/uk/getmondo/payments/send/authentication/p$b;

    const-string v2, "INSUFFICIENT_FUNDS"

    invoke-direct {v1, v2, v5}, Lco/uk/getmondo/payments/send/authentication/p$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/payments/send/authentication/p;->c:Lco/uk/getmondo/payments/send/authentication/p;

    aput-object v1, v0, v5

    new-instance v1, Lco/uk/getmondo/payments/send/authentication/p$e;

    const-string v2, "RECIPIENT_NOT_ENABLED"

    invoke-direct {v1, v2, v6}, Lco/uk/getmondo/payments/send/authentication/p$e;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/payments/send/authentication/p;->d:Lco/uk/getmondo/payments/send/authentication/p;

    aput-object v1, v0, v6

    new-instance v1, Lco/uk/getmondo/payments/send/authentication/p$d;

    const-string v2, "RECIPIENT_NOT_ACTIVE"

    invoke-direct {v1, v2, v7}, Lco/uk/getmondo/payments/send/authentication/p$d;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/payments/send/authentication/p;->e:Lco/uk/getmondo/payments/send/authentication/p;

    aput-object v1, v0, v7

    sput-object v0, Lco/uk/getmondo/payments/send/authentication/p;->f:[Lco/uk/getmondo/payments/send/authentication/p;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "prefix"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lco/uk/getmondo/payments/send/authentication/p;->g:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/payments/send/authentication/p;
    .locals 1

    const-class v0, Lco/uk/getmondo/payments/send/authentication/p;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/send/authentication/p;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/payments/send/authentication/p;
    .locals 1

    sget-object v0, Lco/uk/getmondo/payments/send/authentication/p;->f:[Lco/uk/getmondo/payments/send/authentication/p;

    invoke-virtual {v0}, [Lco/uk/getmondo/payments/send/authentication/p;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/payments/send/authentication/p;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lco/uk/getmondo/payments/send/authentication/p;->g:Ljava/lang/String;

    return-object v0
.end method

.method public abstract a(Landroid/content/res/Resources;Lco/uk/getmondo/payments/send/data/a/f;)Ljava/lang/String;
.end method
