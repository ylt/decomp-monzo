.class public Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "PaymentAuthenticationActivity.java"

# interfaces
.implements Lco/uk/getmondo/payments/send/authentication/h$a;


# instance fields
.field a:Lco/uk/getmondo/payments/send/authentication/h;

.field accountNumberTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101d6
    .end annotation
.end field

.field amountView:Lco/uk/getmondo/common/ui/AmountView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101d5
    .end annotation
.end field

.field b:Lco/uk/getmondo/payments/send/a;

.field private c:Lco/uk/getmondo/common/ui/a;

.field contactImageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101d3
    .end annotation
.end field

.field contactNameTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101d4
    .end annotation
.end field

.field pinEntryView:Lco/uk/getmondo/common/ui/PinEntryView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110472
    .end annotation
.end field

.field progressBar:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101da
    .end annotation
.end field

.field progressBarOverlay:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101d9
    .end annotation
.end field

.field scheduleDescriptionTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101d7
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lco/uk/getmondo/payments/send/data/a/f;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 60
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "KEY_PAYMENT"

    .line 61
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 60
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->pinEntryView:Lco/uk/getmondo/common/ui/PinEntryView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/PinEntryView;->setOnPinEnteredListener(Lco/uk/getmondo/common/ui/PinEntryView$a;)V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;Lio/reactivex/o;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 91
    iget-object v0, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->pinEntryView:Lco/uk/getmondo/common/ui/PinEntryView;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p1}, Lco/uk/getmondo/payments/send/authentication/b;->a(Lio/reactivex/o;)Lco/uk/getmondo/common/ui/PinEntryView$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/PinEntryView;->setOnPinEnteredListener(Lco/uk/getmondo/common/ui/PinEntryView$a;)V

    .line 92
    invoke-static {p0}, Lco/uk/getmondo/payments/send/authentication/c;->a(Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;)Lio/reactivex/c/f;

    move-result-object v0

    invoke-interface {p1, v0}, Lio/reactivex/o;->a(Lio/reactivex/c/f;)V

    .line 93
    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    invoke-static {p0}, Lco/uk/getmondo/payments/send/authentication/a;->a(Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;)Lio/reactivex/p;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public a(Lco/uk/getmondo/payments/send/authentication/p;Lco/uk/getmondo/payments/send/data/a/f;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 134
    sget-object v1, Lco/uk/getmondo/payments/send/authentication/p;->a:Lco/uk/getmondo/payments/send/authentication/p;

    if-ne p1, v1, :cond_0

    .line 135
    invoke-static {v0}, Lco/uk/getmondo/common/d/e;->a(Z)Lco/uk/getmondo/common/d/e;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "tag_error_dialog"

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/common/d/e;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 142
    :goto_0
    return-void

    .line 137
    :cond_0
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, Lco/uk/getmondo/payments/send/authentication/p;->a(Landroid/content/res/Resources;Lco/uk/getmondo/payments/send/data/a/f;)Ljava/lang/String;

    move-result-object v1

    .line 138
    sget-object v2, Lco/uk/getmondo/payments/send/authentication/p;->b:Lco/uk/getmondo/payments/send/authentication/p;

    if-eq p1, v2, :cond_1

    .line 139
    :goto_1
    const v2, 0x7f0a0351

    invoke-virtual {p0, v2}, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1, v0}, Lco/uk/getmondo/common/d/a;->a(Ljava/lang/String;Ljava/lang/String;Z)Lco/uk/getmondo/common/d/a;

    move-result-object v0

    .line 140
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    const-string v2, "tag_error_dialog"

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/common/d/a;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    goto :goto_0

    .line 138
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Lco/uk/getmondo/payments/send/data/a/c;)V
    .locals 6

    .prologue
    const/16 v0, 0x8

    const/4 v1, 0x0

    .line 98
    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/data/a/c;->c()Lco/uk/getmondo/payments/send/data/a/b;

    move-result-object v2

    .line 99
    iget-object v3, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->contactImageView:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 100
    iget-object v3, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->contactNameTextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Lco/uk/getmondo/payments/send/data/a/b;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    iget-object v3, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->amountView:Lco/uk/getmondo/common/ui/AmountView;

    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/data/a/c;->a()Lco/uk/getmondo/d/c;

    move-result-object v4

    invoke-virtual {v3, v4}, Lco/uk/getmondo/common/ui/AmountView;->setAmount(Lco/uk/getmondo/d/c;)V

    .line 103
    const-string v3, "  \u2022  "

    .line 104
    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v2}, Lco/uk/getmondo/payments/send/data/a/b;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v5, 0x1

    invoke-virtual {v2}, Lco/uk/getmondo/payments/send/data/a/b;->e()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v2, 0x2

    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/data/a/c;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    .line 105
    iget-object v2, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->accountNumberTextView:Landroid/widget/TextView;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    iget-object v2, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->accountNumberTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 108
    iget-object v2, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->b:Lco/uk/getmondo/payments/send/a;

    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/data/a/c;->e()Lco/uk/getmondo/payments/a/a/d;

    move-result-object v3

    invoke-virtual {v2, v3}, Lco/uk/getmondo/payments/send/a;->a(Lco/uk/getmondo/payments/a/a/d;)Ljava/lang/String;

    move-result-object v2

    .line 109
    iget-object v3, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->scheduleDescriptionTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    iget-object v3, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->scheduleDescriptionTextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 111
    return-void

    :cond_0
    move v0, v1

    .line 110
    goto :goto_0
.end method

.method public a(Lco/uk/getmondo/payments/send/data/a/f;)V
    .locals 1

    .prologue
    .line 164
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->finish()V

    .line 165
    invoke-static {p0, p1}, Lco/uk/getmondo/payments/send/payment_category/PaymentCategoryActivity;->a(Landroid/content/Context;Lco/uk/getmondo/payments/send/data/a/f;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->startActivity(Landroid/content/Intent;)V

    .line 166
    return-void
.end method

.method public a(Lco/uk/getmondo/payments/send/data/a/g;)V
    .locals 5

    .prologue
    .line 115
    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/data/a/g;->c()Lco/uk/getmondo/d/aa;

    move-result-object v0

    .line 116
    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 117
    iget-object v1, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->contactImageView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/bumptech/glide/g;->b(Landroid/content/Context;)Lcom/bumptech/glide/j;

    move-result-object v1

    .line 118
    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bumptech/glide/j;->a(Landroid/net/Uri;)Lcom/bumptech/glide/d;

    move-result-object v1

    .line 119
    invoke-virtual {v1}, Lcom/bumptech/glide/d;->h()Lcom/bumptech/glide/b;

    move-result-object v1

    .line 120
    invoke-virtual {v1}, Lcom/bumptech/glide/b;->a()Lcom/bumptech/glide/a;

    move-result-object v1

    new-instance v2, Lco/uk/getmondo/common/ui/c;

    iget-object v3, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->contactImageView:Landroid/widget/ImageView;

    invoke-direct {v2, v3}, Lco/uk/getmondo/common/ui/c;-><init>(Landroid/widget/ImageView;)V

    .line 121
    invoke-virtual {v1, v2}, Lcom/bumptech/glide/a;->a(Lcom/bumptech/glide/g/b/j;)Lcom/bumptech/glide/g/b/j;

    .line 126
    :goto_0
    iget-object v1, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->contactImageView:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 128
    iget-object v1, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->contactNameTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    iget-object v0, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->amountView:Lco/uk/getmondo/common/ui/AmountView;

    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/data/a/g;->a()Lco/uk/getmondo/d/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/AmountView;->setAmount(Lco/uk/getmondo/d/c;)V

    .line 130
    return-void

    .line 123
    :cond_0
    const/4 v1, 0x2

    const/high16 v2, 0x41d00000    # 26.0f

    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    .line 124
    iget-object v2, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->contactImageView:Landroid/widget/ImageView;

    iget-object v3, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->c:Lco/uk/getmondo/common/ui/a;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lco/uk/getmondo/common/ui/a;->a(Ljava/lang/String;)Lco/uk/getmondo/common/ui/a$b;

    move-result-object v3

    invoke-virtual {v3, v1}, Lco/uk/getmondo/common/ui/a$b;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 146
    const v0, 0x7f0a0351

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a0352

    invoke-virtual {p0, v1}, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lco/uk/getmondo/common/d/a;->a(Ljava/lang/String;Ljava/lang/String;Z)Lco/uk/getmondo/common/d/a;

    move-result-object v0

    .line 147
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    const-string v2, "tag_error_dialog"

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/common/d/a;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 148
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 152
    iget-object v0, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 153
    iget-object v0, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->progressBarOverlay:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 154
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 158
    iget-object v0, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 159
    iget-object v0, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->progressBarOverlay:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 160
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->pinEntryView:Lco/uk/getmondo/common/ui/PinEntryView;

    invoke-virtual {v0}, Lco/uk/getmondo/common/ui/PinEntryView;->a()V

    .line 171
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->pinEntryView:Lco/uk/getmondo/common/ui/PinEntryView;

    invoke-virtual {v0}, Lco/uk/getmondo/common/ui/PinEntryView;->b()V

    .line 176
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->pinEntryView:Lco/uk/getmondo/common/ui/PinEntryView;

    invoke-virtual {v0}, Lco/uk/getmondo/common/ui/PinEntryView;->c()V

    .line 181
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 66
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 68
    const v0, 0x7f050050

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->setContentView(I)V

    .line 69
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 71
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "KEY_PAYMENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/send/data/a/f;

    .line 72
    if-nez v0, :cond_0

    .line 73
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Activity requires a payment"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    :cond_0
    iget-object v1, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->c:Lco/uk/getmondo/common/ui/a;

    invoke-static {p0}, Lco/uk/getmondo/common/ui/a;->a(Landroid/content/Context;)Lco/uk/getmondo/common/ui/a;

    move-result-object v1

    iput-object v1, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->c:Lco/uk/getmondo/common/ui/a;

    .line 77
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v1

    new-instance v2, Lco/uk/getmondo/payments/send/authentication/f;

    invoke-direct {v2, v0}, Lco/uk/getmondo/payments/send/authentication/f;-><init>(Lco/uk/getmondo/payments/send/data/a/f;)V

    invoke-interface {v1, v2}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/payments/send/authentication/f;)Lco/uk/getmondo/payments/send/authentication/e;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/payments/send/authentication/e;->a(Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;)V

    .line 78
    iget-object v0, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->a:Lco/uk/getmondo/payments/send/authentication/h;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/payments/send/authentication/h;->a(Lco/uk/getmondo/payments/send/authentication/h$a;)V

    .line 79
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->a:Lco/uk/getmondo/payments/send/authentication/h;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/authentication/h;->b()V

    .line 85
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 86
    return-void
.end method
