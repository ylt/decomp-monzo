.class public Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity_ViewBinding;
.super Ljava/lang/Object;
.source "PaymentAuthenticationActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity_ViewBinding;->a:Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;

    .line 30
    const v0, 0x7f1101d3

    const-string v1, "field \'contactImageView\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->contactImageView:Landroid/widget/ImageView;

    .line 31
    const v0, 0x7f1101d4

    const-string v1, "field \'contactNameTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->contactNameTextView:Landroid/widget/TextView;

    .line 32
    const v0, 0x7f1101d5

    const-string v1, "field \'amountView\'"

    const-class v2, Lco/uk/getmondo/common/ui/AmountView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/AmountView;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->amountView:Lco/uk/getmondo/common/ui/AmountView;

    .line 33
    const v0, 0x7f1101da

    const-string v1, "field \'progressBar\'"

    const-class v2, Landroid/widget/ProgressBar;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->progressBar:Landroid/widget/ProgressBar;

    .line 34
    const v0, 0x7f1101d9

    const-string v1, "field \'progressBarOverlay\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->progressBarOverlay:Landroid/view/View;

    .line 35
    const v0, 0x7f110472

    const-string v1, "field \'pinEntryView\'"

    const-class v2, Lco/uk/getmondo/common/ui/PinEntryView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/PinEntryView;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->pinEntryView:Lco/uk/getmondo/common/ui/PinEntryView;

    .line 36
    const v0, 0x7f1101d6

    const-string v1, "field \'accountNumberTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->accountNumberTextView:Landroid/widget/TextView;

    .line 37
    const v0, 0x7f1101d7

    const-string v1, "field \'scheduleDescriptionTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->scheduleDescriptionTextView:Landroid/widget/TextView;

    .line 38
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 43
    iget-object v0, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity_ViewBinding;->a:Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;

    .line 44
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity_ViewBinding;->a:Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;

    .line 47
    iput-object v1, v0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->contactImageView:Landroid/widget/ImageView;

    .line 48
    iput-object v1, v0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->contactNameTextView:Landroid/widget/TextView;

    .line 49
    iput-object v1, v0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->amountView:Lco/uk/getmondo/common/ui/AmountView;

    .line 50
    iput-object v1, v0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->progressBar:Landroid/widget/ProgressBar;

    .line 51
    iput-object v1, v0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->progressBarOverlay:Landroid/view/View;

    .line 52
    iput-object v1, v0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->pinEntryView:Lco/uk/getmondo/common/ui/PinEntryView;

    .line 53
    iput-object v1, v0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->accountNumberTextView:Landroid/widget/TextView;

    .line 54
    iput-object v1, v0, Lco/uk/getmondo/payments/send/authentication/PaymentAuthenticationActivity;->scheduleDescriptionTextView:Landroid/widget/TextView;

    .line 55
    return-void
.end method
