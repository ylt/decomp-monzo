.class public final Lco/uk/getmondo/payments/send/authentication/p$d;
.super Lco/uk/getmondo/payments/send/authentication/p;
.source "PaymentError.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/payments/send/authentication/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "d"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0001\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "Lco/uk/getmondo/payments/send/authentication/PaymentError$RECIPIENT_NOT_ACTIVE;",
        "Lco/uk/getmondo/payments/send/authentication/PaymentError;",
        "(Ljava/lang/String;I)V",
        "getDisplayMessage",
        "",
        "resources",
        "Landroid/content/res/Resources;",
        "payment",
        "Lco/uk/getmondo/payments/send/data/model/Payment;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 31
    const-string v0, "bad_request.recipient_not_active"

    invoke-direct {p0, p1, p2, v0}, Lco/uk/getmondo/payments/send/authentication/p;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/res/Resources;Lco/uk/getmondo/payments/send/data/a/f;)Ljava/lang/String;
    .locals 4

    .prologue
    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "payment"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    const v0, 0x7f0a035d

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-interface {p2}, Lco/uk/getmondo/payments/send/data/a/f;->d()Lco/uk/getmondo/payments/send/data/a/e;

    move-result-object v3

    invoke-interface {v3}, Lco/uk/getmondo/payments/send/data/a/e;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(R.st\u2026tive, payment.payee.name)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
