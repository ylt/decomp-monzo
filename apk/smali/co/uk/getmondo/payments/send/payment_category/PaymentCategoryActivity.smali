.class public Lco/uk/getmondo/payments/send/payment_category/PaymentCategoryActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "PaymentCategoryActivity.java"

# interfaces
.implements Lco/uk/getmondo/payments/send/payment_category/o$a;


# instance fields
.field a:Lco/uk/getmondo/payments/send/payment_category/o;

.field amountView:Lco/uk/getmondo/common/ui/AmountView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101db
    .end annotation
.end field

.field categoryView:Lco/uk/getmondo/payments/send/payment_category/CategoryView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101de
    .end annotation
.end field

.field confirmationTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1101dc
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lco/uk/getmondo/payments/send/data/a/f;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 32
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/payments/send/payment_category/PaymentCategoryActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "KEY_PAYMENT"

    .line 33
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 32
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/payment_category/PaymentCategoryActivity;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lco/uk/getmondo/payments/send/payment_category/PaymentCategoryActivity;->categoryView:Lco/uk/getmondo/payments/send/payment_category/CategoryView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lco/uk/getmondo/payments/send/payment_category/CategoryView;->setCategoryClickListener(Lco/uk/getmondo/payments/send/payment_category/CategoryView$a;)V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/payment_category/PaymentCategoryActivity;Lio/reactivex/o;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lco/uk/getmondo/payments/send/payment_category/PaymentCategoryActivity;->categoryView:Lco/uk/getmondo/payments/send/payment_category/CategoryView;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p1}, Lco/uk/getmondo/payments/send/payment_category/i;->a(Lio/reactivex/o;)Lco/uk/getmondo/payments/send/payment_category/CategoryView$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/payments/send/payment_category/CategoryView;->setCategoryClickListener(Lco/uk/getmondo/payments/send/payment_category/CategoryView$a;)V

    .line 67
    invoke-static {p0}, Lco/uk/getmondo/payments/send/payment_category/j;->a(Lco/uk/getmondo/payments/send/payment_category/PaymentCategoryActivity;)Lio/reactivex/c/f;

    move-result-object v0

    invoke-interface {p1, v0}, Lio/reactivex/o;->a(Lio/reactivex/c/f;)V

    .line 68
    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/d/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    invoke-static {p0}, Lco/uk/getmondo/payments/send/payment_category/h;->a(Lco/uk/getmondo/payments/send/payment_category/PaymentCategoryActivity;)Lio/reactivex/p;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public a(Lco/uk/getmondo/d/c;)V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lco/uk/getmondo/payments/send/payment_category/PaymentCategoryActivity;->amountView:Lco/uk/getmondo/common/ui/AmountView;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/common/ui/AmountView;->setAmount(Lco/uk/getmondo/d/c;)V

    .line 74
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 83
    iget-object v0, p0, Lco/uk/getmondo/payments/send/payment_category/PaymentCategoryActivity;->confirmationTextView:Landroid/widget/TextView;

    const v1, 0x7f0a034b

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {p0, v1, v2}, Lco/uk/getmondo/payments/send/payment_category/PaymentCategoryActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 78
    invoke-static {p0}, Lco/uk/getmondo/main/HomeActivity;->a(Landroid/content/Context;)V

    .line 79
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 55
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onBackPressed()V

    .line 60
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/payment_category/PaymentCategoryActivity;->b()V

    .line 61
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 38
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 39
    const v0, 0x7f050051

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/payment_category/PaymentCategoryActivity;->setContentView(I)V

    .line 40
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 42
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/payment_category/PaymentCategoryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "KEY_PAYMENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/send/data/a/f;

    .line 43
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/payment_category/PaymentCategoryActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v1

    new-instance v2, Lco/uk/getmondo/payments/send/payment_category/m;

    invoke-direct {v2, v0}, Lco/uk/getmondo/payments/send/payment_category/m;-><init>(Lco/uk/getmondo/payments/send/data/a/f;)V

    invoke-interface {v1, v2}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/payments/send/payment_category/m;)Lco/uk/getmondo/payments/send/payment_category/l;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/payments/send/payment_category/l;->a(Lco/uk/getmondo/payments/send/payment_category/PaymentCategoryActivity;)V

    .line 44
    iget-object v0, p0, Lco/uk/getmondo/payments/send/payment_category/PaymentCategoryActivity;->a:Lco/uk/getmondo/payments/send/payment_category/o;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/payments/send/payment_category/o;->a(Lco/uk/getmondo/payments/send/payment_category/o$a;)V

    .line 45
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lco/uk/getmondo/payments/send/payment_category/PaymentCategoryActivity;->a:Lco/uk/getmondo/payments/send/payment_category/o;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/payment_category/o;->b()V

    .line 50
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 51
    return-void
.end method
