.class Lco/uk/getmondo/payments/send/payment_category/o;
.super Lco/uk/getmondo/common/ui/b;
.source "PaymentCategoryPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/payments/send/payment_category/o$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/payments/send/payment_category/o$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lco/uk/getmondo/common/a;

.field private final e:Lco/uk/getmondo/payments/send/payment_category/b$a;

.field private final f:Lco/uk/getmondo/payments/send/data/a/f;


# direct methods
.method constructor <init>(Lio/reactivex/u;Lco/uk/getmondo/payments/send/payment_category/b$a;Lco/uk/getmondo/common/a;Lco/uk/getmondo/payments/send/data/a/f;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 28
    iput-object p1, p0, Lco/uk/getmondo/payments/send/payment_category/o;->c:Lio/reactivex/u;

    .line 29
    iput-object p3, p0, Lco/uk/getmondo/payments/send/payment_category/o;->d:Lco/uk/getmondo/common/a;

    .line 30
    iput-object p2, p0, Lco/uk/getmondo/payments/send/payment_category/o;->e:Lco/uk/getmondo/payments/send/payment_category/b$a;

    .line 31
    iput-object p4, p0, Lco/uk/getmondo/payments/send/payment_category/o;->f:Lco/uk/getmondo/payments/send/data/a/f;

    .line 32
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/payment_category/o;Lco/uk/getmondo/d/h;)Lco/uk/getmondo/d/h;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lco/uk/getmondo/payments/send/payment_category/o;->e:Lco/uk/getmondo/payments/send/payment_category/b$a;

    invoke-virtual {p1}, Lco/uk/getmondo/d/h;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/payments/send/payment_category/b$a;->a(Ljava/lang/String;)V

    .line 51
    return-object p1
.end method

.method private a(Lco/uk/getmondo/d/h;)Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/d/h;",
            ")",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/d/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    invoke-static {p0, p1}, Lco/uk/getmondo/payments/send/payment_category/s;->a(Lco/uk/getmondo/payments/send/payment_category/o;Lco/uk/getmondo/d/h;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/n;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/n;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/payments/send/payment_category/o;->c:Lio/reactivex/u;

    .line 52
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v0

    .line 49
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/payment_category/o;Lco/uk/getmondo/payments/send/payment_category/o$a;Lco/uk/getmondo/d/h;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lco/uk/getmondo/payments/send/payment_category/o;->d:Lco/uk/getmondo/common/a;

    invoke-static {p2}, Lco/uk/getmondo/api/model/tracking/Impression;->a(Lco/uk/getmondo/d/h;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 44
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/payment_category/o$a;->b()V

    .line 45
    return-void
.end method

.method static synthetic b(Lco/uk/getmondo/payments/send/payment_category/o;Lco/uk/getmondo/d/h;)Lio/reactivex/n;
    .locals 1

    invoke-direct {p0, p1}, Lco/uk/getmondo/payments/send/payment_category/o;->a(Lco/uk/getmondo/d/h;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lco/uk/getmondo/payments/send/payment_category/o$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/send/payment_category/o;->a(Lco/uk/getmondo/payments/send/payment_category/o$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/payments/send/payment_category/o$a;)V
    .locals 3

    .prologue
    .line 36
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 37
    iget-object v0, p0, Lco/uk/getmondo/payments/send/payment_category/o;->f:Lco/uk/getmondo/payments/send/data/a/f;

    invoke-interface {v0}, Lco/uk/getmondo/payments/send/data/a/f;->a()Lco/uk/getmondo/d/c;

    move-result-object v0

    invoke-interface {p1, v0}, Lco/uk/getmondo/payments/send/payment_category/o$a;->a(Lco/uk/getmondo/d/c;)V

    .line 38
    iget-object v0, p0, Lco/uk/getmondo/payments/send/payment_category/o;->f:Lco/uk/getmondo/payments/send/data/a/f;

    invoke-interface {v0}, Lco/uk/getmondo/payments/send/data/a/f;->d()Lco/uk/getmondo/payments/send/data/a/e;

    move-result-object v0

    invoke-interface {v0}, Lco/uk/getmondo/payments/send/data/a/e;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lco/uk/getmondo/payments/send/payment_category/o$a;->a(Ljava/lang/String;)V

    .line 40
    invoke-interface {p1}, Lco/uk/getmondo/payments/send/payment_category/o$a;->a()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/payments/send/payment_category/p;->a(Lco/uk/getmondo/payments/send/payment_category/o;)Lio/reactivex/c/h;

    move-result-object v1

    .line 41
    invoke-virtual {v0, v1}, Lio/reactivex/n;->switchMap(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/payments/send/payment_category/q;->a(Lco/uk/getmondo/payments/send/payment_category/o;Lco/uk/getmondo/payments/send/payment_category/o$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/payments/send/payment_category/r;->a()Lio/reactivex/c/g;

    move-result-object v2

    .line 42
    invoke-virtual {v0, v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 40
    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/payment_category/o;->a(Lio/reactivex/b/b;)V

    .line 46
    return-void
.end method
