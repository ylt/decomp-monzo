.class public Lco/uk/getmondo/payments/send/payment_category/CategoryView;
.super Landroid/widget/GridLayout;
.source "CategoryView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/payments/send/payment_category/CategoryView$a;
    }
.end annotation


# instance fields
.field private a:Lco/uk/getmondo/payments/send/payment_category/CategoryView$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Landroid/widget/GridLayout;-><init>(Landroid/content/Context;)V

    .line 23
    invoke-direct {p0}, Lco/uk/getmondo/payments/send/payment_category/CategoryView;->a()V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Landroid/widget/GridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    invoke-direct {p0}, Lco/uk/getmondo/payments/send/payment_category/CategoryView;->a()V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/GridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    invoke-direct {p0}, Lco/uk/getmondo/payments/send/payment_category/CategoryView;->a()V

    .line 34
    return-void
.end method

.method private a(Lco/uk/getmondo/d/h;)Landroid/view/View;
    .locals 5

    .prologue
    .line 56
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/payment_category/CategoryView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0500f4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 57
    const v0, 0x7f110400

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 58
    const v1, 0x7f1103ff

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 61
    invoke-virtual {p1}, Lco/uk/getmondo/d/h;->a()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 62
    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p1}, Lco/uk/getmondo/d/h;->b()I

    move-result v4

    invoke-static {v3, v4}, Landroid/support/v4/content/a;->c(Landroid/content/Context;I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 63
    invoke-virtual {p1}, Lco/uk/getmondo/d/h;->e()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 65
    invoke-static {p0, p1}, Lco/uk/getmondo/payments/send/payment_category/a;->a(Lco/uk/getmondo/payments/send/payment_category/CategoryView;Lco/uk/getmondo/d/h;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    return-object v2
.end method

.method private a()V
    .locals 8

    .prologue
    .line 37
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/payment_category/CategoryView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/payment_category/CategoryView;->setColumnCount(I)V

    .line 39
    invoke-static {}, Lco/uk/getmondo/d/h;->values()[Lco/uk/getmondo/d/h;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 40
    invoke-direct {p0, v3}, Lco/uk/getmondo/payments/send/payment_category/CategoryView;->a(Lco/uk/getmondo/d/h;)Landroid/view/View;

    move-result-object v3

    .line 41
    new-instance v4, Landroid/widget/GridLayout$LayoutParams;

    invoke-direct {v4}, Landroid/widget/GridLayout$LayoutParams;-><init>()V

    .line 42
    const/16 v5, 0x11

    invoke-virtual {v4, v5}, Landroid/widget/GridLayout$LayoutParams;->setGravity(I)V

    .line 43
    const/high16 v5, -0x80000000

    sget-object v6, Landroid/widget/GridLayout;->FILL:Landroid/widget/GridLayout$Alignment;

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-static {v5, v6, v7}, Landroid/widget/GridLayout;->spec(ILandroid/widget/GridLayout$Alignment;F)Landroid/widget/GridLayout$Spec;

    move-result-object v5

    iput-object v5, v4, Landroid/widget/GridLayout$LayoutParams;->columnSpec:Landroid/widget/GridLayout$Spec;

    .line 44
    invoke-virtual {v3, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 45
    const/16 v4, 0x50

    invoke-static {v4}, Lco/uk/getmondo/common/k/n;->b(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setMinimumWidth(I)V

    .line 46
    invoke-virtual {p0, v3}, Lco/uk/getmondo/payments/send/payment_category/CategoryView;->addView(Landroid/view/View;)V

    .line 39
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 48
    :cond_0
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/payment_category/CategoryView;Lco/uk/getmondo/d/h;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lco/uk/getmondo/payments/send/payment_category/CategoryView;->a:Lco/uk/getmondo/payments/send/payment_category/CategoryView$a;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lco/uk/getmondo/payments/send/payment_category/CategoryView;->a:Lco/uk/getmondo/payments/send/payment_category/CategoryView$a;

    invoke-interface {v0, p1}, Lco/uk/getmondo/payments/send/payment_category/CategoryView$a;->a(Lco/uk/getmondo/d/h;)V

    .line 69
    :cond_0
    return-void
.end method


# virtual methods
.method public setCategoryClickListener(Lco/uk/getmondo/payments/send/payment_category/CategoryView$a;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lco/uk/getmondo/payments/send/payment_category/CategoryView;->a:Lco/uk/getmondo/payments/send/payment_category/CategoryView$a;

    .line 52
    return-void
.end method
