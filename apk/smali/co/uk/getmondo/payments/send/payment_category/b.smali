.class public Lco/uk/getmondo/payments/send/payment_category/b;
.super Ljava/lang/Object;
.source "P2pCategoryHackHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/payments/send/payment_category/b$a;
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/api/MonzoApi;

.field private final b:Lco/uk/getmondo/payments/send/payment_category/b$a;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/payments/send/payment_category/b$a;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lco/uk/getmondo/payments/send/payment_category/b;->a:Lco/uk/getmondo/api/MonzoApi;

    .line 34
    iput-object p2, p0, Lco/uk/getmondo/payments/send/payment_category/b;->b:Lco/uk/getmondo/payments/send/payment_category/b$a;

    .line 35
    return-void
.end method

.method private a(Lco/uk/getmondo/api/model/feed/ApiFeedItem;Ljava/lang/String;)Lco/uk/getmondo/api/model/feed/ApiFeedItem;
    .locals 10

    .prologue
    .line 58
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->f()Lco/uk/getmondo/api/model/feed/ApiTransaction;

    move-result-object v0

    .line 59
    invoke-virtual {v0, p2}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->a(Ljava/lang/String;)Lco/uk/getmondo/api/model/feed/ApiTransaction;

    move-result-object v6

    .line 60
    new-instance v0, Lco/uk/getmondo/api/model/feed/ApiFeedItem;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->e()Ljava/util/Date;

    move-result-object v5

    const/4 v7, 0x0

    .line 61
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->h()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->i()Z

    move-result v9

    invoke-direct/range {v0 .. v9}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Lco/uk/getmondo/api/model/feed/ApiTransaction;Lco/uk/getmondo/api/model/feed/ApiFeedItem$Params;Ljava/lang/String;Z)V

    .line 60
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/payment_category/b;Lco/uk/getmondo/api/model/feed/ApiFeedItem;)Lio/reactivex/z;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 39
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/feed/ApiFeedItem;->f()Lco/uk/getmondo/api/model/feed/ApiTransaction;

    move-result-object v0

    .line 40
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->i()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    invoke-static {v0}, Lco/uk/getmondo/payments/send/payment_category/b;->a(Lco/uk/getmondo/api/model/feed/ApiTransaction;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lco/uk/getmondo/payments/send/payment_category/b;->b:Lco/uk/getmondo/payments/send/payment_category/b$a;

    .line 41
    invoke-virtual {v1}, Lco/uk/getmondo/payments/send/payment_category/b$a;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 42
    iget-object v1, p0, Lco/uk/getmondo/payments/send/payment_category/b;->b:Lco/uk/getmondo/payments/send/payment_category/b$a;

    invoke-virtual {v1}, Lco/uk/getmondo/payments/send/payment_category/b$a;->a()Ljava/lang/String;

    move-result-object v1

    .line 43
    iget-object v2, p0, Lco/uk/getmondo/payments/send/payment_category/b;->a:Lco/uk/getmondo/api/MonzoApi;

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->g()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0, v1}, Lco/uk/getmondo/api/MonzoApi;->updateTransactionCategory(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    .line 45
    invoke-direct {p0, p1, v1}, Lco/uk/getmondo/payments/send/payment_category/b;->a(Lco/uk/getmondo/api/model/feed/ApiFeedItem;Ljava/lang/String;)Lco/uk/getmondo/api/model/feed/ApiFeedItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/payments/send/payment_category/d;->a(Lco/uk/getmondo/payments/send/payment_category/b;)Lio/reactivex/c/g;

    move-result-object v1

    .line 47
    invoke-virtual {v0, v1}, Lio/reactivex/v;->c(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/payments/send/payment_category/e;->a()Lio/reactivex/c/g;

    move-result-object v1

    .line 49
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    .line 50
    invoke-virtual {v0, p1}, Lio/reactivex/v;->b(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    .line 52
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Lio/reactivex/v;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lco/uk/getmondo/api/model/feed/ApiTransaction;)Z
    .locals 2

    .prologue
    .line 65
    invoke-virtual {p0}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->t()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->t()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "mondo"

    invoke-virtual {p0}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->t()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "monzo"

    .line 66
    invoke-virtual {p0}, Lco/uk/getmondo/api/model/feed/ApiTransaction;->t()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 65
    :goto_0
    return v0

    .line 66
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lco/uk/getmondo/payments/send/payment_category/b;Lco/uk/getmondo/api/model/feed/ApiFeedItem;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lco/uk/getmondo/payments/send/payment_category/b;->b:Lco/uk/getmondo/payments/send/payment_category/b$a;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/payment_category/b$a;->b()V

    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/api/model/feed/ApiFeedItem;)Lio/reactivex/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/model/feed/ApiFeedItem;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/feed/ApiFeedItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    invoke-static {p0, p1}, Lco/uk/getmondo/payments/send/payment_category/c;->a(Lco/uk/getmondo/payments/send/payment_category/b;Lco/uk/getmondo/api/model/feed/ApiFeedItem;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/v;->a(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method
