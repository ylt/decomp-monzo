.class public final Lco/uk/getmondo/payments/send/payment_category/n;
.super Ljava/lang/Object;
.source "PaymentCategoryModule_ProvidesPaymentFactory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/payments/send/data/a/f;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lco/uk/getmondo/payments/send/payment_category/m;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-class v0, Lco/uk/getmondo/payments/send/payment_category/n;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/payments/send/payment_category/n;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lco/uk/getmondo/payments/send/payment_category/m;)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    sget-boolean v0, Lco/uk/getmondo/payments/send/payment_category/n;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 17
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/payments/send/payment_category/n;->b:Lco/uk/getmondo/payments/send/payment_category/m;

    .line 18
    return-void
.end method

.method public static a(Lco/uk/getmondo/payments/send/payment_category/m;)Lb/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/payments/send/payment_category/m;",
            ")",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/payments/send/data/a/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    new-instance v0, Lco/uk/getmondo/payments/send/payment_category/n;

    invoke-direct {v0, p0}, Lco/uk/getmondo/payments/send/payment_category/n;-><init>(Lco/uk/getmondo/payments/send/payment_category/m;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/payments/send/data/a/f;
    .locals 2

    .prologue
    .line 22
    iget-object v0, p0, Lco/uk/getmondo/payments/send/payment_category/n;->b:Lco/uk/getmondo/payments/send/payment_category/m;

    .line 23
    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/payment_category/m;->a()Lco/uk/getmondo/payments/send/data/a/f;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 22
    invoke-static {v0, v1}, Lb/a/d;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/send/data/a/f;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/payment_category/n;->a()Lco/uk/getmondo/payments/send/data/a/f;

    move-result-object v0

    return-object v0
.end method
