.class public Lco/uk/getmondo/payments/send/data/p;
.super Ljava/lang/Object;
.source "UserSettingsStorage.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/d/am;Lio/realm/av;)V
    .locals 0

    .prologue
    .line 57
    invoke-virtual {p1, p0}, Lio/realm/av;->d(Lio/realm/bb;)V

    return-void
.end method


# virtual methods
.method public a()Lco/uk/getmondo/payments/send/data/a/d;
    .locals 2

    .prologue
    .line 19
    invoke-static {}, Lio/realm/av;->n()Lio/realm/av;

    move-result-object v1

    .line 20
    const-class v0, Lco/uk/getmondo/d/am;

    invoke-virtual {v1, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/bf;->h()Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/am;

    .line 21
    if-nez v0, :cond_0

    .line 22
    invoke-virtual {v1}, Lio/realm/av;->close()V

    .line 23
    sget-object v0, Lco/uk/getmondo/payments/send/data/a/d;->b:Lco/uk/getmondo/payments/send/data/a/d;

    .line 27
    :goto_0
    return-object v0

    .line 25
    :cond_0
    invoke-virtual {v1, v0}, Lio/realm/av;->e(Lio/realm/bb;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/am;

    .line 26
    invoke-virtual {v1}, Lio/realm/av;->close()V

    .line 27
    invoke-virtual {v0}, Lco/uk/getmondo/d/am;->a()Lco/uk/getmondo/payments/send/data/a/d;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lco/uk/getmondo/d/am;)V
    .locals 4

    .prologue
    .line 56
    invoke-static {}, Lio/realm/av;->n()Lio/realm/av;

    move-result-object v2

    const/4 v1, 0x0

    .line 57
    :try_start_0
    invoke-static {p1}, Lco/uk/getmondo/payments/send/data/q;->a(Lco/uk/getmondo/d/am;)Lio/realm/av$a;

    move-result-object v0

    invoke-virtual {v2, v0}, Lio/realm/av;->a(Lio/realm/av$a;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 58
    if-eqz v2, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 58
    :cond_1
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_0

    .line 56
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 58
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_1
    if-eqz v2, :cond_2

    if-eqz v1, :cond_3

    :try_start_3
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    :cond_2
    :goto_2
    throw v0

    :cond_3
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public b()Lco/uk/getmondo/d/p;
    .locals 6

    .prologue
    .line 31
    invoke-static {}, Lio/realm/av;->n()Lio/realm/av;

    move-result-object v2

    const/4 v1, 0x0

    .line 32
    :try_start_0
    const-class v0, Lco/uk/getmondo/d/am;

    invoke-virtual {v2, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    const-string v3, "id"

    const-string v4, "user_settings_storage_static_id"

    .line 33
    invoke-virtual {v0, v3, v4}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Lio/realm/bf;->h()Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/am;

    .line 35
    if-nez v0, :cond_2

    .line 37
    new-instance v0, Lco/uk/getmondo/d/p;

    invoke-direct {v0}, Lco/uk/getmondo/d/p;-><init>()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 40
    if-eqz v2, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2

    .line 39
    :cond_0
    :goto_0
    return-object v0

    .line 40
    :cond_1
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_0

    .line 39
    :cond_2
    :try_start_2
    invoke-virtual {v2, v0}, Lio/realm/av;->e(Lio/realm/bb;)Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/am;

    invoke-virtual {v0}, Lco/uk/getmondo/d/am;->c()Lco/uk/getmondo/d/p;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    .line 40
    if-eqz v2, :cond_0

    if-eqz v1, :cond_3

    :try_start_3
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :cond_3
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_0

    .line 31
    :catch_1
    move-exception v0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 40
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_1
    if-eqz v2, :cond_4

    if-eqz v1, :cond_5

    :try_start_5
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_3

    :cond_4
    :goto_2
    throw v0

    :cond_5
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_0

    :catch_3
    move-exception v1

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public c()Z
    .locals 6

    .prologue
    .line 62
    invoke-static {}, Lio/realm/av;->n()Lio/realm/av;

    move-result-object v2

    const/4 v1, 0x0

    .line 63
    :try_start_0
    const-class v0, Lco/uk/getmondo/d/am;

    invoke-virtual {v2, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    const-string v3, "id"

    const-string v4, "user_settings_storage_static_id"

    .line 64
    invoke-virtual {v0, v3, v4}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lio/realm/bf;->h()Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/am;

    .line 66
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lco/uk/getmondo/d/am;->d()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 67
    :goto_0
    if-eqz v2, :cond_0

    if-eqz v1, :cond_2

    :try_start_1
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 66
    :cond_0
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 67
    :cond_2
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_1

    .line 62
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 67
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_2
    if-eqz v2, :cond_3

    if-eqz v1, :cond_4

    :try_start_3
    invoke-virtual {v2}, Lio/realm/av;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    :cond_3
    :goto_3
    throw v0

    :cond_4
    invoke-virtual {v2}, Lio/realm/av;->close()V

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_1

    :catch_2
    move-exception v1

    goto :goto_3

    :catchall_1
    move-exception v0

    goto :goto_2
.end method
