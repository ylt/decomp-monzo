.class final Lco/uk/getmondo/payments/send/data/PeerToPeerRepository$b;
.super Ljava/lang/Object;
.source "PeerToPeerRepository.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;->a(Lco/uk/getmondo/payments/send/a/b;)Lio/reactivex/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/z",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lco/uk/getmondo/model/Peer;",
        "userResponse",
        "Lco/uk/getmondo/model/UserResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;

.field final synthetic b:Lco/uk/getmondo/payments/send/a/b;


# direct methods
.method constructor <init>(Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;Lco/uk/getmondo/payments/send/a/b;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/send/data/PeerToPeerRepository$b;->a:Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;

    iput-object p2, p0, Lco/uk/getmondo/payments/send/data/PeerToPeerRepository$b;->b:Lco/uk/getmondo/payments/send/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/d/al;)Lio/reactivex/v;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/d/al;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/d/aa;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const-string v0, "userResponse"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    iget-object v0, p0, Lco/uk/getmondo/payments/send/data/PeerToPeerRepository$b;->a:Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;

    iget-object v2, p0, Lco/uk/getmondo/payments/send/data/PeerToPeerRepository$b;->b:Lco/uk/getmondo/payments/send/a/b;

    const/4 v4, 0x1

    move-object v3, p1

    move-object v5, v1

    invoke-static/range {v0 .. v5}, Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;->a(Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;Ljava/lang/String;Lco/uk/getmondo/payments/send/a/b;Lco/uk/getmondo/d/al;ILjava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    check-cast p1, Lco/uk/getmondo/d/al;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/send/data/PeerToPeerRepository$b;->a(Lco/uk/getmondo/d/al;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method
