.class public Lco/uk/getmondo/payments/send/data/a;
.super Ljava/lang/Object;
.source "PaymentsManager.java"


# instance fields
.field private final a:Lco/uk/getmondo/api/PaymentsApi;

.field private final b:Lco/uk/getmondo/common/accounts/d;

.field private final c:Lco/uk/getmondo/payments/a/i;

.field private final d:Lco/uk/getmondo/payments/send/data/f;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/api/PaymentsApi;Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/payments/a/i;Lco/uk/getmondo/payments/send/data/f;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lco/uk/getmondo/payments/send/data/a;->a:Lco/uk/getmondo/api/PaymentsApi;

    .line 37
    iput-object p2, p0, Lco/uk/getmondo/payments/send/data/a;->b:Lco/uk/getmondo/common/accounts/d;

    .line 38
    iput-object p3, p0, Lco/uk/getmondo/payments/send/data/a;->c:Lco/uk/getmondo/payments/a/i;

    .line 39
    iput-object p4, p0, Lco/uk/getmondo/payments/send/data/a;->d:Lco/uk/getmondo/payments/send/data/f;

    .line 40
    return-void
.end method

.method private a(Lco/uk/getmondo/payments/send/data/a/c;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lco/uk/getmondo/payments/send/data/a;->b:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->d()Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0, p1, p3, p2}, Lco/uk/getmondo/payments/send/data/c;->a(Lco/uk/getmondo/payments/send/data/a;Lco/uk/getmondo/payments/send/data/a/c;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/c/h;

    move-result-object v1

    .line 64
    invoke-virtual {v0, v1}, Lio/reactivex/v;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    .line 63
    return-object v0
.end method

.method private a(Ljava/lang/String;Lco/uk/getmondo/payments/send/data/a/g;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;
    .locals 11

    .prologue
    .line 87
    invoke-virtual {p2}, Lco/uk/getmondo/payments/send/data/a/g;->a()Lco/uk/getmondo/d/c;

    move-result-object v1

    .line 88
    invoke-virtual {p2}, Lco/uk/getmondo/payments/send/data/a/g;->c()Lco/uk/getmondo/d/aa;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->d()Ljava/lang/String;

    move-result-object v2

    .line 89
    const-string v3, "phone"

    .line 91
    invoke-static {v2}, Lco/uk/getmondo/common/k/p;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    invoke-virtual {p2}, Lco/uk/getmondo/payments/send/data/a/g;->c()Lco/uk/getmondo/d/aa;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->e()Ljava/lang/String;

    move-result-object v2

    .line 93
    const-string v3, "username"

    .line 95
    :cond_0
    invoke-static {v2}, Lco/uk/getmondo/common/k/p;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 96
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Peer must have a phone number or username"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lio/reactivex/b;->a(Ljava/lang/Throwable;)Lio/reactivex/b;

    move-result-object v0

    .line 99
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/payments/send/data/a;->a:Lco/uk/getmondo/api/PaymentsApi;

    invoke-virtual {v1}, Lco/uk/getmondo/d/c;->k()J

    move-result-wide v4

    neg-long v4, v4

    invoke-virtual {v1}, Lco/uk/getmondo/d/c;->l()Lco/uk/getmondo/common/i/c;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/common/i/c;->b()Ljava/lang/String;

    move-result-object v6

    const-string v7, "pin"

    .line 100
    invoke-virtual {p2}, Lco/uk/getmondo/payments/send/data/a/g;->b()Ljava/lang/String;

    move-result-object v10

    move-object v1, p1

    move-object v8, p3

    move-object v9, p4

    .line 99
    invoke-interface/range {v0 .. v10}, Lco/uk/getmondo/api/PaymentsApi;->transferMoneyToPeer(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/data/a;Lco/uk/getmondo/payments/send/data/a/c;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/d;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 65
    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/data/a/c;->a()Lco/uk/getmondo/d/c;

    move-result-object v1

    .line 66
    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/data/a/c;->c()Lco/uk/getmondo/payments/send/data/a/b;

    move-result-object v7

    .line 67
    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/data/a/c;->e()Lco/uk/getmondo/payments/a/a/d;

    move-result-object v0

    .line 69
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/d;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 70
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/payments/send/data/a;->a:Lco/uk/getmondo/api/PaymentsApi;

    .line 71
    invoke-virtual {v1}, Lco/uk/getmondo/d/c;->k()J

    move-result-wide v2

    .line 72
    invoke-virtual {v1}, Lco/uk/getmondo/d/c;->l()Lco/uk/getmondo/common/i/c;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/common/i/c;->b()Ljava/lang/String;

    move-result-object v4

    .line 73
    invoke-virtual {v7}, Lco/uk/getmondo/payments/send/data/a/b;->e()Ljava/lang/String;

    move-result-object v5

    .line 74
    invoke-virtual {v7}, Lco/uk/getmondo/payments/send/data/a/b;->d()Ljava/lang/String;

    move-result-object v6

    .line 75
    invoke-virtual {v7}, Lco/uk/getmondo/payments/send/data/a/b;->b()Ljava/lang/String;

    move-result-object v7

    const-string v9, "pin"

    .line 79
    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/data/a/c;->b()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v1, p4

    move-object v8, p2

    move-object v10, p3

    .line 70
    invoke-interface/range {v0 .. v11}, Lco/uk/getmondo/api/PaymentsApi;->transferMoneyToBank(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    .line 81
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/payments/send/data/a;->c:Lco/uk/getmondo/payments/a/i;

    invoke-virtual {v0, p1, p3, p2}, Lco/uk/getmondo/payments/a/i;->a(Lco/uk/getmondo/payments/send/data/a/c;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/data/a;Lco/uk/getmondo/payments/send/data/a/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/d;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 49
    instance-of v0, p1, Lco/uk/getmondo/payments/send/data/a/g;

    if-eqz v0, :cond_0

    .line 50
    check-cast p1, Lco/uk/getmondo/payments/send/data/a/g;

    invoke-direct {p0, p2, p1, p3, p4}, Lco/uk/getmondo/payments/send/data/a;->a(Ljava/lang/String;Lco/uk/getmondo/payments/send/data/a/g;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    .line 52
    :goto_0
    return-object v0

    .line 51
    :cond_0
    instance-of v0, p1, Lco/uk/getmondo/payments/send/data/a/c;

    if-eqz v0, :cond_1

    .line 52
    check-cast p1, Lco/uk/getmondo/payments/send/data/a/c;

    invoke-direct {p0, p1, p3, p4}, Lco/uk/getmondo/payments/send/data/a;->a(Lco/uk/getmondo/payments/send/data/a/c;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    goto :goto_0

    .line 54
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported payment type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/payments/send/data/a/b;)Lio/reactivex/b;
    .locals 3

    .prologue
    .line 44
    iget-object v0, p0, Lco/uk/getmondo/payments/send/data/a;->a:Lco/uk/getmondo/api/PaymentsApi;

    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/data/a/b;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/data/a/b;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lco/uk/getmondo/api/PaymentsApi;->validatePayee(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Lco/uk/getmondo/payments/send/data/a/f;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;
    .locals 1

    .prologue
    .line 48
    invoke-static {p0, p2, p1, p3, p4}, Lco/uk/getmondo/payments/send/data/b;->a(Lco/uk/getmondo/payments/send/data/a;Lco/uk/getmondo/payments/send/data/a/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/b;->a(Ljava/util/concurrent/Callable;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public a()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/payments/send/data/a/e;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lco/uk/getmondo/payments/send/data/a;->d:Lco/uk/getmondo/payments/send/data/f;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/data/f;->a()Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method
