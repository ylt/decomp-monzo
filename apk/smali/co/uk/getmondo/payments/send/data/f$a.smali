.class final Lco/uk/getmondo/payments/send/data/f$a;
.super Lkotlin/d/b/m;
.source "RecentPayeesStorage.kt"

# interfaces
.implements Lkotlin/d/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/payments/send/data/f;->a()Lio/reactivex/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/b",
        "<",
        "Lio/realm/av;",
        "Lio/realm/bg",
        "<",
        "Lco/uk/getmondo/d/aj;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/realm/RealmResults;",
        "Lco/uk/getmondo/model/Transaction;",
        "kotlin.jvm.PlatformType",
        "realm",
        "Lio/realm/Realm;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/payments/send/data/f$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lco/uk/getmondo/payments/send/data/f$a;

    invoke-direct {v0}, Lco/uk/getmondo/payments/send/data/f$a;-><init>()V

    sput-object v0, Lco/uk/getmondo/payments/send/data/f$a;->a:Lco/uk/getmondo/payments/send/data/f$a;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lio/realm/av;)Lio/realm/bg;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/av;",
            ")",
            "Lio/realm/bg",
            "<",
            "Lco/uk/getmondo/d/aj;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v0, "realm"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    const-class v0, Lco/uk/getmondo/d/aj;

    invoke-virtual {p1, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    .line 19
    invoke-virtual {v0}, Lio/realm/bf;->a()Lio/realm/bf;

    move-result-object v0

    .line 20
    const-string v1, "peer.phoneNumber"

    invoke-virtual {v0, v1}, Lio/realm/bf;->b(Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    .line 21
    const-string v1, "peer.isEnriched"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lio/realm/bf;

    move-result-object v0

    .line 22
    const-string v1, "peerToPeer"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lio/realm/bf;

    move-result-object v0

    .line 23
    const-string v1, "fromMonzoMe"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lio/realm/bf;

    move-result-object v0

    .line 24
    invoke-virtual {v0}, Lio/realm/bf;->c()Lio/realm/bf;

    move-result-object v0

    .line 25
    const-string v1, "scheme"

    sget-object v2, Lco/uk/getmondo/d/af;->FASTER_PAYMENT:Lco/uk/getmondo/d/af;

    invoke-virtual {v2}, Lco/uk/getmondo/d/af;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    .line 26
    const-string v1, "bankDetails"

    invoke-virtual {v0, v1}, Lio/realm/bf;->b(Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    .line 27
    invoke-virtual {v0}, Lio/realm/bf;->b()Lio/realm/bf;

    move-result-object v0

    .line 28
    const-string v1, "amountValue"

    invoke-virtual {v0, v1, v3}, Lio/realm/bf;->a(Ljava/lang/String;I)Lio/realm/bf;

    move-result-object v0

    .line 29
    const-string v1, "declineReason"

    invoke-virtual {v0, v1}, Lio/realm/bf;->a(Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    .line 30
    const-string v1, "created"

    sget-object v2, Lio/realm/bl;->b:Lio/realm/bl;

    invoke-virtual {v0, v1, v2}, Lio/realm/bf;->a(Ljava/lang/String;Lio/realm/bl;)Lio/realm/bg;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    check-cast p1, Lio/realm/av;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/send/data/f$a;->a(Lio/realm/av;)Lio/realm/bg;

    move-result-object v0

    return-object v0
.end method
