.class public final enum Lco/uk/getmondo/payments/send/data/a/d;
.super Ljava/lang/Enum;
.source "P2pStatus.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/payments/send/data/a/d;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/payments/send/data/a/d;

.field public static final enum b:Lco/uk/getmondo/payments/send/data/a/d;

.field public static final enum c:Lco/uk/getmondo/payments/send/data/a/d;

.field private static final synthetic e:[Lco/uk/getmondo/payments/send/data/a/d;


# instance fields
.field public final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4
    new-instance v0, Lco/uk/getmondo/payments/send/data/a/d;

    const-string v1, "ENABLED"

    const-string v2, "enabled"

    invoke-direct {v0, v1, v3, v2}, Lco/uk/getmondo/payments/send/data/a/d;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/payments/send/data/a/d;->a:Lco/uk/getmondo/payments/send/data/a/d;

    .line 5
    new-instance v0, Lco/uk/getmondo/payments/send/data/a/d;

    const-string v1, "DISABLED"

    const-string v2, "disabled"

    invoke-direct {v0, v1, v4, v2}, Lco/uk/getmondo/payments/send/data/a/d;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/payments/send/data/a/d;->b:Lco/uk/getmondo/payments/send/data/a/d;

    .line 6
    new-instance v0, Lco/uk/getmondo/payments/send/data/a/d;

    const-string v1, "BLOCKED"

    const-string v2, "blocked"

    invoke-direct {v0, v1, v5, v2}, Lco/uk/getmondo/payments/send/data/a/d;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lco/uk/getmondo/payments/send/data/a/d;->c:Lco/uk/getmondo/payments/send/data/a/d;

    .line 3
    const/4 v0, 0x3

    new-array v0, v0, [Lco/uk/getmondo/payments/send/data/a/d;

    sget-object v1, Lco/uk/getmondo/payments/send/data/a/d;->a:Lco/uk/getmondo/payments/send/data/a/d;

    aput-object v1, v0, v3

    sget-object v1, Lco/uk/getmondo/payments/send/data/a/d;->b:Lco/uk/getmondo/payments/send/data/a/d;

    aput-object v1, v0, v4

    sget-object v1, Lco/uk/getmondo/payments/send/data/a/d;->c:Lco/uk/getmondo/payments/send/data/a/d;

    aput-object v1, v0, v5

    sput-object v0, Lco/uk/getmondo/payments/send/data/a/d;->e:[Lco/uk/getmondo/payments/send/data/a/d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 11
    iput-object p3, p0, Lco/uk/getmondo/payments/send/data/a/d;->d:Ljava/lang/String;

    .line 12
    return-void
.end method

.method public static a(Ljava/lang/String;)Lco/uk/getmondo/payments/send/data/a/d;
    .locals 5

    .prologue
    .line 19
    invoke-static {}, Lco/uk/getmondo/payments/send/data/a/d;->values()[Lco/uk/getmondo/payments/send/data/a/d;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 20
    iget-object v4, v3, Lco/uk/getmondo/payments/send/data/a/d;->d:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 21
    return-object v3

    .line 19
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 24
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "P2pStatus id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/payments/send/data/a/d;
    .locals 1

    .prologue
    .line 3
    const-class v0, Lco/uk/getmondo/payments/send/data/a/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/send/data/a/d;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/payments/send/data/a/d;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lco/uk/getmondo/payments/send/data/a/d;->e:[Lco/uk/getmondo/payments/send/data/a/d;

    invoke-virtual {v0}, [Lco/uk/getmondo/payments/send/data/a/d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/payments/send/data/a/d;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lco/uk/getmondo/payments/send/data/a/d;->d:Ljava/lang/String;

    return-object v0
.end method
