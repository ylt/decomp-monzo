.class final synthetic Lco/uk/getmondo/payments/send/data/b;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field private final a:Lco/uk/getmondo/payments/send/data/a;

.field private final b:Lco/uk/getmondo/payments/send/data/a/f;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lco/uk/getmondo/payments/send/data/a;Lco/uk/getmondo/payments/send/data/a/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/payments/send/data/b;->a:Lco/uk/getmondo/payments/send/data/a;

    iput-object p2, p0, Lco/uk/getmondo/payments/send/data/b;->b:Lco/uk/getmondo/payments/send/data/a/f;

    iput-object p3, p0, Lco/uk/getmondo/payments/send/data/b;->c:Ljava/lang/String;

    iput-object p4, p0, Lco/uk/getmondo/payments/send/data/b;->d:Ljava/lang/String;

    iput-object p5, p0, Lco/uk/getmondo/payments/send/data/b;->e:Ljava/lang/String;

    return-void
.end method

.method public static a(Lco/uk/getmondo/payments/send/data/a;Lco/uk/getmondo/payments/send/data/a/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/concurrent/Callable;
    .locals 6

    new-instance v0, Lco/uk/getmondo/payments/send/data/b;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/payments/send/data/b;-><init>(Lco/uk/getmondo/payments/send/data/a;Lco/uk/getmondo/payments/send/data/a/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public call()Ljava/lang/Object;
    .locals 5

    iget-object v0, p0, Lco/uk/getmondo/payments/send/data/b;->a:Lco/uk/getmondo/payments/send/data/a;

    iget-object v1, p0, Lco/uk/getmondo/payments/send/data/b;->b:Lco/uk/getmondo/payments/send/data/a/f;

    iget-object v2, p0, Lco/uk/getmondo/payments/send/data/b;->c:Ljava/lang/String;

    iget-object v3, p0, Lco/uk/getmondo/payments/send/data/b;->d:Ljava/lang/String;

    iget-object v4, p0, Lco/uk/getmondo/payments/send/data/b;->e:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lco/uk/getmondo/payments/send/data/a;->a(Lco/uk/getmondo/payments/send/data/a;Lco/uk/getmondo/payments/send/data/a/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/d;

    move-result-object v0

    return-object v0
.end method
