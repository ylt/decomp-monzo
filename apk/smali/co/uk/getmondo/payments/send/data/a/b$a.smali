.class public final Lco/uk/getmondo/payments/send/data/a/b$a;
.super Ljava/lang/Object;
.source "BankPayee.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/payments/send/data/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0005\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fJ\u000e\u0010\u0010\u001a\u00020\r2\u0006\u0010\u0011\u001a\u00020\u000fJ\u000e\u0010\u0012\u001a\u00020\r2\u0006\u0010\u0013\u001a\u00020\u000fR\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0014\u0010\n\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\t\u00a8\u0006\u0014"
    }
    d2 = {
        "Lco/uk/getmondo/payments/send/data/model/BankPayee$Validator;",
        "",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "Lco/uk/getmondo/payments/send/data/model/BankPayee;",
        "accountNumberRegex",
        "Lkotlin/text/Regex;",
        "getAccountNumberRegex",
        "()Lkotlin/text/Regex;",
        "sortCodeRegex",
        "getSortCodeRegex",
        "isValidAccountNumber",
        "",
        "accountNumber",
        "",
        "isValidRecipientName",
        "recipientName",
        "isValidSortCode",
        "sortCode",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lco/uk/getmondo/payments/send/data/a/b$a;-><init>()V

    return-void
.end method

.method private final a()Lkotlin/h/g;
    .locals 1

    .prologue
    .line 35
    invoke-static {}, Lco/uk/getmondo/payments/send/data/a/b;->f()Lkotlin/h/g;

    move-result-object v0

    return-object v0
.end method

.method private final b()Lkotlin/h/g;
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Lco/uk/getmondo/payments/send/data/a/b;->g()Lkotlin/h/g;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    const-string v0, "recipientName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lkotlin/h/j;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    const-string v0, "sortCode"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    check-cast p1, Ljava/lang/CharSequence;

    check-cast p0, Lco/uk/getmondo/payments/send/data/a/b$a;

    invoke-direct {p0}, Lco/uk/getmondo/payments/send/data/a/b$a;->a()Lkotlin/h/g;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/h/g;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public final c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    const-string v0, "accountNumber"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    check-cast p1, Ljava/lang/CharSequence;

    check-cast p0, Lco/uk/getmondo/payments/send/data/a/b$a;

    invoke-direct {p0}, Lco/uk/getmondo/payments/send/data/a/b$a;->b()Lkotlin/h/g;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlin/h/g;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method
