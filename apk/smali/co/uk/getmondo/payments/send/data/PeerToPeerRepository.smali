.class public final Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;
.super Ljava/lang/Object;
.source "PeerToPeerRepository.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/payments/send/data/PeerToPeerRepository$UserHasP2pDisabledException;,
        Lco/uk/getmondo/payments/send/data/PeerToPeerRepository$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0002\u0011\u0012B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00082\u0006\u0010\n\u001a\u00020\u000bJ\u0014\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00082\u0006\u0010\r\u001a\u00020\u0006J.\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00082\u0008\u0008\u0002\u0010\r\u001a\u00020\u00062\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;",
        "",
        "paymentsApi",
        "Lco/uk/getmondo/api/PaymentsApi;",
        "(Lco/uk/getmondo/api/PaymentsApi;)V",
        "USER_KEY",
        "",
        "userByContact",
        "Lio/reactivex/Single;",
        "Lco/uk/getmondo/model/Peer;",
        "contact",
        "Lco/uk/getmondo/payments/send/contacts/Contact;",
        "userByUsername",
        "username",
        "verifyContactIsOnMonzo",
        "userResponse",
        "Lco/uk/getmondo/model/UserResponse;",
        "UserHasP2pDisabledException",
        "UserNotOnMonzoException",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lco/uk/getmondo/api/PaymentsApi;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/api/PaymentsApi;)V
    .locals 1

    .prologue
    const-string v0, "paymentsApi"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;->b:Lco/uk/getmondo/api/PaymentsApi;

    .line 11
    const-string v0, "0"

    iput-object v0, p0, Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;->a:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic a(Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;Ljava/lang/String;Lco/uk/getmondo/payments/send/a/b;Lco/uk/getmondo/d/al;ILjava/lang/Object;)Lio/reactivex/v;
    .locals 1

    .prologue
    and-int/lit8 v0, p4, 0x1

    if-eqz v0, :cond_0

    .line 24
    const-string p1, ""

    :cond_0
    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_1

    .line 25
    const/4 v0, 0x0

    check-cast v0, Lco/uk/getmondo/payments/send/a/b;

    :goto_0
    invoke-direct {p0, p1, v0, p3}, Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;->a(Ljava/lang/String;Lco/uk/getmondo/payments/send/a/b;Lco/uk/getmondo/d/al;)Lio/reactivex/v;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v0, p2

    goto :goto_0
.end method

.method private final a(Ljava/lang/String;Lco/uk/getmondo/payments/send/a/b;Lco/uk/getmondo/d/al;)Lio/reactivex/v;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lco/uk/getmondo/payments/send/a/b;",
            "Lco/uk/getmondo/d/al;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/d/aa;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 28
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lco/uk/getmondo/payments/send/a/b;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    move-object v1, v0

    .line 29
    :goto_0
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lco/uk/getmondo/d/al;->a()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p3}, Lco/uk/getmondo/d/al;->a()Ljava/util/Map;

    move-result-object v0

    iget-object v2, p0, Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;->a:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 30
    :cond_0
    new-instance v0, Lco/uk/getmondo/payments/send/data/PeerToPeerRepository$a;

    invoke-direct {v0, v1}, Lco/uk/getmondo/payments/send/data/PeerToPeerRepository$a;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    invoke-static {v0}, Lio/reactivex/v;->a(Ljava/lang/Throwable;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "Single.error(UserNotOnMonzoException(name))"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    :goto_1
    return-object v0

    :cond_1
    move-object v1, p1

    .line 28
    goto :goto_0

    .line 32
    :cond_2
    invoke-virtual {p3}, Lco/uk/getmondo/d/al;->a()Ljava/util/Map;

    move-result-object v0

    iget-object v2, p0, Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;->a:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lco/uk/getmondo/d/al$a;

    .line 33
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lco/uk/getmondo/d/al$a;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 34
    :cond_3
    new-instance v0, Lco/uk/getmondo/payments/send/data/PeerToPeerRepository$UserHasP2pDisabledException;

    invoke-direct {v0, v1}, Lco/uk/getmondo/payments/send/data/PeerToPeerRepository$UserHasP2pDisabledException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    invoke-static {v0}, Lio/reactivex/v;->a(Ljava/lang/Throwable;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "Single.error(UserHasP2pDisabledException(name))"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 36
    :cond_4
    new-instance v0, Lco/uk/getmondo/d/aa;

    invoke-virtual {v2}, Lco/uk/getmondo/d/al$a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Lco/uk/getmondo/d/al$a;->c()Ljava/lang/String;

    move-result-object v2

    if-eqz p2, :cond_6

    invoke-virtual {p2}, Lco/uk/getmondo/payments/send/a/b;->c()Ljava/lang/String;

    move-result-object v3

    :goto_2
    if-eqz p2, :cond_7

    invoke-virtual {p2}, Lco/uk/getmondo/payments/send/a/b;->b()Ljava/lang/String;

    move-result-object v5

    :goto_3
    if-eqz p2, :cond_5

    invoke-virtual {p2}, Lco/uk/getmondo/payments/send/a/b;->e()Ljava/lang/String;

    move-result-object v6

    .line 37
    :cond_5
    if-eqz p2, :cond_8

    const/4 v7, 0x1

    :goto_4
    move-object v4, p1

    .line 36
    invoke-direct/range {v0 .. v7}, Lco/uk/getmondo/d/aa;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v0}, Lio/reactivex/v;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "Single.just(Peer(user.us\u2026        contact != null))"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    move-object v3, v6

    goto :goto_2

    :cond_7
    move-object v5, v6

    goto :goto_3

    .line 37
    :cond_8
    const/4 v7, 0x0

    goto :goto_4
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/payments/send/a/b;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/payments/send/a/b;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/d/aa;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "contact"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    iget-object v0, p0, Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;->b:Lco/uk/getmondo/api/PaymentsApi;

    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/a/b;->c()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_0
    invoke-interface {v0, v1}, Lco/uk/getmondo/api/PaymentsApi;->userByPhoneNumber(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v1

    .line 15
    new-instance v0, Lco/uk/getmondo/payments/send/data/PeerToPeerRepository$b;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/payments/send/data/PeerToPeerRepository$b;-><init>(Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;Lco/uk/getmondo/payments/send/a/b;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->a(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "paymentsApi.userByPhoneN\u2026esponse = userResponse) }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/d/aa;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "username"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    iget-object v0, p0, Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;->b:Lco/uk/getmondo/api/PaymentsApi;

    invoke-interface {v0, p1}, Lco/uk/getmondo/api/PaymentsApi;->userByUsername(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v1

    .line 20
    new-instance v0, Lco/uk/getmondo/payments/send/data/PeerToPeerRepository$c;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/payments/send/data/PeerToPeerRepository$c;-><init>(Lco/uk/getmondo/payments/send/data/PeerToPeerRepository;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->a(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    const-string v1, "paymentsApi.userByUserna\u2026esponse = userResponse) }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
