.class final Lco/uk/getmondo/payments/send/data/f$c;
.super Ljava/lang/Object;
.source "RecentPayeesStorage.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/payments/send/data/f;->a()Lio/reactivex/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u0014\u0010\u0004\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00050\u00050\u0001H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lco/uk/getmondo/payments/send/data/model/Payee;",
        "kotlin.jvm.PlatformType",
        "transactions",
        "Lco/uk/getmondo/model/Transaction;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/payments/send/data/f$c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lco/uk/getmondo/payments/send/data/f$c;

    invoke-direct {v0}, Lco/uk/getmondo/payments/send/data/f$c;-><init>()V

    sput-object v0, Lco/uk/getmondo/payments/send/data/f$c;->a:Lco/uk/getmondo/payments/send/data/f$c;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/send/data/f$c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lco/uk/getmondo/d/aj;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/payments/send/data/a/e;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "transactions"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    check-cast p1, Ljava/lang/Iterable;

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 54
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 55
    check-cast v1, Lco/uk/getmondo/d/aj;

    .line 35
    invoke-virtual {v1}, Lco/uk/getmondo/d/aj;->C()Lco/uk/getmondo/payments/send/data/a/a;

    move-result-object v3

    .line 36
    if-eqz v3, :cond_0

    .line 37
    new-instance v1, Lco/uk/getmondo/payments/send/data/a/b;

    invoke-virtual {v3}, Lco/uk/getmondo/payments/send/data/a/a;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Lco/uk/getmondo/payments/send/data/a/a;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lco/uk/getmondo/payments/send/data/a/a;->c()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v4, v5, v3}, Lco/uk/getmondo/payments/send/data/a/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v1, Lco/uk/getmondo/payments/send/data/a/e;

    .line 36
    :goto_1
    nop

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 39
    :cond_0
    invoke-virtual {v1}, Lco/uk/getmondo/d/aj;->B()Lco/uk/getmondo/d/aa;

    move-result-object v1

    check-cast v1, Lco/uk/getmondo/payments/send/data/a/e;

    goto :goto_1

    .line 56
    :cond_1
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 42
    nop

    .line 57
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 58
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 59
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    .line 60
    check-cast v0, Lco/uk/getmondo/payments/send/data/a/e;

    .line 44
    instance-of v2, v0, Lco/uk/getmondo/payments/send/data/a/b;

    if-eqz v2, :cond_3

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object v2, v0

    check-cast v2, Lco/uk/getmondo/payments/send/data/a/b;

    invoke-virtual {v2}, Lco/uk/getmondo/payments/send/data/a/b;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    check-cast v0, Lco/uk/getmondo/payments/send/data/a/b;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/data/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 61
    :goto_3
    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 62
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 45
    :cond_3
    instance-of v2, v0, Lco/uk/getmondo/d/aa;

    if-eqz v2, :cond_4

    check-cast v0, Lco/uk/getmondo/d/aa;

    invoke-virtual {v0}, Lco/uk/getmondo/d/aa;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 46
    :cond_4
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected payee type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_5
    move-object v0, v3

    .line 64
    check-cast v0, Ljava/util/List;

    .line 48
    return-object v0
.end method
