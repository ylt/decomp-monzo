.class public final Lco/uk/getmondo/payments/send/data/f;
.super Ljava/lang/Object;
.source "RecentPayeesStorage.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00060\u00050\u0004\u00a8\u0006\u0007"
    }
    d2 = {
        "Lco/uk/getmondo/payments/send/data/RecentPayeesStorage;",
        "",
        "()V",
        "recentPayees",
        "Lio/reactivex/Observable;",
        "",
        "Lco/uk/getmondo/payments/send/data/model/Payee;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/payments/send/data/a/e;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 17
    sget-object v0, Lco/uk/getmondo/payments/send/data/f$a;->a:Lco/uk/getmondo/payments/send/data/f$a;

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {v0}, Lco/uk/getmondo/common/j/g;->a(Lkotlin/d/a/b;)Lio/reactivex/n;

    move-result-object v1

    .line 32
    sget-object v0, Lco/uk/getmondo/payments/send/data/f$b;->a:Lco/uk/getmondo/payments/send/data/f$b;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    .line 33
    sget-object v0, Lco/uk/getmondo/payments/send/data/f$c;->a:Lco/uk/getmondo/payments/send/data/f$c;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxRealm.asObservable { r\u2026      }\n                }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
