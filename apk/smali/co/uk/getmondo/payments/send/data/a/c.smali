.class public final Lco/uk/getmondo/payments/send/data/a/c;
.super Ljava/lang/Object;
.source "BankPayment.kt"

# interfaces
.implements Lco/uk/getmondo/payments/send/data/a/f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/payments/send/data/a/c$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0008\u0086\u0008\u0018\u0000 \'2\u00020\u0001:\u0001\'B\u000f\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B+\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u000c\u00a2\u0006\u0002\u0010\rJ\t\u0010\u0016\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\nH\u00c6\u0003J\u000b\u0010\u0019\u001a\u0004\u0018\u00010\u000cH\u00c6\u0003J3\u0010\u001a\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u00c6\u0001J\u0008\u0010\u001b\u001a\u00020\u001cH\u0016J\u0013\u0010\u001d\u001a\u00020\u001e2\u0008\u0010\u001f\u001a\u0004\u0018\u00010 H\u00d6\u0003J\t\u0010!\u001a\u00020\u001cH\u00d6\u0001J\t\u0010\"\u001a\u00020\u0008H\u00d6\u0001J\u001a\u0010#\u001a\u00020$2\u0008\u0010%\u001a\u0004\u0018\u00010\u00032\u0006\u0010&\u001a\u00020\u001cH\u0016R\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0014\u0010\t\u001a\u00020\nX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015\u00a8\u0006("
    }
    d2 = {
        "Lco/uk/getmondo/payments/send/data/model/BankPayment;",
        "Lco/uk/getmondo/payments/send/data/model/Payment;",
        "source",
        "Landroid/os/Parcel;",
        "(Landroid/os/Parcel;)V",
        "amount",
        "Lco/uk/getmondo/model/Amount;",
        "reference",
        "",
        "payee",
        "Lco/uk/getmondo/payments/send/data/model/BankPayee;",
        "schedule",
        "Lco/uk/getmondo/payments/data/model/PaymentSchedule;",
        "(Lco/uk/getmondo/model/Amount;Ljava/lang/String;Lco/uk/getmondo/payments/send/data/model/BankPayee;Lco/uk/getmondo/payments/data/model/PaymentSchedule;)V",
        "getAmount",
        "()Lco/uk/getmondo/model/Amount;",
        "getPayee",
        "()Lco/uk/getmondo/payments/send/data/model/BankPayee;",
        "getReference",
        "()Ljava/lang/String;",
        "getSchedule",
        "()Lco/uk/getmondo/payments/data/model/PaymentSchedule;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "writeToParcel",
        "",
        "dest",
        "flags",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lco/uk/getmondo/payments/send/data/a/c;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lco/uk/getmondo/payments/send/data/a/c$a;


# instance fields
.field private final b:Lco/uk/getmondo/d/c;

.field private final c:Ljava/lang/String;

.field private final d:Lco/uk/getmondo/payments/send/data/a/b;

.field private final e:Lco/uk/getmondo/payments/a/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    new-instance v0, Lco/uk/getmondo/payments/send/data/a/c$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/payments/send/data/a/c$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/payments/send/data/a/c;->a:Lco/uk/getmondo/payments/send/data/a/c$a;

    .line 16
    new-instance v0, Lco/uk/getmondo/payments/send/data/a/c$b;

    invoke-direct {v0}, Lco/uk/getmondo/payments/send/data/a/c$b;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lco/uk/getmondo/payments/send/data/a/c;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    const-class v0, Lco/uk/getmondo/d/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    const-string v1, "source.readParcelable<Am\u2026::class.java.classLoader)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lco/uk/getmondo/d/c;

    .line 24
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    const-string v1, "source.readString()"

    invoke-static {v3, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    const-class v1, Lco/uk/getmondo/payments/send/data/a/b;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    const-string v2, "source.readParcelable<Ba\u2026::class.java.classLoader)"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lco/uk/getmondo/payments/send/data/a/b;

    .line 26
    const-class v2, Lco/uk/getmondo/payments/a/a/d;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lco/uk/getmondo/payments/a/a/d;

    .line 22
    invoke-direct {p0, v0, v3, v1, v2}, Lco/uk/getmondo/payments/send/data/a/c;-><init>(Lco/uk/getmondo/d/c;Ljava/lang/String;Lco/uk/getmondo/payments/send/data/a/b;Lco/uk/getmondo/payments/a/a/d;)V

    return-void
.end method

.method public constructor <init>(Lco/uk/getmondo/d/c;Ljava/lang/String;Lco/uk/getmondo/payments/send/data/a/b;Lco/uk/getmondo/payments/a/a/d;)V
    .locals 1

    .prologue
    const-string v0, "amount"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reference"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "payee"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/payments/send/data/a/c;->b:Lco/uk/getmondo/d/c;

    iput-object p2, p0, Lco/uk/getmondo/payments/send/data/a/c;->c:Ljava/lang/String;

    iput-object p3, p0, Lco/uk/getmondo/payments/send/data/a/c;->d:Lco/uk/getmondo/payments/send/data/a/b;

    iput-object p4, p0, Lco/uk/getmondo/payments/send/data/a/c;->e:Lco/uk/getmondo/payments/a/a/d;

    return-void
.end method

.method public synthetic constructor <init>(Lco/uk/getmondo/d/c;Ljava/lang/String;Lco/uk/getmondo/payments/send/data/a/b;Lco/uk/getmondo/payments/a/a/d;ILkotlin/d/b/i;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p5, 0x8

    if-eqz v0, :cond_0

    .line 13
    const/4 v0, 0x0

    check-cast v0, Lco/uk/getmondo/payments/a/a/d;

    :goto_0
    invoke-direct {p0, p1, p2, p3, v0}, Lco/uk/getmondo/payments/send/data/a/c;-><init>(Lco/uk/getmondo/d/c;Ljava/lang/String;Lco/uk/getmondo/payments/send/data/a/b;Lco/uk/getmondo/payments/a/a/d;)V

    return-void

    :cond_0
    move-object v0, p4

    goto :goto_0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/d/c;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lco/uk/getmondo/payments/send/data/a/c;->b:Lco/uk/getmondo/d/c;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lco/uk/getmondo/payments/send/data/a/c;->c:Ljava/lang/String;

    return-object v0
.end method

.method public c()Lco/uk/getmondo/payments/send/data/a/b;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lco/uk/getmondo/payments/send/data/a/c;->d:Lco/uk/getmondo/payments/send/data/a/b;

    return-object v0
.end method

.method public synthetic d()Lco/uk/getmondo/payments/send/data/a/e;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/data/a/c;->c()Lco/uk/getmondo/payments/send/data/a/b;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/send/data/a/e;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lco/uk/getmondo/payments/a/a/d;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lco/uk/getmondo/payments/send/data/a/c;->e:Lco/uk/getmondo/payments/a/a/d;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lco/uk/getmondo/payments/send/data/a/c;

    if-eqz v0, :cond_1

    check-cast p1, Lco/uk/getmondo/payments/send/data/a/c;

    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/data/a/c;->a()Lco/uk/getmondo/d/c;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/data/a/c;->a()Lco/uk/getmondo/d/c;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/payments/send/data/a/c;->c:Ljava/lang/String;

    iget-object v1, p1, Lco/uk/getmondo/payments/send/data/a/c;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/data/a/c;->c()Lco/uk/getmondo/payments/send/data/a/b;

    move-result-object v0

    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/data/a/c;->c()Lco/uk/getmondo/payments/send/data/a/b;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/payments/send/data/a/c;->e:Lco/uk/getmondo/payments/a/a/d;

    iget-object v1, p1, Lco/uk/getmondo/payments/send/data/a/c;->e:Lco/uk/getmondo/payments/a/a/d;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/data/a/c;->a()Lco/uk/getmondo/d/c;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/payments/send/data/a/c;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/data/a/c;->c()Lco/uk/getmondo/payments/send/data/a/b;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/uk/getmondo/payments/send/data/a/c;->e:Lco/uk/getmondo/payments/a/a/d;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BankPayment(amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/data/a/c;->a()Lco/uk/getmondo/d/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", reference="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/payments/send/data/a/c;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", payee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/data/a/c;->c()Lco/uk/getmondo/payments/send/data/a/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", schedule="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/payments/send/data/a/c;->e:Lco/uk/getmondo/payments/a/a/d;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 32
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/data/a/c;->a()Lco/uk/getmondo/d/c;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 33
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/payments/send/data/a/c;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 34
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/data/a/c;->c()Lco/uk/getmondo/payments/send/data/a/b;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 35
    :cond_2
    if-eqz p1, :cond_3

    iget-object v0, p0, Lco/uk/getmondo/payments/send/data/a/c;->e:Lco/uk/getmondo/payments/a/a/d;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 36
    :cond_3
    return-void
.end method
