.class public Lco/uk/getmondo/payments/send/data/h;
.super Ljava/lang/Object;
.source "UserSettingsRepository.java"


# instance fields
.field private final a:Lco/uk/getmondo/api/MonzoApi;

.field private final b:Lco/uk/getmondo/payments/send/data/p;

.field private final c:Lco/uk/getmondo/common/o;

.field private final d:Lco/uk/getmondo/common/x;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/payments/send/data/p;Lco/uk/getmondo/common/o;Lco/uk/getmondo/common/x;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lco/uk/getmondo/payments/send/data/h;->a:Lco/uk/getmondo/api/MonzoApi;

    .line 32
    iput-object p2, p0, Lco/uk/getmondo/payments/send/data/h;->b:Lco/uk/getmondo/payments/send/data/p;

    .line 33
    iput-object p3, p0, Lco/uk/getmondo/payments/send/data/h;->c:Lco/uk/getmondo/common/o;

    .line 34
    iput-object p4, p0, Lco/uk/getmondo/payments/send/data/h;->d:Lco/uk/getmondo/common/x;

    .line 35
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/data/h;Lco/uk/getmondo/api/model/ApiUserSettings;)Lio/reactivex/d;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 39
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/data/h;->a()Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lio/realm/av;)Lio/realm/bg;
    .locals 1

    .prologue
    .line 62
    const-class v0, Lco/uk/getmondo/d/am;

    invoke-virtual {p0, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Lio/realm/bf;->g()Lio/realm/bg;

    move-result-object v0

    .line 62
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/data/h;)Ljava/lang/Boolean;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lco/uk/getmondo/payments/send/data/h;->b:Lco/uk/getmondo/payments/send/data/p;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/data/p;->a()Lco/uk/getmondo/payments/send/data/a/d;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/payments/send/data/a/d;->a:Lco/uk/getmondo/payments/send/data/a/d;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/data/h;Lco/uk/getmondo/api/model/ApiConfig;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 46
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiConfig;->a()Lco/uk/getmondo/api/model/ApiConfig$P2p;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/ApiConfig$P2p;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/payments/send/data/a/d;->a(Ljava/lang/String;)Lco/uk/getmondo/payments/send/data/a/d;

    move-result-object v1

    .line 47
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiConfig;->b()Lco/uk/getmondo/api/model/ApiConfig$MonzoMeUsername;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    .line 48
    :goto_0
    new-instance v2, Lco/uk/getmondo/d/a/h;

    invoke-direct {v2}, Lco/uk/getmondo/d/a/h;-><init>()V

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiConfig;->d()Lco/uk/getmondo/api/model/ApiConfig$InboundP2p;

    move-result-object v3

    invoke-virtual {v2, v3}, Lco/uk/getmondo/d/a/h;->a(Lco/uk/getmondo/api/model/ApiConfig$InboundP2p;)Lco/uk/getmondo/d/p;

    move-result-object v2

    .line 50
    iget-object v3, p0, Lco/uk/getmondo/payments/send/data/h;->d:Lco/uk/getmondo/common/x;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiConfig;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lco/uk/getmondo/common/x;->a(Ljava/lang/String;)V

    .line 51
    iget-object v3, p0, Lco/uk/getmondo/payments/send/data/h;->b:Lco/uk/getmondo/payments/send/data/p;

    new-instance v4, Lco/uk/getmondo/d/am;

    invoke-virtual {v1}, Lco/uk/getmondo/payments/send/data/a/d;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiConfig;->f()Z

    move-result v5

    invoke-direct {v4, v1, v0, v2, v5}, Lco/uk/getmondo/d/am;-><init>(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/d/p;Z)V

    invoke-virtual {v3, v4}, Lco/uk/getmondo/payments/send/data/p;->a(Lco/uk/getmondo/d/am;)V

    .line 52
    iget-object v0, p0, Lco/uk/getmondo/payments/send/data/h;->c:Lco/uk/getmondo/common/o;

    new-instance v1, Lco/uk/getmondo/d/l;

    .line 53
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiConfig;->c()Lco/uk/getmondo/api/model/ApiConfig$FeatureFlags;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/api/model/ApiConfig$FeatureFlags;->a()Z

    move-result v2

    .line 54
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiConfig;->c()Lco/uk/getmondo/api/model/ApiConfig$FeatureFlags;

    move-result-object v3

    invoke-virtual {v3}, Lco/uk/getmondo/api/model/ApiConfig$FeatureFlags;->b()Z

    move-result v3

    invoke-direct {v1, v2, v3}, Lco/uk/getmondo/d/l;-><init>(ZZ)V

    .line 52
    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/o;->a(Lco/uk/getmondo/d/l;)V

    .line 56
    return-void

    .line 47
    :cond_0
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/ApiConfig;->b()Lco/uk/getmondo/api/model/ApiConfig$MonzoMeUsername;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/api/model/ApiConfig$MonzoMeUsername;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a()Lio/reactivex/b;
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lco/uk/getmondo/payments/send/data/h;->a:Lco/uk/getmondo/api/MonzoApi;

    invoke-interface {v0}, Lco/uk/getmondo/api/MonzoApi;->config()Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/payments/send/data/k;->a(Lco/uk/getmondo/payments/send/data/h;)Lio/reactivex/c/g;

    move-result-object v1

    .line 45
    invoke-virtual {v0, v1}, Lio/reactivex/v;->c(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    .line 57
    invoke-virtual {v0}, Lio/reactivex/v;->c()Lio/reactivex/b;

    move-result-object v0

    .line 44
    return-object v0
.end method

.method public a(Z)Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/v",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lco/uk/getmondo/payments/send/data/h;->a:Lco/uk/getmondo/api/MonzoApi;

    invoke-interface {v0, p1}, Lco/uk/getmondo/api/MonzoApi;->optInToPeerToPeer(Z)Lio/reactivex/v;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/payments/send/data/i;->a(Lco/uk/getmondo/payments/send/data/h;)Lio/reactivex/c/h;

    move-result-object v1

    .line 39
    invoke-virtual {v0, v1}, Lio/reactivex/v;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/payments/send/data/j;->a(Lco/uk/getmondo/payments/send/data/h;)Ljava/util/concurrent/Callable;

    move-result-object v1

    .line 40
    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Ljava/util/concurrent/Callable;)Lio/reactivex/v;

    move-result-object v0

    .line 38
    return-object v0
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/d/am;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    invoke-static {}, Lco/uk/getmondo/payments/send/data/l;->a()Lkotlin/d/a/b;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/j/g;->a(Lkotlin/d/a/b;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/payments/send/data/m;->a()Lio/reactivex/c/q;

    move-result-object v1

    .line 64
    invoke-virtual {v0, v1}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/payments/send/data/n;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 65
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 62
    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 3

    .prologue
    .line 69
    invoke-static {}, Lio/realm/av;->n()Lio/realm/av;

    move-result-object v2

    .line 70
    const-class v0, Lco/uk/getmondo/d/am;

    invoke-virtual {v2, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/bf;->h()Lio/realm/bb;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/d/am;

    .line 71
    const-string v1, ""

    .line 72
    if-eqz v0, :cond_0

    .line 73
    invoke-virtual {v0}, Lco/uk/getmondo/d/am;->b()Ljava/lang/String;

    move-result-object v0

    .line 75
    :goto_0
    invoke-virtual {v2}, Lio/realm/av;->close()V

    .line 76
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lco/uk/getmondo/payments/send/data/h;->b:Lco/uk/getmondo/payments/send/data/p;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/data/p;->c()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public e()Lco/uk/getmondo/d/p;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lco/uk/getmondo/payments/send/data/h;->b:Lco/uk/getmondo/payments/send/data/p;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/data/p;->b()Lco/uk/getmondo/d/p;

    move-result-object v0

    return-object v0
.end method
