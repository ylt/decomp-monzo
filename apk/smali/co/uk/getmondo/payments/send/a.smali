.class public final Lco/uk/getmondo/payments/send/a;
.super Ljava/lang/Object;
.source "PaymentScheduleFormatter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u00020\u0001B\u0011\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008J,\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000b2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\r2\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u000f2\u0008\u0008\u0002\u0010\u0010\u001a\u00020\rJ\u000e\u0010\u0011\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lco/uk/getmondo/payments/send/PaymentScheduleFormatter;",
        "",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "format",
        "",
        "schedule",
        "Lco/uk/getmondo/payments/data/model/PaymentSchedule;",
        "formatDate",
        "localDate",
        "Lorg/threeten/bp/LocalDate;",
        "monthBeforeDay",
        "",
        "monthTextStyle",
        "Lorg/threeten/bp/format/TextStyle;",
        "showYearIfDifferent",
        "formatShort",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/payments/send/a;->a:Landroid/content/Context;

    return-void
.end method

.method public static bridge synthetic a(Lco/uk/getmondo/payments/send/a;Lorg/threeten/bp/LocalDate;ZLorg/threeten/bp/format/TextStyle;ZILjava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    and-int/lit8 v1, p5, 0x2

    if-eqz v1, :cond_0

    move p2, v0

    .line 62
    :cond_0
    and-int/lit8 v1, p5, 0x4

    if-eqz v1, :cond_1

    .line 63
    sget-object p3, Lorg/threeten/bp/format/TextStyle;->c:Lorg/threeten/bp/format/TextStyle;

    :cond_1
    and-int/lit8 v1, p5, 0x8

    if-eqz v1, :cond_2

    move p4, v0

    .line 64
    :cond_2
    invoke-virtual {p0, p1, p2, p3, p4}, Lco/uk/getmondo/payments/send/a;->a(Lorg/threeten/bp/LocalDate;ZLorg/threeten/bp/format/TextStyle;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/payments/a/a/d;)Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 22
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lco/uk/getmondo/payments/a/a/d;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 23
    invoke-virtual {p1}, Lco/uk/getmondo/payments/a/a/d;->b()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    const/4 v5, 0x6

    move-object v0, p0

    move-object v6, v3

    invoke-static/range {v0 .. v6}, Lco/uk/getmondo/payments/send/a;->a(Lco/uk/getmondo/payments/send/a;Lorg/threeten/bp/LocalDate;ZLorg/threeten/bp/format/TextStyle;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 24
    iget-object v1, p0, Lco/uk/getmondo/payments/send/a;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lco/uk/getmondo/payments/a/a/d;->c()Lco/uk/getmondo/payments/a/a/d$c;

    move-result-object v5

    invoke-virtual {v5}, Lco/uk/getmondo/payments/a/a/d$c;->b()I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 25
    invoke-virtual {p1}, Lco/uk/getmondo/payments/a/a/d;->c()Lco/uk/getmondo/payments/a/a/d$c;

    move-result-object v5

    sget-object v6, Lco/uk/getmondo/payments/a/a/d$c;->a:Lco/uk/getmondo/payments/a/a/d$c;

    if-ne v5, v6, :cond_0

    .line 33
    :goto_0
    return-object v0

    .line 27
    :cond_0
    invoke-virtual {p1}, Lco/uk/getmondo/payments/a/a/d;->d()Lorg/threeten/bp/LocalDate;

    move-result-object v5

    if-nez v5, :cond_1

    .line 28
    iget-object v3, p0, Lco/uk/getmondo/payments/send/a;->a:Landroid/content/Context;

    const v5, 0x7f0a02eb

    new-array v6, v10, [Ljava/lang/Object;

    aput-object v1, v6, v2

    aput-object v0, v6, v4

    invoke-virtual {v3, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(R.stri\u2026Name, startDateFormatted)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 29
    :cond_1
    iget-object v7, p0, Lco/uk/getmondo/payments/send/a;->a:Landroid/content/Context;

    const v8, 0x7f0a02e9

    const/4 v5, 0x3

    new-array v9, v5, [Ljava/lang/Object;

    aput-object v1, v9, v2

    aput-object v0, v9, v4

    .line 31
    invoke-virtual {p1}, Lco/uk/getmondo/payments/a/a/d;->d()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    const/16 v5, 0xe

    move-object v0, p0

    move v4, v2

    move-object v6, v3

    invoke-static/range {v0 .. v6}, Lco/uk/getmondo/payments/send/a;->a(Lco/uk/getmondo/payments/send/a;Lorg/threeten/bp/LocalDate;ZLorg/threeten/bp/format/TextStyle;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v9, v10

    .line 30
    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(R.stri\u2026atDate(schedule.endDate))"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 33
    :cond_2
    const-string v0, ""

    goto :goto_0
.end method

.method public final a(Lorg/threeten/bp/LocalDate;ZLorg/threeten/bp/format/TextStyle;Z)Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0x20

    const-string v0, "localDate"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "monthTextStyle"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-virtual {p1}, Lorg/threeten/bp/LocalDate;->f()Lorg/threeten/bp/Month;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, p3, v1}, Lorg/threeten/bp/Month;->a(Lorg/threeten/bp/format/TextStyle;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 67
    if-eqz p2, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/common/c/c;->a(Lorg/threeten/bp/LocalDate;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 73
    :goto_0
    if-eqz p4, :cond_0

    invoke-virtual {p1}, Lorg/threeten/bp/LocalDate;->d()I

    move-result v1

    invoke-static {}, Lorg/threeten/bp/Year;->a()Lorg/threeten/bp/Year;

    move-result-object v2

    invoke-virtual {v2}, Lorg/threeten/bp/Year;->b()I

    move-result v2

    if-le v1, v2, :cond_0

    .line 74
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lorg/threeten/bp/LocalDate;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 76
    :cond_0
    return-object v0

    .line 69
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lco/uk/getmondo/common/c/c;->a(Lorg/threeten/bp/LocalDate;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Lco/uk/getmondo/payments/a/a/d;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const-string v0, "schedule"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-virtual {p1}, Lco/uk/getmondo/payments/a/a/d;->c()Lco/uk/getmondo/payments/a/a/d$c;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/payments/send/b;->a:[I

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/d$c;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 55
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 39
    :pswitch_0
    invoke-virtual {p1}, Lco/uk/getmondo/payments/a/a/d;->b()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    .line 41
    sget-object v1, Lorg/threeten/bp/format/TextStyle;->a:Lorg/threeten/bp/format/TextStyle;

    .line 39
    invoke-virtual {p0, v0, v2, v1, v2}, Lco/uk/getmondo/payments/send/a;->a(Lorg/threeten/bp/LocalDate;ZLorg/threeten/bp/format/TextStyle;Z)Ljava/lang/String;

    move-result-object v0

    .line 43
    iget-object v1, p0, Lco/uk/getmondo/payments/send/a;->a:Landroid/content/Context;

    const v3, 0x7f0a02ec

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v4

    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(R.stri\u2026edule_pattern_once, date)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    :goto_0
    return-object v0

    .line 45
    :pswitch_1
    iget-object v0, p0, Lco/uk/getmondo/payments/send/a;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lco/uk/getmondo/payments/a/a/d;->c()Lco/uk/getmondo/payments/a/a/d$c;

    move-result-object v1

    invoke-virtual {v1}, Lco/uk/getmondo/payments/a/a/d$c;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "(this as java.lang.String).toLowerCase()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :pswitch_2
    invoke-virtual {p1}, Lco/uk/getmondo/payments/a/a/d;->b()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->i()Lorg/threeten/bp/DayOfWeek;

    move-result-object v0

    sget-object v1, Lorg/threeten/bp/format/TextStyle;->a:Lorg/threeten/bp/format/TextStyle;

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1, v3}, Lorg/threeten/bp/DayOfWeek;->a(Lorg/threeten/bp/format/TextStyle;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 48
    iget-object v1, p0, Lco/uk/getmondo/payments/send/a;->a:Landroid/content/Context;

    const v3, 0x7f0a02ed

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v4

    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(R.stri\u2026attern_weekly, dayOfWeek)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 51
    :pswitch_3
    iget-object v0, p0, Lco/uk/getmondo/payments/send/a;->a:Landroid/content/Context;

    const v1, 0x7f0a02ea

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Lco/uk/getmondo/payments/a/a/d;->b()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    invoke-static {v3}, Lco/uk/getmondo/common/c/c;->a(Lorg/threeten/bp/LocalDate;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(R.stri\u2026ate.dayOfMonthWithSuffix)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 54
    :pswitch_4
    invoke-virtual {p1}, Lco/uk/getmondo/payments/a/a/d;->b()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    sget-object v3, Lorg/threeten/bp/format/TextStyle;->a:Lorg/threeten/bp/format/TextStyle;

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lco/uk/getmondo/payments/send/a;->a(Lco/uk/getmondo/payments/send/a;Lorg/threeten/bp/LocalDate;ZLorg/threeten/bp/format/TextStyle;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 55
    iget-object v1, p0, Lco/uk/getmondo/payments/send/a;->a:Landroid/content/Context;

    const v3, 0x7f0a02ee

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v4

    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(R.stri\u2026ule_pattern_yearly, date)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 37
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
