.class final Lco/uk/getmondo/payments/send/o$p$1;
.super Ljava/lang/Object;
.source "SendMoneyPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/payments/send/o$p;->a(Ljava/util/Map;)Lio/reactivex/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lkotlin/Pair;",
        "Lco/uk/getmondo/payments/send/contacts/Contact;",
        "",
        "contact",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/payments/send/o$p;


# direct methods
.method constructor <init>(Lco/uk/getmondo/payments/send/o$p;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/send/o$p$1;->a:Lco/uk/getmondo/payments/send/o$p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 35
    check-cast p1, Lco/uk/getmondo/payments/send/a/b;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/send/o$p$1;->a(Lco/uk/getmondo/payments/send/a/b;)Lkotlin/h;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/payments/send/a/b;)Lkotlin/h;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/payments/send/a/b;",
            ")",
            "Lkotlin/h",
            "<",
            "Lco/uk/getmondo/payments/send/a/b;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "contact"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    new-instance v0, Lkotlin/h;

    iget-object v1, p0, Lco/uk/getmondo/payments/send/o$p$1;->a:Lco/uk/getmondo/payments/send/o$p;

    iget-object v1, v1, Lco/uk/getmondo/payments/send/o$p;->a:Lco/uk/getmondo/payments/send/o;

    iget-object v2, p0, Lco/uk/getmondo/payments/send/o$p$1;->a:Lco/uk/getmondo/payments/send/o$p;

    iget-object v2, v2, Lco/uk/getmondo/payments/send/o$p;->a:Lco/uk/getmondo/payments/send/o;

    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/a/b;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lco/uk/getmondo/payments/send/o;->a(Lco/uk/getmondo/payments/send/o;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lco/uk/getmondo/payments/send/o;->a(Lco/uk/getmondo/payments/send/o;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lkotlin/h;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method
