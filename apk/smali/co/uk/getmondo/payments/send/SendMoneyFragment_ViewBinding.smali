.class public Lco/uk/getmondo/payments/send/SendMoneyFragment_ViewBinding;
.super Ljava/lang/Object;
.source "SendMoneyFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/payments/send/SendMoneyFragment;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/payments/send/SendMoneyFragment;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment_ViewBinding;->a:Lco/uk/getmondo/payments/send/SendMoneyFragment;

    .line 23
    const v0, 0x7f110333

    const-string v1, "field \'contactsRecyclerView\'"

    const-class v2, Landroid/support/v7/widget/RecyclerView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/SendMoneyFragment;->contactsRecyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 24
    const v0, 0x7f110331

    const-string v1, "field \'contactsProgressBar\'"

    const-class v2, Landroid/widget/ProgressBar;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/SendMoneyFragment;->contactsProgressBar:Landroid/widget/ProgressBar;

    .line 25
    const v0, 0x7f110332

    const-string v1, "field \'swipeRefreshLayout\'"

    const-class v2, Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    iput-object v0, p1, Lco/uk/getmondo/payments/send/SendMoneyFragment;->swipeRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 26
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 31
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment_ViewBinding;->a:Lco/uk/getmondo/payments/send/SendMoneyFragment;

    .line 32
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 33
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment_ViewBinding;->a:Lco/uk/getmondo/payments/send/SendMoneyFragment;

    .line 35
    iput-object v1, v0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->contactsRecyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 36
    iput-object v1, v0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->contactsProgressBar:Landroid/widget/ProgressBar;

    .line 37
    iput-object v1, v0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->swipeRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 38
    return-void
.end method
