.class Lco/uk/getmondo/payments/send/SendMoneyAdapter$d;
.super Landroid/support/v7/f/b$a;
.source "SendMoneyAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/payments/send/SendMoneyAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "d"
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 369
    invoke-direct {p0}, Landroid/support/v7/f/b$a;-><init>()V

    .line 370
    iput-object p1, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$d;->a:Ljava/util/List;

    .line 371
    iput-object p2, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$d;->b:Ljava/util/List;

    .line 372
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/List;Ljava/util/List;Lco/uk/getmondo/payments/send/SendMoneyAdapter$1;)V
    .locals 0

    .prologue
    .line 365
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/payments/send/SendMoneyAdapter$d;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$d;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public a(II)Z
    .locals 4

    .prologue
    .line 386
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$d;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 387
    iget-object v1, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$d;->b:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 389
    instance-of v2, v0, Lco/uk/getmondo/payments/send/a/a;

    if-eqz v2, :cond_1

    instance-of v2, v1, Lco/uk/getmondo/payments/send/a/a;

    if-eqz v2, :cond_1

    .line 390
    check-cast v0, Lco/uk/getmondo/payments/send/a/a;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/a/a;->a()J

    move-result-wide v2

    move-object v0, v1

    check-cast v0, Lco/uk/getmondo/payments/send/a/a;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/a/a;->a()J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 393
    :goto_0
    return v0

    .line 390
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 393
    :cond_1
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$d;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public b(II)Z
    .locals 2

    .prologue
    .line 398
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$d;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/payments/send/SendMoneyAdapter$d;->b:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
