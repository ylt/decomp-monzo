.class final Lco/uk/getmondo/payments/send/o$q$3;
.super Ljava/lang/Object;
.source "SendMoneyPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/payments/send/o$q;->a(Ljava/util/List;)Lio/reactivex/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u001a\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u0002\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00040\u00020\u00012\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lkotlin/Pair;",
        "",
        "Lco/uk/getmondo/payments/send/contacts/Contact;",
        "Lco/uk/getmondo/payments/send/contacts/ContactWithMultipleNumbers;",
        "contacts",
        "Lco/uk/getmondo/api/model/ApiContactDiscovery;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/payments/send/o$q;

.field final synthetic b:Ljava/util/List;


# direct methods
.method constructor <init>(Lco/uk/getmondo/payments/send/o$q;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/send/o$q$3;->a:Lco/uk/getmondo/payments/send/o$q;

    iput-object p2, p0, Lco/uk/getmondo/payments/send/o$q$3;->b:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 35
    check-cast p1, Lco/uk/getmondo/api/model/ApiContactDiscovery;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/send/o$q$3;->a(Lco/uk/getmondo/api/model/ApiContactDiscovery;)Lkotlin/h;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/api/model/ApiContactDiscovery;)Lkotlin/h;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/model/ApiContactDiscovery;",
            ")",
            "Lkotlin/h",
            "<",
            "Ljava/util/Collection",
            "<",
            "Lco/uk/getmondo/payments/send/a/b;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lco/uk/getmondo/payments/send/a/d;",
            ">;>;"
        }
    .end annotation

    .prologue
    const-string v0, "contacts"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lco/uk/getmondo/payments/send/o$q$3;->a:Lco/uk/getmondo/payments/send/o$q;

    iget-object v0, v0, Lco/uk/getmondo/payments/send/o$q;->a:Lco/uk/getmondo/payments/send/o;

    iget-object v1, p0, Lco/uk/getmondo/payments/send/o$q$3;->b:Ljava/util/List;

    const-string v2, "pairs"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1, p1}, Lco/uk/getmondo/payments/send/o;->a(Lco/uk/getmondo/payments/send/o;Ljava/util/List;Lco/uk/getmondo/api/model/ApiContactDiscovery;)Lkotlin/h;

    move-result-object v0

    return-object v0
.end method
