.class public Lco/uk/getmondo/payments/send/a/e;
.super Ljava/lang/Object;
.source "RxContacts.java"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lio/reactivex/u;

.field private final c:Lio/michaelrocks/libphonenumber/android/h;

.field private final d:Landroid/content/ContentResolver;

.field private final e:Lio/reactivex/i/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/i/c",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/n",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lco/uk/getmondo/payments/send/a/b;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lio/reactivex/u;Lio/reactivex/u;Lio/michaelrocks/libphonenumber/android/h;)V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/a/e;->d:Landroid/content/ContentResolver;

    .line 52
    iput-object p1, p0, Lco/uk/getmondo/payments/send/a/e;->a:Landroid/content/Context;

    .line 53
    iput-object p2, p0, Lco/uk/getmondo/payments/send/a/e;->b:Lio/reactivex/u;

    .line 54
    iput-object p4, p0, Lco/uk/getmondo/payments/send/a/e;->c:Lio/michaelrocks/libphonenumber/android/h;

    .line 55
    invoke-static {}, Lio/reactivex/i/a;->a()Lio/reactivex/i/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/a/e;->e:Lio/reactivex/i/c;

    .line 56
    iget-object v0, p0, Lco/uk/getmondo/payments/send/a/e;->e:Lio/reactivex/i/c;

    invoke-direct {p0}, Lco/uk/getmondo/payments/send/a/e;->d()Lio/reactivex/n;

    move-result-object v1

    invoke-static {v0, v1}, Lio/reactivex/n;->merge(Lio/reactivex/r;Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p3}, Lco/uk/getmondo/payments/send/a/f;->a(Lco/uk/getmondo/payments/send/a/e;Lio/reactivex/u;)Lio/reactivex/c/h;

    move-result-object v1

    .line 57
    invoke-virtual {v0, v1}, Lio/reactivex/n;->switchMap(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const/4 v1, 0x1

    .line 58
    invoke-virtual {v0, v1}, Lio/reactivex/n;->replay(I)Lio/reactivex/e/a;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lio/reactivex/e/a;->b()Lio/reactivex/n;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/a/e;->f:Lio/reactivex/n;

    .line 60
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/a/e;Lio/reactivex/u;Ljava/lang/Object;)Lio/reactivex/r;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 57
    invoke-static {p0}, Lco/uk/getmondo/payments/send/a/i;->a(Lco/uk/getmondo/payments/send/a/e;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/n;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/n;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/reactivex/n;->subscribeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 140
    :try_start_0
    iget-object v0, p0, Lco/uk/getmondo/payments/send/a/e;->c:Lio/michaelrocks/libphonenumber/android/h;

    const-string v1, "GB"

    invoke-virtual {v0, p1, v1}, Lio/michaelrocks/libphonenumber/android/h;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Lio/michaelrocks/libphonenumber/android/j$a;

    move-result-object v0

    .line 142
    iget-object v1, p0, Lco/uk/getmondo/payments/send/a/e;->c:Lio/michaelrocks/libphonenumber/android/h;

    sget-object v2, Lio/michaelrocks/libphonenumber/android/h$a;->a:Lio/michaelrocks/libphonenumber/android/h$a;

    invoke-virtual {v1, v0, v2}, Lio/michaelrocks/libphonenumber/android/h;->a(Lio/michaelrocks/libphonenumber/android/j$a;Lio/michaelrocks/libphonenumber/android/h$a;)Ljava/lang/String;
    :try_end_0
    .catch Lio/michaelrocks/libphonenumber/android/NumberParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 144
    :goto_0
    return-object v0

    .line 143
    :catch_0
    move-exception v0

    .line 144
    const-string v0, ""

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/a/e;)Ljava/util/Map;
    .locals 1

    invoke-direct {p0}, Lco/uk/getmondo/payments/send/a/e;->c()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/a/e;Landroid/database/ContentObserver;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lco/uk/getmondo/payments/send/a/e;->d:Landroid/content/ContentResolver;

    invoke-virtual {v0, p1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/a/e;Lio/reactivex/o;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 91
    new-instance v0, Lco/uk/getmondo/payments/send/a/e$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1, p1}, Lco/uk/getmondo/payments/send/a/e$1;-><init>(Lco/uk/getmondo/payments/send/a/e;Landroid/os/Handler;Lio/reactivex/o;)V

    .line 98
    iget-object v1, p0, Lco/uk/getmondo/payments/send/a/e;->d:Landroid/content/ContentResolver;

    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 99
    invoke-static {p0, v0}, Lco/uk/getmondo/payments/send/a/h;->a(Lco/uk/getmondo/payments/send/a/e;Landroid/database/ContentObserver;)Lio/reactivex/c/f;

    move-result-object v0

    invoke-interface {p1, v0}, Lio/reactivex/o;->a(Lio/reactivex/c/f;)V

    .line 102
    sget-object v0, Lco/uk/getmondo/common/b/a;->a:Lco/uk/getmondo/common/b/a;

    invoke-interface {p1, v0}, Lio/reactivex/o;->a(Ljava/lang/Object;)V

    .line 103
    return-void
.end method

.method private c()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lco/uk/getmondo/payments/send/a/b;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 79
    iget-object v0, p0, Lco/uk/getmondo/payments/send/a/e;->a:Landroid/content/Context;

    const-string v2, "android.permission.READ_CONTACTS"

    .line 80
    invoke-static {v0, v2}, Landroid/support/v4/content/a;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 81
    :goto_0
    const-string v2, "Loading contacts from device"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ld/a/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 82
    if-eqz v0, :cond_1

    .line 83
    invoke-direct {p0}, Lco/uk/getmondo/payments/send/a/e;->e()Ljava/util/Map;

    move-result-object v0

    .line 86
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 80
    goto :goto_0

    .line 85
    :cond_1
    const-string v0, "Missing contacts permission. Returning an empty map instead"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ld/a/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 86
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    goto :goto_1
.end method

.method private d()Lio/reactivex/n;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    invoke-static {p0}, Lco/uk/getmondo/payments/send/a/g;->a(Lco/uk/getmondo/payments/send/a/e;)Lio/reactivex/p;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    .line 105
    iget-object v1, p0, Lco/uk/getmondo/payments/send/a/e;->b:Lio/reactivex/u;

    .line 107
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribeOn(Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v0

    const-wide/16 v2, 0x5

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v4, p0, Lco/uk/getmondo/payments/send/a/e;->b:Lio/reactivex/u;

    .line 110
    invoke-virtual {v0, v2, v3, v1, v4}, Lio/reactivex/n;->throttleFirst(JLjava/util/concurrent/TimeUnit;Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v0

    .line 105
    return-object v0
.end method

.method private e()Ljava/util/Map;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lco/uk/getmondo/payments/send/a/b;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v12, 0x0

    const/4 v3, 0x0

    .line 114
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    .line 115
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "contact_id"

    aput-object v0, v2, v12

    const/4 v0, 0x1

    const-string v1, "display_name"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "data1"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "photo_uri"

    aput-object v1, v2, v0

    .line 117
    iget-object v0, p0, Lco/uk/getmondo/payments/send/a/e;->d:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 118
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 119
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_2

    .line 120
    const-string v0, "contact_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 122
    const-string v0, "display_name"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 123
    const-string v0, "data1"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 124
    const-string v1, "photo_uri"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 126
    invoke-direct {p0, v0}, Lco/uk/getmondo/payments/send/a/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 127
    invoke-static {v9}, Lco/uk/getmondo/common/k/p;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    new-instance v5, Lco/uk/getmondo/payments/send/a/b;

    invoke-direct/range {v5 .. v10}, Lco/uk/getmondo/payments/send/a/b;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v11, v9, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 117
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 132
    :catchall_0
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_1
    if-eqz v2, :cond_1

    if-eqz v3, :cond_5

    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    :cond_1
    :goto_2
    throw v0

    :cond_2
    if-eqz v2, :cond_3

    if-eqz v3, :cond_4

    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    .line 133
    :cond_3
    :goto_3
    const-string v0, "Successfully read contacts."

    new-array v1, v12, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ld/a/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 134
    return-object v11

    .line 132
    :cond_4
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_3

    :cond_5
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public a()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lco/uk/getmondo/payments/send/a/b;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lco/uk/getmondo/payments/send/a/e;->f:Lio/reactivex/n;

    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lco/uk/getmondo/payments/send/a/e;->e:Lio/reactivex/i/c;

    sget-object v1, Lco/uk/getmondo/common/b/a;->a:Lco/uk/getmondo/common/b/a;

    invoke-virtual {v0, v1}, Lio/reactivex/i/c;->onNext(Ljava/lang/Object;)V

    .line 76
    return-void
.end method
