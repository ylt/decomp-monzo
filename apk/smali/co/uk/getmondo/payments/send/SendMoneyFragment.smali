.class public Lco/uk/getmondo/payments/send/SendMoneyFragment;
.super Lco/uk/getmondo/common/f/a;
.source "SendMoneyFragment.java"

# interfaces
.implements Lco/uk/getmondo/payments/send/o$a;


# instance fields
.field a:Lco/uk/getmondo/payments/send/o;

.field c:Lco/uk/getmondo/common/a;

.field contactsProgressBar:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110331
    .end annotation
.end field

.field contactsRecyclerView:Landroid/support/v7/widget/RecyclerView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110333
    .end annotation
.end field

.field d:Lco/uk/getmondo/payments/send/SendMoneyAdapter;

.field private final e:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Lco/uk/getmondo/payments/send/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Lco/uk/getmondo/payments/send/data/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private i:Landroid/support/design/widget/FloatingActionButton;

.field private j:Lbutterknife/Unbinder;

.field private k:Z

.field swipeRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110332
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Lco/uk/getmondo/common/f/a;-><init>()V

    .line 55
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->e:Lcom/b/b/c;

    .line 56
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->f:Lcom/b/b/c;

    .line 57
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->g:Lcom/b/b/c;

    .line 58
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->h:Lcom/b/b/c;

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->k:Z

    return-void
.end method

.method public static a()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 73
    new-instance v0, Lco/uk/getmondo/payments/send/SendMoneyFragment;

    invoke-direct {v0}, Lco/uk/getmondo/payments/send/SendMoneyFragment;-><init>()V

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/SendMoneyFragment;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 171
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->swipeRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroid/support/v4/widget/SwipeRefreshLayout$b;)V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/SendMoneyFragment;Lco/uk/getmondo/payments/send/a/a;Z)V
    .locals 2

    .prologue
    .line 96
    if-eqz p2, :cond_0

    .line 97
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->f:Lcom/b/b/c;

    check-cast p1, Lco/uk/getmondo/payments/send/a/b;

    invoke-virtual {v0, p1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 101
    :goto_0
    return-void

    .line 99
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->g:Lcom/b/b/c;

    invoke-virtual {p1}, Lco/uk/getmondo/payments/send/a/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/payments/send/SendMoneyFragment;Lio/reactivex/o;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 170
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->swipeRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-static {p1}, Lco/uk/getmondo/payments/send/l;->a(Lio/reactivex/o;)Landroid/support/v4/widget/SwipeRefreshLayout$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroid/support/v4/widget/SwipeRefreshLayout$b;)V

    .line 171
    invoke-static {p0}, Lco/uk/getmondo/payments/send/m;->a(Lco/uk/getmondo/payments/send/SendMoneyFragment;)Lio/reactivex/c/f;

    move-result-object v0

    invoke-interface {p1, v0}, Lio/reactivex/o;->a(Lio/reactivex/c/f;)V

    .line 172
    return-void
.end method

.method static synthetic a(Lio/reactivex/o;)V
    .locals 1

    .prologue
    .line 170
    sget-object v0, Lco/uk/getmondo/common/b/a;->a:Lco/uk/getmondo/common/b/a;

    invoke-interface {p0, v0}, Lio/reactivex/o;->a(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic b(Lco/uk/getmondo/payments/send/SendMoneyFragment;)V
    .locals 1

    .prologue
    .line 104
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/SendMoneyFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/payments/send/bank/BankPaymentActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/SendMoneyFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/d/aa;)V
    .locals 2

    .prologue
    .line 250
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/SendMoneyFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression$PaymentFlowFrom;->FROM_CONTACT_DISCOVERY:Lco/uk/getmondo/api/model/tracking/Impression$PaymentFlowFrom;

    invoke-static {v0, p1, v1}, Lco/uk/getmondo/payments/send/peer/PeerPaymentActivity;->a(Landroid/content/Context;Lco/uk/getmondo/d/aa;Lco/uk/getmondo/api/model/tracking/Impression$PaymentFlowFrom;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/SendMoneyFragment;->startActivity(Landroid/content/Intent;)V

    .line 251
    return-void
.end method

.method public a(Lco/uk/getmondo/payments/send/data/a/b;)V
    .locals 1

    .prologue
    .line 263
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/SendMoneyFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    invoke-static {v0, p1}, Lco/uk/getmondo/payments/send/bank/payment/BankPaymentDetailsActivity;->a(Landroid/content/Context;Lco/uk/getmondo/payments/send/data/a/b;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/SendMoneyFragment;->startActivity(Landroid/content/Intent;)V

    .line 264
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 268
    const v0, 0x7f0a0356

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/payments/send/SendMoneyFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 269
    const v1, 0x7f0a0355

    invoke-virtual {p0, v1}, Lco/uk/getmondo/payments/send/SendMoneyFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 270
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/SendMoneyFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v2

    sget-object v3, Lco/uk/getmondo/api/model/tracking/a;->INVITE_CONTACT_DISCOVERY:Lco/uk/getmondo/api/model/tracking/a;

    invoke-static {v2, v0, v1, v3}, Lco/uk/getmondo/common/k/j;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/api/model/tracking/a;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/SendMoneyFragment;->startActivity(Landroid/content/Intent;)V

    .line 271
    return-void
.end method

.method public a(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lco/uk/getmondo/payments/send/data/a/e;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 220
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->d:Lco/uk/getmondo/payments/send/SendMoneyAdapter;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->a(Ljava/util/Collection;)V

    .line 221
    return-void
.end method

.method public a(Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lco/uk/getmondo/payments/send/a/b;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lco/uk/getmondo/payments/send/a/d;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 215
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->d:Lco/uk/getmondo/payments/send/SendMoneyAdapter;

    invoke-virtual {v0, p1, p2}, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->a(Ljava/util/Collection;Ljava/util/Collection;)V

    .line 216
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 139
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->e:Lcom/b/b/c;

    return-object v0
.end method

.method public c()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/payments/send/a/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->f:Lcom/b/b/c;

    return-object v0
.end method

.method public d()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 151
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->g:Lcom/b/b/c;

    return-object v0
.end method

.method public e()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 157
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->i:Landroid/support/design/widget/FloatingActionButton;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public f()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/payments/send/data/a/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 163
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->h:Lcom/b/b/c;

    return-object v0
.end method

.method public g()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 169
    invoke-static {p0}, Lco/uk/getmondo/payments/send/k;->a(Lco/uk/getmondo/payments/send/SendMoneyFragment;)Lio/reactivex/p;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public h()V
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->d:Lco/uk/getmondo/payments/send/SendMoneyAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->a(Z)V

    .line 178
    return-void
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 182
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/SendMoneyFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    const-string v1, "android.permission.READ_CONTACTS"

    invoke-static {v0, v1}, Landroid/support/v4/content/a;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 187
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->c:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->L()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 188
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android.permission.READ_CONTACTS"

    aput-object v1, v0, v2

    invoke-virtual {p0, v0, v2}, Lco/uk/getmondo/payments/send/SendMoneyFragment;->requestPermissions([Ljava/lang/String;I)V

    .line 189
    return-void
.end method

.method public k()V
    .locals 2

    .prologue
    .line 193
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->swipeRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 194
    return-void
.end method

.method public l()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 225
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->d:Lco/uk/getmondo/payments/send/SendMoneyAdapter;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->getItemCount()I

    move-result v0

    if-gt v0, v1, :cond_0

    .line 226
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->contactsProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 230
    :goto_0
    return-void

    .line 228
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->swipeRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    goto :goto_0
.end method

.method public m()V
    .locals 2

    .prologue
    .line 234
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->contactsProgressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 235
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->swipeRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 236
    return-void
.end method

.method public n()V
    .locals 2

    .prologue
    .line 240
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->contactsProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 241
    return-void
.end method

.method public o()V
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->contactsProgressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 246
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 78
    invoke-super {p0, p1}, Lco/uk/getmondo/common/f/a;->onCreate(Landroid/os/Bundle;)V

    .line 79
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/SendMoneyFragment;->B()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/payments/send/SendMoneyFragment;)V

    .line 80
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 84
    const v0, 0x7f0500a6

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->a:Lco/uk/getmondo/payments/send/o;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/send/o;->b()V

    .line 132
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->j:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 133
    invoke-super {p0}, Lco/uk/getmondo/common/f/a;->onDestroyView()V

    .line 134
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 198
    if-nez p1, :cond_2

    .line 200
    array-length v2, p3

    if-lez v2, :cond_0

    aget v2, p3, v1

    if-nez v2, :cond_0

    .line 201
    iget-object v2, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->e:Lcom/b/b/c;

    sget-object v3, Lco/uk/getmondo/common/b/a;->a:Lco/uk/getmondo/common/b/a;

    invoke-virtual {v2, v3}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    move v2, v0

    .line 206
    :goto_0
    if-nez v2, :cond_1

    :goto_1
    iput-boolean v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->k:Z

    .line 207
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->c:Lco/uk/getmondo/common/a;

    invoke-static {v2}, Lco/uk/getmondo/api/model/tracking/Impression;->g(Z)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 211
    :goto_2
    return-void

    :cond_0
    move v2, v1

    .line 204
    goto :goto_0

    :cond_1
    move v0, v1

    .line 206
    goto :goto_1

    .line 209
    :cond_2
    invoke-super {p0, p1, p2, p3}, Lco/uk/getmondo/common/f/a;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    goto :goto_2
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 119
    invoke-super {p0}, Lco/uk/getmondo/common/f/a;->onResume()V

    .line 122
    iget-boolean v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->k:Z

    if-eqz v0, :cond_0

    .line 123
    const-string v0, ""

    const v1, 0x7f0a0350

    invoke-virtual {p0, v1}, Lco/uk/getmondo/payments/send/SendMoneyFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lco/uk/getmondo/common/d/a;->a(Ljava/lang/String;Ljava/lang/String;Z)Lco/uk/getmondo/common/d/a;

    move-result-object v0

    .line 124
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/SendMoneyFragment;->getChildFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    const-string v2, "error_dialog"

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/common/d/a;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 125
    iput-boolean v3, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->k:Z

    .line 127
    :cond_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 89
    invoke-super {p0, p2}, Lco/uk/getmondo/common/f/a;->onCreate(Landroid/os/Bundle;)V

    .line 90
    invoke-static {p0, p1}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->j:Lbutterknife/Unbinder;

    .line 91
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/SendMoneyFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    const v1, 0x7f1102fa

    invoke-virtual {v0, v1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/FloatingActionButton;

    iput-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->i:Landroid/support/design/widget/FloatingActionButton;

    .line 93
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->swipeRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    new-array v1, v4, [I

    const v2, 0x7f0f0056

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    .line 94
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->swipeRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 95
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->d:Lco/uk/getmondo/payments/send/SendMoneyAdapter;

    invoke-static {p0}, Lco/uk/getmondo/payments/send/h;->a(Lco/uk/getmondo/payments/send/SendMoneyFragment;)Lco/uk/getmondo/payments/send/SendMoneyAdapter$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->a(Lco/uk/getmondo/payments/send/SendMoneyAdapter$c;)V

    .line 102
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->d:Lco/uk/getmondo/payments/send/SendMoneyAdapter;

    invoke-virtual {v0, v3}, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->a(Z)V

    .line 103
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->d:Lco/uk/getmondo/payments/send/SendMoneyAdapter;

    iget-object v1, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->h:Lcom/b/b/c;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lco/uk/getmondo/payments/send/i;->a(Lcom/b/b/c;)Lco/uk/getmondo/payments/send/SendMoneyAdapter$f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->a(Lco/uk/getmondo/payments/send/SendMoneyAdapter$f;)V

    .line 104
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->d:Lco/uk/getmondo/payments/send/SendMoneyAdapter;

    invoke-static {p0}, Lco/uk/getmondo/payments/send/j;->a(Lco/uk/getmondo/payments/send/SendMoneyFragment;)Lco/uk/getmondo/payments/send/SendMoneyAdapter$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/payments/send/SendMoneyAdapter;->a(Lco/uk/getmondo/payments/send/SendMoneyAdapter$a;)V

    .line 106
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->contactsRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 107
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->contactsRecyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/SendMoneyFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$h;)V

    .line 108
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->contactsRecyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->d:Lco/uk/getmondo/payments/send/SendMoneyAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 110
    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/SendMoneyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0119

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 111
    iget-object v1, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->contactsRecyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v2, Lco/uk/getmondo/common/ui/h;

    invoke-virtual {p0}, Lco/uk/getmondo/payments/send/SendMoneyFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v3

    iget-object v4, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->d:Lco/uk/getmondo/payments/send/SendMoneyAdapter;

    invoke-direct {v2, v3, v4, v0}, Lco/uk/getmondo/common/ui/h;-><init>(Landroid/content/Context;La/a/a/a/a/a;I)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$g;)V

    .line 112
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->contactsRecyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, La/a/a/a/a/b;

    iget-object v2, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->d:Lco/uk/getmondo/payments/send/SendMoneyAdapter;

    invoke-direct {v1, v2}, La/a/a/a/a/b;-><init>(La/a/a/a/a/a;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$g;)V

    .line 114
    iget-object v0, p0, Lco/uk/getmondo/payments/send/SendMoneyFragment;->a:Lco/uk/getmondo/payments/send/o;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/payments/send/o;->a(Lco/uk/getmondo/payments/send/o$a;)V

    .line 115
    return-void
.end method

.method public p()V
    .locals 3

    .prologue
    .line 255
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.INSERT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 256
    const-string v1, "vnd.android.cursor.dir/raw_contact"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 257
    const-string v1, "finishActivityOnSaveCompleted"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 258
    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/send/SendMoneyFragment;->startActivity(Landroid/content/Intent;)V

    .line 259
    return-void
.end method
