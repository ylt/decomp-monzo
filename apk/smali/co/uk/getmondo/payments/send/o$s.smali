.class final Lco/uk/getmondo/payments/send/o$s;
.super Ljava/lang/Object;
.source "SendMoneyPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/payments/send/o;->a(Lco/uk/getmondo/payments/send/o$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "error",
        "",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/payments/send/o;

.field final synthetic b:Lco/uk/getmondo/payments/send/o$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/payments/send/o;Lco/uk/getmondo/payments/send/o$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/send/o$s;->a:Lco/uk/getmondo/payments/send/o;

    iput-object p2, p0, Lco/uk/getmondo/payments/send/o$s;->b:Lco/uk/getmondo/payments/send/o$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 35
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/send/o$s;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lco/uk/getmondo/payments/send/o$s;->b:Lco/uk/getmondo/payments/send/o$a;

    invoke-interface {v0}, Lco/uk/getmondo/payments/send/o$a;->m()V

    .line 93
    iget-object v0, p0, Lco/uk/getmondo/payments/send/o$s;->a:Lco/uk/getmondo/payments/send/o;

    invoke-static {v0}, Lco/uk/getmondo/payments/send/o;->e(Lco/uk/getmondo/payments/send/o;)Lco/uk/getmondo/common/e/a;

    move-result-object v1

    const-string v0, "error"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lco/uk/getmondo/payments/send/o$s;->b:Lco/uk/getmondo/payments/send/o$a;

    check-cast v0, Lco/uk/getmondo/common/e/a$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    .line 94
    return-void
.end method
