.class public final Lco/uk/getmondo/payments/recurring_cancellation/b;
.super Lco/uk/getmondo/common/ui/b;
.source "RecurringPaymentCancelPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/payments/recurring_cancellation/b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/payments/recurring_cancellation/b$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\u0008\u0004\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0013B3\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0002H\u0002J\u0010\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0002H\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelPresenter$View;",
        "ioScheduler",
        "Lio/reactivex/Scheduler;",
        "uiScheduler",
        "manager",
        "Lco/uk/getmondo/payments/data/RecurringPaymentsManager;",
        "apiErrorHandler",
        "Lco/uk/getmondo/common/errors/ApiErrorHandler;",
        "recurringPayment",
        "Lco/uk/getmondo/payments/data/model/RecurringPayment;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lco/uk/getmondo/payments/data/RecurringPaymentsManager;Lco/uk/getmondo/common/errors/ApiErrorHandler;Lco/uk/getmondo/payments/data/model/RecurringPayment;)V",
        "handleError",
        "",
        "exception",
        "",
        "view",
        "register",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Lio/reactivex/u;

.field private final d:Lio/reactivex/u;

.field private final e:Lco/uk/getmondo/payments/a/i;

.field private final f:Lco/uk/getmondo/common/e/a;

.field private final g:Lco/uk/getmondo/payments/a/a/f;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/payments/a/i;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/payments/a/a/f;)V
    .locals 1

    .prologue
    const-string v0, "ioScheduler"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "uiScheduler"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "manager"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiErrorHandler"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recurringPayment"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/payments/recurring_cancellation/b;->c:Lio/reactivex/u;

    iput-object p2, p0, Lco/uk/getmondo/payments/recurring_cancellation/b;->d:Lio/reactivex/u;

    iput-object p3, p0, Lco/uk/getmondo/payments/recurring_cancellation/b;->e:Lco/uk/getmondo/payments/a/i;

    iput-object p4, p0, Lco/uk/getmondo/payments/recurring_cancellation/b;->f:Lco/uk/getmondo/common/e/a;

    iput-object p5, p0, Lco/uk/getmondo/payments/recurring_cancellation/b;->g:Lco/uk/getmondo/payments/a/a/f;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/payments/recurring_cancellation/b;)Lco/uk/getmondo/payments/a/i;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_cancellation/b;->e:Lco/uk/getmondo/payments/a/i;

    return-object v0
.end method

.method public static final synthetic a(Lco/uk/getmondo/payments/recurring_cancellation/b;Ljava/lang/Throwable;Lco/uk/getmondo/payments/recurring_cancellation/b$a;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/payments/recurring_cancellation/b;->a(Ljava/lang/Throwable;Lco/uk/getmondo/payments/recurring_cancellation/b$a;)V

    return-void
.end method

.method private final a(Ljava/lang/Throwable;Lco/uk/getmondo/payments/recurring_cancellation/b$a;)V
    .locals 2

    .prologue
    .line 56
    instance-of v0, p1, Lco/uk/getmondo/api/ApiException;

    if-eqz v0, :cond_2

    .line 57
    invoke-static {}, Lco/uk/getmondo/payments/a/h;->values()[Lco/uk/getmondo/payments/a/h;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/common/e/f;

    move-object v1, p1

    check-cast v1, Lco/uk/getmondo/api/ApiException;

    invoke-virtual {v1}, Lco/uk/getmondo/api/ApiException;->e()Lco/uk/getmondo/api/model/b;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/b;->a()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-static {v0, v1}, Lco/uk/getmondo/common/e/d;->a([Lco/uk/getmondo/common/e/f;Ljava/lang/String;)Lco/uk/getmondo/common/e/f;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/a/h;

    .line 58
    if-eqz v0, :cond_2

    .line 59
    sget-object v1, Lco/uk/getmondo/payments/recurring_cancellation/c;->a:[I

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/h;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 70
    :cond_0
    :goto_1
    return-void

    .line 57
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 60
    :pswitch_0
    invoke-interface {p2}, Lco/uk/getmondo/payments/recurring_cancellation/b$a;->f()V

    goto :goto_1

    .line 61
    :pswitch_1
    invoke-interface {p2}, Lco/uk/getmondo/payments/recurring_cancellation/b$a;->e()V

    goto :goto_1

    .line 67
    :cond_2
    iget-object v1, p0, Lco/uk/getmondo/payments/recurring_cancellation/b;->f:Lco/uk/getmondo/common/e/a;

    move-object v0, p2

    check-cast v0, Lco/uk/getmondo/common/e/a$a;

    invoke-virtual {v1, p1, v0}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 68
    const v0, 0x7f0a0196

    invoke-interface {p2, v0}, Lco/uk/getmondo/payments/recurring_cancellation/b$a;->b(I)V

    goto :goto_1

    .line 59
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static final synthetic b(Lco/uk/getmondo/payments/recurring_cancellation/b;)Lco/uk/getmondo/payments/a/a/f;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_cancellation/b;->g:Lco/uk/getmondo/payments/a/a/f;

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/payments/recurring_cancellation/b;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_cancellation/b;->c:Lio/reactivex/u;

    return-object v0
.end method

.method public static final synthetic d(Lco/uk/getmondo/payments/recurring_cancellation/b;)Lio/reactivex/u;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_cancellation/b;->d:Lio/reactivex/u;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lco/uk/getmondo/payments/recurring_cancellation/b$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/recurring_cancellation/b;->a(Lco/uk/getmondo/payments/recurring_cancellation/b$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/payments/recurring_cancellation/b$a;)V
    .locals 4

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 33
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 34
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_cancellation/b;->g:Lco/uk/getmondo/payments/a/a/f;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-interface {p1, v0}, Lco/uk/getmondo/payments/recurring_cancellation/b$a;->a(Ljava/lang/Class;)V

    .line 35
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_cancellation/b;->g:Lco/uk/getmondo/payments/a/a/f;

    invoke-interface {p1, v0}, Lco/uk/getmondo/payments/recurring_cancellation/b$a;->a(Lco/uk/getmondo/payments/a/a/f;)V

    .line 37
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 38
    iget-object v2, p0, Lco/uk/getmondo/payments/recurring_cancellation/b;->b:Lio/reactivex/b/a;

    .line 52
    invoke-interface {p1}, Lco/uk/getmondo/payments/recurring_cancellation/b$a;->a()Lio/reactivex/n;

    move-result-object v3

    .line 39
    new-instance v0, Lco/uk/getmondo/payments/recurring_cancellation/b$b;

    invoke-direct {v0, p0, v1, p1}, Lco/uk/getmondo/payments/recurring_cancellation/b$b;-><init>(Lco/uk/getmondo/payments/recurring_cancellation/b;Ljava/lang/String;Lco/uk/getmondo/payments/recurring_cancellation/b$a;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v3, v0}, Lio/reactivex/n;->flatMapMaybe(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    .line 52
    new-instance v0, Lco/uk/getmondo/payments/recurring_cancellation/b$c;

    invoke-direct {v0, p1}, Lco/uk/getmondo/payments/recurring_cancellation/b$c;-><init>(Lco/uk/getmondo/payments/recurring_cancellation/b$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v1, "view.onPinEntered()\n    \u2026ubscribe { view.close() }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-static {v2, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/recurring_cancellation/b;->b:Lio/reactivex/b/a;

    .line 53
    return-void
.end method
