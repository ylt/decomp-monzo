.class public final Lco/uk/getmondo/payments/recurring_cancellation/f;
.super Ljava/lang/Object;
.source "RecurringPaymentModule.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0003H\u0007R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"
    }
    d2 = {
        "Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentModule;",
        "",
        "recurringPayment",
        "Lco/uk/getmondo/payments/data/model/RecurringPayment;",
        "(Lco/uk/getmondo/payments/data/model/RecurringPayment;)V",
        "provideRecurringPayment",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final a:Lco/uk/getmondo/payments/a/a/f;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/payments/a/a/f;)V
    .locals 1

    .prologue
    const-string v0, "recurringPayment"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/payments/recurring_cancellation/f;->a:Lco/uk/getmondo/payments/a/a/f;

    return-void
.end method


# virtual methods
.method public final a()Lco/uk/getmondo/payments/a/a/f;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_cancellation/f;->a:Lco/uk/getmondo/payments/a/a/f;

    return-object v0
.end method
