.class public final Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "RecurringPaymentCancelActivity.kt"

# interfaces
.implements Lco/uk/getmondo/payments/recurring_cancellation/b$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u001e2\u00020\u00012\u00020\u0002:\u0001\u001eB\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\n\u001a\u00020\u000bH\u0016J\u0008\u0010\u000c\u001a\u00020\u000bH\u0016J\u0008\u0010\r\u001a\u00020\u000bH\u0016J\u0012\u0010\u000e\u001a\u00020\u000b2\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0014J\u0008\u0010\u0011\u001a\u00020\u000bH\u0014J\u000e\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0013H\u0016J\u0008\u0010\u0015\u001a\u00020\u000bH\u0016J\u0008\u0010\u0016\u001a\u00020\u000bH\u0016J\u0010\u0010\u0017\u001a\u00020\u000b2\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J\u0008\u0010\u001a\u001a\u00020\u000bH\u0016J\u0018\u0010\u001b\u001a\u00020\u000b2\u000e\u0010\u001c\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00190\u001dH\u0016R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\"\u0004\u0008\u0008\u0010\t\u00a8\u0006\u001f"
    }
    d2 = {
        "Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;",
        "Lco/uk/getmondo/common/activities/BaseActivity;",
        "Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelPresenter$View;",
        "()V",
        "presenter",
        "Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelPresenter;)V",
        "clearPinEntryView",
        "",
        "close",
        "hideLoading",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onDestroy",
        "onPinEntered",
        "Lio/reactivex/Observable;",
        "",
        "showIncorrectPinError",
        "showLoading",
        "showPaymentDescription",
        "recurringPayment",
        "Lco/uk/getmondo/payments/data/model/RecurringPayment;",
        "showPinBlockedError",
        "showTitleAction",
        "paymentType",
        "Ljava/lang/Class;",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final b:Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity$a;


# instance fields
.field public a:Lco/uk/getmondo/payments/recurring_cancellation/b;

.field private c:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;->b:Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;->c:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;->c:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;->c:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;->c:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    new-instance v0, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity$b;-><init>(Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;)V

    check-cast v0, Lio/reactivex/p;

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.create { emit\u2026istener(null) }\n        }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lco/uk/getmondo/payments/a/a/f;)V
    .locals 5

    .prologue
    const-string v0, "recurringPayment"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    sget v0, Lco/uk/getmondo/c$a;->paymentCancellationSubtitle:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 62
    instance-of v1, p1, Lco/uk/getmondo/payments/a/a/e;

    if-eqz v1, :cond_0

    const v2, 0x7f0a02f0

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    .line 63
    const/4 v4, 0x0

    move-object v1, p1

    check-cast v1, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {v1}, Lco/uk/getmondo/payments/a/a/e;->c()Lco/uk/getmondo/d/c;

    move-result-object v1

    aput-object v1, v3, v4

    .line 64
    const/4 v1, 0x1

    invoke-interface {p1}, Lco/uk/getmondo/payments/a/a/f;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    .line 62
    invoke-virtual {p0, v2, v3}, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 61
    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    return-void

    .line 66
    :cond_0
    invoke-interface {p1}, Lco/uk/getmondo/payments/a/a/f;->d()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public a(Ljava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lco/uk/getmondo/payments/a/a/f;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "paymentType"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    sget v0, Lco/uk/getmondo/c$a;->paymentCancellationTitle:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 55
    const-class v1, Lco/uk/getmondo/payments/a/a/e;

    invoke-static {p1, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0a02ef

    invoke-virtual {p0, v1}, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 54
    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    return-void

    .line 56
    :cond_0
    const v1, 0x7f0a0161

    invoke-virtual {p0, v1}, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 71
    sget v0, Lco/uk/getmondo/c$a;->progressBarOverlay:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 72
    sget v0, Lco/uk/getmondo/c$a;->progressBar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->a(Landroid/view/View;)V

    .line 73
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 76
    sget v0, Lco/uk/getmondo/c$a;->progressBarOverlay:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 77
    sget v0, Lco/uk/getmondo/c$a;->progressBar:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-static {v0}, Lco/uk/getmondo/common/ae;->b(Landroid/view/View;)V

    .line 78
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 81
    sget v0, Lco/uk/getmondo/c$a;->pinEntryView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/PinEntryView;

    invoke-virtual {v0}, Lco/uk/getmondo/common/ui/PinEntryView;->a()V

    .line 82
    return-void
.end method

.method public e()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 86
    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {p0}, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;->m()Landroid/view/View;

    move-result-object v1

    .line 87
    const v2, 0x7f0a0182

    invoke-virtual {p0, v2}, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 86
    invoke-static {v0, v1, v2, v3, v3}, Lco/uk/getmondo/common/ui/i;->a(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;IZ)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar;->c()V

    .line 88
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    .line 91
    const/4 v0, 0x1

    invoke-static {v0}, Lco/uk/getmondo/common/d/e;->a(Z)Lco/uk/getmondo/common/d/e;

    move-result-object v0

    .line 92
    invoke-virtual {p0}, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-class v2, Lco/uk/getmondo/common/d/e;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/common/d/e;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 93
    return-void
.end method

.method public g()V
    .locals 0

    .prologue
    .line 96
    invoke-virtual {p0}, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;->finish()V

    .line 97
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 34
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 35
    const v0, 0x7f050059

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;->setContentView(I)V

    .line 36
    invoke-virtual {p0}, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "KEY_RECURRING_PAYMENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/a/a/f;

    .line 37
    invoke-virtual {p0}, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v1

    new-instance v2, Lco/uk/getmondo/payments/recurring_cancellation/f;

    const-string v3, "recurringPayment"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v0}, Lco/uk/getmondo/payments/recurring_cancellation/f;-><init>(Lco/uk/getmondo/payments/a/a/f;)V

    invoke-interface {v1, v2}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/payments/recurring_cancellation/f;)Lco/uk/getmondo/payments/recurring_cancellation/e;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/payments/recurring_cancellation/e;->a(Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;)V

    .line 38
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;->a:Lco/uk/getmondo/payments/recurring_cancellation/b;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    check-cast p0, Lco/uk/getmondo/payments/recurring_cancellation/b$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/payments/recurring_cancellation/b;->a(Lco/uk/getmondo/payments/recurring_cancellation/b$a;)V

    .line 39
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;->a:Lco/uk/getmondo/payments/recurring_cancellation/b;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/payments/recurring_cancellation/b;->b()V

    .line 43
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 44
    return-void
.end method
