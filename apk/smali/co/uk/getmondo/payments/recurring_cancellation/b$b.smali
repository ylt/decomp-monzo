.class final Lco/uk/getmondo/payments/recurring_cancellation/b$b;
.super Ljava/lang/Object;
.source "RecurringPaymentCancelPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/payments/recurring_cancellation/b;->a(Lco/uk/getmondo/payments/recurring_cancellation/b$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/l",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Maybe;",
        "Lco/uk/getmondo/payments/data/model/RecurringPayment;",
        "kotlin.jvm.PlatformType",
        "pin",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/payments/recurring_cancellation/b;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lco/uk/getmondo/payments/recurring_cancellation/b$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/payments/recurring_cancellation/b;Ljava/lang/String;Lco/uk/getmondo/payments/recurring_cancellation/b$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/recurring_cancellation/b$b;->a:Lco/uk/getmondo/payments/recurring_cancellation/b;

    iput-object p2, p0, Lco/uk/getmondo/payments/recurring_cancellation/b$b;->b:Ljava/lang/String;

    iput-object p3, p0, Lco/uk/getmondo/payments/recurring_cancellation/b$b;->c:Lco/uk/getmondo/payments/recurring_cancellation/b$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lio/reactivex/h;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/h",
            "<",
            "Lco/uk/getmondo/payments/a/a/f;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "pin"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_cancellation/b$b;->a:Lco/uk/getmondo/payments/recurring_cancellation/b;

    invoke-static {v0}, Lco/uk/getmondo/payments/recurring_cancellation/b;->a(Lco/uk/getmondo/payments/recurring_cancellation/b;)Lco/uk/getmondo/payments/a/i;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/payments/recurring_cancellation/b$b;->a:Lco/uk/getmondo/payments/recurring_cancellation/b;

    invoke-static {v1}, Lco/uk/getmondo/payments/recurring_cancellation/b;->b(Lco/uk/getmondo/payments/recurring_cancellation/b;)Lco/uk/getmondo/payments/a/a/f;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/payments/recurring_cancellation/b$b;->b:Ljava/lang/String;

    const-string v3, "idempotencyKey"

    invoke-static {v2, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, p1, v2}, Lco/uk/getmondo/payments/a/i;->a(Lco/uk/getmondo/payments/a/a/f;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    .line 41
    iget-object v1, p0, Lco/uk/getmondo/payments/recurring_cancellation/b$b;->a:Lco/uk/getmondo/payments/recurring_cancellation/b;

    invoke-static {v1}, Lco/uk/getmondo/payments/recurring_cancellation/b;->c(Lco/uk/getmondo/payments/recurring_cancellation/b;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    .line 42
    iget-object v1, p0, Lco/uk/getmondo/payments/recurring_cancellation/b$b;->a:Lco/uk/getmondo/payments/recurring_cancellation/b;

    invoke-static {v1}, Lco/uk/getmondo/payments/recurring_cancellation/b;->d(Lco/uk/getmondo/payments/recurring_cancellation/b;)Lio/reactivex/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v1

    .line 43
    new-instance v0, Lco/uk/getmondo/payments/recurring_cancellation/b$b$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/payments/recurring_cancellation/b$b$1;-><init>(Lco/uk/getmondo/payments/recurring_cancellation/b$b;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->c(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v1

    .line 44
    new-instance v0, Lco/uk/getmondo/payments/recurring_cancellation/b$b$2;

    invoke-direct {v0, p0}, Lco/uk/getmondo/payments/recurring_cancellation/b$b$2;-><init>(Lco/uk/getmondo/payments/recurring_cancellation/b$b;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->b(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v1

    .line 48
    new-instance v0, Lco/uk/getmondo/payments/recurring_cancellation/b$b$3;

    invoke-direct {v0, p0}, Lco/uk/getmondo/payments/recurring_cancellation/b$b$3;-><init>(Lco/uk/getmondo/payments/recurring_cancellation/b$b;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->a(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v0

    .line 49
    iget-object v1, p0, Lco/uk/getmondo/payments/recurring_cancellation/b$b;->a:Lco/uk/getmondo/payments/recurring_cancellation/b;

    invoke-static {v1}, Lco/uk/getmondo/payments/recurring_cancellation/b;->b(Lco/uk/getmondo/payments/recurring_cancellation/b;)Lco/uk/getmondo/payments/a/a/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Ljava/lang/Object;)Lio/reactivex/v;

    move-result-object v0

    .line 50
    invoke-static {v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/v;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/recurring_cancellation/b$b;->a(Ljava/lang/String;)Lio/reactivex/h;

    move-result-object v0

    return-object v0
.end method
