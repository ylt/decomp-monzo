.class public final Lco/uk/getmondo/payments/recurring_list/c$a;
.super Ljava/lang/Object;
.source "RecurringPaymentDetailsDialog.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/payments/recurring_list/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lco/uk/getmondo/payments/recurring_list/RecurringPaymentDetailsDialog$Companion;",
        "",
        "()V",
        "KEY_RECURRING_PAYMENT",
        "",
        "newInstance",
        "Lco/uk/getmondo/payments/recurring_list/RecurringPaymentDetailsDialog;",
        "recurringPayment",
        "Lco/uk/getmondo/payments/data/model/RecurringPayment;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lco/uk/getmondo/payments/recurring_list/c$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/payments/a/a/f;)Lco/uk/getmondo/payments/recurring_list/c;
    .locals 3

    .prologue
    const-string v0, "recurringPayment"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    new-instance v0, Lco/uk/getmondo/payments/recurring_list/c;

    invoke-direct {v0}, Lco/uk/getmondo/payments/recurring_list/c;-><init>()V

    .line 24
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 25
    const-string v2, "KEY_RECURRING_PAYMENT"

    check-cast p1, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 26
    invoke-virtual {v0, v1}, Lco/uk/getmondo/payments/recurring_list/c;->setArguments(Landroid/os/Bundle;)V

    .line 27
    return-object v0
.end method
