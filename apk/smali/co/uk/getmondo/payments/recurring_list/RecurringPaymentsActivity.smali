.class public final Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "RecurringPaymentsActivity.kt"

# interfaces
.implements Lco/uk/getmondo/payments/recurring_list/f$a;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010 \n\u0000\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0012\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0014J\u0008\u0010\u0014\u001a\u00020\u0011H\u0014J\u000e\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u0016H\u0016J\u0010\u0010\u0018\u001a\u00020\u00112\u0006\u0010\u0019\u001a\u00020\u0017H\u0016J\u0016\u0010\u001a\u001a\u00020\u00112\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u001cH\u0016R\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\"\u0004\u0008\u0008\u0010\tR\u001e\u0010\n\u001a\u00020\u000b8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000c\u0010\r\"\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\u001d"
    }
    d2 = {
        "Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity;",
        "Lco/uk/getmondo/common/activities/BaseActivity;",
        "Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsPresenter$View;",
        "()V",
        "adapter",
        "Lco/uk/getmondo/payments/recurring_list/RecurringPaymentAdapter;",
        "getAdapter",
        "()Lco/uk/getmondo/payments/recurring_list/RecurringPaymentAdapter;",
        "setAdapter",
        "(Lco/uk/getmondo/payments/recurring_list/RecurringPaymentAdapter;)V",
        "presenter",
        "Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsPresenter;)V",
        "onCreate",
        "",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onDestroy",
        "onRecurringPaymentClicked",
        "Lio/reactivex/Observable;",
        "Lco/uk/getmondo/payments/data/model/RecurringPayment;",
        "showRecurringPaymentDetails",
        "recurringPayment",
        "showRecurringPayments",
        "recurringPayments",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field public a:Lco/uk/getmondo/payments/recurring_list/f;

.field public b:Lco/uk/getmondo/payments/recurring_list/a;

.field private c:Ljava/util/HashMap;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity;->c:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity;->c:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity;->c:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity;->c:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/payments/a/a/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    new-instance v0, Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity$a;

    invoke-direct {v0, p0}, Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity$a;-><init>(Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity;)V

    check-cast v0, Lio/reactivex/p;

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.create<Recurr\u2026licked = null }\n        }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lco/uk/getmondo/payments/a/a/f;)V
    .locals 3

    .prologue
    const-string v0, "recurringPayment"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    sget-object v0, Lco/uk/getmondo/payments/recurring_list/c;->b:Lco/uk/getmondo/payments/recurring_list/c$a;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/payments/recurring_list/c$a;->a(Lco/uk/getmondo/payments/a/a/f;)Lco/uk/getmondo/payments/recurring_list/c;

    move-result-object v0

    .line 55
    invoke-virtual {p0}, Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    const-class v2, Lco/uk/getmondo/payments/recurring_list/c;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/payments/recurring_list/c;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 56
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lco/uk/getmondo/payments/a/a/f;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "recurringPayments"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    sget v0, Lco/uk/getmondo/c$a;->recurringRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$a;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.payments.recurring_list.RecurringPaymentAdapter"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lco/uk/getmondo/payments/recurring_list/a;

    .line 42
    invoke-virtual {v0, p1}, Lco/uk/getmondo/payments/recurring_list/a;->a(Ljava/util/List;)V

    .line 43
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 20
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 21
    const v0, 0x7f05005a

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity;->setContentView(I)V

    .line 23
    invoke-virtual {p0}, Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity;)V

    .line 25
    sget v0, Lco/uk/getmondo/c$a;->recurringRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    new-instance v2, Landroid/support/v7/widget/LinearLayoutManager;

    move-object v1, p0

    check-cast v1, Landroid/content/Context;

    invoke-direct {v2, v1}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    move-object v1, v2

    check-cast v1, Landroid/support/v7/widget/RecyclerView$h;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$h;)V

    .line 26
    sget v0, Lco/uk/getmondo/c$a;->recurringRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity;->b:Lco/uk/getmondo/payments/recurring_list/a;

    if-nez v1, :cond_0

    const-string v2, "adapter"

    invoke-static {v2}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    check-cast v1, Landroid/support/v7/widget/RecyclerView$a;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 27
    sget v0, Lco/uk/getmondo/c$a;->recurringRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 28
    new-instance v1, Landroid/support/v7/widget/al;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0, v3}, Landroid/support/v7/widget/al;-><init>(Landroid/content/Context;I)V

    move-object v0, p0

    .line 29
    check-cast v0, Landroid/content/Context;

    const v2, 0x7f0201d0

    invoke-static {v0, v2}, Landroid/support/v4/content/a;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/al;->a(Landroid/graphics/drawable/Drawable;)V

    .line 30
    sget v0, Lco/uk/getmondo/c$a;->recurringRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    check-cast v1, Landroid/support/v7/widget/RecyclerView$g;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$g;)V

    .line 32
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity;->a:Lco/uk/getmondo/payments/recurring_list/f;

    if-nez v0, :cond_1

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_1
    check-cast p0, Lco/uk/getmondo/payments/recurring_list/f$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/payments/recurring_list/f;->a(Lco/uk/getmondo/payments/recurring_list/f$a;)V

    .line 33
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity;->a:Lco/uk/getmondo/payments/recurring_list/f;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/payments/recurring_list/f;->b()V

    .line 37
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 38
    return-void
.end method
