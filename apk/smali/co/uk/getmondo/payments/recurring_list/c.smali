.class public final Lco/uk/getmondo/payments/recurring_list/c;
.super Landroid/support/v4/app/i;
.source "RecurringPaymentDetailsDialog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/payments/recurring_list/c$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \r2\u00020\u0001:\u0001\rB\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u0016R\u001b\u0010\u0003\u001a\u00020\u00048BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0007\u0010\u0008\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u000e"
    }
    d2 = {
        "Lco/uk/getmondo/payments/recurring_list/RecurringPaymentDetailsDialog;",
        "Landroid/support/v4/app/DialogFragment;",
        "()V",
        "paymentScheduleFormatter",
        "Lco/uk/getmondo/payments/send/PaymentScheduleFormatter;",
        "getPaymentScheduleFormatter",
        "()Lco/uk/getmondo/payments/send/PaymentScheduleFormatter;",
        "paymentScheduleFormatter$delegate",
        "Lkotlin/Lazy;",
        "onCreateDialog",
        "Landroid/app/Dialog;",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;

.field public static final b:Lco/uk/getmondo/payments/recurring_list/c$a;


# instance fields
.field private final c:Lkotlin/c;

.field private d:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/payments/recurring_list/c;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "paymentScheduleFormatter"

    const-string v5, "getPaymentScheduleFormatter()Lco/uk/getmondo/payments/send/PaymentScheduleFormatter;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lco/uk/getmondo/payments/recurring_list/c;->a:[Lkotlin/reflect/l;

    new-instance v0, Lco/uk/getmondo/payments/recurring_list/c$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/payments/recurring_list/c$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/payments/recurring_list/c;->b:Lco/uk/getmondo/payments/recurring_list/c$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/support/v4/app/i;-><init>()V

    .line 17
    new-instance v0, Lco/uk/getmondo/payments/recurring_list/c$c;

    invoke-direct {v0, p0}, Lco/uk/getmondo/payments/recurring_list/c$c;-><init>(Lco/uk/getmondo/payments/recurring_list/c;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/recurring_list/c;->c:Lkotlin/c;

    return-void
.end method

.method private final b()Lco/uk/getmondo/payments/send/a;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/c;->c:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/payments/recurring_list/c;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/send/a;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/c;->d:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/c;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 12

    .prologue
    const/4 v5, 0x4

    const/4 v3, 0x0

    const/4 v11, 0x0

    const/4 v2, 0x1

    .line 32
    invoke-virtual {p0}, Lco/uk/getmondo/payments/recurring_list/c;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "KEY_RECURRING_PAYMENT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lco/uk/getmondo/payments/a/a/f;

    .line 34
    instance-of v0, v7, Lco/uk/getmondo/payments/a/a/e;

    if-eqz v0, :cond_2

    move-object v0, v7

    .line 35
    check-cast v0, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/e;->e()Lco/uk/getmondo/payments/a/a/d;

    move-result-object v4

    .line 36
    invoke-virtual {v4}, Lco/uk/getmondo/payments/a/a/d;->c()Lco/uk/getmondo/payments/a/a/d$c;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/payments/recurring_list/d;->a:[I

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/d$c;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 38
    const v0, 0x7f0a02f2

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {v4}, Lco/uk/getmondo/payments/a/a/d;->c()Lco/uk/getmondo/payments/a/a/d$c;

    move-result-object v6

    invoke-virtual {v6}, Lco/uk/getmondo/payments/a/a/d$c;->b()I

    move-result v6

    invoke-virtual {p0, v6}, Lco/uk/getmondo/payments/recurring_list/c;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v11

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/payments/recurring_list/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v8, v0

    .line 40
    :goto_0
    invoke-direct {p0}, Lco/uk/getmondo/payments/recurring_list/c;->b()Lco/uk/getmondo/payments/send/a;

    move-result-object v0

    .line 41
    invoke-virtual {v4}, Lco/uk/getmondo/payments/a/a/d;->e()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    if-eqz v1, :cond_0

    :goto_1
    move v4, v2

    move-object v6, v3

    .line 40
    invoke-static/range {v0 .. v6}, Lco/uk/getmondo/payments/send/a;->a(Lco/uk/getmondo/payments/send/a;Lorg/threeten/bp/LocalDate;ZLorg/threeten/bp/format/TextStyle;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 45
    const v4, 0x7f0a02f1

    const/4 v0, 0x7

    new-array v6, v0, [Ljava/lang/Object;

    move-object v0, v7

    .line 46
    check-cast v0, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/e;->b()Lco/uk/getmondo/payments/a/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/b;->a()Ljava/lang/String;

    move-result-object v0

    sget-object v9, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v10, "Locale.ENGLISH"

    invoke-static {v9, v10}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez v0, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :pswitch_0
    const v0, 0x7f0a02f3

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/recurring_list/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v8, v0

    goto :goto_0

    .line 41
    :cond_0
    invoke-virtual {v4}, Lco/uk/getmondo/payments/a/a/d;->b()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    goto :goto_1

    .line 46
    :cond_1
    invoke-virtual {v0, v9}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v9, "(this as java.lang.String).toUpperCase(locale)"

    invoke-static {v0, v9}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    aput-object v0, v6, v11

    .line 47
    aput-object v8, v6, v2

    .line 48
    const/4 v2, 0x2

    move-object v0, v7

    check-cast v0, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/e;->c()Lco/uk/getmondo/d/c;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/c;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v2

    .line 49
    const/4 v0, 0x3

    aput-object v1, v6, v0

    .line 50
    invoke-interface {v7}, Lco/uk/getmondo/payments/a/a/f;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v5

    .line 51
    const/4 v1, 0x5

    move-object v0, v7

    check-cast v0, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/e;->b()Lco/uk/getmondo/payments/a/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/b;->c()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v1

    .line 52
    const/4 v1, 0x6

    move-object v0, v7

    check-cast v0, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/e;->b()Lco/uk/getmondo/payments/a/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/b;->b()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v1

    .line 45
    invoke-virtual {p0, v4, v6}, Lco/uk/getmondo/payments/recurring_list/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 58
    :goto_2
    instance-of v0, v7, Lco/uk/getmondo/payments/a/a/e;

    if-eqz v0, :cond_3

    const v0, 0x7f0a02ef

    move v2, v0

    .line 62
    :goto_3
    new-instance v4, Landroid/support/v7/app/d$a;

    invoke-virtual {p0}, Lco/uk/getmondo/payments/recurring_list/c;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const v5, 0x7f0c0111

    invoke-direct {v4, v0, v5}, Landroid/support/v7/app/d$a;-><init>(Landroid/content/Context;I)V

    .line 63
    invoke-interface {v7}, Lco/uk/getmondo/payments/a/a/f;->d()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v4, v0}, Landroid/support/v7/app/d$a;->a(Ljava/lang/CharSequence;)Landroid/support/v7/app/d$a;

    move-result-object v4

    move-object v0, v1

    .line 64
    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v4, v0}, Landroid/support/v7/app/d$a;->b(Ljava/lang/CharSequence;)Landroid/support/v7/app/d$a;

    move-result-object v1

    .line 65
    new-instance v0, Lco/uk/getmondo/payments/recurring_list/c$b;

    invoke-direct {v0, p0, v7}, Lco/uk/getmondo/payments/recurring_list/c$b;-><init>(Lco/uk/getmondo/payments/recurring_list/c;Lco/uk/getmondo/payments/a/a/f;)V

    check-cast v0, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v2, v0}, Landroid/support/v7/app/d$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/d$a;

    move-result-object v0

    .line 68
    const v1, 0x7f0a0330

    invoke-virtual {v0, v1, v3}, Landroid/support/v7/app/d$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/d$a;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Landroid/support/v7/app/d$a;->b()Landroid/support/v7/app/d;

    move-result-object v0

    const-string v1, "AlertDialog.Builder(acti\u2026                .create()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/app/Dialog;

    return-object v0

    .line 55
    :cond_2
    const v0, 0x7f0a0162

    new-array v1, v2, [Ljava/lang/Object;

    invoke-interface {v7}, Lco/uk/getmondo/payments/a/a/f;->d()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v11

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/payments/recurring_list/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 59
    :cond_3
    const v0, 0x7f0a0161

    move v2, v0

    goto :goto_3

    .line 36
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public synthetic onDestroyView()V
    .locals 0

    invoke-super {p0}, Landroid/support/v4/app/i;->onDestroyView()V

    invoke-virtual {p0}, Lco/uk/getmondo/payments/recurring_list/c;->a()V

    return-void
.end method
