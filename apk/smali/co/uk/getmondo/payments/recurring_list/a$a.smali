.class public final Lco/uk/getmondo/payments/recurring_list/a$a;
.super Landroid/support/v7/widget/RecyclerView$w;
.source "RecurringPaymentAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/payments/recurring_list/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u000b"
    }
    d2 = {
        "Lco/uk/getmondo/payments/recurring_list/RecurringPaymentAdapter$ViewHolder;",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        "view",
        "Landroid/view/View;",
        "(Lco/uk/getmondo/payments/recurring_list/RecurringPaymentAdapter;Landroid/view/View;)V",
        "getView",
        "()Landroid/view/View;",
        "bind",
        "",
        "recurringPayment",
        "Lco/uk/getmondo/payments/data/model/RecurringPayment;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/payments/recurring_list/a;

.field private final b:Landroid/view/View;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/payments/recurring_list/a;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "view"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iput-object p1, p0, Lco/uk/getmondo/payments/recurring_list/a$a;->a:Lco/uk/getmondo/payments/recurring_list/a;

    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$w;-><init>(Landroid/view/View;)V

    iput-object p2, p0, Lco/uk/getmondo/payments/recurring_list/a$a;->b:Landroid/view/View;

    .line 52
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/a$a;->b:Landroid/view/View;

    sget v1, Lco/uk/getmondo/c$a;->recurringPaymentImage:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClipToOutline(Z)V

    .line 53
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/a$a;->b:Landroid/view/View;

    sget v1, Lco/uk/getmondo/c$a;->recurringPaymentImage:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f0201f8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 54
    iget-object v1, p0, Lco/uk/getmondo/payments/recurring_list/a$a;->itemView:Landroid/view/View;

    new-instance v0, Lco/uk/getmondo/payments/recurring_list/a$a$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/payments/recurring_list/a$a$1;-><init>(Lco/uk/getmondo/payments/recurring_list/a$a;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/payments/a/a/f;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/16 v5, 0x8

    const/4 v1, 0x0

    const-string v0, "recurringPayment"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/a$a;->b:Landroid/view/View;

    sget v3, Lco/uk/getmondo/c$a;->paymentTitle:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-interface {p1}, Lco/uk/getmondo/payments/a/a/f;->d()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/a$a;->b:Landroid/view/View;

    sget v3, Lco/uk/getmondo/c$a;->paymentSubtitle:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 60
    instance-of v3, p1, Lco/uk/getmondo/payments/a/a/e;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lco/uk/getmondo/payments/recurring_list/a$a;->b:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a0332

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    .line 59
    :goto_0
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    instance-of v0, p1, Lco/uk/getmondo/payments/a/a/e;

    if-eqz v0, :cond_2

    .line 66
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/a$a;->b:Landroid/view/View;

    sget v3, Lco/uk/getmondo/c$a;->paymentAmount:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/AmountView;

    move-object v3, p1

    check-cast v3, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {v3}, Lco/uk/getmondo/payments/a/a/e;->c()Lco/uk/getmondo/d/c;

    move-result-object v3

    invoke-virtual {v0, v3}, Lco/uk/getmondo/common/ui/AmountView;->setAmount(Lco/uk/getmondo/d/c;)V

    .line 67
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/a$a;->b:Landroid/view/View;

    sget v3, Lco/uk/getmondo/c$a;->paymentAmount:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/AmountView;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/ui/AmountView;->setVisibility(I)V

    .line 69
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/a$a;->a:Lco/uk/getmondo/payments/recurring_list/a;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/recurring_list/a;->c()Lco/uk/getmondo/payments/send/a;

    move-result-object v3

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/e;->e()Lco/uk/getmondo/payments/a/a/d;

    move-result-object v0

    invoke-virtual {v3, v0}, Lco/uk/getmondo/payments/send/a;->b(Lco/uk/getmondo/payments/a/a/d;)Ljava/lang/String;

    move-result-object v3

    .line 70
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/a$a;->b:Landroid/view/View;

    sget v4, Lco/uk/getmondo/c$a;->paymentAmountSubtitle:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/a$a;->b:Landroid/view/View;

    sget v3, Lco/uk/getmondo/c$a;->paymentAmountSubtitle:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 73
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/a$a;->a:Lco/uk/getmondo/payments/recurring_list/a;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/recurring_list/a;->b()Lco/uk/getmondo/common/ui/a;

    move-result-object v0

    invoke-interface {p1}, Lco/uk/getmondo/payments/a/a/f;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lco/uk/getmondo/common/ui/a;->a(Ljava/lang/String;)Lco/uk/getmondo/common/ui/a$b;

    move-result-object v0

    const/4 v4, 0x7

    move v3, v1

    move-object v5, v2

    invoke-static/range {v0 .. v5}, Lco/uk/getmondo/common/ui/a$b;->a(Lco/uk/getmondo/common/ui/a$b;ILandroid/graphics/Typeface;ZILjava/lang/Object;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 74
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/a$a;->b:Landroid/view/View;

    sget v2, Lco/uk/getmondo/c$a;->recurringPaymentImage:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 79
    :goto_1
    return-void

    .line 61
    :cond_0
    instance-of v3, p1, Lco/uk/getmondo/payments/a/a/a;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lco/uk/getmondo/payments/recurring_list/a$a;->b:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a0331

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    goto/16 :goto_0

    .line 62
    :cond_1
    const-string v3, ""

    check-cast v3, Ljava/lang/CharSequence;

    goto/16 :goto_0

    .line 76
    :cond_2
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/a$a;->b:Landroid/view/View;

    sget v1, Lco/uk/getmondo/c$a;->recurringPaymentImage:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f020154

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 77
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/a$a;->b:Landroid/view/View;

    sget v1, Lco/uk/getmondo/c$a;->paymentAmount:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/AmountView;

    invoke-virtual {v0, v5}, Lco/uk/getmondo/common/ui/AmountView;->setVisibility(I)V

    .line 78
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/a$a;->b:Landroid/view/View;

    sget v1, Lco/uk/getmondo/c$a;->paymentAmountSubtitle:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method
