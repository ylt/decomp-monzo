.class public final Lco/uk/getmondo/payments/recurring_list/f;
.super Lco/uk/getmondo/common/ui/b;
.source "RecurringPaymentsPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/payments/recurring_list/f$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/payments/recurring_list/f$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\tB\u000f\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u0002H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsPresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsPresenter$View;",
        "manager",
        "Lco/uk/getmondo/payments/data/RecurringPaymentsManager;",
        "(Lco/uk/getmondo/payments/data/RecurringPaymentsManager;)V",
        "register",
        "",
        "view",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private final c:Lco/uk/getmondo/payments/a/i;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/payments/a/i;)V
    .locals 1

    .prologue
    const-string v0, "manager"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/payments/recurring_list/f;->c:Lco/uk/getmondo/payments/a/i;

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 12
    check-cast p1, Lco/uk/getmondo/payments/recurring_list/f$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/recurring_list/f;->a(Lco/uk/getmondo/payments/recurring_list/f$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/payments/recurring_list/f$a;)V
    .locals 3

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 18
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 20
    iget-object v1, p0, Lco/uk/getmondo/payments/recurring_list/f;->b:Lio/reactivex/b/a;

    .line 21
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/f;->c:Lco/uk/getmondo/payments/a/i;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/i;->f()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/payments/recurring_list/f$b;

    invoke-direct {v0, p1}, Lco/uk/getmondo/payments/recurring_list/f$b;-><init>(Lco/uk/getmondo/payments/recurring_list/f$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "manager.allRecurringPaym\u2026owRecurringPayments(it) }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/recurring_list/f;->b:Lio/reactivex/b/a;

    .line 23
    iget-object v1, p0, Lco/uk/getmondo/payments/recurring_list/f;->b:Lio/reactivex/b/a;

    .line 24
    invoke-interface {p1}, Lco/uk/getmondo/payments/recurring_list/f$a;->a()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/payments/recurring_list/f$c;

    invoke-direct {v0, p1}, Lco/uk/getmondo/payments/recurring_list/f$c;-><init>(Lco/uk/getmondo/payments/recurring_list/f$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.onRecurringPaymentC\u2026rringPaymentDetails(it) }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-static {v1, v0}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)Lio/reactivex/b/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/recurring_list/f;->b:Lio/reactivex/b/a;

    .line 25
    return-void
.end method
