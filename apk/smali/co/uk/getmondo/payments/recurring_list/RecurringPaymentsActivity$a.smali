.class final Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity$a;
.super Ljava/lang/Object;
.source "RecurringPaymentsActivity.kt"

# interfaces
.implements Lio/reactivex/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity;->a()Lio/reactivex/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/p",
        "<TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "emitter",
        "Lio/reactivex/ObservableEmitter;",
        "Lco/uk/getmondo/payments/data/model/RecurringPayment;",
        "kotlin.jvm.PlatformType",
        "subscribe"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity;


# direct methods
.method constructor <init>(Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity$a;->a:Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/o;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o",
            "<",
            "Lco/uk/getmondo/payments/a/a/f;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity$a;->a:Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity;

    sget v1, Lco/uk/getmondo/c$a;->recurringRecyclerView:I

    invoke-virtual {v0, v1}, Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$a;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.payments.recurring_list.RecurringPaymentAdapter"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lco/uk/getmondo/payments/recurring_list/a;

    .line 48
    new-instance v1, Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity$a$1;

    invoke-direct {v1, p1}, Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity$a$1;-><init>(Lio/reactivex/o;)V

    check-cast v1, Lkotlin/d/a/b;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/payments/recurring_list/a;->a(Lkotlin/d/a/b;)V

    .line 49
    new-instance v1, Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity$a$2;

    invoke-direct {v1, v0}, Lco/uk/getmondo/payments/recurring_list/RecurringPaymentsActivity$a$2;-><init>(Lco/uk/getmondo/payments/recurring_list/a;)V

    move-object v0, v1

    check-cast v0, Lio/reactivex/c/f;

    invoke-interface {p1, v0}, Lio/reactivex/o;->a(Lio/reactivex/c/f;)V

    .line 50
    return-void
.end method
