.class final Lco/uk/getmondo/payments/recurring_list/c$b;
.super Ljava/lang/Object;
.source "RecurringPaymentDetailsDialog.kt"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/payments/recurring_list/c;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Landroid/content/DialogInterface;",
        "kotlin.jvm.PlatformType",
        "<anonymous parameter 1>",
        "",
        "onClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/payments/recurring_list/c;

.field final synthetic b:Lco/uk/getmondo/payments/a/a/f;


# direct methods
.method constructor <init>(Lco/uk/getmondo/payments/recurring_list/c;Lco/uk/getmondo/payments/a/a/f;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/recurring_list/c$b;->a:Lco/uk/getmondo/payments/recurring_list/c;

    iput-object p2, p0, Lco/uk/getmondo/payments/recurring_list/c$b;->b:Lco/uk/getmondo/payments/a/a/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    .line 66
    iget-object v1, p0, Lco/uk/getmondo/payments/recurring_list/c$b;->a:Lco/uk/getmondo/payments/recurring_list/c;

    sget-object v2, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity;->b:Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity$a;

    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/c$b;->a:Lco/uk/getmondo/payments/recurring_list/c;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/recurring_list/c;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    const-string v3, "activity"

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/content/Context;

    iget-object v3, p0, Lco/uk/getmondo/payments/recurring_list/c$b;->b:Lco/uk/getmondo/payments/a/a/f;

    const-string v4, "recurringPayment"

    invoke-static {v3, v4}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v0, v3}, Lco/uk/getmondo/payments/recurring_cancellation/RecurringPaymentCancelActivity$a;->a(Landroid/content/Context;Lco/uk/getmondo/payments/a/a/f;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Lco/uk/getmondo/payments/recurring_list/c;->startActivity(Landroid/content/Intent;)V

    .line 67
    return-void
.end method
