.class public final Lco/uk/getmondo/payments/recurring_list/g;
.super Ljava/lang/Object;
.source "RecurringPaymentsPresenter_Factory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/payments/recurring_list/f;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/payments/recurring_list/f;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/a/i;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lco/uk/getmondo/payments/recurring_list/g;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/payments/recurring_list/g;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lb/a;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/payments/recurring_list/f;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/a/i;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    sget-boolean v0, Lco/uk/getmondo/payments/recurring_list/g;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 25
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/payments/recurring_list/g;->b:Lb/a;

    .line 26
    sget-boolean v0, Lco/uk/getmondo/payments/recurring_list/g;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 27
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/payments/recurring_list/g;->c:Ljavax/a/a;

    .line 28
    return-void
.end method

.method public static a(Lb/a;Ljavax/a/a;)Lb/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/payments/recurring_list/f;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/payments/a/i;",
            ">;)",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/payments/recurring_list/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    new-instance v0, Lco/uk/getmondo/payments/recurring_list/g;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/payments/recurring_list/g;-><init>(Lb/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/payments/recurring_list/f;
    .locals 3

    .prologue
    .line 32
    iget-object v1, p0, Lco/uk/getmondo/payments/recurring_list/g;->b:Lb/a;

    new-instance v2, Lco/uk/getmondo/payments/recurring_list/f;

    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/g;->c:Ljavax/a/a;

    .line 34
    invoke-interface {v0}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/a/i;

    invoke-direct {v2, v0}, Lco/uk/getmondo/payments/recurring_list/f;-><init>(Lco/uk/getmondo/payments/a/i;)V

    .line 32
    invoke-static {v1, v2}, Lb/a/c;->a(Lb/a;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/recurring_list/f;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0}, Lco/uk/getmondo/payments/recurring_list/g;->a()Lco/uk/getmondo/payments/recurring_list/f;

    move-result-object v0

    return-object v0
.end method
