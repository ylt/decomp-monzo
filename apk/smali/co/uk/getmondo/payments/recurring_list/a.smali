.class public final Lco/uk/getmondo/payments/recurring_list/a;
.super Landroid/support/v7/widget/RecyclerView$a;
.source "RecurringPaymentAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/payments/recurring_list/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$a",
        "<",
        "Lco/uk/getmondo/payments/recurring_list/a$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0010!\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0008\u0002\u0018\u00002\u000c\u0012\u0008\u0012\u00060\u0002R\u00020\u00000\u0001:\u0001&B\u0011\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0008\u0010\u001b\u001a\u00020\u001cH\u0016J\u001c\u0010\u001d\u001a\u00020\u000f2\n\u0010\u001e\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u001f\u001a\u00020\u001cH\u0016J\u001c\u0010 \u001a\u00060\u0002R\u00020\u00002\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u001cH\u0016J\u0014\u0010$\u001a\u00020\u000f2\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u000e0%R\u001b\u0010\u0006\u001a\u00020\u00078FX\u0086\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\n\u0010\u000b\u001a\u0004\u0008\u0008\u0010\tR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R(\u0010\u000c\u001a\u0010\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f\u0018\u00010\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011\"\u0004\u0008\u0012\u0010\u0013R\u0014\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0016\u001a\u00020\u00178FX\u0086\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u001a\u0010\u000b\u001a\u0004\u0008\u0018\u0010\u0019\u00a8\u0006\'"
    }
    d2 = {
        "Lco/uk/getmondo/payments/recurring_list/RecurringPaymentAdapter;",
        "Landroid/support/v7/widget/RecyclerView$Adapter;",
        "Lco/uk/getmondo/payments/recurring_list/RecurringPaymentAdapter$ViewHolder;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "avatarGenerator",
        "Lco/uk/getmondo/common/ui/AvatarGenerator;",
        "getAvatarGenerator",
        "()Lco/uk/getmondo/common/ui/AvatarGenerator;",
        "avatarGenerator$delegate",
        "Lkotlin/Lazy;",
        "recurringPaymentClicked",
        "Lkotlin/Function1;",
        "Lco/uk/getmondo/payments/data/model/RecurringPayment;",
        "",
        "getRecurringPaymentClicked",
        "()Lkotlin/jvm/functions/Function1;",
        "setRecurringPaymentClicked",
        "(Lkotlin/jvm/functions/Function1;)V",
        "recurringPayments",
        "",
        "scheduleFormatter",
        "Lco/uk/getmondo/payments/send/PaymentScheduleFormatter;",
        "getScheduleFormatter",
        "()Lco/uk/getmondo/payments/send/PaymentScheduleFormatter;",
        "scheduleFormatter$delegate",
        "getItemCount",
        "",
        "onBindViewHolder",
        "holder",
        "position",
        "onCreateViewHolder",
        "parent",
        "Landroid/view/ViewGroup;",
        "viewType",
        "setRecurringPayments",
        "",
        "ViewHolder",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;


# instance fields
.field private b:Lkotlin/d/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/b",
            "<-",
            "Lco/uk/getmondo/payments/a/a/f;",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lkotlin/c;

.field private final d:Lkotlin/c;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/payments/a/a/f;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/payments/recurring_list/a;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "avatarGenerator"

    const-string v5, "getAvatarGenerator()Lco/uk/getmondo/common/ui/AvatarGenerator;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    const/4 v2, 0x1

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/payments/recurring_list/a;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "scheduleFormatter"

    const-string v5, "getScheduleFormatter()Lco/uk/getmondo/payments/send/PaymentScheduleFormatter;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lco/uk/getmondo/payments/recurring_list/a;->a:[Lkotlin/reflect/l;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$a;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/payments/recurring_list/a;->f:Landroid/content/Context;

    .line 24
    new-instance v0, Lco/uk/getmondo/payments/recurring_list/a$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/payments/recurring_list/a$b;-><init>(Lco/uk/getmondo/payments/recurring_list/a;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/recurring_list/a;->c:Lkotlin/c;

    .line 25
    new-instance v0, Lco/uk/getmondo/payments/recurring_list/a$c;

    invoke-direct {v0, p0}, Lco/uk/getmondo/payments/recurring_list/a$c;-><init>(Lco/uk/getmondo/payments/recurring_list/a;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/payments/recurring_list/a;->d:Lkotlin/c;

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lco/uk/getmondo/payments/recurring_list/a;->e:Ljava/util/List;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/payments/recurring_list/a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/a;->e:Ljava/util/List;

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/payments/recurring_list/a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/a;->f:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;I)Lco/uk/getmondo/payments/recurring_list/a$a;
    .locals 3

    .prologue
    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f050108

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 37
    new-instance v1, Lco/uk/getmondo/payments/recurring_list/a$a;

    const-string v2, "view"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, p0, v0}, Lco/uk/getmondo/payments/recurring_list/a$a;-><init>(Lco/uk/getmondo/payments/recurring_list/a;Landroid/view/View;)V

    return-object v1
.end method

.method public final a()Lkotlin/d/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/d/a/b",
            "<",
            "Lco/uk/getmondo/payments/a/a/f;",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 22
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/a;->b:Lkotlin/d/a/b;

    return-object v0
.end method

.method public a(Lco/uk/getmondo/payments/recurring_list/a$a;I)V
    .locals 1

    .prologue
    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/a;->e:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/a/a/f;

    invoke-virtual {p1, v0}, Lco/uk/getmondo/payments/recurring_list/a$a;->a(Lco/uk/getmondo/payments/a/a/f;)V

    .line 46
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lco/uk/getmondo/payments/a/a/f;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "recurringPayments"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/a;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 31
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/a;->e:Ljava/util/List;

    check-cast p1, Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 32
    invoke-virtual {p0}, Lco/uk/getmondo/payments/recurring_list/a;->notifyDataSetChanged()V

    .line 33
    return-void
.end method

.method public final a(Lkotlin/d/a/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/d/a/b",
            "<-",
            "Lco/uk/getmondo/payments/a/a/f;",
            "Lkotlin/n;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    iput-object p1, p0, Lco/uk/getmondo/payments/recurring_list/a;->b:Lkotlin/d/a/b;

    return-void
.end method

.method public final b()Lco/uk/getmondo/common/ui/a;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/a;->c:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/payments/recurring_list/a;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/a;

    return-object v0
.end method

.method public final c()Lco/uk/getmondo/payments/send/a;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/a;->d:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/payments/recurring_list/a;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/send/a;

    return-object v0
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lco/uk/getmondo/payments/recurring_list/a;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$w;I)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lco/uk/getmondo/payments/recurring_list/a$a;

    invoke-virtual {p0, p1, p2}, Lco/uk/getmondo/payments/recurring_list/a;->a(Lco/uk/getmondo/payments/recurring_list/a$a;I)V

    return-void
.end method

.method public synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$w;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0, p1, p2}, Lco/uk/getmondo/payments/recurring_list/a;->a(Landroid/view/ViewGroup;I)Lco/uk/getmondo/payments/recurring_list/a$a;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$w;

    return-object v0
.end method
