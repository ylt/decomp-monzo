.class public final Lco/uk/getmondo/payments/a/d;
.super Ljava/lang/Object;
.source "FpsSchemaDataMapper.kt"

# interfaces
.implements Lco/uk/getmondo/d/a/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lco/uk/getmondo/d/a/j",
        "<",
        "Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;",
        "Lco/uk/getmondo/payments/a/a/b;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u00c6\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0002H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "Lco/uk/getmondo/payments/data/FpsSchemaDataMapper;",
        "Lco/uk/getmondo/model/mapper/Mapper;",
        "Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;",
        "Lco/uk/getmondo/payments/data/model/FpsSchemaData;",
        "()V",
        "apply",
        "apiValue",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/payments/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    new-instance v0, Lco/uk/getmondo/payments/a/d;

    invoke-direct {v0}, Lco/uk/getmondo/payments/a/d;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lco/uk/getmondo/payments/a/d;

    sput-object p0, Lco/uk/getmondo/payments/a/d;->a:Lco/uk/getmondo/payments/a/d;

    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;)Lco/uk/getmondo/payments/a/a/b;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const-string v0, "apiValue"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    new-instance v0, Lco/uk/getmondo/payments/a/a/b;

    .line 10
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;->a()Ljava/lang/String;

    move-result-object v2

    .line 11
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;->b()Ljava/lang/String;

    move-result-object v3

    .line 12
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;->c()Ljava/lang/String;

    move-result-object v4

    .line 13
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;->d()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    move-object v7, v1

    .line 9
    invoke-direct/range {v0 .. v7}, Lco/uk/getmondo/payments/a/a/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILkotlin/d/b/i;)V

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 7
    check-cast p1, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/a/d;->a(Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;)Lco/uk/getmondo/payments/a/a/b;

    move-result-object v0

    return-object v0
.end method
