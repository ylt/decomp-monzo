.class public final Lco/uk/getmondo/payments/a/b;
.super Ljava/lang/Object;
.source "DirectDebitsStorage.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00060\u00050\u0004J\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0004J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cJ\u0014\u0010\r\u001a\u00020\n2\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lco/uk/getmondo/payments/data/DirectDebitsStorage;",
        "",
        "()V",
        "allDirectDebits",
        "Lio/reactivex/Observable;",
        "Lco/uk/getmondo/common/data/QueryResults;",
        "Lco/uk/getmondo/payments/data/model/DirectDebit;",
        "count",
        "",
        "delete",
        "Lio/reactivex/Completable;",
        "directDebitId",
        "",
        "saveAll",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lio/reactivex/b;
    .locals 1

    .prologue
    const-string v0, "directDebitId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    new-instance v0, Lco/uk/getmondo/payments/a/b$d;

    invoke-direct {v0, p1}, Lco/uk/getmondo/payments/a/b$d;-><init>(Ljava/lang/String;)V

    check-cast v0, Lio/realm/av$a;

    invoke-static {v0}, Lco/uk/getmondo/common/j/g;->a(Lio/realm/av$a;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/List;)Lio/reactivex/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lco/uk/getmondo/payments/a/a/a;",
            ">;)",
            "Lio/reactivex/b;"
        }
    .end annotation

    .prologue
    const-string v0, "allDirectDebits"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    new-instance v0, Lco/uk/getmondo/payments/a/b$e;

    invoke-direct {v0, p1}, Lco/uk/getmondo/payments/a/b$e;-><init>(Ljava/util/List;)V

    check-cast v0, Lio/realm/av$a;

    invoke-static {v0}, Lco/uk/getmondo/common/j/g;->a(Lio/realm/av$a;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/common/b/b",
            "<",
            "Lco/uk/getmondo/payments/a/a/a;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 34
    sget-object v0, Lco/uk/getmondo/payments/a/b$a;->a:Lco/uk/getmondo/payments/a/b$a;

    check-cast v0, Lkotlin/d/a/b;

    invoke-static {v0}, Lco/uk/getmondo/common/j/g;->a(Lkotlin/d/a/b;)Lio/reactivex/n;

    move-result-object v1

    .line 35
    sget-object v0, Lco/uk/getmondo/payments/a/b$b;->a:Lco/uk/getmondo/payments/a/b$b;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxRealm.asObservable { r\u2026 .map { it.queryResults }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/b;->a()Lio/reactivex/n;

    move-result-object v1

    .line 40
    sget-object v0, Lco/uk/getmondo/payments/a/b$c;->a:Lco/uk/getmondo/payments/a/b$c;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 41
    invoke-virtual {v0}, Lio/reactivex/n;->distinctUntilChanged()Lio/reactivex/n;

    move-result-object v0

    const-string v1, "allDirectDebits()\n      \u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
