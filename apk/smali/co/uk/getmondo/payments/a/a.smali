.class public final Lco/uk/getmondo/payments/a/a;
.super Ljava/lang/Object;
.source "DirectDebitMapper.kt"

# interfaces
.implements Lco/uk/getmondo/d/a/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lco/uk/getmondo/d/a/j",
        "<",
        "Lco/uk/getmondo/api/model/payments/ApiDirectDebit;",
        "Lco/uk/getmondo/payments/a/a/a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u00c6\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0002H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "Lco/uk/getmondo/payments/data/DirectDebitMapper;",
        "Lco/uk/getmondo/model/mapper/Mapper;",
        "Lco/uk/getmondo/api/model/payments/ApiDirectDebit;",
        "Lco/uk/getmondo/payments/data/model/DirectDebit;",
        "()V",
        "apply",
        "apiValue",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/payments/a/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    new-instance v0, Lco/uk/getmondo/payments/a/a;

    invoke-direct {v0}, Lco/uk/getmondo/payments/a/a;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lco/uk/getmondo/payments/a/a;

    sput-object p0, Lco/uk/getmondo/payments/a/a;->a:Lco/uk/getmondo/payments/a/a;

    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/api/model/payments/ApiDirectDebit;)Lco/uk/getmondo/payments/a/a/a;
    .locals 12

    .prologue
    const-string v0, "apiValue"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    new-instance v0, Lco/uk/getmondo/payments/a/a/a;

    .line 11
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/payments/ApiDirectDebit;->a()Ljava/lang/String;

    move-result-object v1

    .line 12
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/payments/ApiDirectDebit;->b()Ljava/lang/String;

    move-result-object v2

    .line 13
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/payments/ApiDirectDebit;->c()Ljava/lang/String;

    move-result-object v3

    .line 14
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/payments/ApiDirectDebit;->d()Ljava/lang/String;

    move-result-object v4

    .line 15
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/payments/ApiDirectDebit;->e()Ljava/lang/String;

    move-result-object v5

    .line 16
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/payments/ApiDirectDebit;->g()Ljava/lang/String;

    move-result-object v7

    .line 17
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/payments/ApiDirectDebit;->f()Ljava/lang/String;

    move-result-object v6

    .line 18
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/payments/ApiDirectDebit;->h()Ljava/lang/String;

    move-result-object v8

    .line 19
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/payments/ApiDirectDebit;->i()Z

    move-result v9

    .line 20
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/payments/ApiDirectDebit;->j()Lorg/threeten/bp/LocalDateTime;

    move-result-object v10

    .line 21
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/payments/ApiDirectDebit;->k()Lorg/threeten/bp/LocalDateTime;

    move-result-object v11

    .line 10
    invoke-direct/range {v0 .. v11}, Lco/uk/getmondo/payments/a/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/LocalDateTime;)V

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 7
    check-cast p1, Lco/uk/getmondo/api/model/payments/ApiDirectDebit;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/a/a;->a(Lco/uk/getmondo/api/model/payments/ApiDirectDebit;)Lco/uk/getmondo/payments/a/a/a;

    move-result-object v0

    return-object v0
.end method
