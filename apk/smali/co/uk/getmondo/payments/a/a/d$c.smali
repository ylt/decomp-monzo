.class public final enum Lco/uk/getmondo/payments/a/a/d$c;
.super Ljava/lang/Enum;
.source "PaymentSchedule.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/payments/a/a/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/payments/a/a/d$c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/payments/a/a/d$c;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0012\u0008\u0086\u0001\u0018\u0000 \u00162\u0008\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0016B/\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0005\u0012\n\u0008\u0003\u0010\u0007\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0015\u0010\u0007\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\n\n\u0002\u0010\r\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000fj\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013j\u0002\u0008\u0014j\u0002\u0008\u0015\u00a8\u0006\u0017"
    }
    d2 = {
        "Lco/uk/getmondo/payments/data/model/PaymentSchedule$Interval;",
        "",
        "apiValue",
        "",
        "nameRes",
        "",
        "repeatLabelRes",
        "descriptionRes",
        "(Ljava/lang/String;ILjava/lang/String;IILjava/lang/Integer;)V",
        "getApiValue",
        "()Ljava/lang/String;",
        "getDescriptionRes",
        "()Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "getNameRes",
        "()I",
        "getRepeatLabelRes",
        "ONCE",
        "DAILY",
        "WEEKLY",
        "MONTHLY",
        "YEARLY",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/payments/a/a/d$c;

.field public static final enum b:Lco/uk/getmondo/payments/a/a/d$c;

.field public static final enum c:Lco/uk/getmondo/payments/a/a/d$c;

.field public static final enum d:Lco/uk/getmondo/payments/a/a/d$c;

.field public static final enum e:Lco/uk/getmondo/payments/a/a/d$c;

.field public static final f:Lco/uk/getmondo/payments/a/a/d$c$a;

.field private static final synthetic g:[Lco/uk/getmondo/payments/a/a/d$c;


# instance fields
.field private final h:Ljava/lang/String;

.field private final i:I

.field private final j:I

.field private final k:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    const/16 v7, 0x8

    const/4 v6, 0x0

    const/4 v0, 0x5

    new-array v15, v0, [Lco/uk/getmondo/payments/a/a/d$c;

    new-instance v0, Lco/uk/getmondo/payments/a/a/d$c;

    const-string v1, "ONCE"

    .line 25
    const-string v3, "ONCE"

    .line 26
    const v4, 0x7f0a01f0

    .line 27
    const v5, 0x7f0a01f7

    move-object v8, v6

    .line 25
    invoke-direct/range {v0 .. v8}, Lco/uk/getmondo/payments/a/a/d$c;-><init>(Ljava/lang/String;ILjava/lang/String;IILjava/lang/Integer;ILkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/payments/a/a/d$c;->a:Lco/uk/getmondo/payments/a/a/d$c;

    aput-object v0, v15, v2

    new-instance v0, Lco/uk/getmondo/payments/a/a/d$c;

    const-string v1, "DAILY"

    .line 28
    const-string v3, "DAILY"

    .line 29
    const v4, 0x7f0a01ee

    .line 30
    const v5, 0x7f0a01f5

    move v2, v9

    move-object v8, v6

    .line 28
    invoke-direct/range {v0 .. v8}, Lco/uk/getmondo/payments/a/a/d$c;-><init>(Ljava/lang/String;ILjava/lang/String;IILjava/lang/Integer;ILkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/payments/a/a/d$c;->b:Lco/uk/getmondo/payments/a/a/d$c;

    aput-object v0, v15, v9

    new-instance v0, Lco/uk/getmondo/payments/a/a/d$c;

    const-string v1, "WEEKLY"

    .line 31
    const-string v3, "WEEKLY"

    .line 32
    const v4, 0x7f0a01f1

    .line 33
    const v5, 0x7f0a01f8

    move v2, v10

    move-object v8, v6

    .line 31
    invoke-direct/range {v0 .. v8}, Lco/uk/getmondo/payments/a/a/d$c;-><init>(Ljava/lang/String;ILjava/lang/String;IILjava/lang/Integer;ILkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/payments/a/a/d$c;->c:Lco/uk/getmondo/payments/a/a/d$c;

    aput-object v0, v15, v10

    const/4 v0, 0x3

    new-instance v8, Lco/uk/getmondo/payments/a/a/d$c;

    const-string v9, "MONTHLY"

    const/4 v10, 0x3

    .line 34
    const-string v11, "MONTHLY"

    .line 35
    const v12, 0x7f0a01ef

    .line 36
    const v13, 0x7f0a01f6

    .line 37
    const v1, 0x7f0a01ed

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    .line 34
    invoke-direct/range {v8 .. v14}, Lco/uk/getmondo/payments/a/a/d$c;-><init>(Ljava/lang/String;ILjava/lang/String;IILjava/lang/Integer;)V

    sput-object v8, Lco/uk/getmondo/payments/a/a/d$c;->d:Lco/uk/getmondo/payments/a/a/d$c;

    aput-object v8, v15, v0

    const/4 v9, 0x4

    new-instance v0, Lco/uk/getmondo/payments/a/a/d$c;

    const-string v1, "YEARLY"

    const/4 v2, 0x4

    .line 38
    const-string v3, "YEARLY"

    .line 39
    const v4, 0x7f0a01f2

    .line 40
    const v5, 0x7f0a01f9

    move-object v8, v6

    .line 38
    invoke-direct/range {v0 .. v8}, Lco/uk/getmondo/payments/a/a/d$c;-><init>(Ljava/lang/String;ILjava/lang/String;IILjava/lang/Integer;ILkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/payments/a/a/d$c;->e:Lco/uk/getmondo/payments/a/a/d$c;

    aput-object v0, v15, v9

    sput-object v15, Lco/uk/getmondo/payments/a/a/d$c;->g:[Lco/uk/getmondo/payments/a/a/d$c;

    new-instance v0, Lco/uk/getmondo/payments/a/a/d$c$a;

    invoke-direct {v0, v6}, Lco/uk/getmondo/payments/a/a/d$c$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/payments/a/a/d$c;->f:Lco/uk/getmondo/payments/a/a/d$c$a;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILjava/lang/String;IILjava/lang/Integer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "apiValue"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lco/uk/getmondo/payments/a/a/d$c;->h:Ljava/lang/String;

    iput p4, p0, Lco/uk/getmondo/payments/a/a/d$c;->i:I

    iput p5, p0, Lco/uk/getmondo/payments/a/a/d$c;->j:I

    iput-object p6, p0, Lco/uk/getmondo/payments/a/a/d$c;->k:Ljava/lang/Integer;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILjava/lang/String;IILjava/lang/Integer;ILkotlin/d/b/i;)V
    .locals 7

    .prologue
    and-int/lit8 v0, p7, 0x8

    if-eqz v0, :cond_0

    .line 23
    const/4 v0, 0x0

    check-cast v0, Ljava/lang/Integer;

    move-object v6, v0

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lco/uk/getmondo/payments/a/a/d$c;-><init>(Ljava/lang/String;ILjava/lang/String;IILjava/lang/Integer;)V

    return-void

    :cond_0
    move-object v6, p6

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/payments/a/a/d$c;
    .locals 1

    const-class v0, Lco/uk/getmondo/payments/a/a/d$c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/a/a/d$c;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/payments/a/a/d$c;
    .locals 1

    sget-object v0, Lco/uk/getmondo/payments/a/a/d$c;->g:[Lco/uk/getmondo/payments/a/a/d$c;

    invoke-virtual {v0}, [Lco/uk/getmondo/payments/a/a/d$c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/payments/a/a/d$c;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/d$c;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lco/uk/getmondo/payments/a/a/d$c;->i:I

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lco/uk/getmondo/payments/a/a/d$c;->j:I

    return v0
.end method

.method public final d()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/d$c;->k:Ljava/lang/Integer;

    return-object v0
.end method
