.class public Lco/uk/getmondo/payments/a/a/e;
.super Lio/realm/bc;
.source "PaymentSeries.kt"

# interfaces
.implements Lco/uk/getmondo/payments/a/a/f;
.implements Lio/realm/ar;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/payments/a/a/e$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\t\n\u0002\u0008\u001d\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0008\u0016\u0018\u0000 ;2\u00020\u00012\u00020\u0002:\u0001;B7\u0008\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0004\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rB\u000f\u0008\u0016\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010B\u0005\u00a2\u0006\u0002\u0010\u0011J\u0008\u00100\u001a\u000201H\u0016J\u0013\u00102\u001a\u0002032\u0008\u00104\u001a\u0004\u0018\u000105H\u0096\u0002J\u0008\u00106\u001a\u000201H\u0016J\u0018\u00107\u001a\u0002082\u0006\u00109\u001a\u00020\u000f2\u0006\u0010:\u001a\u000201H\u0016R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0019\u0010\u001a\"\u0004\u0008\u001b\u0010\u001cR$\u0010\t\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\n8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u001d\u0010\u001e\"\u0004\u0008\u001f\u0010 R\u0011\u0010!\u001a\u00020\u00048F\u00a2\u0006\u0006\u001a\u0004\u0008\"\u0010\u001aR\u001e\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008#\u0010\u001a\"\u0004\u0008$\u0010\u001cR$\u0010\u000b\u001a\u00020\u000c2\u0006\u0010%\u001a\u00020\u000c8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008&\u0010\'\"\u0004\u0008(\u0010)R\u001a\u0010\u0007\u001a\u00020\u0008X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008*\u0010+\"\u0004\u0008,\u0010-R\u001a\u0010\u0006\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008.\u0010\u001a\"\u0004\u0008/\u0010\u001c\u00a8\u0006<"
    }
    d2 = {
        "Lco/uk/getmondo/payments/data/model/PaymentSeries;",
        "Lio/realm/RealmObject;",
        "Lco/uk/getmondo/payments/data/model/RecurringPayment;",
        "id",
        "",
        "accountId",
        "scheme",
        "schemaData",
        "Lco/uk/getmondo/payments/data/model/FpsSchemaData;",
        "amount",
        "Lco/uk/getmondo/model/Amount;",
        "paymentSchedule",
        "Lco/uk/getmondo/payments/data/model/PaymentSchedule;",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/payments/data/model/FpsSchemaData;Lco/uk/getmondo/model/Amount;Lco/uk/getmondo/payments/data/model/PaymentSchedule;)V",
        "source",
        "Landroid/os/Parcel;",
        "(Landroid/os/Parcel;)V",
        "()V",
        "_amount",
        "",
        "_currency",
        "_endDate",
        "_intervalType",
        "_nextIterationDate",
        "_startDate",
        "getAccountId",
        "()Ljava/lang/String;",
        "setAccountId",
        "(Ljava/lang/String;)V",
        "getAmount",
        "()Lco/uk/getmondo/model/Amount;",
        "setAmount",
        "(Lco/uk/getmondo/model/Amount;)V",
        "displayName",
        "getDisplayName",
        "getId",
        "setId",
        "value",
        "getPaymentSchedule",
        "()Lco/uk/getmondo/payments/data/model/PaymentSchedule;",
        "setPaymentSchedule",
        "(Lco/uk/getmondo/payments/data/model/PaymentSchedule;)V",
        "getSchemaData",
        "()Lco/uk/getmondo/payments/data/model/FpsSchemaData;",
        "setSchemaData",
        "(Lco/uk/getmondo/payments/data/model/FpsSchemaData;)V",
        "getScheme",
        "setScheme",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "writeToParcel",
        "",
        "dest",
        "flags",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lco/uk/getmondo/payments/a/a/e;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lco/uk/getmondo/payments/a/a/e$a;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Lco/uk/getmondo/payments/a/a/b;

.field private f:J

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    new-instance v0, Lco/uk/getmondo/payments/a/a/e$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/payments/a/a/e$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/payments/a/a/e;->a:Lco/uk/getmondo/payments/a/a/e$a;

    .line 118
    new-instance v0, Lco/uk/getmondo/payments/a/a/e$b;

    invoke-direct {v0}, Lco/uk/getmondo/payments/a/a/e$b;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lco/uk/getmondo/payments/a/a/e;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Lio/realm/bc;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->u_()V

    .line 11
    :cond_0
    const-string v0, ""

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/a/a/e;->a(Ljava/lang/String;)V

    .line 12
    const-string v0, ""

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/a/a/e;->b(Ljava/lang/String;)V

    .line 13
    const-string v0, ""

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/a/a/e;->c(Ljava/lang/String;)V

    .line 14
    new-instance v0, Lco/uk/getmondo/payments/a/a/b;

    const/16 v6, 0x1f

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v7, v1

    invoke-direct/range {v0 .. v7}, Lco/uk/getmondo/payments/a/a/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILkotlin/d/b/i;)V

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/a/a/e;->a(Lco/uk/getmondo/payments/a/a/b;)V

    .line 38
    const-string v0, ""

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/a/a/e;->d(Ljava/lang/String;)V

    .line 39
    const-string v0, ""

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/a/a/e;->e(Ljava/lang/String;)V

    .line 42
    const-string v0, ""

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/a/a/e;->h(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 7

    .prologue
    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    const-string v0, "source.readString()"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    const-string v0, "source.readString()"

    invoke-static {v2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    const-string v0, "source.readString()"

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    const-class v0, Lco/uk/getmondo/payments/a/a/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v4

    const-string v0, "source.readParcelable(Fp\u2026::class.java.classLoader)"

    invoke-static {v4, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lco/uk/getmondo/payments/a/a/b;

    .line 102
    const-class v0, Lco/uk/getmondo/d/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v5

    const-string v0, "source.readParcelable(Am\u2026::class.java.classLoader)"

    invoke-static {v5, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Lco/uk/getmondo/d/c;

    .line 103
    const-class v0, Lco/uk/getmondo/payments/a/a/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v6

    const-string v0, "source.readParcelable(Pa\u2026::class.java.classLoader)"

    invoke-static {v6, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v6, Lco/uk/getmondo/payments/a/a/d;

    move-object v0, p0

    .line 97
    invoke-direct/range {v0 .. v6}, Lco/uk/getmondo/payments/a/a/e;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/payments/a/a/b;Lco/uk/getmondo/d/c;Lco/uk/getmondo/payments/a/a/d;)V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/l;

    invoke-interface {p0}, Lio/realm/internal/l;->u_()V

    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/payments/a/a/b;Lco/uk/getmondo/d/c;Lco/uk/getmondo/payments/a/a/d;)V
    .locals 1

    .prologue
    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountId"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scheme"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "schemaData"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentSchedule"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0}, Lco/uk/getmondo/payments/a/a/e;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->u_()V

    .line 55
    :cond_0
    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/a/a/e;->a(Ljava/lang/String;)V

    .line 56
    invoke-virtual {p0, p2}, Lco/uk/getmondo/payments/a/a/e;->b(Ljava/lang/String;)V

    .line 57
    invoke-virtual {p0, p3}, Lco/uk/getmondo/payments/a/a/e;->c(Ljava/lang/String;)V

    .line 58
    invoke-virtual {p0, p4}, Lco/uk/getmondo/payments/a/a/e;->a(Lco/uk/getmondo/payments/a/a/b;)V

    .line 59
    invoke-virtual {p0, p5}, Lco/uk/getmondo/payments/a/a/e;->a(Lco/uk/getmondo/d/c;)V

    .line 60
    invoke-virtual {p0, p6}, Lco/uk/getmondo/payments/a/a/e;->a(Lco/uk/getmondo/payments/a/a/d;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(J)V
    .locals 1

    iput-wide p1, p0, Lco/uk/getmondo/payments/a/a/e;->f:J

    return-void
.end method

.method public final a(Lco/uk/getmondo/d/c;)V
    .locals 2

    .prologue
    const-string v0, "amount"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-virtual {p1}, Lco/uk/getmondo/d/c;->k()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lco/uk/getmondo/payments/a/a/e;->a(J)V

    .line 19
    invoke-virtual {p1}, Lco/uk/getmondo/d/c;->l()Lco/uk/getmondo/common/i/c;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/common/i/c;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "amount.currency.currencyCode"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/a/a/e;->d(Ljava/lang/String;)V

    .line 20
    return-void
.end method

.method public a(Lco/uk/getmondo/payments/a/a/b;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/a/a/e;->e:Lco/uk/getmondo/payments/a/a/b;

    return-void
.end method

.method public final a(Lco/uk/getmondo/payments/a/a/d;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-virtual {p1}, Lco/uk/getmondo/payments/a/a/d;->b()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "value.startDate.toString()"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/a/a/e;->e(Ljava/lang/String;)V

    .line 32
    invoke-virtual {p1}, Lco/uk/getmondo/payments/a/a/d;->d()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/a/a/e;->g(Ljava/lang/String;)V

    .line 33
    invoke-virtual {p1}, Lco/uk/getmondo/payments/a/a/d;->c()Lco/uk/getmondo/payments/a/a/d$c;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/d$c;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/a/a/e;->h(Ljava/lang/String;)V

    .line 34
    invoke-virtual {p1}, Lco/uk/getmondo/payments/a/a/d;->e()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_0
    invoke-virtual {p0, v1}, Lco/uk/getmondo/payments/a/a/e;->f(Ljava/lang/String;)V

    .line 35
    return-void

    :cond_1
    move-object v0, v1

    .line 32
    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/a/a/e;->b:Ljava/lang/String;

    return-void
.end method

.method public final b()Lco/uk/getmondo/payments/a/a/b;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->i()Lco/uk/getmondo/payments/a/a/b;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/a/a/e;->c:Ljava/lang/String;

    return-void
.end method

.method public final c()Lco/uk/getmondo/d/c;
    .locals 4

    .prologue
    .line 16
    new-instance v0, Lco/uk/getmondo/d/c;

    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->j()J

    move-result-wide v2

    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->k()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Lco/uk/getmondo/d/c;-><init>(JLjava/lang/String;)V

    return-object v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/a/a/e;->d:Ljava/lang/String;

    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->i()Lco/uk/getmondo/payments/a/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/b;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/a/a/e;->g:Ljava/lang/String;

    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lco/uk/getmondo/payments/a/a/d;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 23
    new-instance v3, Lco/uk/getmondo/payments/a/a/d;

    .line 24
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->l()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lorg/threeten/bp/LocalDate;->a(Ljava/lang/CharSequence;)Lorg/threeten/bp/LocalDate;

    move-result-object v4

    const-string v0, "LocalDate.parse(_startDate)"

    invoke-static {v4, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    sget-object v0, Lco/uk/getmondo/payments/a/a/d$c;->f:Lco/uk/getmondo/payments/a/a/d$c$a;

    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/payments/a/a/d$c$a;->a(Ljava/lang/String;)Lco/uk/getmondo/payments/a/a/d$c;

    move-result-object v5

    .line 26
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->n()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lorg/threeten/bp/LocalDate;->a(Ljava/lang/CharSequence;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    move-object v1, v0

    .line 27
    :goto_0
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->m()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lorg/threeten/bp/LocalDate;->a(Ljava/lang/CharSequence;)Lorg/threeten/bp/LocalDate;

    move-result-object v2

    .line 23
    :cond_0
    invoke-direct {v3, v4, v5, v1, v2}, Lco/uk/getmondo/payments/a/a/d;-><init>(Lorg/threeten/bp/LocalDate;Lco/uk/getmondo/payments/a/a/d$c;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)V

    return-object v3

    :cond_1
    move-object v1, v2

    .line 26
    goto :goto_0
.end method

.method public e(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/a/a/e;->h:Ljava/lang/String;

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 64
    move-object v0, p0

    check-cast v0, Lco/uk/getmondo/payments/a/a/e;

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 80
    :goto_0
    return v0

    .line 65
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    :goto_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 67
    :cond_2
    if-nez p1, :cond_3

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.payments.data.model.PaymentSeries"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/payments/a/a/e;

    .line 69
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->f()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/e;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    move v0, v2

    goto :goto_0

    .line 70
    :cond_4
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->g()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/e;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_5

    move v0, v2

    goto :goto_0

    .line 71
    :cond_5
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->h()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/e;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_6

    move v0, v2

    goto :goto_0

    .line 72
    :cond_6
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->i()Lco/uk/getmondo/payments/a/a/b;

    move-result-object v3

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/e;->i()Lco/uk/getmondo/payments/a/a/b;

    move-result-object v0

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_7

    move v0, v2

    goto :goto_0

    .line 73
    :cond_7
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->j()J

    move-result-wide v4

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/e;->j()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-eqz v0, :cond_8

    move v0, v2

    goto/16 :goto_0

    .line 74
    :cond_8
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->k()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/e;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_9

    move v0, v2

    goto/16 :goto_0

    .line 75
    :cond_9
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->l()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/e;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_a

    move v0, v2

    goto/16 :goto_0

    .line 76
    :cond_a
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->m()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/e;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_b

    move v0, v2

    goto/16 :goto_0

    .line 77
    :cond_b
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->n()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/e;->n()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_c

    move v0, v2

    goto/16 :goto_0

    .line 78
    :cond_c
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->o()Ljava/lang/String;

    move-result-object v0

    check-cast p1, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {p1}, Lco/uk/getmondo/payments/a/a/e;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_d

    move v0, v2

    goto/16 :goto_0

    :cond_d
    move v0, v1

    .line 80
    goto/16 :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/e;->b:Ljava/lang/String;

    return-object v0
.end method

.method public f(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/a/a/e;->i:Ljava/lang/String;

    return-void
.end method

.method public g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/e;->c:Ljava/lang/String;

    return-object v0
.end method

.method public g(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/a/a/e;->j:Ljava/lang/String;

    return-void
.end method

.method public h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/e;->d:Ljava/lang/String;

    return-object v0
.end method

.method public h(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/a/a/e;->k:Ljava/lang/String;

    return-void
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 84
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 85
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 86
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 87
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->i()Lco/uk/getmondo/payments/a/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lco/uk/getmondo/payments/a/a/b;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 88
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 89
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 90
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 91
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    .line 92
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->n()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 93
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 94
    return v0

    :cond_1
    move v0, v1

    .line 91
    goto :goto_0
.end method

.method public i()Lco/uk/getmondo/payments/a/a/b;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/e;->e:Lco/uk/getmondo/payments/a/a/b;

    return-object v0
.end method

.method public j()J
    .locals 2

    iget-wide v0, p0, Lco/uk/getmondo/payments/a/a/e;->f:J

    return-wide v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/e;->g:Ljava/lang/String;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/e;->h:Ljava/lang/String;

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/e;->i:Ljava/lang/String;

    return-object v0
.end method

.method public n()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/e;->j:Ljava/lang/String;

    return-object v0
.end method

.method public o()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/e;->k:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    const-string v0, "dest"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 110
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 111
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->i()Lco/uk/getmondo/payments/a/a/b;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 113
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->c()Lco/uk/getmondo/d/c;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 114
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/e;->e()Lco/uk/getmondo/payments/a/a/d;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 115
    return-void
.end method
