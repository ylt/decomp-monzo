.class public Lco/uk/getmondo/payments/a/a/a;
.super Lio/realm/bc;
.source "DirectDebit.kt"

# interfaces
.implements Lco/uk/getmondo/payments/a/a/f;
.implements Lio/realm/n;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/payments/a/a/a$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0008\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008$\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0008\u0016\u0018\u0000 A2\u00020\u00012\u00020\u0002:\u0001ABa\u0008\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0004\u0012\u0006\u0010\u0007\u001a\u00020\u0004\u0012\u0006\u0010\u0008\u001a\u00020\u0004\u0012\u0006\u0010\t\u001a\u00020\u0004\u0012\u0006\u0010\n\u001a\u00020\u0004\u0012\u0006\u0010\u000b\u001a\u00020\u0004\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u000f\u00a2\u0006\u0002\u0010\u0011B\u000f\u0008\u0016\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\u0002\u0010\u0014B\u0005\u00a2\u0006\u0002\u0010\u0015J\u0008\u00107\u001a\u000208H\u0016J\u0013\u00109\u001a\u00020\r2\u0008\u0010:\u001a\u0004\u0018\u00010;H\u0096\u0002J\u0008\u0010<\u001a\u000208H\u0016J\u0018\u0010=\u001a\u00020>2\u0006\u0010?\u001a\u00020\u00132\u0006\u0010@\u001a\u000208H\u0016R\u000e\u0010\u0016\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019\"\u0004\u0008\u001a\u0010\u001bR\u001a\u0010\u000c\u001a\u00020\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001c\u0010\u001d\"\u0004\u0008\u001e\u0010\u001fR$\u0010\u000e\u001a\u00020\u000f2\u0006\u0010 \u001a\u00020\u000f8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008!\u0010\"\"\u0004\u0008#\u0010$R\u0014\u0010%\u001a\u00020\u00048VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008&\u0010\u0019R\u001e\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\'\u0010\u0019\"\u0004\u0008(\u0010\u001bR(\u0010\u0010\u001a\u0004\u0018\u00010\u000f2\u0008\u0010 \u001a\u0004\u0018\u00010\u000f8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008)\u0010\"\"\u0004\u0008*\u0010$R\u001a\u0010\u0007\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008+\u0010\u0019\"\u0004\u0008,\u0010\u001bR\u001a\u0010\u0008\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008-\u0010\u0019\"\u0004\u0008.\u0010\u001bR\u001a\u0010\u0006\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008/\u0010\u0019\"\u0004\u00080\u0010\u001bR\u001a\u0010\u000b\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u00081\u0010\u0019\"\u0004\u00082\u0010\u001bR\u001a\u0010\n\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u00083\u0010\u0019\"\u0004\u00084\u0010\u001bR\u001a\u0010\t\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u00085\u0010\u0019\"\u0004\u00086\u0010\u001b\u00a8\u0006B"
    }
    d2 = {
        "Lco/uk/getmondo/payments/data/model/DirectDebit;",
        "Lio/realm/RealmObject;",
        "Lco/uk/getmondo/payments/data/model/RecurringPayment;",
        "id",
        "",
        "accountId",
        "payerSortCode",
        "payerAccountNumber",
        "payerName",
        "serviceUserNumber",
        "serviceUserName",
        "reference",
        "active",
        "",
        "created",
        "Lorg/threeten/bp/LocalDateTime;",
        "lastCollected",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/LocalDateTime;)V",
        "source",
        "Landroid/os/Parcel;",
        "(Landroid/os/Parcel;)V",
        "()V",
        "_created",
        "_lastCollected",
        "getAccountId",
        "()Ljava/lang/String;",
        "setAccountId",
        "(Ljava/lang/String;)V",
        "getActive",
        "()Z",
        "setActive",
        "(Z)V",
        "value",
        "getCreated",
        "()Lorg/threeten/bp/LocalDateTime;",
        "setCreated",
        "(Lorg/threeten/bp/LocalDateTime;)V",
        "displayName",
        "getDisplayName",
        "getId",
        "setId",
        "getLastCollected",
        "setLastCollected",
        "getPayerAccountNumber",
        "setPayerAccountNumber",
        "getPayerName",
        "setPayerName",
        "getPayerSortCode",
        "setPayerSortCode",
        "getReference",
        "setReference",
        "getServiceUserName",
        "setServiceUserName",
        "getServiceUserNumber",
        "setServiceUserNumber",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hashCode",
        "writeToParcel",
        "",
        "dest",
        "flags",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lco/uk/getmondo/payments/a/a/a;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lco/uk/getmondo/payments/a/a/a$a;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    new-instance v0, Lco/uk/getmondo/payments/a/a/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/payments/a/a/a$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/payments/a/a/a;->a:Lco/uk/getmondo/payments/a/a/a$a;

    .line 129
    new-instance v0, Lco/uk/getmondo/payments/a/a/a$b;

    invoke-direct {v0}, Lco/uk/getmondo/payments/a/a/a$b;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lco/uk/getmondo/payments/a/a/a;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Lio/realm/bc;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->u_()V

    .line 10
    :cond_0
    const-string v0, ""

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/a/a/a;->a(Ljava/lang/String;)V

    .line 11
    const-string v0, ""

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/a/a/a;->b(Ljava/lang/String;)V

    .line 12
    const-string v0, ""

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/a/a/a;->c(Ljava/lang/String;)V

    .line 13
    const-string v0, ""

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/a/a/a;->d(Ljava/lang/String;)V

    .line 14
    const-string v0, ""

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/a/a/a;->e(Ljava/lang/String;)V

    .line 15
    const-string v0, ""

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/a/a/a;->f(Ljava/lang/String;)V

    .line 16
    const-string v0, ""

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/a/a/a;->g(Ljava/lang/String;)V

    .line 17
    const-string v0, ""

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/a/a/a;->h(Ljava/lang/String;)V

    .line 30
    const-string v0, ""

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/a/a/a;->i(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 12

    .prologue
    const/4 v9, 0x1

    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    const-string v0, "source.readString()"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    const-string v0, "source.readString()"

    invoke-static {v2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    const-string v0, "source.readString()"

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    const-string v0, "source.readString()"

    invoke-static {v4, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    const-string v0, "source.readString()"

    invoke-static {v5, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    const-string v0, "source.readString()"

    invoke-static {v6, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    const-string v0, "source.readString()"

    invoke-static {v7, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    const-string v0, "source.readString()"

    invoke-static {v8, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v9, :cond_0

    .line 108
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v10

    if-nez v10, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.threeten.bp.LocalDateTime"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_0
    const/4 v9, 0x0

    goto :goto_0

    .line 108
    :cond_1
    check-cast v10, Lorg/threeten/bp/LocalDateTime;

    .line 109
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v11

    check-cast v11, Lorg/threeten/bp/LocalDateTime;

    move-object v0, p0

    .line 98
    invoke-direct/range {v0 .. v11}, Lco/uk/getmondo/payments/a/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/LocalDateTime;)V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_2

    check-cast p0, Lio/realm/internal/l;

    invoke-interface {p0}, Lio/realm/internal/l;->u_()V

    :cond_2
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/LocalDateTime;)V
    .locals 1

    .prologue
    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountId"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "payerSortCode"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "payerAccountNumber"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "payerName"

    invoke-static {p5, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serviceUserNumber"

    invoke-static {p6, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serviceUserName"

    invoke-static {p7, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reference"

    invoke-static {p8, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "created"

    invoke-static {p10, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-direct {p0}, Lco/uk/getmondo/payments/a/a/a;-><init>()V

    instance-of v0, p0, Lio/realm/internal/l;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lio/realm/internal/l;

    invoke-interface {v0}, Lio/realm/internal/l;->u_()V

    .line 49
    :cond_0
    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/a/a/a;->a(Ljava/lang/String;)V

    .line 50
    invoke-virtual {p0, p2}, Lco/uk/getmondo/payments/a/a/a;->b(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p0, p3}, Lco/uk/getmondo/payments/a/a/a;->c(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p0, p4}, Lco/uk/getmondo/payments/a/a/a;->d(Ljava/lang/String;)V

    .line 53
    invoke-virtual {p0, p5}, Lco/uk/getmondo/payments/a/a/a;->e(Ljava/lang/String;)V

    .line 54
    invoke-virtual {p0, p6}, Lco/uk/getmondo/payments/a/a/a;->f(Ljava/lang/String;)V

    .line 55
    invoke-virtual {p0, p7}, Lco/uk/getmondo/payments/a/a/a;->g(Ljava/lang/String;)V

    .line 56
    invoke-virtual {p0, p8}, Lco/uk/getmondo/payments/a/a/a;->h(Ljava/lang/String;)V

    .line 57
    invoke-virtual {p0, p9}, Lco/uk/getmondo/payments/a/a/a;->a(Z)V

    .line 58
    invoke-virtual {p0, p10}, Lco/uk/getmondo/payments/a/a/a;->a(Lorg/threeten/bp/LocalDateTime;)V

    .line 59
    invoke-virtual {p0, p11}, Lco/uk/getmondo/payments/a/a/a;->b(Lorg/threeten/bp/LocalDateTime;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/a/a/a;->b:Ljava/lang/String;

    return-void
.end method

.method public final a(Lorg/threeten/bp/LocalDateTime;)V
    .locals 2

    .prologue
    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-virtual {p1}, Lorg/threeten/bp/LocalDateTime;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "value.toString()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/a/a/a;->i(Ljava/lang/String;)V

    .line 23
    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lco/uk/getmondo/payments/a/a/a;->j:Z

    return-void
.end method

.method public final b()Lorg/threeten/bp/LocalDateTime;
    .locals 2

    .prologue
    .line 20
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->n()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lorg/threeten/bp/LocalDateTime;->a(Ljava/lang/CharSequence;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    const-string v1, "LocalDateTime.parse(_created)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/a/a/a;->c:Ljava/lang/String;

    return-void
.end method

.method public final b(Lorg/threeten/bp/LocalDateTime;)V
    .locals 1

    .prologue
    .line 27
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/threeten/bp/LocalDateTime;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lco/uk/getmondo/payments/a/a/a;->j(Ljava/lang/String;)V

    .line 28
    return-void

    .line 27
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lorg/threeten/bp/LocalDateTime;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->o()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->o()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lorg/threeten/bp/LocalDateTime;->a(Ljava/lang/CharSequence;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/a/a/a;->d:Ljava/lang/String;

    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->t_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/a/a/a;->e:Ljava/lang/String;

    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x0

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public e(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/a/a/a;->f:Ljava/lang/String;

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 63
    move-object v0, p0

    check-cast v0, Lco/uk/getmondo/payments/a/a/a;

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 80
    :goto_0
    return v0

    .line 64
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    :goto_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 66
    :cond_2
    if-nez p1, :cond_3

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.payments.data.model.DirectDebit"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/payments/a/a/a;

    .line 68
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->e()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/payments/a/a/a;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/a;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    move v0, v2

    goto :goto_0

    .line 69
    :cond_4
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->f()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/payments/a/a/a;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/a;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_5

    move v0, v2

    goto :goto_0

    .line 70
    :cond_5
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->g()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/payments/a/a/a;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/a;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_6

    move v0, v2

    goto :goto_0

    .line 71
    :cond_6
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->h()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/payments/a/a/a;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/a;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_7

    move v0, v2

    goto :goto_0

    .line 72
    :cond_7
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->i()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/payments/a/a/a;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/a;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_8

    move v0, v2

    goto/16 :goto_0

    .line 73
    :cond_8
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->j()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/payments/a/a/a;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/a;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_9

    move v0, v2

    goto/16 :goto_0

    .line 74
    :cond_9
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->t_()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/payments/a/a/a;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/a;->t_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_a

    move v0, v2

    goto/16 :goto_0

    .line 75
    :cond_a
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->l()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/payments/a/a/a;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/a;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_b

    move v0, v2

    goto/16 :goto_0

    .line 76
    :cond_b
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->m()Z

    move-result v3

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/payments/a/a/a;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/a;->m()Z

    move-result v0

    if-eq v3, v0, :cond_c

    move v0, v2

    goto/16 :goto_0

    .line 77
    :cond_c
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->n()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    check-cast v0, Lco/uk/getmondo/payments/a/a/a;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/a;->n()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_d

    move v0, v2

    goto/16 :goto_0

    .line 78
    :cond_d
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->o()Ljava/lang/String;

    move-result-object v0

    check-cast p1, Lco/uk/getmondo/payments/a/a/a;

    invoke-virtual {p1}, Lco/uk/getmondo/payments/a/a/a;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_e

    move v0, v2

    goto/16 :goto_0

    :cond_e
    move v0, v1

    .line 80
    goto/16 :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/a;->c:Ljava/lang/String;

    return-object v0
.end method

.method public f(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/a/a/a;->g:Ljava/lang/String;

    return-void
.end method

.method public g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/a;->d:Ljava/lang/String;

    return-object v0
.end method

.method public g(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/a/a/a;->h:Ljava/lang/String;

    return-void
.end method

.method public h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/a;->e:Ljava/lang/String;

    return-object v0
.end method

.method public h(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/a/a/a;->i:Ljava/lang/String;

    return-void
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 84
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 85
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 86
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 87
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 88
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 90
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->t_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 91
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 92
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->m()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 93
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 94
    mul-int/lit8 v1, v0, 0x1f

    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->o()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v1

    .line 95
    return v0

    .line 94
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/a;->f:Ljava/lang/String;

    return-object v0
.end method

.method public i(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/a/a/a;->k:Ljava/lang/String;

    return-void
.end method

.method public j()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/a;->g:Ljava/lang/String;

    return-object v0
.end method

.method public j(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/a/a/a;->l:Ljava/lang/String;

    return-void
.end method

.method public l()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/a;->i:Ljava/lang/String;

    return-object v0
.end method

.method public m()Z
    .locals 1

    iget-boolean v0, p0, Lco/uk/getmondo/payments/a/a/a;->j:Z

    return v0
.end method

.method public n()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/a;->k:Ljava/lang/String;

    return-object v0
.end method

.method public o()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/a;->l:Ljava/lang/String;

    return-object v0
.end method

.method public t_()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/a;->h:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    const-string v0, "dest"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 116
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 117
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 118
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 119
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 120
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 121
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->t_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 122
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 123
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 124
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->b()Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 125
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/a/a;->c()Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 126
    return-void

    .line 123
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
