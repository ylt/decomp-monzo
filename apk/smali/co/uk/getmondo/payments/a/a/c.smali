.class public final enum Lco/uk/getmondo/payments/a/a/c;
.super Ljava/lang/Enum;
.source "PayeeValidationError.kt"

# interfaces
.implements Lco/uk/getmondo/common/e/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/payments/a/a/c;",
        ">;",
        "Lco/uk/getmondo/common/e/f;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u000b\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002B#\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00080\u0006\u00a2\u0006\u0002\u0010\tJ\u000e\u0010\u000e\u001a\u00020\u00082\u0006\u0010\u000f\u001a\u00020\u0007R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u001d\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00080\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012\u00a8\u0006\u0013"
    }
    d2 = {
        "Lco/uk/getmondo/payments/data/model/PayeeValidationError;",
        "",
        "Lco/uk/getmondo/common/errors/MatchableError;",
        "prefix",
        "",
        "showError",
        "Lkotlin/Function1;",
        "Lco/uk/getmondo/payments/send/bank/payee/PayeeValidationErrorView;",
        "",
        "(Ljava/lang/String;ILjava/lang/String;Lkotlin/jvm/functions/Function1;)V",
        "getPrefix",
        "()Ljava/lang/String;",
        "getShowError",
        "()Lkotlin/jvm/functions/Function1;",
        "displayError",
        "view",
        "BAD_ACCOUNT_NUMBER",
        "BAD_SORT_CODE",
        "MODULUS_CHECK_FAILED",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/payments/a/a/c;

.field public static final enum b:Lco/uk/getmondo/payments/a/a/c;

.field public static final enum c:Lco/uk/getmondo/payments/a/a/c;

.field private static final synthetic d:[Lco/uk/getmondo/payments/a/a/c;


# instance fields
.field private final e:Ljava/lang/String;

.field private final f:Lkotlin/d/a/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/d/a/b",
            "<",
            "Lco/uk/getmondo/payments/send/bank/payee/a;",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x3

    new-array v1, v0, [Lco/uk/getmondo/payments/a/a/c;

    new-instance v2, Lco/uk/getmondo/payments/a/a/c;

    const-string v3, "BAD_ACCOUNT_NUMBER"

    .line 8
    const-string v4, "bad_request.bad_param.account_number"

    sget-object v0, Lco/uk/getmondo/payments/a/a/c$1;->a:Lco/uk/getmondo/payments/a/a/c$1;

    check-cast v0, Lkotlin/d/a/b;

    invoke-direct {v2, v3, v5, v4, v0}, Lco/uk/getmondo/payments/a/a/c;-><init>(Ljava/lang/String;ILjava/lang/String;Lkotlin/d/a/b;)V

    sput-object v2, Lco/uk/getmondo/payments/a/a/c;->a:Lco/uk/getmondo/payments/a/a/c;

    aput-object v2, v1, v5

    new-instance v2, Lco/uk/getmondo/payments/a/a/c;

    const-string v3, "BAD_SORT_CODE"

    .line 9
    const-string v4, "bad_request.bad_param.sort_code"

    sget-object v0, Lco/uk/getmondo/payments/a/a/c$2;->a:Lco/uk/getmondo/payments/a/a/c$2;

    check-cast v0, Lkotlin/d/a/b;

    invoke-direct {v2, v3, v6, v4, v0}, Lco/uk/getmondo/payments/a/a/c;-><init>(Ljava/lang/String;ILjava/lang/String;Lkotlin/d/a/b;)V

    sput-object v2, Lco/uk/getmondo/payments/a/a/c;->b:Lco/uk/getmondo/payments/a/a/c;

    aput-object v2, v1, v6

    new-instance v2, Lco/uk/getmondo/payments/a/a/c;

    const-string v3, "MODULUS_CHECK_FAILED"

    .line 10
    const-string v4, "bad_request.bad_param.modulus_check_failed"

    sget-object v0, Lco/uk/getmondo/payments/a/a/c$3;->a:Lco/uk/getmondo/payments/a/a/c$3;

    check-cast v0, Lkotlin/d/a/b;

    invoke-direct {v2, v3, v7, v4, v0}, Lco/uk/getmondo/payments/a/a/c;-><init>(Ljava/lang/String;ILjava/lang/String;Lkotlin/d/a/b;)V

    sput-object v2, Lco/uk/getmondo/payments/a/a/c;->c:Lco/uk/getmondo/payments/a/a/c;

    aput-object v2, v1, v7

    sput-object v1, Lco/uk/getmondo/payments/a/a/c;->d:[Lco/uk/getmondo/payments/a/a/c;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILjava/lang/String;Lkotlin/d/a/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/d/a/b",
            "<-",
            "Lco/uk/getmondo/payments/send/bank/payee/a;",
            "Lkotlin/n;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "prefix"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "showError"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lco/uk/getmondo/payments/a/a/c;->e:Ljava/lang/String;

    iput-object p4, p0, Lco/uk/getmondo/payments/a/a/c;->f:Lkotlin/d/a/b;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/payments/a/a/c;
    .locals 1

    const-class v0, Lco/uk/getmondo/payments/a/a/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/a/a/c;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/payments/a/a/c;
    .locals 1

    sget-object v0, Lco/uk/getmondo/payments/a/a/c;->d:[Lco/uk/getmondo/payments/a/a/c;

    invoke-virtual {v0}, [Lco/uk/getmondo/payments/a/a/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/payments/a/a/c;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/c;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/payments/send/bank/payee/a;)V
    .locals 1

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/c;->f:Lkotlin/d/a/b;

    invoke-interface {v0, p1}, Lkotlin/d/a/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 14
    return-void
.end method
