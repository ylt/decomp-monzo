.class public final Lco/uk/getmondo/payments/a/a/d;
.super Ljava/lang/Object;
.source "PaymentSchedule.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/payments/a/a/d$c;,
        Lco/uk/getmondo/payments/a/a/d$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0010\u000b\n\u0002\u0008\t\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0005\u0008\u0086\u0008\u0018\u0000 &2\u00020\u0001:\u0002&\'B\u000f\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B/\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\u0006\u0012\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0015\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0008H\u00c6\u0003J\u000b\u0010\u0017\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\u000b\u0010\u0018\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J5\u0010\u0019\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\u00062\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u0006H\u00c6\u0001J\u0008\u0010\u001a\u001a\u00020\u001bH\u0016J\u0013\u0010\u001c\u001a\u00020\u00112\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u00d6\u0003J\t\u0010\u001f\u001a\u00020\u001bH\u00d6\u0001J\t\u0010 \u001a\u00020!H\u00d6\u0001J\u0018\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020\u00032\u0006\u0010%\u001a\u00020\u001bH\u0016R\u0013\u0010\t\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0010\u001a\u00020\u00118F\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0012R\u0013\u0010\n\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\rR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\r\u00a8\u0006("
    }
    d2 = {
        "Lco/uk/getmondo/payments/data/model/PaymentSchedule;",
        "Landroid/os/Parcelable;",
        "source",
        "Landroid/os/Parcel;",
        "(Landroid/os/Parcel;)V",
        "startDate",
        "Lorg/threeten/bp/LocalDate;",
        "interval",
        "Lco/uk/getmondo/payments/data/model/PaymentSchedule$Interval;",
        "endDate",
        "nextIterationDate",
        "(Lorg/threeten/bp/LocalDate;Lco/uk/getmondo/payments/data/model/PaymentSchedule$Interval;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)V",
        "getEndDate",
        "()Lorg/threeten/bp/LocalDate;",
        "getInterval",
        "()Lco/uk/getmondo/payments/data/model/PaymentSchedule$Interval;",
        "isTodayAndOnce",
        "",
        "()Z",
        "getNextIterationDate",
        "getStartDate",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "dest",
        "flags",
        "Companion",
        "Interval",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lco/uk/getmondo/payments/a/a/d;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lco/uk/getmondo/payments/a/a/d$a;


# instance fields
.field private final b:Lorg/threeten/bp/LocalDate;

.field private final c:Lco/uk/getmondo/payments/a/a/d$c;

.field private final d:Lorg/threeten/bp/LocalDate;

.field private final e:Lorg/threeten/bp/LocalDate;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    new-instance v0, Lco/uk/getmondo/payments/a/a/d$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/payments/a/a/d$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/payments/a/a/d;->a:Lco/uk/getmondo/payments/a/a/d$a;

    .line 49
    new-instance v0, Lco/uk/getmondo/payments/a/a/d$b;

    invoke-direct {v0}, Lco/uk/getmondo/payments/a/a/d$b;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lco/uk/getmondo/payments/a/a/d;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.threeten.bp.LocalDate"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lorg/threeten/bp/LocalDate;

    .line 57
    invoke-static {}, Lco/uk/getmondo/payments/a/a/d$c;->values()[Lco/uk/getmondo/payments/a/a/d$c;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    aget-object v3, v1, v2

    .line 58
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lorg/threeten/bp/LocalDate;

    .line 59
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lorg/threeten/bp/LocalDate;

    .line 55
    invoke-direct {p0, v0, v3, v1, v2}, Lco/uk/getmondo/payments/a/a/d;-><init>(Lorg/threeten/bp/LocalDate;Lco/uk/getmondo/payments/a/a/d$c;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)V

    return-void
.end method

.method public constructor <init>(Lorg/threeten/bp/LocalDate;Lco/uk/getmondo/payments/a/a/d$c;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)V
    .locals 1

    .prologue
    const-string v0, "startDate"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interval"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/payments/a/a/d;->b:Lorg/threeten/bp/LocalDate;

    iput-object p2, p0, Lco/uk/getmondo/payments/a/a/d;->c:Lco/uk/getmondo/payments/a/a/d$c;

    iput-object p3, p0, Lco/uk/getmondo/payments/a/a/d;->d:Lorg/threeten/bp/LocalDate;

    iput-object p4, p0, Lco/uk/getmondo/payments/a/a/d;->e:Lorg/threeten/bp/LocalDate;

    return-void
.end method

.method public synthetic constructor <init>(Lorg/threeten/bp/LocalDate;Lco/uk/getmondo/payments/a/a/d$c;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;ILkotlin/d/b/i;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    and-int/lit8 v0, p5, 0x4

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 13
    check-cast v0, Lorg/threeten/bp/LocalDate;

    :goto_0
    and-int/lit8 v2, p5, 0x8

    if-eqz v2, :cond_0

    .line 14
    check-cast v1, Lorg/threeten/bp/LocalDate;

    :goto_1
    invoke-direct {p0, p1, p2, v0, v1}, Lco/uk/getmondo/payments/a/a/d;-><init>(Lorg/threeten/bp/LocalDate;Lco/uk/getmondo/payments/a/a/d$c;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)V

    return-void

    :cond_0
    move-object v1, p4

    goto :goto_1

    :cond_1
    move-object v0, p3

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 17
    iget-object v1, p0, Lco/uk/getmondo/payments/a/a/d;->b:Lorg/threeten/bp/LocalDate;

    invoke-static {}, Lorg/threeten/bp/LocalDate;->a()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/chrono/ChronoLocalDate;

    invoke-virtual {v1, v0}, Lorg/threeten/bp/LocalDate;->d(Lorg/threeten/bp/chrono/ChronoLocalDate;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/d;->c:Lco/uk/getmondo/payments/a/a/d$c;

    sget-object v1, Lco/uk/getmondo/payments/a/a/d$c;->a:Lco/uk/getmondo/payments/a/a/d$c;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lorg/threeten/bp/LocalDate;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/d;->b:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public final c()Lco/uk/getmondo/payments/a/a/d$c;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/d;->c:Lco/uk/getmondo/payments/a/a/d$c;

    return-object v0
.end method

.method public final d()Lorg/threeten/bp/LocalDate;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/d;->d:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lorg/threeten/bp/LocalDate;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/d;->e:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lco/uk/getmondo/payments/a/a/d;

    if-eqz v0, :cond_1

    check-cast p1, Lco/uk/getmondo/payments/a/a/d;

    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/d;->b:Lorg/threeten/bp/LocalDate;

    iget-object v1, p1, Lco/uk/getmondo/payments/a/a/d;->b:Lorg/threeten/bp/LocalDate;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/d;->c:Lco/uk/getmondo/payments/a/a/d$c;

    iget-object v1, p1, Lco/uk/getmondo/payments/a/a/d;->c:Lco/uk/getmondo/payments/a/a/d$c;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/d;->d:Lorg/threeten/bp/LocalDate;

    iget-object v1, p1, Lco/uk/getmondo/payments/a/a/d;->d:Lorg/threeten/bp/LocalDate;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/d;->e:Lorg/threeten/bp/LocalDate;

    iget-object v1, p1, Lco/uk/getmondo/payments/a/a/d;->e:Lorg/threeten/bp/LocalDate;

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/d;->b:Lorg/threeten/bp/LocalDate;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/d;->c:Lco/uk/getmondo/payments/a/a/d$c;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/d;->d:Lorg/threeten/bp/LocalDate;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lco/uk/getmondo/payments/a/a/d;->e:Lorg/threeten/bp/LocalDate;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PaymentSchedule(startDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/payments/a/a/d;->b:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", interval="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/payments/a/a/d;->c:Lco/uk/getmondo/payments/a/a/d$c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", endDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/payments/a/a/d;->d:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", nextIterationDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/payments/a/a/d;->e:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    const-string v0, "dest"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/d;->b:Lorg/threeten/bp/LocalDate;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 66
    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/d;->c:Lco/uk/getmondo/payments/a/a/d$c;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/d$c;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 67
    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/d;->d:Lorg/threeten/bp/LocalDate;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 68
    iget-object v0, p0, Lco/uk/getmondo/payments/a/a/d;->e:Lorg/threeten/bp/LocalDate;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 69
    return-void
.end method
