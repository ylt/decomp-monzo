.class public final Lco/uk/getmondo/payments/a/i;
.super Ljava/lang/Object;
.source "RecurringPaymentsManager.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/payments/a/i$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0007\u0018\u0000 \'2\u00020\u0001:\u0001\'B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0012\u0010\u000b\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000e0\r0\u000cJ\u0012\u0010\u000f\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00100\r0\u000cJ\u0012\u0010\u0011\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00120\r0\u000cJ&\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0018\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u0016J&\u0010\u001a\u001a\u00020\u00142\u0006\u0010\u001b\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0018\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u0016J\u001e\u0010\u001c\u001a\u00020\u00142\u0006\u0010\u001d\u001a\u00020\u00122\u0006\u0010\u0018\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u0016J\u0018\u0010\u001e\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u00020 0\u001f0\u000cJ\u001e\u0010!\u001a\u00020\u00142\u0006\u0010\"\u001a\u00020#2\u0006\u0010\u0018\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u0016J\u0006\u0010$\u001a\u00020\u0014J\u0006\u0010%\u001a\u00020\u0014J\u0006\u0010&\u001a\u00020\u0014R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006("
    }
    d2 = {
        "Lco/uk/getmondo/payments/data/RecurringPaymentsManager;",
        "",
        "paymentSeriesStorage",
        "Lco/uk/getmondo/payments/data/PaymentSeriesStorage;",
        "directDebitsStorage",
        "Lco/uk/getmondo/payments/data/DirectDebitsStorage;",
        "paymentsApi",
        "Lco/uk/getmondo/api/PaymentsApi;",
        "accountService",
        "Lco/uk/getmondo/common/accounts/AccountService;",
        "(Lco/uk/getmondo/payments/data/PaymentSeriesStorage;Lco/uk/getmondo/payments/data/DirectDebitsStorage;Lco/uk/getmondo/api/PaymentsApi;Lco/uk/getmondo/common/accounts/AccountService;)V",
        "allDirectDebits",
        "Lio/reactivex/Observable;",
        "",
        "Lco/uk/getmondo/payments/data/model/DirectDebit;",
        "allPaymentSeries",
        "Lco/uk/getmondo/payments/data/model/PaymentSeries;",
        "allRecurringPayments",
        "Lco/uk/getmondo/payments/data/model/RecurringPayment;",
        "cancelDirectDebit",
        "Lio/reactivex/Completable;",
        "directDebitId",
        "",
        "accountId",
        "pin",
        "idempotencyKey",
        "cancelPaymentSeries",
        "paymentSeriesId",
        "cancelRecurringPayment",
        "recurringPayment",
        "counts",
        "Lkotlin/Pair;",
        "",
        "scheduleBankPayment",
        "bankPayment",
        "Lco/uk/getmondo/payments/send/data/model/BankPayment;",
        "syncAll",
        "syncDirectDebits",
        "syncPaymentSeries",
        "Companion",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/payments/a/i$a;


# instance fields
.field private final b:Lco/uk/getmondo/payments/a/f;

.field private final c:Lco/uk/getmondo/payments/a/b;

.field private final d:Lco/uk/getmondo/api/PaymentsApi;

.field private final e:Lco/uk/getmondo/common/accounts/d;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/uk/getmondo/payments/a/i$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/uk/getmondo/payments/a/i$a;-><init>(Lkotlin/d/b/i;)V

    sput-object v0, Lco/uk/getmondo/payments/a/i;->a:Lco/uk/getmondo/payments/a/i$a;

    return-void
.end method

.method public constructor <init>(Lco/uk/getmondo/payments/a/f;Lco/uk/getmondo/payments/a/b;Lco/uk/getmondo/api/PaymentsApi;Lco/uk/getmondo/common/accounts/d;)V
    .locals 1

    .prologue
    const-string v0, "paymentSeriesStorage"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "directDebitsStorage"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentsApi"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountService"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/payments/a/i;->b:Lco/uk/getmondo/payments/a/f;

    iput-object p2, p0, Lco/uk/getmondo/payments/a/i;->c:Lco/uk/getmondo/payments/a/b;

    iput-object p3, p0, Lco/uk/getmondo/payments/a/i;->d:Lco/uk/getmondo/api/PaymentsApi;

    iput-object p4, p0, Lco/uk/getmondo/payments/a/i;->e:Lco/uk/getmondo/common/accounts/d;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/payments/a/i;)Lco/uk/getmondo/api/PaymentsApi;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lco/uk/getmondo/payments/a/i;->d:Lco/uk/getmondo/api/PaymentsApi;

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/payments/a/i;)Lco/uk/getmondo/payments/a/f;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lco/uk/getmondo/payments/a/i;->b:Lco/uk/getmondo/payments/a/f;

    return-object v0
.end method

.method public static final synthetic c(Lco/uk/getmondo/payments/a/i;)Lco/uk/getmondo/payments/a/b;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lco/uk/getmondo/payments/a/i;->c:Lco/uk/getmondo/payments/a/b;

    return-object v0
.end method


# virtual methods
.method public final a()Lio/reactivex/b;
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lco/uk/getmondo/payments/a/i;->e:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->d()Lio/reactivex/v;

    move-result-object v1

    .line 28
    new-instance v0, Lco/uk/getmondo/payments/a/i$l;

    invoke-direct {v0, p0}, Lco/uk/getmondo/payments/a/i$l;-><init>(Lco/uk/getmondo/payments/a/i;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->a(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v1

    .line 29
    sget-object v0, Lco/uk/getmondo/payments/a/i$m;->a:Lco/uk/getmondo/payments/a/i$m;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v1

    .line 30
    new-instance v0, Lco/uk/getmondo/payments/a/i$n;

    invoke-direct {v0, p0}, Lco/uk/getmondo/payments/a/i$n;-><init>(Lco/uk/getmondo/payments/a/i;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "accountService.accountId\u2026riesStorage.saveAll(it) }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/payments/a/a/f;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;
    .locals 2

    .prologue
    const-string v0, "recurringPayment"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pin"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "idempotencyKey"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    iget-object v0, p0, Lco/uk/getmondo/payments/a/i;->e:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->d()Lio/reactivex/v;

    move-result-object v1

    .line 116
    new-instance v0, Lco/uk/getmondo/payments/a/i$e;

    invoke-direct {v0, p0, p1, p2, p3}, Lco/uk/getmondo/payments/a/i$e;-><init>(Lco/uk/getmondo/payments/a/i;Lco/uk/getmondo/payments/a/a/f;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "accountService.accountId\u2026      }\n                }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/payments/send/data/a/c;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;
    .locals 2

    .prologue
    const-string v0, "bankPayment"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pin"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "idempotencyKey"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    iget-object v0, p0, Lco/uk/getmondo/payments/a/i;->e:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->d()Lio/reactivex/v;

    move-result-object v1

    .line 62
    new-instance v0, Lco/uk/getmondo/payments/a/i$f;

    invoke-direct {v0, p0, p1, p3, p2}, Lco/uk/getmondo/payments/a/i$f;-><init>(Lco/uk/getmondo/payments/a/i;Lco/uk/getmondo/payments/send/data/a/c;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->a(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v1

    .line 84
    sget-object v0, Lco/uk/getmondo/payments/a/i$g;->a:Lco/uk/getmondo/payments/a/i$g;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v1

    .line 85
    new-instance v0, Lco/uk/getmondo/payments/a/i$h;

    invoke-direct {v0, p0}, Lco/uk/getmondo/payments/a/i$h;-><init>(Lco/uk/getmondo/payments/a/i;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->c(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, Lio/reactivex/v;->c()Lio/reactivex/b;

    move-result-object v0

    const-string v1, "accountService.accountId\u2026         .toCompletable()"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;
    .locals 6

    .prologue
    const-string v0, "paymentSeriesId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountId"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pin"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "idempotencyKey"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lco/uk/getmondo/payments/a/i;->d:Lco/uk/getmondo/api/PaymentsApi;

    .line 96
    const-string v3, "pin"

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, Lco/uk/getmondo/api/PaymentsApi;->cancelScheduledPaymentSeries(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v1

    .line 97
    iget-object v0, p0, Lco/uk/getmondo/payments/a/i;->b:Lco/uk/getmondo/payments/a/f;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/payments/a/f;->a(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    check-cast v0, Lio/reactivex/d;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->b(Lio/reactivex/d;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "paymentsApi\n            \u2026.delete(paymentSeriesId))"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final b()Lio/reactivex/b;
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lco/uk/getmondo/payments/a/i;->e:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->d()Lio/reactivex/v;

    move-result-object v1

    .line 35
    new-instance v0, Lco/uk/getmondo/payments/a/i$i;

    invoke-direct {v0, p0}, Lco/uk/getmondo/payments/a/i$i;-><init>(Lco/uk/getmondo/payments/a/i;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->a(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v1

    .line 36
    sget-object v0, Lco/uk/getmondo/payments/a/i$j;->a:Lco/uk/getmondo/payments/a/i$j;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v1

    .line 37
    new-instance v0, Lco/uk/getmondo/payments/a/i$k;

    invoke-direct {v0, p0}, Lco/uk/getmondo/payments/a/i$k;-><init>(Lco/uk/getmondo/payments/a/i;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/v;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "accountService.accountId\u2026bitsStorage.saveAll(it) }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;
    .locals 6

    .prologue
    const-string v0, "directDebitId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountId"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pin"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "idempotencyKey"

    invoke-static {p4, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lco/uk/getmondo/payments/a/i;->d:Lco/uk/getmondo/api/PaymentsApi;

    const-string v3, "pin"

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, Lco/uk/getmondo/api/PaymentsApi;->cancelBacsDirectDebit(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v1

    .line 107
    iget-object v0, p0, Lco/uk/getmondo/payments/a/i;->c:Lco/uk/getmondo/payments/a/b;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/payments/a/b;->a(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    check-cast v0, Lio/reactivex/d;

    invoke-virtual {v1, v0}, Lio/reactivex/b;->b(Lio/reactivex/d;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "paymentsApi.cancelBacsDi\u2026ge.delete(directDebitId))"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final c()Lio/reactivex/b;
    .locals 3

    .prologue
    .line 41
    const/4 v0, 0x2

    new-array v0, v0, [Lio/reactivex/b;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/i;->a()Lio/reactivex/b;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/i;->b()Lio/reactivex/b;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lkotlin/a/m;->b([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lio/reactivex/b;->a(Ljava/lang/Iterable;)Lio/reactivex/b;

    move-result-object v0

    const-string v1, "Completable.mergeDelayEr\u2026s(), syncDirectDebits()))"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final d()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/payments/a/a/e;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lco/uk/getmondo/payments/a/i;->b:Lco/uk/getmondo/payments/a/f;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/f;->a()Lio/reactivex/n;

    move-result-object v1

    .line 46
    sget-object v0, Lco/uk/getmondo/payments/a/i$c;->a:Lco/uk/getmondo/payments/a/i$c;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "paymentSeriesStorage.all\u2026      .map { it.results }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final e()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/payments/a/a/a;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lco/uk/getmondo/payments/a/i;->c:Lco/uk/getmondo/payments/a/b;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/b;->a()Lio/reactivex/n;

    move-result-object v1

    .line 51
    sget-object v0, Lco/uk/getmondo/payments/a/i$b;->a:Lco/uk/getmondo/payments/a/i$b;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "directDebitsStorage.allD\u2026      .map { it.results }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final f()Lio/reactivex/n;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/payments/a/a/f;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 55
    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/i;->d()Lio/reactivex/n;

    move-result-object v0

    check-cast v0, Lio/reactivex/r;

    invoke-virtual {p0}, Lco/uk/getmondo/payments/a/i;->e()Lio/reactivex/n;

    move-result-object v1

    check-cast v1, Lio/reactivex/r;

    .line 56
    sget-object v2, Lco/uk/getmondo/payments/a/i$d;->a:Lco/uk/getmondo/payments/a/i$d;

    check-cast v2, Lio/reactivex/c/c;

    .line 55
    invoke-static {v0, v1, v2}, Lio/reactivex/n;->combineLatest(Lio/reactivex/r;Lio/reactivex/r;Lio/reactivex/c/c;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026hat makes sense\n        )"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final g()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/h",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 126
    iget-object v0, p0, Lco/uk/getmondo/payments/a/i;->c:Lco/uk/getmondo/payments/a/b;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/b;->b()Lio/reactivex/n;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/payments/a/i;->b:Lco/uk/getmondo/payments/a/f;

    invoke-virtual {v1}, Lco/uk/getmondo/payments/a/f;->b()Lio/reactivex/n;

    move-result-object v1

    invoke-static {v0, v1}, Lco/uk/getmondo/common/j/f;->a(Lio/reactivex/n;Lio/reactivex/n;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method
