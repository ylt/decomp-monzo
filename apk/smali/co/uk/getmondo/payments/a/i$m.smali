.class final Lco/uk/getmondo/payments/a/i$m;
.super Ljava/lang/Object;
.source "RecurringPaymentsManager.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/payments/a/i;->a()Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lco/uk/getmondo/payments/data/model/PaymentSeries;",
        "<name for destructuring parameter 0>",
        "Lco/uk/getmondo/api/model/payments/SeriesListResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/payments/a/i$m;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lco/uk/getmondo/payments/a/i$m;

    invoke-direct {v0}, Lco/uk/getmondo/payments/a/i$m;-><init>()V

    sput-object v0, Lco/uk/getmondo/payments/a/i$m;->a:Lco/uk/getmondo/payments/a/i$m;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    check-cast p1, Lco/uk/getmondo/api/model/payments/SeriesListResponse;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/a/i$m;->a(Lco/uk/getmondo/api/model/payments/SeriesListResponse;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lco/uk/getmondo/api/model/payments/SeriesListResponse;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/api/model/payments/SeriesListResponse;",
            ")",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/payments/a/a/e;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/payments/SeriesListResponse;->a()Ljava/util/List;

    move-result-object v0

    .line 29
    check-cast v0, Ljava/lang/Iterable;

    .line 133
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 134
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 135
    check-cast v0, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;

    .line 29
    sget-object v3, Lco/uk/getmondo/payments/a/e;->a:Lco/uk/getmondo/payments/a/e;

    invoke-virtual {v3, v0}, Lco/uk/getmondo/payments/a/e;->a(Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;)Lco/uk/getmondo/payments/a/a/e;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 136
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 29
    return-object v1
.end method
