.class final Lco/uk/getmondo/payments/a/f$f;
.super Ljava/lang/Object;
.source "PaymentSeriesStorage.kt"

# interfaces
.implements Lio/realm/av$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/payments/a/f;->a(Ljava/util/List;)Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "realm",
        "Lio/realm/Realm;",
        "kotlin.jvm.PlatformType",
        "execute"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/a/f$f;->a:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/realm/av;)V
    .locals 3

    .prologue
    .line 18
    iget-object v0, p0, Lco/uk/getmondo/payments/a/f$f;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 19
    const-class v0, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {p1, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    .line 20
    invoke-virtual {v0}, Lio/realm/bf;->f()Lio/realm/bg;

    move-result-object v0

    .line 21
    invoke-virtual {v0}, Lio/realm/bg;->b()Z

    .line 29
    :goto_0
    return-void

    .line 23
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/payments/a/f$f;->a:Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    invoke-virtual {p1, v0}, Lio/realm/av;->a(Ljava/util/Collection;)V

    .line 24
    iget-object v0, p0, Lco/uk/getmondo/payments/a/f$f;->a:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 58
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/m;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 59
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 60
    check-cast v0, Lco/uk/getmondo/payments/a/a/e;

    .line 24
    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/e;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 61
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    .line 24
    nop

    .line 63
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 24
    :cond_2
    check-cast v0, [Ljava/lang/String;

    .line 25
    const-class v1, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {p1, v1}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v1

    .line 26
    invoke-virtual {v1}, Lio/realm/bf;->d()Lio/realm/bf;

    move-result-object v1

    const-string v2, "id"

    invoke-virtual {v1, v2, v0}, Lio/realm/bf;->a(Ljava/lang/String;[Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    .line 27
    invoke-virtual {v0}, Lio/realm/bf;->f()Lio/realm/bg;

    move-result-object v0

    .line 28
    invoke-virtual {v0}, Lio/realm/bg;->b()Z

    goto :goto_0
.end method
