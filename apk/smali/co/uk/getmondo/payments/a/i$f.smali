.class final Lco/uk/getmondo/payments/a/i$f;
.super Ljava/lang/Object;
.source "RecurringPaymentsManager.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/payments/a/i;->a(Lco/uk/getmondo/payments/send/data/a/c;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<TT;",
        "Lio/reactivex/z",
        "<+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lco/uk/getmondo/api/model/payments/SeriesResponse;",
        "accountId",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/payments/a/i;

.field final synthetic b:Lco/uk/getmondo/payments/send/data/a/c;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;


# direct methods
.method constructor <init>(Lco/uk/getmondo/payments/a/i;Lco/uk/getmondo/payments/send/data/a/c;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/a/i$f;->a:Lco/uk/getmondo/payments/a/i;

    iput-object p2, p0, Lco/uk/getmondo/payments/a/i$f;->b:Lco/uk/getmondo/payments/send/data/a/c;

    iput-object p3, p0, Lco/uk/getmondo/payments/a/i$f;->c:Ljava/lang/String;

    iput-object p4, p0, Lco/uk/getmondo/payments/a/i$f;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lio/reactivex/v;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/v",
            "<",
            "Lco/uk/getmondo/api/model/payments/SeriesResponse;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v2, "accountId"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    move-object/from16 v0, p0

    iget-object v2, v0, Lco/uk/getmondo/payments/a/i$f;->b:Lco/uk/getmondo/payments/send/data/a/c;

    invoke-virtual {v2}, Lco/uk/getmondo/payments/send/data/a/c;->e()Lco/uk/getmondo/payments/a/a/d;

    move-result-object v2

    if-nez v2, :cond_0

    .line 64
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Payment must contain scheduling data "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lco/uk/getmondo/payments/a/i$f;->b:Lco/uk/getmondo/payments/send/data/a/c;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    invoke-static {v2}, Lio/reactivex/v;->a(Ljava/lang/Throwable;)Lio/reactivex/v;

    .line 68
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lco/uk/getmondo/payments/a/i$f;->a:Lco/uk/getmondo/payments/a/i;

    invoke-static {v2}, Lco/uk/getmondo/payments/a/i;->a(Lco/uk/getmondo/payments/a/i;)Lco/uk/getmondo/api/PaymentsApi;

    move-result-object v2

    .line 70
    move-object/from16 v0, p0

    iget-object v3, v0, Lco/uk/getmondo/payments/a/i$f;->b:Lco/uk/getmondo/payments/send/data/a/c;

    invoke-virtual {v3}, Lco/uk/getmondo/payments/send/data/a/c;->a()Lco/uk/getmondo/d/c;

    move-result-object v3

    invoke-virtual {v3}, Lco/uk/getmondo/d/c;->k()J

    move-result-wide v4

    .line 71
    move-object/from16 v0, p0

    iget-object v3, v0, Lco/uk/getmondo/payments/a/i$f;->b:Lco/uk/getmondo/payments/send/data/a/c;

    invoke-virtual {v3}, Lco/uk/getmondo/payments/send/data/a/c;->a()Lco/uk/getmondo/d/c;

    move-result-object v3

    invoke-virtual {v3}, Lco/uk/getmondo/d/c;->l()Lco/uk/getmondo/common/i/c;

    move-result-object v3

    invoke-virtual {v3}, Lco/uk/getmondo/common/i/c;->b()Ljava/lang/String;

    move-result-object v6

    const-string v3, "bankPayment.amount.currency.currencyCode"

    invoke-static {v6, v3}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    move-object/from16 v0, p0

    iget-object v3, v0, Lco/uk/getmondo/payments/a/i$f;->b:Lco/uk/getmondo/payments/send/data/a/c;

    invoke-virtual {v3}, Lco/uk/getmondo/payments/send/data/a/c;->c()Lco/uk/getmondo/payments/send/data/a/b;

    move-result-object v3

    invoke-virtual {v3}, Lco/uk/getmondo/payments/send/data/a/b;->e()Ljava/lang/String;

    move-result-object v7

    .line 73
    move-object/from16 v0, p0

    iget-object v3, v0, Lco/uk/getmondo/payments/a/i$f;->b:Lco/uk/getmondo/payments/send/data/a/c;

    invoke-virtual {v3}, Lco/uk/getmondo/payments/send/data/a/c;->c()Lco/uk/getmondo/payments/send/data/a/b;

    move-result-object v3

    invoke-virtual {v3}, Lco/uk/getmondo/payments/send/data/a/b;->d()Ljava/lang/String;

    move-result-object v8

    .line 74
    move-object/from16 v0, p0

    iget-object v3, v0, Lco/uk/getmondo/payments/a/i$f;->b:Lco/uk/getmondo/payments/send/data/a/c;

    invoke-virtual {v3}, Lco/uk/getmondo/payments/send/data/a/c;->c()Lco/uk/getmondo/payments/send/data/a/b;

    move-result-object v3

    invoke-virtual {v3}, Lco/uk/getmondo/payments/send/data/a/b;->b()Ljava/lang/String;

    move-result-object v9

    .line 75
    move-object/from16 v0, p0

    iget-object v10, v0, Lco/uk/getmondo/payments/a/i$f;->c:Ljava/lang/String;

    .line 76
    const-string v11, "pin"

    .line 77
    move-object/from16 v0, p0

    iget-object v12, v0, Lco/uk/getmondo/payments/a/i$f;->d:Ljava/lang/String;

    .line 78
    move-object/from16 v0, p0

    iget-object v3, v0, Lco/uk/getmondo/payments/a/i$f;->b:Lco/uk/getmondo/payments/send/data/a/c;

    invoke-virtual {v3}, Lco/uk/getmondo/payments/send/data/a/c;->b()Ljava/lang/String;

    move-result-object v13

    .line 79
    move-object/from16 v0, p0

    iget-object v3, v0, Lco/uk/getmondo/payments/a/i$f;->b:Lco/uk/getmondo/payments/send/data/a/c;

    invoke-virtual {v3}, Lco/uk/getmondo/payments/send/data/a/c;->e()Lco/uk/getmondo/payments/a/a/d;

    move-result-object v3

    if-nez v3, :cond_1

    invoke-static {}, Lkotlin/d/b/l;->a()V

    :cond_1
    invoke-virtual {v3}, Lco/uk/getmondo/payments/a/a/d;->b()Lorg/threeten/bp/LocalDate;

    move-result-object v14

    .line 80
    move-object/from16 v0, p0

    iget-object v3, v0, Lco/uk/getmondo/payments/a/i$f;->b:Lco/uk/getmondo/payments/send/data/a/c;

    invoke-virtual {v3}, Lco/uk/getmondo/payments/send/data/a/c;->e()Lco/uk/getmondo/payments/a/a/d;

    move-result-object v3

    invoke-virtual {v3}, Lco/uk/getmondo/payments/a/a/d;->c()Lco/uk/getmondo/payments/a/a/d$c;

    move-result-object v3

    invoke-virtual {v3}, Lco/uk/getmondo/payments/a/a/d$c;->a()Ljava/lang/String;

    move-result-object v15

    .line 81
    move-object/from16 v0, p0

    iget-object v3, v0, Lco/uk/getmondo/payments/a/i$f;->b:Lco/uk/getmondo/payments/send/data/a/c;

    invoke-virtual {v3}, Lco/uk/getmondo/payments/send/data/a/c;->e()Lco/uk/getmondo/payments/a/a/d;

    move-result-object v3

    invoke-virtual {v3}, Lco/uk/getmondo/payments/a/a/d;->d()Lorg/threeten/bp/LocalDate;

    move-result-object v16

    move-object/from16 v3, p1

    .line 68
    invoke-interface/range {v2 .. v16}, Lco/uk/getmondo/api/PaymentsApi;->scheduleBankPayment(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/LocalDate;Ljava/lang/String;Lorg/threeten/bp/LocalDate;)Lio/reactivex/v;

    move-result-object v2

    .line 82
    return-object v2
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/a/i$f;->a(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    return-object v0
.end method
