.class final Lco/uk/getmondo/payments/a/i$e;
.super Ljava/lang/Object;
.source "RecurringPaymentsManager.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/payments/a/i;->a(Lco/uk/getmondo/payments/a/a/f;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h",
        "<",
        "Ljava/lang/String;",
        "Lio/reactivex/d;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/CompletableSource;",
        "kotlin.jvm.PlatformType",
        "accountId",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/payments/a/i;

.field final synthetic b:Lco/uk/getmondo/payments/a/a/f;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;


# direct methods
.method constructor <init>(Lco/uk/getmondo/payments/a/i;Lco/uk/getmondo/payments/a/a/f;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/payments/a/i$e;->a:Lco/uk/getmondo/payments/a/i;

    iput-object p2, p0, Lco/uk/getmondo/payments/a/i$e;->b:Lco/uk/getmondo/payments/a/a/f;

    iput-object p3, p0, Lco/uk/getmondo/payments/a/i$e;->c:Ljava/lang/String;

    iput-object p4, p0, Lco/uk/getmondo/payments/a/i$e;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lio/reactivex/d;
    .locals 4

    .prologue
    const-string v0, "accountId"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lco/uk/getmondo/payments/a/i$e;->b:Lco/uk/getmondo/payments/a/a/f;

    .line 118
    instance-of v1, v0, Lco/uk/getmondo/payments/a/a/a;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lco/uk/getmondo/payments/a/i$e;->a:Lco/uk/getmondo/payments/a/i;

    iget-object v0, p0, Lco/uk/getmondo/payments/a/i$e;->b:Lco/uk/getmondo/payments/a/a/f;

    check-cast v0, Lco/uk/getmondo/payments/a/a/a;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/a;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lco/uk/getmondo/payments/a/i$e;->c:Ljava/lang/String;

    iget-object v3, p0, Lco/uk/getmondo/payments/a/i$e;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, p1, v2, v3}, Lco/uk/getmondo/payments/a/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    check-cast v0, Lio/reactivex/d;

    .line 117
    :goto_0
    return-object v0

    .line 119
    :cond_0
    instance-of v0, v0, Lco/uk/getmondo/payments/a/a/e;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lco/uk/getmondo/payments/a/i$e;->a:Lco/uk/getmondo/payments/a/i;

    iget-object v0, p0, Lco/uk/getmondo/payments/a/i$e;->b:Lco/uk/getmondo/payments/a/a/f;

    check-cast v0, Lco/uk/getmondo/payments/a/a/e;

    invoke-virtual {v0}, Lco/uk/getmondo/payments/a/a/e;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lco/uk/getmondo/payments/a/i$e;->c:Ljava/lang/String;

    iget-object v3, p0, Lco/uk/getmondo/payments/a/i$e;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, p1, v2, v3}, Lco/uk/getmondo/payments/a/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    check-cast v0, Lio/reactivex/d;

    goto :goto_0

    .line 120
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported payment type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/payments/a/i$e;->b:Lco/uk/getmondo/payments/a/a/f;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    invoke-static {v0}, Lio/reactivex/b;->a(Ljava/lang/Throwable;)Lio/reactivex/b;

    move-result-object v0

    check-cast v0, Lio/reactivex/d;

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/a/i$e;->a(Ljava/lang/String;)Lio/reactivex/d;

    move-result-object v0

    return-object v0
.end method
