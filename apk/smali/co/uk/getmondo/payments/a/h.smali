.class public final enum Lco/uk/getmondo/payments/a/h;
.super Ljava/lang/Enum;
.source "PinEntryError.kt"

# interfaces
.implements Lco/uk/getmondo/common/e/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/payments/a/h;",
        ">;",
        "Lco/uk/getmondo/common/e/f;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002B\u000f\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007j\u0002\u0008\u0008j\u0002\u0008\t\u00a8\u0006\n"
    }
    d2 = {
        "Lco/uk/getmondo/payments/data/PinEntryError;",
        "",
        "Lco/uk/getmondo/common/errors/MatchableError;",
        "prefix",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "getPrefix",
        "()Ljava/lang/String;",
        "BLOCKED",
        "PIN_INCORRECT",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/payments/a/h;

.field public static final enum b:Lco/uk/getmondo/payments/a/h;

.field private static final synthetic c:[Lco/uk/getmondo/payments/a/h;


# instance fields
.field private final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x2

    new-array v0, v0, [Lco/uk/getmondo/payments/a/h;

    new-instance v1, Lco/uk/getmondo/payments/a/h;

    const-string v2, "BLOCKED"

    .line 6
    const-string v3, "forbidden"

    invoke-direct {v1, v2, v4, v3}, Lco/uk/getmondo/payments/a/h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/payments/a/h;->a:Lco/uk/getmondo/payments/a/h;

    aput-object v1, v0, v4

    new-instance v1, Lco/uk/getmondo/payments/a/h;

    const-string v2, "PIN_INCORRECT"

    .line 7
    const-string v3, "bad_request.bad_param.pin"

    invoke-direct {v1, v2, v5, v3}, Lco/uk/getmondo/payments/a/h;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lco/uk/getmondo/payments/a/h;->b:Lco/uk/getmondo/payments/a/h;

    aput-object v1, v0, v5

    sput-object v0, Lco/uk/getmondo/payments/a/h;->c:[Lco/uk/getmondo/payments/a/h;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "prefix"

    invoke-static {p3, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lco/uk/getmondo/payments/a/h;->d:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/payments/a/h;
    .locals 1

    const-class v0, Lco/uk/getmondo/payments/a/h;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/payments/a/h;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/payments/a/h;
    .locals 1

    sget-object v0, Lco/uk/getmondo/payments/a/h;->c:[Lco/uk/getmondo/payments/a/h;

    invoke-virtual {v0}, [Lco/uk/getmondo/payments/a/h;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/payments/a/h;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5
    iget-object v0, p0, Lco/uk/getmondo/payments/a/h;->d:Ljava/lang/String;

    return-object v0
.end method
