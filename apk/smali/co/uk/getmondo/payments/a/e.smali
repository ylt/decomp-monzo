.class public final Lco/uk/getmondo/payments/a/e;
.super Ljava/lang/Object;
.source "PaymentSeriesMapper.kt"

# interfaces
.implements Lco/uk/getmondo/d/a/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lco/uk/getmondo/d/a/j",
        "<",
        "Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;",
        "Lco/uk/getmondo/payments/a/a/e;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u00c6\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0002H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "Lco/uk/getmondo/payments/data/PaymentSeriesMapper;",
        "Lco/uk/getmondo/model/mapper/Mapper;",
        "Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;",
        "Lco/uk/getmondo/payments/data/model/PaymentSeries;",
        "()V",
        "apply",
        "apiValue",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final a:Lco/uk/getmondo/payments/a/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    new-instance v0, Lco/uk/getmondo/payments/a/e;

    invoke-direct {v0}, Lco/uk/getmondo/payments/a/e;-><init>()V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p0, Lco/uk/getmondo/payments/a/e;

    sput-object p0, Lco/uk/getmondo/payments/a/e;->a:Lco/uk/getmondo/payments/a/e;

    return-void
.end method


# virtual methods
.method public a(Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;)Lco/uk/getmondo/payments/a/a/e;
    .locals 10

    .prologue
    const-string v0, "apiValue"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    sget-object v0, Lco/uk/getmondo/payments/a/d;->a:Lco/uk/getmondo/payments/a/d;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->f()Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/payments/a/d;->a(Lco/uk/getmondo/api/model/payments/ApiPaymentSeries$FpsSchemeData;)Lco/uk/getmondo/payments/a/a/b;

    move-result-object v4

    .line 13
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lco/uk/getmondo/payments/a/a/b;->a(Ljava/lang/String;)V

    .line 14
    new-instance v6, Lco/uk/getmondo/payments/a/a/d;

    .line 15
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->g()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    .line 16
    sget-object v1, Lco/uk/getmondo/payments/a/a/d$c;->f:Lco/uk/getmondo/payments/a/a/d$c$a;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lco/uk/getmondo/payments/a/a/d$c$a;->a(Ljava/lang/String;)Lco/uk/getmondo/payments/a/a/d$c;

    move-result-object v1

    .line 17
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->i()Lorg/threeten/bp/LocalDate;

    move-result-object v2

    .line 18
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->j()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    .line 14
    invoke-direct {v6, v0, v1, v3, v2}, Lco/uk/getmondo/payments/a/a/d;-><init>(Lorg/threeten/bp/LocalDate;Lco/uk/getmondo/payments/a/a/d$c;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)V

    .line 20
    new-instance v0, Lco/uk/getmondo/payments/a/a/e;

    .line 21
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->a()Ljava/lang/String;

    move-result-object v1

    .line 22
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->b()Ljava/lang/String;

    move-result-object v2

    .line 23
    invoke-virtual {p1}, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->e()Ljava/lang/String;

    move-result-object v3

    .line 25
    new-instance v5, Lco/uk/getmondo/d/c;

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->c()J

    move-result-wide v8

    invoke-virtual {p1}, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;->d()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v8, v9, v7}, Lco/uk/getmondo/d/c;-><init>(JLjava/lang/String;)V

    .line 20
    invoke-direct/range {v0 .. v6}, Lco/uk/getmondo/payments/a/a/e;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/payments/a/a/b;Lco/uk/getmondo/d/c;Lco/uk/getmondo/payments/a/a/d;)V

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    check-cast p1, Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/payments/a/e;->a(Lco/uk/getmondo/api/model/payments/ApiPaymentSeries;)Lco/uk/getmondo/payments/a/a/e;

    move-result-object v0

    return-object v0
.end method
