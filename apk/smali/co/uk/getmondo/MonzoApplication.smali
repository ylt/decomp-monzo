.class public Lco/uk/getmondo/MonzoApplication;
.super Landroid/support/e/b;
.source "MonzoApplication.java"


# instance fields
.field a:Lco/uk/getmondo/common/accounts/d;

.field b:Lco/uk/getmondo/b/a;

.field c:Lco/uk/getmondo/common/a/c;

.field d:Lco/uk/getmondo/common/q;

.field e:Lco/uk/getmondo/common/m;

.field f:Lco/uk/getmondo/common/g;

.field g:Lco/uk/getmondo/adjust/a;

.field private final h:Lco/uk/getmondo/common/h/a/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/support/e/b;-><init>()V

    .line 37
    invoke-virtual {p0}, Lco/uk/getmondo/MonzoApplication;->a()Lco/uk/getmondo/common/h/a/a;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/MonzoApplication;->h:Lco/uk/getmondo/common/h/a/a;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lco/uk/getmondo/MonzoApplication;
    .locals 1

    .prologue
    .line 49
    instance-of v0, p0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 50
    check-cast p0, Landroid/app/Activity;

    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    .line 56
    :goto_0
    check-cast v0, Lco/uk/getmondo/MonzoApplication;

    return-object v0

    .line 51
    :cond_0
    instance-of v0, p0, Landroid/app/Service;

    if-eqz v0, :cond_1

    .line 52
    check-cast p0, Landroid/app/Service;

    invoke-virtual {p0}, Landroid/app/Service;->getApplication()Landroid/app/Application;

    move-result-object v0

    goto :goto_0

    .line 54
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    goto :goto_0
.end method


# virtual methods
.method protected a()Lco/uk/getmondo/common/h/a/a;
    .locals 2

    .prologue
    .line 102
    invoke-static {}, Lco/uk/getmondo/common/h/a/o;->Q()Lco/uk/getmondo/common/h/a/o$a;

    move-result-object v0

    new-instance v1, Lco/uk/getmondo/common/h/a/b;

    invoke-direct {v1, p0}, Lco/uk/getmondo/common/h/a/b;-><init>(Landroid/app/Application;)V

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/h/a/o$a;->a(Lco/uk/getmondo/common/h/a/b;)Lco/uk/getmondo/common/h/a/o$a;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/common/h/a/o$a;->a()Lco/uk/getmondo/common/h/a/a;

    move-result-object v0

    return-object v0
.end method

.method public b()Lco/uk/getmondo/common/h/a/a;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lco/uk/getmondo/MonzoApplication;->h:Lco/uk/getmondo/common/h/a/a;

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 61
    invoke-super {p0}, Landroid/support/e/b;->onCreate()V

    .line 63
    invoke-virtual {p0}, Lco/uk/getmondo/MonzoApplication;->b()Lco/uk/getmondo/common/h/a/a;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/a/a;->a(Lco/uk/getmondo/MonzoApplication;)V

    .line 65
    const/4 v0, 0x1

    new-array v0, v0, [Lio/fabric/sdk/android/h;

    const/4 v1, 0x0

    new-instance v2, Lcom/crashlytics/android/Crashlytics;

    invoke-direct {v2}, Lcom/crashlytics/android/Crashlytics;-><init>()V

    aput-object v2, v0, v1

    invoke-static {p0, v0}, Lio/fabric/sdk/android/c;->a(Landroid/content/Context;[Lio/fabric/sdk/android/h;)Lio/fabric/sdk/android/c;

    .line 66
    invoke-static {p0}, Lcom/squareup/a/a;->a(Landroid/app/Application;)Lcom/squareup/a/b;

    .line 67
    invoke-static {p0}, Lcom/b/c/a;->a(Landroid/app/Application;)V

    .line 68
    invoke-static {p0}, Lio/realm/av;->a(Landroid/content/Context;)V

    .line 70
    new-instance v0, Lco/uk/getmondo/common/f;

    invoke-direct {v0}, Lco/uk/getmondo/common/f;-><init>()V

    invoke-static {v0}, Ld/a/a;->a(Ld/a/a$b;)V

    .line 72
    iget-object v0, p0, Lco/uk/getmondo/MonzoApplication;->a:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->b()Lco/uk/getmondo/d/ak;

    move-result-object v0

    .line 73
    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->d()Lco/uk/getmondo/d/ac;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/ac;->b()Ljava/lang/String;

    move-result-object v0

    .line 76
    :goto_0
    invoke-static {v0}, Lco/uk/getmondo/common/k/p;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 77
    iget-object v1, p0, Lco/uk/getmondo/MonzoApplication;->f:Lco/uk/getmondo/common/g;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/g;->a(Ljava/lang/String;)V

    .line 78
    iget-object v1, p0, Lco/uk/getmondo/MonzoApplication;->d:Lco/uk/getmondo/common/q;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/q;->a(Ljava/lang/String;)V

    .line 82
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/MonzoApplication;->b:Lco/uk/getmondo/b/a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/b/a;->a(Landroid/content/Context;)V

    .line 84
    invoke-virtual {p0}, Lco/uk/getmondo/MonzoApplication;->b()Lco/uk/getmondo/common/h/a/a;

    move-result-object v0

    invoke-interface {v0}, Lco/uk/getmondo/common/h/a/a;->i()Lco/uk/getmondo/b/c;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/MonzoApplication;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 86
    iget-object v0, p0, Lco/uk/getmondo/MonzoApplication;->g:Lco/uk/getmondo/adjust/a;

    invoke-virtual {v0}, Lco/uk/getmondo/adjust/a;->a()V

    .line 88
    new-instance v0, Lio/realm/ay$a;

    invoke-direct {v0}, Lio/realm/ay$a;-><init>()V

    invoke-virtual {v0}, Lio/realm/ay$a;->a()Lio/realm/ay$a;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/ay$a;->b()Lio/realm/ay;

    move-result-object v0

    invoke-static {v0}, Lio/realm/av;->b(Lio/realm/ay;)V

    .line 90
    iget-object v0, p0, Lco/uk/getmondo/MonzoApplication;->c:Lco/uk/getmondo/common/a/c;

    invoke-interface {v0}, Lco/uk/getmondo/common/a/c;->a()V

    .line 92
    new-instance v0, Landroid/support/text/emoji/a/a;

    invoke-virtual {p0}, Lco/uk/getmondo/MonzoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/text/emoji/a/a;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Landroid/support/text/emoji/a;->a(Landroid/support/text/emoji/a$c;)Landroid/support/text/emoji/a;

    .line 93
    return-void

    .line 75
    :cond_1
    invoke-virtual {v0}, Lco/uk/getmondo/d/ak;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public onTerminate()V
    .locals 1

    .prologue
    .line 97
    invoke-virtual {p0}, Lco/uk/getmondo/MonzoApplication;->b()Lco/uk/getmondo/common/h/a/a;

    move-result-object v0

    invoke-interface {v0}, Lco/uk/getmondo/common/h/a/a;->i()Lco/uk/getmondo/b/c;

    move-result-object v0

    invoke-virtual {p0, v0}, Lco/uk/getmondo/MonzoApplication;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 98
    invoke-super {p0}, Landroid/support/e/b;->onTerminate()V

    .line 99
    return-void
.end method
