.class public interface abstract Lco/uk/getmondo/pots/b$a;
.super Ljava/lang/Object;
.source "CreatePotPresenter.kt"

# interfaces
.implements Lco/uk/getmondo/common/ui/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/pots/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH&J\u0008\u0010\u000b\u001a\u00020\u0008H&J\u0016\u0010\u000c\u001a\u00020\u00082\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u000eH&R\u0018\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u000f"
    }
    d2 = {
        "Lco/uk/getmondo/pots/CreatePotPresenter$View;",
        "Lco/uk/getmondo/common/ui/MvpView;",
        "potNameClicks",
        "Lio/reactivex/Observable;",
        "Lco/uk/getmondo/pots/data/model/PotName;",
        "getPotNameClicks",
        "()Lio/reactivex/Observable;",
        "openPotStyle",
        "",
        "potCreation",
        "Lco/uk/getmondo/pots/data/model/PotCreation;",
        "showNameInput",
        "showPotNames",
        "potNames",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# virtual methods
.method public abstract a()Lio/reactivex/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/pots/a/a/b;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Lco/uk/getmondo/pots/a/a/a;)V
.end method

.method public abstract a(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/pots/a/a/b;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract b()V
.end method
