.class final Lco/uk/getmondo/pots/a/a/b$a$a;
.super Lkotlin/d/b/m;
.source "PotName.kt"

# interfaces
.implements Lkotlin/d/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/pots/a/a/b$a;->a(Lco/uk/getmondo/common/v;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/d/b/m;",
        "Lkotlin/d/a/a",
        "<",
        "Ljava/util/List",
        "<+",
        "Lco/uk/getmondo/pots/a/a/b;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lco/uk/getmondo/pots/data/model/PotName;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/common/v;


# direct methods
.method constructor <init>(Lco/uk/getmondo/common/v;)V
    .locals 1

    iput-object p1, p0, Lco/uk/getmondo/pots/a/a/b$a$a;->a:Lco/uk/getmondo/common/v;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/d/b/m;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final b()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/pots/a/a/b;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 21
    const/4 v0, 0x7

    new-array v6, v0, [Lco/uk/getmondo/pots/a/a/b;

    .line 22
    const/4 v7, 0x0

    new-instance v0, Lco/uk/getmondo/pots/a/a/b;

    const-string v1, "\ud83d\udcb0"

    iget-object v2, p0, Lco/uk/getmondo/pots/a/a/b$a$a;->a:Lco/uk/getmondo/common/v;

    const v5, 0x7f0a031a

    invoke-virtual {v2, v5}, Lco/uk/getmondo/common/v;->a(I)Ljava/lang/String;

    move-result-object v2

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/pots/a/a/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/pots/a/a/b$b;ILkotlin/d/b/i;)V

    aput-object v0, v6, v7

    .line 23
    const/4 v7, 0x1

    new-instance v0, Lco/uk/getmondo/pots/a/a/b;

    const-string v1, "\u2708\ufe0f"

    iget-object v2, p0, Lco/uk/getmondo/pots/a/a/b$a$a;->a:Lco/uk/getmondo/common/v;

    const v5, 0x7f0a0317

    invoke-virtual {v2, v5}, Lco/uk/getmondo/common/v;->a(I)Ljava/lang/String;

    move-result-object v2

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/pots/a/a/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/pots/a/a/b$b;ILkotlin/d/b/i;)V

    aput-object v0, v6, v7

    .line 24
    const/4 v7, 0x2

    new-instance v0, Lco/uk/getmondo/pots/a/a/b;

    const-string v1, "\ud83c\udf27"

    iget-object v2, p0, Lco/uk/getmondo/pots/a/a/b$a$a;->a:Lco/uk/getmondo/common/v;

    const v5, 0x7f0a0319

    invoke-virtual {v2, v5}, Lco/uk/getmondo/common/v;->a(I)Ljava/lang/String;

    move-result-object v2

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/pots/a/a/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/pots/a/a/b$b;ILkotlin/d/b/i;)V

    aput-object v0, v6, v7

    .line 25
    const/4 v7, 0x3

    new-instance v0, Lco/uk/getmondo/pots/a/a/b;

    const-string v1, "\ud83c\udfe1"

    iget-object v2, p0, Lco/uk/getmondo/pots/a/a/b$a$a;->a:Lco/uk/getmondo/common/v;

    const v5, 0x7f0a0318

    invoke-virtual {v2, v5}, Lco/uk/getmondo/common/v;->a(I)Ljava/lang/String;

    move-result-object v2

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/pots/a/a/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/pots/a/a/b$b;ILkotlin/d/b/i;)V

    aput-object v0, v6, v7

    .line 26
    new-instance v0, Lco/uk/getmondo/pots/a/a/b;

    const-string v1, "\ud83c\udf84"

    iget-object v2, p0, Lco/uk/getmondo/pots/a/a/b$a$a;->a:Lco/uk/getmondo/common/v;

    const v5, 0x7f0a0315

    invoke-virtual {v2, v5}, Lco/uk/getmondo/common/v;->a(I)Ljava/lang/String;

    move-result-object v2

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/pots/a/a/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/pots/a/a/b$b;ILkotlin/d/b/i;)V

    aput-object v0, v6, v4

    .line 27
    const/4 v7, 0x5

    new-instance v0, Lco/uk/getmondo/pots/a/a/b;

    const-string v1, "\ud83c\udfae"

    iget-object v2, p0, Lco/uk/getmondo/pots/a/a/b$a$a;->a:Lco/uk/getmondo/common/v;

    const v5, 0x7f0a0316

    invoke-virtual {v2, v5}, Lco/uk/getmondo/common/v;->a(I)Ljava/lang/String;

    move-result-object v2

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lco/uk/getmondo/pots/a/a/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/pots/a/a/b$b;ILkotlin/d/b/i;)V

    aput-object v0, v6, v7

    .line 28
    const/4 v0, 0x6

    new-instance v1, Lco/uk/getmondo/pots/a/a/b;

    const-string v2, "\ud83d\udd8b\ufe0f"

    iget-object v3, p0, Lco/uk/getmondo/pots/a/a/b$a$a;->a:Lco/uk/getmondo/common/v;

    const v4, 0x7f0a031b

    invoke-virtual {v3, v4}, Lco/uk/getmondo/common/v;->a(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lco/uk/getmondo/pots/a/a/b$b;->b:Lco/uk/getmondo/pots/a/a/b$b;

    invoke-direct {v1, v2, v3, v4}, Lco/uk/getmondo/pots/a/a/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lco/uk/getmondo/pots/a/a/b$b;)V

    aput-object v1, v6, v0

    .line 21
    invoke-static {v6}, Lkotlin/a/m;->b([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 29
    return-object v0
.end method

.method public synthetic v_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lco/uk/getmondo/pots/a/a/b$a$a;->b()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
