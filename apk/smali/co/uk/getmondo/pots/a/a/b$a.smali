.class public final Lco/uk/getmondo/pots/a/a/b$a;
.super Ljava/lang/Object;
.source "PotName.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/pots/a/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007\u00a8\u0006\u0008\u00b2\u0006\u0013\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u008a\u0084\u0002\u00a2\u0006\u0000"
    }
    d2 = {
        "Lco/uk/getmondo/pots/data/model/PotName$Companion;",
        "",
        "()V",
        "values",
        "",
        "Lco/uk/getmondo/pots/data/model/PotName;",
        "resourceProvider",
        "Lco/uk/getmondo/common/ResourceProvider;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/u;

    const-class v3, Lco/uk/getmondo/pots/a/a/b$a;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "values"

    const-string v5, "<v#0>"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/u;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/t;)Lkotlin/reflect/m;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lco/uk/getmondo/pots/a/a/b$a;->a:[Lkotlin/reflect/l;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/d/b/i;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lco/uk/getmondo/pots/a/a/b$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/common/v;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lco/uk/getmondo/common/v;",
            ")",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/pots/a/a/b;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "resourceProvider"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    new-instance v0, Lco/uk/getmondo/pots/a/a/b$a$a;

    invoke-direct {v0, p1}, Lco/uk/getmondo/pots/a/a/b$a$a;-><init>(Lco/uk/getmondo/common/v;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/pots/a/a/b$a;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    .line 31
    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method
