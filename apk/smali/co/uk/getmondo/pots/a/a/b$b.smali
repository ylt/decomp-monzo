.class public final enum Lco/uk/getmondo/pots/a/a/b$b;
.super Ljava/lang/Enum;
.source "PotName.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/pots/a/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/pots/a/a/b$b;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0004\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lco/uk/getmondo/pots/data/model/PotName$Type;",
        "",
        "(Ljava/lang/String;I)V",
        "SUGGESTED",
        "CUSTOM",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/pots/a/a/b$b;

.field public static final enum b:Lco/uk/getmondo/pots/a/a/b$b;

.field private static final synthetic c:[Lco/uk/getmondo/pots/a/a/b$b;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v0, v0, [Lco/uk/getmondo/pots/a/a/b$b;

    new-instance v1, Lco/uk/getmondo/pots/a/a/b$b;

    const-string v2, "SUGGESTED"

    invoke-direct {v1, v2, v3}, Lco/uk/getmondo/pots/a/a/b$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/pots/a/a/b$b;->a:Lco/uk/getmondo/pots/a/a/b$b;

    aput-object v1, v0, v3

    new-instance v1, Lco/uk/getmondo/pots/a/a/b$b;

    const-string v2, "CUSTOM"

    invoke-direct {v1, v2, v4}, Lco/uk/getmondo/pots/a/a/b$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lco/uk/getmondo/pots/a/a/b$b;->b:Lco/uk/getmondo/pots/a/a/b$b;

    aput-object v1, v0, v4

    sput-object v0, Lco/uk/getmondo/pots/a/a/b$b;->c:[Lco/uk/getmondo/pots/a/a/b$b;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/pots/a/a/b$b;
    .locals 1

    const-class v0, Lco/uk/getmondo/pots/a/a/b$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/pots/a/a/b$b;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/pots/a/a/b$b;
    .locals 1

    sget-object v0, Lco/uk/getmondo/pots/a/a/b$b;->c:[Lco/uk/getmondo/pots/a/a/b$b;

    invoke-virtual {v0}, [Lco/uk/getmondo/pots/a/a/b$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/pots/a/a/b$b;

    return-object v0
.end method
