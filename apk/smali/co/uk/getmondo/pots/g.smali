.class public final Lco/uk/getmondo/pots/g;
.super Ljava/lang/Object;
.source "CustomPotNamePresenter_Factory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/pots/f;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/pots/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-class v0, Lco/uk/getmondo/pots/g;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/pots/g;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lb/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/pots/f;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    sget-boolean v0, Lco/uk/getmondo/pots/g;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 18
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/pots/g;->b:Lb/a;

    .line 19
    return-void
.end method

.method public static a(Lb/a;)Lb/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/pots/f;",
            ">;)",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/pots/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    new-instance v0, Lco/uk/getmondo/pots/g;

    invoke-direct {v0, p0}, Lco/uk/getmondo/pots/g;-><init>(Lb/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/pots/f;
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lco/uk/getmondo/pots/g;->b:Lb/a;

    new-instance v1, Lco/uk/getmondo/pots/f;

    invoke-direct {v1}, Lco/uk/getmondo/pots/f;-><init>()V

    invoke-static {v0, v1}, Lb/a/c;->a(Lb/a;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/pots/f;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8
    invoke-virtual {p0}, Lco/uk/getmondo/pots/g;->a()Lco/uk/getmondo/pots/f;

    move-result-object v0

    return-object v0
.end method
