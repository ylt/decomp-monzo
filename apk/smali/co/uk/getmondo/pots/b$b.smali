.class final Lco/uk/getmondo/pots/b$b;
.super Ljava/lang/Object;
.source "CreatePotPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/pots/b;->a(Lco/uk/getmondo/pots/b$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g",
        "<",
        "Lco/uk/getmondo/pots/a/a/b;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lco/uk/getmondo/pots/data/model/PotName;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/pots/b$a;


# direct methods
.method constructor <init>(Lco/uk/getmondo/pots/b$a;)V
    .locals 0

    iput-object p1, p0, Lco/uk/getmondo/pots/b$b;->a:Lco/uk/getmondo/pots/b$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/pots/a/a/b;)V
    .locals 3

    .prologue
    .line 27
    invoke-virtual {p1}, Lco/uk/getmondo/pots/a/a/b;->c()Lco/uk/getmondo/pots/a/a/b$b;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/pots/c;->a:[I

    invoke-virtual {v0}, Lco/uk/getmondo/pots/a/a/b$b;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 30
    :goto_0
    return-void

    .line 28
    :pswitch_0
    iget-object v0, p0, Lco/uk/getmondo/pots/b$b;->a:Lco/uk/getmondo/pots/b$a;

    new-instance v1, Lco/uk/getmondo/pots/a/a/a;

    invoke-virtual {p1}, Lco/uk/getmondo/pots/a/a/b;->b()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lco/uk/getmondo/pots/a/a/a;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lco/uk/getmondo/pots/b$a;->a(Lco/uk/getmondo/pots/a/a/a;)V

    goto :goto_0

    .line 29
    :pswitch_1
    iget-object v0, p0, Lco/uk/getmondo/pots/b$b;->a:Lco/uk/getmondo/pots/b$a;

    invoke-interface {v0}, Lco/uk/getmondo/pots/b$a;->b()V

    goto :goto_0

    .line 27
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 15
    check-cast p1, Lco/uk/getmondo/pots/a/a/b;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/pots/b$b;->a(Lco/uk/getmondo/pots/a/a/b;)V

    return-void
.end method
