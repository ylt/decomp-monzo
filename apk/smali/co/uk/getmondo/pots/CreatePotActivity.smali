.class public final Lco/uk/getmondo/pots/CreatePotActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "CreatePotActivity.kt"

# interfaces
.implements Lco/uk/getmondo/pots/b$a;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0000\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0012\u0010\u0017\u001a\u00020\u00182\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0014J\u0008\u0010\u001b\u001a\u00020\u0018H\u0014J\u0010\u0010\u001c\u001a\u00020\u00182\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\u0008\u0010\u001f\u001a\u00020\u0018H\u0016J\u0016\u0010 \u001a\u00020\u00182\u000c\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\"H\u0016R\u001b\u0010\u0004\u001a\u00020\u00058BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0008\u0010\t\u001a\u0004\u0008\u0006\u0010\u0007R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000f\u0010\u0010R\u001e\u0010\u0011\u001a\u00020\u00128\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\"\u0004\u0008\u0015\u0010\u0016\u00a8\u0006#"
    }
    d2 = {
        "Lco/uk/getmondo/pots/CreatePotActivity;",
        "Lco/uk/getmondo/common/activities/BaseActivity;",
        "Lco/uk/getmondo/pots/CreatePotPresenter$View;",
        "()V",
        "gridLayoutManager",
        "Landroid/support/v7/widget/GridLayoutManager;",
        "getGridLayoutManager",
        "()Landroid/support/v7/widget/GridLayoutManager;",
        "gridLayoutManager$delegate",
        "Lkotlin/Lazy;",
        "potNameAdapter",
        "Lco/uk/getmondo/pots/PotNameAdapter;",
        "potNameClicks",
        "Lio/reactivex/Observable;",
        "Lco/uk/getmondo/pots/data/model/PotName;",
        "getPotNameClicks",
        "()Lio/reactivex/Observable;",
        "presenter",
        "Lco/uk/getmondo/pots/CreatePotPresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/pots/CreatePotPresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/pots/CreatePotPresenter;)V",
        "onCreate",
        "",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onDestroy",
        "openPotStyle",
        "potCreation",
        "Lco/uk/getmondo/pots/data/model/PotCreation;",
        "showNameInput",
        "showPotNames",
        "potNames",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/reflect/l;


# instance fields
.field public b:Lco/uk/getmondo/pots/b;

.field private final c:Lco/uk/getmondo/pots/h;

.field private final e:Lkotlin/c;

.field private f:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Lkotlin/reflect/l;

    const/4 v2, 0x0

    new-instance v0, Lkotlin/d/b/w;

    const-class v3, Lco/uk/getmondo/pots/CreatePotActivity;

    invoke-static {v3}, Lkotlin/d/b/y;->a(Ljava/lang/Class;)Lkotlin/reflect/c;

    move-result-object v3

    const-string v4, "gridLayoutManager"

    const-string v5, "getGridLayoutManager()Landroid/support/v7/widget/GridLayoutManager;"

    invoke-direct {v0, v3, v4, v5}, Lkotlin/d/b/w;-><init>(Lkotlin/reflect/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/d/b/y;->a(Lkotlin/d/b/v;)Lkotlin/reflect/n;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/l;

    aput-object v0, v1, v2

    sput-object v1, Lco/uk/getmondo/pots/CreatePotActivity;->a:[Lkotlin/reflect/l;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 16
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 20
    new-instance v0, Lco/uk/getmondo/pots/h;

    const/4 v1, 0x3

    invoke-direct {v0, v2, v2, v1, v2}, Lco/uk/getmondo/pots/h;-><init>(Ljava/util/List;Lkotlin/d/a/b;ILkotlin/d/b/i;)V

    iput-object v0, p0, Lco/uk/getmondo/pots/CreatePotActivity;->c:Lco/uk/getmondo/pots/h;

    .line 21
    new-instance v0, Lco/uk/getmondo/pots/CreatePotActivity$a;

    invoke-direct {v0, p0}, Lco/uk/getmondo/pots/CreatePotActivity$a;-><init>(Lco/uk/getmondo/pots/CreatePotActivity;)V

    check-cast v0, Lkotlin/d/a/a;

    invoke-static {v0}, Lkotlin/d;->a(Lkotlin/d/a/a;)Lkotlin/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/pots/CreatePotActivity;->e:Lkotlin/c;

    return-void
.end method

.method public static final synthetic a(Lco/uk/getmondo/pots/CreatePotActivity;)Lco/uk/getmondo/pots/h;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lco/uk/getmondo/pots/CreatePotActivity;->c:Lco/uk/getmondo/pots/h;

    return-object v0
.end method

.method public static final synthetic b(Lco/uk/getmondo/pots/CreatePotActivity;)Landroid/support/v7/widget/GridLayoutManager;
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Lco/uk/getmondo/pots/CreatePotActivity;->c()Landroid/support/v7/widget/GridLayoutManager;

    move-result-object v0

    return-object v0
.end method

.method private final c()Landroid/support/v7/widget/GridLayoutManager;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/pots/CreatePotActivity;->e:Lkotlin/c;

    sget-object v1, Lco/uk/getmondo/pots/CreatePotActivity;->a:[Lkotlin/reflect/l;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/GridLayoutManager;

    return-object v0
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/pots/CreatePotActivity;->f:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/pots/CreatePotActivity;->f:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/pots/CreatePotActivity;->f:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/pots/CreatePotActivity;->f:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/pots/a/a/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    new-instance v0, Lco/uk/getmondo/pots/CreatePotActivity$b;

    invoke-direct {v0, p0}, Lco/uk/getmondo/pots/CreatePotActivity$b;-><init>(Lco/uk/getmondo/pots/CreatePotActivity;)V

    check-cast v0, Lio/reactivex/p;

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "Observable.create { emit\u2026stener = null }\n        }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    return-object v0
.end method

.method public a(Lco/uk/getmondo/pots/a/a/a;)V
    .locals 1

    .prologue
    const-string v0, "potCreation"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lco/uk/getmondo/pots/a/a/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-string v0, "potNames"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-direct {p0}, Lco/uk/getmondo/pots/CreatePotActivity;->c()Landroid/support/v7/widget/GridLayoutManager;

    move-result-object v1

    new-instance v0, Lco/uk/getmondo/pots/CreatePotActivity$c;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/pots/CreatePotActivity$c;-><init>(Lco/uk/getmondo/pots/CreatePotActivity;Ljava/util/List;)V

    check-cast v0, Landroid/support/v7/widget/GridLayoutManager$c;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/GridLayoutManager;->a(Landroid/support/v7/widget/GridLayoutManager$c;)V

    .line 59
    iget-object v0, p0, Lco/uk/getmondo/pots/CreatePotActivity;->c:Lco/uk/getmondo/pots/h;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/pots/h;->a(Ljava/util/List;)V

    .line 60
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 63
    new-instance v1, Landroid/content/Intent;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    const-class v2, Lco/uk/getmondo/pots/CustomPotNameActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lco/uk/getmondo/pots/CreatePotActivity;->startActivity(Landroid/content/Intent;)V

    .line 64
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 24
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 25
    const v0, 0x7f05002d

    invoke-virtual {p0, v0}, Lco/uk/getmondo/pots/CreatePotActivity;->setContentView(I)V

    .line 27
    sget v0, Lco/uk/getmondo/c$a;->potNameRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/pots/CreatePotActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-direct {p0}, Lco/uk/getmondo/pots/CreatePotActivity;->c()Landroid/support/v7/widget/GridLayoutManager;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView$h;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$h;)V

    .line 28
    sget v0, Lco/uk/getmondo/c$a;->potNameRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/pots/CreatePotActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lco/uk/getmondo/pots/CreatePotActivity;->c:Lco/uk/getmondo/pots/h;

    check-cast v1, Landroid/support/v7/widget/RecyclerView$a;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 29
    sget v0, Lco/uk/getmondo/c$a;->potNameRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/pots/CreatePotActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 30
    new-instance v1, Lco/uk/getmondo/common/ui/d;

    invoke-virtual {p0}, Lco/uk/getmondo/pots/CreatePotActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0115

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-direct {v1, v0, v2, v3, v4}, Lco/uk/getmondo/common/ui/d;-><init>(IZILkotlin/d/b/i;)V

    .line 31
    sget v0, Lco/uk/getmondo/c$a;->potNameRecyclerView:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/pots/CreatePotActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    check-cast v1, Landroid/support/v7/widget/RecyclerView$g;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$g;)V

    .line 33
    invoke-virtual {p0}, Lco/uk/getmondo/pots/CreatePotActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/pots/CreatePotActivity;)V

    .line 35
    iget-object v0, p0, Lco/uk/getmondo/pots/CreatePotActivity;->b:Lco/uk/getmondo/pots/b;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    check-cast p0, Lco/uk/getmondo/pots/b$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/pots/b;->a(Lco/uk/getmondo/pots/b$a;)V

    .line 36
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lco/uk/getmondo/pots/CreatePotActivity;->b:Lco/uk/getmondo/pots/b;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/pots/b;->b()V

    .line 40
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 41
    return-void
.end method
