.class public final Lco/uk/getmondo/pots/h$a;
.super Landroid/support/v7/widget/RecyclerView$w;
.source "PotNameAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/pots/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u000b"
    }
    d2 = {
        "Lco/uk/getmondo/pots/PotNameAdapter$PotNameViewHolder;",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        "view",
        "Landroid/view/View;",
        "(Lco/uk/getmondo/pots/PotNameAdapter;Landroid/view/View;)V",
        "getView",
        "()Landroid/view/View;",
        "bind",
        "",
        "potName",
        "Lco/uk/getmondo/pots/data/model/PotName;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/pots/h;

.field private final b:Landroid/view/View;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/pots/h;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "view"

    invoke-static {p2, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iput-object p1, p0, Lco/uk/getmondo/pots/h$a;->a:Lco/uk/getmondo/pots/h;

    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$w;-><init>(Landroid/view/View;)V

    iput-object p2, p0, Lco/uk/getmondo/pots/h$a;->b:Landroid/view/View;

    .line 38
    iget-object v1, p0, Lco/uk/getmondo/pots/h$a;->b:Landroid/view/View;

    new-instance v0, Lco/uk/getmondo/pots/h$a$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/pots/h$a$1;-><init>(Lco/uk/getmondo/pots/h$a;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public final a(Lco/uk/getmondo/pots/a/a/b;)V
    .locals 5

    .prologue
    const-string v0, "potName"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    iget-object v0, p0, Lco/uk/getmondo/pots/h$a;->b:Landroid/view/View;

    sget v1, Lco/uk/getmondo/c$a;->potNameLabelText:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/text/emoji/widget/EmojiTextView;

    sget-object v1, Lkotlin/d/b/ab;->a:Lkotlin/d/b/ab;

    const-string v1, "%s %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lco/uk/getmondo/pots/a/a/b;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p1}, Lco/uk/getmondo/pots/a/a/b;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    array-length v3, v2

    invoke-static {v2, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "java.lang.String.format(format, *args)"

    invoke-static {v1, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/text/emoji/widget/EmojiTextView;->setText(Ljava/lang/CharSequence;)V

    .line 44
    invoke-virtual {p0}, Lco/uk/getmondo/pots/h$a;->getAdapterPosition()I

    move-result v0

    iget-object v1, p0, Lco/uk/getmondo/pots/h$a;->a:Lco/uk/getmondo/pots/h;

    invoke-static {v1}, Lco/uk/getmondo/pots/h;->a(Lco/uk/getmondo/pots/h;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/pots/h$a;->getAdapterPosition()I

    move-result v0

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_0

    .line 45
    iget-object v0, p0, Lco/uk/getmondo/pots/h$a;->b:Landroid/view/View;

    sget v1, Lco/uk/getmondo/c$a;->potNameLabelText:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/text/emoji/widget/EmojiTextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/support/text/emoji/widget/EmojiTextView;->setGravity(I)V

    .line 47
    :cond_0
    return-void
.end method
