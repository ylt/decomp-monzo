.class public final Lco/uk/getmondo/pots/CustomPotNameActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "CustomPotNameActivity.kt"

# interfaces
.implements Lco/uk/getmondo/pots/f$a;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0012\u0010\u0014\u001a\u00020\u00122\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0014J\u0008\u0010\u0017\u001a\u00020\u0012H\u0014J\u0008\u0010\u0018\u001a\u00020\u0012H\u0016J\u0010\u0010\u0019\u001a\u00020\u00122\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0010\u0010\u001c\u001a\u00020\u00122\u0006\u0010\u001d\u001a\u00020\u001eH\u0016R\u001a\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008R\u001a\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u0008R\u001e\u0010\u000b\u001a\u00020\u000c8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\r\u0010\u000e\"\u0004\u0008\u000f\u0010\u0010R\u001a\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0013\u0010\u0008\u00a8\u0006\u001f"
    }
    d2 = {
        "Lco/uk/getmondo/pots/CustomPotNameActivity;",
        "Lco/uk/getmondo/common/activities/BaseActivity;",
        "Lco/uk/getmondo/pots/CustomPotNamePresenter$View;",
        "()V",
        "customNameEntered",
        "Lio/reactivex/Observable;",
        "",
        "getCustomNameEntered",
        "()Lio/reactivex/Observable;",
        "potNameChanges",
        "getPotNameChanges",
        "presenter",
        "Lco/uk/getmondo/pots/CustomPotNamePresenter;",
        "getPresenter",
        "()Lco/uk/getmondo/pots/CustomPotNamePresenter;",
        "setPresenter",
        "(Lco/uk/getmondo/pots/CustomPotNamePresenter;)V",
        "suggestionsClicks",
        "",
        "getSuggestionsClicks",
        "onCreate",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onDestroy",
        "openPotNameSuggestions",
        "openPotStyle",
        "potCreation",
        "Lco/uk/getmondo/pots/data/model/PotCreation;",
        "setNextButtonEnabled",
        "enabled",
        "",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field public a:Lco/uk/getmondo/pots/f;

.field private b:Ljava/util/HashMap;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lco/uk/getmondo/pots/CustomPotNameActivity;->b:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/pots/CustomPotNameActivity;->b:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/pots/CustomPotNameActivity;->b:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/j;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/pots/CustomPotNameActivity;->b:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    sget v0, Lco/uk/getmondo/c$a;->potNameEditText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/pots/CustomPotNameActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputEditText;

    .line 52
    invoke-static {v0}, Lcom/b/a/d/e;->c(Landroid/widget/TextView;)Lcom/b/a/a;

    move-result-object v1

    const-string v0, "RxTextView.textChanges(this)"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    sget-object v0, Lco/uk/getmondo/pots/CustomPotNameActivity$b;->a:Lco/uk/getmondo/pots/CustomPotNameActivity$b;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lcom/b/a/a;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "potNameEditText.textChan\u2026s().map { it.toString() }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lco/uk/getmondo/pots/a/a/a;)V
    .locals 1

    .prologue
    const-string v0, "potCreation"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 39
    sget v0, Lco/uk/getmondo/c$a;->nextButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/pots/CustomPotNameActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 40
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Lkotlin/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    sget v0, Lco/uk/getmondo/c$a;->nameSuggestionsText:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/pots/CustomPotNameActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 53
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    return-object v0
.end method

.method public c()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    sget v0, Lco/uk/getmondo/c$a;->nextButton:I

    invoke-virtual {p0, v0}, Lco/uk/getmondo/pots/CustomPotNameActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 54
    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v1

    sget-object v0, Lcom/b/a/a/d;->a:Lcom/b/a/a/d;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    const-string v0, "RxView.clicks(this).map(VoidToUnit)"

    invoke-static {v1, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    new-instance v0, Lco/uk/getmondo/pots/CustomPotNameActivity$a;

    invoke-direct {v0, p0}, Lco/uk/getmondo/pots/CustomPotNameActivity$a;-><init>(Lco/uk/getmondo/pots/CustomPotNameActivity;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    const-string v1, "nextButton.clicks().map \u2026ditText.text.toString() }"

    invoke-static {v0, v1}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public d()V
    .locals 3

    .prologue
    .line 43
    new-instance v1, Landroid/content/Intent;

    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    const-class v2, Lco/uk/getmondo/pots/CreatePotActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 44
    const/high16 v0, 0x4000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 45
    invoke-virtual {p0, v1}, Lco/uk/getmondo/pots/CustomPotNameActivity;->startActivity(Landroid/content/Intent;)V

    .line 46
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 19
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 20
    const v0, 0x7f05002f

    invoke-virtual {p0, v0}, Lco/uk/getmondo/pots/CustomPotNameActivity;->setContentView(I)V

    .line 22
    invoke-virtual {p0}, Lco/uk/getmondo/pots/CustomPotNameActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/pots/CustomPotNameActivity;)V

    .line 24
    iget-object v0, p0, Lco/uk/getmondo/pots/CustomPotNameActivity;->a:Lco/uk/getmondo/pots/f;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    check-cast p0, Lco/uk/getmondo/pots/f$a;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/pots/f;->a(Lco/uk/getmondo/pots/f$a;)V

    .line 25
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Lco/uk/getmondo/pots/CustomPotNameActivity;->a:Lco/uk/getmondo/pots/f;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/d/b/l;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lco/uk/getmondo/pots/f;->b()V

    .line 29
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 30
    return-void
.end method
