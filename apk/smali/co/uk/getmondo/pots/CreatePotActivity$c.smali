.class public final Lco/uk/getmondo/pots/CreatePotActivity$c;
.super Landroid/support/v7/widget/GridLayoutManager$c;
.source "CreatePotActivity.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/pots/CreatePotActivity;->a(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0015\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0004H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "co/uk/getmondo/pots/CreatePotActivity$showPotNames$1",
        "Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;",
        "(Lco/uk/getmondo/pots/CreatePotActivity;Ljava/util/List;)V",
        "getSpanSize",
        "",
        "position",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field final synthetic b:Lco/uk/getmondo/pots/CreatePotActivity;

.field final synthetic c:Ljava/util/List;


# direct methods
.method constructor <init>(Lco/uk/getmondo/pots/CreatePotActivity;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List;",
            ")V"
        }
    .end annotation

    .prologue
    .line 50
    iput-object p1, p0, Lco/uk/getmondo/pots/CreatePotActivity$c;->b:Lco/uk/getmondo/pots/CreatePotActivity;

    iput-object p2, p0, Lco/uk/getmondo/pots/CreatePotActivity$c;->c:Ljava/util/List;

    invoke-direct {p0}, Landroid/support/v7/widget/GridLayoutManager$c;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lco/uk/getmondo/pots/CreatePotActivity$c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    rem-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_0

    .line 53
    iget-object v0, p0, Lco/uk/getmondo/pots/CreatePotActivity$c;->b:Lco/uk/getmondo/pots/CreatePotActivity;

    invoke-static {v0}, Lco/uk/getmondo/pots/CreatePotActivity;->b(Lco/uk/getmondo/pots/CreatePotActivity;)Landroid/support/v7/widget/GridLayoutManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/GridLayoutManager;->c()I

    move-result v0

    .line 52
    :goto_0
    return v0

    .line 55
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/pots/CreatePotActivity$c;->b:Lco/uk/getmondo/pots/CreatePotActivity;

    invoke-static {v0}, Lco/uk/getmondo/pots/CreatePotActivity;->b(Lco/uk/getmondo/pots/CreatePotActivity;)Landroid/support/v7/widget/GridLayoutManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/GridLayoutManager;->c()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    goto :goto_0
.end method
