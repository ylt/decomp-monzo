.class public final Lco/uk/getmondo/pots/f;
.super Lco/uk/getmondo/common/ui/b;
.source "CustomPotNamePresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/pots/f$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/pots/f$a;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0007B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0002H\u0016\u00a8\u0006\u0008"
    }
    d2 = {
        "Lco/uk/getmondo/pots/CustomPotNamePresenter;",
        "Lco/uk/getmondo/common/ui/BasePresenter;",
        "Lco/uk/getmondo/pots/CustomPotNamePresenter$View;",
        "()V",
        "register",
        "",
        "view",
        "View",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 11
    check-cast p1, Lco/uk/getmondo/pots/f$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/pots/f;->a(Lco/uk/getmondo/pots/f$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/pots/f$a;)V
    .locals 3

    .prologue
    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 15
    check-cast v0, Lco/uk/getmondo/common/ui/f;

    invoke-super {p0, v0}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 17
    iget-object v1, p0, Lco/uk/getmondo/pots/f;->b:Lio/reactivex/b/a;

    .line 19
    invoke-interface {p1}, Lco/uk/getmondo/pots/f$a;->a()Lio/reactivex/n;

    move-result-object v2

    .line 18
    sget-object v0, Lco/uk/getmondo/pots/f$b;->a:Lco/uk/getmondo/pots/f$b;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v2

    .line 19
    new-instance v0, Lco/uk/getmondo/pots/f$c;

    invoke-direct {v0, p1}, Lco/uk/getmondo/pots/f$c;-><init>(Lco/uk/getmondo/pots/f$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.potNameChanges\n    \u2026etNextButtonEnabled(it) }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-static {v1, v0}, Lio/reactivex/rxkotlin/a;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)V

    .line 21
    iget-object v1, p0, Lco/uk/getmondo/pots/f;->b:Lio/reactivex/b/a;

    .line 22
    invoke-interface {p1}, Lco/uk/getmondo/pots/f$a;->b()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/pots/f$d;

    invoke-direct {v0, p1}, Lco/uk/getmondo/pots/f$d;-><init>(Lco/uk/getmondo/pots/f$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.suggestionsClicks\n \u2026penPotNameSuggestions() }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-static {v1, v0}, Lio/reactivex/rxkotlin/a;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)V

    .line 24
    iget-object v1, p0, Lco/uk/getmondo/pots/f;->b:Lio/reactivex/b/a;

    .line 25
    invoke-interface {p1}, Lco/uk/getmondo/pots/f$a;->c()Lio/reactivex/n;

    move-result-object v2

    new-instance v0, Lco/uk/getmondo/pots/f$e;

    invoke-direct {v0, p1}, Lco/uk/getmondo/pots/f$e;-><init>(Lco/uk/getmondo/pots/f$a;)V

    check-cast v0, Lio/reactivex/c/g;

    invoke-virtual {v2, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    const-string v2, "view.customNameEntered\n \u2026tStyle(PotCreation(it)) }"

    invoke-static {v0, v2}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-static {v1, v0}, Lio/reactivex/rxkotlin/a;->a(Lio/reactivex/b/a;Lio/reactivex/b/b;)V

    .line 26
    return-void
.end method
