.class public final Lco/uk/getmondo/adjust/i;
.super Ljava/lang/Object;
.source "Adjust_Factory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/adjust/a;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/adjust/g;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/AnalyticsApi;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljavax/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/a/a",
            "<",
            "Lokhttp3/logging/HttpLoggingInterceptor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lco/uk/getmondo/adjust/i;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/adjust/i;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/adjust/g;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/AnalyticsApi;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lokhttp3/logging/HttpLoggingInterceptor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    sget-boolean v0, Lco/uk/getmondo/adjust/i;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 40
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/adjust/i;->b:Ljavax/a/a;

    .line 41
    sget-boolean v0, Lco/uk/getmondo/adjust/i;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 42
    :cond_1
    iput-object p2, p0, Lco/uk/getmondo/adjust/i;->c:Ljavax/a/a;

    .line 43
    sget-boolean v0, Lco/uk/getmondo/adjust/i;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 44
    :cond_2
    iput-object p3, p0, Lco/uk/getmondo/adjust/i;->d:Ljavax/a/a;

    .line 45
    sget-boolean v0, Lco/uk/getmondo/adjust/i;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 46
    :cond_3
    iput-object p4, p0, Lco/uk/getmondo/adjust/i;->e:Ljavax/a/a;

    .line 47
    sget-boolean v0, Lco/uk/getmondo/adjust/i;->a:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 48
    :cond_4
    iput-object p5, p0, Lco/uk/getmondo/adjust/i;->f:Ljavax/a/a;

    .line 49
    sget-boolean v0, Lco/uk/getmondo/adjust/i;->a:Z

    if-nez v0, :cond_5

    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 50
    :cond_5
    iput-object p6, p0, Lco/uk/getmondo/adjust/i;->g:Ljavax/a/a;

    .line 51
    sget-boolean v0, Lco/uk/getmondo/adjust/i;->a:Z

    if-nez v0, :cond_6

    if-nez p7, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 52
    :cond_6
    iput-object p7, p0, Lco/uk/getmondo/adjust/i;->h:Ljavax/a/a;

    .line 53
    return-void
.end method

.method public static a(Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)Lb/a/b;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/a/a",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lio/reactivex/u;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/adjust/g;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lco/uk/getmondo/api/AnalyticsApi;",
            ">;",
            "Ljavax/a/a",
            "<",
            "Lokhttp3/logging/HttpLoggingInterceptor;",
            ">;)",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/adjust/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75
    new-instance v0, Lco/uk/getmondo/adjust/i;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lco/uk/getmondo/adjust/i;-><init>(Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;Ljavax/a/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/adjust/a;
    .locals 8

    .prologue
    .line 57
    new-instance v0, Lco/uk/getmondo/adjust/a;

    iget-object v1, p0, Lco/uk/getmondo/adjust/i;->b:Ljavax/a/a;

    .line 58
    invoke-interface {v1}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iget-object v2, p0, Lco/uk/getmondo/adjust/i;->c:Ljavax/a/a;

    .line 59
    invoke-interface {v2}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/reactivex/u;

    iget-object v3, p0, Lco/uk/getmondo/adjust/i;->d:Ljavax/a/a;

    .line 60
    invoke-interface {v3}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, p0, Lco/uk/getmondo/adjust/i;->e:Ljavax/a/a;

    .line 61
    invoke-interface {v4}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lco/uk/getmondo/adjust/g;

    iget-object v5, p0, Lco/uk/getmondo/adjust/i;->f:Ljavax/a/a;

    .line 62
    invoke-interface {v5}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lokhttp3/OkHttpClient;

    iget-object v6, p0, Lco/uk/getmondo/adjust/i;->g:Ljavax/a/a;

    .line 63
    invoke-interface {v6}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lco/uk/getmondo/api/AnalyticsApi;

    iget-object v7, p0, Lco/uk/getmondo/adjust/i;->h:Ljavax/a/a;

    .line 64
    invoke-interface {v7}, Ljavax/a/a;->b()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lokhttp3/logging/HttpLoggingInterceptor;

    invoke-direct/range {v0 .. v7}, Lco/uk/getmondo/adjust/a;-><init>(Landroid/content/Context;Lio/reactivex/u;Ljava/lang/String;Lco/uk/getmondo/adjust/g;Lokhttp3/OkHttpClient;Lco/uk/getmondo/api/AnalyticsApi;Lokhttp3/logging/HttpLoggingInterceptor;)V

    .line 57
    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lco/uk/getmondo/adjust/i;->a()Lco/uk/getmondo/adjust/a;

    move-result-object v0

    return-object v0
.end method
