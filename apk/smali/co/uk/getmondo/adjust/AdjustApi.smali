.class interface abstract Lco/uk/getmondo/adjust/AdjustApi;
.super Ljava/lang/Object;
.source "AdjustApi.java"


# virtual methods
.method public abstract trackSession(Ljava/util/Map;)Lio/reactivex/b;
    .param p1    # Ljava/util/Map;
        .annotation runtime Lretrofit2/http/FieldMap;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/b;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/FormUrlEncoded;
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Client-SDK: android4.10.4"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "session"
    .end annotation
.end method
