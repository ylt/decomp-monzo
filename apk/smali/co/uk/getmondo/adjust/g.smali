.class public Lco/uk/getmondo/adjust/g;
.super Ljava/lang/Object;
.source "AdjustStorage.java"


# instance fields
.field private a:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const-string v0, "adjust_storage"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/adjust/g;->a:Landroid/content/SharedPreferences;

    .line 19
    return-void
.end method


# virtual methods
.method a(Z)V
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Lco/uk/getmondo/adjust/g;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "KEY_TRACKED"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 27
    return-void
.end method

.method a()Z
    .locals 3

    .prologue
    .line 22
    iget-object v0, p0, Lco/uk/getmondo/adjust/g;->a:Landroid/content/SharedPreferences;

    const-string v1, "KEY_TRACKED"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
