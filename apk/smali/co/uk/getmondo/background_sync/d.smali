.class public Lco/uk/getmondo/background_sync/d;
.super Ljava/lang/Object;
.source "SyncManager.java"


# instance fields
.field private final a:Lco/uk/getmondo/feed/a/d;

.field private final b:Lco/uk/getmondo/a/a;

.field private final c:Lco/uk/getmondo/common/accounts/b;

.field private final d:Lco/uk/getmondo/overdraft/a/c;

.field private final e:Lco/uk/getmondo/overdraft/a/a;

.field private final f:Lio/reactivex/u;


# direct methods
.method public constructor <init>(Lio/reactivex/u;Lco/uk/getmondo/feed/a/d;Lco/uk/getmondo/a/a;Lco/uk/getmondo/common/accounts/b;Lco/uk/getmondo/overdraft/a/c;Lco/uk/getmondo/overdraft/a/a;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p2, p0, Lco/uk/getmondo/background_sync/d;->a:Lco/uk/getmondo/feed/a/d;

    .line 34
    iput-object p3, p0, Lco/uk/getmondo/background_sync/d;->b:Lco/uk/getmondo/a/a;

    .line 35
    iput-object p4, p0, Lco/uk/getmondo/background_sync/d;->c:Lco/uk/getmondo/common/accounts/b;

    .line 36
    iput-object p5, p0, Lco/uk/getmondo/background_sync/d;->d:Lco/uk/getmondo/overdraft/a/c;

    .line 37
    iput-object p6, p0, Lco/uk/getmondo/background_sync/d;->e:Lco/uk/getmondo/overdraft/a/a;

    .line 38
    iput-object p1, p0, Lco/uk/getmondo/background_sync/d;->f:Lio/reactivex/u;

    .line 39
    return-void
.end method

.method private a(Ljava/lang/String;)Lio/reactivex/b;
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lco/uk/getmondo/background_sync/d;->d:Lco/uk/getmondo/overdraft/a/c;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/overdraft/a/c;->b(Ljava/lang/String;)Lio/reactivex/n;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Lio/reactivex/n;->firstElement()Lio/reactivex/h;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/background_sync/e;->a(Lco/uk/getmondo/background_sync/d;)Lio/reactivex/c/h;

    move-result-object v1

    .line 64
    invoke-virtual {v0, v1}, Lio/reactivex/h;->b(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/background_sync/d;->f:Lio/reactivex/u;

    .line 71
    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    .line 62
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/background_sync/d;Lco/uk/getmondo/d/w;)Lio/reactivex/d;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 65
    invoke-virtual {p1}, Lco/uk/getmondo/d/w;->d()Lco/uk/getmondo/d/c;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/d/c;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lco/uk/getmondo/background_sync/d;->a:Lco/uk/getmondo/feed/a/d;

    const-string v1, "overdraft_charges_item_id"

    invoke-virtual {v0, v1}, Lco/uk/getmondo/feed/a/d;->c(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    .line 68
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/background_sync/d;->a:Lco/uk/getmondo/feed/a/d;

    iget-object v1, p0, Lco/uk/getmondo/background_sync/d;->e:Lco/uk/getmondo/overdraft/a/a;

    invoke-virtual {v1, p1}, Lco/uk/getmondo/overdraft/a/a;->a(Lco/uk/getmondo/d/w;)Lco/uk/getmondo/d/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/feed/a/d;->a(Lco/uk/getmondo/d/m;)Lio/reactivex/b;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a()Lio/reactivex/b;
    .locals 3

    .prologue
    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 43
    iget-object v1, p0, Lco/uk/getmondo/background_sync/d;->c:Lco/uk/getmondo/common/accounts/b;

    invoke-virtual {v1}, Lco/uk/getmondo/common/accounts/b;->c()Ljava/lang/String;

    move-result-object v1

    .line 44
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    iget-object v2, p0, Lco/uk/getmondo/background_sync/d;->c:Lco/uk/getmondo/common/accounts/b;

    invoke-virtual {v2}, Lco/uk/getmondo/common/accounts/b;->d()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 46
    iget-object v2, p0, Lco/uk/getmondo/background_sync/d;->c:Lco/uk/getmondo/common/accounts/b;

    invoke-virtual {v2}, Lco/uk/getmondo/common/accounts/b;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    :cond_0
    iget-object v2, p0, Lco/uk/getmondo/background_sync/d;->a:Lco/uk/getmondo/feed/a/d;

    invoke-virtual {v2, v0}, Lco/uk/getmondo/feed/a/d;->a(Ljava/util/List;)Lio/reactivex/b;

    move-result-object v0

    iget-object v2, p0, Lco/uk/getmondo/background_sync/d;->b:Lco/uk/getmondo/a/a;

    .line 49
    invoke-virtual {v2, v1}, Lco/uk/getmondo/a/a;->b(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v2

    invoke-virtual {v0, v2}, Lio/reactivex/b;->d(Lio/reactivex/d;)Lio/reactivex/b;

    move-result-object v0

    .line 50
    iget-object v2, p0, Lco/uk/getmondo/background_sync/d;->c:Lco/uk/getmondo/common/accounts/b;

    invoke-virtual {v2}, Lco/uk/getmondo/common/accounts/b;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 51
    iget-object v2, p0, Lco/uk/getmondo/background_sync/d;->d:Lco/uk/getmondo/overdraft/a/c;

    invoke-virtual {v2, v1}, Lco/uk/getmondo/overdraft/a/c;->a(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v2

    invoke-virtual {v0, v2}, Lio/reactivex/b;->d(Lio/reactivex/d;)Lio/reactivex/b;

    move-result-object v0

    .line 52
    invoke-direct {p0, v1}, Lco/uk/getmondo/background_sync/d;->a(Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/d;)Lio/reactivex/b;

    move-result-object v0

    .line 54
    :cond_1
    return-object v0
.end method
