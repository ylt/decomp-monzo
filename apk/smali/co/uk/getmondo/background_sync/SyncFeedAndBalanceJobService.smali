.class public Lco/uk/getmondo/background_sync/SyncFeedAndBalanceJobService;
.super Landroid/app/job/JobService;
.source "SyncFeedAndBalanceJobService.java"


# instance fields
.field a:Lio/reactivex/u;

.field b:Lco/uk/getmondo/background_sync/d;

.field private final c:Lio/reactivex/b/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    .line 27
    new-instance v0, Lio/reactivex/b/a;

    invoke-direct {v0}, Lio/reactivex/b/a;-><init>()V

    iput-object v0, p0, Lco/uk/getmondo/background_sync/SyncFeedAndBalanceJobService;->c:Lio/reactivex/b/a;

    return-void
.end method

.method private a(Landroid/app/job/JobParameters;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 59
    .line 60
    instance-of v0, p2, Lco/uk/getmondo/api/ApiException;

    if-eqz v0, :cond_0

    move-object v0, p2

    .line 61
    check-cast v0, Lco/uk/getmondo/api/ApiException;

    .line 62
    invoke-virtual {v0}, Lco/uk/getmondo/api/ApiException;->b()I

    move-result v0

    const/16 v3, 0x1f4

    if-lt v0, v3, :cond_2

    move v0, v1

    .line 70
    :goto_0
    if-eqz v0, :cond_1

    .line 71
    const-string v1, "Error with background refresh, rescheduling"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ld/a/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 76
    :goto_1
    invoke-virtual {p0, p1, v0}, Lco/uk/getmondo/background_sync/SyncFeedAndBalanceJobService;->jobFinished(Landroid/app/job/JobParameters;Z)V

    .line 77
    return-void

    .line 65
    :cond_0
    invoke-static {p2}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-nez v0, :cond_2

    instance-of v0, p2, Lio/realm/internal/IOException;

    if-eqz v0, :cond_2

    .line 67
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v3, "Rescheduling background refresh"

    invoke-direct {v0, v3, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v0}, Ld/a/a;->a(Ljava/lang/Throwable;)V

    move v0, v1

    goto :goto_0

    .line 74
    :cond_1
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Giving up trying to perform background refresh"

    invoke-direct {v1, v2, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v1}, Ld/a/a;->a(Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 30
    new-instance v1, Landroid/content/ComponentName;

    const-class v0, Lco/uk/getmondo/background_sync/SyncFeedAndBalanceJobService;

    invoke-direct {v1, p0, v0}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 32
    const-string v0, "Creating background refresher job"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Ld/a/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 33
    const-string v0, "jobscheduler"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    .line 34
    new-instance v2, Landroid/app/job/JobInfo$Builder;

    const/16 v3, 0x64

    invoke-direct {v2, v3, v1}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    const/4 v1, 0x1

    .line 35
    invoke-virtual {v2, v1}, Landroid/app/job/JobInfo$Builder;->setRequiredNetworkType(I)Landroid/app/job/JobInfo$Builder;

    move-result-object v1

    .line 36
    invoke-virtual {v1}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v1

    .line 34
    invoke-virtual {v0, v1}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I

    .line 37
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/background_sync/SyncFeedAndBalanceJobService;Landroid/app/job/JobParameters;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 52
    const-string v0, "Feed and balance synced successfully"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ld/a/a;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 53
    invoke-virtual {p0, p1, v2}, Lco/uk/getmondo/background_sync/SyncFeedAndBalanceJobService;->jobFinished(Landroid/app/job/JobParameters;Z)V

    .line 54
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/background_sync/SyncFeedAndBalanceJobService;Landroid/app/job/JobParameters;Ljava/lang/Throwable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lco/uk/getmondo/background_sync/SyncFeedAndBalanceJobService;->a(Landroid/app/job/JobParameters;Ljava/lang/Throwable;)V

    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 1

    .prologue
    .line 41
    invoke-super {p0}, Landroid/app/job/JobService;->onCreate()V

    .line 43
    invoke-static {p0}, Lco/uk/getmondo/MonzoApplication;->a(Landroid/content/Context;)Lco/uk/getmondo/MonzoApplication;

    move-result-object v0

    invoke-virtual {v0}, Lco/uk/getmondo/MonzoApplication;->b()Lco/uk/getmondo/common/h/a/a;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/a/a;->a(Lco/uk/getmondo/background_sync/SyncFeedAndBalanceJobService;)V

    .line 44
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lco/uk/getmondo/background_sync/SyncFeedAndBalanceJobService;->c:Lio/reactivex/b/a;

    invoke-virtual {v0}, Lio/reactivex/b/a;->a()V

    .line 89
    invoke-super {p0}, Landroid/app/job/JobService;->onDestroy()V

    .line 90
    return-void
.end method

.method public onStartJob(Landroid/app/job/JobParameters;)Z
    .locals 4

    .prologue
    .line 48
    const-string v0, "Background sync job triggered"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ld/a/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 49
    iget-object v0, p0, Lco/uk/getmondo/background_sync/SyncFeedAndBalanceJobService;->c:Lio/reactivex/b/a;

    iget-object v1, p0, Lco/uk/getmondo/background_sync/SyncFeedAndBalanceJobService;->b:Lco/uk/getmondo/background_sync/d;

    invoke-virtual {v1}, Lco/uk/getmondo/background_sync/d;->a()Lio/reactivex/b;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/background_sync/SyncFeedAndBalanceJobService;->a:Lio/reactivex/u;

    .line 50
    invoke-virtual {v1, v2}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v1

    invoke-static {p0, p1}, Lco/uk/getmondo/background_sync/a;->a(Lco/uk/getmondo/background_sync/SyncFeedAndBalanceJobService;Landroid/app/job/JobParameters;)Lio/reactivex/c/a;

    move-result-object v2

    invoke-static {p0, p1}, Lco/uk/getmondo/background_sync/b;->a(Lco/uk/getmondo/background_sync/SyncFeedAndBalanceJobService;Landroid/app/job/JobParameters;)Lio/reactivex/c/g;

    move-result-object v3

    .line 51
    invoke-virtual {v1, v2, v3}, Lio/reactivex/b;->a(Lio/reactivex/c/a;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v1

    .line 49
    invoke-virtual {v0, v1}, Lio/reactivex/b/a;->a(Lio/reactivex/b/b;)Z

    .line 55
    const/4 v0, 0x1

    return v0
.end method

.method public onStopJob(Landroid/app/job/JobParameters;)Z
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lco/uk/getmondo/background_sync/SyncFeedAndBalanceJobService;->c:Lio/reactivex/b/a;

    invoke-virtual {v0}, Lio/reactivex/b/a;->a()V

    .line 83
    const/4 v0, 0x1

    return v0
.end method
