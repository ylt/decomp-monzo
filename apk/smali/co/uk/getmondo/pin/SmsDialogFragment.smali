.class public Lco/uk/getmondo/pin/SmsDialogFragment;
.super Landroid/support/v4/app/i;
.source "SmsDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/pin/SmsDialogFragment$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Lbutterknife/Unbinder;

.field private d:Lco/uk/getmondo/pin/SmsDialogFragment$a;

.field textMeButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1102c7
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lco/uk/getmondo/pin/SmsDialogFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lco/uk/getmondo/pin/SmsDialogFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v4/app/i;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lco/uk/getmondo/pin/SmsDialogFragment;
    .locals 3

    .prologue
    .line 33
    new-instance v0, Lco/uk/getmondo/pin/SmsDialogFragment;

    invoke-direct {v0}, Lco/uk/getmondo/pin/SmsDialogFragment;-><init>()V

    .line 34
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 35
    const-string v2, "phone_number"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    invoke-virtual {v0, v1}, Lco/uk/getmondo/pin/SmsDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 37
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lco/uk/getmondo/pin/SmsDialogFragment;->setCancelable(Z)V

    .line 38
    return-object v0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/pin/SmsDialogFragment$a;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lco/uk/getmondo/pin/SmsDialogFragment;->d:Lco/uk/getmondo/pin/SmsDialogFragment$a;

    .line 96
    return-void
.end method

.method public onChangedNumber()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1102c8
        }
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, Lco/uk/getmondo/pin/SmsDialogFragment;->d:Lco/uk/getmondo/pin/SmsDialogFragment$a;

    if-nez v0, :cond_0

    .line 86
    :goto_0
    return-void

    .line 85
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/pin/SmsDialogFragment;->d:Lco/uk/getmondo/pin/SmsDialogFragment$a;

    invoke-interface {v0}, Lco/uk/getmondo/pin/SmsDialogFragment$a;->E()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 43
    invoke-super {p0, p1}, Landroid/support/v4/app/i;->onCreate(Landroid/os/Bundle;)V

    .line 44
    invoke-virtual {p0}, Lco/uk/getmondo/pin/SmsDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "phone_number"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    invoke-virtual {p0}, Lco/uk/getmondo/pin/SmsDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "phone_number"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/pin/SmsDialogFragment;->b:Ljava/lang/String;

    .line 47
    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 52
    invoke-virtual {p0}, Lco/uk/getmondo/pin/SmsDialogFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f050096

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 53
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lco/uk/getmondo/pin/SmsDialogFragment;->c:Lbutterknife/Unbinder;

    .line 55
    iget-object v1, p0, Lco/uk/getmondo/pin/SmsDialogFragment;->textMeButton:Landroid/widget/Button;

    const v2, 0x7f0a03cd

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lco/uk/getmondo/pin/SmsDialogFragment;->b:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Lco/uk/getmondo/pin/SmsDialogFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 57
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lco/uk/getmondo/pin/SmsDialogFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v2

    const v3, 0x7f0c0112

    invoke-direct {v1, v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v2, 0x7f0a0309

    .line 58
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 59
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 60
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 63
    if-eqz v1, :cond_0

    .line 64
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 67
    :cond_0
    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lco/uk/getmondo/pin/SmsDialogFragment;->c:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 73
    invoke-super {p0}, Landroid/support/v4/app/i;->onDestroyView()V

    .line 74
    return-void
.end method

.method public onNotNow()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1102c9
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lco/uk/getmondo/pin/SmsDialogFragment;->d:Lco/uk/getmondo/pin/SmsDialogFragment$a;

    if-nez v0, :cond_0

    .line 92
    :goto_0
    return-void

    .line 91
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/pin/SmsDialogFragment;->d:Lco/uk/getmondo/pin/SmsDialogFragment$a;

    invoke-interface {v0}, Lco/uk/getmondo/pin/SmsDialogFragment$a;->a()V

    goto :goto_0
.end method

.method public onTextMe()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1102c7
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lco/uk/getmondo/pin/SmsDialogFragment;->d:Lco/uk/getmondo/pin/SmsDialogFragment$a;

    if-nez v0, :cond_0

    .line 80
    :goto_0
    return-void

    .line 79
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/pin/SmsDialogFragment;->d:Lco/uk/getmondo/pin/SmsDialogFragment$a;

    invoke-interface {v0}, Lco/uk/getmondo/pin/SmsDialogFragment$a;->D()V

    goto :goto_0
.end method
