.class Lco/uk/getmondo/pin/e;
.super Lco/uk/getmondo/common/ui/b;
.source "ForgotPinPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/pin/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/pin/e$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lorg/threeten/bp/format/DateTimeFormatter;

.field private d:Lorg/threeten/bp/LocalDate;

.field private final e:Lio/reactivex/u;

.field private final f:Lio/reactivex/u;

.field private final g:Lio/reactivex/u;

.field private final h:Lco/uk/getmondo/pin/a/a;

.field private final i:Lco/uk/getmondo/common/e/a;

.field private final j:Lco/uk/getmondo/signup_old/s;

.field private final k:Lco/uk/getmondo/common/a;


# direct methods
.method constructor <init>(Lio/reactivex/u;Lio/reactivex/u;Lio/reactivex/u;Lco/uk/getmondo/pin/a/a;Lco/uk/getmondo/common/e/a;Lco/uk/getmondo/signup_old/s;Lco/uk/getmondo/common/a;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 37
    const-string v0, "dd/MM/yyyy"

    invoke-static {v0}, Lorg/threeten/bp/format/DateTimeFormatter;->a(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/pin/e;->c:Lorg/threeten/bp/format/DateTimeFormatter;

    .line 52
    iput-object p1, p0, Lco/uk/getmondo/pin/e;->e:Lio/reactivex/u;

    .line 53
    iput-object p2, p0, Lco/uk/getmondo/pin/e;->f:Lio/reactivex/u;

    .line 54
    iput-object p3, p0, Lco/uk/getmondo/pin/e;->g:Lio/reactivex/u;

    .line 55
    iput-object p4, p0, Lco/uk/getmondo/pin/e;->h:Lco/uk/getmondo/pin/a/a;

    .line 56
    iput-object p5, p0, Lco/uk/getmondo/pin/e;->i:Lco/uk/getmondo/common/e/a;

    .line 57
    iput-object p6, p0, Lco/uk/getmondo/pin/e;->j:Lco/uk/getmondo/signup_old/s;

    .line 58
    iput-object p7, p0, Lco/uk/getmondo/pin/e;->k:Lco/uk/getmondo/common/a;

    .line 59
    return-void
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 98
    const-string v0, " "

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/pin/e$a;Lco/uk/getmondo/common/b/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 132
    invoke-interface {p0}, Lco/uk/getmondo/pin/e$a;->B()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/pin/e$a;Lio/reactivex/b/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 118
    invoke-interface {p0}, Lco/uk/getmondo/pin/e$a;->i()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/pin/e$a;Ljava/lang/Boolean;Ljava/lang/Throwable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 87
    invoke-interface {p0}, Lco/uk/getmondo/pin/e$a;->k()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/pin/e$a;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 66
    invoke-interface {p0}, Lco/uk/getmondo/pin/e$a;->B()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/pin/e;Lco/uk/getmondo/pin/e$a;Ljava/lang/Boolean;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 89
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lco/uk/getmondo/pin/e;->k:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->M()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 91
    invoke-interface {p1}, Lco/uk/getmondo/pin/e$a;->z()V

    .line 95
    :goto_0
    return-void

    .line 93
    :cond_0
    invoke-interface {p1}, Lco/uk/getmondo/pin/e$a;->v()V

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/pin/e;Lco/uk/getmondo/pin/e$a;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 136
    invoke-interface {p1}, Lco/uk/getmondo/pin/e$a;->x()V

    .line 137
    iget-object v0, p0, Lco/uk/getmondo/pin/e;->k:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->N()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 138
    invoke-interface {p1}, Lco/uk/getmondo/pin/e$a;->y()V

    .line 139
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/pin/e;Lco/uk/getmondo/pin/e$a;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p0, Lco/uk/getmondo/pin/e;->j:Lco/uk/getmondo/signup_old/s;

    invoke-virtual {v0, p3}, Lco/uk/getmondo/signup_old/s;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lco/uk/getmondo/pin/e;->c:Lorg/threeten/bp/format/DateTimeFormatter;

    invoke-static {p3, v0}, Lorg/threeten/bp/LocalDate;->a(Ljava/lang/CharSequence;Lorg/threeten/bp/format/DateTimeFormatter;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/pin/e;->d:Lorg/threeten/bp/LocalDate;

    .line 102
    invoke-interface {p1}, Lco/uk/getmondo/pin/e$a;->w()V

    .line 103
    iget-object v0, p0, Lco/uk/getmondo/pin/e;->k:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->P()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 104
    invoke-interface {p1, p2}, Lco/uk/getmondo/pin/e$a;->d(Ljava/lang/String;)V

    .line 108
    :goto_0
    return-void

    .line 106
    :cond_0
    invoke-interface {p1}, Lco/uk/getmondo/pin/e$a;->g()V

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/pin/e;Lco/uk/getmondo/pin/e$a;Ljava/lang/Throwable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 120
    invoke-interface {p1}, Lco/uk/getmondo/pin/e$a;->k()V

    .line 121
    invoke-direct {p0, p2}, Lco/uk/getmondo/pin/e;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lco/uk/getmondo/pin/e;->k:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->M()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 123
    invoke-interface {p1}, Lco/uk/getmondo/pin/e$a;->z()V

    .line 127
    :goto_0
    return-void

    .line 126
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/pin/e;->i:Lco/uk/getmondo/common/e/a;

    invoke-virtual {v0, p2, p1}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    goto :goto_0
.end method

.method private a(Ljava/lang/Throwable;)Z
    .locals 1

    .prologue
    .line 148
    invoke-direct {p0, p1}, Lco/uk/getmondo/pin/e;->b(Ljava/lang/Throwable;)Lco/uk/getmondo/api/model/pin/a;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/lang/Throwable;)Lco/uk/getmondo/api/model/pin/a;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 158
    instance-of v1, p1, Lco/uk/getmondo/api/ApiException;

    if-nez v1, :cond_1

    .line 165
    :cond_0
    :goto_0
    return-object v0

    .line 161
    :cond_1
    check-cast p1, Lco/uk/getmondo/api/ApiException;

    invoke-virtual {p1}, Lco/uk/getmondo/api/ApiException;->e()Lco/uk/getmondo/api/model/b;

    move-result-object v1

    .line 162
    if-eqz v1, :cond_0

    .line 165
    invoke-static {}, Lco/uk/getmondo/api/model/pin/a;->values()[Lco/uk/getmondo/api/model/pin/a;

    move-result-object v0

    invoke-virtual {v1}, Lco/uk/getmondo/api/model/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lco/uk/getmondo/common/e/d;->a([Lco/uk/getmondo/common/e/f;Ljava/lang/String;)Lco/uk/getmondo/common/e/f;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/api/model/pin/a;

    goto :goto_0
.end method

.method static synthetic b(Lco/uk/getmondo/pin/e;Lco/uk/getmondo/pin/e$a;Ljava/lang/Object;)Lio/reactivex/r;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lco/uk/getmondo/pin/e;->h:Lco/uk/getmondo/pin/a/a;

    iget-object v1, p0, Lco/uk/getmondo/pin/e;->d:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/pin/a/a;->a(Lorg/threeten/bp/LocalDate;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/pin/e;->f:Lio/reactivex/u;

    .line 116
    invoke-virtual {v0, v1}, Lio/reactivex/b;->b(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/pin/e;->e:Lio/reactivex/u;

    .line 117
    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/u;)Lio/reactivex/b;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/pin/m;->a(Lco/uk/getmondo/pin/e$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 118
    invoke-virtual {v0, v1}, Lio/reactivex/b;->c(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/pin/n;->a(Lco/uk/getmondo/pin/e;Lco/uk/getmondo/pin/e$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 119
    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/c/g;)Lio/reactivex/b;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/common/b/a;->a:Lco/uk/getmondo/common/b/a;

    .line 128
    invoke-static {v1}, Lio/reactivex/n;->just(Ljava/lang/Object;)Lio/reactivex/n;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/b;->a(Lio/reactivex/r;)Lio/reactivex/n;

    move-result-object v0

    .line 115
    return-object v0
.end method

.method static synthetic b(Lco/uk/getmondo/pin/e$a;Lco/uk/getmondo/common/b/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 131
    invoke-interface {p0}, Lco/uk/getmondo/pin/e$a;->k()V

    return-void
.end method

.method static synthetic b(Lco/uk/getmondo/pin/e$a;Lio/reactivex/b/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 86
    invoke-interface {p0}, Lco/uk/getmondo/pin/e$a;->h()V

    return-void
.end method

.method static synthetic b(Lco/uk/getmondo/pin/e;Lco/uk/getmondo/pin/e$a;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lco/uk/getmondo/pin/e;->i:Lco/uk/getmondo/common/e/a;

    invoke-virtual {v0, p2, p1}, Lco/uk/getmondo/common/e/a;->a(Ljava/lang/Throwable;Lco/uk/getmondo/common/e/a$a;)Z

    return-void
.end method

.method static synthetic c(Lco/uk/getmondo/pin/e$a;Lco/uk/getmondo/common/b/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 129
    invoke-interface {p0}, Lco/uk/getmondo/pin/e$a;->j()V

    return-void
.end method

.method static synthetic c(Lco/uk/getmondo/pin/e;Lco/uk/getmondo/pin/e$a;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, Lco/uk/getmondo/pin/e;->k:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->O()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 113
    invoke-interface {p1}, Lco/uk/getmondo/pin/e$a;->x()V

    .line 114
    return-void
.end method

.method static synthetic d(Lco/uk/getmondo/pin/e;Lco/uk/getmondo/pin/e$a;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lco/uk/getmondo/pin/e;->k:Lco/uk/getmondo/common/a;

    sget-object v1, Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;->PIN:Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;

    invoke-static {v1}, Lco/uk/getmondo/api/model/tracking/Impression;->a(Lco/uk/getmondo/api/model/tracking/Impression$IntercomFrom;)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 71
    invoke-interface {p1}, Lco/uk/getmondo/pin/e$a;->A()V

    .line 72
    invoke-interface {p1}, Lco/uk/getmondo/pin/e$a;->B()V

    .line 73
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 35
    check-cast p1, Lco/uk/getmondo/pin/e$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/pin/e;->a(Lco/uk/getmondo/pin/e$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/pin/e$a;)V
    .locals 5

    .prologue
    .line 63
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 65
    invoke-interface {p1}, Lco/uk/getmondo/pin/e$a;->b()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/pin/f;->a(Lco/uk/getmondo/pin/e$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 66
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 65
    invoke-virtual {p0, v0}, Lco/uk/getmondo/pin/e;->a(Lio/reactivex/b/b;)V

    .line 68
    invoke-interface {p1}, Lco/uk/getmondo/pin/e$a;->f()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/pin/o;->a(Lco/uk/getmondo/pin/e;Lco/uk/getmondo/pin/e$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 69
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 68
    invoke-virtual {p0, v0}, Lco/uk/getmondo/pin/e;->a(Lio/reactivex/b/b;)V

    .line 76
    iget-object v0, p0, Lco/uk/getmondo/pin/e;->h:Lco/uk/getmondo/pin/a/a;

    invoke-virtual {v0}, Lco/uk/getmondo/pin/a/a;->a()Ljava/lang/String;

    move-result-object v0

    .line 77
    invoke-static {v0}, Lco/uk/getmondo/common/k/p;->d(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 78
    iget-object v0, p0, Lco/uk/getmondo/pin/e;->k:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->M()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 79
    invoke-interface {p1}, Lco/uk/getmondo/pin/e$a;->z()V

    .line 140
    :goto_0
    return-void

    .line 83
    :cond_0
    iget-object v1, p0, Lco/uk/getmondo/pin/e;->h:Lco/uk/getmondo/pin/a/a;

    invoke-virtual {v1}, Lco/uk/getmondo/pin/a/a;->b()Lio/reactivex/v;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/pin/e;->f:Lio/reactivex/u;

    .line 84
    invoke-virtual {v1, v2}, Lio/reactivex/v;->b(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v1

    iget-object v2, p0, Lco/uk/getmondo/pin/e;->e:Lio/reactivex/u;

    .line 85
    invoke-virtual {v1, v2}, Lio/reactivex/v;->a(Lio/reactivex/u;)Lio/reactivex/v;

    move-result-object v1

    invoke-static {p1}, Lco/uk/getmondo/pin/p;->a(Lco/uk/getmondo/pin/e$a;)Lio/reactivex/c/g;

    move-result-object v2

    .line 86
    invoke-virtual {v1, v2}, Lio/reactivex/v;->b(Lio/reactivex/c/g;)Lio/reactivex/v;

    move-result-object v1

    invoke-static {p1}, Lco/uk/getmondo/pin/q;->a(Lco/uk/getmondo/pin/e$a;)Lio/reactivex/c/b;

    move-result-object v2

    .line 87
    invoke-virtual {v1, v2}, Lio/reactivex/v;->a(Lio/reactivex/c/b;)Lio/reactivex/v;

    move-result-object v1

    invoke-static {p0, p1}, Lco/uk/getmondo/pin/r;->a(Lco/uk/getmondo/pin/e;Lco/uk/getmondo/pin/e$a;)Lio/reactivex/c/g;

    move-result-object v2

    invoke-static {p0, p1}, Lco/uk/getmondo/pin/s;->a(Lco/uk/getmondo/pin/e;Lco/uk/getmondo/pin/e$a;)Lio/reactivex/c/g;

    move-result-object v3

    .line 88
    invoke-virtual {v1, v2, v3}, Lio/reactivex/v;->a(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v1

    .line 83
    invoke-virtual {p0, v1}, Lco/uk/getmondo/pin/e;->a(Lio/reactivex/b/b;)V

    .line 97
    invoke-interface {p1}, Lco/uk/getmondo/pin/e$a;->c()Lio/reactivex/n;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/pin/t;->a()Lio/reactivex/c/h;

    move-result-object v2

    .line 98
    invoke-virtual {v1, v2}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v1

    invoke-static {p0, p1, v0}, Lco/uk/getmondo/pin/u;->a(Lco/uk/getmondo/pin/e;Lco/uk/getmondo/pin/e$a;Ljava/lang/String;)Lio/reactivex/c/g;

    move-result-object v0

    .line 99
    invoke-virtual {v1, v0}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 97
    invoke-virtual {p0, v0}, Lco/uk/getmondo/pin/e;->a(Lio/reactivex/b/b;)V

    .line 110
    invoke-interface {p1}, Lco/uk/getmondo/pin/e$a;->d()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/pin/v;->a(Lco/uk/getmondo/pin/e;Lco/uk/getmondo/pin/e$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 111
    invoke-virtual {v0, v1}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/pin/g;->a(Lco/uk/getmondo/pin/e;Lco/uk/getmondo/pin/e$a;)Lio/reactivex/c/h;

    move-result-object v1

    .line 115
    invoke-virtual {v0, v1}, Lio/reactivex/n;->switchMap(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/pin/h;->a(Lco/uk/getmondo/pin/e$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 129
    invoke-virtual {v0, v1}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v0

    const-wide/16 v2, 0x5

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v4, p0, Lco/uk/getmondo/pin/e;->g:Lio/reactivex/u;

    .line 130
    invoke-virtual {v0, v2, v3, v1, v4}, Lio/reactivex/n;->delay(JLjava/util/concurrent/TimeUnit;Lio/reactivex/u;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/pin/i;->a(Lco/uk/getmondo/pin/e$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 131
    invoke-virtual {v0, v1}, Lio/reactivex/n;->doOnNext(Lio/reactivex/c/g;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/pin/j;->a(Lco/uk/getmondo/pin/e$a;)Lio/reactivex/c/g;

    move-result-object v1

    invoke-static {}, Lco/uk/getmondo/pin/k;->a()Lio/reactivex/c/g;

    move-result-object v2

    .line 132
    invoke-virtual {v0, v1, v2}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 110
    invoke-virtual {p0, v0}, Lco/uk/getmondo/pin/e;->a(Lio/reactivex/b/b;)V

    .line 134
    invoke-interface {p1}, Lco/uk/getmondo/pin/e$a;->e()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0, p1}, Lco/uk/getmondo/pin/l;->a(Lco/uk/getmondo/pin/e;Lco/uk/getmondo/pin/e$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 135
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 134
    invoke-virtual {p0, v0}, Lco/uk/getmondo/pin/e;->a(Lio/reactivex/b/b;)V

    goto/16 :goto_0
.end method
