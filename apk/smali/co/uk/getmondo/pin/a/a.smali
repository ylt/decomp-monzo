.class public Lco/uk/getmondo/pin/a/a;
.super Ljava/lang/Object;
.source "PinManager.java"


# instance fields
.field private final a:Lco/uk/getmondo/common/accounts/d;

.field private final b:Lco/uk/getmondo/api/MonzoApi;


# direct methods
.method constructor <init>(Lco/uk/getmondo/common/accounts/d;Lco/uk/getmondo/api/MonzoApi;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lco/uk/getmondo/pin/a/a;->a:Lco/uk/getmondo/common/accounts/d;

    .line 25
    iput-object p2, p0, Lco/uk/getmondo/pin/a/a;->b:Lco/uk/getmondo/api/MonzoApi;

    .line 26
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/pin/a/a;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/d;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lco/uk/getmondo/pin/a/a;->b:Lco/uk/getmondo/api/MonzoApi;

    invoke-interface {v0, p2, p1}, Lco/uk/getmondo/api/MonzoApi;->pinViaSms(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/d/ac;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 31
    invoke-virtual {p0}, Lco/uk/getmondo/d/ac;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 32
    const-string v0, ""

    return-object v0
.end method


# virtual methods
.method public a(Lorg/threeten/bp/LocalDate;)Lio/reactivex/b;
    .locals 2

    .prologue
    .line 37
    sget-object v0, Lorg/threeten/bp/format/DateTimeFormatter;->c:Lorg/threeten/bp/format/DateTimeFormatter;

    invoke-virtual {p1, v0}, Lorg/threeten/bp/LocalDate;->a(Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object v0

    .line 38
    iget-object v1, p0, Lco/uk/getmondo/pin/a/a;->a:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v1}, Lco/uk/getmondo/common/accounts/d;->d()Lio/reactivex/v;

    move-result-object v1

    invoke-static {p0, v0}, Lco/uk/getmondo/pin/a/e;->a(Lco/uk/getmondo/pin/a/a;Ljava/lang/String;)Lio/reactivex/c/h;

    move-result-object v0

    .line 39
    invoke-virtual {v1, v0}, Lio/reactivex/v;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    .line 38
    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, Lco/uk/getmondo/pin/a/a;->a:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->c()Lio/reactivex/v;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/pin/a/b;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 30
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/pin/a/c;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 31
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/pin/a/d;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 32
    invoke-virtual {v0, v1}, Lio/reactivex/v;->e(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lio/reactivex/v;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 29
    return-object v0
.end method

.method public b()Lio/reactivex/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/v",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lco/uk/getmondo/pin/a/a;->a:Lco/uk/getmondo/common/accounts/d;

    invoke-virtual {v0}, Lco/uk/getmondo/common/accounts/d;->d()Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/pin/a/a;->b:Lco/uk/getmondo/api/MonzoApi;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lco/uk/getmondo/pin/a/f;->a(Lco/uk/getmondo/api/MonzoApi;)Lio/reactivex/c/h;

    move-result-object v1

    .line 44
    invoke-virtual {v0, v1}, Lio/reactivex/v;->a(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/pin/a/g;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 45
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    .line 43
    return-object v0
.end method
