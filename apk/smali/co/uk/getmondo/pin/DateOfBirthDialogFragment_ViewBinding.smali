.class public Lco/uk/getmondo/pin/DateOfBirthDialogFragment_ViewBinding;
.super Ljava/lang/Object;
.source "DateOfBirthDialogFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/pin/DateOfBirthDialogFragment;

.field private b:Landroid/view/View;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/pin/DateOfBirthDialogFragment;Landroid/view/View;)V
    .locals 4

    .prologue
    const v3, 0x7f1102c6

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment_ViewBinding;->a:Lco/uk/getmondo/pin/DateOfBirthDialogFragment;

    .line 27
    const v0, 0x7f11026b

    const-string v1, "field \'dobWrapper\'"

    const-class v2, Landroid/support/design/widget/TextInputLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    iput-object v0, p1, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->dobWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 28
    const-string v0, "field \'dobEditText\' and method \'onNext\'"

    invoke-static {p2, v3, v0}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 29
    const-string v0, "field \'dobEditText\'"

    const-class v2, Landroid/widget/EditText;

    invoke-static {v1, v3, v0, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p1, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->dobEditText:Landroid/widget/EditText;

    .line 30
    iput-object v1, p0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment_ViewBinding;->b:Landroid/view/View;

    move-object v0, v1

    .line 31
    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Lco/uk/getmondo/pin/DateOfBirthDialogFragment_ViewBinding$1;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/pin/DateOfBirthDialogFragment_ViewBinding$1;-><init>(Lco/uk/getmondo/pin/DateOfBirthDialogFragment_ViewBinding;Lco/uk/getmondo/pin/DateOfBirthDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 37
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42
    iget-object v0, p0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment_ViewBinding;->a:Lco/uk/getmondo/pin/DateOfBirthDialogFragment;

    .line 43
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment_ViewBinding;->a:Lco/uk/getmondo/pin/DateOfBirthDialogFragment;

    .line 46
    iput-object v1, v0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->dobWrapper:Landroid/support/design/widget/TextInputLayout;

    .line 47
    iput-object v1, v0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->dobEditText:Landroid/widget/EditText;

    .line 49
    iget-object v0, p0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment_ViewBinding;->b:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 50
    iput-object v1, p0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment_ViewBinding;->b:Landroid/view/View;

    .line 51
    return-void
.end method
