.class public Lco/uk/getmondo/pin/DateOfBirthDialogFragment;
.super Landroid/support/v4/app/i;
.source "DateOfBirthDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/pin/DateOfBirthDialogFragment$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:Lbutterknife/Unbinder;

.field private c:Lco/uk/getmondo/pin/DateOfBirthDialogFragment$a;

.field dobEditText:Landroid/widget/EditText;
    .annotation build Lbutterknife/BindView;
        value = 0x7f1102c6
    .end annotation
.end field

.field dobWrapper:Landroid/support/design/widget/TextInputLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11026b
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/support/v4/app/i;-><init>()V

    return-void
.end method

.method public static a()Lco/uk/getmondo/pin/DateOfBirthDialogFragment;
    .locals 2

    .prologue
    .line 38
    new-instance v0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;

    invoke-direct {v0}, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;-><init>()V

    .line 39
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->setCancelable(Z)V

    .line 40
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/pin/DateOfBirthDialogFragment;Landroid/app/AlertDialog;Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 79
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/pin/c;->a(Lco/uk/getmondo/pin/DateOfBirthDialogFragment;)Landroid/view/View$OnClickListener;

    move-result-object v1

    .line 80
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/pin/DateOfBirthDialogFragment;Landroid/content/DialogInterface;I)V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->c:Lco/uk/getmondo/pin/DateOfBirthDialogFragment$a;

    if-nez v0, :cond_0

    .line 74
    :goto_0
    return-void

    .line 73
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->c:Lco/uk/getmondo/pin/DateOfBirthDialogFragment$a;

    invoke-interface {v0}, Lco/uk/getmondo/pin/DateOfBirthDialogFragment$a;->a()V

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/pin/DateOfBirthDialogFragment;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->c:Lco/uk/getmondo/pin/DateOfBirthDialogFragment$a;

    if-nez v0, :cond_0

    .line 83
    :goto_0
    return-void

    .line 82
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->c:Lco/uk/getmondo/pin/DateOfBirthDialogFragment$a;

    iget-object v1, p0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->dobEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lco/uk/getmondo/pin/DateOfBirthDialogFragment$a;->a(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/pin/DateOfBirthDialogFragment$a;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->c:Lco/uk/getmondo/pin/DateOfBirthDialogFragment$a;

    .line 117
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->dobWrapper:Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 112
    iget-object v0, p0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->dobWrapper:Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 113
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 46
    invoke-virtual {p0}, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f050095

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 47
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->b:Lbutterknife/Unbinder;

    .line 49
    new-instance v1, Lco/uk/getmondo/signup_old/r;

    invoke-direct {v1}, Lco/uk/getmondo/signup_old/r;-><init>()V

    .line 50
    iget-object v2, p0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->dobEditText:Landroid/widget/EditText;

    new-instance v3, Lco/uk/getmondo/pin/DateOfBirthDialogFragment$1;

    invoke-direct {v3, p0, v1}, Lco/uk/getmondo/pin/DateOfBirthDialogFragment$1;-><init>(Lco/uk/getmondo/pin/DateOfBirthDialogFragment;Lco/uk/getmondo/signup_old/r;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 67
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->getActivity()Landroid/support/v4/app/j;

    move-result-object v2

    const v3, 0x7f0c0112

    invoke-direct {v1, v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v2, 0x7f0a0303

    .line 68
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 69
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a02bd

    .line 70
    invoke-virtual {p0, v1}, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a02c3

    .line 71
    invoke-virtual {p0, v1}, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lco/uk/getmondo/pin/a;->a(Lco/uk/getmondo/pin/DateOfBirthDialogFragment;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 75
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 76
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 79
    invoke-static {p0, v0}, Lco/uk/getmondo/pin/b;->a(Lco/uk/getmondo/pin/DateOfBirthDialogFragment;Landroid/app/AlertDialog;)Landroid/content/DialogInterface$OnShowListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 85
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 86
    if-eqz v1, :cond_0

    .line 87
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 88
    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 91
    :cond_0
    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->b:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 97
    invoke-super {p0}, Landroid/support/v4/app/i;->onDestroyView()V

    .line 98
    return-void
.end method

.method protected onNext(I)Z
    .locals 2
    .annotation build Lbutterknife/OnEditorAction;
        value = {
            0x7f1102c6
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 102
    const/4 v1, 0x6

    if-ne p1, v1, :cond_0

    .line 103
    iget-object v1, p0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->c:Lco/uk/getmondo/pin/DateOfBirthDialogFragment$a;

    if-nez v1, :cond_1

    .line 107
    :cond_0
    :goto_0
    return v0

    .line 104
    :cond_1
    iget-object v0, p0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->c:Lco/uk/getmondo/pin/DateOfBirthDialogFragment$a;

    iget-object v1, p0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->dobEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lco/uk/getmondo/pin/DateOfBirthDialogFragment$a;->a(Ljava/lang/String;)V

    .line 105
    const/4 v0, 0x1

    goto :goto_0
.end method
