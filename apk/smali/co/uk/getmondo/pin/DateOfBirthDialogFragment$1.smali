.class Lco/uk/getmondo/pin/DateOfBirthDialogFragment$1;
.super Ljava/lang/Object;
.source "DateOfBirthDialogFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lco/uk/getmondo/signup_old/r;

.field final synthetic b:Lco/uk/getmondo/pin/DateOfBirthDialogFragment;


# direct methods
.method constructor <init>(Lco/uk/getmondo/pin/DateOfBirthDialogFragment;Lco/uk/getmondo/signup_old/r;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment$1;->b:Lco/uk/getmondo/pin/DateOfBirthDialogFragment;

    iput-object p2, p0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment$1;->a:Lco/uk/getmondo/signup_old/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 64
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 53
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 6

    .prologue
    .line 57
    iget-object v0, p0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment$1;->b:Lco/uk/getmondo/pin/DateOfBirthDialogFragment;

    iget-object v0, v0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->dobWrapper:Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 58
    iget-object v0, p0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment$1;->b:Lco/uk/getmondo/pin/DateOfBirthDialogFragment;

    iget-object v0, v0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->dobWrapper:Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 59
    iget-object v0, p0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment$1;->a:Lco/uk/getmondo/signup_old/r;

    iget-object v1, p0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment$1;->b:Lco/uk/getmondo/pin/DateOfBirthDialogFragment;

    iget-object v5, v1, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->dobEditText:Landroid/widget/EditText;

    move-object v1, p1

    move v2, p3

    move v3, p4

    move-object v4, p0

    invoke-virtual/range {v0 .. v5}, Lco/uk/getmondo/signup_old/r;->a(Ljava/lang/CharSequence;IILandroid/text/TextWatcher;Landroid/widget/EditText;)V

    .line 60
    return-void
.end method
