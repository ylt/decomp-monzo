.class public Lco/uk/getmondo/pin/x;
.super Landroid/support/v4/app/i;
.source "SupportDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/pin/x$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Lco/uk/getmondo/pin/x$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lco/uk/getmondo/pin/x;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lco/uk/getmondo/pin/x;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/support/v4/app/i;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/pin/x;
    .locals 3

    .prologue
    .line 25
    new-instance v0, Lco/uk/getmondo/pin/x;

    invoke-direct {v0}, Lco/uk/getmondo/pin/x;-><init>()V

    .line 26
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 27
    const-string v2, "title"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    const-string v2, "message"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    invoke-virtual {v0, v1}, Lco/uk/getmondo/pin/x;->setArguments(Landroid/os/Bundle;)V

    .line 30
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lco/uk/getmondo/pin/x;->setCancelable(Z)V

    .line 31
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/pin/x;Landroid/content/DialogInterface;I)V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lco/uk/getmondo/pin/x;->d:Lco/uk/getmondo/pin/x$a;

    if-nez v0, :cond_0

    .line 58
    :goto_0
    return-void

    .line 57
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/pin/x;->d:Lco/uk/getmondo/pin/x$a;

    invoke-interface {v0}, Lco/uk/getmondo/pin/x$a;->C()V

    goto :goto_0
.end method

.method static synthetic b(Lco/uk/getmondo/pin/x;Landroid/content/DialogInterface;I)V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lco/uk/getmondo/pin/x;->d:Lco/uk/getmondo/pin/x$a;

    if-nez v0, :cond_0

    .line 54
    :goto_0
    return-void

    .line 53
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/pin/x;->d:Lco/uk/getmondo/pin/x$a;

    invoke-interface {v0}, Lco/uk/getmondo/pin/x$a;->a()V

    goto :goto_0
.end method


# virtual methods
.method public a(Lco/uk/getmondo/pin/x$a;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lco/uk/getmondo/pin/x;->d:Lco/uk/getmondo/pin/x$a;

    .line 71
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 36
    invoke-super {p0, p1}, Landroid/support/v4/app/i;->onCreate(Landroid/os/Bundle;)V

    .line 37
    invoke-virtual {p0}, Lco/uk/getmondo/pin/x;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    invoke-virtual {p0}, Lco/uk/getmondo/pin/x;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "title"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/pin/x;->b:Ljava/lang/String;

    .line 40
    :cond_0
    invoke-virtual {p0}, Lco/uk/getmondo/pin/x;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 41
    invoke-virtual {p0}, Lco/uk/getmondo/pin/x;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "message"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/pin/x;->c:Ljava/lang/String;

    .line 43
    :cond_1
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 48
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lco/uk/getmondo/pin/x;->getActivity()Landroid/support/v4/app/j;

    move-result-object v1

    const v2, 0x7f0c0112

    invoke-direct {v0, v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Lco/uk/getmondo/pin/x;->b:Ljava/lang/String;

    .line 49
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/pin/x;->c:Ljava/lang/String;

    .line 50
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a02c3

    invoke-static {p0}, Lco/uk/getmondo/pin/y;->a(Lco/uk/getmondo/pin/x;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    .line 51
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a012b

    invoke-static {p0}, Lco/uk/getmondo/pin/z;->a(Lco/uk/getmondo/pin/x;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    .line 55
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 62
    if-eqz v1, :cond_0

    .line 63
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 66
    :cond_0
    return-object v0
.end method
