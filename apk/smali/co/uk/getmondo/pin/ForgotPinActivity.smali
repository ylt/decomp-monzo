.class public Lco/uk/getmondo/pin/ForgotPinActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "ForgotPinActivity.java"

# interfaces
.implements Lco/uk/getmondo/pin/DateOfBirthDialogFragment$a;
.implements Lco/uk/getmondo/pin/SmsDialogFragment$a;
.implements Lco/uk/getmondo/pin/e$a;
.implements Lco/uk/getmondo/pin/x$a;


# instance fields
.field a:Lco/uk/getmondo/pin/e;

.field b:Lco/uk/getmondo/common/q;

.field private c:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/b/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/c",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private i:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    .line 28
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/pin/ForgotPinActivity;->c:Lcom/b/b/c;

    .line 29
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/pin/ForgotPinActivity;->e:Lcom/b/b/c;

    .line 30
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/pin/ForgotPinActivity;->f:Lcom/b/b/c;

    .line 31
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/pin/ForgotPinActivity;->g:Lcom/b/b/c;

    .line 32
    invoke-static {}, Lcom/b/b/c;->a()Lcom/b/b/c;

    move-result-object v0

    iput-object v0, p0, Lco/uk/getmondo/pin/ForgotPinActivity;->h:Lcom/b/b/c;

    return-void
.end method

.method private e(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 199
    invoke-virtual {p0}, Lco/uk/getmondo/pin/ForgotPinActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/n;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 200
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/support/v4/app/i;

    if-eqz v1, :cond_0

    .line 201
    check-cast v0, Landroid/support/v4/app/i;

    .line 202
    invoke-virtual {v0}, Landroid/support/v4/app/i;->dismiss()V

    .line 204
    :cond_0
    return-void
.end method

.method private f(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Lco/uk/getmondo/pin/ForgotPinActivity;->i:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 208
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lco/uk/getmondo/pin/ForgotPinActivity;->i:Landroid/app/ProgressDialog;

    .line 209
    iget-object v0, p0, Lco/uk/getmondo/pin/ForgotPinActivity;->i:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 211
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/pin/ForgotPinActivity;->i:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 212
    iget-object v0, p0, Lco/uk/getmondo/pin/ForgotPinActivity;->i:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 213
    return-void
.end method


# virtual methods
.method public A()V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lco/uk/getmondo/pin/ForgotPinActivity;->b:Lco/uk/getmondo/common/q;

    invoke-virtual {v0}, Lco/uk/getmondo/common/q;->a()V

    .line 157
    return-void
.end method

.method public B()V
    .locals 1

    .prologue
    .line 161
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lco/uk/getmondo/pin/ForgotPinActivity;->setResult(I)V

    .line 162
    invoke-virtual {p0}, Lco/uk/getmondo/pin/ForgotPinActivity;->finish()V

    .line 163
    return-void
.end method

.method public C()V
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, Lco/uk/getmondo/pin/ForgotPinActivity;->h:Lcom/b/b/c;

    sget-object v1, Lco/uk/getmondo/common/b/a;->a:Lco/uk/getmondo/common/b/a;

    invoke-virtual {v0, v1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 173
    return-void
.end method

.method public D()V
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lco/uk/getmondo/pin/ForgotPinActivity;->f:Lcom/b/b/c;

    sget-object v1, Lco/uk/getmondo/common/b/a;->a:Lco/uk/getmondo/common/b/a;

    invoke-virtual {v0, v1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 178
    return-void
.end method

.method public E()V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lco/uk/getmondo/pin/ForgotPinActivity;->g:Lcom/b/b/c;

    sget-object v1, Lco/uk/getmondo/common/b/a;->a:Lco/uk/getmondo/common/b/a;

    invoke-virtual {v0, v1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 183
    return-void
.end method

.method public a()V
    .locals 2

    .prologue
    .line 167
    iget-object v0, p0, Lco/uk/getmondo/pin/ForgotPinActivity;->c:Lcom/b/b/c;

    sget-object v1, Lco/uk/getmondo/common/b/a;->a:Lco/uk/getmondo/common/b/a;

    invoke-virtual {v0, v1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 168
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lco/uk/getmondo/pin/ForgotPinActivity;->e:Lcom/b/b/c;

    invoke-virtual {v0, p1}, Lcom/b/b/c;->a(Ljava/lang/Object;)V

    .line 188
    return-void
.end method

.method public b()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lco/uk/getmondo/pin/ForgotPinActivity;->c:Lcom/b/b/c;

    return-object v0
.end method

.method public b(I)V
    .locals 3

    .prologue
    .line 192
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 193
    const-string v1, "pin_error_msg"

    invoke-virtual {p0, p1}, Lco/uk/getmondo/pin/ForgotPinActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 194
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lco/uk/getmondo/pin/ForgotPinActivity;->setResult(ILandroid/content/Intent;)V

    .line 195
    invoke-virtual {p0}, Lco/uk/getmondo/pin/ForgotPinActivity;->finish()V

    .line 196
    return-void
.end method

.method public c()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lco/uk/getmondo/pin/ForgotPinActivity;->e:Lcom/b/b/c;

    return-object v0
.end method

.method public d()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lco/uk/getmondo/pin/ForgotPinActivity;->f:Lcom/b/b/c;

    return-object v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 128
    invoke-static {p1}, Lco/uk/getmondo/pin/SmsDialogFragment;->a(Ljava/lang/String;)Lco/uk/getmondo/pin/SmsDialogFragment;

    move-result-object v0

    .line 129
    invoke-virtual {v0, p0}, Lco/uk/getmondo/pin/SmsDialogFragment;->a(Lco/uk/getmondo/pin/SmsDialogFragment$a;)V

    .line 130
    invoke-virtual {p0}, Lco/uk/getmondo/pin/ForgotPinActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    sget-object v2, Lco/uk/getmondo/pin/SmsDialogFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/pin/SmsDialogFragment;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 131
    return-void
.end method

.method public e()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lco/uk/getmondo/pin/ForgotPinActivity;->g:Lcom/b/b/c;

    return-object v0
.end method

.method public f()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lco/uk/getmondo/pin/ForgotPinActivity;->h:Lcom/b/b/c;

    return-object v0
.end method

.method public g()V
    .locals 2

    .prologue
    .line 84
    invoke-virtual {p0}, Lco/uk/getmondo/pin/ForgotPinActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v0

    sget-object v1, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/n;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 85
    if-eqz v0, :cond_0

    instance-of v1, v0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;

    if-eqz v1, :cond_0

    .line 86
    check-cast v0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;

    .line 87
    const v1, 0x7f0a017b

    invoke-virtual {p0, v1}, Lco/uk/getmondo/pin/ForgotPinActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->a(Ljava/lang/String;)V

    .line 89
    :cond_0
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 93
    const v0, 0x7f0a0262

    invoke-virtual {p0, v0}, Lco/uk/getmondo/pin/ForgotPinActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/uk/getmondo/pin/ForgotPinActivity;->f(Ljava/lang/String;)V

    .line 94
    return-void
.end method

.method public i()V
    .locals 1

    .prologue
    .line 98
    const v0, 0x7f0a030e

    invoke-virtual {p0, v0}, Lco/uk/getmondo/pin/ForgotPinActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/uk/getmondo/pin/ForgotPinActivity;->f(Ljava/lang/String;)V

    .line 99
    return-void
.end method

.method public j()V
    .locals 1

    .prologue
    .line 103
    const v0, 0x7f0a0308

    invoke-virtual {p0, v0}, Lco/uk/getmondo/pin/ForgotPinActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/uk/getmondo/pin/ForgotPinActivity;->f(Ljava/lang/String;)V

    .line 104
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lco/uk/getmondo/pin/ForgotPinActivity;->i:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lco/uk/getmondo/pin/ForgotPinActivity;->i:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 110
    const/4 v0, 0x0

    iput-object v0, p0, Lco/uk/getmondo/pin/ForgotPinActivity;->i:Landroid/app/ProgressDialog;

    .line 112
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 41
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 42
    const v0, 0x7f05003b

    invoke-virtual {p0, v0}, Lco/uk/getmondo/pin/ForgotPinActivity;->setContentView(I)V

    .line 43
    invoke-virtual {p0}, Lco/uk/getmondo/pin/ForgotPinActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/pin/ForgotPinActivity;)V

    .line 44
    iget-object v0, p0, Lco/uk/getmondo/pin/ForgotPinActivity;->a:Lco/uk/getmondo/pin/e;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/pin/e;->a(Lco/uk/getmondo/pin/e$a;)V

    .line 45
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lco/uk/getmondo/pin/ForgotPinActivity;->a:Lco/uk/getmondo/pin/e;

    invoke-virtual {v0}, Lco/uk/getmondo/pin/e;->b()V

    .line 51
    iget-object v0, p0, Lco/uk/getmondo/pin/ForgotPinActivity;->i:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/uk/getmondo/pin/ForgotPinActivity;->i:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lco/uk/getmondo/pin/ForgotPinActivity;->i:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 54
    :cond_0
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 55
    return-void
.end method

.method public v()V
    .locals 3

    .prologue
    .line 116
    invoke-static {}, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->a()Lco/uk/getmondo/pin/DateOfBirthDialogFragment;

    move-result-object v0

    .line 117
    invoke-virtual {v0, p0}, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->a(Lco/uk/getmondo/pin/DateOfBirthDialogFragment$a;)V

    .line 118
    invoke-virtual {p0}, Lco/uk/getmondo/pin/ForgotPinActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    sget-object v2, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 119
    return-void
.end method

.method public w()V
    .locals 1

    .prologue
    .line 123
    sget-object v0, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lco/uk/getmondo/pin/ForgotPinActivity;->e(Ljava/lang/String;)V

    .line 124
    return-void
.end method

.method public x()V
    .locals 1

    .prologue
    .line 135
    sget-object v0, Lco/uk/getmondo/pin/SmsDialogFragment;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lco/uk/getmondo/pin/ForgotPinActivity;->e(Ljava/lang/String;)V

    .line 136
    return-void
.end method

.method public y()V
    .locals 3

    .prologue
    .line 140
    const v0, 0x7f0a030b

    .line 141
    invoke-virtual {p0, v0}, Lco/uk/getmondo/pin/ForgotPinActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a030a

    invoke-virtual {p0, v1}, Lco/uk/getmondo/pin/ForgotPinActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lco/uk/getmondo/pin/x;->a(Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/pin/x;

    move-result-object v0

    .line 142
    invoke-virtual {v0, p0}, Lco/uk/getmondo/pin/x;->a(Lco/uk/getmondo/pin/x$a;)V

    .line 143
    invoke-virtual {p0}, Lco/uk/getmondo/pin/ForgotPinActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    sget-object v2, Lco/uk/getmondo/pin/x;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/pin/x;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 144
    return-void
.end method

.method public z()V
    .locals 3

    .prologue
    .line 148
    const v0, 0x7f0a030d

    .line 149
    invoke-virtual {p0, v0}, Lco/uk/getmondo/pin/ForgotPinActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a030c

    invoke-virtual {p0, v1}, Lco/uk/getmondo/pin/ForgotPinActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lco/uk/getmondo/pin/x;->a(Ljava/lang/String;Ljava/lang/String;)Lco/uk/getmondo/pin/x;

    move-result-object v0

    .line 150
    invoke-virtual {v0, p0}, Lco/uk/getmondo/pin/x;->a(Lco/uk/getmondo/pin/x$a;)V

    .line 151
    invoke-virtual {p0}, Lco/uk/getmondo/pin/ForgotPinActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    sget-object v2, Lco/uk/getmondo/pin/x;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lco/uk/getmondo/pin/x;->show(Landroid/support/v4/app/n;Ljava/lang/String;)V

    .line 152
    return-void
.end method
