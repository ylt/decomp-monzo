.class public Lco/uk/getmondo/pin/SmsDialogFragment_ViewBinding;
.super Ljava/lang/Object;
.source "SmsDialogFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/pin/SmsDialogFragment;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/pin/SmsDialogFragment;Landroid/view/View;)V
    .locals 4

    .prologue
    const v3, 0x7f1102c7

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lco/uk/getmondo/pin/SmsDialogFragment_ViewBinding;->a:Lco/uk/getmondo/pin/SmsDialogFragment;

    .line 29
    const-string v0, "field \'textMeButton\' and method \'onTextMe\'"

    invoke-static {p2, v3, v0}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 30
    const-string v0, "field \'textMeButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {v1, v3, v0, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lco/uk/getmondo/pin/SmsDialogFragment;->textMeButton:Landroid/widget/Button;

    .line 31
    iput-object v1, p0, Lco/uk/getmondo/pin/SmsDialogFragment_ViewBinding;->b:Landroid/view/View;

    .line 32
    new-instance v0, Lco/uk/getmondo/pin/SmsDialogFragment_ViewBinding$1;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/pin/SmsDialogFragment_ViewBinding$1;-><init>(Lco/uk/getmondo/pin/SmsDialogFragment_ViewBinding;Lco/uk/getmondo/pin/SmsDialogFragment;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 38
    const v0, 0x7f1102c8

    const-string v1, "method \'onChangedNumber\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 39
    iput-object v0, p0, Lco/uk/getmondo/pin/SmsDialogFragment_ViewBinding;->c:Landroid/view/View;

    .line 40
    new-instance v1, Lco/uk/getmondo/pin/SmsDialogFragment_ViewBinding$2;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/pin/SmsDialogFragment_ViewBinding$2;-><init>(Lco/uk/getmondo/pin/SmsDialogFragment_ViewBinding;Lco/uk/getmondo/pin/SmsDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    const v0, 0x7f1102c9

    const-string v1, "method \'onNotNow\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 47
    iput-object v0, p0, Lco/uk/getmondo/pin/SmsDialogFragment_ViewBinding;->d:Landroid/view/View;

    .line 48
    new-instance v1, Lco/uk/getmondo/pin/SmsDialogFragment_ViewBinding$3;

    invoke-direct {v1, p0, p1}, Lco/uk/getmondo/pin/SmsDialogFragment_ViewBinding$3;-><init>(Lco/uk/getmondo/pin/SmsDialogFragment_ViewBinding;Lco/uk/getmondo/pin/SmsDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 59
    iget-object v0, p0, Lco/uk/getmondo/pin/SmsDialogFragment_ViewBinding;->a:Lco/uk/getmondo/pin/SmsDialogFragment;

    .line 60
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/pin/SmsDialogFragment_ViewBinding;->a:Lco/uk/getmondo/pin/SmsDialogFragment;

    .line 63
    iput-object v1, v0, Lco/uk/getmondo/pin/SmsDialogFragment;->textMeButton:Landroid/widget/Button;

    .line 65
    iget-object v0, p0, Lco/uk/getmondo/pin/SmsDialogFragment_ViewBinding;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    iput-object v1, p0, Lco/uk/getmondo/pin/SmsDialogFragment_ViewBinding;->b:Landroid/view/View;

    .line 67
    iget-object v0, p0, Lco/uk/getmondo/pin/SmsDialogFragment_ViewBinding;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    iput-object v1, p0, Lco/uk/getmondo/pin/SmsDialogFragment_ViewBinding;->c:Landroid/view/View;

    .line 69
    iget-object v0, p0, Lco/uk/getmondo/pin/SmsDialogFragment_ViewBinding;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    iput-object v1, p0, Lco/uk/getmondo/pin/SmsDialogFragment_ViewBinding;->d:Landroid/view/View;

    .line 71
    return-void
.end method
