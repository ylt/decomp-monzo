.class final synthetic Lco/uk/getmondo/pin/b;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnShowListener;


# instance fields
.field private final a:Lco/uk/getmondo/pin/DateOfBirthDialogFragment;

.field private final b:Landroid/app/AlertDialog;


# direct methods
.method private constructor <init>(Lco/uk/getmondo/pin/DateOfBirthDialogFragment;Landroid/app/AlertDialog;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/pin/b;->a:Lco/uk/getmondo/pin/DateOfBirthDialogFragment;

    iput-object p2, p0, Lco/uk/getmondo/pin/b;->b:Landroid/app/AlertDialog;

    return-void
.end method

.method public static a(Lco/uk/getmondo/pin/DateOfBirthDialogFragment;Landroid/app/AlertDialog;)Landroid/content/DialogInterface$OnShowListener;
    .locals 1

    new-instance v0, Lco/uk/getmondo/pin/b;

    invoke-direct {v0, p0, p1}, Lco/uk/getmondo/pin/b;-><init>(Lco/uk/getmondo/pin/DateOfBirthDialogFragment;Landroid/app/AlertDialog;)V

    return-object v0
.end method


# virtual methods
.method public onShow(Landroid/content/DialogInterface;)V
    .locals 2

    iget-object v0, p0, Lco/uk/getmondo/pin/b;->a:Lco/uk/getmondo/pin/DateOfBirthDialogFragment;

    iget-object v1, p0, Lco/uk/getmondo/pin/b;->b:Landroid/app/AlertDialog;

    invoke-static {v0, v1, p1}, Lco/uk/getmondo/pin/DateOfBirthDialogFragment;->a(Lco/uk/getmondo/pin/DateOfBirthDialogFragment;Landroid/app/AlertDialog;Landroid/content/DialogInterface;)V

    return-void
.end method
