.class public final Lco/uk/getmondo/welcome/WindowInsettingViewPager;
.super Landroid/support/v4/view/ViewPager;
.source "WindowInsettingViewPager.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/welcome/WindowInsettingViewPager$a;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0001\u0017B\u001b\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006J\u0012\u0010\u000c\u001a\u00020\r2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0014J\u0008\u0010\u0010\u001a\u00020\u000fH\u0014J\u0012\u0010\u0011\u001a\u00020\u000f2\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0016J\u0012\u0010\u0011\u001a\u00020\u000f2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0014J\u0018\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0015H\u0014R\"\u0010\t\u001a\u0004\u0018\u00010\u00082\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008@BX\u0082\u000e\u00a2\u0006\u0008\n\u0000\"\u0004\u0008\n\u0010\u000b\u00a8\u0006\u0018"
    }
    d2 = {
        "Lco/uk/getmondo/welcome/WindowInsettingViewPager;",
        "Landroid/support/v4/view/ViewPager;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "value",
        "Landroid/view/WindowInsets;",
        "lastInsets",
        "setLastInsets",
        "(Landroid/view/WindowInsets;)V",
        "checkLayoutParams",
        "",
        "p",
        "Landroid/view/ViewGroup$LayoutParams;",
        "generateDefaultLayoutParams",
        "generateLayoutParams",
        "onMeasure",
        "",
        "widthMeasureSpec",
        "",
        "heightMeasureSpec",
        "LayoutParams",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private d:Landroid/view/WindowInsets;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x2

    invoke-direct {p0, p1, v1, v0, v1}, Lco/uk/getmondo/welcome/WindowInsettingViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/d/b/i;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    new-instance v0, Lco/uk/getmondo/welcome/WindowInsettingViewPager$1;

    invoke-direct {v0, p0}, Lco/uk/getmondo/welcome/WindowInsettingViewPager$1;-><init>(Lco/uk/getmondo/welcome/WindowInsettingViewPager;)V

    check-cast v0, Landroid/view/View$OnApplyWindowInsetsListener;

    invoke-virtual {p0, v0}, Lco/uk/getmondo/welcome/WindowInsettingViewPager;->setOnApplyWindowInsetsListener(Landroid/view/View$OnApplyWindowInsetsListener;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/d/b/i;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_0

    .line 16
    const/4 v0, 0x0

    check-cast v0, Landroid/util/AttributeSet;

    :goto_0
    invoke-direct {p0, p1, v0}, Lco/uk/getmondo/welcome/WindowInsettingViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void

    :cond_0
    move-object v0, p2

    goto :goto_0
.end method

.method public static final synthetic a(Lco/uk/getmondo/welcome/WindowInsettingViewPager;Landroid/view/WindowInsets;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lco/uk/getmondo/welcome/WindowInsettingViewPager;->setLastInsets(Landroid/view/WindowInsets;)V

    return-void
.end method

.method private final setLastInsets(Landroid/view/WindowInsets;)V
    .locals 0

    .prologue
    .line 20
    iput-object p1, p0, Lco/uk/getmondo/welcome/WindowInsettingViewPager;->d:Landroid/view/WindowInsets;

    .line 21
    invoke-virtual {p0}, Lco/uk/getmondo/welcome/WindowInsettingViewPager;->requestLayout()V

    .line 22
    return-void
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 63
    instance-of v0, p1, Lco/uk/getmondo/welcome/WindowInsettingViewPager$a;

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lco/uk/getmondo/welcome/WindowInsettingViewPager$a;

    invoke-direct {v0}, Lco/uk/getmondo/welcome/WindowInsettingViewPager$a;-><init>()V

    check-cast v0, Landroid/view/ViewGroup$LayoutParams;

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 64
    new-instance v0, Landroid/support/v4/view/ViewPager$c;

    invoke-virtual {p0}, Lco/uk/getmondo/welcome/WindowInsettingViewPager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/support/v4/view/ViewPager$c;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    check-cast v0, Landroid/view/ViewGroup$LayoutParams;

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 65
    invoke-virtual {p0}, Lco/uk/getmondo/welcome/WindowInsettingViewPager;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected onMeasure(II)V
    .locals 10

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-virtual {p0}, Lco/uk/getmondo/welcome/WindowInsettingViewPager;->getChildCount()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_4

    .line 33
    invoke-virtual {p0, v1}, Lco/uk/getmondo/welcome/WindowInsettingViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 35
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v4, 0x8

    if-ne v0, v4, :cond_1

    .line 55
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 39
    :cond_1
    iget-object v4, p0, Lco/uk/getmondo/welcome/WindowInsettingViewPager;->d:Landroid/view/WindowInsets;

    .line 40
    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lco/uk/getmondo/welcome/WindowInsettingViewPager;->getFitsSystemWindows()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    invoke-virtual {v3}, Landroid/view/View;->getFitsSystemWindows()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 42
    iget-object v0, p0, Lco/uk/getmondo/welcome/WindowInsettingViewPager;->d:Landroid/view/WindowInsets;

    invoke-virtual {v3, v0}, Landroid/view/View;->dispatchApplyWindowInsets(Landroid/view/WindowInsets;)Landroid/view/WindowInsets;

    goto :goto_1

    .line 44
    :cond_2
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type co.uk.getmondo.welcome.WindowInsettingViewPager.LayoutParams"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    check-cast v0, Lco/uk/getmondo/welcome/WindowInsettingViewPager$a;

    .line 45
    invoke-virtual {v3}, Landroid/view/View;->getPaddingLeft()I

    move-result v5

    invoke-virtual {v0}, Lco/uk/getmondo/welcome/WindowInsettingViewPager$a;->a()I

    move-result v6

    sub-int/2addr v5, v6

    .line 46
    invoke-virtual {v3}, Landroid/view/View;->getPaddingTop()I

    move-result v6

    invoke-virtual {v0}, Lco/uk/getmondo/welcome/WindowInsettingViewPager$a;->b()I

    move-result v7

    sub-int/2addr v6, v7

    .line 47
    invoke-virtual {v3}, Landroid/view/View;->getPaddingRight()I

    move-result v7

    invoke-virtual {v0}, Lco/uk/getmondo/welcome/WindowInsettingViewPager$a;->d()I

    move-result v8

    sub-int/2addr v7, v8

    .line 48
    invoke-virtual {v3}, Landroid/view/View;->getPaddingBottom()I

    move-result v8

    invoke-virtual {v0}, Lco/uk/getmondo/welcome/WindowInsettingViewPager$a;->c()I

    move-result v9

    sub-int/2addr v8, v9

    .line 49
    invoke-virtual {v4}, Landroid/view/WindowInsets;->getSystemWindowInsetLeft()I

    move-result v9

    invoke-virtual {v0, v9}, Lco/uk/getmondo/welcome/WindowInsettingViewPager$a;->a(I)V

    .line 50
    invoke-virtual {v4}, Landroid/view/WindowInsets;->getSystemWindowInsetTop()I

    move-result v9

    invoke-virtual {v0, v9}, Lco/uk/getmondo/welcome/WindowInsettingViewPager$a;->b(I)V

    .line 51
    invoke-virtual {v4}, Landroid/view/WindowInsets;->getSystemWindowInsetRight()I

    move-result v9

    invoke-virtual {v0, v9}, Lco/uk/getmondo/welcome/WindowInsettingViewPager$a;->d(I)V

    .line 52
    invoke-virtual {v4}, Landroid/view/WindowInsets;->getSystemWindowInsetBottom()I

    move-result v4

    invoke-virtual {v0, v4}, Lco/uk/getmondo/welcome/WindowInsettingViewPager$a;->c(I)V

    .line 53
    invoke-virtual {v0}, Lco/uk/getmondo/welcome/WindowInsettingViewPager$a;->a()I

    move-result v4

    add-int/2addr v4, v5

    invoke-virtual {v0}, Lco/uk/getmondo/welcome/WindowInsettingViewPager$a;->b()I

    move-result v5

    add-int/2addr v5, v6

    .line 54
    invoke-virtual {v0}, Lco/uk/getmondo/welcome/WindowInsettingViewPager$a;->d()I

    move-result v6

    add-int/2addr v6, v7

    invoke-virtual {v0}, Lco/uk/getmondo/welcome/WindowInsettingViewPager$a;->c()I

    move-result v0

    add-int/2addr v0, v8

    .line 53
    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/view/View;->setPadding(IIII)V

    goto/16 :goto_1

    .line 59
    :cond_4
    invoke-super {p0, p1, p2}, Landroid/support/v4/view/ViewPager;->onMeasure(II)V

    .line 60
    return-void
.end method
