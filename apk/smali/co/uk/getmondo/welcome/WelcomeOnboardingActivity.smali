.class public Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "WelcomeOnboardingActivity.java"

# interfaces
.implements Lco/uk/getmondo/welcome/e$a;


# instance fields
.field a:Lco/uk/getmondo/welcome/e;

.field b:Lco/uk/getmondo/common/pager/h;

.field logInButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11010a
    .end annotation
.end field

.field mainActionButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110109
    .end annotation
.end field

.field viewPager:Landroid/support/v4/view/ViewPager;
    .annotation build Lbutterknife/BindView;
        value = 0x7f110108
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;Ljava/lang/Integer;)Ljava/lang/Boolean;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    iget-object v1, p0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/p;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/p;->b()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;Ljava/lang/Object;)Ljava/lang/Boolean;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 116
    iget-object v0, p0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    iget-object v1, p0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/p;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/p;->b()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 46
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 47
    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-static {v0}, Lcom/b/a/b/b/a/a;->a(Landroid/support/v4/view/ViewPager;)Lcom/b/a/a;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/welcome/b;->a(Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;)Lio/reactivex/c/h;

    move-result-object v1

    .line 110
    invoke-virtual {v0, v1}, Lcom/b/a/a;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 109
    return-object v0
.end method

.method public b()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->mainActionButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/welcome/c;->a(Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;)Lio/reactivex/c/h;

    move-result-object v1

    .line 116
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 115
    return-object v0
.end method

.method public c()Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    iget-object v0, p0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->logInButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->mainActionButton:Landroid/widget/Button;

    const v1, 0x7f0a044c

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 127
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->mainActionButton:Landroid/widget/Button;

    const v1, 0x7f0a0446

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 132
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    .line 136
    iget-object v0, p0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 137
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/p;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/p;->b()I

    move-result v0

    invoke-static {p0, v0}, Lco/uk/getmondo/signup_old/SignUpActivity;->a(Landroid/app/Activity;I)V

    .line 142
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 100
    iget-object v0, p0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    if-lez v0, :cond_0

    .line 101
    iget-object v0, p0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 105
    :goto_0
    return-void

    .line 103
    :cond_0
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/4 v11, 0x5

    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/high16 v7, 0x3f800000    # 1.0f

    .line 51
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 52
    invoke-virtual {p0}, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;)V

    .line 54
    const v0, 0x7f05001f

    invoke-virtual {p0, v0}, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->setContentView(I)V

    .line 55
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 57
    const/4 v0, 0x6

    new-array v0, v0, [Lco/uk/getmondo/common/pager/f;

    const/4 v1, 0x0

    new-instance v2, Lco/uk/getmondo/welcome/a;

    iget-object v3, p0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-direct {v2, v3}, Lco/uk/getmondo/welcome/a;-><init>(Landroid/support/v4/view/ViewPager;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lco/uk/getmondo/common/pager/e;

    const-string v3, "lottie/welcome_imagine.json"

    const v4, 0x7f0a044b

    const v5, 0x7f0a044a

    .line 60
    invoke-static {v8}, Lco/uk/getmondo/api/model/tracking/Impression;->b(I)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v6

    invoke-direct {v2, v3, v4, v5, v6}, Lco/uk/getmondo/common/pager/e;-><init>(Ljava/lang/String;IILco/uk/getmondo/api/model/tracking/Impression;)V

    aput-object v2, v0, v1

    new-instance v1, Lco/uk/getmondo/common/pager/d;

    const v2, 0x7f02020f

    const v3, 0x7f0a0445

    const v4, 0x7f0a0444

    .line 62
    invoke-static {v9}, Lco/uk/getmondo/api/model/tracking/Impression;->b(I)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lco/uk/getmondo/common/pager/d;-><init>(IIILco/uk/getmondo/api/model/tracking/Impression;)V

    aput-object v1, v0, v8

    new-instance v1, Lco/uk/getmondo/common/pager/d;

    const v2, 0x7f02020e

    const v3, 0x7f0a0443

    const v4, 0x7f0a0442

    .line 64
    invoke-static {v10}, Lco/uk/getmondo/api/model/tracking/Impression;->b(I)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lco/uk/getmondo/common/pager/d;-><init>(IIILco/uk/getmondo/api/model/tracking/Impression;)V

    aput-object v1, v0, v9

    new-instance v1, Lco/uk/getmondo/common/pager/b;

    const v2, 0x7f050125

    .line 65
    invoke-static {v11}, Lco/uk/getmondo/api/model/tracking/Impression;->b(I)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lco/uk/getmondo/common/pager/b;-><init>(ILco/uk/getmondo/api/model/tracking/Impression;)V

    aput-object v1, v0, v10

    new-instance v1, Lco/uk/getmondo/common/pager/d;

    const v2, 0x7f020211

    const v3, 0x7f0a0449

    const v4, 0x7f0a0448

    const/4 v5, 0x6

    .line 67
    invoke-static {v5}, Lco/uk/getmondo/api/model/tracking/Impression;->b(I)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lco/uk/getmondo/common/pager/d;-><init>(IIILco/uk/getmondo/api/model/tracking/Impression;)V

    aput-object v1, v0, v11

    .line 57
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 71
    iget-object v1, p0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    new-instance v2, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity$1;

    invoke-direct {v2, p0, v0}, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity$1;-><init>(Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/p;)V

    .line 83
    iget-object v0, p0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->b:Lco/uk/getmondo/common/pager/h;

    iget-object v1, p0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/pager/h;->a(Landroid/support/v4/view/ViewPager;)V

    .line 85
    iget-object v0, p0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->a:Lco/uk/getmondo/welcome/e;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/welcome/e;->a(Lco/uk/getmondo/welcome/e$a;)V

    .line 87
    iget-object v0, p0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->mainActionButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x4b0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 88
    iget-object v0, p0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->logInButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x4b0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 89
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->b:Lco/uk/getmondo/common/pager/h;

    invoke-virtual {v0}, Lco/uk/getmondo/common/pager/h;->a()V

    .line 94
    iget-object v0, p0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->a:Lco/uk/getmondo/welcome/e;

    invoke-virtual {v0}, Lco/uk/getmondo/welcome/e;->b()V

    .line 95
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 96
    return-void
.end method
