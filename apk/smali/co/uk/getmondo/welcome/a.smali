.class public final Lco/uk/getmondo/welcome/a;
.super Ljava/lang/Object;
.source "VideoViewPage.kt"

# interfaces
.implements Lco/uk/getmondo/common/pager/f$b;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\nH\u0016J\u0008\u0010\u000b\u001a\u00020\u000cH\u0016R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lco/uk/getmondo/welcome/VideoViewPage;",
        "Lco/uk/getmondo/common/pager/Page$CustomView;",
        "viewPager",
        "Landroid/support/v4/view/ViewPager;",
        "(Landroid/support/v4/view/ViewPager;)V",
        "videoImageView",
        "Lco/uk/getmondo/common/ui/VideoImageView;",
        "createView",
        "Landroid/view/View;",
        "destroyView",
        "",
        "getOnPageViewedImpression",
        "Lco/uk/getmondo/api/model/tracking/Impression;",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private a:Lco/uk/getmondo/common/ui/VideoImageView;

.field private final b:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>(Landroid/support/v4/view/ViewPager;)V
    .locals 1

    .prologue
    const-string v0, "viewPager"

    invoke-static {p1, v0}, Lkotlin/d/b/l;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lco/uk/getmondo/welcome/a;->b:Landroid/support/v4/view/ViewPager;

    return-void
.end method


# virtual methods
.method public a()Lco/uk/getmondo/api/model/tracking/Impression;
    .locals 2

    .prologue
    .line 51
    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression;->Companion:Lco/uk/getmondo/api/model/tracking/Impression$Companion;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/api/model/tracking/Impression$Companion;->b(I)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lco/uk/getmondo/welcome/a;->a:Lco/uk/getmondo/common/ui/VideoImageView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lco/uk/getmondo/common/ui/VideoImageView;->a()V

    .line 48
    :cond_0
    const/4 v0, 0x0

    check-cast v0, Lco/uk/getmondo/common/ui/VideoImageView;

    iput-object v0, p0, Lco/uk/getmondo/welcome/a;->a:Lco/uk/getmondo/common/ui/VideoImageView;

    .line 49
    return-void
.end method

.method public c()Landroid/view/View;
    .locals 5

    .prologue
    .line 18
    iget-object v0, p0, Lco/uk/getmondo/welcome/a;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 19
    const v2, 0x7f050124

    iget-object v0, p0, Lco/uk/getmondo/welcome/a;->b:Landroid/support/v4/view/ViewPager;

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 21
    const v0, 0x7f11045c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/common/ui/VideoImageView;

    iput-object v0, p0, Lco/uk/getmondo/welcome/a;->a:Lco/uk/getmondo/common/ui/VideoImageView;

    .line 23
    const v0, 0x7f11045d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 24
    const v0, 0x7f11045e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/airbnb/lottie/LottieAnimationView;

    .line 28
    const v1, 0x7f11045b

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    sget-object v1, Lco/uk/getmondo/welcome/a$a;->a:Lco/uk/getmondo/welcome/a$a;

    check-cast v1, Landroid/view/View$OnApplyWindowInsetsListener;

    invoke-virtual {v4, v1}, Landroid/view/View;->setOnApplyWindowInsetsListener(Landroid/view/View$OnApplyWindowInsetsListener;)V

    .line 32
    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 33
    const/16 v0, 0x8

    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 41
    :goto_0
    const-string v0, "view"

    invoke-static {v2, v0}, Lkotlin/d/b/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v2

    .line 35
    :cond_0
    new-instance v1, Lco/uk/getmondo/welcome/a$b;

    invoke-direct {v1, v3}, Lco/uk/getmondo/welcome/a$b;-><init>(Landroid/view/View;)V

    check-cast v1, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->a(Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0
.end method
