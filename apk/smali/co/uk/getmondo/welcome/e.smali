.class Lco/uk/getmondo/welcome/e;
.super Lco/uk/getmondo/common/ui/b;
.source "WelcomeOnboardingPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lco/uk/getmondo/welcome/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lco/uk/getmondo/common/ui/b",
        "<",
        "Lco/uk/getmondo/welcome/e$a;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lco/uk/getmondo/common/ui/b;-><init>()V

    .line 15
    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/welcome/e$a;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 35
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    invoke-interface {p0}, Lco/uk/getmondo/welcome/e$a;->d()V

    .line 40
    :goto_0
    return-void

    .line 38
    :cond_0
    invoke-interface {p0}, Lco/uk/getmondo/welcome/e$a;->e()V

    goto :goto_0
.end method

.method static synthetic a(Lco/uk/getmondo/welcome/e$a;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 31
    invoke-interface {p0}, Lco/uk/getmondo/welcome/e$a;->g()V

    return-void
.end method

.method static synthetic b(Lco/uk/getmondo/welcome/e$a;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 23
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24
    invoke-interface {p0}, Lco/uk/getmondo/welcome/e$a;->g()V

    .line 28
    :goto_0
    return-void

    .line 26
    :cond_0
    invoke-interface {p0}, Lco/uk/getmondo/welcome/e$a;->f()V

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic a(Lco/uk/getmondo/common/ui/f;)V
    .locals 0

    .prologue
    .line 10
    check-cast p1, Lco/uk/getmondo/welcome/e$a;

    invoke-virtual {p0, p1}, Lco/uk/getmondo/welcome/e;->a(Lco/uk/getmondo/welcome/e$a;)V

    return-void
.end method

.method public a(Lco/uk/getmondo/welcome/e$a;)V
    .locals 2

    .prologue
    .line 19
    invoke-super {p0, p1}, Lco/uk/getmondo/common/ui/b;->a(Lco/uk/getmondo/common/ui/f;)V

    .line 21
    invoke-interface {p1}, Lco/uk/getmondo/welcome/e$a;->b()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/welcome/f;->a(Lco/uk/getmondo/welcome/e$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 22
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 21
    invoke-virtual {p0, v0}, Lco/uk/getmondo/welcome/e;->a(Lio/reactivex/b/b;)V

    .line 30
    invoke-interface {p1}, Lco/uk/getmondo/welcome/e$a;->c()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/welcome/g;->a(Lco/uk/getmondo/welcome/e$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 31
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 30
    invoke-virtual {p0, v0}, Lco/uk/getmondo/welcome/e;->a(Lio/reactivex/b/b;)V

    .line 33
    invoke-interface {p1}, Lco/uk/getmondo/welcome/e$a;->a()Lio/reactivex/n;

    move-result-object v0

    invoke-static {p1}, Lco/uk/getmondo/welcome/h;->a(Lco/uk/getmondo/welcome/e$a;)Lio/reactivex/c/g;

    move-result-object v1

    .line 34
    invoke-virtual {v0, v1}, Lio/reactivex/n;->subscribe(Lio/reactivex/c/g;)Lio/reactivex/b/b;

    move-result-object v0

    .line 33
    invoke-virtual {p0, v0}, Lco/uk/getmondo/welcome/e;->a(Lio/reactivex/b/b;)V

    .line 41
    return-void
.end method
