.class public final Lco/uk/getmondo/welcome/WindowInsettingViewPager$a;
.super Landroid/support/v4/view/ViewPager$c;
.source "WindowInsettingViewPager.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lco/uk/getmondo/welcome/WindowInsettingViewPager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x2
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u000e\u0018\u00002\u00020\u0001B\u0007\u0008\u0016\u00a2\u0006\u0002\u0010\u0002B\u0017\u0008\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007R\u001a\u0010\u0008\u001a\u00020\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\n\u0010\u000b\"\u0004\u0008\u000c\u0010\rR\u001a\u0010\u000e\u001a\u00020\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000f\u0010\u000b\"\u0004\u0008\u0010\u0010\rR\u001a\u0010\u0011\u001a\u00020\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0012\u0010\u000b\"\u0004\u0008\u0013\u0010\rR\u001a\u0010\u0014\u001a\u00020\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0015\u0010\u000b\"\u0004\u0008\u0016\u0010\r\u00a8\u0006\u0017"
    }
    d2 = {
        "Lco/uk/getmondo/welcome/WindowInsettingViewPager$LayoutParams;",
        "Landroid/support/v4/view/ViewPager$LayoutParams;",
        "()V",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "offsetBottom",
        "",
        "getOffsetBottom",
        "()I",
        "setOffsetBottom",
        "(I)V",
        "offsetLeft",
        "getOffsetLeft",
        "setOffsetLeft",
        "offsetRight",
        "getOffsetRight",
        "setOffsetRight",
        "offsetTop",
        "getOffsetTop",
        "setOffsetTop",
        "app_monzoPrepaidRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x7
    }
.end annotation


# instance fields
.field private g:I

.field private h:I

.field private i:I

.field private j:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager$c;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lco/uk/getmondo/welcome/WindowInsettingViewPager$a;->g:I

    return v0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 68
    iput p1, p0, Lco/uk/getmondo/welcome/WindowInsettingViewPager$a;->g:I

    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lco/uk/getmondo/welcome/WindowInsettingViewPager$a;->h:I

    return v0
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 69
    iput p1, p0, Lco/uk/getmondo/welcome/WindowInsettingViewPager$a;->h:I

    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lco/uk/getmondo/welcome/WindowInsettingViewPager$a;->i:I

    return v0
.end method

.method public final c(I)V
    .locals 0

    .prologue
    .line 70
    iput p1, p0, Lco/uk/getmondo/welcome/WindowInsettingViewPager$a;->i:I

    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lco/uk/getmondo/welcome/WindowInsettingViewPager$a;->j:I

    return v0
.end method

.method public final d(I)V
    .locals 0

    .prologue
    .line 71
    iput p1, p0, Lco/uk/getmondo/welcome/WindowInsettingViewPager$a;->j:I

    return-void
.end method
