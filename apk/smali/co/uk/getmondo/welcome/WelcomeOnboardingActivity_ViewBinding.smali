.class public Lco/uk/getmondo/welcome/WelcomeOnboardingActivity_ViewBinding;
.super Ljava/lang/Object;
.source "WelcomeOnboardingActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private a:Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity_ViewBinding;->a:Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;

    .line 27
    const v0, 0x7f110108

    const-string v1, "field \'viewPager\'"

    const-class v2, Landroid/support/v4/view/ViewPager;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p1, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    .line 28
    const v0, 0x7f110109

    const-string v1, "field \'mainActionButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->mainActionButton:Landroid/widget/Button;

    .line 29
    const v0, 0x7f11010a

    const-string v1, "field \'logInButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->logInButton:Landroid/widget/Button;

    .line 30
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    iget-object v0, p0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity_ViewBinding;->a:Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;

    .line 36
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_0
    iput-object v1, p0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity_ViewBinding;->a:Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;

    .line 39
    iput-object v1, v0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    .line 40
    iput-object v1, v0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->mainActionButton:Landroid/widget/Button;

    .line 41
    iput-object v1, v0, Lco/uk/getmondo/welcome/WelcomeOnboardingActivity;->logInButton:Landroid/widget/Button;

    .line 42
    return-void
.end method
