.class public Lco/uk/getmondo/b/a;
.super Ljava/lang/Object;
.source "AppAnalyticsTracker.java"


# instance fields
.field private final a:Landroid/support/v4/b/b/a;

.field private final b:Lco/uk/getmondo/common/a;


# direct methods
.method constructor <init>(Landroid/support/v4/b/b/a;Lco/uk/getmondo/common/a;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lco/uk/getmondo/b/a;->a:Landroid/support/v4/b/b/a;

    .line 25
    iput-object p2, p0, Lco/uk/getmondo/b/a;->b:Lco/uk/getmondo/common/a;

    .line 26
    return-void
.end method

.method private a()Lco/uk/getmondo/api/model/tracking/Impression$FingerprintSupportType;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lco/uk/getmondo/b/a;->a:Landroid/support/v4/b/b/a;

    invoke-virtual {v0}, Landroid/support/v4/b/b/a;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 41
    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression$FingerprintSupportType;->NO_HARDWARE_DETECTED:Lco/uk/getmondo/api/model/tracking/Impression$FingerprintSupportType;

    .line 47
    :goto_0
    return-object v0

    .line 42
    :cond_0
    iget-object v0, p0, Lco/uk/getmondo/b/a;->a:Landroid/support/v4/b/b/a;

    invoke-virtual {v0}, Landroid/support/v4/b/b/a;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 43
    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression$FingerprintSupportType;->NO_ENROLLED_FINGERPRINTS:Lco/uk/getmondo/api/model/tracking/Impression$FingerprintSupportType;

    goto :goto_0

    .line 45
    :cond_1
    sget-object v0, Lco/uk/getmondo/api/model/tracking/Impression$FingerprintSupportType;->READY_TO_USE:Lco/uk/getmondo/api/model/tracking/Impression$FingerprintSupportType;

    goto :goto_0
.end method

.method private b(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 51
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    .line 52
    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 53
    invoke-virtual {v0}, Landroid/app/NotificationManager;->getActiveNotifications()[Landroid/service/notification/StatusBarNotification;

    move-result-object v0

    array-length v0, v0

    .line 55
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private c(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 59
    const-string v0, "nfc"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/nfc/NfcManager;

    .line 60
    invoke-virtual {v0}, Landroid/nfc/NfcManager;->getDefaultAdapter()Landroid/nfc/NfcAdapter;

    move-result-object v0

    .line 61
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/nfc/NfcAdapter;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 30
    :try_start_0
    invoke-direct {p0}, Lco/uk/getmondo/b/a;->a()Lco/uk/getmondo/api/model/tracking/Impression$FingerprintSupportType;

    move-result-object v0

    invoke-direct {p0, p1}, Lco/uk/getmondo/b/a;->b(Landroid/content/Context;)I

    move-result v1

    .line 31
    invoke-direct {p0, p1}, Lco/uk/getmondo/b/a;->c(Landroid/content/Context;)Z

    move-result v2

    .line 30
    invoke-static {v0, v1, v2}, Lco/uk/getmondo/api/model/tracking/Impression;->a(Lco/uk/getmondo/api/model/tracking/Impression$FingerprintSupportType;IZ)Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v0

    .line 32
    iget-object v1, p0, Lco/uk/getmondo/b/a;->b:Lco/uk/getmondo/common/a;

    invoke-virtual {v1, v0}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    :goto_0
    return-void

    .line 33
    :catch_0
    move-exception v0

    .line 34
    const-string v1, "Failed to send analytic"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Ld/a/a;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
