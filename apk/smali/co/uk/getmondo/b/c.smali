.class public Lco/uk/getmondo/b/c;
.super Ljava/lang/Object;
.source "LifecycleAnalyticsTracker.java"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# instance fields
.field private final a:Lco/uk/getmondo/common/a;

.field private b:I


# direct methods
.method constructor <init>(Lco/uk/getmondo/common/a;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lco/uk/getmondo/b/c;->b:I

    .line 21
    iput-object p1, p0, Lco/uk/getmondo/b/c;->a:Lco/uk/getmondo/common/a;

    .line 22
    return-void
.end method

.method private a(Landroid/app/Activity;)Z
    .locals 2

    .prologue
    .line 73
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "co.uk.getmondo"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 27
    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 47
    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 42
    return-void
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 64
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 31
    iget v0, p0, Lco/uk/getmondo/b/c;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lco/uk/getmondo/b/c;->b:I

    .line 32
    iget v0, p0, Lco/uk/getmondo/b/c;->b:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 33
    invoke-direct {p0, p1}, Lco/uk/getmondo/b/c;->a(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lco/uk/getmondo/b/c;->a:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->b()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 37
    :cond_0
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 51
    iget v0, p0, Lco/uk/getmondo/b/c;->b:I

    if-lez v0, :cond_0

    .line 52
    iget v0, p0, Lco/uk/getmondo/b/c;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lco/uk/getmondo/b/c;->b:I

    .line 54
    :cond_0
    iget v0, p0, Lco/uk/getmondo/b/c;->b:I

    if-nez v0, :cond_1

    .line 55
    invoke-direct {p0, p1}, Lco/uk/getmondo/b/c;->a(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 56
    iget-object v0, p0, Lco/uk/getmondo/b/c;->a:Lco/uk/getmondo/common/a;

    invoke-static {}, Lco/uk/getmondo/api/model/tracking/Impression;->c()Lco/uk/getmondo/api/model/tracking/Impression;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco/uk/getmondo/common/a;->a(Lco/uk/getmondo/api/model/tracking/Impression;)V

    .line 59
    :cond_1
    return-void
.end method
