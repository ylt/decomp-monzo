.class public Lco/uk/getmondo/a/a;
.super Ljava/lang/Object;
.source "BalanceRepository.java"


# instance fields
.field private final a:Lco/uk/getmondo/api/MonzoApi;

.field private final b:Lco/uk/getmondo/a/e;


# direct methods
.method public constructor <init>(Lco/uk/getmondo/api/MonzoApi;Lco/uk/getmondo/a/e;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lco/uk/getmondo/a/a;->a:Lco/uk/getmondo/api/MonzoApi;

    .line 20
    iput-object p2, p0, Lco/uk/getmondo/a/a;->b:Lco/uk/getmondo/a/e;

    .line 21
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/d/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lco/uk/getmondo/a/a;->b:Lco/uk/getmondo/a/e;

    invoke-virtual {v0, p1}, Lco/uk/getmondo/a/e;->a(Ljava/lang/String;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lio/reactivex/b;
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lco/uk/getmondo/a/a;->a:Lco/uk/getmondo/api/MonzoApi;

    invoke-interface {v0, p1}, Lco/uk/getmondo/api/MonzoApi;->balance(Ljava/lang/String;)Lio/reactivex/v;

    move-result-object v0

    new-instance v1, Lco/uk/getmondo/d/a/d;

    invoke-direct {v1, p1}, Lco/uk/getmondo/d/a/d;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lco/uk/getmondo/a/b;->a(Lco/uk/getmondo/d/a/d;)Lio/reactivex/c/h;

    move-result-object v1

    .line 37
    invoke-virtual {v0, v1}, Lio/reactivex/v;->d(Lio/reactivex/c/h;)Lio/reactivex/v;

    move-result-object v0

    iget-object v1, p0, Lco/uk/getmondo/a/a;->b:Lco/uk/getmondo/a/e;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lco/uk/getmondo/a/c;->a(Lco/uk/getmondo/a/e;)Lio/reactivex/c/h;

    move-result-object v1

    .line 38
    invoke-virtual {v0, v1}, Lio/reactivex/v;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v0

    .line 36
    return-object v0
.end method
