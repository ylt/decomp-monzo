.class Lco/uk/getmondo/a/e;
.super Ljava/lang/Object;
.source "BalanceStorage.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method

.method static synthetic a(Ljava/lang/String;Lio/realm/av;)Lio/realm/bg;
    .locals 2

    .prologue
    .line 23
    const-class v0, Lco/uk/getmondo/d/b;

    invoke-virtual {p1, v0}, Lio/realm/av;->a(Ljava/lang/Class;)Lio/realm/bf;

    move-result-object v0

    const-string v1, "accountId"

    .line 24
    invoke-virtual {v0, v1, p0}, Lio/realm/bf;->a(Ljava/lang/String;Ljava/lang/String;)Lio/realm/bf;

    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lio/realm/bf;->g()Lio/realm/bg;

    move-result-object v0

    .line 23
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/d/b;Lio/realm/av;)V
    .locals 0

    .prologue
    .line 32
    invoke-virtual {p1, p0}, Lio/realm/av;->d(Lio/realm/bb;)V

    return-void
.end method


# virtual methods
.method a(Lco/uk/getmondo/d/b;)Lio/reactivex/b;
    .locals 1

    .prologue
    .line 32
    invoke-static {p1}, Lco/uk/getmondo/a/i;->a(Lco/uk/getmondo/d/b;)Lio/realm/av$a;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/j/g;->a(Lio/realm/av$a;)Lio/reactivex/b;

    move-result-object v0

    return-object v0
.end method

.method a(Ljava/lang/String;)Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/n",
            "<",
            "Lco/uk/getmondo/d/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 23
    invoke-static {p1}, Lco/uk/getmondo/a/f;->a(Ljava/lang/String;)Lkotlin/d/a/b;

    move-result-object v0

    invoke-static {v0}, Lco/uk/getmondo/common/j/g;->a(Lkotlin/d/a/b;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/a/g;->a()Lio/reactivex/c/q;

    move-result-object v1

    .line 26
    invoke-virtual {v0, v1}, Lio/reactivex/n;->filter(Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {}, Lco/uk/getmondo/a/h;->a()Lio/reactivex/c/h;

    move-result-object v1

    .line 27
    invoke-virtual {v0, v1}, Lio/reactivex/n;->map(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 28
    invoke-virtual {v0}, Lio/reactivex/n;->distinctUntilChanged()Lio/reactivex/n;

    move-result-object v0

    .line 23
    return-object v0
.end method
