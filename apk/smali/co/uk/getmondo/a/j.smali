.class public final enum Lco/uk/getmondo/a/j;
.super Ljava/lang/Enum;
.source "BalanceStorage_Factory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lco/uk/getmondo/a/j;",
        ">;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/a/e;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lco/uk/getmondo/a/j;

.field private static final synthetic b:[Lco/uk/getmondo/a/j;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 11
    new-instance v0, Lco/uk/getmondo/a/j;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lco/uk/getmondo/a/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lco/uk/getmondo/a/j;->a:Lco/uk/getmondo/a/j;

    .line 6
    const/4 v0, 0x1

    new-array v0, v0, [Lco/uk/getmondo/a/j;

    sget-object v1, Lco/uk/getmondo/a/j;->a:Lco/uk/getmondo/a/j;

    aput-object v1, v0, v2

    sput-object v0, Lco/uk/getmondo/a/j;->b:[Lco/uk/getmondo/a/j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static c()Lb/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/a/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 19
    sget-object v0, Lco/uk/getmondo/a/j;->a:Lco/uk/getmondo/a/j;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lco/uk/getmondo/a/j;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lco/uk/getmondo/a/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/a/j;

    return-object v0
.end method

.method public static values()[Lco/uk/getmondo/a/j;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lco/uk/getmondo/a/j;->b:[Lco/uk/getmondo/a/j;

    invoke-virtual {v0}, [Lco/uk/getmondo/a/j;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lco/uk/getmondo/a/j;

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/a/e;
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lco/uk/getmondo/a/e;

    invoke-direct {v0}, Lco/uk/getmondo/a/e;-><init>()V

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0}, Lco/uk/getmondo/a/j;->a()Lco/uk/getmondo/a/e;

    move-result-object v0

    return-object v0
.end method
