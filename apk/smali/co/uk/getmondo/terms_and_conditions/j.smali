.class public final Lco/uk/getmondo/terms_and_conditions/j;
.super Ljava/lang/Object;
.source "TermsAndConditionsPresenter_Factory.java"

# interfaces
.implements Lb/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb/a/b",
        "<",
        "Lco/uk/getmondo/terms_and_conditions/g;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lb/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a",
            "<",
            "Lco/uk/getmondo/terms_and_conditions/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-class v0, Lco/uk/getmondo/terms_and_conditions/j;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lco/uk/getmondo/terms_and_conditions/j;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lb/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/terms_and_conditions/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    sget-boolean v0, Lco/uk/getmondo/terms_and_conditions/j;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 20
    :cond_0
    iput-object p1, p0, Lco/uk/getmondo/terms_and_conditions/j;->b:Lb/a;

    .line 21
    return-void
.end method

.method public static a(Lb/a;)Lb/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb/a",
            "<",
            "Lco/uk/getmondo/terms_and_conditions/g;",
            ">;)",
            "Lb/a/b",
            "<",
            "Lco/uk/getmondo/terms_and_conditions/g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    new-instance v0, Lco/uk/getmondo/terms_and_conditions/j;

    invoke-direct {v0, p0}, Lco/uk/getmondo/terms_and_conditions/j;-><init>(Lb/a;)V

    return-object v0
.end method


# virtual methods
.method public a()Lco/uk/getmondo/terms_and_conditions/g;
    .locals 2

    .prologue
    .line 25
    iget-object v0, p0, Lco/uk/getmondo/terms_and_conditions/j;->b:Lb/a;

    new-instance v1, Lco/uk/getmondo/terms_and_conditions/g;

    invoke-direct {v1}, Lco/uk/getmondo/terms_and_conditions/g;-><init>()V

    invoke-static {v0, v1}, Lb/a/c;->a(Lb/a;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lco/uk/getmondo/terms_and_conditions/g;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8
    invoke-virtual {p0}, Lco/uk/getmondo/terms_and_conditions/j;->a()Lco/uk/getmondo/terms_and_conditions/g;

    move-result-object v0

    return-object v0
.end method
