.class public Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;
.super Lco/uk/getmondo/common/activities/b;
.source "TermsAndConditionsActivity.java"

# interfaces
.implements Lco/uk/getmondo/terms_and_conditions/g$a;


# instance fields
.field a:Lco/uk/getmondo/terms_and_conditions/g;

.field nextPageButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11023c
    .end annotation
.end field

.field termsWebView:Landroid/webkit/WebView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f11023b
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lco/uk/getmondo/common/activities/b;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 36
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 40
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 41
    const-string v1, "EXTRA_OVERRIDE_URL"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 42
    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;Ljava/lang/Object;)Lio/reactivex/r;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 89
    invoke-static {p0}, Lco/uk/getmondo/terms_and_conditions/d;->a(Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;)Lio/reactivex/p;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/n;->create(Lio/reactivex/p;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;Lio/reactivex/o;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;->termsWebView:Landroid/webkit/WebView;

    const-string v1, "window.isLastArticle()"

    invoke-static {p0, p1}, Lco/uk/getmondo/terms_and_conditions/e;->a(Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;Lio/reactivex/o;)Landroid/webkit/ValueCallback;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->evaluateJavascript(Ljava/lang/String;Landroid/webkit/ValueCallback;)V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;Lio/reactivex/o;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0, p2}, Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;->a(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lio/reactivex/o;->a(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 96
    iget-object v0, p0, Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;->termsWebView:Landroid/webkit/WebView;

    const-string v1, "window.isLastArticle()"

    invoke-static {p0}, Lco/uk/getmondo/terms_and_conditions/c;->a(Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;)Landroid/webkit/ValueCallback;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->evaluateJavascript(Ljava/lang/String;Landroid/webkit/ValueCallback;)V

    return-void
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 104
    const-string v0, "true"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;->nextPageButton:Landroid/widget/Button;

    const v1, 0x7f0a03c9

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 100
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    iget-object v0, p0, Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;->nextPageButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/b/a/c/c;->a(Landroid/view/View;)Lio/reactivex/n;

    move-result-object v0

    invoke-static {p0}, Lco/uk/getmondo/terms_and_conditions/a;->a(Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;)Lio/reactivex/c/h;

    move-result-object v1

    .line 89
    invoke-virtual {v0, v1}, Lio/reactivex/n;->flatMap(Lio/reactivex/c/h;)Lio/reactivex/n;

    move-result-object v0

    .line 88
    return-object v0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 95
    iget-object v0, p0, Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;->termsWebView:Landroid/webkit/WebView;

    const-string v1, "window.nextArticle()"

    invoke-static {p0}, Lco/uk/getmondo/terms_and_conditions/b;->a(Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;)Landroid/webkit/ValueCallback;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->evaluateJavascript(Ljava/lang/String;Landroid/webkit/ValueCallback;)V

    .line 101
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    .line 48
    invoke-super {p0, p1}, Lco/uk/getmondo/common/activities/b;->onCreate(Landroid/os/Bundle;)V

    .line 50
    const v0, 0x7f050068

    invoke-virtual {p0, v0}, Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;->setContentView(I)V

    .line 51
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    .line 52
    invoke-virtual {p0}, Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;->l()Lco/uk/getmondo/common/h/b/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lco/uk/getmondo/common/h/b/b;->a(Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;)V

    .line 54
    invoke-virtual {p0}, Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;->getSupportActionBar()Landroid/support/v7/app/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->b(Z)V

    .line 56
    iget-object v0, p0, Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;->termsWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 57
    iget-object v0, p0, Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;->termsWebView:Landroid/webkit/WebView;

    new-instance v1, Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity$1;

    invoke-direct {v1, p0}, Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity$1;-><init>(Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 70
    const-string v0, "https://monzo.com/-webviews/terms-and-conditions/"

    .line 71
    invoke-virtual {p0}, Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "EXTRA_OVERRIDE_URL"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 72
    invoke-virtual {p0}, Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_OVERRIDE_URL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 74
    :cond_0
    iget-object v1, p0, Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;->termsWebView:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 76
    iget-object v0, p0, Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;->a:Lco/uk/getmondo/terms_and_conditions/g;

    invoke-virtual {v0, p0}, Lco/uk/getmondo/terms_and_conditions/g;->a(Lco/uk/getmondo/terms_and_conditions/g$a;)V

    .line 77
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lco/uk/getmondo/terms_and_conditions/TermsAndConditionsActivity;->a:Lco/uk/getmondo/terms_and_conditions/g;

    invoke-virtual {v0}, Lco/uk/getmondo/terms_and_conditions/g;->b()V

    .line 83
    invoke-super {p0}, Lco/uk/getmondo/common/activities/b;->onDestroy()V

    .line 84
    return-void
.end method
