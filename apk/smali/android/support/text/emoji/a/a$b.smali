.class Landroid/support/text/emoji/a/a$b;
.super Ljava/lang/Object;
.source "BundledEmojiCompatConfig.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/text/emoji/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field private final a:Landroid/support/text/emoji/a$g;

.field private final b:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/support/text/emoji/a$g;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-object p1, p0, Landroid/support/text/emoji/a/a$b;->b:Landroid/content/Context;

    .line 73
    iput-object p2, p0, Landroid/support/text/emoji/a/a$b;->a:Landroid/support/text/emoji/a$g;

    .line 74
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Landroid/support/text/emoji/a$g;Landroid/support/text/emoji/a/a$1;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Landroid/support/text/emoji/a/a$b;-><init>(Landroid/content/Context;Landroid/support/text/emoji/a$g;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 79
    :try_start_0
    iget-object v0, p0, Landroid/support/text/emoji/a/a$b;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 80
    const-string v1, "NotoColorEmojiCompat.ttf"

    invoke-static {v0, v1}, Landroid/support/text/emoji/f;->a(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/support/text/emoji/f;

    move-result-object v0

    .line 81
    iget-object v1, p0, Landroid/support/text/emoji/a/a$b;->a:Landroid/support/text/emoji/a$g;

    invoke-virtual {v1, v0}, Landroid/support/text/emoji/a$g;->a(Landroid/support/text/emoji/f;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    :goto_0
    return-void

    .line 82
    :catch_0
    move-exception v0

    .line 83
    iget-object v1, p0, Landroid/support/text/emoji/a/a$b;->a:Landroid/support/text/emoji/a$g;

    invoke-virtual {v1, v0}, Landroid/support/text/emoji/a$g;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
