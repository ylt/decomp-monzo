.class Landroid/support/text/emoji/a/a$a;
.super Ljava/lang/Object;
.source "BundledEmojiCompatConfig.java"

# interfaces
.implements Landroid/support/text/emoji/a$f;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/text/emoji/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Landroid/support/text/emoji/a/a$a;->a:Landroid/content/Context;

    .line 51
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Landroid/support/text/emoji/a/a$1;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1}, Landroid/support/text/emoji/a/a$a;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/support/text/emoji/a$g;)V
    .locals 3

    .prologue
    .line 56
    const-string v0, "loaderCallback cannot be null"

    invoke-static {p1, v0}, Landroid/support/v4/g/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    new-instance v0, Landroid/support/text/emoji/a/a$b;

    iget-object v1, p0, Landroid/support/text/emoji/a/a$a;->a:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Landroid/support/text/emoji/a/a$b;-><init>(Landroid/content/Context;Landroid/support/text/emoji/a$g;Landroid/support/text/emoji/a/a$1;)V

    .line 58
    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 59
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 60
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 61
    return-void
.end method
