.class Landroid/support/text/emoji/f$a;
.super Ljava/lang/Object;
.source "MetadataRepo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/text/emoji/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/support/text/emoji/f$a;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/support/text/emoji/b;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 214
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/text/emoji/f$a;-><init>(I)V

    .line 215
    return-void
.end method

.method private constructor <init>(I)V
    .locals 1

    .prologue
    .line 217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 218
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, p1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Landroid/support/text/emoji/f$a;->a:Landroid/util/SparseArray;

    .line 219
    return-void
.end method

.method synthetic constructor <init>(ILandroid/support/text/emoji/f$1;)V
    .locals 0

    .prologue
    .line 209
    invoke-direct {p0, p1}, Landroid/support/text/emoji/f$a;-><init>(I)V

    return-void
.end method

.method private a(Landroid/support/text/emoji/b;II)V
    .locals 3

    .prologue
    .line 230
    invoke-virtual {p1, p2}, Landroid/support/text/emoji/b;->a(I)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/text/emoji/f$a;->a(I)Landroid/support/text/emoji/f$a;

    move-result-object v0

    .line 231
    if-nez v0, :cond_0

    .line 232
    new-instance v0, Landroid/support/text/emoji/f$a;

    invoke-direct {v0}, Landroid/support/text/emoji/f$a;-><init>()V

    .line 233
    iget-object v1, p0, Landroid/support/text/emoji/f$a;->a:Landroid/util/SparseArray;

    invoke-virtual {p1, p2}, Landroid/support/text/emoji/b;->a(I)I

    move-result v2

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 236
    :cond_0
    if-le p3, p2, :cond_1

    .line 237
    add-int/lit8 v1, p2, 0x1

    invoke-direct {v0, p1, v1, p3}, Landroid/support/text/emoji/f$a;->a(Landroid/support/text/emoji/b;II)V

    .line 241
    :goto_0
    return-void

    .line 239
    :cond_1
    iput-object p1, v0, Landroid/support/text/emoji/f$a;->b:Landroid/support/text/emoji/b;

    goto :goto_0
.end method

.method static synthetic a(Landroid/support/text/emoji/f$a;Landroid/support/text/emoji/b;II)V
    .locals 0

    .prologue
    .line 209
    invoke-direct {p0, p1, p2, p3}, Landroid/support/text/emoji/f$a;->a(Landroid/support/text/emoji/b;II)V

    return-void
.end method


# virtual methods
.method final a()Landroid/support/text/emoji/b;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Landroid/support/text/emoji/f$a;->b:Landroid/support/text/emoji/b;

    return-object v0
.end method

.method a(I)Landroid/support/text/emoji/f$a;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Landroid/support/text/emoji/f$a;->a:Landroid/util/SparseArray;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Landroid/support/text/emoji/f$a;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/text/emoji/f$a;

    goto :goto_0
.end method
