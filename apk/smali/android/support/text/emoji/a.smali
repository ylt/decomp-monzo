.class public Landroid/support/text/emoji/a;
.super Ljava/lang/Object;
.source "EmojiCompat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/text/emoji/a$b;,
        Landroid/support/text/emoji/a$a;,
        Landroid/support/text/emoji/a$e;,
        Landroid/support/text/emoji/a$c;,
        Landroid/support/text/emoji/a$g;,
        Landroid/support/text/emoji/a$f;,
        Landroid/support/text/emoji/a$d;,
        Landroid/support/text/emoji/a$h;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Object;

.field private static volatile b:Landroid/support/text/emoji/a;


# instance fields
.field private final c:Ljava/util/concurrent/locks/ReadWriteLock;

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/support/text/emoji/a$d;",
            ">;"
        }
    .end annotation
.end field

.field private e:I

.field private final f:Landroid/os/Handler;

.field private final g:Landroid/support/text/emoji/a$a;

.field private final h:Landroid/support/text/emoji/a$f;

.field private final i:Z

.field private final j:Z

.field private final k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 152
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Landroid/support/text/emoji/a;->a:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/support/text/emoji/a$c;)V
    .locals 2

    .prologue
    .line 201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 202
    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>()V

    iput-object v0, p0, Landroid/support/text/emoji/a;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    .line 203
    invoke-static {p1}, Landroid/support/text/emoji/a$c;->a(Landroid/support/text/emoji/a$c;)Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/text/emoji/a;->i:Z

    .line 204
    invoke-static {p1}, Landroid/support/text/emoji/a$c;->b(Landroid/support/text/emoji/a$c;)Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/text/emoji/a;->j:Z

    .line 205
    invoke-static {p1}, Landroid/support/text/emoji/a$c;->c(Landroid/support/text/emoji/a$c;)I

    move-result v0

    iput v0, p0, Landroid/support/text/emoji/a;->k:I

    .line 206
    invoke-static {p1}, Landroid/support/text/emoji/a$c;->d(Landroid/support/text/emoji/a$c;)Landroid/support/text/emoji/a$f;

    move-result-object v0

    iput-object v0, p0, Landroid/support/text/emoji/a;->h:Landroid/support/text/emoji/a$f;

    .line 207
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Landroid/support/text/emoji/a;->f:Landroid/os/Handler;

    .line 208
    new-instance v0, Landroid/support/v4/g/b;

    invoke-direct {v0}, Landroid/support/v4/g/b;-><init>()V

    iput-object v0, p0, Landroid/support/text/emoji/a;->d:Ljava/util/Set;

    .line 209
    invoke-static {p1}, Landroid/support/text/emoji/a$c;->e(Landroid/support/text/emoji/a$c;)Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/support/text/emoji/a$c;->e(Landroid/support/text/emoji/a$c;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 210
    iget-object v0, p0, Landroid/support/text/emoji/a;->d:Ljava/util/Set;

    invoke-static {p1}, Landroid/support/text/emoji/a$c;->e(Landroid/support/text/emoji/a$c;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 212
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_1

    new-instance v0, Landroid/support/text/emoji/a$a;

    invoke-direct {v0, p0}, Landroid/support/text/emoji/a$a;-><init>(Landroid/support/text/emoji/a;)V

    :goto_0
    iput-object v0, p0, Landroid/support/text/emoji/a;->g:Landroid/support/text/emoji/a$a;

    .line 214
    invoke-direct {p0}, Landroid/support/text/emoji/a;->e()V

    .line 215
    return-void

    .line 212
    :cond_1
    new-instance v0, Landroid/support/text/emoji/a$b;

    invoke-direct {v0, p0}, Landroid/support/text/emoji/a$b;-><init>(Landroid/support/text/emoji/a;)V

    goto :goto_0
.end method

.method public static a()Landroid/support/text/emoji/a;
    .locals 3

    .prologue
    .line 284
    sget-object v1, Landroid/support/text/emoji/a;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 285
    :try_start_0
    sget-object v0, Landroid/support/text/emoji/a;->b:Landroid/support/text/emoji/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "EmojiCompat is not initialized. Please call EmojiCompat.init() first"

    invoke-static {v0, v2}, Landroid/support/v4/g/l;->a(ZLjava/lang/String;)V

    .line 287
    sget-object v0, Landroid/support/text/emoji/a;->b:Landroid/support/text/emoji/a;

    monitor-exit v1

    return-object v0

    .line 285
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 288
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Landroid/support/text/emoji/a$c;)Landroid/support/text/emoji/a;
    .locals 2

    .prologue
    .line 225
    sget-object v0, Landroid/support/text/emoji/a;->b:Landroid/support/text/emoji/a;

    if-nez v0, :cond_1

    .line 226
    sget-object v1, Landroid/support/text/emoji/a;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 227
    :try_start_0
    sget-object v0, Landroid/support/text/emoji/a;->b:Landroid/support/text/emoji/a;

    if-nez v0, :cond_0

    .line 228
    new-instance v0, Landroid/support/text/emoji/a;

    invoke-direct {v0, p0}, Landroid/support/text/emoji/a;-><init>(Landroid/support/text/emoji/a$c;)V

    sput-object v0, Landroid/support/text/emoji/a;->b:Landroid/support/text/emoji/a;

    .line 230
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 232
    :cond_1
    sget-object v0, Landroid/support/text/emoji/a;->b:Landroid/support/text/emoji/a;

    return-object v0

    .line 230
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic a(Landroid/support/text/emoji/a;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Landroid/support/text/emoji/a;->f()V

    return-void
.end method

.method static synthetic a(Landroid/support/text/emoji/a;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1}, Landroid/support/text/emoji/a;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method private a(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 317
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 318
    iget-object v1, p0, Landroid/support/text/emoji/a;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 320
    const/4 v1, 0x2

    :try_start_0
    iput v1, p0, Landroid/support/text/emoji/a;->e:I

    .line 321
    iget-object v1, p0, Landroid/support/text/emoji/a;->d:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 322
    iget-object v1, p0, Landroid/support/text/emoji/a;->d:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 324
    iget-object v1, p0, Landroid/support/text/emoji/a;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 326
    iget-object v1, p0, Landroid/support/text/emoji/a;->f:Landroid/os/Handler;

    new-instance v2, Landroid/support/text/emoji/a$e;

    iget v3, p0, Landroid/support/text/emoji/a;->e:I

    invoke-direct {v2, v0, v3, p1}, Landroid/support/text/emoji/a$e;-><init>(Ljava/util/Collection;ILjava/lang/Throwable;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 327
    return-void

    .line 324
    :catchall_0
    move-exception v0

    iget-object v1, p0, Landroid/support/text/emoji/a;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public static a(Landroid/text/Editable;ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 435
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 436
    invoke-static {p0, p1, p2}, Landroid/support/text/emoji/c;->a(Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 438
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/view/inputmethod/InputConnection;Landroid/text/Editable;IIZ)Z
    .locals 2

    .prologue
    .line 464
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 465
    invoke-static {p0, p1, p2, p3, p4}, Landroid/support/text/emoji/c;->a(Landroid/view/inputmethod/InputConnection;Landroid/text/Editable;IIZ)Z

    move-result v0

    .line 468
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Landroid/support/text/emoji/a;)Landroid/support/text/emoji/a$f;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Landroid/support/text/emoji/a;->h:Landroid/support/text/emoji/a$f;

    return-object v0
.end method

.method static synthetic c(Landroid/support/text/emoji/a;)Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Landroid/support/text/emoji/a;->i:Z

    return v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 292
    iget-object v0, p0, Landroid/support/text/emoji/a;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 294
    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Landroid/support/text/emoji/a;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 296
    iget-object v0, p0, Landroid/support/text/emoji/a;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 299
    iget-object v0, p0, Landroid/support/text/emoji/a;->g:Landroid/support/text/emoji/a$a;

    invoke-virtual {v0}, Landroid/support/text/emoji/a$a;->a()V

    .line 300
    return-void

    .line 296
    :catchall_0
    move-exception v0

    iget-object v1, p0, Landroid/support/text/emoji/a;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 303
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 304
    iget-object v1, p0, Landroid/support/text/emoji/a;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 306
    const/4 v1, 0x1

    :try_start_0
    iput v1, p0, Landroid/support/text/emoji/a;->e:I

    .line 307
    iget-object v1, p0, Landroid/support/text/emoji/a;->d:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 308
    iget-object v1, p0, Landroid/support/text/emoji/a;->d:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 310
    iget-object v1, p0, Landroid/support/text/emoji/a;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 313
    iget-object v1, p0, Landroid/support/text/emoji/a;->f:Landroid/os/Handler;

    new-instance v2, Landroid/support/text/emoji/a$e;

    iget v3, p0, Landroid/support/text/emoji/a;->e:I

    invoke-direct {v2, v0, v3}, Landroid/support/text/emoji/a$e;-><init>(Ljava/util/Collection;I)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 314
    return-void

    .line 310
    :catchall_0
    move-exception v0

    iget-object v1, p0, Landroid/support/text/emoji/a;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method private g()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 392
    invoke-virtual {p0}, Landroid/support/text/emoji/a;->b()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 523
    if-nez p1, :cond_0

    move v0, v1

    .line 524
    :goto_0
    invoke-virtual {p0, p1, v1, v0}, Landroid/support/text/emoji/a;->a(Ljava/lang/CharSequence;II)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    .line 523
    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    goto :goto_0
.end method

.method public a(Ljava/lang/CharSequence;II)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 556
    const v0, 0x7fffffff

    invoke-virtual {p0, p1, p2, p3, v0}, Landroid/support/text/emoji/a;->a(Ljava/lang/CharSequence;III)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/CharSequence;III)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    .line 592
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Landroid/support/text/emoji/a;->a(Ljava/lang/CharSequence;IIII)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/CharSequence;IIII)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 632
    invoke-direct {p0}, Landroid/support/text/emoji/a;->g()Z

    move-result v0

    const-string v3, "Not initialized yet"

    invoke-static {v0, v3}, Landroid/support/v4/g/l;->a(ZLjava/lang/String;)V

    .line 633
    const-string v0, "start cannot be negative"

    invoke-static {p2, v0}, Landroid/support/v4/g/l;->a(ILjava/lang/String;)I

    .line 634
    const-string v0, "end cannot be negative"

    invoke-static {p3, v0}, Landroid/support/v4/g/l;->a(ILjava/lang/String;)I

    .line 635
    const-string v0, "maxEmojiCount cannot be negative"

    invoke-static {p4, v0}, Landroid/support/v4/g/l;->a(ILjava/lang/String;)I

    .line 636
    if-gt p2, p3, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "start should be <= than end"

    invoke-static {v0, v3}, Landroid/support/v4/g/l;->a(ZLjava/lang/Object;)V

    .line 640
    if-nez p1, :cond_2

    .line 668
    :cond_0
    :goto_1
    return-object p1

    :cond_1
    move v0, v2

    .line 636
    goto :goto_0

    .line 644
    :cond_2
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-gt p2, v0, :cond_3

    move v0, v1

    :goto_2
    const-string v3, "start should be < than charSequence length"

    invoke-static {v0, v3}, Landroid/support/v4/g/l;->a(ZLjava/lang/Object;)V

    .line 646
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-gt p3, v0, :cond_4

    move v0, v1

    :goto_3
    const-string v3, "end should be < than charSequence length"

    invoke-static {v0, v3}, Landroid/support/v4/g/l;->a(ZLjava/lang/Object;)V

    .line 650
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_0

    if-eq p2, p3, :cond_0

    .line 655
    packed-switch p5, :pswitch_data_0

    .line 664
    iget-boolean v5, p0, Landroid/support/text/emoji/a;->i:Z

    .line 668
    :goto_4
    iget-object v0, p0, Landroid/support/text/emoji/a;->g:Landroid/support/text/emoji/a$a;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Landroid/support/text/emoji/a$a;->a(Ljava/lang/CharSequence;IIIZ)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_1

    :cond_3
    move v0, v2

    .line 644
    goto :goto_2

    :cond_4
    move v0, v2

    .line 646
    goto :goto_3

    :pswitch_0
    move v5, v1

    .line 658
    goto :goto_4

    :pswitch_1
    move v5, v2

    .line 661
    goto :goto_4

    .line 655
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/support/text/emoji/a$d;)V
    .locals 3

    .prologue
    .line 343
    const-string v0, "initCallback cannot be null"

    invoke-static {p1, v0}, Landroid/support/v4/g/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 345
    iget-object v0, p0, Landroid/support/text/emoji/a;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 347
    :try_start_0
    iget v0, p0, Landroid/support/text/emoji/a;->e:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Landroid/support/text/emoji/a;->e:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 348
    :cond_0
    iget-object v0, p0, Landroid/support/text/emoji/a;->f:Landroid/os/Handler;

    new-instance v1, Landroid/support/text/emoji/a$e;

    iget v2, p0, Landroid/support/text/emoji/a;->e:I

    invoke-direct {v1, p1, v2}, Landroid/support/text/emoji/a$e;-><init>(Landroid/support/text/emoji/a$d;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 353
    :goto_0
    iget-object v0, p0, Landroid/support/text/emoji/a;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 355
    return-void

    .line 350
    :cond_1
    :try_start_1
    iget-object v0, p0, Landroid/support/text/emoji/a;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 353
    :catchall_0
    move-exception v0

    iget-object v1, p0, Landroid/support/text/emoji/a;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public a(Landroid/view/inputmethod/EditorInfo;)V
    .locals 1

    .prologue
    .line 699
    invoke-direct {p0}, Landroid/support/text/emoji/a;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p1, Landroid/view/inputmethod/EditorInfo;->extras:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 700
    iget-object v0, p0, Landroid/support/text/emoji/a;->g:Landroid/support/text/emoji/a$a;

    invoke-virtual {v0, p1}, Landroid/support/text/emoji/a$a;->a(Landroid/view/inputmethod/EditorInfo;)V

    .line 702
    :cond_0
    return-void
.end method

.method public b()I
    .locals 2

    .prologue
    .line 380
    iget-object v0, p0, Landroid/support/text/emoji/a;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 382
    :try_start_0
    iget v0, p0, Landroid/support/text/emoji/a;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 384
    iget-object v1, p0, Landroid/support/text/emoji/a;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Landroid/support/text/emoji/a;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method c()Z
    .locals 1

    .prologue
    .line 401
    iget-boolean v0, p0, Landroid/support/text/emoji/a;->j:Z

    return v0
.end method

.method d()I
    .locals 1

    .prologue
    .line 410
    iget v0, p0, Landroid/support/text/emoji/a;->k:I

    return v0
.end method
