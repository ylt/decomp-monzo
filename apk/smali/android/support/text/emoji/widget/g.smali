.class public final Landroid/support/text/emoji/widget/g;
.super Ljava/lang/Object;
.source "EmojiTextViewHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/text/emoji/widget/g$b;,
        Landroid/support/text/emoji/widget/g$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/support/text/emoji/widget/g$a;


# direct methods
.method public constructor <init>(Landroid/widget/TextView;)V
    .locals 2

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    const-string v0, "textView cannot be null"

    invoke-static {p1, v0}, Landroid/support/v4/g/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    new-instance v0, Landroid/support/text/emoji/widget/g$b;

    invoke-direct {v0, p1}, Landroid/support/text/emoji/widget/g$b;-><init>(Landroid/widget/TextView;)V

    :goto_0
    iput-object v0, p0, Landroid/support/text/emoji/widget/g;->a:Landroid/support/text/emoji/widget/g$a;

    .line 75
    return-void

    .line 73
    :cond_0
    new-instance v0, Landroid/support/text/emoji/widget/g$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/support/text/emoji/widget/g$a;-><init>(Landroid/support/text/emoji/widget/g$1;)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Landroid/support/text/emoji/widget/g;->a:Landroid/support/text/emoji/widget/g$a;

    invoke-virtual {v0}, Landroid/support/text/emoji/widget/g$a;->a()V

    .line 86
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Landroid/support/text/emoji/widget/g;->a:Landroid/support/text/emoji/widget/g$a;

    invoke-virtual {v0, p1}, Landroid/support/text/emoji/widget/g$a;->a(Z)V

    .line 124
    return-void
.end method

.method public a([Landroid/text/InputFilter;)[Landroid/text/InputFilter;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Landroid/support/text/emoji/widget/g;->a:Landroid/support/text/emoji/widget/g$a;

    invoke-virtual {v0, p1}, Landroid/support/text/emoji/widget/g$a;->a([Landroid/text/InputFilter;)[Landroid/text/InputFilter;

    move-result-object v0

    return-object v0
.end method
