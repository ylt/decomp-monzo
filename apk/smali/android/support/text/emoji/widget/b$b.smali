.class Landroid/support/text/emoji/widget/b$b;
.super Landroid/support/text/emoji/widget/b$a;
.source "EmojiEditTextHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/text/emoji/widget/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field private final a:Landroid/widget/EditText;

.field private final b:Landroid/support/text/emoji/widget/h;


# direct methods
.method constructor <init>(Landroid/widget/EditText;)V
    .locals 2

    .prologue
    .line 203
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/text/emoji/widget/b$a;-><init>(Landroid/support/text/emoji/widget/b$1;)V

    .line 204
    iput-object p1, p0, Landroid/support/text/emoji/widget/b$b;->a:Landroid/widget/EditText;

    .line 205
    new-instance v0, Landroid/support/text/emoji/widget/h;

    iget-object v1, p0, Landroid/support/text/emoji/widget/b$b;->a:Landroid/widget/EditText;

    invoke-direct {v0, v1}, Landroid/support/text/emoji/widget/h;-><init>(Landroid/widget/EditText;)V

    iput-object v0, p0, Landroid/support/text/emoji/widget/b$b;->b:Landroid/support/text/emoji/widget/h;

    .line 206
    iget-object v0, p0, Landroid/support/text/emoji/widget/b$b;->a:Landroid/widget/EditText;

    iget-object v1, p0, Landroid/support/text/emoji/widget/b$b;->b:Landroid/support/text/emoji/widget/h;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 207
    iget-object v0, p0, Landroid/support/text/emoji/widget/b$b;->a:Landroid/widget/EditText;

    invoke-static {}, Landroid/support/text/emoji/widget/c;->a()Landroid/text/Editable$Factory;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEditableFactory(Landroid/text/Editable$Factory;)V

    .line 208
    return-void
.end method


# virtual methods
.method a(Landroid/text/method/KeyListener;)Landroid/text/method/KeyListener;
    .locals 1

    .prologue
    .line 222
    instance-of v0, p1, Landroid/support/text/emoji/widget/f;

    if-eqz v0, :cond_0

    .line 225
    :goto_0
    return-object p1

    :cond_0
    new-instance v0, Landroid/support/text/emoji/widget/f;

    invoke-direct {v0, p1}, Landroid/support/text/emoji/widget/f;-><init>(Landroid/text/method/KeyListener;)V

    move-object p1, v0

    goto :goto_0
.end method

.method a(Landroid/view/inputmethod/InputConnection;Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 2

    .prologue
    .line 231
    instance-of v0, p1, Landroid/support/text/emoji/widget/d;

    if-eqz v0, :cond_0

    .line 234
    :goto_0
    return-object p1

    :cond_0
    new-instance v0, Landroid/support/text/emoji/widget/d;

    iget-object v1, p0, Landroid/support/text/emoji/widget/b$b;->a:Landroid/widget/EditText;

    invoke-direct {v0, v1, p1, p2}, Landroid/support/text/emoji/widget/d;-><init>(Landroid/widget/TextView;Landroid/view/inputmethod/InputConnection;Landroid/view/inputmethod/EditorInfo;)V

    move-object p1, v0

    goto :goto_0
.end method

.method a(I)V
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Landroid/support/text/emoji/widget/b$b;->b:Landroid/support/text/emoji/widget/h;

    invoke-virtual {v0, p1}, Landroid/support/text/emoji/widget/h;->a(I)V

    .line 213
    return-void
.end method

.method b(I)V
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Landroid/support/text/emoji/widget/b$b;->b:Landroid/support/text/emoji/widget/h;

    invoke-virtual {v0, p1}, Landroid/support/text/emoji/widget/h;->b(I)V

    .line 218
    return-void
.end method
