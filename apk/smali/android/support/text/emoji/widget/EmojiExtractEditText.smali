.class public Landroid/support/text/emoji/widget/EmojiExtractEditText;
.super Landroid/inputmethodservice/ExtractEditText;
.source "EmojiExtractEditText.java"


# instance fields
.field private a:Landroid/support/text/emoji/widget/b;

.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0, p1}, Landroid/inputmethodservice/ExtractEditText;-><init>(Landroid/content/Context;)V

    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, Landroid/support/text/emoji/widget/EmojiExtractEditText;->a(Landroid/util/AttributeSet;II)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Landroid/inputmethodservice/ExtractEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 58
    const v0, 0x101006e

    const/4 v1, 0x0

    invoke-direct {p0, p2, v0, v1}, Landroid/support/text/emoji/widget/EmojiExtractEditText;->a(Landroid/util/AttributeSet;II)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0, p1, p2, p3}, Landroid/inputmethodservice/ExtractEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 63
    const/4 v0, 0x0

    invoke-direct {p0, p2, p3, v0}, Landroid/support/text/emoji/widget/EmojiExtractEditText;->a(Landroid/util/AttributeSet;II)V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 69
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/inputmethodservice/ExtractEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 70
    invoke-direct {p0, p2, p3, p4}, Landroid/support/text/emoji/widget/EmojiExtractEditText;->a(Landroid/util/AttributeSet;II)V

    .line 71
    return-void
.end method

.method private a(Landroid/util/AttributeSet;II)V
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Landroid/support/text/emoji/widget/EmojiExtractEditText;->b:Z

    if-nez v0, :cond_0

    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/text/emoji/widget/EmojiExtractEditText;->b:Z

    .line 76
    new-instance v0, Landroid/support/text/emoji/widget/a;

    invoke-direct {v0, p0, p1, p2, p3}, Landroid/support/text/emoji/widget/a;-><init>(Landroid/view/View;Landroid/util/AttributeSet;II)V

    .line 78
    invoke-virtual {v0}, Landroid/support/text/emoji/widget/a;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/text/emoji/widget/EmojiExtractEditText;->setMaxEmojiCount(I)V

    .line 79
    invoke-super {p0}, Landroid/inputmethodservice/ExtractEditText;->getKeyListener()Landroid/text/method/KeyListener;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/text/emoji/widget/EmojiExtractEditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 81
    :cond_0
    return-void
.end method

.method private getEmojiEditTextHelper()Landroid/support/text/emoji/widget/b;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Landroid/support/text/emoji/widget/EmojiExtractEditText;->a:Landroid/support/text/emoji/widget/b;

    if-nez v0, :cond_0

    .line 143
    new-instance v0, Landroid/support/text/emoji/widget/b;

    invoke-direct {v0, p0}, Landroid/support/text/emoji/widget/b;-><init>(Landroid/widget/EditText;)V

    iput-object v0, p0, Landroid/support/text/emoji/widget/EmojiExtractEditText;->a:Landroid/support/text/emoji/widget/b;

    .line 145
    :cond_0
    iget-object v0, p0, Landroid/support/text/emoji/widget/EmojiExtractEditText;->a:Landroid/support/text/emoji/widget/b;

    return-object v0
.end method


# virtual methods
.method public getEmojiReplaceStrategy()I
    .locals 1

    .prologue
    .line 128
    invoke-direct {p0}, Landroid/support/text/emoji/widget/EmojiExtractEditText;->getEmojiEditTextHelper()Landroid/support/text/emoji/widget/b;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/text/emoji/widget/b;->b()I

    move-result v0

    return v0
.end method

.method public getMaxEmojiCount()I
    .locals 1

    .prologue
    .line 138
    invoke-direct {p0}, Landroid/support/text/emoji/widget/EmojiExtractEditText;->getEmojiEditTextHelper()Landroid/support/text/emoji/widget/b;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/text/emoji/widget/b;->a()I

    move-result v0

    return v0
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 2

    .prologue
    .line 90
    invoke-super {p0, p1}, Landroid/inputmethodservice/ExtractEditText;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    .line 91
    invoke-direct {p0}, Landroid/support/text/emoji/widget/EmojiExtractEditText;->getEmojiEditTextHelper()Landroid/support/text/emoji/widget/b;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Landroid/support/text/emoji/widget/b;->a(Landroid/view/inputmethod/InputConnection;Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    return-object v0
.end method

.method public setEmojiReplaceStrategy(I)V
    .locals 1

    .prologue
    .line 116
    invoke-direct {p0}, Landroid/support/text/emoji/widget/EmojiExtractEditText;->getEmojiEditTextHelper()Landroid/support/text/emoji/widget/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/text/emoji/widget/b;->b(I)V

    .line 117
    return-void
.end method

.method public setKeyListener(Landroid/text/method/KeyListener;)V
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0}, Landroid/support/text/emoji/widget/EmojiExtractEditText;->getEmojiEditTextHelper()Landroid/support/text/emoji/widget/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/text/emoji/widget/b;->a(Landroid/text/method/KeyListener;)Landroid/text/method/KeyListener;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/inputmethodservice/ExtractEditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 86
    return-void
.end method

.method public setMaxEmojiCount(I)V
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Landroid/support/text/emoji/widget/EmojiExtractEditText;->getEmojiEditTextHelper()Landroid/support/text/emoji/widget/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/text/emoji/widget/b;->a(I)V

    .line 105
    return-void
.end method
