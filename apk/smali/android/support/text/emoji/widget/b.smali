.class public final Landroid/support/text/emoji/widget/b;
.super Ljava/lang/Object;
.source "EmojiEditTextHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/text/emoji/widget/b$b;,
        Landroid/support/text/emoji/widget/b$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/support/text/emoji/widget/b$a;

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(Landroid/widget/EditText;)V
    .locals 2

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    const v0, 0x7fffffff

    iput v0, p0, Landroid/support/text/emoji/widget/b;->b:I

    .line 72
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/text/emoji/widget/b;->c:I

    .line 81
    const-string v0, "editText cannot be null"

    invoke-static {p1, v0}, Landroid/support/v4/g/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    new-instance v0, Landroid/support/text/emoji/widget/b$b;

    invoke-direct {v0, p1}, Landroid/support/text/emoji/widget/b$b;-><init>(Landroid/widget/EditText;)V

    :goto_0
    iput-object v0, p0, Landroid/support/text/emoji/widget/b;->a:Landroid/support/text/emoji/widget/b$a;

    .line 84
    return-void

    .line 82
    :cond_0
    new-instance v0, Landroid/support/text/emoji/widget/b$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/support/text/emoji/widget/b$a;-><init>(Landroid/support/text/emoji/widget/b$1;)V

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 111
    iget v0, p0, Landroid/support/text/emoji/widget/b;->b:I

    return v0
.end method

.method public a(Landroid/text/method/KeyListener;)Landroid/text/method/KeyListener;
    .locals 1

    .prologue
    .line 126
    const-string v0, "keyListener cannot be null"

    invoke-static {p1, v0}, Landroid/support/v4/g/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    iget-object v0, p0, Landroid/support/text/emoji/widget/b;->a:Landroid/support/text/emoji/widget/b$a;

    invoke-virtual {v0, p1}, Landroid/support/text/emoji/widget/b$a;->a(Landroid/text/method/KeyListener;)Landroid/text/method/KeyListener;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/inputmethod/InputConnection;Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 1

    .prologue
    .line 144
    const-string v0, "inputConnection cannot be null"

    invoke-static {p1, v0}, Landroid/support/v4/g/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    iget-object v0, p0, Landroid/support/text/emoji/widget/b;->a:Landroid/support/text/emoji/widget/b$a;

    invoke-virtual {v0, p1, p2}, Landroid/support/text/emoji/widget/b$a;->a(Landroid/view/inputmethod/InputConnection;Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    return-object v0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 98
    const-string v0, "maxEmojiCount should be greater than 0"

    invoke-static {p1, v0}, Landroid/support/v4/g/l;->a(ILjava/lang/String;)I

    .line 100
    iput p1, p0, Landroid/support/text/emoji/widget/b;->b:I

    .line 101
    iget-object v0, p0, Landroid/support/text/emoji/widget/b;->a:Landroid/support/text/emoji/widget/b$a;

    invoke-virtual {v0, p1}, Landroid/support/text/emoji/widget/b$a;->a(I)V

    .line 102
    return-void
.end method

.method b()I
    .locals 1

    .prologue
    .line 175
    iget v0, p0, Landroid/support/text/emoji/widget/b;->c:I

    return v0
.end method

.method b(I)V
    .locals 1

    .prologue
    .line 160
    iput p1, p0, Landroid/support/text/emoji/widget/b;->c:I

    .line 161
    iget-object v0, p0, Landroid/support/text/emoji/widget/b;->a:Landroid/support/text/emoji/widget/b$a;

    invoke-virtual {v0, p1}, Landroid/support/text/emoji/widget/b$a;->b(I)V

    .line 162
    return-void
.end method
