.class public final Landroid/support/text/emoji/f;
.super Ljava/lang/Object;
.source "MetadataRepo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/text/emoji/f$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/support/text/emoji/b/b;

.field private final b:[C

.field private final c:Landroid/support/text/emoji/f$a;

.field private final d:Landroid/graphics/Typeface;


# direct methods
.method constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object v2, p0, Landroid/support/text/emoji/f;->d:Landroid/graphics/Typeface;

    .line 75
    iput-object v2, p0, Landroid/support/text/emoji/f;->a:Landroid/support/text/emoji/b/b;

    .line 76
    new-instance v0, Landroid/support/text/emoji/f$a;

    const/16 v1, 0x400

    invoke-direct {v0, v1, v2}, Landroid/support/text/emoji/f$a;-><init>(ILandroid/support/text/emoji/f$1;)V

    iput-object v0, p0, Landroid/support/text/emoji/f;->c:Landroid/support/text/emoji/f$a;

    .line 77
    const/4 v0, 0x0

    new-array v0, v0, [C

    iput-object v0, p0, Landroid/support/text/emoji/f;->b:[C

    .line 78
    return-void
.end method

.method private constructor <init>(Landroid/graphics/Typeface;Landroid/support/text/emoji/b/b;)V
    .locals 3

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-object p1, p0, Landroid/support/text/emoji/f;->d:Landroid/graphics/Typeface;

    .line 89
    iput-object p2, p0, Landroid/support/text/emoji/f;->a:Landroid/support/text/emoji/b/b;

    .line 90
    new-instance v0, Landroid/support/text/emoji/f$a;

    const/16 v1, 0x400

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/support/text/emoji/f$a;-><init>(ILandroid/support/text/emoji/f$1;)V

    iput-object v0, p0, Landroid/support/text/emoji/f;->c:Landroid/support/text/emoji/f$a;

    .line 91
    iget-object v0, p0, Landroid/support/text/emoji/f;->a:Landroid/support/text/emoji/b/b;

    invoke-virtual {v0}, Landroid/support/text/emoji/b/b;->b()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [C

    iput-object v0, p0, Landroid/support/text/emoji/f;->b:[C

    .line 92
    iget-object v0, p0, Landroid/support/text/emoji/f;->a:Landroid/support/text/emoji/b/b;

    invoke-direct {p0, v0}, Landroid/support/text/emoji/f;->a(Landroid/support/text/emoji/b/b;)V

    .line 93
    return-void
.end method

.method public static a(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/support/text/emoji/f;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 128
    invoke-static {p0, p1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 129
    new-instance v1, Landroid/support/text/emoji/f;

    invoke-static {p0, p1}, Landroid/support/text/emoji/e;->a(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/support/text/emoji/b/b;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Landroid/support/text/emoji/f;-><init>(Landroid/graphics/Typeface;Landroid/support/text/emoji/b/b;)V

    return-object v1
.end method

.method private a(Landroid/support/text/emoji/b/b;)V
    .locals 6

    .prologue
    .line 136
    invoke-virtual {p1}, Landroid/support/text/emoji/b/b;->b()I

    move-result v1

    .line 137
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 138
    new-instance v2, Landroid/support/text/emoji/b;

    invoke-direct {v2, p0, v0}, Landroid/support/text/emoji/b;-><init>(Landroid/support/text/emoji/f;I)V

    .line 142
    invoke-virtual {v2}, Landroid/support/text/emoji/b;->a()I

    move-result v3

    iget-object v4, p0, Landroid/support/text/emoji/f;->b:[C

    mul-int/lit8 v5, v0, 0x2

    invoke-static {v3, v4, v5}, Ljava/lang/Character;->toChars(I[CI)I

    .line 143
    invoke-virtual {p0, v2}, Landroid/support/text/emoji/f;->a(Landroid/support/text/emoji/b;)V

    .line 137
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 145
    :cond_0
    return-void
.end method


# virtual methods
.method a()Landroid/graphics/Typeface;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Landroid/support/text/emoji/f;->d:Landroid/graphics/Typeface;

    return-object v0
.end method

.method a(Landroid/support/text/emoji/b;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 195
    const-string v0, "emoji metadata cannot be null"

    invoke-static {p1, v0}, Landroid/support/v4/g/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    invoke-virtual {p1}, Landroid/support/text/emoji/b;->g()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "invalid metadata codepoint length"

    invoke-static {v0, v2}, Landroid/support/v4/g/l;->a(ZLjava/lang/Object;)V

    .line 199
    iget-object v0, p0, Landroid/support/text/emoji/f;->c:Landroid/support/text/emoji/f$a;

    invoke-virtual {p1}, Landroid/support/text/emoji/b;->g()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, p1, v1, v2}, Landroid/support/text/emoji/f$a;->a(Landroid/support/text/emoji/f$a;Landroid/support/text/emoji/b;II)V

    .line 200
    return-void

    :cond_0
    move v0, v1

    .line 196
    goto :goto_0
.end method

.method b()I
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Landroid/support/text/emoji/f;->a:Landroid/support/text/emoji/b/b;

    invoke-virtual {v0}, Landroid/support/text/emoji/b/b;->a()I

    move-result v0

    return v0
.end method

.method c()Landroid/support/text/emoji/f$a;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Landroid/support/text/emoji/f;->c:Landroid/support/text/emoji/f$a;

    return-object v0
.end method

.method public d()[C
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Landroid/support/text/emoji/f;->b:[C

    return-object v0
.end method

.method public e()Landroid/support/text/emoji/b/b;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Landroid/support/text/emoji/f;->a:Landroid/support/text/emoji/b/b;

    return-object v0
.end method
