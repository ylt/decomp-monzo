.class public Landroid/support/text/emoji/b;
.super Ljava/lang/Object;
.source "EmojiMetadata.java"


# static fields
.field private static final a:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Landroid/support/text/emoji/b/a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:I

.field private final c:Landroid/support/text/emoji/f;

.field private volatile d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Landroid/support/text/emoji/b;->a:Ljava/lang/ThreadLocal;

    return-void
.end method

.method constructor <init>(Landroid/support/text/emoji/f;I)V
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/text/emoji/b;->d:I

    .line 89
    iput-object p1, p0, Landroid/support/text/emoji/b;->c:Landroid/support/text/emoji/f;

    .line 90
    iput p2, p0, Landroid/support/text/emoji/b;->b:I

    .line 91
    return-void
.end method

.method private h()Landroid/support/text/emoji/b/a;
    .locals 3

    .prologue
    .line 120
    sget-object v0, Landroid/support/text/emoji/b;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/text/emoji/b/a;

    .line 121
    if-nez v0, :cond_0

    .line 122
    new-instance v0, Landroid/support/text/emoji/b/a;

    invoke-direct {v0}, Landroid/support/text/emoji/b/a;-><init>()V

    .line 123
    sget-object v1, Landroid/support/text/emoji/b;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 132
    :cond_0
    iget-object v1, p0, Landroid/support/text/emoji/b;->c:Landroid/support/text/emoji/f;

    invoke-virtual {v1}, Landroid/support/text/emoji/f;->e()Landroid/support/text/emoji/b/b;

    move-result-object v1

    iget v2, p0, Landroid/support/text/emoji/b;->b:I

    invoke-virtual {v1, v0, v2}, Landroid/support/text/emoji/b/b;->a(Landroid/support/text/emoji/b/a;I)Landroid/support/text/emoji/b/a;

    .line 133
    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 140
    invoke-direct {p0}, Landroid/support/text/emoji/b;->h()Landroid/support/text/emoji/b/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/text/emoji/b/a;->a()I

    move-result v0

    return v0
.end method

.method public a(I)I
    .locals 1

    .prologue
    .line 203
    invoke-direct {p0}, Landroid/support/text/emoji/b;->h()Landroid/support/text/emoji/b/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/text/emoji/b/a;->a(I)I

    move-result v0

    return v0
.end method

.method public a(Landroid/graphics/Canvas;FFLandroid/graphics/Paint;)V
    .locals 8

    .prologue
    .line 104
    iget-object v0, p0, Landroid/support/text/emoji/b;->c:Landroid/support/text/emoji/f;

    invoke-virtual {v0}, Landroid/support/text/emoji/f;->a()Landroid/graphics/Typeface;

    move-result-object v0

    .line 105
    invoke-virtual {p4}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v7

    .line 106
    invoke-virtual {p4, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 111
    iget v0, p0, Landroid/support/text/emoji/b;->b:I

    mul-int/lit8 v2, v0, 0x2

    .line 112
    iget-object v0, p0, Landroid/support/text/emoji/b;->c:Landroid/support/text/emoji/f;

    invoke-virtual {v0}, Landroid/support/text/emoji/f;->d()[C

    move-result-object v1

    const/4 v3, 0x2

    move-object v0, p1

    move v4, p2

    move v5, p3

    move-object v6, p4

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText([CIIFFLandroid/graphics/Paint;)V

    .line 113
    invoke-virtual {p4, v7}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 114
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 186
    if-eqz p1, :cond_0

    const/4 v0, 0x2

    :goto_0
    iput v0, p0, Landroid/support/text/emoji/b;->d:I

    .line 187
    return-void

    .line 186
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()S
    .locals 1

    .prologue
    .line 147
    invoke-direct {p0}, Landroid/support/text/emoji/b;->h()Landroid/support/text/emoji/b/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/text/emoji/b/a;->d()S

    move-result v0

    return v0
.end method

.method public c()S
    .locals 1

    .prologue
    .line 154
    invoke-direct {p0}, Landroid/support/text/emoji/b;->h()Landroid/support/text/emoji/b/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/text/emoji/b/a;->e()S

    move-result v0

    return v0
.end method

.method public d()S
    .locals 1

    .prologue
    .line 168
    invoke-direct {p0}, Landroid/support/text/emoji/b;->h()Landroid/support/text/emoji/b/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/text/emoji/b/a;->c()S

    move-result v0

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 177
    iget v0, p0, Landroid/support/text/emoji/b;->d:I

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 194
    invoke-direct {p0}, Landroid/support/text/emoji/b;->h()Landroid/support/text/emoji/b/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/text/emoji/b/a;->b()Z

    move-result v0

    return v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 210
    invoke-direct {p0}, Landroid/support/text/emoji/b;->h()Landroid/support/text/emoji/b/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/text/emoji/b/a;->f()I

    move-result v0

    return v0
.end method
