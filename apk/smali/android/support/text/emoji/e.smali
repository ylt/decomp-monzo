.class Landroid/support/text/emoji/e;
.super Ljava/lang/Object;
.source "MetadataListReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/text/emoji/e$a;,
        Landroid/support/text/emoji/e$c;,
        Landroid/support/text/emoji/e$b;
    }
.end annotation


# direct methods
.method static synthetic a(S)I
    .locals 1

    .prologue
    .line 40
    invoke-static {p0}, Landroid/support/text/emoji/e;->b(S)I

    move-result v0

    return v0
.end method

.method static synthetic a(I)J
    .locals 2

    .prologue
    .line 40
    invoke-static {p0}, Landroid/support/text/emoji/e;->b(I)J

    move-result-wide v0

    return-wide v0
.end method

.method static a(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/support/text/emoji/b/b;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 106
    invoke-virtual {p0, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    const/4 v1, 0x0

    .line 107
    :try_start_0
    invoke-static {v2}, Landroid/support/text/emoji/e;->a(Ljava/io/InputStream;)Landroid/support/text/emoji/b/b;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 108
    if-eqz v2, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v2

    invoke-virtual {v1, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    goto :goto_0

    .line 106
    :catch_1
    move-exception v1

    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 108
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_2

    if-eqz v1, :cond_3

    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    :cond_2
    :goto_1
    throw v0

    :catch_2
    move-exception v2

    invoke-virtual {v1, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    goto :goto_1
.end method

.method static a(Ljava/io/InputStream;)Landroid/support/text/emoji/b/b;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    new-instance v0, Landroid/support/text/emoji/e$a;

    invoke-direct {v0, p0}, Landroid/support/text/emoji/e$a;-><init>(Ljava/io/InputStream;)V

    .line 70
    invoke-static {v0}, Landroid/support/text/emoji/e;->a(Landroid/support/text/emoji/e$c;)Landroid/support/text/emoji/e$b;

    move-result-object v1

    .line 72
    invoke-virtual {v1}, Landroid/support/text/emoji/e$b;->a()J

    move-result-wide v2

    invoke-interface {v0}, Landroid/support/text/emoji/e$c;->d()J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-int v2, v2

    invoke-interface {v0, v2}, Landroid/support/text/emoji/e$c;->a(I)V

    .line 74
    invoke-virtual {v1}, Landroid/support/text/emoji/e$b;->b()J

    move-result-wide v2

    long-to-int v0, v2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 75
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/io/InputStream;->read([B)I

    move-result v2

    .line 76
    int-to-long v4, v2

    invoke-virtual {v1}, Landroid/support/text/emoji/e$b;->b()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 77
    new-instance v0, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Needed "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/support/text/emoji/e$b;->b()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " bytes, got "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :cond_0
    invoke-static {v0}, Landroid/support/text/emoji/b/b;->a(Ljava/nio/ByteBuffer;)Landroid/support/text/emoji/b/b;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/support/text/emoji/e$c;)Landroid/support/text/emoji/e$b;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, -0x1

    const/4 v0, 0x0

    const/4 v9, 0x4

    .line 120
    invoke-interface {p0, v9}, Landroid/support/text/emoji/e$c;->a(I)V

    .line 122
    invoke-interface {p0}, Landroid/support/text/emoji/e$c;->a()I

    move-result v6

    .line 123
    const/16 v1, 0x64

    if-le v6, v1, :cond_0

    .line 125
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Cannot read metadata."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :cond_0
    const/4 v1, 0x6

    invoke-interface {p0, v1}, Landroid/support/text/emoji/e$c;->a(I)V

    move v1, v0

    .line 131
    :goto_0
    if-ge v1, v6, :cond_5

    .line 132
    invoke-interface {p0}, Landroid/support/text/emoji/e$c;->c()I

    move-result v7

    .line 134
    invoke-interface {p0, v9}, Landroid/support/text/emoji/e$c;->a(I)V

    .line 135
    invoke-interface {p0}, Landroid/support/text/emoji/e$c;->b()J

    move-result-wide v2

    .line 137
    invoke-interface {p0, v9}, Landroid/support/text/emoji/e$c;->a(I)V

    .line 138
    const v8, 0x6d657461

    if-ne v8, v7, :cond_2

    .line 144
    :goto_1
    cmp-long v1, v2, v4

    if-eqz v1, :cond_4

    .line 146
    invoke-interface {p0}, Landroid/support/text/emoji/e$c;->d()J

    move-result-wide v4

    sub-long v4, v2, v4

    long-to-int v1, v4

    invoke-interface {p0, v1}, Landroid/support/text/emoji/e$c;->a(I)V

    .line 148
    const/16 v1, 0xc

    invoke-interface {p0, v1}, Landroid/support/text/emoji/e$c;->a(I)V

    .line 150
    invoke-interface {p0}, Landroid/support/text/emoji/e$c;->b()J

    move-result-wide v4

    .line 151
    :goto_2
    int-to-long v6, v0

    cmp-long v1, v6, v4

    if-gez v1, :cond_4

    .line 152
    invoke-interface {p0}, Landroid/support/text/emoji/e$c;->c()I

    move-result v1

    .line 153
    invoke-interface {p0}, Landroid/support/text/emoji/e$c;->b()J

    move-result-wide v6

    .line 154
    invoke-interface {p0}, Landroid/support/text/emoji/e$c;->b()J

    move-result-wide v8

    .line 155
    const v10, 0x456d6a69

    if-eq v10, v1, :cond_1

    const v10, 0x656d6a69

    if-ne v10, v1, :cond_3

    .line 156
    :cond_1
    new-instance v0, Landroid/support/text/emoji/e$b;

    add-long/2addr v2, v6

    invoke-direct {v0, v2, v3, v8, v9}, Landroid/support/text/emoji/e$b;-><init>(JJ)V

    return-object v0

    .line 131
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 151
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 161
    :cond_4
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Cannot read metadata."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    move-wide v2, v4

    goto :goto_1
.end method

.method private static b(S)I
    .locals 1

    .prologue
    .line 186
    const v0, 0xffff

    and-int/2addr v0, p0

    return v0
.end method

.method private static b(I)J
    .locals 4

    .prologue
    .line 190
    int-to-long v0, p0

    const-wide v2, 0xffffffffL

    and-long/2addr v0, v2

    return-wide v0
.end method
