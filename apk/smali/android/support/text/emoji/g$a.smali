.class public final Landroid/support/text/emoji/g$a;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/text/emoji/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# static fields
.field public static final EmojiEditText:[I

.field public static final EmojiEditText_maxEmojiCount:I = 0x0

.field public static final EmojiExtractTextLayout:[I

.field public static final EmojiExtractTextLayout_emojiReplaceStrategy:I = 0x0

.field public static final FontFamily:[I

.field public static final FontFamilyFont:[I

.field public static final FontFamilyFont_font:I = 0x1

.field public static final FontFamilyFont_fontStyle:I = 0x0

.field public static final FontFamilyFont_fontWeight:I = 0x2

.field public static final FontFamily_fontProviderAuthority:I = 0x0

.field public static final FontFamily_fontProviderCerts:I = 0x3

.field public static final FontFamily_fontProviderFetchStrategy:I = 0x4

.field public static final FontFamily_fontProviderFetchTimeout:I = 0x5

.field public static final FontFamily_fontProviderPackage:I = 0x1

.field public static final FontFamily_fontProviderQuery:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 125
    new-array v0, v3, [I

    const v1, 0x7f01015c

    aput v1, v0, v2

    sput-object v0, Landroid/support/text/emoji/g$a;->EmojiEditText:[I

    .line 127
    new-array v0, v3, [I

    const v1, 0x7f01015d

    aput v1, v0, v2

    sput-object v0, Landroid/support/text/emoji/g$a;->EmojiExtractTextLayout:[I

    .line 129
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Landroid/support/text/emoji/g$a;->FontFamily:[I

    .line 136
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Landroid/support/text/emoji/g$a;->FontFamilyFont:[I

    return-void

    .line 129
    nop

    :array_0
    .array-data 4
        0x7f01017a
        0x7f01017b
        0x7f01017c
        0x7f01017d
        0x7f01017e
        0x7f01017f
    .end array-data

    .line 136
    :array_1
    .array-data 4
        0x7f010180
        0x7f010181
        0x7f010182
    .end array-data
.end method
