.class final Landroid/support/text/emoji/c$c;
.super Ljava/lang/Object;
.source "EmojiProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/text/emoji/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "c"
.end annotation


# instance fields
.field private a:I

.field private final b:Landroid/support/text/emoji/f$a;

.field private c:Landroid/support/text/emoji/f$a;

.field private d:Landroid/support/text/emoji/f$a;

.field private e:I

.field private f:I


# direct methods
.method constructor <init>(Landroid/support/text/emoji/f$a;)V
    .locals 1

    .prologue
    .line 486
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 458
    const/4 v0, 0x1

    iput v0, p0, Landroid/support/text/emoji/c$c;->a:I

    .line 487
    iput-object p1, p0, Landroid/support/text/emoji/c$c;->b:Landroid/support/text/emoji/f$a;

    .line 488
    iput-object p1, p0, Landroid/support/text/emoji/c$c;->c:Landroid/support/text/emoji/f$a;

    .line 489
    return-void
.end method

.method private static b(I)Z
    .locals 1

    .prologue
    .line 585
    const v0, 0xfe0f

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(I)Z
    .locals 1

    .prologue
    .line 594
    const v0, 0xfe0e

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()I
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 545
    iput v1, p0, Landroid/support/text/emoji/c$c;->a:I

    .line 546
    iget-object v0, p0, Landroid/support/text/emoji/c$c;->b:Landroid/support/text/emoji/f$a;

    iput-object v0, p0, Landroid/support/text/emoji/c$c;->c:Landroid/support/text/emoji/f$a;

    .line 547
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/text/emoji/c$c;->f:I

    .line 548
    return v1
.end method


# virtual methods
.method a(I)I
    .locals 5

    .prologue
    const/4 v1, 0x3

    const/4 v4, 0x1

    const/4 v0, 0x2

    .line 494
    iget-object v2, p0, Landroid/support/text/emoji/c$c;->c:Landroid/support/text/emoji/f$a;

    invoke-virtual {v2, p1}, Landroid/support/text/emoji/f$a;->a(I)Landroid/support/text/emoji/f$a;

    move-result-object v2

    .line 495
    iget v3, p0, Landroid/support/text/emoji/c$c;->a:I

    packed-switch v3, :pswitch_data_0

    .line 528
    if-nez v2, :cond_7

    .line 529
    invoke-direct {p0}, Landroid/support/text/emoji/c$c;->d()I

    move-result v0

    .line 539
    :cond_0
    :goto_0
    iput p1, p0, Landroid/support/text/emoji/c$c;->e:I

    .line 540
    return v0

    .line 497
    :pswitch_0
    if-eqz v2, :cond_1

    .line 498
    iput-object v2, p0, Landroid/support/text/emoji/c$c;->c:Landroid/support/text/emoji/f$a;

    .line 499
    iget v1, p0, Landroid/support/text/emoji/c$c;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Landroid/support/text/emoji/c$c;->f:I

    goto :goto_0

    .line 502
    :cond_1
    invoke-static {p1}, Landroid/support/text/emoji/c$c;->c(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 503
    invoke-direct {p0}, Landroid/support/text/emoji/c$c;->d()I

    move-result v0

    goto :goto_0

    .line 504
    :cond_2
    invoke-static {p1}, Landroid/support/text/emoji/c$c;->b(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 506
    iget-object v0, p0, Landroid/support/text/emoji/c$c;->c:Landroid/support/text/emoji/f$a;

    invoke-virtual {v0}, Landroid/support/text/emoji/f$a;->a()Landroid/support/text/emoji/b;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 507
    iget v0, p0, Landroid/support/text/emoji/c$c;->f:I

    if-ne v0, v4, :cond_5

    .line 508
    iget-object v0, p0, Landroid/support/text/emoji/c$c;->c:Landroid/support/text/emoji/f$a;

    invoke-virtual {v0}, Landroid/support/text/emoji/f$a;->a()Landroid/support/text/emoji/b;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/text/emoji/b;->f()Z

    move-result v0

    if-nez v0, :cond_3

    iget v0, p0, Landroid/support/text/emoji/c$c;->e:I

    .line 509
    invoke-static {v0}, Landroid/support/text/emoji/c$c;->b(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 510
    :cond_3
    iget-object v0, p0, Landroid/support/text/emoji/c$c;->c:Landroid/support/text/emoji/f$a;

    iput-object v0, p0, Landroid/support/text/emoji/c$c;->d:Landroid/support/text/emoji/f$a;

    .line 512
    invoke-direct {p0}, Landroid/support/text/emoji/c$c;->d()I

    move v0, v1

    goto :goto_0

    .line 514
    :cond_4
    invoke-direct {p0}, Landroid/support/text/emoji/c$c;->d()I

    move-result v0

    goto :goto_0

    .line 517
    :cond_5
    iget-object v0, p0, Landroid/support/text/emoji/c$c;->c:Landroid/support/text/emoji/f$a;

    iput-object v0, p0, Landroid/support/text/emoji/c$c;->d:Landroid/support/text/emoji/f$a;

    .line 519
    invoke-direct {p0}, Landroid/support/text/emoji/c$c;->d()I

    move v0, v1

    goto :goto_0

    .line 522
    :cond_6
    invoke-direct {p0}, Landroid/support/text/emoji/c$c;->d()I

    move-result v0

    goto :goto_0

    .line 531
    :cond_7
    iput v0, p0, Landroid/support/text/emoji/c$c;->a:I

    .line 532
    iput-object v2, p0, Landroid/support/text/emoji/c$c;->c:Landroid/support/text/emoji/f$a;

    .line 533
    iput v4, p0, Landroid/support/text/emoji/c$c;->f:I

    goto :goto_0

    .line 495
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method a()Landroid/support/text/emoji/b;
    .locals 1

    .prologue
    .line 555
    iget-object v0, p0, Landroid/support/text/emoji/c$c;->d:Landroid/support/text/emoji/f$a;

    invoke-virtual {v0}, Landroid/support/text/emoji/f$a;->a()Landroid/support/text/emoji/b;

    move-result-object v0

    return-object v0
.end method

.method b()Landroid/support/text/emoji/b;
    .locals 1

    .prologue
    .line 562
    iget-object v0, p0, Landroid/support/text/emoji/c$c;->c:Landroid/support/text/emoji/f$a;

    invoke-virtual {v0}, Landroid/support/text/emoji/f$a;->a()Landroid/support/text/emoji/b;

    move-result-object v0

    return-object v0
.end method

.method c()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 573
    iget v1, p0, Landroid/support/text/emoji/c$c;->a:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Landroid/support/text/emoji/c$c;->c:Landroid/support/text/emoji/f$a;

    invoke-virtual {v1}, Landroid/support/text/emoji/f$a;->a()Landroid/support/text/emoji/b;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/text/emoji/c$c;->c:Landroid/support/text/emoji/f$a;

    .line 574
    invoke-virtual {v1}, Landroid/support/text/emoji/f$a;->a()Landroid/support/text/emoji/b;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/text/emoji/b;->f()Z

    move-result v1

    if-nez v1, :cond_0

    iget v1, p0, Landroid/support/text/emoji/c$c;->e:I

    .line 575
    invoke-static {v1}, Landroid/support/text/emoji/c$c;->b(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget v1, p0, Landroid/support/text/emoji/c$c;->f:I

    if-le v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
