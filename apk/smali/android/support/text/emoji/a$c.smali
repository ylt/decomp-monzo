.class public abstract Landroid/support/text/emoji/a$c;
.super Ljava/lang/Object;
.source "EmojiCompat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/text/emoji/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "c"
.end annotation


# instance fields
.field private final a:Landroid/support/text/emoji/a$f;

.field private b:Z

.field private c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/support/text/emoji/a$d;",
            ">;"
        }
    .end annotation
.end field

.field private d:Z

.field private e:I


# direct methods
.method protected constructor <init>(Landroid/support/text/emoji/a$f;)V
    .locals 1

    .prologue
    .line 796
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 789
    const v0, -0xff0100

    iput v0, p0, Landroid/support/text/emoji/a$c;->e:I

    .line 797
    const-string v0, "metadataLoader cannot be null."

    invoke-static {p1, v0}, Landroid/support/v4/g/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 798
    iput-object p1, p0, Landroid/support/text/emoji/a$c;->a:Landroid/support/text/emoji/a$f;

    .line 799
    return-void
.end method

.method static synthetic a(Landroid/support/text/emoji/a$c;)Z
    .locals 1

    .prologue
    .line 784
    iget-boolean v0, p0, Landroid/support/text/emoji/a$c;->b:Z

    return v0
.end method

.method static synthetic b(Landroid/support/text/emoji/a$c;)Z
    .locals 1

    .prologue
    .line 784
    iget-boolean v0, p0, Landroid/support/text/emoji/a$c;->d:Z

    return v0
.end method

.method static synthetic c(Landroid/support/text/emoji/a$c;)I
    .locals 1

    .prologue
    .line 784
    iget v0, p0, Landroid/support/text/emoji/a$c;->e:I

    return v0
.end method

.method static synthetic d(Landroid/support/text/emoji/a$c;)Landroid/support/text/emoji/a$f;
    .locals 1

    .prologue
    .line 784
    iget-object v0, p0, Landroid/support/text/emoji/a$c;->a:Landroid/support/text/emoji/a$f;

    return-object v0
.end method

.method static synthetic e(Landroid/support/text/emoji/a$c;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 784
    iget-object v0, p0, Landroid/support/text/emoji/a$c;->c:Ljava/util/Set;

    return-object v0
.end method
