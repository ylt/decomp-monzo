.class final Landroid/support/text/emoji/a$b;
.super Landroid/support/text/emoji/a$a;
.source "EmojiCompat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/text/emoji/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation


# instance fields
.field private volatile b:Landroid/support/text/emoji/c;

.field private volatile c:Landroid/support/text/emoji/f;


# direct methods
.method constructor <init>(Landroid/support/text/emoji/a;)V
    .locals 0

    .prologue
    .line 988
    invoke-direct {p0, p1}, Landroid/support/text/emoji/a$a;-><init>(Landroid/support/text/emoji/a;)V

    .line 989
    return-void
.end method

.method static synthetic a(Landroid/support/text/emoji/a$b;Landroid/support/text/emoji/f;)V
    .locals 0

    .prologue
    .line 974
    invoke-direct {p0, p1}, Landroid/support/text/emoji/a$b;->a(Landroid/support/text/emoji/f;)V

    return-void
.end method

.method private a(Landroid/support/text/emoji/f;)V
    .locals 3

    .prologue
    .line 1013
    if-nez p1, :cond_0

    .line 1014
    iget-object v0, p0, Landroid/support/text/emoji/a$b;->a:Landroid/support/text/emoji/a;

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "metadataRepo cannot be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Landroid/support/text/emoji/a;->a(Landroid/support/text/emoji/a;Ljava/lang/Throwable;)V

    .line 1023
    :goto_0
    return-void

    .line 1019
    :cond_0
    iput-object p1, p0, Landroid/support/text/emoji/a$b;->c:Landroid/support/text/emoji/f;

    .line 1020
    new-instance v0, Landroid/support/text/emoji/c;

    iget-object v1, p0, Landroid/support/text/emoji/a$b;->c:Landroid/support/text/emoji/f;

    new-instance v2, Landroid/support/text/emoji/a$h;

    invoke-direct {v2}, Landroid/support/text/emoji/a$h;-><init>()V

    invoke-direct {v0, v1, v2}, Landroid/support/text/emoji/c;-><init>(Landroid/support/text/emoji/f;Landroid/support/text/emoji/a$h;)V

    iput-object v0, p0, Landroid/support/text/emoji/a$b;->b:Landroid/support/text/emoji/c;

    .line 1022
    iget-object v0, p0, Landroid/support/text/emoji/a$b;->a:Landroid/support/text/emoji/a;

    invoke-static {v0}, Landroid/support/text/emoji/a;->a(Landroid/support/text/emoji/a;)V

    goto :goto_0
.end method


# virtual methods
.method a(Ljava/lang/CharSequence;IIIZ)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    .line 1039
    iget-object v0, p0, Landroid/support/text/emoji/a$b;->b:Landroid/support/text/emoji/c;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/support/text/emoji/c;->a(Ljava/lang/CharSequence;IIIZ)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method a()V
    .locals 2

    .prologue
    .line 994
    :try_start_0
    new-instance v0, Landroid/support/text/emoji/a$b$1;

    invoke-direct {v0, p0}, Landroid/support/text/emoji/a$b$1;-><init>(Landroid/support/text/emoji/a$b;)V

    .line 1005
    iget-object v1, p0, Landroid/support/text/emoji/a$b;->a:Landroid/support/text/emoji/a;

    invoke-static {v1}, Landroid/support/text/emoji/a;->b(Landroid/support/text/emoji/a;)Landroid/support/text/emoji/a$f;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/support/text/emoji/a$f;->a(Landroid/support/text/emoji/a$g;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1009
    :goto_0
    return-void

    .line 1006
    :catch_0
    move-exception v0

    .line 1007
    iget-object v1, p0, Landroid/support/text/emoji/a$b;->a:Landroid/support/text/emoji/a;

    invoke-static {v1, v0}, Landroid/support/text/emoji/a;->a(Landroid/support/text/emoji/a;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method a(Landroid/view/inputmethod/EditorInfo;)V
    .locals 3

    .prologue
    .line 1044
    iget-object v0, p1, Landroid/view/inputmethod/EditorInfo;->extras:Landroid/os/Bundle;

    const-string v1, "android.support.text.emoji.emojiCompat_metadataVersion"

    iget-object v2, p0, Landroid/support/text/emoji/a$b;->c:Landroid/support/text/emoji/f;

    invoke-virtual {v2}, Landroid/support/text/emoji/f;->b()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1045
    iget-object v0, p1, Landroid/view/inputmethod/EditorInfo;->extras:Landroid/os/Bundle;

    const-string v1, "android.support.text.emoji.emojiCompat_replaceAll"

    iget-object v2, p0, Landroid/support/text/emoji/a$b;->a:Landroid/support/text/emoji/a;

    invoke-static {v2}, Landroid/support/text/emoji/a;->c(Landroid/support/text/emoji/a;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1046
    return-void
.end method
