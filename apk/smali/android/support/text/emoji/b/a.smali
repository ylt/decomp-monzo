.class public final Landroid/support/text/emoji/b/a;
.super Landroid/support/text/emoji/b/c;
.source "MetadataItem.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/support/text/emoji/b/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 51
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Landroid/support/text/emoji/b/a;->b(I)I

    move-result v0

    .line 52
    if-eqz v0, :cond_0

    iget-object v1, p0, Landroid/support/text/emoji/b/a;->c:Ljava/nio/ByteBuffer;

    iget v2, p0, Landroid/support/text/emoji/b/a;->b:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)I
    .locals 3

    .prologue
    .line 81
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Landroid/support/text/emoji/b/a;->b(I)I

    move-result v0

    .line 82
    if-eqz v0, :cond_0

    iget-object v1, p0, Landroid/support/text/emoji/b/a;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {p0, v0}, Landroid/support/text/emoji/b/a;->e(I)I

    move-result v0

    mul-int/lit8 v2, p1, 0x4

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(ILjava/nio/ByteBuffer;)V
    .locals 0

    .prologue
    .line 41
    iput p1, p0, Landroid/support/text/emoji/b/a;->b:I

    .line 42
    iput-object p2, p0, Landroid/support/text/emoji/b/a;->c:Ljava/nio/ByteBuffer;

    .line 43
    return-void
.end method

.method public b(ILjava/nio/ByteBuffer;)Landroid/support/text/emoji/b/a;
    .locals 0

    .prologue
    .line 46
    invoke-virtual {p0, p1, p2}, Landroid/support/text/emoji/b/a;->a(ILjava/nio/ByteBuffer;)V

    .line 47
    return-object p0
.end method

.method public b()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 56
    const/4 v1, 0x6

    invoke-virtual {p0, v1}, Landroid/support/text/emoji/b/a;->b(I)I

    move-result v1

    .line 57
    if-eqz v1, :cond_0

    iget-object v2, p0, Landroid/support/text/emoji/b/a;->c:Ljava/nio/ByteBuffer;

    iget v3, p0, Landroid/support/text/emoji/b/a;->b:I

    add-int/2addr v1, v3

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public c()S
    .locals 3

    .prologue
    .line 61
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/support/text/emoji/b/a;->b(I)I

    move-result v0

    .line 62
    if-eqz v0, :cond_0

    iget-object v1, p0, Landroid/support/text/emoji/b/a;->c:Ljava/nio/ByteBuffer;

    iget v2, p0, Landroid/support/text/emoji/b/a;->b:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()S
    .locals 3

    .prologue
    .line 71
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, Landroid/support/text/emoji/b/a;->b(I)I

    move-result v0

    .line 72
    if-eqz v0, :cond_0

    iget-object v1, p0, Landroid/support/text/emoji/b/a;->c:Ljava/nio/ByteBuffer;

    iget v2, p0, Landroid/support/text/emoji/b/a;->b:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()S
    .locals 3

    .prologue
    .line 76
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Landroid/support/text/emoji/b/a;->b(I)I

    move-result v0

    .line 77
    if-eqz v0, :cond_0

    iget-object v1, p0, Landroid/support/text/emoji/b/a;->c:Ljava/nio/ByteBuffer;

    iget v2, p0, Landroid/support/text/emoji/b/a;->b:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 86
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Landroid/support/text/emoji/b/a;->b(I)I

    move-result v0

    .line 87
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/support/text/emoji/b/a;->d(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
