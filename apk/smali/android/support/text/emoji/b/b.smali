.class public final Landroid/support/text/emoji/b/b;
.super Landroid/support/text/emoji/b/c;
.source "MetadataList.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/support/text/emoji/b/c;-><init>()V

    return-void
.end method

.method public static a(Ljava/nio/ByteBuffer;)Landroid/support/text/emoji/b/b;
    .locals 1

    .prologue
    .line 32
    new-instance v0, Landroid/support/text/emoji/b/b;

    invoke-direct {v0}, Landroid/support/text/emoji/b/b;-><init>()V

    invoke-static {p0, v0}, Landroid/support/text/emoji/b/b;->a(Ljava/nio/ByteBuffer;Landroid/support/text/emoji/b/b;)Landroid/support/text/emoji/b/b;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/nio/ByteBuffer;Landroid/support/text/emoji/b/b;)Landroid/support/text/emoji/b/b;
    .locals 2

    .prologue
    .line 36
    sget-object v0, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 37
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p1, v0, p0}, Landroid/support/text/emoji/b/b;->b(ILjava/nio/ByteBuffer;)Landroid/support/text/emoji/b/b;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 51
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Landroid/support/text/emoji/b/b;->b(I)I

    move-result v0

    .line 52
    if-eqz v0, :cond_0

    iget-object v1, p0, Landroid/support/text/emoji/b/b;->c:Ljava/nio/ByteBuffer;

    iget v2, p0, Landroid/support/text/emoji/b/b;->b:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/support/text/emoji/b/a;I)Landroid/support/text/emoji/b/a;
    .locals 2

    .prologue
    .line 60
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Landroid/support/text/emoji/b/b;->b(I)I

    move-result v0

    .line 61
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/support/text/emoji/b/b;->e(I)I

    move-result v0

    mul-int/lit8 v1, p2, 0x4

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Landroid/support/text/emoji/b/b;->c(I)I

    move-result v0

    iget-object v1, p0, Landroid/support/text/emoji/b/b;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {p1, v0, v1}, Landroid/support/text/emoji/b/a;->b(ILjava/nio/ByteBuffer;)Landroid/support/text/emoji/b/a;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(ILjava/nio/ByteBuffer;)V
    .locals 0

    .prologue
    .line 41
    iput p1, p0, Landroid/support/text/emoji/b/b;->b:I

    .line 42
    iput-object p2, p0, Landroid/support/text/emoji/b/b;->c:Ljava/nio/ByteBuffer;

    .line 43
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Landroid/support/text/emoji/b/b;->b(I)I

    move-result v0

    .line 66
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/support/text/emoji/b/b;->d(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(ILjava/nio/ByteBuffer;)Landroid/support/text/emoji/b/b;
    .locals 0

    .prologue
    .line 46
    invoke-virtual {p0, p1, p2}, Landroid/support/text/emoji/b/b;->a(ILjava/nio/ByteBuffer;)V

    .line 47
    return-object p0
.end method
