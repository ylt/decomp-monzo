.class public final Landroid/support/text/emoji/h;
.super Landroid/support/text/emoji/d;
.source "TypefaceEmojiSpan.java"


# static fields
.field private static a:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/support/text/emoji/b;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/support/text/emoji/d;-><init>(Landroid/support/text/emoji/b;)V

    .line 49
    return-void
.end method

.method private static c()Landroid/graphics/Paint;
    .locals 2

    .prologue
    .line 62
    sget-object v0, Landroid/support/text/emoji/h;->a:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    .line 63
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    sput-object v0, Landroid/support/text/emoji/h;->a:Landroid/graphics/Paint;

    .line 64
    sget-object v0, Landroid/support/text/emoji/h;->a:Landroid/graphics/Paint;

    invoke-static {}, Landroid/support/text/emoji/a;->a()Landroid/support/text/emoji/a;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/text/emoji/a;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 65
    sget-object v0, Landroid/support/text/emoji/h;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 67
    :cond_0
    sget-object v0, Landroid/support/text/emoji/h;->a:Landroid/graphics/Paint;

    return-object v0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .locals 6

    .prologue
    .line 55
    invoke-static {}, Landroid/support/text/emoji/a;->a()Landroid/support/text/emoji/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/text/emoji/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    int-to-float v2, p6

    invoke-virtual {p0}, Landroid/support/text/emoji/h;->b()I

    move-result v0

    int-to-float v0, v0

    add-float v3, p5, v0

    int-to-float v4, p8

    invoke-static {}, Landroid/support/text/emoji/h;->c()Landroid/graphics/Paint;

    move-result-object v5

    move-object v0, p1

    move v1, p5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 58
    :cond_0
    invoke-virtual {p0}, Landroid/support/text/emoji/h;->a()Landroid/support/text/emoji/b;

    move-result-object v0

    int-to-float v1, p7

    invoke-virtual {v0, p1, p5, v1, p9}, Landroid/support/text/emoji/b;->a(Landroid/graphics/Canvas;FFLandroid/graphics/Paint;)V

    .line 59
    return-void
.end method
