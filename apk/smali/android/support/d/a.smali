.class public Landroid/support/d/a;
.super Ljava/lang/Object;
.source "ExifInterface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/d/a$a;,
        Landroid/support/d/a$c;,
        Landroid/support/d/a$b;,
        Landroid/support/d/a$d;
    }
.end annotation


# static fields
.field private static final A:Landroid/support/d/a$c;

.field private static final B:[Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/support/d/a$c;",
            ">;"
        }
    .end annotation
.end field

.field private static final C:[Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/support/d/a$c;",
            ">;"
        }
    .end annotation
.end field

.field private static final D:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final E:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final F:Ljava/nio/charset/Charset;

.field private static final W:Ljava/util/regex/Pattern;

.field private static final X:Ljava/util/regex/Pattern;

.field static final a:[B

.field static final b:[Ljava/lang/String;

.field static final c:[I

.field static final d:[[Landroid/support/d/a$c;

.field static final e:[B

.field private static final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final h:[B

.field private static final i:[B

.field private static j:Ljava/text/SimpleDateFormat;

.field private static final k:[B

.field private static final l:[I

.field private static final m:[I

.field private static final n:[I

.field private static final o:[Landroid/support/d/a$c;

.field private static final p:[Landroid/support/d/a$c;

.field private static final q:[Landroid/support/d/a$c;

.field private static final r:[Landroid/support/d/a$c;

.field private static final s:[Landroid/support/d/a$c;

.field private static final t:Landroid/support/d/a$c;

.field private static final u:[Landroid/support/d/a$c;

.field private static final v:[Landroid/support/d/a$c;

.field private static final w:[Landroid/support/d/a$c;

.field private static final x:[Landroid/support/d/a$c;

.field private static final y:[Landroid/support/d/a$c;

.field private static final z:Landroid/support/d/a$c;


# instance fields
.field private final G:Ljava/lang/String;

.field private final H:Landroid/content/res/AssetManager$AssetInputStream;

.field private I:I

.field private final J:[Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/support/d/a$b;",
            ">;"
        }
    .end annotation
.end field

.field private K:Ljava/nio/ByteOrder;

.field private L:Z

.field private M:I

.field private N:I

.field private O:[B

.field private P:I

.field private Q:I

.field private R:I

.field private S:I

.field private T:I

.field private U:I

.field private V:Z


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x5

    const/4 v9, 0x2

    const/4 v4, 0x4

    const/4 v3, 0x3

    const/4 v5, 0x0

    .line 399
    new-array v0, v4, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x6

    .line 400
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v9

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    .line 399
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Landroid/support/d/a;->f:Ljava/util/List;

    .line 401
    new-array v0, v4, [Ljava/lang/Integer;

    const/4 v1, 0x0

    .line 402
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v9

    .line 403
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    .line 401
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Landroid/support/d/a;->g:Ljava/util/List;

    .line 412
    new-array v0, v3, [B

    fill-array-data v0, :array_0

    sput-object v0, Landroid/support/d/a;->a:[B

    .line 424
    const/4 v0, 0x6

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Landroid/support/d/a;->h:[B

    .line 426
    const/16 v0, 0xa

    new-array v0, v0, [B

    fill-array-data v0, :array_2

    sput-object v0, Landroid/support/d/a;->i:[B

    .line 471
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "BYTE"

    aput-object v2, v0, v1

    const-string v1, "STRING"

    aput-object v1, v0, v9

    const-string v1, "USHORT"

    aput-object v1, v0, v3

    const-string v1, "ULONG"

    aput-object v1, v0, v4

    const-string v1, "URATIONAL"

    aput-object v1, v0, v10

    const/4 v1, 0x6

    const-string v2, "SBYTE"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "UNDEFINED"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "SSHORT"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "SLONG"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "SRATIONAL"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "SINGLE"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "DOUBLE"

    aput-object v2, v0, v1

    sput-object v0, Landroid/support/d/a;->b:[Ljava/lang/String;

    .line 476
    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Landroid/support/d/a;->c:[I

    .line 479
    const/16 v0, 0x8

    new-array v0, v0, [B

    fill-array-data v0, :array_4

    sput-object v0, Landroid/support/d/a;->k:[B

    .line 502
    new-array v0, v3, [I

    fill-array-data v0, :array_5

    sput-object v0, Landroid/support/d/a;->l:[I

    .line 503
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput v4, v0, v1

    sput-object v0, Landroid/support/d/a;->m:[I

    .line 504
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0x8

    aput v2, v0, v1

    sput-object v0, Landroid/support/d/a;->n:[I

    .line 958
    const/16 v0, 0x29

    new-array v6, v0, [Landroid/support/d/a$c;

    const/4 v0, 0x0

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "NewSubfileType"

    const/16 v7, 0xfe

    invoke-direct {v1, v2, v7, v4, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/4 v0, 0x1

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "SubfileType"

    const/16 v7, 0xff

    invoke-direct {v1, v2, v7, v4, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    new-instance v0, Landroid/support/d/a$c;

    const-string v1, "ImageWidth"

    const/16 v2, 0x100

    invoke-direct/range {v0 .. v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IIILandroid/support/d/a$1;)V

    aput-object v0, v6, v9

    new-instance v0, Landroid/support/d/a$c;

    const-string v1, "ImageLength"

    const/16 v2, 0x101

    invoke-direct/range {v0 .. v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IIILandroid/support/d/a$1;)V

    aput-object v0, v6, v3

    new-instance v0, Landroid/support/d/a$c;

    const-string v1, "BitsPerSample"

    const/16 v2, 0x102

    invoke-direct {v0, v1, v2, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v0, v6, v4

    new-instance v0, Landroid/support/d/a$c;

    const-string v1, "Compression"

    const/16 v2, 0x103

    invoke-direct {v0, v1, v2, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v0, v6, v10

    const/4 v0, 0x6

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "PhotometricInterpretation"

    const/16 v7, 0x106

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/4 v0, 0x7

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "ImageDescription"

    const/16 v7, 0x10e

    invoke-direct {v1, v2, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x8

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "Make"

    const/16 v7, 0x10f

    invoke-direct {v1, v2, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x9

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "Model"

    const/16 v7, 0x110

    invoke-direct {v1, v2, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v7, 0xa

    new-instance v0, Landroid/support/d/a$c;

    const-string v1, "StripOffsets"

    const/16 v2, 0x111

    invoke-direct/range {v0 .. v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IIILandroid/support/d/a$1;)V

    aput-object v0, v6, v7

    const/16 v0, 0xb

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "Orientation"

    const/16 v7, 0x112

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0xc

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "SamplesPerPixel"

    const/16 v7, 0x115

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v7, 0xd

    new-instance v0, Landroid/support/d/a$c;

    const-string v1, "RowsPerStrip"

    const/16 v2, 0x116

    invoke-direct/range {v0 .. v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IIILandroid/support/d/a$1;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    new-instance v0, Landroid/support/d/a$c;

    const-string v1, "StripByteCounts"

    const/16 v2, 0x117

    invoke-direct/range {v0 .. v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IIILandroid/support/d/a$1;)V

    aput-object v0, v6, v7

    const/16 v0, 0xf

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "XResolution"

    const/16 v7, 0x11a

    invoke-direct {v1, v2, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x10

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "YResolution"

    const/16 v7, 0x11b

    invoke-direct {v1, v2, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x11

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "PlanarConfiguration"

    const/16 v7, 0x11c

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x12

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "ResolutionUnit"

    const/16 v7, 0x128

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x13

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "TransferFunction"

    const/16 v7, 0x12d

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x14

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "Software"

    const/16 v7, 0x131

    invoke-direct {v1, v2, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x15

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "DateTime"

    const/16 v7, 0x132

    invoke-direct {v1, v2, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x16

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "Artist"

    const/16 v7, 0x13b

    invoke-direct {v1, v2, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x17

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "WhitePoint"

    const/16 v7, 0x13e

    invoke-direct {v1, v2, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x18

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "PrimaryChromaticities"

    const/16 v7, 0x13f

    invoke-direct {v1, v2, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x19

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "SubIFDPointer"

    const/16 v7, 0x14a

    invoke-direct {v1, v2, v7, v4, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x1a

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "JPEGInterchangeFormat"

    const/16 v7, 0x201

    invoke-direct {v1, v2, v7, v4, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x1b

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "JPEGInterchangeFormatLength"

    const/16 v7, 0x202

    invoke-direct {v1, v2, v7, v4, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x1c

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "YCbCrCoefficients"

    const/16 v7, 0x211

    invoke-direct {v1, v2, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x1d

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "YCbCrSubSampling"

    const/16 v7, 0x212

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x1e

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "YCbCrPositioning"

    const/16 v7, 0x213

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x1f

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "ReferenceBlackWhite"

    const/16 v7, 0x214

    invoke-direct {v1, v2, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x20

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "Copyright"

    const v7, 0x8298

    invoke-direct {v1, v2, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x21

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "ExifIFDPointer"

    const v7, 0x8769

    invoke-direct {v1, v2, v7, v4, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x22

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "GPSInfoIFDPointer"

    const v7, 0x8825

    invoke-direct {v1, v2, v7, v4, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x23

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "SensorTopBorder"

    invoke-direct {v1, v2, v4, v4, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x24

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "SensorLeftBorder"

    invoke-direct {v1, v2, v10, v4, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x25

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "SensorBottomBorder"

    const/4 v7, 0x6

    invoke-direct {v1, v2, v7, v4, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x26

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "SensorRightBorder"

    const/4 v7, 0x7

    invoke-direct {v1, v2, v7, v4, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x27

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "ISO"

    const/16 v7, 0x17

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x28

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "JpgFromRaw"

    const/16 v7, 0x2e

    const/4 v8, 0x7

    invoke-direct {v1, v2, v7, v8, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    sput-object v6, Landroid/support/d/a;->o:[Landroid/support/d/a$c;

    .line 1007
    const/16 v0, 0x3b

    new-array v6, v0, [Landroid/support/d/a$c;

    const/4 v0, 0x0

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "ExposureTime"

    const v7, 0x829a

    invoke-direct {v1, v2, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/4 v0, 0x1

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "FNumber"

    const v7, 0x829d

    invoke-direct {v1, v2, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    new-instance v0, Landroid/support/d/a$c;

    const-string v1, "ExposureProgram"

    const v2, 0x8822

    invoke-direct {v0, v1, v2, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v0, v6, v9

    new-instance v0, Landroid/support/d/a$c;

    const-string v1, "SpectralSensitivity"

    const v2, 0x8824

    invoke-direct {v0, v1, v2, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v0, v6, v3

    new-instance v0, Landroid/support/d/a$c;

    const-string v1, "ISOSpeedRatings"

    const v2, 0x8827

    invoke-direct {v0, v1, v2, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v0, v6, v4

    new-instance v0, Landroid/support/d/a$c;

    const-string v1, "OECF"

    const v2, 0x8828

    const/4 v7, 0x7

    invoke-direct {v0, v1, v2, v7, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v0, v6, v10

    const/4 v0, 0x6

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "ExifVersion"

    const v7, 0x9000

    invoke-direct {v1, v2, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/4 v0, 0x7

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "DateTimeOriginal"

    const v7, 0x9003

    invoke-direct {v1, v2, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x8

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "DateTimeDigitized"

    const v7, 0x9004

    invoke-direct {v1, v2, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x9

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "ComponentsConfiguration"

    const v7, 0x9101

    const/4 v8, 0x7

    invoke-direct {v1, v2, v7, v8, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0xa

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "CompressedBitsPerPixel"

    const v7, 0x9102

    invoke-direct {v1, v2, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0xb

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "ShutterSpeedValue"

    const v7, 0x9201

    const/16 v8, 0xa

    invoke-direct {v1, v2, v7, v8, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0xc

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "ApertureValue"

    const v7, 0x9202

    invoke-direct {v1, v2, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0xd

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "BrightnessValue"

    const v7, 0x9203

    const/16 v8, 0xa

    invoke-direct {v1, v2, v7, v8, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0xe

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "ExposureBiasValue"

    const v7, 0x9204

    const/16 v8, 0xa

    invoke-direct {v1, v2, v7, v8, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0xf

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "MaxApertureValue"

    const v7, 0x9205

    invoke-direct {v1, v2, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x10

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "SubjectDistance"

    const v7, 0x9206

    invoke-direct {v1, v2, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x11

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "MeteringMode"

    const v7, 0x9207

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x12

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "LightSource"

    const v7, 0x9208

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x13

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "Flash"

    const v7, 0x9209

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x14

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "FocalLength"

    const v7, 0x920a

    invoke-direct {v1, v2, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x15

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "SubjectArea"

    const v7, 0x9214

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x16

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "MakerNote"

    const v7, 0x927c

    const/4 v8, 0x7

    invoke-direct {v1, v2, v7, v8, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x17

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "UserComment"

    const v7, 0x9286

    const/4 v8, 0x7

    invoke-direct {v1, v2, v7, v8, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x18

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "SubSecTime"

    const v7, 0x9290

    invoke-direct {v1, v2, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x19

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "SubSecTimeOriginal"

    const v7, 0x9291

    invoke-direct {v1, v2, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x1a

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "SubSecTimeDigitized"

    const v7, 0x9292

    invoke-direct {v1, v2, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x1b

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "FlashpixVersion"

    const v7, 0xa000

    const/4 v8, 0x7

    invoke-direct {v1, v2, v7, v8, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x1c

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "ColorSpace"

    const v7, 0xa001

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v7, 0x1d

    new-instance v0, Landroid/support/d/a$c;

    const-string v1, "PixelXDimension"

    const v2, 0xa002

    invoke-direct/range {v0 .. v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IIILandroid/support/d/a$1;)V

    aput-object v0, v6, v7

    const/16 v7, 0x1e

    new-instance v0, Landroid/support/d/a$c;

    const-string v1, "PixelYDimension"

    const v2, 0xa003

    invoke-direct/range {v0 .. v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IIILandroid/support/d/a$1;)V

    aput-object v0, v6, v7

    const/16 v0, 0x1f

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "RelatedSoundFile"

    const v7, 0xa004

    invoke-direct {v1, v2, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x20

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "InteroperabilityIFDPointer"

    const v7, 0xa005

    invoke-direct {v1, v2, v7, v4, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x21

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "FlashEnergy"

    const v7, 0xa20b

    invoke-direct {v1, v2, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x22

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "SpatialFrequencyResponse"

    const v7, 0xa20c

    const/4 v8, 0x7

    invoke-direct {v1, v2, v7, v8, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x23

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "FocalPlaneXResolution"

    const v7, 0xa20e

    invoke-direct {v1, v2, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x24

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "FocalPlaneYResolution"

    const v7, 0xa20f

    invoke-direct {v1, v2, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x25

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "FocalPlaneResolutionUnit"

    const v7, 0xa210

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x26

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "SubjectLocation"

    const v7, 0xa214

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x27

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "ExposureIndex"

    const v7, 0xa215

    invoke-direct {v1, v2, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x28

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "SensingMethod"

    const v7, 0xa217

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x29

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "FileSource"

    const v7, 0xa300

    const/4 v8, 0x7

    invoke-direct {v1, v2, v7, v8, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x2a

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "SceneType"

    const v7, 0xa301

    const/4 v8, 0x7

    invoke-direct {v1, v2, v7, v8, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x2b

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "CFAPattern"

    const v7, 0xa302

    const/4 v8, 0x7

    invoke-direct {v1, v2, v7, v8, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x2c

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "CustomRendered"

    const v7, 0xa401

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x2d

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "ExposureMode"

    const v7, 0xa402

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x2e

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "WhiteBalance"

    const v7, 0xa403

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x2f

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "DigitalZoomRatio"

    const v7, 0xa404

    invoke-direct {v1, v2, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x30

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "FocalLengthIn35mmFilm"

    const v7, 0xa405

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x31

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "SceneCaptureType"

    const v7, 0xa406

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x32

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "GainControl"

    const v7, 0xa407

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x33

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "Contrast"

    const v7, 0xa408

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x34

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "Saturation"

    const v7, 0xa409

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x35

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "Sharpness"

    const v7, 0xa40a

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x36

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "DeviceSettingDescription"

    const v7, 0xa40b

    const/4 v8, 0x7

    invoke-direct {v1, v2, v7, v8, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x37

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "SubjectDistanceRange"

    const v7, 0xa40c

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x38

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "ImageUniqueID"

    const v7, 0xa420

    invoke-direct {v1, v2, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x39

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "DNGVersion"

    const v7, 0xc612

    const/4 v8, 0x1

    invoke-direct {v1, v2, v7, v8, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v7, 0x3a

    new-instance v0, Landroid/support/d/a$c;

    const-string v1, "DefaultCropSize"

    const v2, 0xc620

    invoke-direct/range {v0 .. v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IIILandroid/support/d/a$1;)V

    aput-object v0, v6, v7

    sput-object v6, Landroid/support/d/a;->p:[Landroid/support/d/a$c;

    .line 1070
    const/16 v0, 0x1f

    new-array v0, v0, [Landroid/support/d/a$c;

    const/4 v1, 0x0

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "GPSVersionID"

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-direct {v2, v6, v7, v8, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "GPSLatitudeRef"

    const/4 v7, 0x1

    invoke-direct {v2, v6, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "GPSLatitude"

    invoke-direct {v1, v2, v9, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v0, v9

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "GPSLongitudeRef"

    invoke-direct {v1, v2, v3, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v0, v3

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "GPSLongitude"

    invoke-direct {v1, v2, v4, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v0, v4

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "GPSAltitudeRef"

    const/4 v6, 0x1

    invoke-direct {v1, v2, v10, v6, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v0, v10

    const/4 v1, 0x6

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "GPSAltitude"

    const/4 v7, 0x6

    invoke-direct {v2, v6, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "GPSTimeStamp"

    const/4 v7, 0x7

    invoke-direct {v2, v6, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "GPSSatellites"

    const/16 v7, 0x8

    invoke-direct {v2, v6, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "GPSStatus"

    const/16 v7, 0x9

    invoke-direct {v2, v6, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "GPSMeasureMode"

    const/16 v7, 0xa

    invoke-direct {v2, v6, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "GPSDOP"

    const/16 v7, 0xb

    invoke-direct {v2, v6, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "GPSSpeedRef"

    const/16 v7, 0xc

    invoke-direct {v2, v6, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "GPSSpeed"

    const/16 v7, 0xd

    invoke-direct {v2, v6, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "GPSTrackRef"

    const/16 v7, 0xe

    invoke-direct {v2, v6, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "GPSTrack"

    const/16 v7, 0xf

    invoke-direct {v2, v6, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "GPSImgDirectionRef"

    const/16 v7, 0x10

    invoke-direct {v2, v6, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "GPSImgDirection"

    const/16 v7, 0x11

    invoke-direct {v2, v6, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "GPSMapDatum"

    const/16 v7, 0x12

    invoke-direct {v2, v6, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "GPSDestLatitudeRef"

    const/16 v7, 0x13

    invoke-direct {v2, v6, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "GPSDestLatitude"

    const/16 v7, 0x14

    invoke-direct {v2, v6, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    const/16 v1, 0x15

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "GPSDestLongitudeRef"

    const/16 v7, 0x15

    invoke-direct {v2, v6, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "GPSDestLongitude"

    const/16 v7, 0x16

    invoke-direct {v2, v6, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    const/16 v1, 0x17

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "GPSDestBearingRef"

    const/16 v7, 0x17

    invoke-direct {v2, v6, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    const/16 v1, 0x18

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "GPSDestBearing"

    const/16 v7, 0x18

    invoke-direct {v2, v6, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    const/16 v1, 0x19

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "GPSDestDistanceRef"

    const/16 v7, 0x19

    invoke-direct {v2, v6, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "GPSDestDistance"

    const/16 v7, 0x1a

    invoke-direct {v2, v6, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "GPSProcessingMethod"

    const/16 v7, 0x1b

    const/4 v8, 0x7

    invoke-direct {v2, v6, v7, v8, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "GPSAreaInformation"

    const/16 v7, 0x1c

    const/4 v8, 0x7

    invoke-direct {v2, v6, v7, v8, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "GPSDateStamp"

    const/16 v7, 0x1d

    invoke-direct {v2, v6, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "GPSDifferential"

    const/16 v7, 0x1e

    invoke-direct {v2, v6, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    sput-object v0, Landroid/support/d/a;->q:[Landroid/support/d/a$c;

    .line 1104
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/support/d/a$c;

    const/4 v1, 0x0

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "InteroperabilityIndex"

    const/4 v7, 0x1

    invoke-direct {v2, v6, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    sput-object v0, Landroid/support/d/a;->r:[Landroid/support/d/a$c;

    .line 1108
    const/16 v0, 0x25

    new-array v6, v0, [Landroid/support/d/a$c;

    const/4 v0, 0x0

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "NewSubfileType"

    const/16 v7, 0xfe

    invoke-direct {v1, v2, v7, v4, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/4 v0, 0x1

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "SubfileType"

    const/16 v7, 0xff

    invoke-direct {v1, v2, v7, v4, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    new-instance v0, Landroid/support/d/a$c;

    const-string v1, "ThumbnailImageWidth"

    const/16 v2, 0x100

    invoke-direct/range {v0 .. v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IIILandroid/support/d/a$1;)V

    aput-object v0, v6, v9

    new-instance v0, Landroid/support/d/a$c;

    const-string v1, "ThumbnailImageLength"

    const/16 v2, 0x101

    invoke-direct/range {v0 .. v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IIILandroid/support/d/a$1;)V

    aput-object v0, v6, v3

    new-instance v0, Landroid/support/d/a$c;

    const-string v1, "BitsPerSample"

    const/16 v2, 0x102

    invoke-direct {v0, v1, v2, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v0, v6, v4

    new-instance v0, Landroid/support/d/a$c;

    const-string v1, "Compression"

    const/16 v2, 0x103

    invoke-direct {v0, v1, v2, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v0, v6, v10

    const/4 v0, 0x6

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "PhotometricInterpretation"

    const/16 v7, 0x106

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/4 v0, 0x7

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "ImageDescription"

    const/16 v7, 0x10e

    invoke-direct {v1, v2, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x8

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "Make"

    const/16 v7, 0x10f

    invoke-direct {v1, v2, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x9

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "Model"

    const/16 v7, 0x110

    invoke-direct {v1, v2, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v7, 0xa

    new-instance v0, Landroid/support/d/a$c;

    const-string v1, "StripOffsets"

    const/16 v2, 0x111

    invoke-direct/range {v0 .. v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IIILandroid/support/d/a$1;)V

    aput-object v0, v6, v7

    const/16 v0, 0xb

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "Orientation"

    const/16 v7, 0x112

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0xc

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "SamplesPerPixel"

    const/16 v7, 0x115

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v7, 0xd

    new-instance v0, Landroid/support/d/a$c;

    const-string v1, "RowsPerStrip"

    const/16 v2, 0x116

    invoke-direct/range {v0 .. v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IIILandroid/support/d/a$1;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    new-instance v0, Landroid/support/d/a$c;

    const-string v1, "StripByteCounts"

    const/16 v2, 0x117

    invoke-direct/range {v0 .. v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IIILandroid/support/d/a$1;)V

    aput-object v0, v6, v7

    const/16 v0, 0xf

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "XResolution"

    const/16 v7, 0x11a

    invoke-direct {v1, v2, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x10

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "YResolution"

    const/16 v7, 0x11b

    invoke-direct {v1, v2, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x11

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "PlanarConfiguration"

    const/16 v7, 0x11c

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x12

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "ResolutionUnit"

    const/16 v7, 0x128

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x13

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "TransferFunction"

    const/16 v7, 0x12d

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x14

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "Software"

    const/16 v7, 0x131

    invoke-direct {v1, v2, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x15

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "DateTime"

    const/16 v7, 0x132

    invoke-direct {v1, v2, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x16

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "Artist"

    const/16 v7, 0x13b

    invoke-direct {v1, v2, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x17

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "WhitePoint"

    const/16 v7, 0x13e

    invoke-direct {v1, v2, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x18

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "PrimaryChromaticities"

    const/16 v7, 0x13f

    invoke-direct {v1, v2, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x19

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "SubIFDPointer"

    const/16 v7, 0x14a

    invoke-direct {v1, v2, v7, v4, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x1a

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "JPEGInterchangeFormat"

    const/16 v7, 0x201

    invoke-direct {v1, v2, v7, v4, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x1b

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "JPEGInterchangeFormatLength"

    const/16 v7, 0x202

    invoke-direct {v1, v2, v7, v4, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x1c

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "YCbCrCoefficients"

    const/16 v7, 0x211

    invoke-direct {v1, v2, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x1d

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "YCbCrSubSampling"

    const/16 v7, 0x212

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x1e

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "YCbCrPositioning"

    const/16 v7, 0x213

    invoke-direct {v1, v2, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x1f

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "ReferenceBlackWhite"

    const/16 v7, 0x214

    invoke-direct {v1, v2, v7, v10, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x20

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "Copyright"

    const v7, 0x8298

    invoke-direct {v1, v2, v7, v9, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x21

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "ExifIFDPointer"

    const v7, 0x8769

    invoke-direct {v1, v2, v7, v4, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x22

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "GPSInfoIFDPointer"

    const v7, 0x8825

    invoke-direct {v1, v2, v7, v4, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v0, 0x23

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "DNGVersion"

    const v7, 0xc612

    const/4 v8, 0x1

    invoke-direct {v1, v2, v7, v8, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v6, v0

    const/16 v7, 0x24

    new-instance v0, Landroid/support/d/a$c;

    const-string v1, "DefaultCropSize"

    const v2, 0xc620

    invoke-direct/range {v0 .. v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IIILandroid/support/d/a$1;)V

    aput-object v0, v6, v7

    sput-object v6, Landroid/support/d/a;->s:[Landroid/support/d/a$c;

    .line 1151
    new-instance v0, Landroid/support/d/a$c;

    const-string v1, "StripOffsets"

    const/16 v2, 0x111

    invoke-direct {v0, v1, v2, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    sput-object v0, Landroid/support/d/a;->t:Landroid/support/d/a$c;

    .line 1155
    new-array v0, v3, [Landroid/support/d/a$c;

    const/4 v1, 0x0

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "ThumbnailImage"

    const/16 v7, 0x100

    const/4 v8, 0x7

    invoke-direct {v2, v6, v7, v8, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "CameraSettingsIFDPointer"

    const/16 v7, 0x2020

    invoke-direct {v2, v6, v7, v4, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "ImageProcessingIFDPointer"

    const/16 v6, 0x2040

    invoke-direct {v1, v2, v6, v4, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v0, v9

    sput-object v0, Landroid/support/d/a;->u:[Landroid/support/d/a$c;

    .line 1160
    new-array v0, v9, [Landroid/support/d/a$c;

    const/4 v1, 0x0

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "PreviewImageStart"

    const/16 v7, 0x101

    invoke-direct {v2, v6, v7, v4, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "PreviewImageLength"

    const/16 v7, 0x102

    invoke-direct {v2, v6, v7, v4, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    sput-object v0, Landroid/support/d/a;->v:[Landroid/support/d/a$c;

    .line 1164
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/support/d/a$c;

    const/4 v1, 0x0

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "AspectFrame"

    const/16 v7, 0x1113

    invoke-direct {v2, v6, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    sput-object v0, Landroid/support/d/a;->w:[Landroid/support/d/a$c;

    .line 1168
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/support/d/a$c;

    const/4 v1, 0x0

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "ColorSpace"

    const/16 v7, 0x37

    invoke-direct {v2, v6, v7, v3, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    sput-object v0, Landroid/support/d/a;->x:[Landroid/support/d/a$c;

    .line 1195
    const/16 v0, 0xa

    new-array v0, v0, [[Landroid/support/d/a$c;

    const/4 v1, 0x0

    sget-object v2, Landroid/support/d/a;->o:[Landroid/support/d/a$c;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Landroid/support/d/a;->p:[Landroid/support/d/a$c;

    aput-object v2, v0, v1

    sget-object v1, Landroid/support/d/a;->q:[Landroid/support/d/a$c;

    aput-object v1, v0, v9

    sget-object v1, Landroid/support/d/a;->r:[Landroid/support/d/a$c;

    aput-object v1, v0, v3

    sget-object v1, Landroid/support/d/a;->s:[Landroid/support/d/a$c;

    aput-object v1, v0, v4

    sget-object v1, Landroid/support/d/a;->o:[Landroid/support/d/a$c;

    aput-object v1, v0, v10

    const/4 v1, 0x6

    sget-object v2, Landroid/support/d/a;->u:[Landroid/support/d/a$c;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Landroid/support/d/a;->v:[Landroid/support/d/a$c;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Landroid/support/d/a;->w:[Landroid/support/d/a$c;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Landroid/support/d/a;->x:[Landroid/support/d/a$c;

    aput-object v2, v0, v1

    sput-object v0, Landroid/support/d/a;->d:[[Landroid/support/d/a$c;

    .line 1201
    const/4 v0, 0x6

    new-array v0, v0, [Landroid/support/d/a$c;

    const/4 v1, 0x0

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "SubIFDPointer"

    const/16 v7, 0x14a

    invoke-direct {v2, v6, v7, v4, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Landroid/support/d/a$c;

    const-string v6, "ExifIFDPointer"

    const v7, 0x8769

    invoke-direct {v2, v6, v7, v4, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v2, v0, v1

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "GPSInfoIFDPointer"

    const v6, 0x8825

    invoke-direct {v1, v2, v6, v4, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v0, v9

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "InteroperabilityIFDPointer"

    const v6, 0xa005

    invoke-direct {v1, v2, v6, v4, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v0, v3

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "CameraSettingsIFDPointer"

    const/16 v6, 0x2020

    const/4 v7, 0x1

    invoke-direct {v1, v2, v6, v7, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v0, v4

    new-instance v1, Landroid/support/d/a$c;

    const-string v2, "ImageProcessingIFDPointer"

    const/16 v6, 0x2040

    const/4 v7, 0x1

    invoke-direct {v1, v2, v6, v7, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    aput-object v1, v0, v10

    sput-object v0, Landroid/support/d/a;->y:[Landroid/support/d/a$c;

    .line 1211
    new-instance v0, Landroid/support/d/a$c;

    const-string v1, "JPEGInterchangeFormat"

    const/16 v2, 0x201

    invoke-direct {v0, v1, v2, v4, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    sput-object v0, Landroid/support/d/a;->z:Landroid/support/d/a$c;

    .line 1213
    new-instance v0, Landroid/support/d/a$c;

    const-string v1, "JPEGInterchangeFormatLength"

    const/16 v2, 0x202

    invoke-direct {v0, v1, v2, v4, v5}, Landroid/support/d/a$c;-><init>(Ljava/lang/String;IILandroid/support/d/a$1;)V

    sput-object v0, Landroid/support/d/a;->A:Landroid/support/d/a$c;

    .line 1218
    sget-object v0, Landroid/support/d/a;->d:[[Landroid/support/d/a$c;

    array-length v0, v0

    new-array v0, v0, [Ljava/util/HashMap;

    sput-object v0, Landroid/support/d/a;->B:[Ljava/util/HashMap;

    .line 1222
    sget-object v0, Landroid/support/d/a;->d:[[Landroid/support/d/a$c;

    array-length v0, v0

    new-array v0, v0, [Ljava/util/HashMap;

    sput-object v0, Landroid/support/d/a;->C:[Ljava/util/HashMap;

    .line 1224
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v10, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "FNumber"

    aput-object v5, v1, v2

    const/4 v2, 0x1

    const-string v5, "DigitalZoomRatio"

    aput-object v5, v1, v2

    const-string v2, "ExposureTime"

    aput-object v2, v1, v9

    const-string v2, "SubjectDistance"

    aput-object v2, v1, v3

    const-string v2, "GPSTimeStamp"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Landroid/support/d/a;->D:Ljava/util/HashSet;

    .line 1229
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Landroid/support/d/a;->E:Ljava/util/HashMap;

    .line 1236
    const-string v0, "US-ASCII"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Landroid/support/d/a;->F:Ljava/nio/charset/Charset;

    .line 1238
    const-string v0, "Exif\u0000\u0000"

    sget-object v1, Landroid/support/d/a;->F:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    sput-object v0, Landroid/support/d/a;->e:[B

    .line 1277
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy:MM:dd HH:mm:ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Landroid/support/d/a;->j:Ljava/text/SimpleDateFormat;

    .line 1278
    sget-object v0, Landroid/support/d/a;->j:Ljava/text/SimpleDateFormat;

    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1281
    const/4 v0, 0x0

    :goto_0
    sget-object v1, Landroid/support/d/a;->d:[[Landroid/support/d/a$c;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1282
    sget-object v1, Landroid/support/d/a;->B:[Ljava/util/HashMap;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    aput-object v2, v1, v0

    .line 1283
    sget-object v1, Landroid/support/d/a;->C:[Ljava/util/HashMap;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    aput-object v2, v1, v0

    .line 1284
    sget-object v1, Landroid/support/d/a;->d:[[Landroid/support/d/a$c;

    aget-object v2, v1, v0

    array-length v5, v2

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v5, :cond_0

    aget-object v6, v2, v1

    .line 1285
    sget-object v7, Landroid/support/d/a;->B:[Ljava/util/HashMap;

    aget-object v7, v7, v0

    iget v8, v6, Landroid/support/d/a$c;->a:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1286
    sget-object v7, Landroid/support/d/a;->C:[Ljava/util/HashMap;

    aget-object v7, v7, v0

    iget-object v8, v6, Landroid/support/d/a$c;->b:Ljava/lang/String;

    invoke-virtual {v7, v8, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1284
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1281
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1291
    :cond_1
    sget-object v0, Landroid/support/d/a;->E:Ljava/util/HashMap;

    sget-object v1, Landroid/support/d/a;->y:[Landroid/support/d/a$c;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iget v1, v1, Landroid/support/d/a$c;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1292
    sget-object v0, Landroid/support/d/a;->E:Ljava/util/HashMap;

    sget-object v1, Landroid/support/d/a;->y:[Landroid/support/d/a$c;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    iget v1, v1, Landroid/support/d/a$c;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1293
    sget-object v0, Landroid/support/d/a;->E:Ljava/util/HashMap;

    sget-object v1, Landroid/support/d/a;->y:[Landroid/support/d/a$c;

    aget-object v1, v1, v9

    iget v1, v1, Landroid/support/d/a$c;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1294
    sget-object v0, Landroid/support/d/a;->E:Ljava/util/HashMap;

    sget-object v1, Landroid/support/d/a;->y:[Landroid/support/d/a$c;

    aget-object v1, v1, v3

    iget v1, v1, Landroid/support/d/a$c;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1295
    sget-object v0, Landroid/support/d/a;->E:Ljava/util/HashMap;

    sget-object v1, Landroid/support/d/a;->y:[Landroid/support/d/a$c;

    aget-object v1, v1, v4

    iget v1, v1, Landroid/support/d/a$c;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1296
    sget-object v0, Landroid/support/d/a;->E:Ljava/util/HashMap;

    sget-object v1, Landroid/support/d/a;->y:[Landroid/support/d/a$c;

    aget-object v1, v1, v10

    iget v1, v1, Landroid/support/d/a$c;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1319
    const-string v0, ".*[1-9].*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Landroid/support/d/a;->W:Ljava/util/regex/Pattern;

    .line 1321
    const-string v0, "^([0-9][0-9]):([0-9][0-9]):([0-9][0-9])$"

    .line 1322
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Landroid/support/d/a;->X:Ljava/util/regex/Pattern;

    .line 1321
    return-void

    .line 412
    nop

    :array_0
    .array-data 1
        -0x1t
        -0x28t
        -0x1t
    .end array-data

    .line 424
    :array_1
    .array-data 1
        0x4ft
        0x4ct
        0x59t
        0x4dt
        0x50t
        0x0t
    .end array-data

    .line 426
    nop

    :array_2
    .array-data 1
        0x4ft
        0x4ct
        0x59t
        0x4dt
        0x50t
        0x55t
        0x53t
        0x0t
        0x49t
        0x49t
    .end array-data

    .line 476
    nop

    :array_3
    .array-data 4
        0x0
        0x1
        0x1
        0x2
        0x4
        0x8
        0x1
        0x1
        0x2
        0x4
        0x8
        0x4
        0x8
        0x1
    .end array-data

    .line 479
    :array_4
    .array-data 1
        0x41t
        0x53t
        0x43t
        0x49t
        0x49t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 502
    :array_5
    .array-data 4
        0x8
        0x8
        0x8
    .end array-data
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1302
    sget-object v0, Landroid/support/d/a;->d:[[Landroid/support/d/a$c;

    array-length v0, v0

    new-array v0, v0, [Ljava/util/HashMap;

    iput-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    .line 1304
    sget-object v0, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    iput-object v0, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 1349
    if-nez p1, :cond_0

    .line 1350
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "inputStream cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1352
    :cond_0
    iput-object v1, p0, Landroid/support/d/a;->G:Ljava/lang/String;

    .line 1353
    instance-of v0, p1, Landroid/content/res/AssetManager$AssetInputStream;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 1354
    check-cast v0, Landroid/content/res/AssetManager$AssetInputStream;

    iput-object v0, p0, Landroid/support/d/a;->H:Landroid/content/res/AssetManager$AssetInputStream;

    .line 1358
    :goto_0
    invoke-direct {p0, p1}, Landroid/support/d/a;->a(Ljava/io/InputStream;)V

    .line 1359
    return-void

    .line 1356
    :cond_1
    iput-object v1, p0, Landroid/support/d/a;->H:Landroid/content/res/AssetManager$AssetInputStream;

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1327
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1302
    sget-object v0, Landroid/support/d/a;->d:[[Landroid/support/d/a$c;

    array-length v0, v0

    new-array v0, v0, [Ljava/util/HashMap;

    iput-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    .line 1304
    sget-object v0, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    iput-object v0, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 1328
    if-nez p1, :cond_0

    .line 1329
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "filename cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1332
    :cond_0
    iput-object v2, p0, Landroid/support/d/a;->H:Landroid/content/res/AssetManager$AssetInputStream;

    .line 1333
    iput-object p1, p0, Landroid/support/d/a;->G:Ljava/lang/String;

    .line 1335
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1336
    :try_start_1
    invoke-direct {p0, v1}, Landroid/support/d/a;->a(Ljava/io/InputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1338
    invoke-static {v1}, Landroid/support/d/a;->a(Ljava/io/Closeable;)V

    .line 1340
    return-void

    .line 1338
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_0
    invoke-static {v1}, Landroid/support/d/a;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method private a(Ljava/io/BufferedInputStream;)I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v2, 0x1388

    .line 2233
    invoke-virtual {p1, v2}, Ljava/io/BufferedInputStream;->mark(I)V

    .line 2234
    new-array v0, v2, [B

    .line 2235
    invoke-virtual {p1, v0}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v1

    if-eq v1, v2, :cond_0

    .line 2236
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 2238
    :cond_0
    invoke-virtual {p1}, Ljava/io/BufferedInputStream;->reset()V

    .line 2239
    invoke-static {v0}, Landroid/support/d/a;->a([B)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2240
    const/4 v0, 0x4

    .line 2249
    :goto_0
    return v0

    .line 2241
    :cond_1
    invoke-direct {p0, v0}, Landroid/support/d/a;->b([B)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2242
    const/16 v0, 0x9

    goto :goto_0

    .line 2243
    :cond_2
    invoke-direct {p0, v0}, Landroid/support/d/a;->c([B)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2244
    const/4 v0, 0x7

    goto :goto_0

    .line 2245
    :cond_3
    invoke-direct {p0, v0}, Landroid/support/d/a;->d([B)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2246
    const/16 v0, 0xa

    goto :goto_0

    .line 2249
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a()Ljava/nio/charset/Charset;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Landroid/support/d/a;->F:Ljava/nio/charset/Charset;

    return-object v0
.end method

.method private a(II)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3948
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v0, v0, p2

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3985
    :cond_0
    :goto_0
    return-void

    .line 3955
    :cond_1
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v0, v0, p1

    const-string v1, "ImageLength"

    .line 3956
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/d/a$b;

    .line 3957
    iget-object v1, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v1, v1, p1

    const-string v2, "ImageWidth"

    .line 3958
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/d/a$b;

    .line 3959
    iget-object v2, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v2, v2, p2

    const-string v3, "ImageLength"

    .line 3960
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/d/a$b;

    .line 3961
    iget-object v3, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v3, v3, p2

    const-string v4, "ImageWidth"

    .line 3962
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/d/a$b;

    .line 3964
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 3968
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 3973
    iget-object v4, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v4}, Landroid/support/d/a$b;->b(Ljava/nio/ByteOrder;)I

    move-result v0

    .line 3974
    iget-object v4, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v4}, Landroid/support/d/a$b;->b(Ljava/nio/ByteOrder;)I

    move-result v1

    .line 3975
    iget-object v4, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v4}, Landroid/support/d/a$b;->b(Ljava/nio/ByteOrder;)I

    move-result v2

    .line 3976
    iget-object v4, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    invoke-virtual {v3, v4}, Landroid/support/d/a$b;->b(Ljava/nio/ByteOrder;)I

    move-result v3

    .line 3978
    if-ge v0, v2, :cond_0

    if-ge v1, v3, :cond_0

    .line 3980
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v0, v0, p1

    .line 3981
    iget-object v1, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    iget-object v2, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v2, v2, p2

    aput-object v2, v1, p1

    .line 3982
    iget-object v1, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aput-object v0, v1, p2

    goto :goto_0
.end method

.method private a(Landroid/support/d/a$a;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x9

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 2472
    invoke-virtual {p1}, Landroid/support/d/a$a;->available()I

    move-result v0

    invoke-direct {p0, p1, v0}, Landroid/support/d/a;->a(Landroid/support/d/a$a;I)V

    .line 2475
    invoke-direct {p0, p1, v1}, Landroid/support/d/a;->b(Landroid/support/d/a$a;I)V

    .line 2478
    invoke-direct {p0, p1, v1}, Landroid/support/d/a;->d(Landroid/support/d/a$a;I)V

    .line 2479
    const/4 v0, 0x5

    invoke-direct {p0, p1, v0}, Landroid/support/d/a;->d(Landroid/support/d/a$a;I)V

    .line 2480
    const/4 v0, 0x4

    invoke-direct {p0, p1, v0}, Landroid/support/d/a;->d(Landroid/support/d/a$a;I)V

    .line 2483
    invoke-direct {p0, p1}, Landroid/support/d/a;->b(Ljava/io/InputStream;)V

    .line 2485
    iget v0, p0, Landroid/support/d/a;->I:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 2488
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v0, v0, v4

    const-string v1, "MakerNote"

    .line 2489
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/d/a$b;

    .line 2490
    if-eqz v0, :cond_0

    .line 2492
    new-instance v1, Landroid/support/d/a$a;

    iget-object v0, v0, Landroid/support/d/a$b;->c:[B

    invoke-direct {v1, v0}, Landroid/support/d/a$a;-><init>([B)V

    .line 2494
    iget-object v0, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v0}, Landroid/support/d/a$a;->a(Ljava/nio/ByteOrder;)V

    .line 2497
    const-wide/16 v2, 0x6

    invoke-virtual {v1, v2, v3}, Landroid/support/d/a$a;->a(J)V

    .line 2500
    invoke-direct {p0, v1, v5}, Landroid/support/d/a;->b(Landroid/support/d/a$a;I)V

    .line 2503
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v0, v0, v5

    const-string v1, "ColorSpace"

    .line 2504
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/d/a$b;

    .line 2505
    if-eqz v0, :cond_0

    .line 2506
    iget-object v1, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v1, v1, v4

    const-string v2, "ColorSpace"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2510
    :cond_0
    return-void
.end method

.method private a(Landroid/support/d/a$a;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2844
    invoke-direct {p0, p1}, Landroid/support/d/a;->e(Landroid/support/d/a$a;)Ljava/nio/ByteOrder;

    move-result-object v0

    iput-object v0, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 2846
    iget-object v0, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    invoke-virtual {p1, v0}, Landroid/support/d/a$a;->a(Ljava/nio/ByteOrder;)V

    .line 2849
    invoke-virtual {p1}, Landroid/support/d/a$a;->readUnsignedShort()I

    move-result v0

    .line 2850
    iget v1, p0, Landroid/support/d/a;->I:I

    const/4 v2, 0x7

    if-eq v1, v2, :cond_0

    iget v1, p0, Landroid/support/d/a;->I:I

    const/16 v2, 0xa

    if-eq v1, v2, :cond_0

    const/16 v1, 0x2a

    if-eq v0, v1, :cond_0

    .line 2851
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid start code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2855
    :cond_0
    invoke-virtual {p1}, Landroid/support/d/a$a;->readInt()I

    move-result v0

    .line 2856
    const/16 v1, 0x8

    if-lt v0, v1, :cond_1

    if-lt v0, p2, :cond_2

    .line 2857
    :cond_1
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid first Ifd offset: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2859
    :cond_2
    add-int/lit8 v0, v0, -0x8

    .line 2860
    if-lez v0, :cond_3

    .line 2861
    invoke-virtual {p1, v0}, Landroid/support/d/a$a;->skipBytes(I)I

    move-result v1

    if-eq v1, v0, :cond_3

    .line 2862
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t jump to first Ifd: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2865
    :cond_3
    return-void
.end method

.method private a(Landroid/support/d/a$a;II)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v10, -0x1

    const/4 v9, 0x6

    const/4 v8, 0x1

    .line 2337
    sget-object v0, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {p1, v0}, Landroid/support/d/a$a;->a(Ljava/nio/ByteOrder;)V

    .line 2340
    int-to-long v2, p2

    invoke-virtual {p1, v2, v3}, Landroid/support/d/a$a;->a(J)V

    .line 2344
    invoke-virtual {p1}, Landroid/support/d/a$a;->readByte()B

    move-result v0

    if-eq v0, v10, :cond_0

    .line 2345
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid marker: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    and-int/lit16 v0, v0, 0xff

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2347
    :cond_0
    add-int/lit8 v2, p2, 0x1

    .line 2348
    invoke-virtual {p1}, Landroid/support/d/a$a;->readByte()B

    move-result v3

    const/16 v4, -0x28

    if-eq v3, v4, :cond_1

    .line 2349
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid marker: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    and-int/lit16 v0, v0, 0xff

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2351
    :cond_1
    add-int/lit8 v0, v2, 0x1

    .line 2353
    :goto_0
    invoke-virtual {p1}, Landroid/support/d/a$a;->readByte()B

    move-result v2

    .line 2354
    if-eq v2, v10, :cond_2

    .line 2355
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid marker:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    and-int/lit16 v2, v2, 0xff

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2357
    :cond_2
    add-int/lit8 v0, v0, 0x1

    .line 2358
    invoke-virtual {p1}, Landroid/support/d/a$a;->readByte()B

    move-result v3

    .line 2362
    add-int/lit8 v2, v0, 0x1

    .line 2366
    const/16 v0, -0x27

    if-eq v3, v0, :cond_3

    const/16 v0, -0x26

    if-ne v3, v0, :cond_4

    .line 2467
    :cond_3
    iget-object v0, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    invoke-virtual {p1, v0}, Landroid/support/d/a$a;->a(Ljava/nio/ByteOrder;)V

    .line 2468
    return-void

    .line 2369
    :cond_4
    invoke-virtual {p1}, Landroid/support/d/a$a;->readUnsignedShort()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    .line 2370
    add-int/lit8 v2, v2, 0x2

    .line 2375
    if-gez v0, :cond_5

    .line 2376
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Invalid length"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2378
    :cond_5
    sparse-switch v3, :sswitch_data_0

    .line 2458
    :cond_6
    :goto_1
    if-gez v0, :cond_c

    .line 2459
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Invalid length"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2383
    :sswitch_0
    if-lt v0, v9, :cond_6

    .line 2387
    new-array v3, v9, [B

    .line 2388
    invoke-virtual {p1, v3}, Landroid/support/d/a$a;->read([B)I

    move-result v4

    if-eq v4, v9, :cond_7

    .line 2389
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Invalid exif"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2391
    :cond_7
    add-int/lit8 v2, v2, 0x6

    .line 2392
    add-int/lit8 v0, v0, -0x6

    .line 2393
    sget-object v4, Landroid/support/d/a;->e:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2397
    if-gtz v0, :cond_8

    .line 2398
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Invalid exif"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2404
    :cond_8
    iput v2, p0, Landroid/support/d/a;->Q:I

    .line 2406
    new-array v3, v0, [B

    .line 2407
    invoke-virtual {p1, v3}, Landroid/support/d/a$a;->read([B)I

    move-result v4

    if-eq v4, v0, :cond_9

    .line 2408
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Invalid exif"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2410
    :cond_9
    add-int/2addr v0, v2

    .line 2413
    invoke-direct {p0, v3, p3}, Landroid/support/d/a;->a([BI)V

    move v2, v0

    move v0, v1

    .line 2414
    goto :goto_1

    .line 2418
    :sswitch_1
    new-array v3, v0, [B

    .line 2419
    invoke-virtual {p1, v3}, Landroid/support/d/a$a;->read([B)I

    move-result v4

    if-eq v4, v0, :cond_a

    .line 2420
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Invalid exif"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2423
    :cond_a
    const-string v0, "UserComment"

    invoke-virtual {p0, v0}, Landroid/support/d/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_e

    .line 2424
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v0, v0, v8

    const-string v4, "UserComment"

    new-instance v5, Ljava/lang/String;

    sget-object v6, Landroid/support/d/a;->F:Ljava/nio/charset/Charset;

    invoke-direct {v5, v3, v6}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-static {v5}, Landroid/support/d/a$b;->a(Ljava/lang/String;)Landroid/support/d/a$b;

    move-result-object v3

    invoke-virtual {v0, v4, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    goto :goto_1

    .line 2443
    :sswitch_2
    invoke-virtual {p1, v8}, Landroid/support/d/a$a;->skipBytes(I)I

    move-result v3

    if-eq v3, v8, :cond_b

    .line 2444
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Invalid SOFx"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2446
    :cond_b
    iget-object v3, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v3, v3, p3

    const-string v4, "ImageLength"

    .line 2447
    invoke-virtual {p1}, Landroid/support/d/a$a;->readUnsignedShort()I

    move-result v5

    int-to-long v6, v5

    iget-object v5, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 2446
    invoke-static {v6, v7, v5}, Landroid/support/d/a$b;->a(JLjava/nio/ByteOrder;)Landroid/support/d/a$b;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2448
    iget-object v3, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v3, v3, p3

    const-string v4, "ImageWidth"

    .line 2449
    invoke-virtual {p1}, Landroid/support/d/a$a;->readUnsignedShort()I

    move-result v5

    int-to-long v6, v5

    iget-object v5, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 2448
    invoke-static {v6, v7, v5}, Landroid/support/d/a$b;->a(JLjava/nio/ByteOrder;)Landroid/support/d/a$b;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2450
    add-int/lit8 v0, v0, -0x5

    .line 2451
    goto/16 :goto_1

    .line 2461
    :cond_c
    invoke-virtual {p1, v0}, Landroid/support/d/a$a;->skipBytes(I)I

    move-result v3

    if-eq v3, v0, :cond_d

    .line 2462
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Invalid JPEG segment"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2464
    :cond_d
    add-int/2addr v0, v2

    .line 2465
    goto/16 :goto_0

    :cond_e
    move v0, v1

    goto/16 :goto_1

    .line 2378
    :sswitch_data_0
    .sparse-switch
        -0x40 -> :sswitch_2
        -0x3f -> :sswitch_2
        -0x3e -> :sswitch_2
        -0x3d -> :sswitch_2
        -0x3b -> :sswitch_2
        -0x3a -> :sswitch_2
        -0x39 -> :sswitch_2
        -0x37 -> :sswitch_2
        -0x36 -> :sswitch_2
        -0x35 -> :sswitch_2
        -0x33 -> :sswitch_2
        -0x32 -> :sswitch_2
        -0x31 -> :sswitch_2
        -0x1f -> :sswitch_0
        -0x2 -> :sswitch_1
    .end sparse-switch
.end method

.method private a(Landroid/support/d/a$a;Ljava/util/HashMap;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3125
    const-string v0, "JPEGInterchangeFormat"

    .line 3126
    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/d/a$b;

    .line 3127
    const-string v1, "JPEGInterchangeFormatLength"

    .line 3128
    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/d/a$b;

    .line 3129
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 3131
    iget-object v2, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v2}, Landroid/support/d/a$b;->b(Ljava/nio/ByteOrder;)I

    move-result v0

    .line 3132
    iget-object v2, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v2}, Landroid/support/d/a$b;->b(Ljava/nio/ByteOrder;)I

    move-result v1

    .line 3135
    invoke-virtual {p1}, Landroid/support/d/a$a;->available()I

    move-result v2

    sub-int/2addr v2, v0

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 3136
    iget v2, p0, Landroid/support/d/a;->I:I

    const/4 v3, 0x4

    if-eq v2, v3, :cond_0

    iget v2, p0, Landroid/support/d/a;->I:I

    const/16 v3, 0x9

    if-eq v2, v3, :cond_0

    iget v2, p0, Landroid/support/d/a;->I:I

    const/16 v3, 0xa

    if-ne v2, v3, :cond_3

    .line 3138
    :cond_0
    iget v2, p0, Landroid/support/d/a;->Q:I

    add-int/2addr v0, v2

    .line 3147
    :cond_1
    :goto_0
    if-lez v0, :cond_2

    if-lez v1, :cond_2

    .line 3148
    const/4 v2, 0x1

    iput-boolean v2, p0, Landroid/support/d/a;->L:Z

    .line 3149
    iput v0, p0, Landroid/support/d/a;->M:I

    .line 3150
    iput v1, p0, Landroid/support/d/a;->N:I

    .line 3151
    iget-object v2, p0, Landroid/support/d/a;->G:Ljava/lang/String;

    if-nez v2, :cond_2

    iget-object v2, p0, Landroid/support/d/a;->H:Landroid/content/res/AssetManager$AssetInputStream;

    if-nez v2, :cond_2

    .line 3153
    new-array v1, v1, [B

    .line 3154
    int-to-long v2, v0

    invoke-virtual {p1, v2, v3}, Landroid/support/d/a$a;->a(J)V

    .line 3155
    invoke-virtual {p1, v1}, Landroid/support/d/a$a;->readFully([B)V

    .line 3156
    iput-object v1, p0, Landroid/support/d/a;->O:[B

    .line 3160
    :cond_2
    return-void

    .line 3139
    :cond_3
    iget v2, p0, Landroid/support/d/a;->I:I

    const/4 v3, 0x7

    if-ne v2, v3, :cond_1

    .line 3141
    iget v2, p0, Landroid/support/d/a;->R:I

    add-int/2addr v0, v2

    goto :goto_0
.end method

.method private static a(Ljava/io/Closeable;)V
    .locals 1

    .prologue
    .line 3991
    if-eqz p0, :cond_0

    .line 3993
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 3999
    :cond_0
    :goto_0
    return-void

    .line 3994
    :catch_0
    move-exception v0

    .line 3995
    throw v0

    .line 3996
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method private a(Ljava/io/InputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1752
    :goto_0
    :try_start_0
    sget-object v2, Landroid/support/d/a;->d:[[Landroid/support/d/a$c;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 1753
    iget-object v2, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    aput-object v3, v2, v1

    .line 1752
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1757
    :cond_0
    new-instance v2, Ljava/io/BufferedInputStream;

    const/16 v1, 0x1388

    invoke-direct {v2, p1, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 1758
    move-object v0, v2

    check-cast v0, Ljava/io/BufferedInputStream;

    move-object v1, v0

    invoke-direct {p0, v1}, Landroid/support/d/a;->a(Ljava/io/BufferedInputStream;)I

    move-result v1

    iput v1, p0, Landroid/support/d/a;->I:I

    .line 1761
    new-instance v1, Landroid/support/d/a$a;

    invoke-direct {v1, v2}, Landroid/support/d/a$a;-><init>(Ljava/io/InputStream;)V

    .line 1763
    iget v2, p0, Landroid/support/d/a;->I:I

    packed-switch v2, :pswitch_data_0

    .line 1796
    :goto_1
    invoke-direct {p0, v1}, Landroid/support/d/a;->f(Landroid/support/d/a$a;)V

    .line 1797
    const/4 v1, 0x1

    iput-boolean v1, p0, Landroid/support/d/a;->V:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1808
    invoke-direct {p0}, Landroid/support/d/a;->c()V

    .line 1814
    :goto_2
    return-void

    .line 1765
    :pswitch_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    :try_start_1
    invoke-direct {p0, v1, v2, v3}, Landroid/support/d/a;->a(Landroid/support/d/a$a;II)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1798
    :catch_0
    move-exception v1

    .line 1801
    const/4 v1, 0x0

    :try_start_2
    iput-boolean v1, p0, Landroid/support/d/a;->V:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1808
    invoke-direct {p0}, Landroid/support/d/a;->c()V

    goto :goto_2

    .line 1769
    :pswitch_1
    :try_start_3
    invoke-direct {p0, v1}, Landroid/support/d/a;->b(Landroid/support/d/a$a;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 1808
    :catchall_0
    move-exception v1

    invoke-direct {p0}, Landroid/support/d/a;->c()V

    throw v1

    .line 1773
    :pswitch_2
    :try_start_4
    invoke-direct {p0, v1}, Landroid/support/d/a;->c(Landroid/support/d/a$a;)V

    goto :goto_1

    .line 1777
    :pswitch_3
    invoke-direct {p0, v1}, Landroid/support/d/a;->d(Landroid/support/d/a$a;)V

    goto :goto_1

    .line 1788
    :pswitch_4
    invoke-direct {p0, v1}, Landroid/support/d/a;->a(Landroid/support/d/a$a;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 1763
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_4
        :pswitch_4
        :pswitch_2
        :pswitch_4
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private a([BI)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2784
    new-instance v0, Landroid/support/d/a$a;

    invoke-direct {v0, p1}, Landroid/support/d/a$a;-><init>([B)V

    .line 2788
    array-length v1, p1

    invoke-direct {p0, v0, v1}, Landroid/support/d/a;->a(Landroid/support/d/a$a;I)V

    .line 2791
    invoke-direct {p0, v0, p2}, Landroid/support/d/a;->b(Landroid/support/d/a$a;I)V

    .line 2792
    return-void
.end method

.method private a(Ljava/util/HashMap;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 3226
    const-string v0, "BitsPerSample"

    .line 3227
    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/d/a$b;

    .line 3228
    if-eqz v0, :cond_3

    .line 3229
    iget-object v1, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    invoke-static {v0, v1}, Landroid/support/d/a$b;->a(Landroid/support/d/a$b;Ljava/nio/ByteOrder;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    check-cast v0, [I

    .line 3231
    sget-object v1, Landroid/support/d/a;->l:[I

    invoke-static {v1, v0}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v1

    if-eqz v1, :cond_0

    move v0, v2

    .line 3256
    :goto_0
    return v0

    .line 3236
    :cond_0
    iget v1, p0, Landroid/support/d/a;->I:I

    const/4 v3, 0x3

    if-ne v1, v3, :cond_3

    .line 3237
    const-string v1, "PhotometricInterpretation"

    .line 3238
    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/d/a$b;

    .line 3239
    if-eqz v1, :cond_3

    .line 3240
    iget-object v3, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 3241
    invoke-virtual {v1, v3}, Landroid/support/d/a$b;->b(Ljava/nio/ByteOrder;)I

    move-result v1

    .line 3242
    if-ne v1, v2, :cond_1

    sget-object v3, Landroid/support/d/a;->n:[I

    .line 3243
    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    const/4 v3, 0x6

    if-ne v1, v3, :cond_3

    sget-object v1, Landroid/support/d/a;->l:[I

    .line 3245
    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v0, v2

    .line 3246
    goto :goto_0

    .line 3256
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a([B)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2257
    move v0, v1

    :goto_0
    sget-object v2, Landroid/support/d/a;->a:[B

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 2258
    aget-byte v2, p0, v0

    sget-object v3, Landroid/support/d/a;->a:[B

    aget-byte v3, v3, v0

    if-eq v2, v3, :cond_0

    .line 2262
    :goto_1
    return v1

    .line 2257
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2262
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private static a(Ljava/lang/Object;)[J
    .locals 4

    .prologue
    .line 4021
    instance-of v0, p0, [I

    if-eqz v0, :cond_1

    .line 4022
    check-cast p0, [I

    check-cast p0, [I

    .line 4023
    array-length v0, p0

    new-array v1, v0, [J

    .line 4024
    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 4025
    aget v2, p0, v0

    int-to-long v2, v2

    aput-wide v2, v1, v0

    .line 4024
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move-object p0, v1

    .line 4031
    :goto_1
    return-object p0

    .line 4028
    :cond_1
    instance-of v0, p0, [J

    if-eqz v0, :cond_2

    .line 4029
    check-cast p0, [J

    check-cast p0, [J

    goto :goto_1

    .line 4031
    :cond_2
    const/4 p0, 0x0

    goto :goto_1
.end method

.method private b(Ljava/lang/String;)Landroid/support/d/a$b;
    .locals 2

    .prologue
    .line 1370
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    sget-object v0, Landroid/support/d/a;->d:[[Landroid/support/d/a$c;

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 1371
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/d/a$b;

    .line 1372
    if-eqz v0, :cond_0

    .line 1376
    :goto_1
    return-object v0

    .line 1370
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1376
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b(Landroid/support/d/a$a;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v1, 0x0

    .line 2523
    const/16 v0, 0x54

    invoke-virtual {p1, v0}, Landroid/support/d/a$a;->skipBytes(I)I

    .line 2524
    new-array v0, v3, [B

    .line 2525
    new-array v2, v3, [B

    .line 2526
    invoke-virtual {p1, v0}, Landroid/support/d/a$a;->read([B)I

    .line 2528
    invoke-virtual {p1, v3}, Landroid/support/d/a$a;->skipBytes(I)I

    .line 2529
    invoke-virtual {p1, v2}, Landroid/support/d/a$a;->read([B)I

    .line 2530
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    .line 2531
    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    .line 2534
    const/4 v3, 0x5

    invoke-direct {p0, p1, v0, v3}, Landroid/support/d/a;->a(Landroid/support/d/a$a;II)V

    .line 2537
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Landroid/support/d/a$a;->a(J)V

    .line 2540
    sget-object v0, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {p1, v0}, Landroid/support/d/a$a;->a(Ljava/nio/ByteOrder;)V

    .line 2541
    invoke-virtual {p1}, Landroid/support/d/a$a;->readInt()I

    move-result v2

    move v0, v1

    .line 2548
    :goto_0
    if-ge v0, v2, :cond_0

    .line 2549
    invoke-virtual {p1}, Landroid/support/d/a$a;->readUnsignedShort()I

    move-result v3

    .line 2550
    invoke-virtual {p1}, Landroid/support/d/a$a;->readUnsignedShort()I

    move-result v4

    .line 2551
    sget-object v5, Landroid/support/d/a;->t:Landroid/support/d/a$c;

    iget v5, v5, Landroid/support/d/a$c;->a:I

    if-ne v3, v5, :cond_1

    .line 2552
    invoke-virtual {p1}, Landroid/support/d/a$a;->readShort()S

    move-result v0

    .line 2553
    invoke-virtual {p1}, Landroid/support/d/a$a;->readShort()S

    move-result v2

    .line 2554
    iget-object v3, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 2555
    invoke-static {v0, v3}, Landroid/support/d/a$b;->a(ILjava/nio/ByteOrder;)Landroid/support/d/a$b;

    move-result-object v0

    .line 2556
    iget-object v3, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 2557
    invoke-static {v2, v3}, Landroid/support/d/a$b;->a(ILjava/nio/ByteOrder;)Landroid/support/d/a$b;

    move-result-object v2

    .line 2558
    iget-object v3, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v3, v3, v1

    const-string v4, "ImageLength"

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2559
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v0, v0, v1

    const-string v1, "ImageWidth"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2567
    :cond_0
    return-void

    .line 2565
    :cond_1
    invoke-virtual {p1, v4}, Landroid/support/d/a$a;->skipBytes(I)I

    .line 2548
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private b(Landroid/support/d/a$a;I)V
    .locals 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2870
    invoke-static/range {p1 .. p1}, Landroid/support/d/a$a;->a(Landroid/support/d/a$a;)I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    invoke-static/range {p1 .. p1}, Landroid/support/d/a$a;->b(Landroid/support/d/a$a;)I

    move-result v3

    if-le v2, v3, :cond_1

    .line 3063
    :cond_0
    :goto_0
    return-void

    .line 2875
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/support/d/a$a;->readShort()S

    move-result v9

    .line 2879
    invoke-static/range {p1 .. p1}, Landroid/support/d/a$a;->a(Landroid/support/d/a$a;)I

    move-result v2

    mul-int/lit8 v3, v9, 0xc

    add-int/2addr v2, v3

    invoke-static/range {p1 .. p1}, Landroid/support/d/a$a;->b(Landroid/support/d/a$a;)I

    move-result v3

    if-gt v2, v3, :cond_0

    .line 2885
    const/4 v2, 0x0

    move v8, v2

    :goto_1
    if-ge v8, v9, :cond_17

    .line 2886
    invoke-virtual/range {p1 .. p1}, Landroid/support/d/a$a;->readUnsignedShort()I

    move-result v10

    .line 2887
    invoke-virtual/range {p1 .. p1}, Landroid/support/d/a$a;->readUnsignedShort()I

    move-result v3

    .line 2888
    invoke-virtual/range {p1 .. p1}, Landroid/support/d/a$a;->readInt()I

    move-result v11

    .line 2890
    invoke-virtual/range {p1 .. p1}, Landroid/support/d/a$a;->a()I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    int-to-long v12, v2

    .line 2893
    sget-object v2, Landroid/support/d/a;->B:[Ljava/util/HashMap;

    aget-object v2, v2, p2

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/d/a$c;

    .line 2901
    const-wide/16 v6, 0x0

    .line 2902
    const/4 v4, 0x0

    .line 2903
    if-nez v2, :cond_3

    .line 2904
    const-string v5, "ExifInterface"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Skip the tag entry since tag number is not defined: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v5, v14}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move/from16 v18, v4

    move-wide v4, v6

    move v6, v3

    move/from16 v3, v18

    .line 2922
    :goto_2
    if-nez v3, :cond_a

    .line 2923
    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Landroid/support/d/a$a;->a(J)V

    .line 2885
    :cond_2
    :goto_3
    add-int/lit8 v2, v8, 0x1

    int-to-short v2, v2

    move v8, v2

    goto :goto_1

    .line 2905
    :cond_3
    if-lez v3, :cond_4

    sget-object v5, Landroid/support/d/a;->c:[I

    array-length v5, v5

    if-lt v3, v5, :cond_5

    .line 2906
    :cond_4
    const-string v5, "ExifInterface"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Skip the tag entry since data format is invalid: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v5, v14}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move/from16 v18, v4

    move-wide v4, v6

    move v6, v3

    move/from16 v3, v18

    goto :goto_2

    .line 2907
    :cond_5
    invoke-static {v2, v3}, Landroid/support/d/a$c;->a(Landroid/support/d/a$c;I)Z

    move-result v5

    if-nez v5, :cond_6

    .line 2908
    const-string v5, "ExifInterface"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Skip the tag entry since data format ("

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    sget-object v15, Landroid/support/d/a;->b:[Ljava/lang/String;

    aget-object v15, v15, v3

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ") is unexpected for tag: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v2, Landroid/support/d/a$c;->b:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v5, v14}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move/from16 v18, v4

    move-wide v4, v6

    move v6, v3

    move/from16 v3, v18

    goto :goto_2

    .line 2911
    :cond_6
    const/4 v5, 0x7

    if-ne v3, v5, :cond_7

    .line 2912
    iget v3, v2, Landroid/support/d/a$c;->c:I

    .line 2914
    :cond_7
    int-to-long v6, v11

    sget-object v5, Landroid/support/d/a;->c:[I

    aget v5, v5, v3

    int-to-long v14, v5

    mul-long/2addr v6, v14

    .line 2915
    const-wide/16 v14, 0x0

    cmp-long v5, v6, v14

    if-ltz v5, :cond_8

    const-wide/32 v14, 0x7fffffff

    cmp-long v5, v6, v14

    if-lez v5, :cond_9

    .line 2916
    :cond_8
    const-string v5, "ExifInterface"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Skip the tag entry since the number of components is invalid: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v5, v14}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move/from16 v18, v4

    move-wide v4, v6

    move v6, v3

    move/from16 v3, v18

    goto/16 :goto_2

    .line 2919
    :cond_9
    const/4 v4, 0x1

    move/from16 v18, v4

    move-wide v4, v6

    move v6, v3

    move/from16 v3, v18

    goto/16 :goto_2

    .line 2929
    :cond_a
    const-wide/16 v14, 0x4

    cmp-long v3, v4, v14

    if-lez v3, :cond_c

    .line 2930
    invoke-virtual/range {p1 .. p1}, Landroid/support/d/a$a;->readInt()I

    move-result v3

    .line 2934
    move-object/from16 v0, p0

    iget v7, v0, Landroid/support/d/a;->I:I

    const/4 v14, 0x7

    if-ne v7, v14, :cond_e

    .line 2935
    const-string v7, "MakerNote"

    iget-object v14, v2, Landroid/support/d/a$c;->b:Ljava/lang/String;

    invoke-virtual {v7, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 2937
    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/d/a;->R:I

    .line 2962
    :cond_b
    :goto_4
    int-to-long v14, v3

    add-long/2addr v14, v4

    invoke-static/range {p1 .. p1}, Landroid/support/d/a$a;->b(Landroid/support/d/a$a;)I

    move-result v7

    int-to-long v0, v7

    move-wide/from16 v16, v0

    cmp-long v7, v14, v16

    if-gtz v7, :cond_f

    .line 2963
    int-to-long v14, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Landroid/support/d/a$a;->a(J)V

    .line 2973
    :cond_c
    sget-object v3, Landroid/support/d/a;->E:Ljava/util/HashMap;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 2978
    if-eqz v3, :cond_11

    .line 2979
    const-wide/16 v4, -0x1

    .line 2981
    packed-switch v6, :pswitch_data_0

    .line 3007
    :goto_5
    :pswitch_0
    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-lez v2, :cond_10

    invoke-static/range {p1 .. p1}, Landroid/support/d/a$a;->b(Landroid/support/d/a$a;)I

    move-result v2

    int-to-long v6, v2

    cmp-long v2, v4, v6

    if-gez v2, :cond_10

    .line 3008
    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/support/d/a$a;->a(J)V

    .line 3009
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Landroid/support/d/a;->b(Landroid/support/d/a$a;I)V

    .line 3014
    :goto_6
    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Landroid/support/d/a$a;->a(J)V

    goto/16 :goto_3

    .line 2938
    :cond_d
    const/4 v7, 0x6

    move/from16 v0, p2

    if-ne v0, v7, :cond_b

    const-string v7, "ThumbnailImage"

    iget-object v14, v2, Landroid/support/d/a$c;->b:Ljava/lang/String;

    .line 2939
    invoke-virtual {v7, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 2941
    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/d/a;->S:I

    .line 2942
    move-object/from16 v0, p0

    iput v11, v0, Landroid/support/d/a;->T:I

    .line 2944
    const/4 v7, 0x6

    move-object/from16 v0, p0

    iget-object v14, v0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 2945
    invoke-static {v7, v14}, Landroid/support/d/a$b;->a(ILjava/nio/ByteOrder;)Landroid/support/d/a$b;

    move-result-object v7

    .line 2946
    move-object/from16 v0, p0

    iget v14, v0, Landroid/support/d/a;->S:I

    int-to-long v14, v14

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    move-object/from16 v16, v0

    .line 2947
    invoke-static/range {v14 .. v16}, Landroid/support/d/a$b;->a(JLjava/nio/ByteOrder;)Landroid/support/d/a$b;

    move-result-object v14

    .line 2948
    move-object/from16 v0, p0

    iget v15, v0, Landroid/support/d/a;->T:I

    int-to-long v0, v15

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v15, v0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 2949
    move-wide/from16 v0, v16

    invoke-static {v0, v1, v15}, Landroid/support/d/a$b;->a(JLjava/nio/ByteOrder;)Landroid/support/d/a$b;

    move-result-object v15

    .line 2951
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    move-object/from16 v16, v0

    const/16 v17, 0x4

    aget-object v16, v16, v17

    const-string v17, "Compression"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2952
    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    const/16 v16, 0x4

    aget-object v7, v7, v16

    const-string v16, "JPEGInterchangeFormat"

    move-object/from16 v0, v16

    invoke-virtual {v7, v0, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2954
    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    const/4 v14, 0x4

    aget-object v7, v7, v14

    const-string v14, "JPEGInterchangeFormatLength"

    invoke-virtual {v7, v14, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_4

    .line 2957
    :cond_e
    move-object/from16 v0, p0

    iget v7, v0, Landroid/support/d/a;->I:I

    const/16 v14, 0xa

    if-ne v7, v14, :cond_b

    .line 2958
    const-string v7, "JpgFromRaw"

    iget-object v14, v2, Landroid/support/d/a$c;->b:Ljava/lang/String;

    invoke-virtual {v7, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 2959
    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/d/a;->U:I

    goto/16 :goto_4

    .line 2966
    :cond_f
    const-string v2, "ExifInterface"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Skip the tag entry since data offset is invalid: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2967
    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Landroid/support/d/a$a;->a(J)V

    goto/16 :goto_3

    .line 2983
    :pswitch_1
    invoke-virtual/range {p1 .. p1}, Landroid/support/d/a$a;->readUnsignedShort()I

    move-result v2

    int-to-long v4, v2

    .line 2984
    goto/16 :goto_5

    .line 2987
    :pswitch_2
    invoke-virtual/range {p1 .. p1}, Landroid/support/d/a$a;->readShort()S

    move-result v2

    int-to-long v4, v2

    .line 2988
    goto/16 :goto_5

    .line 2991
    :pswitch_3
    invoke-virtual/range {p1 .. p1}, Landroid/support/d/a$a;->b()J

    move-result-wide v4

    goto/16 :goto_5

    .line 2996
    :pswitch_4
    invoke-virtual/range {p1 .. p1}, Landroid/support/d/a$a;->readInt()I

    move-result v2

    int-to-long v4, v2

    .line 2997
    goto/16 :goto_5

    .line 3011
    :cond_10
    const-string v2, "ExifInterface"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Skip jump into the IFD since its offset is invalid: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    .line 3018
    :cond_11
    long-to-int v3, v4

    new-array v3, v3, [B

    .line 3019
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/support/d/a$a;->readFully([B)V

    .line 3020
    new-instance v4, Landroid/support/d/a$b;

    const/4 v5, 0x0

    invoke-direct {v4, v6, v11, v3, v5}, Landroid/support/d/a$b;-><init>(II[BLandroid/support/d/a$1;)V

    .line 3021
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v3, v3, p2

    iget-object v5, v2, Landroid/support/d/a$c;->b:Ljava/lang/String;

    invoke-virtual {v3, v5, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3026
    const-string v3, "DNGVersion"

    iget-object v5, v2, Landroid/support/d/a$c;->b:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 3027
    const/4 v3, 0x3

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/d/a;->I:I

    .line 3033
    :cond_12
    const-string v3, "Make"

    iget-object v5, v2, Landroid/support/d/a$c;->b:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_13

    const-string v3, "Model"

    iget-object v5, v2, Landroid/support/d/a$c;->b:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_14

    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 3034
    invoke-virtual {v4, v3}, Landroid/support/d/a$b;->c(Ljava/nio/ByteOrder;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "PENTAX"

    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_15

    :cond_14
    const-string v3, "Compression"

    iget-object v2, v2, Landroid/support/d/a$c;->b:Ljava/lang/String;

    .line 3035
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 3036
    invoke-virtual {v4, v2}, Landroid/support/d/a$b;->b(Ljava/nio/ByteOrder;)I

    move-result v2

    const v3, 0xffff

    if-ne v2, v3, :cond_16

    .line 3037
    :cond_15
    const/16 v2, 0x8

    move-object/from16 v0, p0

    iput v2, v0, Landroid/support/d/a;->I:I

    .line 3041
    :cond_16
    invoke-virtual/range {p1 .. p1}, Landroid/support/d/a$a;->a()I

    move-result v2

    int-to-long v2, v2

    cmp-long v2, v2, v12

    if-eqz v2, :cond_2

    .line 3042
    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Landroid/support/d/a$a;->a(J)V

    goto/16 :goto_3

    .line 3046
    :cond_17
    invoke-virtual/range {p1 .. p1}, Landroid/support/d/a$a;->a()I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    invoke-static/range {p1 .. p1}, Landroid/support/d/a$a;->b(Landroid/support/d/a$a;)I

    move-result v3

    if-gt v2, v3, :cond_0

    .line 3047
    invoke-virtual/range {p1 .. p1}, Landroid/support/d/a$a;->readInt()I

    move-result v2

    .line 3053
    const/16 v3, 0x8

    if-le v2, v3, :cond_0

    invoke-static/range {p1 .. p1}, Landroid/support/d/a$a;->b(Landroid/support/d/a$a;)I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 3054
    int-to-long v2, v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/support/d/a$a;->a(J)V

    .line 3055
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_18

    .line 3057
    const/4 v2, 0x4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Landroid/support/d/a;->b(Landroid/support/d/a$a;I)V

    goto/16 :goto_0

    .line 3058
    :cond_18
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3059
    const/4 v2, 0x5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Landroid/support/d/a;->b(Landroid/support/d/a$a;I)V

    goto/16 :goto_0

    .line 2981
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private b(Landroid/support/d/a$a;Ljava/util/HashMap;)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 3165
    const-string v0, "StripOffsets"

    .line 3166
    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/d/a$b;

    .line 3167
    const-string v1, "StripByteCounts"

    .line 3168
    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/d/a$b;

    .line 3170
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 3171
    iget-object v3, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 3172
    invoke-static {v0, v3}, Landroid/support/d/a$b;->a(Landroid/support/d/a$b;Ljava/nio/ByteOrder;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Landroid/support/d/a;->a(Ljava/lang/Object;)[J

    move-result-object v6

    .line 3173
    iget-object v0, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 3174
    invoke-static {v1, v0}, Landroid/support/d/a$b;->a(Landroid/support/d/a$b;Ljava/nio/ByteOrder;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Landroid/support/d/a;->a(Ljava/lang/Object;)[J

    move-result-object v7

    .line 3176
    if-nez v6, :cond_1

    .line 3177
    const-string v0, "ExifInterface"

    const-string v1, "stripOffsets should not be null."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3222
    :cond_0
    :goto_0
    return-void

    .line 3180
    :cond_1
    if-nez v7, :cond_2

    .line 3181
    const-string v0, "ExifInterface"

    const-string v1, "stripByteCounts should not be null."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3185
    :cond_2
    const-wide/16 v0, 0x0

    .line 3186
    array-length v3, v7

    move-wide v4, v0

    move v0, v2

    :goto_1
    if-ge v0, v3, :cond_3

    aget-wide v8, v7, v0

    .line 3187
    add-long/2addr v4, v8

    .line 3186
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3191
    :cond_3
    long-to-int v0, v4

    new-array v4, v0, [B

    move v0, v2

    move v1, v2

    move v3, v2

    .line 3195
    :goto_2
    array-length v5, v6

    if-ge v0, v5, :cond_5

    .line 3196
    aget-wide v8, v6, v0

    long-to-int v5, v8

    .line 3197
    aget-wide v8, v7, v0

    long-to-int v8, v8

    .line 3200
    sub-int/2addr v5, v3

    .line 3201
    if-gez v5, :cond_4

    .line 3202
    const-string v9, "ExifInterface"

    const-string v10, "Invalid strip offset value"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3204
    :cond_4
    int-to-long v10, v5

    invoke-virtual {p1, v10, v11}, Landroid/support/d/a$a;->a(J)V

    .line 3205
    add-int/2addr v3, v5

    .line 3208
    new-array v5, v8, [B

    .line 3209
    invoke-virtual {p1, v5}, Landroid/support/d/a$a;->read([B)I

    .line 3210
    add-int/2addr v3, v8

    .line 3213
    array-length v8, v5

    invoke-static {v5, v2, v4, v1, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3215
    array-length v5, v5

    add-int/2addr v1, v5

    .line 3195
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 3218
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/d/a;->L:Z

    .line 3219
    iput-object v4, p0, Landroid/support/d/a;->O:[B

    .line 3220
    array-length v0, v4

    iput v0, p0, Landroid/support/d/a;->N:I

    goto :goto_0
.end method

.method private b(Ljava/io/InputStream;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x5

    const/4 v4, 0x4

    .line 3278
    invoke-direct {p0, v6, v5}, Landroid/support/d/a;->a(II)V

    .line 3279
    invoke-direct {p0, v6, v4}, Landroid/support/d/a;->a(II)V

    .line 3280
    invoke-direct {p0, v5, v4}, Landroid/support/d/a;->a(II)V

    .line 3285
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v0, v0, v2

    const-string v1, "PixelXDimension"

    .line 3286
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/d/a$b;

    .line 3287
    iget-object v1, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v1, v1, v2

    const-string v2, "PixelYDimension"

    .line 3288
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/d/a$b;

    .line 3289
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 3290
    iget-object v2, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v2, v2, v6

    const-string v3, "ImageWidth"

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3291
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v0, v0, v6

    const-string v2, "ImageLength"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3296
    :cond_0
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v0, v0, v4

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3297
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v0, v0, v5

    invoke-direct {p0, v0}, Landroid/support/d/a;->b(Ljava/util/HashMap;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3298
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    iget-object v1, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v1, v1, v5

    aput-object v1, v0, v4

    .line 3299
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    aput-object v1, v0, v5

    .line 3304
    :cond_1
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v0, v0, v4

    invoke-direct {p0, v0}, Landroid/support/d/a;->b(Ljava/util/HashMap;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 3305
    const-string v0, "ExifInterface"

    const-string v1, "No image meets the size requirements of a thumbnail image."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3307
    :cond_2
    return-void
.end method

.method private b(Ljava/util/HashMap;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v3, 0x200

    .line 3262
    const-string v0, "ImageLength"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/d/a$b;

    .line 3263
    const-string v1, "ImageWidth"

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/d/a$b;

    .line 3265
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 3266
    iget-object v2, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v2}, Landroid/support/d/a$b;->b(Ljava/nio/ByteOrder;)I

    move-result v0

    .line 3267
    iget-object v2, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v2}, Landroid/support/d/a$b;->b(Ljava/nio/ByteOrder;)I

    move-result v1

    .line 3268
    if-gt v0, v3, :cond_0

    if-gt v1, v3, :cond_0

    .line 3269
    const/4 v0, 0x1

    .line 3272
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b([B)Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2272
    const-string v0, "FUJIFILMCCD-RAW"

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2

    move v0, v1

    .line 2273
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_1

    .line 2274
    aget-byte v3, p1, v0

    aget-byte v4, v2, v0

    if-eq v3, v4, :cond_0

    .line 2278
    :goto_1
    return v1

    .line 2273
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2278
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method static synthetic b()[B
    .locals 1

    .prologue
    .line 67
    sget-object v0, Landroid/support/d/a;->k:[B

    return-object v0
.end method

.method private c()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 2796
    const-string v0, "DateTimeOriginal"

    invoke-virtual {p0, v0}, Landroid/support/d/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2797
    if-eqz v0, :cond_0

    const-string v1, "DateTime"

    invoke-virtual {p0, v1}, Landroid/support/d/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 2798
    iget-object v1, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v1, v1, v3

    const-string v2, "DateTime"

    .line 2799
    invoke-static {v0}, Landroid/support/d/a$b;->a(Ljava/lang/String;)Landroid/support/d/a$b;

    move-result-object v0

    .line 2798
    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2803
    :cond_0
    const-string v0, "ImageWidth"

    invoke-virtual {p0, v0}, Landroid/support/d/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2804
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v0, v0, v3

    const-string v1, "ImageWidth"

    iget-object v2, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 2805
    invoke-static {v4, v5, v2}, Landroid/support/d/a$b;->a(JLjava/nio/ByteOrder;)Landroid/support/d/a$b;

    move-result-object v2

    .line 2804
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2807
    :cond_1
    const-string v0, "ImageLength"

    invoke-virtual {p0, v0}, Landroid/support/d/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2808
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v0, v0, v3

    const-string v1, "ImageLength"

    iget-object v2, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 2809
    invoke-static {v4, v5, v2}, Landroid/support/d/a$b;->a(JLjava/nio/ByteOrder;)Landroid/support/d/a$b;

    move-result-object v2

    .line 2808
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2811
    :cond_2
    const-string v0, "Orientation"

    invoke-virtual {p0, v0}, Landroid/support/d/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    .line 2812
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v0, v0, v3

    const-string v1, "Orientation"

    iget-object v2, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 2813
    invoke-static {v4, v5, v2}, Landroid/support/d/a$b;->a(JLjava/nio/ByteOrder;)Landroid/support/d/a$b;

    move-result-object v2

    .line 2812
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2815
    :cond_3
    const-string v0, "LightSource"

    invoke-virtual {p0, v0}, Landroid/support/d/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    .line 2816
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    const-string v1, "LightSource"

    iget-object v2, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 2817
    invoke-static {v4, v5, v2}, Landroid/support/d/a$b;->a(JLjava/nio/ByteOrder;)Landroid/support/d/a$b;

    move-result-object v2

    .line 2816
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2819
    :cond_4
    return-void
.end method

.method private c(Landroid/support/d/a$a;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2582
    invoke-direct {p0, p1}, Landroid/support/d/a;->a(Landroid/support/d/a$a;)V

    .line 2587
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v0, v0, v5

    const-string v1, "MakerNote"

    .line 2588
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/d/a$b;

    .line 2589
    if-eqz v0, :cond_3

    .line 2591
    new-instance v1, Landroid/support/d/a$a;

    iget-object v0, v0, Landroid/support/d/a$b;->c:[B

    invoke-direct {v1, v0}, Landroid/support/d/a$a;-><init>([B)V

    .line 2593
    iget-object v0, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v0}, Landroid/support/d/a$a;->a(Ljava/nio/ByteOrder;)V

    .line 2597
    sget-object v0, Landroid/support/d/a;->h:[B

    array-length v0, v0

    new-array v0, v0, [B

    .line 2598
    invoke-virtual {v1, v0}, Landroid/support/d/a$a;->readFully([B)V

    .line 2599
    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/support/d/a$a;->a(J)V

    .line 2600
    sget-object v2, Landroid/support/d/a;->i:[B

    array-length v2, v2

    new-array v2, v2, [B

    .line 2601
    invoke-virtual {v1, v2}, Landroid/support/d/a$a;->readFully([B)V

    .line 2603
    sget-object v3, Landroid/support/d/a;->h:[B

    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2604
    const-wide/16 v2, 0x8

    invoke-virtual {v1, v2, v3}, Landroid/support/d/a$a;->a(J)V

    .line 2610
    :cond_0
    :goto_0
    const/4 v0, 0x6

    invoke-direct {p0, v1, v0}, Landroid/support/d/a;->b(Landroid/support/d/a$a;I)V

    .line 2613
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    const/4 v1, 0x7

    aget-object v0, v0, v1

    const-string v1, "PreviewImageStart"

    .line 2614
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/d/a$b;

    .line 2615
    iget-object v1, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    const-string v2, "PreviewImageLength"

    .line 2616
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/d/a$b;

    .line 2618
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 2619
    iget-object v2, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v2, v2, v8

    const-string v3, "JPEGInterchangeFormat"

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2621
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v0, v0, v8

    const-string v2, "JPEGInterchangeFormatLength"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2628
    :cond_1
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    const/16 v1, 0x8

    aget-object v0, v0, v1

    const-string v1, "AspectFrame"

    .line 2629
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/d/a$b;

    .line 2630
    if-eqz v0, :cond_3

    .line 2631
    iget-object v1, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    invoke-static {v0, v1}, Landroid/support/d/a$b;->a(Landroid/support/d/a$b;Ljava/nio/ByteOrder;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    check-cast v0, [I

    .line 2632
    if-eqz v0, :cond_2

    array-length v1, v0

    const/4 v2, 0x4

    if-eq v1, v2, :cond_5

    .line 2633
    :cond_2
    const-string v1, "ExifInterface"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid aspect frame values. frame="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2634
    invoke-static {v0}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2633
    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2657
    :cond_3
    :goto_1
    return-void

    .line 2605
    :cond_4
    sget-object v0, Landroid/support/d/a;->i:[B

    invoke-static {v2, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2606
    const-wide/16 v2, 0xc

    invoke-virtual {v1, v2, v3}, Landroid/support/d/a$a;->a(J)V

    goto :goto_0

    .line 2637
    :cond_5
    aget v1, v0, v6

    aget v2, v0, v4

    if-le v1, v2, :cond_3

    aget v1, v0, v7

    aget v2, v0, v5

    if-le v1, v2, :cond_3

    .line 2639
    aget v1, v0, v6

    aget v2, v0, v4

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    .line 2640
    aget v2, v0, v7

    aget v0, v0, v5

    sub-int v0, v2, v0

    add-int/lit8 v0, v0, 0x1

    .line 2642
    if-ge v1, v0, :cond_6

    .line 2643
    add-int/2addr v1, v0

    .line 2644
    sub-int v0, v1, v0

    .line 2645
    sub-int/2addr v1, v0

    .line 2647
    :cond_6
    iget-object v2, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 2648
    invoke-static {v1, v2}, Landroid/support/d/a$b;->a(ILjava/nio/ByteOrder;)Landroid/support/d/a$b;

    move-result-object v1

    .line 2649
    iget-object v2, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 2650
    invoke-static {v0, v2}, Landroid/support/d/a$b;->a(ILjava/nio/ByteOrder;)Landroid/support/d/a$b;

    move-result-object v0

    .line 2652
    iget-object v2, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v2, v2, v4

    const-string v3, "ImageWidth"

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2653
    iget-object v1, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v1, v1, v4

    const-string v2, "ImageLength"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method private c(Landroid/support/d/a$a;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3074
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v0, v0, p2

    const-string v1, "ImageLength"

    .line 3075
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/d/a$b;

    .line 3076
    iget-object v1, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v1, v1, p2

    const-string v2, "ImageWidth"

    .line 3077
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/d/a$b;

    .line 3079
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 3081
    :cond_0
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v0, v0, p2

    const-string v1, "JPEGInterchangeFormat"

    .line 3082
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/d/a$b;

    .line 3083
    if-eqz v0, :cond_1

    .line 3084
    iget-object v1, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 3085
    invoke-virtual {v0, v1}, Landroid/support/d/a$b;->b(Ljava/nio/ByteOrder;)I

    move-result v0

    .line 3088
    invoke-direct {p0, p1, v0, p2}, Landroid/support/d/a;->a(Landroid/support/d/a$a;II)V

    .line 3091
    :cond_1
    return-void
.end method

.method private c([B)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2290
    new-instance v0, Landroid/support/d/a$a;

    invoke-direct {v0, p1}, Landroid/support/d/a$a;-><init>([B)V

    .line 2293
    invoke-direct {p0, v0}, Landroid/support/d/a;->e(Landroid/support/d/a$a;)Ljava/nio/ByteOrder;

    move-result-object v1

    iput-object v1, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 2295
    iget-object v1, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Landroid/support/d/a$a;->a(Ljava/nio/ByteOrder;)V

    .line 2297
    invoke-virtual {v0}, Landroid/support/d/a$a;->readShort()S

    move-result v1

    .line 2298
    invoke-virtual {v0}, Landroid/support/d/a$a;->close()V

    .line 2299
    const/16 v0, 0x4f52

    if-eq v1, v0, :cond_0

    const/16 v0, 0x5352

    if-ne v1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(Landroid/support/d/a$a;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2664
    invoke-direct {p0, p1}, Landroid/support/d/a;->a(Landroid/support/d/a$a;)V

    .line 2667
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v0, v0, v2

    const-string v1, "JpgFromRaw"

    .line 2668
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/d/a$b;

    .line 2669
    if-eqz v0, :cond_0

    .line 2670
    iget v0, p0, Landroid/support/d/a;->U:I

    const/4 v1, 0x5

    invoke-direct {p0, p1, v0, v1}, Landroid/support/d/a;->a(Landroid/support/d/a$a;II)V

    .line 2674
    :cond_0
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v0, v0, v2

    const-string v1, "ISO"

    .line 2675
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/d/a$b;

    .line 2676
    iget-object v1, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v1, v1, v3

    const-string v2, "ISOSpeedRatings"

    .line 2677
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/d/a$b;

    .line 2678
    if-eqz v0, :cond_1

    if-nez v1, :cond_1

    .line 2680
    iget-object v1, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v1, v1, v3

    const-string v2, "ISOSpeedRatings"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2682
    :cond_1
    return-void
.end method

.method private d(Landroid/support/d/a$a;I)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 3322
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v0, v0, p2

    const-string v1, "DefaultCropSize"

    .line 3323
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/d/a$b;

    .line 3325
    iget-object v1, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v1, v1, p2

    const-string v2, "SensorTopBorder"

    .line 3326
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/d/a$b;

    .line 3327
    iget-object v2, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v2, v2, p2

    const-string v3, "SensorLeftBorder"

    .line 3328
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/d/a$b;

    .line 3329
    iget-object v3, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v3, v3, p2

    const-string v4, "SensorBottomBorder"

    .line 3330
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/d/a$b;

    .line 3331
    iget-object v4, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v4, v4, p2

    const-string v5, "SensorRightBorder"

    .line 3332
    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/support/d/a$b;

    .line 3334
    if-eqz v0, :cond_6

    .line 3337
    iget v1, v0, Landroid/support/d/a$b;->a:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_3

    .line 3338
    iget-object v1, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 3339
    invoke-static {v0, v1}, Landroid/support/d/a$b;->a(Landroid/support/d/a$b;Ljava/nio/ByteOrder;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/support/d/a$d;

    check-cast v0, [Landroid/support/d/a$d;

    .line 3340
    if-eqz v0, :cond_0

    array-length v1, v0

    if-eq v1, v8, :cond_2

    .line 3341
    :cond_0
    const-string v1, "ExifInterface"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid crop size values. cropSize="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 3342
    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3341
    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3384
    :cond_1
    :goto_0
    return-void

    .line 3345
    :cond_2
    aget-object v1, v0, v6

    iget-object v2, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 3346
    invoke-static {v1, v2}, Landroid/support/d/a$b;->a(Landroid/support/d/a$d;Ljava/nio/ByteOrder;)Landroid/support/d/a$b;

    move-result-object v1

    .line 3347
    aget-object v0, v0, v7

    iget-object v2, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 3348
    invoke-static {v0, v2}, Landroid/support/d/a$b;->a(Landroid/support/d/a$d;Ljava/nio/ByteOrder;)Landroid/support/d/a$b;

    move-result-object v0

    .line 3362
    :goto_1
    iget-object v2, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v2, v2, p2

    const-string v3, "ImageWidth"

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3363
    iget-object v1, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v1, v1, p2

    const-string v2, "ImageLength"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 3350
    :cond_3
    iget-object v1, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 3351
    invoke-static {v0, v1}, Landroid/support/d/a$b;->a(Landroid/support/d/a$b;Ljava/nio/ByteOrder;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    check-cast v0, [I

    .line 3352
    if-eqz v0, :cond_4

    array-length v1, v0

    if-eq v1, v8, :cond_5

    .line 3353
    :cond_4
    const-string v1, "ExifInterface"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid crop size values. cropSize="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 3354
    invoke-static {v0}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3353
    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3357
    :cond_5
    aget v1, v0, v6

    iget-object v2, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 3358
    invoke-static {v1, v2}, Landroid/support/d/a$b;->a(ILjava/nio/ByteOrder;)Landroid/support/d/a$b;

    move-result-object v1

    .line 3359
    aget v0, v0, v7

    iget-object v2, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 3360
    invoke-static {v0, v2}, Landroid/support/d/a$b;->a(ILjava/nio/ByteOrder;)Landroid/support/d/a$b;

    move-result-object v0

    goto :goto_1

    .line 3364
    :cond_6
    if-eqz v1, :cond_7

    if-eqz v2, :cond_7

    if-eqz v3, :cond_7

    if-eqz v4, :cond_7

    .line 3367
    iget-object v0, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v0}, Landroid/support/d/a$b;->b(Ljava/nio/ByteOrder;)I

    move-result v0

    .line 3368
    iget-object v1, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    invoke-virtual {v3, v1}, Landroid/support/d/a$b;->b(Ljava/nio/ByteOrder;)I

    move-result v1

    .line 3369
    iget-object v3, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    invoke-virtual {v4, v3}, Landroid/support/d/a$b;->b(Ljava/nio/ByteOrder;)I

    move-result v3

    .line 3370
    iget-object v4, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v4}, Landroid/support/d/a$b;->b(Ljava/nio/ByteOrder;)I

    move-result v2

    .line 3371
    if-le v1, v0, :cond_1

    if-le v3, v2, :cond_1

    .line 3372
    sub-int v0, v1, v0

    .line 3373
    sub-int v1, v3, v2

    .line 3374
    iget-object v2, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 3375
    invoke-static {v0, v2}, Landroid/support/d/a$b;->a(ILjava/nio/ByteOrder;)Landroid/support/d/a$b;

    move-result-object v0

    .line 3376
    iget-object v2, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 3377
    invoke-static {v1, v2}, Landroid/support/d/a$b;->a(ILjava/nio/ByteOrder;)Landroid/support/d/a$b;

    move-result-object v1

    .line 3378
    iget-object v2, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v2, v2, p2

    const-string v3, "ImageLength"

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3379
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    aget-object v0, v0, p2

    const-string v2, "ImageWidth"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 3382
    :cond_7
    invoke-direct {p0, p1, p2}, Landroid/support/d/a;->c(Landroid/support/d/a$a;I)V

    goto/16 :goto_0
.end method

.method private d([B)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2307
    new-instance v0, Landroid/support/d/a$a;

    invoke-direct {v0, p1}, Landroid/support/d/a$a;-><init>([B)V

    .line 2310
    invoke-direct {p0, v0}, Landroid/support/d/a;->e(Landroid/support/d/a$a;)Ljava/nio/ByteOrder;

    move-result-object v1

    iput-object v1, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    .line 2312
    iget-object v1, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Landroid/support/d/a$a;->a(Ljava/nio/ByteOrder;)V

    .line 2314
    invoke-virtual {v0}, Landroid/support/d/a$a;->readShort()S

    move-result v1

    .line 2315
    invoke-virtual {v0}, Landroid/support/d/a$a;->close()V

    .line 2316
    const/16 v0, 0x55

    if-ne v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(Landroid/support/d/a$a;)Ljava/nio/ByteOrder;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2824
    invoke-virtual {p1}, Landroid/support/d/a$a;->readShort()S

    move-result v0

    .line 2825
    sparse-switch v0, :sswitch_data_0

    .line 2837
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid byte order: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2830
    :sswitch_0
    sget-object v0, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    .line 2835
    :goto_0
    return-object v0

    :sswitch_1
    sget-object v0, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    goto :goto_0

    .line 2825
    :sswitch_data_0
    .sparse-switch
        0x4949 -> :sswitch_0
        0x4d4d -> :sswitch_1
    .end sparse-switch
.end method

.method private f(Landroid/support/d/a$a;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3095
    iget-object v0, p0, Landroid/support/d/a;->J:[Ljava/util/HashMap;

    const/4 v1, 0x4

    aget-object v1, v0, v1

    .line 3097
    const-string v0, "Compression"

    .line 3098
    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/d/a$b;

    .line 3099
    if-eqz v0, :cond_1

    .line 3100
    iget-object v2, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v2}, Landroid/support/d/a$b;->b(Ljava/nio/ByteOrder;)I

    move-result v0

    iput v0, p0, Landroid/support/d/a;->P:I

    .line 3101
    iget v0, p0, Landroid/support/d/a;->P:I

    sparse-switch v0, :sswitch_data_0

    .line 3119
    :cond_0
    :goto_0
    return-void

    .line 3103
    :sswitch_0
    invoke-direct {p0, p1, v1}, Landroid/support/d/a;->a(Landroid/support/d/a$a;Ljava/util/HashMap;)V

    goto :goto_0

    .line 3108
    :sswitch_1
    invoke-direct {p0, v1}, Landroid/support/d/a;->a(Ljava/util/HashMap;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3109
    invoke-direct {p0, p1, v1}, Landroid/support/d/a;->b(Landroid/support/d/a$a;Ljava/util/HashMap;)V

    goto :goto_0

    .line 3116
    :cond_1
    const/4 v0, 0x6

    iput v0, p0, Landroid/support/d/a;->P:I

    .line 3117
    invoke-direct {p0, p1, v1}, Landroid/support/d/a;->a(Landroid/support/d/a$a;Ljava/util/HashMap;)V

    goto :goto_0

    .line 3101
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x6 -> :sswitch_0
        0x7 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public a(Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 1426
    invoke-direct {p0, p1}, Landroid/support/d/a;->b(Ljava/lang/String;)Landroid/support/d/a$b;

    move-result-object v0

    .line 1427
    if-nez v0, :cond_0

    .line 1434
    :goto_0
    return p2

    .line 1432
    :cond_0
    :try_start_0
    iget-object v1, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Landroid/support/d/a$b;->b(Ljava/nio/ByteOrder;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    goto :goto_0

    .line 1433
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v4, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 1386
    invoke-direct {p0, p1}, Landroid/support/d/a;->b(Ljava/lang/String;)Landroid/support/d/a$b;

    move-result-object v0

    .line 1387
    if-eqz v0, :cond_5

    .line 1388
    sget-object v2, Landroid/support/d/a;->D:Ljava/util/HashSet;

    invoke-virtual {v2, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1389
    iget-object v1, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Landroid/support/d/a$b;->c(Ljava/nio/ByteOrder;)Ljava/lang/String;

    move-result-object v0

    .line 1414
    :goto_0
    return-object v0

    .line 1391
    :cond_0
    const-string v2, "GPSTimeStamp"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1393
    iget v2, v0, Landroid/support/d/a$b;->a:I

    const/4 v3, 0x5

    if-eq v2, v3, :cond_1

    iget v2, v0, Landroid/support/d/a$b;->a:I

    const/16 v3, 0xa

    if-eq v2, v3, :cond_1

    .line 1395
    const-string v2, "ExifInterface"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GPS Timestamp format is not rational. format="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v0, v0, Landroid/support/d/a$b;->a:I

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 1396
    goto :goto_0

    .line 1398
    :cond_1
    iget-object v2, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    invoke-static {v0, v2}, Landroid/support/d/a$b;->a(Landroid/support/d/a$b;Ljava/nio/ByteOrder;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/support/d/a$d;

    check-cast v0, [Landroid/support/d/a$d;

    .line 1399
    if-eqz v0, :cond_2

    array-length v2, v0

    if-eq v2, v4, :cond_3

    .line 1400
    :cond_2
    const-string v2, "ExifInterface"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid GPS Timestamp array. array="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 1401
    goto :goto_0

    .line 1403
    :cond_3
    const-string v1, "%02d:%02d:%02d"

    new-array v2, v4, [Ljava/lang/Object;

    aget-object v3, v0, v6

    iget-wide v4, v3, Landroid/support/d/a$d;->a:J

    long-to-float v3, v4

    aget-object v4, v0, v6

    iget-wide v4, v4, Landroid/support/d/a$d;->b:J

    long-to-float v4, v4

    div-float/2addr v3, v4

    float-to-int v3, v3

    .line 1404
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    aget-object v3, v0, v7

    iget-wide v4, v3, Landroid/support/d/a$d;->a:J

    long-to-float v3, v4

    aget-object v4, v0, v7

    iget-wide v4, v4, Landroid/support/d/a$d;->b:J

    long-to-float v4, v4

    div-float/2addr v3, v4

    float-to-int v3, v3

    .line 1405
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    aget-object v3, v0, v8

    iget-wide v4, v3, Landroid/support/d/a$d;->a:J

    long-to-float v3, v4

    aget-object v0, v0, v8

    iget-wide v4, v0, Landroid/support/d/a$d;->b:J

    long-to-float v0, v4

    div-float v0, v3, v0

    float-to-int v0, v0

    .line 1406
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v8

    .line 1403
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1409
    :cond_4
    :try_start_0
    iget-object v2, p0, Landroid/support/d/a;->K:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v2}, Landroid/support/d/a$b;->a(Ljava/nio/ByteOrder;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto/16 :goto_0

    .line 1410
    :catch_0
    move-exception v0

    move-object v0, v1

    .line 1411
    goto/16 :goto_0

    :cond_5
    move-object v0, v1

    .line 1414
    goto/16 :goto_0
.end method
