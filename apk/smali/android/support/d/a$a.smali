.class Landroid/support/d/a$a;
.super Ljava/io/InputStream;
.source "ExifInterface.java"

# interfaces
.implements Ljava/io/DataInput;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/d/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# static fields
.field private static final a:Ljava/nio/ByteOrder;

.field private static final b:Ljava/nio/ByteOrder;


# instance fields
.field private c:Ljava/io/DataInputStream;

.field private d:Ljava/nio/ByteOrder;

.field private final e:I

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3644
    sget-object v0, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    sput-object v0, Landroid/support/d/a$a;->a:Ljava/nio/ByteOrder;

    .line 3645
    sget-object v0, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    sput-object v0, Landroid/support/d/a$a;->b:Ljava/nio/ByteOrder;

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3652
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 3648
    sget-object v0, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    iput-object v0, p0, Landroid/support/d/a$a;->d:Ljava/nio/ByteOrder;

    .line 3653
    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    .line 3654
    iget-object v0, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->available()I

    move-result v0

    iput v0, p0, Landroid/support/d/a$a;->e:I

    .line 3655
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/d/a$a;->f:I

    .line 3656
    iget-object v0, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    iget v1, p0, Landroid/support/d/a$a;->e:I

    invoke-virtual {v0, v1}, Ljava/io/DataInputStream;->mark(I)V

    .line 3657
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3660
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {p0, v0}, Landroid/support/d/a$a;-><init>(Ljava/io/InputStream;)V

    .line 3661
    return-void
.end method

.method static synthetic a(Landroid/support/d/a$a;)I
    .locals 1

    .prologue
    .line 3643
    iget v0, p0, Landroid/support/d/a$a;->f:I

    return v0
.end method

.method static synthetic b(Landroid/support/d/a$a;)I
    .locals 1

    .prologue
    .line 3643
    iget v0, p0, Landroid/support/d/a$a;->e:I

    return v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 3682
    iget v0, p0, Landroid/support/d/a$a;->f:I

    return v0
.end method

.method public a(J)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3668
    iget v0, p0, Landroid/support/d/a$a;->f:I

    int-to-long v0, v0

    cmp-long v0, v0, p1

    if-lez v0, :cond_0

    .line 3669
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/d/a$a;->f:I

    .line 3670
    iget-object v0, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->reset()V

    .line 3671
    iget-object v0, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    iget v1, p0, Landroid/support/d/a$a;->e:I

    invoke-virtual {v0, v1}, Ljava/io/DataInputStream;->mark(I)V

    .line 3676
    :goto_0
    long-to-int v0, p1

    invoke-virtual {p0, v0}, Landroid/support/d/a$a;->skipBytes(I)I

    move-result v0

    long-to-int v1, p1

    if-eq v0, v1, :cond_1

    .line 3677
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Couldn\'t seek up to the byteCount"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3673
    :cond_0
    iget v0, p0, Landroid/support/d/a$a;->f:I

    int-to-long v0, v0

    sub-long/2addr p1, v0

    goto :goto_0

    .line 3679
    :cond_1
    return-void
.end method

.method public a(Ljava/nio/ByteOrder;)V
    .locals 0

    .prologue
    .line 3664
    iput-object p1, p0, Landroid/support/d/a$a;->d:Ljava/nio/ByteOrder;

    .line 3665
    return-void
.end method

.method public available()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3687
    iget-object v0, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->available()I

    move-result v0

    return v0
.end method

.method public b()J
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3839
    invoke-virtual {p0}, Landroid/support/d/a$a;->readInt()I

    move-result v0

    int-to-long v0, v0

    const-wide v2, 0xffffffffL

    and-long/2addr v0, v2

    return-wide v0
.end method

.method public read()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3692
    iget v0, p0, Landroid/support/d/a$a;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/d/a$a;->f:I

    .line 3693
    iget-object v0, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->read()I

    move-result v0

    return v0
.end method

.method public read([BII)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3698
    iget-object v0, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/DataInputStream;->read([BII)I

    move-result v0

    .line 3699
    iget v1, p0, Landroid/support/d/a$a;->f:I

    add-int/2addr v1, v0

    iput v1, p0, Landroid/support/d/a$a;->f:I

    .line 3700
    return v0
.end method

.method public readBoolean()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3717
    iget v0, p0, Landroid/support/d/a$a;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/d/a$a;->f:I

    .line 3718
    iget-object v0, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readBoolean()Z

    move-result v0

    return v0
.end method

.method public readByte()B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3757
    iget v0, p0, Landroid/support/d/a$a;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/d/a$a;->f:I

    .line 3758
    iget v0, p0, Landroid/support/d/a$a;->f:I

    iget v1, p0, Landroid/support/d/a$a;->e:I

    if-le v0, v1, :cond_0

    .line 3759
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 3761
    :cond_0
    iget-object v0, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->read()I

    move-result v0

    .line 3762
    if-gez v0, :cond_1

    .line 3763
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 3765
    :cond_1
    int-to-byte v0, v0

    return v0
.end method

.method public readChar()C
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3723
    iget v0, p0, Landroid/support/d/a$a;->f:I

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Landroid/support/d/a$a;->f:I

    .line 3724
    iget-object v0, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readChar()C

    move-result v0

    return v0
.end method

.method public readDouble()D
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3878
    invoke-virtual {p0}, Landroid/support/d/a$a;->readLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    return-wide v0
.end method

.method public readFloat()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3873
    invoke-virtual {p0}, Landroid/support/d/a$a;->readInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    return v0
.end method

.method public readFully([B)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3746
    iget v0, p0, Landroid/support/d/a$a;->f:I

    array-length v1, p1

    add-int/2addr v0, v1

    iput v0, p0, Landroid/support/d/a$a;->f:I

    .line 3747
    iget v0, p0, Landroid/support/d/a$a;->f:I

    iget v1, p0, Landroid/support/d/a$a;->e:I

    if-le v0, v1, :cond_0

    .line 3748
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 3750
    :cond_0
    iget-object v0, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    const/4 v1, 0x0

    array-length v2, p1

    invoke-virtual {v0, p1, v1, v2}, Ljava/io/DataInputStream;->read([BII)I

    move-result v0

    array-length v1, p1

    if-eq v0, v1, :cond_1

    .line 3751
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Couldn\'t read up to the length of buffer"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3753
    :cond_1
    return-void
.end method

.method public readFully([BII)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3735
    iget v0, p0, Landroid/support/d/a$a;->f:I

    add-int/2addr v0, p3

    iput v0, p0, Landroid/support/d/a$a;->f:I

    .line 3736
    iget v0, p0, Landroid/support/d/a$a;->f:I

    iget v1, p0, Landroid/support/d/a$a;->e:I

    if-le v0, v1, :cond_0

    .line 3737
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 3739
    :cond_0
    iget-object v0, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/DataInputStream;->read([BII)I

    move-result v0

    if-eq v0, p3, :cond_1

    .line 3740
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Couldn\'t read up to the length of buffer"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3742
    :cond_1
    return-void
.end method

.method public readInt()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3789
    iget v0, p0, Landroid/support/d/a$a;->f:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Landroid/support/d/a$a;->f:I

    .line 3790
    iget v0, p0, Landroid/support/d/a$a;->f:I

    iget v1, p0, Landroid/support/d/a$a;->e:I

    if-le v0, v1, :cond_0

    .line 3791
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 3793
    :cond_0
    iget-object v0, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->read()I

    move-result v0

    .line 3794
    iget-object v1, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->read()I

    move-result v1

    .line 3795
    iget-object v2, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->read()I

    move-result v2

    .line 3796
    iget-object v3, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    invoke-virtual {v3}, Ljava/io/DataInputStream;->read()I

    move-result v3

    .line 3797
    or-int v4, v0, v1

    or-int/2addr v4, v2

    or-int/2addr v4, v3

    if-gez v4, :cond_1

    .line 3798
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 3800
    :cond_1
    iget-object v4, p0, Landroid/support/d/a$a;->d:Ljava/nio/ByteOrder;

    sget-object v5, Landroid/support/d/a$a;->a:Ljava/nio/ByteOrder;

    if-ne v4, v5, :cond_2

    .line 3801
    shl-int/lit8 v3, v3, 0x18

    shl-int/lit8 v2, v2, 0x10

    add-int/2addr v2, v3

    shl-int/lit8 v1, v1, 0x8

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 3803
    :goto_0
    return v0

    .line 3802
    :cond_2
    iget-object v4, p0, Landroid/support/d/a$a;->d:Ljava/nio/ByteOrder;

    sget-object v5, Landroid/support/d/a$a;->b:Ljava/nio/ByteOrder;

    if-ne v4, v5, :cond_3

    .line 3803
    shl-int/lit8 v0, v0, 0x18

    shl-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    shl-int/lit8 v1, v2, 0x8

    add-int/2addr v0, v1

    add-int/2addr v0, v3

    goto :goto_0

    .line 3805
    :cond_3
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid byte order: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/d/a$a;->d:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public readLine()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3711
    const-string v0, "ExifInterface"

    const-string v1, "Currently unsupported"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3712
    const/4 v0, 0x0

    return-object v0
.end method

.method public readLong()J
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3844
    iget v0, p0, Landroid/support/d/a$a;->f:I

    add-int/lit8 v0, v0, 0x8

    iput v0, p0, Landroid/support/d/a$a;->f:I

    .line 3845
    iget v0, p0, Landroid/support/d/a$a;->f:I

    iget v1, p0, Landroid/support/d/a$a;->e:I

    if-le v0, v1, :cond_0

    .line 3846
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 3848
    :cond_0
    iget-object v0, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->read()I

    move-result v0

    .line 3849
    iget-object v1, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->read()I

    move-result v1

    .line 3850
    iget-object v2, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->read()I

    move-result v2

    .line 3851
    iget-object v3, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    invoke-virtual {v3}, Ljava/io/DataInputStream;->read()I

    move-result v3

    .line 3852
    iget-object v4, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    invoke-virtual {v4}, Ljava/io/DataInputStream;->read()I

    move-result v4

    .line 3853
    iget-object v5, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    invoke-virtual {v5}, Ljava/io/DataInputStream;->read()I

    move-result v5

    .line 3854
    iget-object v6, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->read()I

    move-result v6

    .line 3855
    iget-object v7, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    invoke-virtual {v7}, Ljava/io/DataInputStream;->read()I

    move-result v7

    .line 3856
    or-int v8, v0, v1

    or-int/2addr v8, v2

    or-int/2addr v8, v3

    or-int/2addr v8, v4

    or-int/2addr v8, v5

    or-int/2addr v8, v6

    or-int/2addr v8, v7

    if-gez v8, :cond_1

    .line 3857
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 3859
    :cond_1
    iget-object v8, p0, Landroid/support/d/a$a;->d:Ljava/nio/ByteOrder;

    sget-object v9, Landroid/support/d/a$a;->a:Ljava/nio/ByteOrder;

    if-ne v8, v9, :cond_2

    .line 3860
    int-to-long v8, v7

    const/16 v7, 0x38

    shl-long/2addr v8, v7

    int-to-long v6, v6

    const/16 v10, 0x30

    shl-long/2addr v6, v10

    add-long/2addr v6, v8

    int-to-long v8, v5

    const/16 v5, 0x28

    shl-long/2addr v8, v5

    add-long/2addr v6, v8

    int-to-long v4, v4

    const/16 v8, 0x20

    shl-long/2addr v4, v8

    add-long/2addr v4, v6

    int-to-long v6, v3

    const/16 v3, 0x18

    shl-long/2addr v6, v3

    add-long/2addr v4, v6

    int-to-long v2, v2

    const/16 v6, 0x10

    shl-long/2addr v2, v6

    add-long/2addr v2, v4

    int-to-long v4, v1

    const/16 v1, 0x8

    shl-long/2addr v4, v1

    add-long/2addr v2, v4

    int-to-long v0, v0

    add-long/2addr v0, v2

    .line 3864
    :goto_0
    return-wide v0

    .line 3863
    :cond_2
    iget-object v8, p0, Landroid/support/d/a$a;->d:Ljava/nio/ByteOrder;

    sget-object v9, Landroid/support/d/a$a;->b:Ljava/nio/ByteOrder;

    if-ne v8, v9, :cond_3

    .line 3864
    int-to-long v8, v0

    const/16 v0, 0x38

    shl-long/2addr v8, v0

    int-to-long v0, v1

    const/16 v10, 0x30

    shl-long/2addr v0, v10

    add-long/2addr v0, v8

    int-to-long v8, v2

    const/16 v2, 0x28

    shl-long/2addr v8, v2

    add-long/2addr v0, v8

    int-to-long v2, v3

    const/16 v8, 0x20

    shl-long/2addr v2, v8

    add-long/2addr v0, v2

    int-to-long v2, v4

    const/16 v4, 0x18

    shl-long/2addr v2, v4

    add-long/2addr v0, v2

    int-to-long v2, v5

    const/16 v4, 0x10

    shl-long/2addr v2, v4

    add-long/2addr v0, v2

    int-to-long v2, v6

    const/16 v4, 0x8

    shl-long/2addr v2, v4

    add-long/2addr v0, v2

    int-to-long v2, v7

    add-long/2addr v0, v2

    goto :goto_0

    .line 3868
    :cond_3
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid byte order: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/d/a$a;->d:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public readShort()S
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3770
    iget v0, p0, Landroid/support/d/a$a;->f:I

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Landroid/support/d/a$a;->f:I

    .line 3771
    iget v0, p0, Landroid/support/d/a$a;->f:I

    iget v1, p0, Landroid/support/d/a$a;->e:I

    if-le v0, v1, :cond_0

    .line 3772
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 3774
    :cond_0
    iget-object v0, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->read()I

    move-result v0

    .line 3775
    iget-object v1, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->read()I

    move-result v1

    .line 3776
    or-int v2, v0, v1

    if-gez v2, :cond_1

    .line 3777
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 3779
    :cond_1
    iget-object v2, p0, Landroid/support/d/a$a;->d:Ljava/nio/ByteOrder;

    sget-object v3, Landroid/support/d/a$a;->a:Ljava/nio/ByteOrder;

    if-ne v2, v3, :cond_2

    .line 3780
    shl-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    int-to-short v0, v0

    .line 3782
    :goto_0
    return v0

    .line 3781
    :cond_2
    iget-object v2, p0, Landroid/support/d/a$a;->d:Ljava/nio/ByteOrder;

    sget-object v3, Landroid/support/d/a$a;->b:Ljava/nio/ByteOrder;

    if-ne v2, v3, :cond_3

    .line 3782
    shl-int/lit8 v0, v0, 0x8

    add-int/2addr v0, v1

    int-to-short v0, v0

    goto :goto_0

    .line 3784
    :cond_3
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid byte order: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/d/a$a;->d:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public readUTF()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3729
    iget v0, p0, Landroid/support/d/a$a;->f:I

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Landroid/support/d/a$a;->f:I

    .line 3730
    iget-object v0, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public readUnsignedByte()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3705
    iget v0, p0, Landroid/support/d/a$a;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/d/a$a;->f:I

    .line 3706
    iget-object v0, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readUnsignedByte()I

    move-result v0

    return v0
.end method

.method public readUnsignedShort()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3821
    iget v0, p0, Landroid/support/d/a$a;->f:I

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Landroid/support/d/a$a;->f:I

    .line 3822
    iget v0, p0, Landroid/support/d/a$a;->f:I

    iget v1, p0, Landroid/support/d/a$a;->e:I

    if-le v0, v1, :cond_0

    .line 3823
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 3825
    :cond_0
    iget-object v0, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->read()I

    move-result v0

    .line 3826
    iget-object v1, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->read()I

    move-result v1

    .line 3827
    or-int v2, v0, v1

    if-gez v2, :cond_1

    .line 3828
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 3830
    :cond_1
    iget-object v2, p0, Landroid/support/d/a$a;->d:Ljava/nio/ByteOrder;

    sget-object v3, Landroid/support/d/a$a;->a:Ljava/nio/ByteOrder;

    if-ne v2, v3, :cond_2

    .line 3831
    shl-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 3833
    :goto_0
    return v0

    .line 3832
    :cond_2
    iget-object v2, p0, Landroid/support/d/a$a;->d:Ljava/nio/ByteOrder;

    sget-object v3, Landroid/support/d/a$a;->b:Ljava/nio/ByteOrder;

    if-ne v2, v3, :cond_3

    .line 3833
    shl-int/lit8 v0, v0, 0x8

    add-int/2addr v0, v1

    goto :goto_0

    .line 3835
    :cond_3
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid byte order: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/d/a$a;->d:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public skipBytes(I)I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3810
    iget v0, p0, Landroid/support/d/a$a;->e:I

    iget v1, p0, Landroid/support/d/a$a;->f:I

    sub-int/2addr v0, v1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 3811
    const/4 v0, 0x0

    .line 3812
    :goto_0
    if-ge v0, v1, :cond_0

    .line 3813
    iget-object v2, p0, Landroid/support/d/a$a;->c:Ljava/io/DataInputStream;

    sub-int v3, v1, v0

    invoke-virtual {v2, v3}, Ljava/io/DataInputStream;->skipBytes(I)I

    move-result v2

    add-int/2addr v0, v2

    goto :goto_0

    .line 3815
    :cond_0
    iget v1, p0, Landroid/support/d/a$a;->f:I

    add-int/2addr v1, v0

    iput v1, p0, Landroid/support/d/a$a;->f:I

    .line 3816
    return v0
.end method
