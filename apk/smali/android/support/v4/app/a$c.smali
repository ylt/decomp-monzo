.class Landroid/support/v4/app/a$c;
.super Landroid/app/SharedElementCallback;
.source "ActivityCompat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v4/app/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation


# instance fields
.field protected a:Landroid/support/v4/app/ap;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/ap;)V
    .locals 0

    .prologue
    .line 451
    invoke-direct {p0}, Landroid/app/SharedElementCallback;-><init>()V

    .line 452
    iput-object p1, p0, Landroid/support/v4/app/a$c;->a:Landroid/support/v4/app/ap;

    .line 453
    return-void
.end method


# virtual methods
.method public onCaptureSharedElementSnapshot(Landroid/view/View;Landroid/graphics/Matrix;Landroid/graphics/RectF;)Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 482
    iget-object v0, p0, Landroid/support/v4/app/a$c;->a:Landroid/support/v4/app/ap;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/ap;->a(Landroid/view/View;Landroid/graphics/Matrix;Landroid/graphics/RectF;)Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method public onCreateSnapshotView(Landroid/content/Context;Landroid/os/Parcelable;)Landroid/view/View;
    .locals 1

    .prologue
    .line 488
    iget-object v0, p0, Landroid/support/v4/app/a$c;->a:Landroid/support/v4/app/ap;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/app/ap;->a(Landroid/content/Context;Landroid/os/Parcelable;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onMapSharedElements(Ljava/util/List;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 476
    iget-object v0, p0, Landroid/support/v4/app/a$c;->a:Landroid/support/v4/app/ap;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/app/ap;->a(Ljava/util/List;Ljava/util/Map;)V

    .line 477
    return-void
.end method

.method public onRejectSharedElements(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 471
    iget-object v0, p0, Landroid/support/v4/app/a$c;->a:Landroid/support/v4/app/ap;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/ap;->a(Ljava/util/List;)V

    .line 472
    return-void
.end method

.method public onSharedElementEnd(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 465
    iget-object v0, p0, Landroid/support/v4/app/a$c;->a:Landroid/support/v4/app/ap;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/ap;->b(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 467
    return-void
.end method

.method public onSharedElementStart(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 458
    iget-object v0, p0, Landroid/support/v4/app/a$c;->a:Landroid/support/v4/app/ap;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/ap;->a(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 460
    return-void
.end method
