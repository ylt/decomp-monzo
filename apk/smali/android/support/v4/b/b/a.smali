.class public final Landroid/support/v4/b/b/a;
.super Ljava/lang/Object;
.source "FingerprintManagerCompat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v4/b/b/a$a;,
        Landroid/support/v4/b/b/a$c;,
        Landroid/support/v4/b/b/a$b;
    }
.end annotation


# static fields
.field static final a:Landroid/support/v4/b/b/a$b;


# instance fields
.field private b:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 53
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    .line 54
    new-instance v0, Landroid/support/v4/b/b/a$a;

    invoke-direct {v0}, Landroid/support/v4/b/b/a$a;-><init>()V

    sput-object v0, Landroid/support/v4/b/b/a;->a:Landroid/support/v4/b/b/a$b;

    .line 58
    :goto_0
    return-void

    .line 56
    :cond_0
    new-instance v0, Landroid/support/v4/b/b/a$c;

    invoke-direct {v0}, Landroid/support/v4/b/b/a$c;-><init>()V

    sput-object v0, Landroid/support/v4/b/b/a;->a:Landroid/support/v4/b/b/a$b;

    goto :goto_0
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Landroid/support/v4/b/b/a;->b:Landroid/content/Context;

    .line 49
    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/support/v4/b/b/a;
    .locals 1

    .prologue
    .line 44
    new-instance v0, Landroid/support/v4/b/b/a;

    invoke-direct {v0, p0}, Landroid/support/v4/b/b/a;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 2

    .prologue
    .line 66
    sget-object v0, Landroid/support/v4/b/b/a;->a:Landroid/support/v4/b/b/a$b;

    iget-object v1, p0, Landroid/support/v4/b/b/a;->b:Landroid/content/Context;

    invoke-interface {v0, v1}, Landroid/support/v4/b/b/a$b;->a(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 75
    sget-object v0, Landroid/support/v4/b/b/a;->a:Landroid/support/v4/b/b/a$b;

    iget-object v1, p0, Landroid/support/v4/b/b/a;->b:Landroid/content/Context;

    invoke-interface {v0, v1}, Landroid/support/v4/b/b/a$b;->b(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
