.class public Landroid/support/design/internal/d;
.super Ljava/lang/Object;
.source "BottomNavigationPresenter.java"

# interfaces
.implements Landroid/support/v7/view/menu/o;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/design/internal/d$a;
    }
.end annotation


# instance fields
.field private a:Landroid/support/v7/view/menu/h;

.field private b:Landroid/support/design/internal/c;

.field private c:Z

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/design/internal/d;->c:Z

    .line 121
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 95
    iput p1, p0, Landroid/support/design/internal/d;->d:I

    .line 96
    return-void
.end method

.method public a(Landroid/content/Context;Landroid/support/v7/view/menu/h;)V
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Landroid/support/design/internal/d;->b:Landroid/support/design/internal/c;

    iget-object v1, p0, Landroid/support/design/internal/d;->a:Landroid/support/v7/view/menu/h;

    invoke-virtual {v0, v1}, Landroid/support/design/internal/c;->a(Landroid/support/v7/view/menu/h;)V

    .line 50
    iput-object p2, p0, Landroid/support/design/internal/d;->a:Landroid/support/v7/view/menu/h;

    .line 51
    return-void
.end method

.method public a(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 112
    instance-of v0, p1, Landroid/support/design/internal/d$a;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Landroid/support/design/internal/d;->b:Landroid/support/design/internal/c;

    check-cast p1, Landroid/support/design/internal/d$a;

    iget v1, p1, Landroid/support/design/internal/d$a;->a:I

    invoke-virtual {v0, v1}, Landroid/support/design/internal/c;->a(I)V

    .line 115
    :cond_0
    return-void
.end method

.method public a(Landroid/support/design/internal/c;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Landroid/support/design/internal/d;->b:Landroid/support/design/internal/c;

    .line 45
    return-void
.end method

.method public a(Landroid/support/v7/view/menu/h;Z)V
    .locals 0

    .prologue
    .line 77
    return-void
.end method

.method public a(Landroid/support/v7/view/menu/o$a;)V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Landroid/support/design/internal/d;->c:Z

    if-eqz v0, :cond_0

    .line 66
    :goto_0
    return-void

    .line 61
    :cond_0
    if-eqz p1, :cond_1

    .line 62
    iget-object v0, p0, Landroid/support/design/internal/d;->b:Landroid/support/design/internal/c;

    invoke-virtual {v0}, Landroid/support/design/internal/c;->a()V

    goto :goto_0

    .line 64
    :cond_1
    iget-object v0, p0, Landroid/support/design/internal/d;->b:Landroid/support/design/internal/c;

    invoke-virtual {v0}, Landroid/support/design/internal/c;->b()V

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/support/v7/view/menu/h;Landroid/support/v7/view/menu/j;)Z
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/support/v7/view/menu/u;)Z
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Landroid/support/design/internal/d;->d:I

    return v0
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 118
    iput-boolean p1, p0, Landroid/support/design/internal/d;->c:Z

    .line 119
    return-void
.end method

.method public b(Landroid/support/v7/view/menu/h;Landroid/support/v7/view/menu/j;)Z
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    return v0
.end method

.method public c()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 105
    new-instance v0, Landroid/support/design/internal/d$a;

    invoke-direct {v0}, Landroid/support/design/internal/d$a;-><init>()V

    .line 106
    iget-object v1, p0, Landroid/support/design/internal/d;->b:Landroid/support/design/internal/c;

    invoke-virtual {v1}, Landroid/support/design/internal/c;->getSelectedItemId()I

    move-result v1

    iput v1, v0, Landroid/support/design/internal/d$a;->a:I

    .line 107
    return-object v0
.end method
