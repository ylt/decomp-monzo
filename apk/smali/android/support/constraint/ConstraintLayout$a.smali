.class public Landroid/support/constraint/ConstraintLayout$a;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source "ConstraintLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/constraint/ConstraintLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field A:I

.field public B:F

.field public C:F

.field public D:I

.field public E:I

.field public F:I

.field public G:I

.field public H:I

.field public I:I

.field public J:I

.field public K:I

.field public L:F

.field public M:F

.field public N:I

.field public O:I

.field public P:I

.field Q:Z

.field R:Z

.field S:Z

.field T:Z

.field U:Z

.field V:Z

.field W:I

.field X:I

.field Y:I

.field Z:I

.field public a:I

.field aa:I

.field ab:I

.field ac:F

.field ad:Landroid/support/constraint/a/a/c;

.field public ae:Z

.field public b:I

.field public c:F

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field public m:I

.field public n:I

.field public o:I

.field public p:I

.field public q:I

.field public r:I

.field public s:I

.field public t:I

.field public u:I

.field public v:I

.field public w:F

.field public x:F

.field public y:Ljava/lang/String;

.field z:F


# direct methods
.method public constructor <init>(II)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 2116
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 1554
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->a:I

    .line 1559
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->b:I

    .line 1564
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->c:F

    .line 1569
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->d:I

    .line 1574
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->e:I

    .line 1579
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->f:I

    .line 1584
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->g:I

    .line 1589
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->h:I

    .line 1594
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->i:I

    .line 1599
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->j:I

    .line 1604
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->k:I

    .line 1609
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->l:I

    .line 1614
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->m:I

    .line 1619
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->n:I

    .line 1624
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->o:I

    .line 1629
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->p:I

    .line 1634
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->q:I

    .line 1639
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->r:I

    .line 1644
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->s:I

    .line 1649
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->t:I

    .line 1654
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->u:I

    .line 1659
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->v:I

    .line 1664
    iput v4, p0, Landroid/support/constraint/ConstraintLayout$a;->w:F

    .line 1669
    iput v4, p0, Landroid/support/constraint/ConstraintLayout$a;->x:F

    .line 1674
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/constraint/ConstraintLayout$a;->y:Ljava/lang/String;

    .line 1679
    iput v3, p0, Landroid/support/constraint/ConstraintLayout$a;->z:F

    .line 1684
    iput v5, p0, Landroid/support/constraint/ConstraintLayout$a;->A:I

    .line 1690
    iput v3, p0, Landroid/support/constraint/ConstraintLayout$a;->B:F

    .line 1696
    iput v3, p0, Landroid/support/constraint/ConstraintLayout$a;->C:F

    .line 1708
    iput v2, p0, Landroid/support/constraint/ConstraintLayout$a;->D:I

    .line 1720
    iput v2, p0, Landroid/support/constraint/ConstraintLayout$a;->E:I

    .line 1732
    iput v2, p0, Landroid/support/constraint/ConstraintLayout$a;->F:I

    .line 1744
    iput v2, p0, Landroid/support/constraint/ConstraintLayout$a;->G:I

    .line 1750
    iput v2, p0, Landroid/support/constraint/ConstraintLayout$a;->H:I

    .line 1756
    iput v2, p0, Landroid/support/constraint/ConstraintLayout$a;->I:I

    .line 1762
    iput v2, p0, Landroid/support/constraint/ConstraintLayout$a;->J:I

    .line 1768
    iput v2, p0, Landroid/support/constraint/ConstraintLayout$a;->K:I

    .line 1773
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->L:F

    .line 1778
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->M:F

    .line 1784
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->N:I

    .line 1790
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->O:I

    .line 1792
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->P:I

    .line 1795
    iput-boolean v5, p0, Landroid/support/constraint/ConstraintLayout$a;->Q:Z

    .line 1796
    iput-boolean v5, p0, Landroid/support/constraint/ConstraintLayout$a;->R:Z

    .line 1798
    iput-boolean v2, p0, Landroid/support/constraint/ConstraintLayout$a;->S:Z

    .line 1799
    iput-boolean v2, p0, Landroid/support/constraint/ConstraintLayout$a;->T:Z

    .line 1800
    iput-boolean v2, p0, Landroid/support/constraint/ConstraintLayout$a;->U:Z

    .line 1801
    iput-boolean v2, p0, Landroid/support/constraint/ConstraintLayout$a;->V:Z

    .line 1803
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->W:I

    .line 1804
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->X:I

    .line 1805
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->Y:I

    .line 1806
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->Z:I

    .line 1807
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->aa:I

    .line 1808
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->ab:I

    .line 1809
    iput v4, p0, Landroid/support/constraint/ConstraintLayout$a;->ac:F

    .line 1811
    new-instance v0, Landroid/support/constraint/a/a/c;

    invoke-direct {v0}, Landroid/support/constraint/a/a/c;-><init>()V

    iput-object v0, p0, Landroid/support/constraint/ConstraintLayout$a;->ad:Landroid/support/constraint/a/a/c;

    .line 1813
    iput-boolean v2, p0, Landroid/support/constraint/ConstraintLayout$a;->ae:Z

    .line 2117
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, -0x2

    const/4 v1, 0x0

    const/4 v8, -0x1

    .line 1877
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1554
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->a:I

    .line 1559
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->b:I

    .line 1564
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->c:F

    .line 1569
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->d:I

    .line 1574
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->e:I

    .line 1579
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->f:I

    .line 1584
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->g:I

    .line 1589
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->h:I

    .line 1594
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->i:I

    .line 1599
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->j:I

    .line 1604
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->k:I

    .line 1609
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->l:I

    .line 1614
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->m:I

    .line 1619
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->n:I

    .line 1624
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->o:I

    .line 1629
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->p:I

    .line 1634
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->q:I

    .line 1639
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->r:I

    .line 1644
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->s:I

    .line 1649
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->t:I

    .line 1654
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->u:I

    .line 1659
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->v:I

    .line 1664
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->w:F

    .line 1669
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->x:F

    .line 1674
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/constraint/ConstraintLayout$a;->y:Ljava/lang/String;

    .line 1679
    iput v10, p0, Landroid/support/constraint/ConstraintLayout$a;->z:F

    .line 1684
    iput v11, p0, Landroid/support/constraint/ConstraintLayout$a;->A:I

    .line 1690
    iput v10, p0, Landroid/support/constraint/ConstraintLayout$a;->B:F

    .line 1696
    iput v10, p0, Landroid/support/constraint/ConstraintLayout$a;->C:F

    .line 1708
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->D:I

    .line 1720
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->E:I

    .line 1732
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->F:I

    .line 1744
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->G:I

    .line 1750
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->H:I

    .line 1756
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->I:I

    .line 1762
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->J:I

    .line 1768
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->K:I

    .line 1773
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->L:F

    .line 1778
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->M:F

    .line 1784
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->N:I

    .line 1790
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->O:I

    .line 1792
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->P:I

    .line 1795
    iput-boolean v11, p0, Landroid/support/constraint/ConstraintLayout$a;->Q:Z

    .line 1796
    iput-boolean v11, p0, Landroid/support/constraint/ConstraintLayout$a;->R:Z

    .line 1798
    iput-boolean v1, p0, Landroid/support/constraint/ConstraintLayout$a;->S:Z

    .line 1799
    iput-boolean v1, p0, Landroid/support/constraint/ConstraintLayout$a;->T:Z

    .line 1800
    iput-boolean v1, p0, Landroid/support/constraint/ConstraintLayout$a;->U:Z

    .line 1801
    iput-boolean v1, p0, Landroid/support/constraint/ConstraintLayout$a;->V:Z

    .line 1803
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->W:I

    .line 1804
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->X:I

    .line 1805
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->Y:I

    .line 1806
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->Z:I

    .line 1807
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->aa:I

    .line 1808
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->ab:I

    .line 1809
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->ac:F

    .line 1811
    new-instance v0, Landroid/support/constraint/a/a/c;

    invoke-direct {v0}, Landroid/support/constraint/a/a/c;-><init>()V

    iput-object v0, p0, Landroid/support/constraint/ConstraintLayout$a;->ad:Landroid/support/constraint/a/a/c;

    .line 1813
    iput-boolean v1, p0, Landroid/support/constraint/ConstraintLayout$a;->ae:Z

    .line 1879
    sget-object v0, Landroid/support/constraint/e$b;->ConstraintLayout_Layout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 1880
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v4

    move v2, v1

    .line 1881
    :goto_0
    if-ge v2, v4, :cond_2e

    .line 1882
    invoke-virtual {v3, v2}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v0

    .line 1883
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintLeft_toLeftOf:I

    if-ne v0, v5, :cond_1

    .line 1884
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->d:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    iput v5, p0, Landroid/support/constraint/ConstraintLayout$a;->d:I

    .line 1885
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->d:I

    if-ne v5, v8, :cond_0

    .line 1886
    invoke-virtual {v3, v0, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->d:I

    .line 1881
    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1888
    :cond_1
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintLeft_toRightOf:I

    if-ne v0, v5, :cond_2

    .line 1889
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->e:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    iput v5, p0, Landroid/support/constraint/ConstraintLayout$a;->e:I

    .line 1890
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->e:I

    if-ne v5, v8, :cond_0

    .line 1891
    invoke-virtual {v3, v0, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->e:I

    goto :goto_1

    .line 1893
    :cond_2
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintRight_toLeftOf:I

    if-ne v0, v5, :cond_3

    .line 1894
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->f:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    iput v5, p0, Landroid/support/constraint/ConstraintLayout$a;->f:I

    .line 1895
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->f:I

    if-ne v5, v8, :cond_0

    .line 1896
    invoke-virtual {v3, v0, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->f:I

    goto :goto_1

    .line 1898
    :cond_3
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintRight_toRightOf:I

    if-ne v0, v5, :cond_4

    .line 1899
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->g:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    iput v5, p0, Landroid/support/constraint/ConstraintLayout$a;->g:I

    .line 1900
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->g:I

    if-ne v5, v8, :cond_0

    .line 1901
    invoke-virtual {v3, v0, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->g:I

    goto :goto_1

    .line 1903
    :cond_4
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintTop_toTopOf:I

    if-ne v0, v5, :cond_5

    .line 1904
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->h:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    iput v5, p0, Landroid/support/constraint/ConstraintLayout$a;->h:I

    .line 1905
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->h:I

    if-ne v5, v8, :cond_0

    .line 1906
    invoke-virtual {v3, v0, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->h:I

    goto :goto_1

    .line 1908
    :cond_5
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintTop_toBottomOf:I

    if-ne v0, v5, :cond_6

    .line 1909
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->i:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    iput v5, p0, Landroid/support/constraint/ConstraintLayout$a;->i:I

    .line 1910
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->i:I

    if-ne v5, v8, :cond_0

    .line 1911
    invoke-virtual {v3, v0, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->i:I

    goto :goto_1

    .line 1913
    :cond_6
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintBottom_toTopOf:I

    if-ne v0, v5, :cond_7

    .line 1914
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->j:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    iput v5, p0, Landroid/support/constraint/ConstraintLayout$a;->j:I

    .line 1915
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->j:I

    if-ne v5, v8, :cond_0

    .line 1916
    invoke-virtual {v3, v0, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->j:I

    goto/16 :goto_1

    .line 1918
    :cond_7
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintBottom_toBottomOf:I

    if-ne v0, v5, :cond_8

    .line 1919
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->k:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    iput v5, p0, Landroid/support/constraint/ConstraintLayout$a;->k:I

    .line 1920
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->k:I

    if-ne v5, v8, :cond_0

    .line 1921
    invoke-virtual {v3, v0, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->k:I

    goto/16 :goto_1

    .line 1923
    :cond_8
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintBaseline_toBaselineOf:I

    if-ne v0, v5, :cond_9

    .line 1924
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->l:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    iput v5, p0, Landroid/support/constraint/ConstraintLayout$a;->l:I

    .line 1925
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->l:I

    if-ne v5, v8, :cond_0

    .line 1926
    invoke-virtual {v3, v0, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->l:I

    goto/16 :goto_1

    .line 1928
    :cond_9
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_editor_absoluteX:I

    if-ne v0, v5, :cond_a

    .line 1929
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->N:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->N:I

    goto/16 :goto_1

    .line 1930
    :cond_a
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_editor_absoluteY:I

    if-ne v0, v5, :cond_b

    .line 1931
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->O:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->O:I

    goto/16 :goto_1

    .line 1932
    :cond_b
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintGuide_begin:I

    if-ne v0, v5, :cond_c

    .line 1933
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->a:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->a:I

    goto/16 :goto_1

    .line 1934
    :cond_c
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintGuide_end:I

    if-ne v0, v5, :cond_d

    .line 1935
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->b:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->b:I

    goto/16 :goto_1

    .line 1936
    :cond_d
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintGuide_percent:I

    if-ne v0, v5, :cond_e

    .line 1937
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->c:F

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->c:F

    goto/16 :goto_1

    .line 1938
    :cond_e
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_android_orientation:I

    if-ne v0, v5, :cond_f

    .line 1939
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->P:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->P:I

    goto/16 :goto_1

    .line 1940
    :cond_f
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintStart_toEndOf:I

    if-ne v0, v5, :cond_10

    .line 1941
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->m:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    iput v5, p0, Landroid/support/constraint/ConstraintLayout$a;->m:I

    .line 1942
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->m:I

    if-ne v5, v8, :cond_0

    .line 1943
    invoke-virtual {v3, v0, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->m:I

    goto/16 :goto_1

    .line 1945
    :cond_10
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintStart_toStartOf:I

    if-ne v0, v5, :cond_11

    .line 1946
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->n:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    iput v5, p0, Landroid/support/constraint/ConstraintLayout$a;->n:I

    .line 1947
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->n:I

    if-ne v5, v8, :cond_0

    .line 1948
    invoke-virtual {v3, v0, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->n:I

    goto/16 :goto_1

    .line 1950
    :cond_11
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintEnd_toStartOf:I

    if-ne v0, v5, :cond_12

    .line 1951
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->o:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    iput v5, p0, Landroid/support/constraint/ConstraintLayout$a;->o:I

    .line 1952
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->o:I

    if-ne v5, v8, :cond_0

    .line 1953
    invoke-virtual {v3, v0, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->o:I

    goto/16 :goto_1

    .line 1955
    :cond_12
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintEnd_toEndOf:I

    if-ne v0, v5, :cond_13

    .line 1956
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->p:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    iput v5, p0, Landroid/support/constraint/ConstraintLayout$a;->p:I

    .line 1957
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->p:I

    if-ne v5, v8, :cond_0

    .line 1958
    invoke-virtual {v3, v0, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->p:I

    goto/16 :goto_1

    .line 1960
    :cond_13
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_goneMarginLeft:I

    if-ne v0, v5, :cond_14

    .line 1961
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->q:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->q:I

    goto/16 :goto_1

    .line 1962
    :cond_14
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_goneMarginTop:I

    if-ne v0, v5, :cond_15

    .line 1963
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->r:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->r:I

    goto/16 :goto_1

    .line 1964
    :cond_15
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_goneMarginRight:I

    if-ne v0, v5, :cond_16

    .line 1965
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->s:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->s:I

    goto/16 :goto_1

    .line 1966
    :cond_16
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_goneMarginBottom:I

    if-ne v0, v5, :cond_17

    .line 1967
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->t:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->t:I

    goto/16 :goto_1

    .line 1968
    :cond_17
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_goneMarginStart:I

    if-ne v0, v5, :cond_18

    .line 1969
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->u:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->u:I

    goto/16 :goto_1

    .line 1970
    :cond_18
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_goneMarginEnd:I

    if-ne v0, v5, :cond_19

    .line 1971
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->v:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->v:I

    goto/16 :goto_1

    .line 1972
    :cond_19
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintHorizontal_bias:I

    if-ne v0, v5, :cond_1a

    .line 1973
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->w:F

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->w:F

    goto/16 :goto_1

    .line 1974
    :cond_1a
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintVertical_bias:I

    if-ne v0, v5, :cond_1b

    .line 1975
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->x:F

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->x:F

    goto/16 :goto_1

    .line 1976
    :cond_1b
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintDimensionRatio:I

    if-ne v0, v5, :cond_21

    .line 1977
    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/constraint/ConstraintLayout$a;->y:Ljava/lang/String;

    .line 1978
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->z:F

    .line 1979
    iput v8, p0, Landroid/support/constraint/ConstraintLayout$a;->A:I

    .line 1980
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout$a;->y:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1981
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout$a;->y:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    .line 1982
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout$a;->y:Ljava/lang/String;

    const/16 v6, 0x2c

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 1983
    if-lez v0, :cond_1e

    add-int/lit8 v6, v5, -0x1

    if-ge v0, v6, :cond_1e

    .line 1984
    iget-object v6, p0, Landroid/support/constraint/ConstraintLayout$a;->y:Ljava/lang/String;

    invoke-virtual {v6, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 1985
    const-string v7, "W"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1d

    .line 1986
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->A:I

    .line 1990
    :cond_1c
    :goto_2
    add-int/lit8 v0, v0, 0x1

    .line 1994
    :goto_3
    iget-object v6, p0, Landroid/support/constraint/ConstraintLayout$a;->y:Ljava/lang/String;

    const/16 v7, 0x3a

    invoke-virtual {v6, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    .line 1995
    if-ltz v6, :cond_20

    add-int/lit8 v5, v5, -0x1

    if-ge v6, v5, :cond_20

    .line 1996
    iget-object v5, p0, Landroid/support/constraint/ConstraintLayout$a;->y:Ljava/lang/String;

    invoke-virtual {v5, v0, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1997
    iget-object v5, p0, Landroid/support/constraint/ConstraintLayout$a;->y:Ljava/lang/String;

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 1998
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_0

    .line 2000
    :try_start_0
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    .line 2001
    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    .line 2002
    cmpl-float v6, v0, v10

    if-lez v6, :cond_0

    cmpl-float v6, v5, v10

    if-lez v6, :cond_0

    .line 2003
    iget v6, p0, Landroid/support/constraint/ConstraintLayout$a;->A:I

    if-ne v6, v11, :cond_1f

    .line 2004
    div-float v0, v5, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->z:F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 2009
    :catch_0
    move-exception v0

    goto/16 :goto_1

    .line 1987
    :cond_1d
    const-string v7, "H"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1c

    .line 1988
    iput v11, p0, Landroid/support/constraint/ConstraintLayout$a;->A:I

    goto :goto_2

    :cond_1e
    move v0, v1

    .line 1992
    goto :goto_3

    .line 2006
    :cond_1f
    div-float/2addr v0, v5

    :try_start_1
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->z:F
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 2014
    :cond_20
    iget-object v5, p0, Landroid/support/constraint/ConstraintLayout$a;->y:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2015
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_0

    .line 2017
    :try_start_2
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->z:F
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_1

    .line 2018
    :catch_1
    move-exception v0

    goto/16 :goto_1

    .line 2024
    :cond_21
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintHorizontal_weight:I

    if-ne v0, v5, :cond_22

    .line 2025
    invoke-virtual {v3, v0, v10}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->B:F

    goto/16 :goto_1

    .line 2026
    :cond_22
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintVertical_weight:I

    if-ne v0, v5, :cond_23

    .line 2027
    invoke-virtual {v3, v0, v10}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->C:F

    goto/16 :goto_1

    .line 2028
    :cond_23
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintHorizontal_chainStyle:I

    if-ne v0, v5, :cond_24

    .line 2029
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->D:I

    goto/16 :goto_1

    .line 2030
    :cond_24
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintVertical_chainStyle:I

    if-ne v0, v5, :cond_25

    .line 2031
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->E:I

    goto/16 :goto_1

    .line 2032
    :cond_25
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintWidth_default:I

    if-ne v0, v5, :cond_26

    .line 2033
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->F:I

    goto/16 :goto_1

    .line 2034
    :cond_26
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintHeight_default:I

    if-ne v0, v5, :cond_27

    .line 2035
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->G:I

    goto/16 :goto_1

    .line 2036
    :cond_27
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintWidth_min:I

    if-ne v0, v5, :cond_28

    .line 2038
    :try_start_3
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->H:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v5

    iput v5, p0, Landroid/support/constraint/ConstraintLayout$a;->H:I
    :try_end_3
    .catch Landroid/view/InflateException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_1

    .line 2039
    :catch_2
    move-exception v5

    .line 2040
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->H:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 2041
    if-ne v0, v9, :cond_0

    .line 2042
    iput v9, p0, Landroid/support/constraint/ConstraintLayout$a;->H:I

    goto/16 :goto_1

    .line 2045
    :cond_28
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintWidth_max:I

    if-ne v0, v5, :cond_29

    .line 2047
    :try_start_4
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->J:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v5

    iput v5, p0, Landroid/support/constraint/ConstraintLayout$a;->J:I
    :try_end_4
    .catch Landroid/view/InflateException; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_1

    .line 2048
    :catch_3
    move-exception v5

    .line 2049
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->J:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 2050
    if-ne v0, v9, :cond_0

    .line 2051
    iput v9, p0, Landroid/support/constraint/ConstraintLayout$a;->J:I

    goto/16 :goto_1

    .line 2054
    :cond_29
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintWidth_percent:I

    if-ne v0, v5, :cond_2a

    .line 2055
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->L:F

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->L:F

    goto/16 :goto_1

    .line 2056
    :cond_2a
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintHeight_min:I

    if-ne v0, v5, :cond_2b

    .line 2058
    :try_start_5
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->I:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v5

    iput v5, p0, Landroid/support/constraint/ConstraintLayout$a;->I:I
    :try_end_5
    .catch Landroid/view/InflateException; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_1

    .line 2059
    :catch_4
    move-exception v5

    .line 2060
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->I:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 2061
    if-ne v0, v9, :cond_0

    .line 2062
    iput v9, p0, Landroid/support/constraint/ConstraintLayout$a;->I:I

    goto/16 :goto_1

    .line 2065
    :cond_2b
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintHeight_max:I

    if-ne v0, v5, :cond_2c

    .line 2067
    :try_start_6
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->K:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v5

    iput v5, p0, Landroid/support/constraint/ConstraintLayout$a;->K:I
    :try_end_6
    .catch Landroid/view/InflateException; {:try_start_6 .. :try_end_6} :catch_5

    goto/16 :goto_1

    .line 2068
    :catch_5
    move-exception v5

    .line 2069
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->K:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 2070
    if-ne v0, v9, :cond_0

    .line 2071
    iput v9, p0, Landroid/support/constraint/ConstraintLayout$a;->K:I

    goto/16 :goto_1

    .line 2074
    :cond_2c
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintHeight_percent:I

    if-ne v0, v5, :cond_2d

    .line 2075
    iget v5, p0, Landroid/support/constraint/ConstraintLayout$a;->M:F

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->M:F

    goto/16 :goto_1

    .line 2076
    :cond_2d
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintLeft_creator:I

    if-eq v0, v5, :cond_0

    .line 2078
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintTop_creator:I

    if-eq v0, v5, :cond_0

    .line 2080
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintRight_creator:I

    if-eq v0, v5, :cond_0

    .line 2082
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintBottom_creator:I

    if-eq v0, v5, :cond_0

    .line 2084
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_constraintBaseline_creator:I

    if-ne v0, v5, :cond_0

    goto/16 :goto_1

    .line 2090
    :cond_2e
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 2091
    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout$a;->a()V

    .line 2092
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 2120
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1554
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->a:I

    .line 1559
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->b:I

    .line 1564
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->c:F

    .line 1569
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->d:I

    .line 1574
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->e:I

    .line 1579
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->f:I

    .line 1584
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->g:I

    .line 1589
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->h:I

    .line 1594
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->i:I

    .line 1599
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->j:I

    .line 1604
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->k:I

    .line 1609
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->l:I

    .line 1614
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->m:I

    .line 1619
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->n:I

    .line 1624
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->o:I

    .line 1629
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->p:I

    .line 1634
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->q:I

    .line 1639
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->r:I

    .line 1644
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->s:I

    .line 1649
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->t:I

    .line 1654
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->u:I

    .line 1659
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->v:I

    .line 1664
    iput v4, p0, Landroid/support/constraint/ConstraintLayout$a;->w:F

    .line 1669
    iput v4, p0, Landroid/support/constraint/ConstraintLayout$a;->x:F

    .line 1674
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/constraint/ConstraintLayout$a;->y:Ljava/lang/String;

    .line 1679
    iput v3, p0, Landroid/support/constraint/ConstraintLayout$a;->z:F

    .line 1684
    iput v5, p0, Landroid/support/constraint/ConstraintLayout$a;->A:I

    .line 1690
    iput v3, p0, Landroid/support/constraint/ConstraintLayout$a;->B:F

    .line 1696
    iput v3, p0, Landroid/support/constraint/ConstraintLayout$a;->C:F

    .line 1708
    iput v2, p0, Landroid/support/constraint/ConstraintLayout$a;->D:I

    .line 1720
    iput v2, p0, Landroid/support/constraint/ConstraintLayout$a;->E:I

    .line 1732
    iput v2, p0, Landroid/support/constraint/ConstraintLayout$a;->F:I

    .line 1744
    iput v2, p0, Landroid/support/constraint/ConstraintLayout$a;->G:I

    .line 1750
    iput v2, p0, Landroid/support/constraint/ConstraintLayout$a;->H:I

    .line 1756
    iput v2, p0, Landroid/support/constraint/ConstraintLayout$a;->I:I

    .line 1762
    iput v2, p0, Landroid/support/constraint/ConstraintLayout$a;->J:I

    .line 1768
    iput v2, p0, Landroid/support/constraint/ConstraintLayout$a;->K:I

    .line 1773
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->L:F

    .line 1778
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->M:F

    .line 1784
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->N:I

    .line 1790
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->O:I

    .line 1792
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->P:I

    .line 1795
    iput-boolean v5, p0, Landroid/support/constraint/ConstraintLayout$a;->Q:Z

    .line 1796
    iput-boolean v5, p0, Landroid/support/constraint/ConstraintLayout$a;->R:Z

    .line 1798
    iput-boolean v2, p0, Landroid/support/constraint/ConstraintLayout$a;->S:Z

    .line 1799
    iput-boolean v2, p0, Landroid/support/constraint/ConstraintLayout$a;->T:Z

    .line 1800
    iput-boolean v2, p0, Landroid/support/constraint/ConstraintLayout$a;->U:Z

    .line 1801
    iput-boolean v2, p0, Landroid/support/constraint/ConstraintLayout$a;->V:Z

    .line 1803
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->W:I

    .line 1804
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->X:I

    .line 1805
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->Y:I

    .line 1806
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->Z:I

    .line 1807
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->aa:I

    .line 1808
    iput v1, p0, Landroid/support/constraint/ConstraintLayout$a;->ab:I

    .line 1809
    iput v4, p0, Landroid/support/constraint/ConstraintLayout$a;->ac:F

    .line 1811
    new-instance v0, Landroid/support/constraint/a/a/c;

    invoke-direct {v0}, Landroid/support/constraint/a/a/c;-><init>()V

    iput-object v0, p0, Landroid/support/constraint/ConstraintLayout$a;->ad:Landroid/support/constraint/a/a/c;

    .line 1813
    iput-boolean v2, p0, Landroid/support/constraint/ConstraintLayout$a;->ae:Z

    .line 2121
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    .line 2095
    iput-boolean v1, p0, Landroid/support/constraint/ConstraintLayout$a;->T:Z

    .line 2096
    iput-boolean v2, p0, Landroid/support/constraint/ConstraintLayout$a;->Q:Z

    .line 2097
    iput-boolean v2, p0, Landroid/support/constraint/ConstraintLayout$a;->R:Z

    .line 2098
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->width:I

    if-eqz v0, :cond_0

    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->width:I

    if-ne v0, v3, :cond_1

    .line 2099
    :cond_0
    iput-boolean v1, p0, Landroid/support/constraint/ConstraintLayout$a;->Q:Z

    .line 2101
    :cond_1
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->height:I

    if-eqz v0, :cond_2

    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->height:I

    if-ne v0, v3, :cond_3

    .line 2102
    :cond_2
    iput-boolean v1, p0, Landroid/support/constraint/ConstraintLayout$a;->R:Z

    .line 2104
    :cond_3
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->c:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_4

    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->a:I

    if-ne v0, v3, :cond_4

    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->b:I

    if-eq v0, v3, :cond_6

    .line 2105
    :cond_4
    iput-boolean v2, p0, Landroid/support/constraint/ConstraintLayout$a;->T:Z

    .line 2106
    iput-boolean v2, p0, Landroid/support/constraint/ConstraintLayout$a;->Q:Z

    .line 2107
    iput-boolean v2, p0, Landroid/support/constraint/ConstraintLayout$a;->R:Z

    .line 2108
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout$a;->ad:Landroid/support/constraint/a/a/c;

    instance-of v0, v0, Landroid/support/constraint/a/a/e;

    if-nez v0, :cond_5

    .line 2109
    new-instance v0, Landroid/support/constraint/a/a/e;

    invoke-direct {v0}, Landroid/support/constraint/a/a/e;-><init>()V

    iput-object v0, p0, Landroid/support/constraint/ConstraintLayout$a;->ad:Landroid/support/constraint/a/a/c;

    .line 2111
    :cond_5
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout$a;->ad:Landroid/support/constraint/a/a/c;

    check-cast v0, Landroid/support/constraint/a/a/e;

    iget v1, p0, Landroid/support/constraint/ConstraintLayout$a;->P:I

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/e;->a(I)V

    .line 2113
    :cond_6
    return-void
.end method

.method public resolveLayoutDirection(I)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v3, -0x1

    .line 2129
    invoke-super {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;->resolveLayoutDirection(I)V

    .line 2131
    iput v3, p0, Landroid/support/constraint/ConstraintLayout$a;->Y:I

    .line 2132
    iput v3, p0, Landroid/support/constraint/ConstraintLayout$a;->Z:I

    .line 2133
    iput v3, p0, Landroid/support/constraint/ConstraintLayout$a;->W:I

    .line 2134
    iput v3, p0, Landroid/support/constraint/ConstraintLayout$a;->X:I

    .line 2136
    iput v3, p0, Landroid/support/constraint/ConstraintLayout$a;->aa:I

    .line 2137
    iput v3, p0, Landroid/support/constraint/ConstraintLayout$a;->ab:I

    .line 2138
    iget v2, p0, Landroid/support/constraint/ConstraintLayout$a;->q:I

    iput v2, p0, Landroid/support/constraint/ConstraintLayout$a;->aa:I

    .line 2139
    iget v2, p0, Landroid/support/constraint/ConstraintLayout$a;->s:I

    iput v2, p0, Landroid/support/constraint/ConstraintLayout$a;->ab:I

    .line 2140
    iget v2, p0, Landroid/support/constraint/ConstraintLayout$a;->w:F

    iput v2, p0, Landroid/support/constraint/ConstraintLayout$a;->ac:F

    .line 2142
    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout$a;->getLayoutDirection()I

    move-result v2

    if-ne v1, v2, :cond_7

    move v2, v1

    .line 2144
    :goto_0
    if-eqz v2, :cond_9

    .line 2146
    iget v2, p0, Landroid/support/constraint/ConstraintLayout$a;->m:I

    if-eq v2, v3, :cond_8

    .line 2147
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->m:I

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->Y:I

    move v0, v1

    .line 2153
    :cond_0
    :goto_1
    iget v2, p0, Landroid/support/constraint/ConstraintLayout$a;->o:I

    if-eq v2, v3, :cond_1

    .line 2154
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->o:I

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->X:I

    move v0, v1

    .line 2157
    :cond_1
    iget v2, p0, Landroid/support/constraint/ConstraintLayout$a;->p:I

    if-eq v2, v3, :cond_11

    .line 2158
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->p:I

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->W:I

    .line 2161
    :goto_2
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->u:I

    if-eq v0, v3, :cond_2

    .line 2162
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->u:I

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->ab:I

    .line 2164
    :cond_2
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->v:I

    if-eq v0, v3, :cond_3

    .line 2165
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->v:I

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->aa:I

    .line 2167
    :cond_3
    if-eqz v1, :cond_4

    .line 2168
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Landroid/support/constraint/ConstraintLayout$a;->w:F

    sub-float/2addr v0, v1

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->ac:F

    .line 2191
    :cond_4
    :goto_3
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->o:I

    if-ne v0, v3, :cond_5

    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->p:I

    if-ne v0, v3, :cond_5

    .line 2192
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->f:I

    if-eq v0, v3, :cond_f

    .line 2193
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->f:I

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->Y:I

    .line 2198
    :cond_5
    :goto_4
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->n:I

    if-ne v0, v3, :cond_6

    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->m:I

    if-ne v0, v3, :cond_6

    .line 2199
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->d:I

    if-eq v0, v3, :cond_10

    .line 2200
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->d:I

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->W:I

    .line 2205
    :cond_6
    :goto_5
    return-void

    :cond_7
    move v2, v0

    .line 2142
    goto :goto_0

    .line 2149
    :cond_8
    iget v2, p0, Landroid/support/constraint/ConstraintLayout$a;->n:I

    if-eq v2, v3, :cond_0

    .line 2150
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->n:I

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->Z:I

    move v0, v1

    .line 2151
    goto :goto_1

    .line 2171
    :cond_9
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->m:I

    if-eq v0, v3, :cond_a

    .line 2172
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->m:I

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->X:I

    .line 2174
    :cond_a
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->n:I

    if-eq v0, v3, :cond_b

    .line 2175
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->n:I

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->W:I

    .line 2177
    :cond_b
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->o:I

    if-eq v0, v3, :cond_c

    .line 2178
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->o:I

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->Y:I

    .line 2180
    :cond_c
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->p:I

    if-eq v0, v3, :cond_d

    .line 2181
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->p:I

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->Z:I

    .line 2183
    :cond_d
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->u:I

    if-eq v0, v3, :cond_e

    .line 2184
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->u:I

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->aa:I

    .line 2186
    :cond_e
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->v:I

    if-eq v0, v3, :cond_4

    .line 2187
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->v:I

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->ab:I

    goto :goto_3

    .line 2194
    :cond_f
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->g:I

    if-eq v0, v3, :cond_5

    .line 2195
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->g:I

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->Z:I

    goto :goto_4

    .line 2201
    :cond_10
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->e:I

    if-eq v0, v3, :cond_6

    .line 2202
    iget v0, p0, Landroid/support/constraint/ConstraintLayout$a;->e:I

    iput v0, p0, Landroid/support/constraint/ConstraintLayout$a;->X:I

    goto :goto_5

    :cond_11
    move v1, v0

    goto/16 :goto_2
.end method
