.class public Landroid/support/constraint/Group;
.super Landroid/support/constraint/a;
.source "Group.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1}, Landroid/support/constraint/a;-><init>(Landroid/content/Context;)V

    .line 15
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Landroid/support/constraint/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Landroid/support/constraint/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    return-void
.end method


# virtual methods
.method public a(Landroid/support/constraint/ConstraintLayout;)V
    .locals 7

    .prologue
    const/16 v6, 0x15

    const/4 v1, 0x0

    .line 32
    invoke-virtual {p0}, Landroid/support/constraint/Group;->getVisibility()I

    move-result v3

    .line 34
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v6, :cond_2

    .line 35
    invoke-virtual {p0}, Landroid/support/constraint/Group;->getElevation()F

    move-result v0

    .line 37
    :goto_0
    const/4 v2, 0x0

    :goto_1
    iget v4, p0, Landroid/support/constraint/Group;->b:I

    if-ge v2, v4, :cond_1

    .line 38
    iget-object v4, p0, Landroid/support/constraint/Group;->a:[I

    aget v4, v4, v2

    .line 39
    invoke-virtual {p1, v4}, Landroid/support/constraint/ConstraintLayout;->a(I)Landroid/view/View;

    move-result-object v4

    .line 40
    if-eqz v4, :cond_0

    .line 41
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    .line 42
    cmpl-float v5, v0, v1

    if-lez v5, :cond_0

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v5, v6, :cond_0

    .line 43
    invoke-virtual {v4, v0}, Landroid/view/View;->setElevation(F)V

    .line 37
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 47
    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method protected a(Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 26
    invoke-super {p0, p1}, Landroid/support/constraint/a;->a(Landroid/util/AttributeSet;)V

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/constraint/Group;->e:Z

    .line 28
    return-void
.end method

.method public c(Landroid/support/constraint/ConstraintLayout;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 51
    invoke-virtual {p0}, Landroid/support/constraint/Group;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/ConstraintLayout$a;

    .line 52
    iget-object v1, v0, Landroid/support/constraint/ConstraintLayout$a;->ad:Landroid/support/constraint/a/a/c;

    invoke-virtual {v1, v2}, Landroid/support/constraint/a/a/c;->e(I)V

    .line 53
    iget-object v0, v0, Landroid/support/constraint/ConstraintLayout$a;->ad:Landroid/support/constraint/a/a/c;

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/a/c;->f(I)V

    .line 54
    return-void
.end method
