.class public abstract Landroid/support/constraint/a;
.super Landroid/view/View;
.source "ConstraintHelper.java"


# instance fields
.field protected a:[I

.field protected b:I

.field protected c:Landroid/content/Context;

.field protected d:Landroid/support/constraint/a/a/f;

.field protected e:Z

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 28
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 19
    const/16 v0, 0x20

    new-array v0, v0, [I

    iput-object v0, p0, Landroid/support/constraint/a;->a:[I

    .line 20
    iput v1, p0, Landroid/support/constraint/a;->b:I

    .line 23
    iput-object v2, p0, Landroid/support/constraint/a;->d:Landroid/support/constraint/a/a/f;

    .line 24
    iput-boolean v1, p0, Landroid/support/constraint/a;->e:Z

    .line 29
    iput-object p1, p0, Landroid/support/constraint/a;->c:Landroid/content/Context;

    .line 30
    invoke-virtual {p0, v2}, Landroid/support/constraint/a;->a(Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    const/16 v0, 0x20

    new-array v0, v0, [I

    iput-object v0, p0, Landroid/support/constraint/a;->a:[I

    .line 20
    iput v1, p0, Landroid/support/constraint/a;->b:I

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/constraint/a;->d:Landroid/support/constraint/a/a/f;

    .line 24
    iput-boolean v1, p0, Landroid/support/constraint/a;->e:Z

    .line 35
    iput-object p1, p0, Landroid/support/constraint/a;->c:Landroid/content/Context;

    .line 36
    invoke-virtual {p0, p2}, Landroid/support/constraint/a;->a(Landroid/util/AttributeSet;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 19
    const/16 v0, 0x20

    new-array v0, v0, [I

    iput-object v0, p0, Landroid/support/constraint/a;->a:[I

    .line 20
    iput v1, p0, Landroid/support/constraint/a;->b:I

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/constraint/a;->d:Landroid/support/constraint/a/a/f;

    .line 24
    iput-boolean v1, p0, Landroid/support/constraint/a;->e:Z

    .line 41
    iput-object p1, p0, Landroid/support/constraint/a;->c:Landroid/content/Context;

    .line 42
    invoke-virtual {p0, p2}, Landroid/support/constraint/a;->a(Landroid/util/AttributeSet;)V

    .line 43
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 121
    if-nez p1, :cond_1

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 124
    :cond_1
    iget-object v0, p0, Landroid/support/constraint/a;->c:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 127
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 130
    :try_start_0
    const-class v0, Landroid/support/constraint/e$a;

    .line 131
    invoke-virtual {v0, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 132
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 137
    :goto_1
    if-nez v1, :cond_2

    .line 138
    iget-object v0, p0, Landroid/support/constraint/a;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "id"

    iget-object v4, p0, Landroid/support/constraint/a;->c:Landroid/content/Context;

    .line 139
    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 138
    invoke-virtual {v0, v3, v1, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 141
    :cond_2
    if-nez v1, :cond_4

    invoke-virtual {p0}, Landroid/support/constraint/a;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Landroid/support/constraint/a;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/support/constraint/ConstraintLayout;

    if-eqz v0, :cond_4

    .line 142
    invoke-virtual {p0}, Landroid/support/constraint/a;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/ConstraintLayout;

    .line 143
    invoke-virtual {v0, v2, v3}, Landroid/support/constraint/ConstraintLayout;->a(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 144
    if-eqz v0, :cond_4

    instance-of v2, v0, Ljava/lang/Integer;

    if-eqz v2, :cond_4

    .line 145
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 149
    :goto_2
    if-eqz v0, :cond_3

    .line 150
    invoke-virtual {p0, v0, v5}, Landroid/support/constraint/a;->setTag(ILjava/lang/Object;)V

    goto :goto_0

    .line 134
    :catch_0
    move-exception v0

    move v1, v2

    goto :goto_1

    .line 152
    :cond_3
    const-string v0, "ConstraintHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not find id of \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method private setIds(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 157
    if-nez p1, :cond_0

    .line 170
    :goto_0
    return-void

    .line 160
    :cond_0
    const/4 v0, 0x0

    .line 162
    :goto_1
    const/16 v1, 0x2c

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 163
    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 164
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/constraint/a;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 167
    :cond_1
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/constraint/a;->a(Ljava/lang/String;)V

    .line 168
    add-int/lit8 v0, v1, 0x1

    .line 169
    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Landroid/support/constraint/a;->d:Landroid/support/constraint/a/a/f;

    if-nez v0, :cond_1

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    invoke-virtual {p0}, Landroid/support/constraint/a;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 114
    instance-of v1, v0, Landroid/support/constraint/ConstraintLayout$a;

    if-eqz v1, :cond_0

    .line 115
    check-cast v0, Landroid/support/constraint/ConstraintLayout$a;

    .line 116
    iget-object v1, p0, Landroid/support/constraint/a;->d:Landroid/support/constraint/a/a/f;

    iput-object v1, v0, Landroid/support/constraint/ConstraintLayout$a;->ad:Landroid/support/constraint/a/a/c;

    goto :goto_0
.end method

.method public a(Landroid/support/constraint/ConstraintLayout;)V
    .locals 3

    .prologue
    .line 178
    invoke-virtual {p0}, Landroid/support/constraint/a;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Landroid/support/constraint/a;->f:Ljava/lang/String;

    invoke-direct {p0, v0}, Landroid/support/constraint/a;->setIds(Ljava/lang/String;)V

    .line 181
    :cond_0
    iget-object v0, p0, Landroid/support/constraint/a;->d:Landroid/support/constraint/a/a/f;

    if-nez v0, :cond_2

    .line 192
    :cond_1
    return-void

    .line 184
    :cond_2
    iget-object v0, p0, Landroid/support/constraint/a;->d:Landroid/support/constraint/a/a/f;

    invoke-virtual {v0}, Landroid/support/constraint/a/a/f;->D()V

    .line 185
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Landroid/support/constraint/a;->b:I

    if-ge v0, v1, :cond_1

    .line 186
    iget-object v1, p0, Landroid/support/constraint/a;->a:[I

    aget v1, v1, v0

    .line 187
    invoke-virtual {p1, v1}, Landroid/support/constraint/ConstraintLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 188
    if-eqz v1, :cond_3

    .line 189
    iget-object v2, p0, Landroid/support/constraint/a;->d:Landroid/support/constraint/a/a/f;

    invoke-virtual {p1, v1}, Landroid/support/constraint/ConstraintLayout;->a(Landroid/view/View;)Landroid/support/constraint/a/a/c;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/support/constraint/a/a/f;->b(Landroid/support/constraint/a/a/c;)V

    .line 185
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected a(Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    .line 46
    if-eqz p1, :cond_1

    .line 47
    invoke-virtual {p0}, Landroid/support/constraint/a;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Landroid/support/constraint/e$b;->ConstraintLayout_Layout:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 48
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v2

    .line 49
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    .line 50
    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v3

    .line 51
    sget v4, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_constraint_referenced_ids:I

    if-ne v3, v4, :cond_0

    .line 52
    invoke-virtual {v1, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Landroid/support/constraint/a;->f:Ljava/lang/String;

    .line 53
    iget-object v3, p0, Landroid/support/constraint/a;->f:Ljava/lang/String;

    invoke-direct {p0, v3}, Landroid/support/constraint/a;->setIds(Ljava/lang/String;)V

    .line 49
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 57
    :cond_1
    return-void
.end method

.method public b(Landroid/support/constraint/ConstraintLayout;)V
    .locals 0

    .prologue
    .line 196
    return-void
.end method

.method public c(Landroid/support/constraint/ConstraintLayout;)V
    .locals 0

    .prologue
    .line 199
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 92
    return-void
.end method

.method public getReferencedIds()[I
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Landroid/support/constraint/a;->a:[I

    iget v1, p0, Landroid/support/constraint/a;->b:I

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    return-object v0
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 99
    iget-boolean v0, p0, Landroid/support/constraint/a;->e:Z

    if-eqz v0, :cond_0

    .line 100
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 104
    :goto_0
    return-void

    .line 102
    :cond_0
    invoke-virtual {p0, v1, v1}, Landroid/support/constraint/a;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public setReferencedIds([I)V
    .locals 3

    .prologue
    .line 72
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 73
    aget v1, p1, v0

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/support/constraint/a;->setTag(ILjava/lang/Object;)V

    .line 72
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 75
    :cond_0
    return-void
.end method

.method public setTag(ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 79
    iget v0, p0, Landroid/support/constraint/a;->b:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Landroid/support/constraint/a;->a:[I

    array-length v1, v1

    if-le v0, v1, :cond_0

    .line 80
    iget-object v0, p0, Landroid/support/constraint/a;->a:[I

    iget-object v1, p0, Landroid/support/constraint/a;->a:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p0, Landroid/support/constraint/a;->a:[I

    .line 82
    :cond_0
    iget-object v0, p0, Landroid/support/constraint/a;->a:[I

    iget v1, p0, Landroid/support/constraint/a;->b:I

    aput p1, v0, v1

    .line 83
    iget v0, p0, Landroid/support/constraint/a;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/constraint/a;->b:I

    .line 84
    return-void
.end method
