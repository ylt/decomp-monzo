.class public Landroid/support/constraint/a/a/d;
.super Landroid/support/constraint/a/a/j;
.source "ConstraintWidgetContainer.java"


# static fields
.field static al:Z


# instance fields
.field private aA:[Z

.field private aB:[Landroid/support/constraint/a/a/c;

.field private aC:Z

.field private aD:Z

.field protected aj:Landroid/support/constraint/a/e;

.field protected ak:Landroid/support/constraint/a/e;

.field am:I

.field an:I

.field ao:I

.field ap:I

.field aq:I

.field ar:I

.field private at:Landroid/support/constraint/a/a/i;

.field private au:I

.field private av:I

.field private aw:[Landroid/support/constraint/a/a/c;

.field private ax:[Landroid/support/constraint/a/a/c;

.field private ay:[Landroid/support/constraint/a/a/c;

.field private az:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x1

    sput-boolean v0, Landroid/support/constraint/a/a/d;->al:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 92
    invoke-direct {p0}, Landroid/support/constraint/a/a/j;-><init>()V

    .line 39
    new-instance v0, Landroid/support/constraint/a/e;

    invoke-direct {v0}, Landroid/support/constraint/a/e;-><init>()V

    iput-object v0, p0, Landroid/support/constraint/a/a/d;->aj:Landroid/support/constraint/a/e;

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/constraint/a/a/d;->ak:Landroid/support/constraint/a/e;

    .line 54
    iput v1, p0, Landroid/support/constraint/a/a/d;->au:I

    .line 55
    iput v1, p0, Landroid/support/constraint/a/a/d;->av:I

    .line 56
    new-array v0, v2, [Landroid/support/constraint/a/a/c;

    iput-object v0, p0, Landroid/support/constraint/a/a/d;->aw:[Landroid/support/constraint/a/a/c;

    .line 57
    new-array v0, v2, [Landroid/support/constraint/a/a/c;

    iput-object v0, p0, Landroid/support/constraint/a/a/d;->ax:[Landroid/support/constraint/a/a/c;

    .line 58
    new-array v0, v2, [Landroid/support/constraint/a/a/c;

    iput-object v0, p0, Landroid/support/constraint/a/a/d;->ay:[Landroid/support/constraint/a/a/c;

    .line 66
    const/4 v0, 0x2

    iput v0, p0, Landroid/support/constraint/a/a/d;->az:I

    .line 69
    const/4 v0, 0x3

    new-array v0, v0, [Z

    iput-object v0, p0, Landroid/support/constraint/a/a/d;->aA:[Z

    .line 76
    new-array v0, v2, [Landroid/support/constraint/a/a/c;

    iput-object v0, p0, Landroid/support/constraint/a/a/d;->aB:[Landroid/support/constraint/a/a/c;

    .line 82
    iput-boolean v1, p0, Landroid/support/constraint/a/a/d;->aC:Z

    .line 83
    iput-boolean v1, p0, Landroid/support/constraint/a/a/d;->aD:Z

    .line 93
    return-void
.end method

.method private J()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1864
    iput v0, p0, Landroid/support/constraint/a/a/d;->au:I

    .line 1865
    iput v0, p0, Landroid/support/constraint/a/a/d;->av:I

    .line 1866
    return-void
.end method

.method private a(Landroid/support/constraint/a/e;[Landroid/support/constraint/a/a/c;Landroid/support/constraint/a/a/c;I[Z)I
    .locals 10

    .prologue
    .line 1941
    const/4 v3, 0x0

    .line 1942
    const/4 v0, 0x0

    const/4 v1, 0x1

    aput-boolean v1, p5, v0

    .line 1943
    const/4 v0, 0x1

    const/4 v1, 0x0

    aput-boolean v1, p5, v0

    .line 1944
    const/4 v0, 0x0

    const/4 v1, 0x0

    aput-object v1, p2, v0

    .line 1945
    const/4 v0, 0x2

    const/4 v1, 0x0

    aput-object v1, p2, v0

    .line 1946
    const/4 v0, 0x1

    const/4 v1, 0x0

    aput-object v1, p2, v0

    .line 1947
    const/4 v0, 0x3

    const/4 v1, 0x0

    aput-object v1, p2, v0

    .line 1949
    if-nez p4, :cond_b

    .line 1950
    const/4 v0, 0x1

    .line 1952
    const/4 v2, 0x0

    .line 1953
    iget-object v1, p3, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v1, v1, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v1, :cond_1c

    iget-object v1, p3, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v1, v1, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v1, v1, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    if-eq v1, p0, :cond_1c

    .line 1954
    const/4 v0, 0x0

    move v1, v0

    .line 1956
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p3, Landroid/support/constraint/a/a/c;->ah:Landroid/support/constraint/a/a/c;

    .line 1957
    const/4 v0, 0x0

    .line 1958
    invoke-virtual {p3}, Landroid/support/constraint/a/a/c;->d()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_0

    move-object v0, p3

    :cond_0
    move-object v4, v2

    move v5, v3

    move-object v6, p3

    move-object v2, v0

    .line 1962
    :goto_1
    iget-object v3, v6, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v3, v3, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v3, :cond_1b

    .line 1963
    const/4 v3, 0x0

    iput-object v3, v6, Landroid/support/constraint/a/a/c;->ah:Landroid/support/constraint/a/a/c;

    .line 1964
    invoke-virtual {v6}, Landroid/support/constraint/a/a/c;->d()I

    move-result v3

    const/16 v7, 0x8

    if-eq v3, v7, :cond_9

    .line 1965
    if-nez v2, :cond_1a

    move-object v3, v6

    .line 1968
    :goto_2
    if-eqz v0, :cond_1

    if-eq v0, v6, :cond_1

    .line 1969
    iput-object v6, v0, Landroid/support/constraint/a/a/c;->ah:Landroid/support/constraint/a/a/c;

    :cond_1
    move-object v2, v6

    .line 1976
    :goto_3
    invoke-virtual {v6}, Landroid/support/constraint/a/a/c;->d()I

    move-result v0

    const/16 v7, 0x8

    if-eq v0, v7, :cond_4

    iget-object v0, v6, Landroid/support/constraint/a/a/c;->P:Landroid/support/constraint/a/a/c$a;

    sget-object v7, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-ne v0, v7, :cond_4

    .line 1977
    iget-object v0, v6, Landroid/support/constraint/a/a/c;->Q:Landroid/support/constraint/a/a/c$a;

    sget-object v7, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-ne v0, v7, :cond_2

    .line 1978
    const/4 v0, 0x0

    const/4 v7, 0x0

    aput-boolean v7, p5, v0

    .line 1980
    :cond_2
    iget v0, v6, Landroid/support/constraint/a/a/c;->z:F

    const/4 v7, 0x0

    cmpg-float v0, v0, v7

    if-gtz v0, :cond_4

    .line 1981
    const/4 v0, 0x0

    const/4 v7, 0x0

    aput-boolean v7, p5, v0

    .line 1982
    add-int/lit8 v0, v5, 0x1

    iget-object v7, p0, Landroid/support/constraint/a/a/d;->aw:[Landroid/support/constraint/a/a/c;

    array-length v7, v7

    if-lt v0, v7, :cond_3

    .line 1983
    iget-object v0, p0, Landroid/support/constraint/a/a/d;->aw:[Landroid/support/constraint/a/a/c;

    iget-object v7, p0, Landroid/support/constraint/a/a/d;->aw:[Landroid/support/constraint/a/a/c;

    array-length v7, v7

    mul-int/lit8 v7, v7, 0x2

    invoke-static {v0, v7}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/support/constraint/a/a/c;

    iput-object v0, p0, Landroid/support/constraint/a/a/d;->aw:[Landroid/support/constraint/a/a/c;

    .line 1985
    :cond_3
    iget-object v7, p0, Landroid/support/constraint/a/a/d;->aw:[Landroid/support/constraint/a/a/c;

    add-int/lit8 v0, v5, 0x1

    aput-object v6, v7, v5

    move v5, v0

    .line 1988
    :cond_4
    iget-object v0, v6, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v0, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-nez v0, :cond_a

    .line 2000
    :cond_5
    :goto_4
    iget-object v0, v6, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v0, :cond_6

    iget-object v0, v6, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    if-eq v0, p0, :cond_6

    .line 2001
    const/4 v1, 0x0

    .line 2003
    :cond_6
    iget-object v0, p3, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v0, :cond_7

    iget-object v0, v4, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-nez v0, :cond_8

    .line 2004
    :cond_7
    const/4 v0, 0x1

    const/4 v6, 0x1

    aput-boolean v6, p5, v0

    .line 2008
    :cond_8
    iput-boolean v1, p3, Landroid/support/constraint/a/a/c;->ad:Z

    .line 2009
    const/4 v0, 0x0

    iput-object v0, v4, Landroid/support/constraint/a/a/c;->ah:Landroid/support/constraint/a/a/c;

    .line 2010
    const/4 v0, 0x0

    aput-object p3, p2, v0

    .line 2011
    const/4 v0, 0x2

    aput-object v3, p2, v0

    .line 2012
    const/4 v0, 0x1

    aput-object v4, p2, v0

    .line 2013
    const/4 v0, 0x3

    aput-object v2, p2, v0

    .line 2080
    :goto_5
    return v5

    .line 1973
    :cond_9
    iget-object v3, v6, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v3, v3, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    iget-object v7, v6, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    const/4 v8, 0x0

    const/4 v9, 0x5

    invoke-virtual {p1, v3, v7, v8, v9}, Landroid/support/constraint/a/e;->c(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)Landroid/support/constraint/a/b;

    .line 1974
    iget-object v3, v6, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v3, v3, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    iget-object v7, v6, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    const/4 v8, 0x0

    const/4 v9, 0x5

    invoke-virtual {p1, v3, v7, v8, v9}, Landroid/support/constraint/a/e;->c(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)Landroid/support/constraint/a/b;

    move-object v3, v2

    move-object v2, v0

    goto/16 :goto_3

    .line 1991
    :cond_a
    iget-object v0, v6, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v0, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    if-ne v0, v6, :cond_5

    .line 1994
    iget-object v0, v6, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    if-eq v0, v6, :cond_5

    .line 1997
    iget-object v0, v6, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    move-object v4, v0

    move-object v6, v0

    move-object v0, v2

    move-object v2, v3

    .line 1998
    goto/16 :goto_1

    .line 2015
    :cond_b
    const/4 v0, 0x1

    .line 2017
    const/4 v2, 0x0

    .line 2018
    iget-object v1, p3, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v1, v1, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v1, :cond_19

    iget-object v1, p3, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v1, v1, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v1, v1, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    if-eq v1, p0, :cond_19

    .line 2019
    const/4 v0, 0x0

    move v1, v0

    .line 2021
    :goto_6
    const/4 v0, 0x0

    iput-object v0, p3, Landroid/support/constraint/a/a/c;->ai:Landroid/support/constraint/a/a/c;

    .line 2022
    const/4 v0, 0x0

    .line 2023
    invoke-virtual {p3}, Landroid/support/constraint/a/a/c;->d()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_c

    move-object v0, p3

    :cond_c
    move-object v4, v2

    move v5, v3

    move-object v6, p3

    move-object v2, v0

    .line 2027
    :goto_7
    iget-object v3, v6, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v3, v3, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v3, :cond_18

    .line 2028
    const/4 v3, 0x0

    iput-object v3, v6, Landroid/support/constraint/a/a/c;->ai:Landroid/support/constraint/a/a/c;

    .line 2029
    invoke-virtual {v6}, Landroid/support/constraint/a/a/c;->d()I

    move-result v3

    const/16 v7, 0x8

    if-eq v3, v7, :cond_15

    .line 2030
    if-nez v2, :cond_17

    move-object v3, v6

    .line 2033
    :goto_8
    if-eqz v0, :cond_d

    if-eq v0, v6, :cond_d

    .line 2034
    iput-object v6, v0, Landroid/support/constraint/a/a/c;->ai:Landroid/support/constraint/a/a/c;

    :cond_d
    move-object v2, v6

    .line 2041
    :goto_9
    invoke-virtual {v6}, Landroid/support/constraint/a/a/c;->d()I

    move-result v0

    const/16 v7, 0x8

    if-eq v0, v7, :cond_10

    iget-object v0, v6, Landroid/support/constraint/a/a/c;->Q:Landroid/support/constraint/a/a/c$a;

    sget-object v7, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-ne v0, v7, :cond_10

    .line 2042
    iget-object v0, v6, Landroid/support/constraint/a/a/c;->P:Landroid/support/constraint/a/a/c$a;

    sget-object v7, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-ne v0, v7, :cond_e

    .line 2043
    const/4 v0, 0x0

    const/4 v7, 0x0

    aput-boolean v7, p5, v0

    .line 2045
    :cond_e
    iget v0, v6, Landroid/support/constraint/a/a/c;->z:F

    const/4 v7, 0x0

    cmpg-float v0, v0, v7

    if-gtz v0, :cond_10

    .line 2046
    const/4 v0, 0x0

    const/4 v7, 0x0

    aput-boolean v7, p5, v0

    .line 2047
    add-int/lit8 v0, v5, 0x1

    iget-object v7, p0, Landroid/support/constraint/a/a/d;->aw:[Landroid/support/constraint/a/a/c;

    array-length v7, v7

    if-lt v0, v7, :cond_f

    .line 2048
    iget-object v0, p0, Landroid/support/constraint/a/a/d;->aw:[Landroid/support/constraint/a/a/c;

    iget-object v7, p0, Landroid/support/constraint/a/a/d;->aw:[Landroid/support/constraint/a/a/c;

    array-length v7, v7

    mul-int/lit8 v7, v7, 0x2

    invoke-static {v0, v7}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/support/constraint/a/a/c;

    iput-object v0, p0, Landroid/support/constraint/a/a/d;->aw:[Landroid/support/constraint/a/a/c;

    .line 2050
    :cond_f
    iget-object v7, p0, Landroid/support/constraint/a/a/d;->aw:[Landroid/support/constraint/a/a/c;

    add-int/lit8 v0, v5, 0x1

    aput-object v6, v7, v5

    move v5, v0

    .line 2053
    :cond_10
    iget-object v0, v6, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v0, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-nez v0, :cond_16

    .line 2065
    :cond_11
    :goto_a
    iget-object v0, v6, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v0, :cond_12

    iget-object v0, v6, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    if-eq v0, p0, :cond_12

    .line 2066
    const/4 v1, 0x0

    .line 2068
    :cond_12
    iget-object v0, p3, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v0, :cond_13

    iget-object v0, v4, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-nez v0, :cond_14

    .line 2069
    :cond_13
    const/4 v0, 0x1

    const/4 v6, 0x1

    aput-boolean v6, p5, v0

    .line 2073
    :cond_14
    iput-boolean v1, p3, Landroid/support/constraint/a/a/c;->ae:Z

    .line 2074
    const/4 v0, 0x0

    iput-object v0, v4, Landroid/support/constraint/a/a/c;->ai:Landroid/support/constraint/a/a/c;

    .line 2075
    const/4 v0, 0x0

    aput-object p3, p2, v0

    .line 2076
    const/4 v0, 0x2

    aput-object v3, p2, v0

    .line 2077
    const/4 v0, 0x1

    aput-object v4, p2, v0

    .line 2078
    const/4 v0, 0x3

    aput-object v2, p2, v0

    goto/16 :goto_5

    .line 2038
    :cond_15
    iget-object v3, v6, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v3, v3, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    iget-object v7, v6, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    const/4 v8, 0x0

    const/4 v9, 0x5

    invoke-virtual {p1, v3, v7, v8, v9}, Landroid/support/constraint/a/e;->c(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)Landroid/support/constraint/a/b;

    .line 2039
    iget-object v3, v6, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v3, v3, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    iget-object v7, v6, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    const/4 v8, 0x0

    const/4 v9, 0x5

    invoke-virtual {p1, v3, v7, v8, v9}, Landroid/support/constraint/a/e;->c(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)Landroid/support/constraint/a/b;

    move-object v3, v2

    move-object v2, v0

    goto/16 :goto_9

    .line 2056
    :cond_16
    iget-object v0, v6, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v0, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    if-ne v0, v6, :cond_11

    .line 2059
    iget-object v0, v6, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    if-eq v0, v6, :cond_11

    .line 2062
    iget-object v0, v6, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    move-object v4, v0

    move-object v6, v0

    move-object v0, v2

    move-object v2, v3

    .line 2063
    goto/16 :goto_7

    :cond_17
    move-object v3, v2

    goto/16 :goto_8

    :cond_18
    move-object v3, v2

    move-object v2, v0

    goto :goto_a

    :cond_19
    move v1, v0

    goto/16 :goto_6

    :cond_1a
    move-object v3, v2

    goto/16 :goto_2

    :cond_1b
    move-object v3, v2

    move-object v2, v0

    goto/16 :goto_4

    :cond_1c
    move v1, v0

    goto/16 :goto_0
.end method

.method private a(Landroid/support/constraint/a/e;)Z
    .locals 14

    .prologue
    const/4 v13, -0x1

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 263
    iget-object v0, p0, Landroid/support/constraint/a/a/d;->as:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v10

    move v1, v5

    .line 268
    :goto_0
    if-ge v1, v10, :cond_13

    .line 269
    iget-object v0, p0, Landroid/support/constraint/a/a/d;->as:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/a/a/c;

    .line 271
    iput v13, v0, Landroid/support/constraint/a/a/c;->a:I

    .line 272
    iput v13, v0, Landroid/support/constraint/a/a/c;->b:I

    .line 273
    iget-object v2, v0, Landroid/support/constraint/a/a/c;->P:Landroid/support/constraint/a/a/c$a;

    sget-object v4, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-eq v2, v4, :cond_0

    iget-object v2, v0, Landroid/support/constraint/a/a/c;->Q:Landroid/support/constraint/a/a/c$a;

    sget-object v4, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-ne v2, v4, :cond_1

    .line 275
    :cond_0
    iput v3, v0, Landroid/support/constraint/a/a/c;->a:I

    .line 276
    iput v3, v0, Landroid/support/constraint/a/a/c;->b:I

    .line 278
    :cond_1
    instance-of v2, v0, Landroid/support/constraint/a/a/a;

    if-eqz v2, :cond_2

    .line 279
    iput v3, v0, Landroid/support/constraint/a/a/c;->a:I

    .line 280
    iput v3, v0, Landroid/support/constraint/a/a/c;->b:I

    .line 268
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 323
    :cond_3
    if-nez v1, :cond_9

    if-nez v2, :cond_9

    move v0, v3

    :goto_1
    move v8, v2

    move v9, v1

    move v4, v0

    move v0, v7

    .line 283
    :goto_2
    if-nez v4, :cond_a

    .line 288
    add-int/lit8 v7, v0, 0x1

    move v6, v5

    move v2, v5

    move v1, v5

    .line 292
    :goto_3
    if-ge v6, v10, :cond_3

    .line 293
    iget-object v0, p0, Landroid/support/constraint/a/a/d;->as:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/a/a/c;

    .line 294
    iget v11, v0, Landroid/support/constraint/a/a/c;->a:I

    if-ne v11, v13, :cond_4

    .line 295
    iget-object v11, p0, Landroid/support/constraint/a/a/d;->P:Landroid/support/constraint/a/a/c$a;

    sget-object v12, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    if-ne v11, v12, :cond_7

    .line 296
    iput v3, v0, Landroid/support/constraint/a/a/c;->a:I

    .line 301
    :cond_4
    :goto_4
    iget v11, v0, Landroid/support/constraint/a/a/c;->b:I

    if-ne v11, v13, :cond_5

    .line 302
    iget-object v11, p0, Landroid/support/constraint/a/a/d;->Q:Landroid/support/constraint/a/a/c$a;

    sget-object v12, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    if-ne v11, v12, :cond_8

    .line 303
    iput v3, v0, Landroid/support/constraint/a/a/c;->b:I

    .line 313
    :cond_5
    :goto_5
    iget v11, v0, Landroid/support/constraint/a/a/c;->b:I

    if-ne v11, v13, :cond_6

    .line 314
    add-int/lit8 v1, v1, 0x1

    .line 316
    :cond_6
    iget v0, v0, Landroid/support/constraint/a/a/c;->a:I

    if-ne v0, v13, :cond_12

    .line 317
    add-int/lit8 v0, v2, 0x1

    .line 292
    :goto_6
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move v2, v0

    goto :goto_3

    .line 298
    :cond_7
    invoke-static {p0, p1, v0}, Landroid/support/constraint/a/a/g;->b(Landroid/support/constraint/a/a/d;Landroid/support/constraint/a/e;Landroid/support/constraint/a/a/c;)V

    goto :goto_4

    .line 305
    :cond_8
    invoke-static {p0, p1, v0}, Landroid/support/constraint/a/a/g;->c(Landroid/support/constraint/a/a/d;Landroid/support/constraint/a/e;Landroid/support/constraint/a/a/c;)V

    goto :goto_5

    .line 325
    :cond_9
    if-ne v9, v1, :cond_11

    if-ne v8, v2, :cond_11

    move v0, v3

    .line 326
    goto :goto_1

    :cond_a
    move v4, v5

    move v2, v5

    move v1, v5

    .line 335
    :goto_7
    if-ge v4, v10, :cond_e

    .line 336
    iget-object v0, p0, Landroid/support/constraint/a/a/d;->as:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/a/a/c;

    .line 337
    iget v6, v0, Landroid/support/constraint/a/a/c;->a:I

    if-eq v6, v3, :cond_b

    iget v6, v0, Landroid/support/constraint/a/a/c;->a:I

    if-ne v6, v13, :cond_c

    .line 339
    :cond_b
    add-int/lit8 v1, v1, 0x1

    .line 341
    :cond_c
    iget v6, v0, Landroid/support/constraint/a/a/c;->b:I

    if-eq v6, v3, :cond_d

    iget v0, v0, Landroid/support/constraint/a/a/c;->b:I

    if-ne v0, v13, :cond_10

    .line 343
    :cond_d
    add-int/lit8 v0, v2, 0x1

    .line 335
    :goto_8
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v0

    goto :goto_7

    .line 346
    :cond_e
    if-nez v1, :cond_f

    if-nez v2, :cond_f

    .line 349
    :goto_9
    return v3

    :cond_f
    move v3, v5

    goto :goto_9

    :cond_10
    move v0, v2

    goto :goto_8

    :cond_11
    move v0, v4

    goto :goto_1

    :cond_12
    move v0, v2

    goto :goto_6

    :cond_13
    move v0, v5

    move v8, v5

    move v9, v5

    move v4, v5

    goto :goto_2
.end method

.method private b(Landroid/support/constraint/a/e;)V
    .locals 23

    .prologue
    .line 361
    const/4 v4, 0x0

    move/from16 v16, v4

    :goto_0
    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/constraint/a/a/d;->au:I

    move/from16 v0, v16

    if-ge v0, v4, :cond_31

    .line 362
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/d;->ay:[Landroid/support/constraint/a/a/c;

    aget-object v21, v4, v16

    .line 363
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/d;->aB:[Landroid/support/constraint/a/a/c;

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/d;->ay:[Landroid/support/constraint/a/a/c;

    aget-object v7, v4, v16

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Landroid/support/constraint/a/a/d;->aA:[Z

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    invoke-direct/range {v4 .. v9}, Landroid/support/constraint/a/a/d;->a(Landroid/support/constraint/a/e;[Landroid/support/constraint/a/a/c;Landroid/support/constraint/a/a/c;I[Z)I

    move-result v18

    .line 365
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/d;->aB:[Landroid/support/constraint/a/a/c;

    const/4 v5, 0x2

    aget-object v15, v4, v5

    .line 366
    if-nez v15, :cond_1

    .line 361
    :cond_0
    :goto_1
    add-int/lit8 v4, v16, 0x1

    move/from16 v16, v4

    goto :goto_0

    .line 370
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/d;->aA:[Z

    const/4 v5, 0x1

    aget-boolean v4, v4, v5

    if-eqz v4, :cond_2

    .line 371
    invoke-virtual/range {v21 .. v21}, Landroid/support/constraint/a/a/c;->n()I

    move-result v4

    .line 372
    :goto_2
    if-eqz v15, :cond_0

    .line 373
    iget-object v5, v15, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;I)V

    .line 374
    iget-object v5, v15, Landroid/support/constraint/a/a/c;->ah:Landroid/support/constraint/a/a/c;

    .line 375
    iget-object v6, v15, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    invoke-virtual {v6}, Landroid/support/constraint/a/a/b;->d()I

    move-result v6

    invoke-virtual {v15}, Landroid/support/constraint/a/a/c;->h()I

    move-result v7

    add-int/2addr v6, v7

    iget-object v7, v15, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    invoke-virtual {v7}, Landroid/support/constraint/a/a/b;->d()I

    move-result v7

    add-int/2addr v6, v7

    add-int/2addr v4, v6

    move-object v15, v5

    .line 377
    goto :goto_2

    .line 380
    :cond_2
    move-object/from16 v0, v21

    iget v4, v0, Landroid/support/constraint/a/a/c;->ab:I

    if-nez v4, :cond_4

    const/4 v4, 0x1

    move v13, v4

    .line 381
    :goto_3
    move-object/from16 v0, v21

    iget v4, v0, Landroid/support/constraint/a/a/c;->ab:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_5

    const/4 v4, 0x1

    move v14, v4

    .line 383
    :goto_4
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/d;->P:Landroid/support/constraint/a/a/c$a;

    sget-object v5, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    if-ne v4, v5, :cond_6

    const/4 v4, 0x1

    .line 384
    :goto_5
    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/constraint/a/a/d;->az:I

    const/4 v6, 0x2

    if-eq v5, v6, :cond_3

    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/constraint/a/a/d;->az:I

    const/16 v6, 0x8

    if-ne v5, v6, :cond_7

    :cond_3
    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/constraint/a/a/d;->aA:[Z

    const/4 v6, 0x0

    aget-boolean v5, v5, v6

    if-eqz v5, :cond_7

    move-object/from16 v0, v21

    iget-boolean v5, v0, Landroid/support/constraint/a/a/c;->ad:Z

    if-eqz v5, :cond_7

    if-nez v14, :cond_7

    if-nez v4, :cond_7

    move-object/from16 v0, v21

    iget v4, v0, Landroid/support/constraint/a/a/c;->ab:I

    if-nez v4, :cond_7

    .line 388
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    move-object/from16 v3, v21

    invoke-static {v0, v1, v2, v3}, Landroid/support/constraint/a/a/g;->a(Landroid/support/constraint/a/a/d;Landroid/support/constraint/a/e;ILandroid/support/constraint/a/a/c;)V

    goto/16 :goto_1

    .line 380
    :cond_4
    const/4 v4, 0x0

    move v13, v4

    goto :goto_3

    .line 381
    :cond_5
    const/4 v4, 0x0

    move v14, v4

    goto :goto_4

    .line 383
    :cond_6
    const/4 v4, 0x0

    goto :goto_5

    .line 390
    :cond_7
    if-eqz v18, :cond_8

    if-eqz v14, :cond_1c

    .line 391
    :cond_8
    const/4 v5, 0x0

    .line 392
    const/4 v8, 0x0

    .line 396
    const/4 v4, 0x0

    move-object v9, v5

    move-object/from16 v20, v15

    .line 398
    :goto_6
    if-eqz v20, :cond_19

    .line 399
    move-object/from16 v0, v20

    iget-object v5, v0, Landroid/support/constraint/a/a/c;->ah:Landroid/support/constraint/a/a/c;

    .line 400
    if-nez v5, :cond_36

    .line 401
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/d;->aB:[Landroid/support/constraint/a/a/c;

    const/4 v6, 0x1

    aget-object v6, v4, v6

    .line 402
    const/4 v4, 0x1

    move/from16 v18, v4

    move-object/from16 v19, v6

    .line 404
    :goto_7
    if-eqz v14, :cond_d

    .line 405
    move-object/from16 v0, v20

    iget-object v7, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    .line 406
    invoke-virtual {v7}, Landroid/support/constraint/a/a/b;->d()I

    move-result v4

    .line 407
    if-eqz v9, :cond_35

    .line 408
    iget-object v6, v9, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    invoke-virtual {v6}, Landroid/support/constraint/a/a/b;->d()I

    move-result v6

    .line 409
    add-int/2addr v4, v6

    move v6, v4

    .line 411
    :goto_8
    const/4 v4, 0x1

    .line 412
    move-object/from16 v0, v20

    if-eq v15, v0, :cond_9

    .line 413
    const/4 v4, 0x3

    .line 415
    :cond_9
    iget-object v8, v7, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    iget-object v9, v7, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v9, v9, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v9, v6, v4}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 416
    move-object/from16 v0, v20

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->P:Landroid/support/constraint/a/a/c$a;

    sget-object v6, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-ne v4, v6, :cond_a

    .line 417
    move-object/from16 v0, v20

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    .line 418
    move-object/from16 v0, v20

    iget v6, v0, Landroid/support/constraint/a/a/c;->c:I

    const/4 v8, 0x1

    if-ne v6, v8, :cond_c

    .line 419
    move-object/from16 v0, v20

    iget v6, v0, Landroid/support/constraint/a/a/c;->e:I

    invoke-virtual/range {v20 .. v20}, Landroid/support/constraint/a/a/c;->h()I

    move-result v8

    invoke-static {v6, v8}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 420
    iget-object v4, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    const/4 v8, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7, v6, v8}, Landroid/support/constraint/a/e;->c(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)Landroid/support/constraint/a/b;

    :cond_a
    :goto_9
    move-object v4, v5

    .line 473
    :goto_a
    if-eqz v18, :cond_b

    const/4 v4, 0x0

    :cond_b
    move-object/from16 v8, v19

    move-object/from16 v9, v20

    move-object/from16 v20, v4

    move/from16 v4, v18

    goto :goto_6

    .line 423
    :cond_c
    iget-object v6, v7, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    iget-object v8, v7, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v8, v8, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    iget v9, v7, Landroid/support/constraint/a/a/b;->d:I

    const/4 v10, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v8, v9, v10}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 425
    iget-object v4, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    iget-object v6, v7, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    move-object/from16 v0, v20

    iget v7, v0, Landroid/support/constraint/a/a/c;->e:I

    const/4 v8, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6, v7, v8}, Landroid/support/constraint/a/e;->b(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    goto :goto_9

    .line 430
    :cond_d
    if-nez v13, :cond_f

    if-eqz v18, :cond_f

    if-eqz v9, :cond_f

    .line 431
    move-object/from16 v0, v20

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-nez v4, :cond_e

    .line 432
    move-object/from16 v0, v20

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    invoke-virtual/range {v20 .. v20}, Landroid/support/constraint/a/a/c;->q()I

    move-result v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;I)V

    move-object v4, v5

    goto :goto_a

    .line 434
    :cond_e
    move-object/from16 v0, v20

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v4

    .line 435
    move-object/from16 v0, v20

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    move-object/from16 v0, v19

    iget-object v7, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    neg-int v4, v4

    const/4 v8, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7, v4, v8}, Landroid/support/constraint/a/e;->c(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)Landroid/support/constraint/a/b;

    move-object v4, v5

    .line 436
    goto :goto_a

    .line 437
    :cond_f
    if-nez v13, :cond_11

    if-nez v18, :cond_11

    if-nez v9, :cond_11

    .line 438
    move-object/from16 v0, v20

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-nez v4, :cond_10

    .line 439
    move-object/from16 v0, v20

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    invoke-virtual/range {v20 .. v20}, Landroid/support/constraint/a/a/c;->n()I

    move-result v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;I)V

    move-object v4, v5

    goto/16 :goto_a

    .line 441
    :cond_10
    move-object/from16 v0, v20

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v4

    .line 442
    move-object/from16 v0, v20

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    move-object/from16 v0, v21

    iget-object v7, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    const/4 v8, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7, v4, v8}, Landroid/support/constraint/a/e;->c(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)Landroid/support/constraint/a/b;

    move-object v4, v5

    .line 443
    goto/16 :goto_a

    .line 446
    :cond_11
    move-object/from16 v0, v20

    iget-object v8, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    .line 447
    move-object/from16 v0, v20

    iget-object v10, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    .line 448
    invoke-virtual {v8}, Landroid/support/constraint/a/a/b;->d()I

    move-result v7

    .line 449
    invoke-virtual {v10}, Landroid/support/constraint/a/a/b;->d()I

    move-result v11

    .line 450
    iget-object v4, v8, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    iget-object v6, v8, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6, v7, v12}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 451
    iget-object v4, v10, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    iget-object v6, v10, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    neg-int v12, v11

    const/16 v17, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v4, v6, v12, v1}, Landroid/support/constraint/a/e;->b(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 452
    iget-object v4, v8, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v4, :cond_15

    iget-object v4, v8, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v6, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 453
    :goto_b
    if-nez v9, :cond_12

    .line 455
    move-object/from16 v0, v21

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v4, :cond_16

    move-object/from16 v0, v21

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    :goto_c
    move-object v6, v4

    .line 457
    :cond_12
    if-nez v5, :cond_34

    .line 458
    move-object/from16 v0, v19

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v4, :cond_17

    move-object/from16 v0, v19

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    :goto_d
    move-object/from16 v17, v4

    .line 460
    :goto_e
    if-eqz v17, :cond_14

    .line 461
    move-object/from16 v0, v17

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v9, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 462
    if-eqz v18, :cond_13

    .line 463
    move-object/from16 v0, v19

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v4, :cond_18

    move-object/from16 v0, v19

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    :goto_f
    move-object v9, v4

    .line 465
    :cond_13
    if-eqz v6, :cond_14

    if-eqz v9, :cond_14

    .line 466
    iget-object v5, v8, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    const/high16 v8, 0x3f000000    # 0.5f

    iget-object v10, v10, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    const/4 v12, 0x4

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v12}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;IFLandroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    :cond_14
    move-object/from16 v4, v17

    goto/16 :goto_a

    .line 452
    :cond_15
    const/4 v6, 0x0

    goto :goto_b

    .line 455
    :cond_16
    const/4 v4, 0x0

    goto :goto_c

    .line 458
    :cond_17
    const/4 v4, 0x0

    goto :goto_d

    .line 463
    :cond_18
    const/4 v4, 0x0

    goto :goto_f

    .line 475
    :cond_19
    if-eqz v14, :cond_0

    .line 476
    iget-object v4, v15, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    .line 477
    iget-object v10, v8, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    .line 478
    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v7

    .line 479
    invoke-virtual {v10}, Landroid/support/constraint/a/a/b;->d()I

    move-result v11

    .line 480
    move-object/from16 v0, v21

    iget-object v5, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v5, :cond_1a

    move-object/from16 v0, v21

    iget-object v5, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v6, v5, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 481
    :goto_10
    iget-object v5, v8, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v5, :cond_1b

    iget-object v5, v8, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v9, v5, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 482
    :goto_11
    if-eqz v6, :cond_0

    if-eqz v9, :cond_0

    .line 483
    iget-object v5, v10, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    neg-int v8, v11

    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v9, v8, v12}, Landroid/support/constraint/a/e;->b(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 484
    iget-object v5, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    move-object/from16 v0, v21

    iget v8, v0, Landroid/support/constraint/a/a/c;->N:F

    iget-object v10, v10, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    const/4 v12, 0x4

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v12}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;IFLandroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    goto/16 :goto_1

    .line 480
    :cond_1a
    const/4 v6, 0x0

    goto :goto_10

    .line 481
    :cond_1b
    const/4 v9, 0x0

    goto :goto_11

    .line 489
    :cond_1c
    const/4 v4, 0x0

    .line 490
    const/4 v6, 0x0

    move-object v5, v4

    move-object v8, v15

    .line 491
    :goto_12
    if-eqz v8, :cond_23

    .line 492
    iget-object v4, v8, Landroid/support/constraint/a/a/c;->P:Landroid/support/constraint/a/a/c$a;

    sget-object v7, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-eq v4, v7, :cond_21

    .line 493
    iget-object v4, v8, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v4

    .line 494
    if-eqz v5, :cond_1d

    .line 495
    iget-object v5, v5, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    invoke-virtual {v5}, Landroid/support/constraint/a/a/b;->d()I

    move-result v5

    add-int/2addr v4, v5

    .line 497
    :cond_1d
    const/4 v5, 0x3

    .line 498
    iget-object v7, v8, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v7, v7, Landroid/support/constraint/a/a/c;->P:Landroid/support/constraint/a/a/c$a;

    sget-object v9, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-ne v7, v9, :cond_1e

    .line 499
    const/4 v5, 0x2

    .line 501
    :cond_1e
    iget-object v7, v8, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    iget-object v9, v8, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v9, v9, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v9, v9, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v9, v4, v5}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 502
    iget-object v4, v8, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v4

    .line 503
    iget-object v5, v8, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v5, v5, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v5, :cond_1f

    iget-object v5, v8, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v5, v5, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    if-ne v5, v8, :cond_1f

    .line 504
    iget-object v5, v8, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v5, v5, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    invoke-virtual {v5}, Landroid/support/constraint/a/a/b;->d()I

    move-result v5

    add-int/2addr v4, v5

    .line 506
    :cond_1f
    const/4 v5, 0x3

    .line 507
    iget-object v7, v8, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v7, v7, Landroid/support/constraint/a/a/c;->P:Landroid/support/constraint/a/a/c$a;

    sget-object v9, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-ne v7, v9, :cond_20

    .line 508
    const/4 v5, 0x2

    .line 510
    :cond_20
    iget-object v7, v8, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 511
    move-object/from16 v0, p0

    iget-object v9, v0, Landroid/support/constraint/a/a/d;->aB:[Landroid/support/constraint/a/a/c;

    const/4 v10, 0x3

    aget-object v9, v9, v10

    if-ne v8, v9, :cond_33

    .line 512
    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/constraint/a/a/d;->aB:[Landroid/support/constraint/a/a/c;

    const/4 v7, 0x1

    aget-object v5, v5, v7

    iget-object v5, v5, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 513
    const/4 v7, 0x3

    .line 515
    :goto_13
    iget-object v9, v8, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v9, v9, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    neg-int v4, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v5, v4, v7}, Landroid/support/constraint/a/e;->b(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 529
    :goto_14
    iget-object v4, v8, Landroid/support/constraint/a/a/c;->ah:Landroid/support/constraint/a/a/c;

    move-object v5, v8

    move-object v8, v4

    goto/16 :goto_12

    .line 517
    :cond_21
    iget v4, v8, Landroid/support/constraint/a/a/c;->af:F

    add-float/2addr v6, v4

    .line 518
    const/4 v4, 0x0

    .line 519
    iget-object v5, v8, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v5, :cond_22

    .line 520
    iget-object v4, v8, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v4

    .line 521
    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/constraint/a/a/d;->aB:[Landroid/support/constraint/a/a/c;

    const/4 v7, 0x3

    aget-object v5, v5, v7

    if-eq v8, v5, :cond_22

    .line 522
    iget-object v5, v8, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v5, v5, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    invoke-virtual {v5}, Landroid/support/constraint/a/a/b;->d()I

    move-result v5

    add-int/2addr v4, v5

    .line 525
    :cond_22
    iget-object v5, v8, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    iget-object v7, v8, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    const/4 v9, 0x0

    const/4 v10, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v7, v9, v10}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 526
    iget-object v5, v8, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    iget-object v7, v8, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    neg-int v4, v4

    const/4 v9, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v7, v4, v9}, Landroid/support/constraint/a/e;->b(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    goto :goto_14

    .line 531
    :cond_23
    const/4 v4, 0x1

    move/from16 v0, v18

    if-ne v0, v4, :cond_28

    .line 532
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/d;->aw:[Landroid/support/constraint/a/a/c;

    const/4 v5, 0x0

    aget-object v7, v4, v5

    .line 533
    iget-object v4, v7, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v4

    .line 534
    iget-object v5, v7, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v5, :cond_24

    .line 535
    iget-object v5, v7, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    invoke-virtual {v5}, Landroid/support/constraint/a/a/b;->d()I

    move-result v5

    add-int/2addr v4, v5

    .line 537
    :cond_24
    iget-object v5, v7, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    invoke-virtual {v5}, Landroid/support/constraint/a/a/b;->d()I

    move-result v5

    .line 538
    iget-object v6, v7, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v6, :cond_25

    .line 539
    iget-object v6, v7, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    invoke-virtual {v6}, Landroid/support/constraint/a/a/b;->d()I

    move-result v6

    add-int/2addr v5, v6

    .line 541
    :cond_25
    move-object/from16 v0, v21

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 542
    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/constraint/a/a/d;->aB:[Landroid/support/constraint/a/a/c;

    const/4 v9, 0x3

    aget-object v8, v8, v9

    if-ne v7, v8, :cond_26

    .line 543
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/d;->aB:[Landroid/support/constraint/a/a/c;

    const/4 v8, 0x1

    aget-object v6, v6, v8

    iget-object v6, v6, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 546
    :cond_26
    iget v8, v7, Landroid/support/constraint/a/a/c;->c:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_27

    .line 547
    move-object/from16 v0, v21

    iget-object v7, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    move-object/from16 v0, v21

    iget-object v8, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v8, v8, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v8, v8, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    const/4 v9, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v8, v4, v9}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 548
    move-object/from16 v0, v21

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    neg-int v5, v5

    const/4 v7, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6, v5, v7}, Landroid/support/constraint/a/e;->b(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 549
    move-object/from16 v0, v21

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    move-object/from16 v0, v21

    iget-object v5, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    invoke-virtual/range {v21 .. v21}, Landroid/support/constraint/a/a/c;->h()I

    move-result v6

    const/4 v7, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/support/constraint/a/e;->c(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)Landroid/support/constraint/a/b;

    goto/16 :goto_1

    .line 551
    :cond_27
    iget-object v8, v7, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v8, v8, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    iget-object v9, v7, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v9, v9, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v9, v9, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    const/4 v10, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v9, v4, v10}, Landroid/support/constraint/a/e;->c(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)Landroid/support/constraint/a/b;

    .line 552
    iget-object v4, v7, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    neg-int v5, v5

    const/4 v7, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6, v5, v7}, Landroid/support/constraint/a/e;->c(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)Landroid/support/constraint/a/b;

    goto/16 :goto_1

    .line 555
    :cond_28
    const/4 v4, 0x0

    move/from16 v17, v4

    :goto_15
    add-int/lit8 v4, v18, -0x1

    move/from16 v0, v17

    if-ge v0, v4, :cond_0

    .line 556
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/d;->aw:[Landroid/support/constraint/a/a/c;

    aget-object v11, v4, v17

    .line 557
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/d;->aw:[Landroid/support/constraint/a/a/c;

    add-int/lit8 v5, v17, 0x1

    aget-object v15, v4, v5

    .line 558
    iget-object v4, v11, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v8, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 559
    iget-object v4, v11, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v10, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 560
    iget-object v4, v15, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v12, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 561
    iget-object v4, v15, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v14, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 562
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/d;->aB:[Landroid/support/constraint/a/a/c;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    if-ne v15, v4, :cond_29

    .line 563
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/d;->aB:[Landroid/support/constraint/a/a/c;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    iget-object v4, v4, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v14, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 565
    :cond_29
    iget-object v4, v11, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v4

    .line 566
    iget-object v5, v11, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v5, :cond_2a

    iget-object v5, v11, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v5, v5, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v5, :cond_2a

    iget-object v5, v11, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v5, v5, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    if-ne v5, v11, :cond_2a

    .line 568
    iget-object v5, v11, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v5, v5, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    invoke-virtual {v5}, Landroid/support/constraint/a/a/b;->d()I

    move-result v5

    add-int/2addr v4, v5

    .line 570
    :cond_2a
    iget-object v5, v11, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    const/4 v7, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v5, v4, v7}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 571
    iget-object v4, v11, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v5

    .line 572
    iget-object v4, v11, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v4, :cond_32

    iget-object v4, v11, Landroid/support/constraint/a/a/c;->ah:Landroid/support/constraint/a/a/c;

    if-eqz v4, :cond_32

    .line 573
    iget-object v4, v11, Landroid/support/constraint/a/a/c;->ah:Landroid/support/constraint/a/a/c;

    iget-object v4, v4, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v4, :cond_30

    iget-object v4, v11, Landroid/support/constraint/a/a/c;->ah:Landroid/support/constraint/a/a/c;

    iget-object v4, v4, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v4

    :goto_16
    add-int/2addr v4, v5

    .line 575
    :goto_17
    iget-object v5, v11, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    neg-int v4, v4

    const/4 v7, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v5, v4, v7}, Landroid/support/constraint/a/e;->b(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 576
    add-int/lit8 v4, v17, 0x1

    add-int/lit8 v5, v18, -0x1

    if-ne v4, v5, :cond_2e

    .line 578
    iget-object v4, v15, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v4

    .line 579
    iget-object v5, v15, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v5, :cond_2b

    iget-object v5, v15, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v5, v5, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v5, :cond_2b

    iget-object v5, v15, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v5, v5, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    if-ne v5, v15, :cond_2b

    .line 581
    iget-object v5, v15, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v5, v5, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    invoke-virtual {v5}, Landroid/support/constraint/a/a/b;->d()I

    move-result v5

    add-int/2addr v4, v5

    .line 583
    :cond_2b
    iget-object v5, v15, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    const/4 v7, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v5, v4, v7}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 584
    iget-object v4, v15, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    .line 585
    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/constraint/a/a/d;->aB:[Landroid/support/constraint/a/a/c;

    const/4 v7, 0x3

    aget-object v5, v5, v7

    if-ne v15, v5, :cond_2c

    .line 586
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/d;->aB:[Landroid/support/constraint/a/a/c;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    iget-object v4, v4, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    .line 588
    :cond_2c
    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v5

    .line 589
    iget-object v7, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v7, :cond_2d

    iget-object v7, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v7, v7, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v7, :cond_2d

    iget-object v7, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v7, v7, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    if-ne v7, v15, :cond_2d

    .line 591
    iget-object v7, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v7, v7, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    invoke-virtual {v7}, Landroid/support/constraint/a/a/b;->d()I

    move-result v7

    add-int/2addr v5, v7

    .line 593
    :cond_2d
    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    neg-int v5, v5

    const/4 v7, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v4, v5, v7}, Landroid/support/constraint/a/e;->b(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 596
    :cond_2e
    move-object/from16 v0, v21

    iget v4, v0, Landroid/support/constraint/a/a/c;->f:I

    if-lez v4, :cond_2f

    .line 597
    move-object/from16 v0, v21

    iget v4, v0, Landroid/support/constraint/a/a/c;->f:I

    const/4 v5, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v8, v4, v5}, Landroid/support/constraint/a/e;->b(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 600
    :cond_2f
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->b()Landroid/support/constraint/a/b;

    move-result-object v4

    .line 601
    iget v5, v11, Landroid/support/constraint/a/a/c;->af:F

    iget v7, v15, Landroid/support/constraint/a/a/c;->af:F

    iget-object v9, v11, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    .line 603
    invoke-virtual {v9}, Landroid/support/constraint/a/a/b;->d()I

    move-result v9

    iget-object v11, v11, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    .line 604
    invoke-virtual {v11}, Landroid/support/constraint/a/a/b;->d()I

    move-result v11

    iget-object v13, v15, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    .line 605
    invoke-virtual {v13}, Landroid/support/constraint/a/a/b;->d()I

    move-result v13

    iget-object v15, v15, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    .line 606
    invoke-virtual {v15}, Landroid/support/constraint/a/a/b;->d()I

    move-result v15

    .line 601
    invoke-virtual/range {v4 .. v15}, Landroid/support/constraint/a/b;->a(FFFLandroid/support/constraint/a/g;ILandroid/support/constraint/a/g;ILandroid/support/constraint/a/g;ILandroid/support/constraint/a/g;I)Landroid/support/constraint/a/b;

    .line 607
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    .line 555
    add-int/lit8 v4, v17, 0x1

    move/from16 v17, v4

    goto/16 :goto_15

    .line 573
    :cond_30
    const/4 v4, 0x0

    goto/16 :goto_16

    .line 613
    :cond_31
    return-void

    :cond_32
    move v4, v5

    goto/16 :goto_17

    :cond_33
    move-object/from16 v22, v7

    move v7, v5

    move-object/from16 v5, v22

    goto/16 :goto_13

    :cond_34
    move-object/from16 v17, v5

    goto/16 :goto_e

    :cond_35
    move v6, v4

    goto/16 :goto_8

    :cond_36
    move/from16 v18, v4

    move-object/from16 v19, v8

    goto/16 :goto_7
.end method

.method private c(Landroid/support/constraint/a/e;)V
    .locals 23

    .prologue
    .line 622
    const/4 v4, 0x0

    move/from16 v16, v4

    :goto_0
    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/constraint/a/a/d;->av:I

    move/from16 v0, v16

    if-ge v0, v4, :cond_35

    .line 623
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/d;->ax:[Landroid/support/constraint/a/a/c;

    aget-object v21, v4, v16

    .line 624
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/d;->aB:[Landroid/support/constraint/a/a/c;

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/d;->ax:[Landroid/support/constraint/a/a/c;

    aget-object v7, v4, v16

    const/4 v8, 0x1

    move-object/from16 v0, p0

    iget-object v9, v0, Landroid/support/constraint/a/a/d;->aA:[Z

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    invoke-direct/range {v4 .. v9}, Landroid/support/constraint/a/a/d;->a(Landroid/support/constraint/a/e;[Landroid/support/constraint/a/a/c;Landroid/support/constraint/a/a/c;I[Z)I

    move-result v18

    .line 626
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/d;->aB:[Landroid/support/constraint/a/a/c;

    const/4 v5, 0x2

    aget-object v15, v4, v5

    .line 627
    if-nez v15, :cond_1

    .line 622
    :cond_0
    :goto_1
    add-int/lit8 v4, v16, 0x1

    move/from16 v16, v4

    goto :goto_0

    .line 631
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/d;->aA:[Z

    const/4 v5, 0x1

    aget-boolean v4, v4, v5

    if-eqz v4, :cond_2

    .line 632
    invoke-virtual/range {v21 .. v21}, Landroid/support/constraint/a/a/c;->o()I

    move-result v4

    .line 633
    :goto_2
    if-eqz v15, :cond_0

    .line 634
    iget-object v5, v15, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;I)V

    .line 635
    iget-object v5, v15, Landroid/support/constraint/a/a/c;->ai:Landroid/support/constraint/a/a/c;

    .line 636
    iget-object v6, v15, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    invoke-virtual {v6}, Landroid/support/constraint/a/a/b;->d()I

    move-result v6

    invoke-virtual {v15}, Landroid/support/constraint/a/a/c;->l()I

    move-result v7

    add-int/2addr v6, v7

    iget-object v7, v15, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    invoke-virtual {v7}, Landroid/support/constraint/a/a/b;->d()I

    move-result v7

    add-int/2addr v6, v7

    add-int/2addr v4, v6

    move-object v15, v5

    .line 638
    goto :goto_2

    .line 641
    :cond_2
    move-object/from16 v0, v21

    iget v4, v0, Landroid/support/constraint/a/a/c;->ac:I

    if-nez v4, :cond_4

    const/4 v4, 0x1

    move v13, v4

    .line 642
    :goto_3
    move-object/from16 v0, v21

    iget v4, v0, Landroid/support/constraint/a/a/c;->ac:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_5

    const/4 v4, 0x1

    move v14, v4

    .line 644
    :goto_4
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/d;->Q:Landroid/support/constraint/a/a/c$a;

    sget-object v5, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    if-ne v4, v5, :cond_6

    const/4 v4, 0x1

    .line 645
    :goto_5
    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/constraint/a/a/d;->az:I

    const/4 v6, 0x2

    if-eq v5, v6, :cond_3

    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/constraint/a/a/d;->az:I

    const/16 v6, 0x8

    if-ne v5, v6, :cond_7

    :cond_3
    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/constraint/a/a/d;->aA:[Z

    const/4 v6, 0x0

    aget-boolean v5, v5, v6

    if-eqz v5, :cond_7

    move-object/from16 v0, v21

    iget-boolean v5, v0, Landroid/support/constraint/a/a/c;->ae:Z

    if-eqz v5, :cond_7

    if-nez v14, :cond_7

    if-nez v4, :cond_7

    move-object/from16 v0, v21

    iget v4, v0, Landroid/support/constraint/a/a/c;->ac:I

    if-nez v4, :cond_7

    .line 649
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    move-object/from16 v3, v21

    invoke-static {v0, v1, v2, v3}, Landroid/support/constraint/a/a/g;->b(Landroid/support/constraint/a/a/d;Landroid/support/constraint/a/e;ILandroid/support/constraint/a/a/c;)V

    goto/16 :goto_1

    .line 641
    :cond_4
    const/4 v4, 0x0

    move v13, v4

    goto :goto_3

    .line 642
    :cond_5
    const/4 v4, 0x0

    move v14, v4

    goto :goto_4

    .line 644
    :cond_6
    const/4 v4, 0x0

    goto :goto_5

    .line 651
    :cond_7
    if-eqz v18, :cond_8

    if-eqz v14, :cond_20

    .line 652
    :cond_8
    const/4 v5, 0x0

    .line 653
    const/4 v8, 0x0

    .line 657
    const/4 v4, 0x0

    move-object v9, v5

    move-object/from16 v20, v15

    .line 659
    :goto_6
    if-eqz v20, :cond_1d

    .line 660
    move-object/from16 v0, v20

    iget-object v5, v0, Landroid/support/constraint/a/a/c;->ai:Landroid/support/constraint/a/a/c;

    .line 661
    if-nez v5, :cond_39

    .line 662
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/d;->aB:[Landroid/support/constraint/a/a/c;

    const/4 v6, 0x1

    aget-object v6, v4, v6

    .line 663
    const/4 v4, 0x1

    move/from16 v18, v4

    move-object/from16 v19, v6

    .line 665
    :goto_7
    if-eqz v14, :cond_11

    .line 666
    move-object/from16 v0, v20

    iget-object v10, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    .line 667
    invoke-virtual {v10}, Landroid/support/constraint/a/a/b;->d()I

    move-result v8

    .line 668
    if-eqz v9, :cond_9

    .line 669
    iget-object v4, v9, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v4

    .line 670
    add-int/2addr v8, v4

    .line 672
    :cond_9
    const/4 v4, 0x1

    .line 673
    move-object/from16 v0, v20

    if-eq v15, v0, :cond_a

    .line 674
    const/4 v4, 0x3

    .line 676
    :cond_a
    const/4 v7, 0x0

    .line 677
    const/4 v6, 0x0

    .line 678
    iget-object v9, v10, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v9, :cond_f

    .line 679
    iget-object v7, v10, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 680
    iget-object v6, v10, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 686
    :cond_b
    :goto_8
    if-eqz v7, :cond_c

    if-eqz v6, :cond_c

    .line 687
    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6, v8, v4}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 689
    :cond_c
    move-object/from16 v0, v20

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->Q:Landroid/support/constraint/a/a/c$a;

    sget-object v6, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-ne v4, v6, :cond_d

    .line 690
    move-object/from16 v0, v20

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    .line 691
    move-object/from16 v0, v20

    iget v6, v0, Landroid/support/constraint/a/a/c;->d:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_10

    .line 692
    move-object/from16 v0, v20

    iget v6, v0, Landroid/support/constraint/a/a/c;->h:I

    invoke-virtual/range {v20 .. v20}, Landroid/support/constraint/a/a/c;->l()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 693
    iget-object v4, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    iget-object v7, v10, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    const/4 v8, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7, v6, v8}, Landroid/support/constraint/a/e;->c(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)Landroid/support/constraint/a/b;

    :cond_d
    :goto_9
    move-object v4, v5

    .line 746
    :goto_a
    if-eqz v18, :cond_e

    const/4 v4, 0x0

    :cond_e
    move-object/from16 v8, v19

    move-object/from16 v9, v20

    move-object/from16 v20, v4

    move/from16 v4, v18

    goto :goto_6

    .line 681
    :cond_f
    move-object/from16 v0, v20

    iget-object v9, v0, Landroid/support/constraint/a/a/c;->q:Landroid/support/constraint/a/a/b;

    iget-object v9, v9, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v9, :cond_b

    .line 682
    move-object/from16 v0, v20

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->q:Landroid/support/constraint/a/a/b;

    iget-object v7, v6, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 683
    move-object/from16 v0, v20

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->q:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 684
    invoke-virtual {v10}, Landroid/support/constraint/a/a/b;->d()I

    move-result v9

    sub-int/2addr v8, v9

    goto :goto_8

    .line 696
    :cond_10
    iget-object v6, v10, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    iget-object v7, v10, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    iget v8, v10, Landroid/support/constraint/a/a/b;->d:I

    const/4 v9, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7, v8, v9}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 698
    iget-object v4, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    iget-object v6, v10, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    move-object/from16 v0, v20

    iget v7, v0, Landroid/support/constraint/a/a/c;->h:I

    const/4 v8, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6, v7, v8}, Landroid/support/constraint/a/e;->b(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    goto :goto_9

    .line 703
    :cond_11
    if-nez v13, :cond_13

    if-eqz v18, :cond_13

    if-eqz v9, :cond_13

    .line 704
    move-object/from16 v0, v20

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-nez v4, :cond_12

    .line 705
    move-object/from16 v0, v20

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    invoke-virtual/range {v20 .. v20}, Landroid/support/constraint/a/a/c;->p()I

    move-result v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;I)V

    move-object v4, v5

    goto :goto_a

    .line 707
    :cond_12
    move-object/from16 v0, v20

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v4

    .line 708
    move-object/from16 v0, v20

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    move-object/from16 v0, v19

    iget-object v7, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    neg-int v4, v4

    const/4 v8, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7, v4, v8}, Landroid/support/constraint/a/e;->c(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)Landroid/support/constraint/a/b;

    move-object v4, v5

    .line 709
    goto/16 :goto_a

    .line 710
    :cond_13
    if-nez v13, :cond_15

    if-nez v18, :cond_15

    if-nez v9, :cond_15

    .line 711
    move-object/from16 v0, v20

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-nez v4, :cond_14

    .line 712
    move-object/from16 v0, v20

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    invoke-virtual/range {v20 .. v20}, Landroid/support/constraint/a/a/c;->o()I

    move-result v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;I)V

    move-object v4, v5

    goto/16 :goto_a

    .line 714
    :cond_14
    move-object/from16 v0, v20

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v4

    .line 715
    move-object/from16 v0, v20

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    move-object/from16 v0, v21

    iget-object v7, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    const/4 v8, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7, v4, v8}, Landroid/support/constraint/a/e;->c(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)Landroid/support/constraint/a/b;

    move-object v4, v5

    .line 716
    goto/16 :goto_a

    .line 719
    :cond_15
    move-object/from16 v0, v20

    iget-object v8, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    .line 720
    move-object/from16 v0, v20

    iget-object v10, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    .line 721
    invoke-virtual {v8}, Landroid/support/constraint/a/a/b;->d()I

    move-result v7

    .line 722
    invoke-virtual {v10}, Landroid/support/constraint/a/a/b;->d()I

    move-result v11

    .line 723
    iget-object v4, v8, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    iget-object v6, v8, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6, v7, v12}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 724
    iget-object v4, v10, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    iget-object v6, v10, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    neg-int v12, v11

    const/16 v17, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v4, v6, v12, v1}, Landroid/support/constraint/a/e;->b(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 725
    iget-object v4, v8, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v4, :cond_19

    iget-object v4, v8, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v6, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 726
    :goto_b
    if-nez v9, :cond_16

    .line 728
    move-object/from16 v0, v21

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v4, :cond_1a

    move-object/from16 v0, v21

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    :goto_c
    move-object v6, v4

    .line 730
    :cond_16
    if-nez v5, :cond_38

    .line 731
    move-object/from16 v0, v19

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v4, :cond_1b

    move-object/from16 v0, v19

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    :goto_d
    move-object/from16 v17, v4

    .line 733
    :goto_e
    if-eqz v17, :cond_18

    .line 734
    move-object/from16 v0, v17

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v9, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 735
    if-eqz v18, :cond_17

    .line 736
    move-object/from16 v0, v19

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v4, :cond_1c

    move-object/from16 v0, v19

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    :goto_f
    move-object v9, v4

    .line 738
    :cond_17
    if-eqz v6, :cond_18

    if-eqz v9, :cond_18

    .line 739
    iget-object v5, v8, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    const/high16 v8, 0x3f000000    # 0.5f

    iget-object v10, v10, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    const/4 v12, 0x4

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v12}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;IFLandroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    :cond_18
    move-object/from16 v4, v17

    goto/16 :goto_a

    .line 725
    :cond_19
    const/4 v6, 0x0

    goto :goto_b

    .line 728
    :cond_1a
    const/4 v4, 0x0

    goto :goto_c

    .line 731
    :cond_1b
    const/4 v4, 0x0

    goto :goto_d

    .line 736
    :cond_1c
    const/4 v4, 0x0

    goto :goto_f

    .line 748
    :cond_1d
    if-eqz v14, :cond_0

    .line 749
    iget-object v4, v15, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    .line 750
    iget-object v10, v8, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    .line 751
    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v7

    .line 752
    invoke-virtual {v10}, Landroid/support/constraint/a/a/b;->d()I

    move-result v11

    .line 753
    move-object/from16 v0, v21

    iget-object v5, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v5, :cond_1e

    move-object/from16 v0, v21

    iget-object v5, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v6, v5, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 754
    :goto_10
    iget-object v5, v8, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v5, :cond_1f

    iget-object v5, v8, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v9, v5, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 755
    :goto_11
    if-eqz v6, :cond_0

    if-eqz v9, :cond_0

    .line 756
    iget-object v5, v10, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    neg-int v8, v11

    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v9, v8, v12}, Landroid/support/constraint/a/e;->b(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 757
    iget-object v5, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    move-object/from16 v0, v21

    iget v8, v0, Landroid/support/constraint/a/a/c;->O:F

    iget-object v10, v10, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    const/4 v12, 0x4

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v12}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;IFLandroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    goto/16 :goto_1

    .line 753
    :cond_1e
    const/4 v6, 0x0

    goto :goto_10

    .line 754
    :cond_1f
    const/4 v9, 0x0

    goto :goto_11

    .line 762
    :cond_20
    const/4 v4, 0x0

    .line 763
    const/4 v6, 0x0

    move-object v5, v4

    move-object v8, v15

    .line 764
    :goto_12
    if-eqz v8, :cond_27

    .line 765
    iget-object v4, v8, Landroid/support/constraint/a/a/c;->Q:Landroid/support/constraint/a/a/c$a;

    sget-object v7, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-eq v4, v7, :cond_25

    .line 766
    iget-object v4, v8, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v4

    .line 767
    if-eqz v5, :cond_21

    .line 768
    iget-object v5, v5, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    invoke-virtual {v5}, Landroid/support/constraint/a/a/b;->d()I

    move-result v5

    add-int/2addr v4, v5

    .line 770
    :cond_21
    const/4 v5, 0x3

    .line 771
    iget-object v7, v8, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v7, v7, Landroid/support/constraint/a/a/c;->Q:Landroid/support/constraint/a/a/c$a;

    sget-object v9, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-ne v7, v9, :cond_22

    .line 772
    const/4 v5, 0x2

    .line 774
    :cond_22
    iget-object v7, v8, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    iget-object v9, v8, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v9, v9, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v9, v9, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v9, v4, v5}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 775
    iget-object v4, v8, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v4

    .line 776
    iget-object v5, v8, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v5, v5, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v5, :cond_23

    iget-object v5, v8, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v5, v5, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    if-ne v5, v8, :cond_23

    .line 777
    iget-object v5, v8, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v5, v5, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    invoke-virtual {v5}, Landroid/support/constraint/a/a/b;->d()I

    move-result v5

    add-int/2addr v4, v5

    .line 779
    :cond_23
    const/4 v5, 0x3

    .line 780
    iget-object v7, v8, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v7, v7, Landroid/support/constraint/a/a/c;->Q:Landroid/support/constraint/a/a/c$a;

    sget-object v9, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-ne v7, v9, :cond_24

    .line 781
    const/4 v5, 0x2

    .line 783
    :cond_24
    iget-object v7, v8, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 784
    move-object/from16 v0, p0

    iget-object v9, v0, Landroid/support/constraint/a/a/d;->aB:[Landroid/support/constraint/a/a/c;

    const/4 v10, 0x3

    aget-object v9, v9, v10

    if-ne v8, v9, :cond_37

    .line 785
    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/constraint/a/a/d;->aB:[Landroid/support/constraint/a/a/c;

    const/4 v7, 0x1

    aget-object v5, v5, v7

    iget-object v5, v5, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 786
    const/4 v7, 0x3

    .line 788
    :goto_13
    iget-object v9, v8, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v9, v9, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    neg-int v4, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v5, v4, v7}, Landroid/support/constraint/a/e;->b(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 802
    :goto_14
    iget-object v4, v8, Landroid/support/constraint/a/a/c;->ai:Landroid/support/constraint/a/a/c;

    move-object v5, v8

    move-object v8, v4

    goto/16 :goto_12

    .line 790
    :cond_25
    iget v4, v8, Landroid/support/constraint/a/a/c;->ag:F

    add-float/2addr v6, v4

    .line 791
    const/4 v4, 0x0

    .line 792
    iget-object v5, v8, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v5, :cond_26

    .line 793
    iget-object v4, v8, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v4

    .line 794
    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/constraint/a/a/d;->aB:[Landroid/support/constraint/a/a/c;

    const/4 v7, 0x3

    aget-object v5, v5, v7

    if-eq v8, v5, :cond_26

    .line 795
    iget-object v5, v8, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v5, v5, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    invoke-virtual {v5}, Landroid/support/constraint/a/a/b;->d()I

    move-result v5

    add-int/2addr v4, v5

    .line 798
    :cond_26
    iget-object v5, v8, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    iget-object v7, v8, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    const/4 v9, 0x0

    const/4 v10, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v7, v9, v10}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 799
    iget-object v5, v8, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    iget-object v7, v8, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    neg-int v4, v4

    const/4 v9, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v7, v4, v9}, Landroid/support/constraint/a/e;->b(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    goto :goto_14

    .line 804
    :cond_27
    const/4 v4, 0x1

    move/from16 v0, v18

    if-ne v0, v4, :cond_2c

    .line 805
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/d;->aw:[Landroid/support/constraint/a/a/c;

    const/4 v5, 0x0

    aget-object v7, v4, v5

    .line 806
    iget-object v4, v7, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v4

    .line 807
    iget-object v5, v7, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v5, :cond_28

    .line 808
    iget-object v5, v7, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    invoke-virtual {v5}, Landroid/support/constraint/a/a/b;->d()I

    move-result v5

    add-int/2addr v4, v5

    .line 810
    :cond_28
    iget-object v5, v7, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    invoke-virtual {v5}, Landroid/support/constraint/a/a/b;->d()I

    move-result v5

    .line 811
    iget-object v6, v7, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v6, :cond_29

    .line 812
    iget-object v6, v7, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    invoke-virtual {v6}, Landroid/support/constraint/a/a/b;->d()I

    move-result v6

    add-int/2addr v5, v6

    .line 814
    :cond_29
    move-object/from16 v0, v21

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 815
    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/constraint/a/a/d;->aB:[Landroid/support/constraint/a/a/c;

    const/4 v9, 0x3

    aget-object v8, v8, v9

    if-ne v7, v8, :cond_2a

    .line 816
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/d;->aB:[Landroid/support/constraint/a/a/c;

    const/4 v8, 0x1

    aget-object v6, v6, v8

    iget-object v6, v6, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 819
    :cond_2a
    iget v8, v7, Landroid/support/constraint/a/a/c;->d:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_2b

    .line 820
    move-object/from16 v0, v21

    iget-object v7, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    move-object/from16 v0, v21

    iget-object v8, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v8, v8, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v8, v8, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    const/4 v9, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v8, v4, v9}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 821
    move-object/from16 v0, v21

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    neg-int v5, v5

    const/4 v7, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6, v5, v7}, Landroid/support/constraint/a/e;->b(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 822
    move-object/from16 v0, v21

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    move-object/from16 v0, v21

    iget-object v5, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    invoke-virtual/range {v21 .. v21}, Landroid/support/constraint/a/a/c;->l()I

    move-result v6

    const/4 v7, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/support/constraint/a/e;->c(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)Landroid/support/constraint/a/b;

    goto/16 :goto_1

    .line 824
    :cond_2b
    iget-object v8, v7, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v8, v8, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    iget-object v9, v7, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v9, v9, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v9, v9, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    const/4 v10, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v9, v4, v10}, Landroid/support/constraint/a/e;->c(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)Landroid/support/constraint/a/b;

    .line 825
    iget-object v4, v7, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    neg-int v5, v5

    const/4 v7, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6, v5, v7}, Landroid/support/constraint/a/e;->c(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)Landroid/support/constraint/a/b;

    goto/16 :goto_1

    .line 828
    :cond_2c
    const/4 v4, 0x0

    move/from16 v17, v4

    :goto_15
    add-int/lit8 v4, v18, -0x1

    move/from16 v0, v17

    if-ge v0, v4, :cond_0

    .line 829
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/d;->aw:[Landroid/support/constraint/a/a/c;

    aget-object v11, v4, v17

    .line 830
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/d;->aw:[Landroid/support/constraint/a/a/c;

    add-int/lit8 v5, v17, 0x1

    aget-object v15, v4, v5

    .line 831
    iget-object v4, v11, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v8, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 832
    iget-object v4, v11, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v10, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 833
    iget-object v4, v15, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v12, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 834
    iget-object v4, v15, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v14, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 835
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/d;->aB:[Landroid/support/constraint/a/a/c;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    if-ne v15, v4, :cond_2d

    .line 836
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/d;->aB:[Landroid/support/constraint/a/a/c;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    iget-object v4, v4, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v14, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    .line 838
    :cond_2d
    iget-object v4, v11, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v4

    .line 839
    iget-object v5, v11, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v5, :cond_2e

    iget-object v5, v11, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v5, v5, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v5, :cond_2e

    iget-object v5, v11, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v5, v5, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    if-ne v5, v11, :cond_2e

    .line 841
    iget-object v5, v11, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v5, v5, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    invoke-virtual {v5}, Landroid/support/constraint/a/a/b;->d()I

    move-result v5

    add-int/2addr v4, v5

    .line 843
    :cond_2e
    iget-object v5, v11, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    const/4 v7, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v5, v4, v7}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 844
    iget-object v4, v11, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v5

    .line 845
    iget-object v4, v11, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v4, :cond_36

    iget-object v4, v11, Landroid/support/constraint/a/a/c;->ai:Landroid/support/constraint/a/a/c;

    if-eqz v4, :cond_36

    .line 846
    iget-object v4, v11, Landroid/support/constraint/a/a/c;->ai:Landroid/support/constraint/a/a/c;

    iget-object v4, v4, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v4, :cond_34

    iget-object v4, v11, Landroid/support/constraint/a/a/c;->ai:Landroid/support/constraint/a/a/c;

    iget-object v4, v4, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v4

    :goto_16
    add-int/2addr v4, v5

    .line 848
    :goto_17
    iget-object v5, v11, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    neg-int v4, v4

    const/4 v7, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v5, v4, v7}, Landroid/support/constraint/a/e;->b(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 849
    add-int/lit8 v4, v17, 0x1

    add-int/lit8 v5, v18, -0x1

    if-ne v4, v5, :cond_32

    .line 851
    iget-object v4, v15, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v4

    .line 852
    iget-object v5, v15, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v5, :cond_2f

    iget-object v5, v15, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v5, v5, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v5, :cond_2f

    iget-object v5, v15, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v5, v5, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    if-ne v5, v15, :cond_2f

    .line 854
    iget-object v5, v15, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v5, v5, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    invoke-virtual {v5}, Landroid/support/constraint/a/a/b;->d()I

    move-result v5

    add-int/2addr v4, v5

    .line 856
    :cond_2f
    iget-object v5, v15, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    const/4 v7, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v5, v4, v7}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 857
    iget-object v4, v15, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    .line 858
    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/constraint/a/a/d;->aB:[Landroid/support/constraint/a/a/c;

    const/4 v7, 0x3

    aget-object v5, v5, v7

    if-ne v15, v5, :cond_30

    .line 859
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/d;->aB:[Landroid/support/constraint/a/a/c;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    iget-object v4, v4, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    .line 861
    :cond_30
    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v5

    .line 862
    iget-object v7, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v7, :cond_31

    iget-object v7, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v7, v7, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v7, :cond_31

    iget-object v7, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v7, v7, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    if-ne v7, v15, :cond_31

    .line 864
    iget-object v7, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v7, v7, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v7, v7, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    invoke-virtual {v7}, Landroid/support/constraint/a/a/b;->d()I

    move-result v7

    add-int/2addr v5, v7

    .line 866
    :cond_31
    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->f:Landroid/support/constraint/a/g;

    neg-int v5, v5

    const/4 v7, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v4, v5, v7}, Landroid/support/constraint/a/e;->b(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 869
    :cond_32
    move-object/from16 v0, v21

    iget v4, v0, Landroid/support/constraint/a/a/c;->i:I

    if-lez v4, :cond_33

    .line 870
    move-object/from16 v0, v21

    iget v4, v0, Landroid/support/constraint/a/a/c;->i:I

    const/4 v5, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v8, v4, v5}, Landroid/support/constraint/a/e;->b(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 873
    :cond_33
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->b()Landroid/support/constraint/a/b;

    move-result-object v4

    .line 874
    iget v5, v11, Landroid/support/constraint/a/a/c;->ag:F

    iget v7, v15, Landroid/support/constraint/a/a/c;->ag:F

    iget-object v9, v11, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    .line 876
    invoke-virtual {v9}, Landroid/support/constraint/a/a/b;->d()I

    move-result v9

    iget-object v11, v11, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    .line 877
    invoke-virtual {v11}, Landroid/support/constraint/a/a/b;->d()I

    move-result v11

    iget-object v13, v15, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    .line 878
    invoke-virtual {v13}, Landroid/support/constraint/a/a/b;->d()I

    move-result v13

    iget-object v15, v15, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    .line 879
    invoke-virtual {v15}, Landroid/support/constraint/a/a/b;->d()I

    move-result v15

    .line 874
    invoke-virtual/range {v4 .. v15}, Landroid/support/constraint/a/b;->a(FFFLandroid/support/constraint/a/g;ILandroid/support/constraint/a/g;ILandroid/support/constraint/a/g;ILandroid/support/constraint/a/g;I)Landroid/support/constraint/a/b;

    .line 880
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    .line 828
    add-int/lit8 v4, v17, 0x1

    move/from16 v17, v4

    goto/16 :goto_15

    .line 846
    :cond_34
    const/4 v4, 0x0

    goto/16 :goto_16

    .line 886
    :cond_35
    return-void

    :cond_36
    move v4, v5

    goto/16 :goto_17

    :cond_37
    move-object/from16 v22, v7

    move v7, v5

    move-object/from16 v5, v22

    goto/16 :goto_13

    :cond_38
    move-object/from16 v17, v5

    goto/16 :goto_e

    :cond_39
    move/from16 v18, v4

    move-object/from16 v19, v8

    goto/16 :goto_7
.end method

.method private d(Landroid/support/constraint/a/a/c;)V
    .locals 2

    .prologue
    .line 1904
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Landroid/support/constraint/a/a/d;->au:I

    if-ge v0, v1, :cond_1

    .line 1905
    iget-object v1, p0, Landroid/support/constraint/a/a/d;->ay:[Landroid/support/constraint/a/a/c;

    aget-object v1, v1, v0

    if-ne v1, p1, :cond_0

    .line 1914
    :goto_1
    return-void

    .line 1904
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1909
    :cond_1
    iget v0, p0, Landroid/support/constraint/a/a/d;->au:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Landroid/support/constraint/a/a/d;->ay:[Landroid/support/constraint/a/a/c;

    array-length v1, v1

    if-lt v0, v1, :cond_2

    .line 1910
    iget-object v0, p0, Landroid/support/constraint/a/a/d;->ay:[Landroid/support/constraint/a/a/c;

    iget-object v1, p0, Landroid/support/constraint/a/a/d;->ay:[Landroid/support/constraint/a/a/c;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/support/constraint/a/a/c;

    iput-object v0, p0, Landroid/support/constraint/a/a/d;->ay:[Landroid/support/constraint/a/a/c;

    .line 1912
    :cond_2
    iget-object v0, p0, Landroid/support/constraint/a/a/d;->ay:[Landroid/support/constraint/a/a/c;

    iget v1, p0, Landroid/support/constraint/a/a/d;->au:I

    aput-object p1, v0, v1

    .line 1913
    iget v0, p0, Landroid/support/constraint/a/a/d;->au:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/constraint/a/a/d;->au:I

    goto :goto_1
.end method

.method private e(Landroid/support/constraint/a/a/c;)V
    .locals 2

    .prologue
    .line 1923
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Landroid/support/constraint/a/a/d;->av:I

    if-ge v0, v1, :cond_1

    .line 1924
    iget-object v1, p0, Landroid/support/constraint/a/a/d;->ax:[Landroid/support/constraint/a/a/c;

    aget-object v1, v1, v0

    if-ne v1, p1, :cond_0

    .line 1933
    :goto_1
    return-void

    .line 1923
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1928
    :cond_1
    iget v0, p0, Landroid/support/constraint/a/a/d;->av:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Landroid/support/constraint/a/a/d;->ax:[Landroid/support/constraint/a/a/c;

    array-length v1, v1

    if-lt v0, v1, :cond_2

    .line 1929
    iget-object v0, p0, Landroid/support/constraint/a/a/d;->ax:[Landroid/support/constraint/a/a/c;

    iget-object v1, p0, Landroid/support/constraint/a/a/d;->ax:[Landroid/support/constraint/a/a/c;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/support/constraint/a/a/c;

    iput-object v0, p0, Landroid/support/constraint/a/a/d;->ax:[Landroid/support/constraint/a/a/c;

    .line 1931
    :cond_2
    iget-object v0, p0, Landroid/support/constraint/a/a/d;->ax:[Landroid/support/constraint/a/a/c;

    iget v1, p0, Landroid/support/constraint/a/a/d;->av:I

    aput-object p1, v0, v1

    .line 1932
    iget v0, p0, Landroid/support/constraint/a/a/d;->av:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/constraint/a/a/d;->av:I

    goto :goto_1
.end method


# virtual methods
.method public D()Z
    .locals 1

    .prologue
    .line 152
    iget-boolean v0, p0, Landroid/support/constraint/a/a/d;->aC:Z

    return v0
.end method

.method public E()Z
    .locals 1

    .prologue
    .line 157
    iget-boolean v0, p0, Landroid/support/constraint/a/a/d;->aD:Z

    return v0
.end method

.method public F()V
    .locals 18

    .prologue
    .line 931
    move-object/from16 v0, p0

    iget v8, v0, Landroid/support/constraint/a/a/d;->F:I

    .line 932
    move-object/from16 v0, p0

    iget v9, v0, Landroid/support/constraint/a/a/d;->G:I

    .line 933
    const/4 v1, 0x0

    invoke-virtual/range {p0 .. p0}, Landroid/support/constraint/a/a/d;->h()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v10

    .line 934
    const/4 v1, 0x0

    invoke-virtual/range {p0 .. p0}, Landroid/support/constraint/a/a/d;->l()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v11

    .line 935
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Landroid/support/constraint/a/a/d;->aC:Z

    .line 936
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Landroid/support/constraint/a/a/d;->aD:Z

    .line 938
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/constraint/a/a/d;->w:Landroid/support/constraint/a/a/c;

    if-eqz v1, :cond_6

    .line 939
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/constraint/a/a/d;->at:Landroid/support/constraint/a/a/i;

    if-nez v1, :cond_0

    .line 940
    new-instance v1, Landroid/support/constraint/a/a/i;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Landroid/support/constraint/a/a/i;-><init>(Landroid/support/constraint/a/a/c;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Landroid/support/constraint/a/a/d;->at:Landroid/support/constraint/a/a/i;

    .line 942
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/constraint/a/a/d;->at:Landroid/support/constraint/a/a/i;

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Landroid/support/constraint/a/a/i;->a(Landroid/support/constraint/a/a/c;)V

    .line 947
    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/constraint/a/a/d;->ao:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/d;->c(I)V

    .line 948
    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/constraint/a/a/d;->ap:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/d;->d(I)V

    .line 949
    invoke-virtual/range {p0 .. p0}, Landroid/support/constraint/a/a/d;->A()V

    .line 950
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/constraint/a/a/d;->aj:Landroid/support/constraint/a/e;

    invoke-virtual {v1}, Landroid/support/constraint/a/e;->f()Landroid/support/constraint/a/c;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/d;->a(Landroid/support/constraint/a/c;)V

    .line 956
    :goto_0
    const/4 v1, 0x0

    .line 957
    move-object/from16 v0, p0

    iget-object v12, v0, Landroid/support/constraint/a/a/d;->Q:Landroid/support/constraint/a/a/c$a;

    .line 958
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/support/constraint/a/a/d;->P:Landroid/support/constraint/a/a/c$a;

    .line 964
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/constraint/a/a/d;->az:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/constraint/a/a/d;->Q:Landroid/support/constraint/a/a/c$a;

    sget-object v3, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    if-eq v2, v3, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/constraint/a/a/d;->P:Landroid/support/constraint/a/a/c$a;

    sget-object v3, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    if-ne v2, v3, :cond_9

    .line 968
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/constraint/a/a/d;->as:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/constraint/a/a/d;->aA:[Z

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Landroid/support/constraint/a/a/d;->a(Ljava/util/ArrayList;[Z)V

    .line 969
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/constraint/a/a/d;->aA:[Z

    const/4 v2, 0x0

    aget-boolean v1, v1, v2

    .line 973
    if-lez v10, :cond_3

    if-lez v11, :cond_3

    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/constraint/a/a/d;->am:I

    if-gt v2, v10, :cond_2

    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/constraint/a/a/d;->an:I

    if-le v2, v11, :cond_3

    .line 976
    :cond_2
    const/4 v1, 0x0

    .line 978
    :cond_3
    if-eqz v1, :cond_9

    .line 979
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/constraint/a/a/d;->P:Landroid/support/constraint/a/a/c$a;

    sget-object v3, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    if-ne v2, v3, :cond_4

    .line 980
    sget-object v2, Landroid/support/constraint/a/a/c$a;->a:Landroid/support/constraint/a/a/c$a;

    move-object/from16 v0, p0

    iput-object v2, v0, Landroid/support/constraint/a/a/d;->P:Landroid/support/constraint/a/a/c$a;

    .line 981
    if-lez v10, :cond_7

    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/constraint/a/a/d;->am:I

    if-ge v10, v2, :cond_7

    .line 982
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Landroid/support/constraint/a/a/d;->aC:Z

    .line 983
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Landroid/support/constraint/a/a/d;->e(I)V

    .line 988
    :cond_4
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/constraint/a/a/d;->Q:Landroid/support/constraint/a/a/c$a;

    sget-object v3, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    if-ne v2, v3, :cond_9

    .line 989
    sget-object v2, Landroid/support/constraint/a/a/c$a;->a:Landroid/support/constraint/a/a/c$a;

    move-object/from16 v0, p0

    iput-object v2, v0, Landroid/support/constraint/a/a/d;->Q:Landroid/support/constraint/a/a/c$a;

    .line 990
    if-lez v11, :cond_8

    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/constraint/a/a/d;->an:I

    if-ge v11, v2, :cond_8

    .line 991
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Landroid/support/constraint/a/a/d;->aD:Z

    .line 992
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Landroid/support/constraint/a/a/d;->f(I)V

    move v2, v1

    .line 1001
    :goto_2
    invoke-direct/range {p0 .. p0}, Landroid/support/constraint/a/a/d;->J()V

    .line 1005
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/constraint/a/a/d;->as:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v14

    .line 1006
    const/4 v1, 0x0

    move v3, v1

    :goto_3
    if-ge v3, v14, :cond_a

    .line 1007
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/constraint/a/a/d;->as:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/constraint/a/a/c;

    .line 1008
    instance-of v4, v1, Landroid/support/constraint/a/a/j;

    if-eqz v4, :cond_5

    .line 1009
    check-cast v1, Landroid/support/constraint/a/a/j;

    invoke-virtual {v1}, Landroid/support/constraint/a/a/j;->F()V

    .line 1006
    :cond_5
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_3

    .line 952
    :cond_6
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Landroid/support/constraint/a/a/d;->F:I

    .line 953
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Landroid/support/constraint/a/a/d;->G:I

    goto/16 :goto_0

    .line 985
    :cond_7
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/constraint/a/a/d;->K:I

    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/constraint/a/a/d;->am:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/a/d;->e(I)V

    goto :goto_1

    .line 994
    :cond_8
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/constraint/a/a/d;->L:I

    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/constraint/a/a/d;->an:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/a/d;->f(I)V

    :cond_9
    move v2, v1

    goto :goto_2

    .line 1014
    :cond_a
    const/4 v3, 0x1

    .line 1015
    const/4 v1, 0x0

    move/from16 v17, v3

    move v3, v2

    move/from16 v2, v17

    .line 1016
    :goto_4
    if-eqz v2, :cond_16

    .line 1017
    add-int/lit8 v7, v1, 0x1

    .line 1019
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/constraint/a/a/d;->aj:Landroid/support/constraint/a/e;

    invoke-virtual {v1}, Landroid/support/constraint/a/e;->a()V

    .line 1029
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/constraint/a/a/d;->aj:Landroid/support/constraint/a/e;

    const v4, 0x7fffffff

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v4}, Landroid/support/constraint/a/a/d;->c(Landroid/support/constraint/a/e;I)Z

    move-result v2

    .line 1030
    if-eqz v2, :cond_b

    .line 1031
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/constraint/a/a/d;->aj:Landroid/support/constraint/a/e;

    invoke-virtual {v1}, Landroid/support/constraint/a/e;->e()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1036
    :cond_b
    :goto_5
    if-eqz v2, :cond_d

    .line 1037
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/constraint/a/a/d;->aj:Landroid/support/constraint/a/e;

    const v2, 0x7fffffff

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/d;->aA:[Z

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2, v4}, Landroid/support/constraint/a/a/d;->a(Landroid/support/constraint/a/e;I[Z)V

    .line 1054
    :cond_c
    :goto_6
    const/4 v2, 0x0

    .line 1056
    const/16 v1, 0x8

    if-ge v7, v1, :cond_1b

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/constraint/a/a/d;->aA:[Z

    const/4 v4, 0x2

    aget-boolean v1, v1, v4

    if-eqz v1, :cond_1b

    .line 1058
    const/4 v5, 0x0

    .line 1059
    const/4 v4, 0x0

    .line 1060
    const/4 v1, 0x0

    move v6, v5

    move v5, v4

    move v4, v1

    :goto_7
    if-ge v4, v14, :cond_10

    .line 1061
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/constraint/a/a/d;->as:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/constraint/a/a/c;

    .line 1062
    iget v15, v1, Landroid/support/constraint/a/a/c;->F:I

    invoke-virtual {v1}, Landroid/support/constraint/a/a/c;->h()I

    move-result v16

    add-int v15, v15, v16

    invoke-static {v6, v15}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 1063
    iget v15, v1, Landroid/support/constraint/a/a/c;->G:I

    invoke-virtual {v1}, Landroid/support/constraint/a/a/c;->l()I

    move-result v1

    add-int/2addr v1, v15

    invoke-static {v5, v1}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 1060
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_7

    .line 1033
    :catch_0
    move-exception v1

    .line 1034
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    .line 1039
    :cond_d
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/constraint/a/a/d;->aj:Landroid/support/constraint/a/e;

    const v2, 0x7fffffff

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Landroid/support/constraint/a/a/d;->b(Landroid/support/constraint/a/e;I)V

    .line 1040
    const/4 v1, 0x0

    move v2, v1

    :goto_8
    if-ge v2, v14, :cond_c

    .line 1041
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/constraint/a/a/d;->as:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/constraint/a/a/c;

    .line 1042
    iget-object v4, v1, Landroid/support/constraint/a/a/c;->P:Landroid/support/constraint/a/a/c$a;

    sget-object v5, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-ne v4, v5, :cond_e

    .line 1043
    invoke-virtual {v1}, Landroid/support/constraint/a/a/c;->h()I

    move-result v4

    invoke-virtual {v1}, Landroid/support/constraint/a/a/c;->k()I

    move-result v5

    if-ge v4, v5, :cond_e

    .line 1044
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/constraint/a/a/d;->aA:[Z

    const/4 v2, 0x2

    const/4 v4, 0x1

    aput-boolean v4, v1, v2

    goto :goto_6

    .line 1047
    :cond_e
    iget-object v4, v1, Landroid/support/constraint/a/a/c;->Q:Landroid/support/constraint/a/a/c$a;

    sget-object v5, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-ne v4, v5, :cond_f

    .line 1048
    invoke-virtual {v1}, Landroid/support/constraint/a/a/c;->l()I

    move-result v4

    invoke-virtual {v1}, Landroid/support/constraint/a/a/c;->m()I

    move-result v1

    if-ge v4, v1, :cond_f

    .line 1049
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/constraint/a/a/d;->aA:[Z

    const/4 v2, 0x2

    const/4 v4, 0x1

    aput-boolean v4, v1, v2

    goto/16 :goto_6

    .line 1040
    :cond_f
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_8

    .line 1065
    :cond_10
    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/constraint/a/a/d;->K:I

    invoke-static {v1, v6}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1066
    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/constraint/a/a/d;->L:I

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 1067
    sget-object v5, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    if-ne v13, v5, :cond_1a

    .line 1068
    invoke-virtual/range {p0 .. p0}, Landroid/support/constraint/a/a/d;->h()I

    move-result v5

    if-ge v5, v1, :cond_1a

    .line 1072
    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/d;->e(I)V

    .line 1073
    sget-object v1, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    move-object/from16 v0, p0

    iput-object v1, v0, Landroid/support/constraint/a/a/d;->P:Landroid/support/constraint/a/a/c$a;

    .line 1074
    const/4 v2, 0x1

    .line 1075
    const/4 v1, 0x1

    .line 1078
    :goto_9
    sget-object v3, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    if-ne v12, v3, :cond_11

    .line 1079
    invoke-virtual/range {p0 .. p0}, Landroid/support/constraint/a/a/d;->l()I

    move-result v3

    if-ge v3, v4, :cond_11

    .line 1083
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/support/constraint/a/a/d;->f(I)V

    .line 1084
    sget-object v1, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    move-object/from16 v0, p0

    iput-object v1, v0, Landroid/support/constraint/a/a/d;->Q:Landroid/support/constraint/a/a/c$a;

    .line 1085
    const/4 v2, 0x1

    .line 1086
    const/4 v1, 0x1

    .line 1091
    :cond_11
    :goto_a
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/constraint/a/a/d;->K:I

    invoke-virtual/range {p0 .. p0}, Landroid/support/constraint/a/a/d;->h()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1092
    invoke-virtual/range {p0 .. p0}, Landroid/support/constraint/a/a/d;->h()I

    move-result v4

    if-le v3, v4, :cond_12

    .line 1096
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/support/constraint/a/a/d;->e(I)V

    .line 1097
    sget-object v1, Landroid/support/constraint/a/a/c$a;->a:Landroid/support/constraint/a/a/c$a;

    move-object/from16 v0, p0

    iput-object v1, v0, Landroid/support/constraint/a/a/d;->P:Landroid/support/constraint/a/a/c$a;

    .line 1098
    const/4 v2, 0x1

    .line 1099
    const/4 v1, 0x1

    .line 1101
    :cond_12
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/constraint/a/a/d;->L:I

    invoke-virtual/range {p0 .. p0}, Landroid/support/constraint/a/a/d;->l()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1102
    invoke-virtual/range {p0 .. p0}, Landroid/support/constraint/a/a/d;->l()I

    move-result v4

    if-le v3, v4, :cond_13

    .line 1106
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/support/constraint/a/a/d;->f(I)V

    .line 1107
    sget-object v1, Landroid/support/constraint/a/a/c$a;->a:Landroid/support/constraint/a/a/c$a;

    move-object/from16 v0, p0

    iput-object v1, v0, Landroid/support/constraint/a/a/d;->Q:Landroid/support/constraint/a/a/c$a;

    .line 1108
    const/4 v2, 0x1

    .line 1109
    const/4 v1, 0x1

    .line 1112
    :cond_13
    if-nez v2, :cond_15

    .line 1113
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/constraint/a/a/d;->P:Landroid/support/constraint/a/a/c$a;

    sget-object v4, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    if-ne v3, v4, :cond_14

    if-lez v10, :cond_14

    .line 1114
    invoke-virtual/range {p0 .. p0}, Landroid/support/constraint/a/a/d;->h()I

    move-result v3

    if-le v3, v10, :cond_14

    .line 1118
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Landroid/support/constraint/a/a/d;->aC:Z

    .line 1119
    const/4 v2, 0x1

    .line 1120
    sget-object v1, Landroid/support/constraint/a/a/c$a;->a:Landroid/support/constraint/a/a/c$a;

    move-object/from16 v0, p0

    iput-object v1, v0, Landroid/support/constraint/a/a/d;->P:Landroid/support/constraint/a/a/c$a;

    .line 1121
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Landroid/support/constraint/a/a/d;->e(I)V

    .line 1122
    const/4 v1, 0x1

    .line 1125
    :cond_14
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/constraint/a/a/d;->Q:Landroid/support/constraint/a/a/c$a;

    sget-object v4, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    if-ne v3, v4, :cond_15

    if-lez v11, :cond_15

    .line 1126
    invoke-virtual/range {p0 .. p0}, Landroid/support/constraint/a/a/d;->l()I

    move-result v3

    if-le v3, v11, :cond_15

    .line 1130
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Landroid/support/constraint/a/a/d;->aD:Z

    .line 1131
    const/4 v2, 0x1

    .line 1132
    sget-object v1, Landroid/support/constraint/a/a/c$a;->a:Landroid/support/constraint/a/a/c$a;

    move-object/from16 v0, p0

    iput-object v1, v0, Landroid/support/constraint/a/a/d;->Q:Landroid/support/constraint/a/a/c$a;

    .line 1133
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Landroid/support/constraint/a/a/d;->f(I)V

    .line 1134
    const/4 v1, 0x1

    :cond_15
    move v3, v2

    move v2, v1

    move v1, v7

    .line 1138
    goto/16 :goto_4

    .line 1142
    :cond_16
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/constraint/a/a/d;->w:Landroid/support/constraint/a/a/c;

    if-eqz v1, :cond_19

    .line 1143
    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/constraint/a/a/d;->K:I

    invoke-virtual/range {p0 .. p0}, Landroid/support/constraint/a/a/d;->h()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1144
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/constraint/a/a/d;->L:I

    invoke-virtual/range {p0 .. p0}, Landroid/support/constraint/a/a/d;->l()I

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1146
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/d;->at:Landroid/support/constraint/a/a/i;

    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Landroid/support/constraint/a/a/i;->b(Landroid/support/constraint/a/a/c;)V

    .line 1147
    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/constraint/a/a/d;->ao:I

    add-int/2addr v1, v4

    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/constraint/a/a/d;->aq:I

    add-int/2addr v1, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/d;->e(I)V

    .line 1148
    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/constraint/a/a/d;->ap:I

    add-int/2addr v1, v2

    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/constraint/a/a/d;->ar:I

    add-int/2addr v1, v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/d;->f(I)V

    .line 1153
    :goto_b
    if-eqz v3, :cond_17

    .line 1154
    move-object/from16 v0, p0

    iput-object v13, v0, Landroid/support/constraint/a/a/d;->P:Landroid/support/constraint/a/a/c$a;

    .line 1155
    move-object/from16 v0, p0

    iput-object v12, v0, Landroid/support/constraint/a/a/d;->Q:Landroid/support/constraint/a/a/c$a;

    .line 1157
    :cond_17
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/constraint/a/a/d;->aj:Landroid/support/constraint/a/e;

    invoke-virtual {v1}, Landroid/support/constraint/a/e;->f()Landroid/support/constraint/a/c;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/d;->a(Landroid/support/constraint/a/c;)V

    .line 1158
    invoke-virtual/range {p0 .. p0}, Landroid/support/constraint/a/a/d;->H()Landroid/support/constraint/a/a/d;

    move-result-object v1

    move-object/from16 v0, p0

    if-ne v0, v1, :cond_18

    .line 1159
    invoke-virtual/range {p0 .. p0}, Landroid/support/constraint/a/a/d;->z()V

    .line 1161
    :cond_18
    return-void

    .line 1150
    :cond_19
    move-object/from16 v0, p0

    iput v8, v0, Landroid/support/constraint/a/a/d;->F:I

    .line 1151
    move-object/from16 v0, p0

    iput v9, v0, Landroid/support/constraint/a/a/d;->G:I

    goto :goto_b

    :cond_1a
    move v1, v2

    move v2, v3

    goto/16 :goto_9

    :cond_1b
    move v1, v2

    move v2, v3

    goto/16 :goto_a
.end method

.method public G()Z
    .locals 1

    .prologue
    .line 1811
    const/4 v0, 0x0

    return v0
.end method

.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 138
    iget-object v0, p0, Landroid/support/constraint/a/a/d;->aj:Landroid/support/constraint/a/e;

    invoke-virtual {v0}, Landroid/support/constraint/a/e;->a()V

    .line 142
    iput v1, p0, Landroid/support/constraint/a/a/d;->ao:I

    .line 143
    iput v1, p0, Landroid/support/constraint/a/a/d;->aq:I

    .line 144
    iput v1, p0, Landroid/support/constraint/a/a/d;->ap:I

    .line 145
    iput v1, p0, Landroid/support/constraint/a/a/d;->ar:I

    .line 146
    invoke-super {p0}, Landroid/support/constraint/a/a/j;->a()V

    .line 147
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 123
    iput p1, p0, Landroid/support/constraint/a/a/d;->az:I

    .line 124
    return-void
.end method

.method a(Landroid/support/constraint/a/a/c;I)V
    .locals 2

    .prologue
    .line 1875
    .line 1876
    if-nez p2, :cond_2

    .line 1878
    :goto_0
    iget-object v0, p1, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v0, :cond_0

    iget-object v0, p1, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v0, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v0, :cond_0

    iget-object v0, p1, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v0, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v1, p1, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    if-eq v0, p1, :cond_0

    .line 1882
    iget-object v0, p1, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object p1, v0, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    goto :goto_0

    .line 1884
    :cond_0
    invoke-direct {p0, p1}, Landroid/support/constraint/a/a/d;->d(Landroid/support/constraint/a/a/c;)V

    .line 1895
    :cond_1
    :goto_1
    return-void

    .line 1885
    :cond_2
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    .line 1887
    :goto_2
    iget-object v0, p1, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v0, :cond_3

    iget-object v0, p1, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v0, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v0, :cond_3

    iget-object v0, p1, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v0, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v1, p1, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    if-ne v0, v1, :cond_3

    iget-object v0, p1, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    if-eq v0, p1, :cond_3

    .line 1891
    iget-object v0, p1, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object p1, v0, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    goto :goto_2

    .line 1893
    :cond_3
    invoke-direct {p0, p1}, Landroid/support/constraint/a/a/d;->e(Landroid/support/constraint/a/a/c;)V

    goto :goto_1
.end method

.method public a(Landroid/support/constraint/a/a/c;[Z)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v7, -0x1

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 1213
    iget-object v0, p1, Landroid/support/constraint/a/a/c;->P:Landroid/support/constraint/a/a/c$a;

    sget-object v2, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-ne v0, v2, :cond_0

    iget-object v0, p1, Landroid/support/constraint/a/a/c;->Q:Landroid/support/constraint/a/a/c$a;

    sget-object v2, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-ne v0, v2, :cond_0

    iget v0, p1, Landroid/support/constraint/a/a/c;->z:F

    cmpl-float v0, v0, v5

    if-lez v0, :cond_0

    .line 1216
    aput-boolean v1, p2, v1

    .line 1325
    :goto_0
    return-void

    .line 1219
    :cond_0
    iget-object v0, p1, Landroid/support/constraint/a/a/c;->P:Landroid/support/constraint/a/a/c$a;

    sget-object v2, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-ne v0, v2, :cond_1

    iget v0, p1, Landroid/support/constraint/a/a/c;->c:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    .line 1221
    aput-boolean v1, p2, v1

    goto :goto_0

    .line 1224
    :cond_1
    invoke-virtual {p1}, Landroid/support/constraint/a/a/c;->i()I

    move-result v2

    .line 1226
    iget-object v0, p1, Landroid/support/constraint/a/a/c;->P:Landroid/support/constraint/a/a/c$a;

    sget-object v4, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-ne v0, v4, :cond_2

    .line 1227
    iget-object v0, p1, Landroid/support/constraint/a/a/c;->Q:Landroid/support/constraint/a/a/c$a;

    sget-object v4, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-eq v0, v4, :cond_2

    iget v0, p1, Landroid/support/constraint/a/a/c;->z:F

    cmpl-float v0, v0, v5

    if-lez v0, :cond_2

    .line 1229
    aput-boolean v1, p2, v1

    goto :goto_0

    .line 1240
    :cond_2
    iput-boolean v6, p1, Landroid/support/constraint/a/a/c;->Z:Z

    .line 1242
    instance-of v0, p1, Landroid/support/constraint/a/a/e;

    if-eqz v0, :cond_7

    move-object v0, p1

    .line 1243
    check-cast v0, Landroid/support/constraint/a/a/e;

    .line 1244
    invoke-virtual {v0}, Landroid/support/constraint/a/a/e;->D()I

    move-result v3

    if-ne v3, v6, :cond_1b

    .line 1247
    invoke-virtual {v0}, Landroid/support/constraint/a/a/e;->F()I

    move-result v2

    if-eq v2, v7, :cond_5

    .line 1248
    invoke-virtual {v0}, Landroid/support/constraint/a/a/e;->F()I

    move-result v0

    :goto_1
    move v2, v0

    move v4, v1

    .line 1319
    :cond_3
    :goto_2
    invoke-virtual {p1}, Landroid/support/constraint/a/a/c;->d()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 1320
    iget v0, p1, Landroid/support/constraint/a/a/c;->x:I

    sub-int/2addr v2, v0

    .line 1321
    iget v0, p1, Landroid/support/constraint/a/a/c;->x:I

    sub-int/2addr v4, v0

    .line 1323
    :cond_4
    iput v2, p1, Landroid/support/constraint/a/a/c;->S:I

    .line 1324
    iput v4, p1, Landroid/support/constraint/a/a/c;->T:I

    goto :goto_0

    .line 1249
    :cond_5
    invoke-virtual {v0}, Landroid/support/constraint/a/a/e;->G()I

    move-result v2

    if-eq v2, v7, :cond_6

    .line 1250
    invoke-virtual {v0}, Landroid/support/constraint/a/a/e;->G()I

    move-result v2

    move v0, v1

    move v1, v2

    goto :goto_1

    .line 1251
    :cond_6
    invoke-virtual {v0}, Landroid/support/constraint/a/a/e;->E()F

    move-result v0

    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_1a

    .line 1252
    aput-boolean v1, p2, v1

    goto :goto_0

    .line 1256
    :cond_7
    iget-object v0, p1, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0}, Landroid/support/constraint/a/a/b;->j()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p1, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0}, Landroid/support/constraint/a/a/b;->j()Z

    move-result v0

    if-nez v0, :cond_8

    .line 1257
    invoke-virtual {p1}, Landroid/support/constraint/a/a/c;->f()I

    move-result v0

    add-int/2addr v0, v2

    move v4, v2

    move v2, v0

    goto :goto_2

    .line 1259
    :cond_8
    iget-object v0, p1, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v0, :cond_9

    iget-object v0, p1, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v0, :cond_9

    iget-boolean v0, p1, Landroid/support/constraint/a/a/c;->k:Z

    if-eqz v0, :cond_9

    iget-object v0, p1, Landroid/support/constraint/a/a/c;->Q:Landroid/support/constraint/a/a/c$a;

    sget-object v4, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-ne v0, v4, :cond_9

    .line 1262
    aput-boolean v1, p2, v1

    goto/16 :goto_0

    .line 1265
    :cond_9
    iget-object v0, p1, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v0, :cond_b

    iget-object v0, p1, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v0, :cond_b

    iget-object v0, p1, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v4, p1, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eq v0, v4, :cond_a

    iget-object v0, p1, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v4, p1, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    if-ne v0, v4, :cond_b

    iget-object v0, p1, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v4, p1, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    if-eq v0, v4, :cond_b

    .line 1269
    :cond_a
    aput-boolean v1, p2, v1

    goto/16 :goto_0

    .line 1272
    :cond_b
    iget-object v0, p1, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v0, :cond_19

    .line 1273
    iget-object v0, p1, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    .line 1274
    iget-object v4, p1, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v4

    add-int/2addr v4, v2

    .line 1275
    invoke-virtual {v0}, Landroid/support/constraint/a/a/c;->b()Z

    move-result v5

    if-nez v5, :cond_c

    iget-boolean v5, v0, Landroid/support/constraint/a/a/c;->Z:Z

    if-nez v5, :cond_c

    .line 1276
    invoke-virtual {p0, v0, p2}, Landroid/support/constraint/a/a/d;->a(Landroid/support/constraint/a/a/c;[Z)V

    .line 1279
    :cond_c
    :goto_3
    iget-object v5, p1, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v5, :cond_d

    .line 1280
    iget-object v3, p1, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v3, v3, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v3, v3, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    .line 1281
    iget-object v5, p1, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    invoke-virtual {v5}, Landroid/support/constraint/a/a/b;->d()I

    move-result v5

    add-int/2addr v2, v5

    .line 1282
    invoke-virtual {v3}, Landroid/support/constraint/a/a/c;->b()Z

    move-result v5

    if-nez v5, :cond_d

    iget-boolean v5, v3, Landroid/support/constraint/a/a/c;->Z:Z

    if-nez v5, :cond_d

    .line 1283
    invoke-virtual {p0, v3, p2}, Landroid/support/constraint/a/a/d;->a(Landroid/support/constraint/a/a/c;[Z)V

    .line 1287
    :cond_d
    iget-object v5, p1, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v5, :cond_10

    invoke-virtual {v0}, Landroid/support/constraint/a/a/c;->b()Z

    move-result v5

    if-nez v5, :cond_10

    .line 1288
    iget-object v5, p1, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->b:Landroid/support/constraint/a/a/b$c;

    sget-object v7, Landroid/support/constraint/a/a/b$c;->d:Landroid/support/constraint/a/a/b$c;

    if-ne v5, v7, :cond_14

    .line 1289
    iget v5, v0, Landroid/support/constraint/a/a/c;->T:I

    invoke-virtual {v0}, Landroid/support/constraint/a/a/c;->i()I

    move-result v7

    sub-int/2addr v5, v7

    add-int/2addr v4, v5

    .line 1294
    :cond_e
    :goto_4
    iget-boolean v5, v0, Landroid/support/constraint/a/a/c;->W:Z

    if-nez v5, :cond_f

    iget-object v5, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v5, :cond_15

    iget-object v5, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v5, :cond_15

    iget-object v5, v0, Landroid/support/constraint/a/a/c;->P:Landroid/support/constraint/a/a/c$a;

    sget-object v7, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-eq v5, v7, :cond_15

    :cond_f
    move v5, v6

    :goto_5
    iput-boolean v5, p1, Landroid/support/constraint/a/a/c;->W:Z

    .line 1297
    iget-boolean v5, p1, Landroid/support/constraint/a/a/c;->W:Z

    if-eqz v5, :cond_10

    iget-object v5, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-nez v5, :cond_16

    .line 1299
    :goto_6
    iget v0, v0, Landroid/support/constraint/a/a/c;->T:I

    sub-int v0, v4, v0

    add-int/2addr v4, v0

    .line 1303
    :cond_10
    iget-object v0, p1, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v0, :cond_3

    invoke-virtual {v3}, Landroid/support/constraint/a/a/c;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1304
    iget-object v0, p1, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0}, Landroid/support/constraint/a/a/b;->c()Landroid/support/constraint/a/a/b$c;

    move-result-object v0

    sget-object v5, Landroid/support/constraint/a/a/b$c;->b:Landroid/support/constraint/a/a/b$c;

    if-ne v0, v5, :cond_17

    .line 1305
    iget v0, v3, Landroid/support/constraint/a/a/c;->S:I

    invoke-virtual {v3}, Landroid/support/constraint/a/a/c;->i()I

    move-result v5

    sub-int/2addr v0, v5

    add-int/2addr v2, v0

    .line 1310
    :cond_11
    :goto_7
    iget-boolean v0, v3, Landroid/support/constraint/a/a/c;->V:Z

    if-nez v0, :cond_12

    iget-object v0, v3, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v0, :cond_13

    iget-object v0, v3, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v0, :cond_13

    iget-object v0, v3, Landroid/support/constraint/a/a/c;->P:Landroid/support/constraint/a/a/c$a;

    sget-object v5, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-eq v0, v5, :cond_13

    :cond_12
    move v1, v6

    :cond_13
    iput-boolean v1, p1, Landroid/support/constraint/a/a/c;->V:Z

    .line 1313
    iget-boolean v0, p1, Landroid/support/constraint/a/a/c;->V:Z

    if-eqz v0, :cond_3

    iget-object v0, v3, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-nez v0, :cond_18

    .line 1315
    :goto_8
    iget v0, v3, Landroid/support/constraint/a/a/c;->S:I

    sub-int v0, v2, v0

    add-int/2addr v2, v0

    goto/16 :goto_2

    .line 1290
    :cond_14
    iget-object v5, p1, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    invoke-virtual {v5}, Landroid/support/constraint/a/a/b;->c()Landroid/support/constraint/a/a/b$c;

    move-result-object v5

    sget-object v7, Landroid/support/constraint/a/a/b$c;->b:Landroid/support/constraint/a/a/b$c;

    if-ne v5, v7, :cond_e

    .line 1291
    iget v5, v0, Landroid/support/constraint/a/a/c;->T:I

    add-int/2addr v4, v5

    goto/16 :goto_4

    :cond_15
    move v5, v1

    .line 1294
    goto :goto_5

    .line 1297
    :cond_16
    iget-object v5, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    if-eq v5, p1, :cond_10

    goto :goto_6

    .line 1306
    :cond_17
    iget-object v0, p1, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0}, Landroid/support/constraint/a/a/b;->c()Landroid/support/constraint/a/a/b$c;

    move-result-object v0

    sget-object v5, Landroid/support/constraint/a/a/b$c;->d:Landroid/support/constraint/a/a/b$c;

    if-ne v0, v5, :cond_11

    .line 1307
    iget v0, v3, Landroid/support/constraint/a/a/c;->S:I

    add-int/2addr v2, v0

    goto :goto_7

    .line 1313
    :cond_18
    iget-object v0, v3, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    if-eq v0, p1, :cond_3

    goto :goto_8

    :cond_19
    move-object v0, v3

    move v4, v2

    goto/16 :goto_3

    :cond_1a
    move v0, v1

    goto/16 :goto_1

    :cond_1b
    move v0, v2

    move v1, v2

    goto/16 :goto_1
.end method

.method public a(Landroid/support/constraint/a/e;I[Z)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x0

    const/4 v5, 0x2

    .line 894
    aput-boolean v0, p3, v5

    .line 895
    invoke-virtual {p0, p1, p2}, Landroid/support/constraint/a/a/d;->b(Landroid/support/constraint/a/e;I)V

    .line 896
    iget-object v1, p0, Landroid/support/constraint/a/a/d;->as:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    .line 897
    :goto_0
    if-ge v1, v2, :cond_2

    .line 898
    iget-object v0, p0, Landroid/support/constraint/a/a/d;->as:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/a/a/c;

    .line 899
    invoke-virtual {v0, p1, p2}, Landroid/support/constraint/a/a/c;->b(Landroid/support/constraint/a/e;I)V

    .line 900
    iget-object v3, v0, Landroid/support/constraint/a/a/c;->P:Landroid/support/constraint/a/a/c$a;

    sget-object v4, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-ne v3, v4, :cond_0

    .line 901
    invoke-virtual {v0}, Landroid/support/constraint/a/a/c;->h()I

    move-result v3

    invoke-virtual {v0}, Landroid/support/constraint/a/a/c;->k()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 902
    aput-boolean v6, p3, v5

    .line 904
    :cond_0
    iget-object v3, v0, Landroid/support/constraint/a/a/c;->Q:Landroid/support/constraint/a/a/c$a;

    sget-object v4, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-ne v3, v4, :cond_1

    .line 905
    invoke-virtual {v0}, Landroid/support/constraint/a/a/c;->l()I

    move-result v3

    invoke-virtual {v0}, Landroid/support/constraint/a/a/c;->m()I

    move-result v0

    if-ge v3, v0, :cond_1

    .line 906
    aput-boolean v6, p3, v5

    .line 897
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 909
    :cond_2
    return-void
.end method

.method public a(Ljava/util/ArrayList;[Z)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/constraint/a/a/c;",
            ">;[Z)V"
        }
    .end annotation

    .prologue
    .line 1462
    const/4 v6, 0x0

    .line 1463
    const/4 v5, 0x0

    .line 1464
    const/4 v4, 0x0

    .line 1465
    const/4 v3, 0x0

    .line 1467
    const/4 v2, 0x0

    .line 1468
    const/4 v1, 0x0

    .line 1469
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v10

    .line 1470
    const/4 v0, 0x0

    const/4 v7, 0x1

    aput-boolean v7, p2, v0

    .line 1473
    const/4 v0, 0x0

    move v9, v0

    :goto_0
    if-ge v9, v10, :cond_8

    .line 1474
    :try_start_0
    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/a/a/c;

    .line 1475
    invoke-virtual {v0}, Landroid/support/constraint/a/a/c;->b()Z

    move-result v7

    if-eqz v7, :cond_0

    move v0, v1

    move v1, v2

    move v2, v3

    move v3, v4

    move v4, v5

    move v5, v6

    .line 1473
    :goto_1
    add-int/lit8 v6, v9, 0x1

    move v9, v6

    move v6, v5

    move v5, v4

    move v4, v3

    move v3, v2

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 1478
    :cond_0
    iget-boolean v7, v0, Landroid/support/constraint/a/a/c;->Z:Z

    if-nez v7, :cond_1

    .line 1479
    invoke-virtual {p0, v0, p2}, Landroid/support/constraint/a/a/d;->a(Landroid/support/constraint/a/a/c;[Z)V

    .line 1481
    :cond_1
    const/4 v7, 0x0

    aget-boolean v7, p2, v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v7, :cond_2

    .line 1515
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v10, :cond_a

    .line 1516
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/a/a/c;

    .line 1517
    const/4 v2, 0x0

    iput-boolean v2, v0, Landroid/support/constraint/a/a/c;->Z:Z

    .line 1518
    const/4 v2, 0x0

    iput-boolean v2, v0, Landroid/support/constraint/a/a/c;->aa:Z

    .line 1519
    const/4 v2, 0x0

    iput-boolean v2, v0, Landroid/support/constraint/a/a/c;->V:Z

    .line 1520
    const/4 v2, 0x0

    iput-boolean v2, v0, Landroid/support/constraint/a/a/c;->W:Z

    .line 1521
    const/4 v2, 0x0

    iput-boolean v2, v0, Landroid/support/constraint/a/a/c;->X:Z

    .line 1522
    const/4 v2, 0x0

    iput-boolean v2, v0, Landroid/support/constraint/a/a/c;->Y:Z

    .line 1515
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1484
    :cond_2
    :try_start_1
    iget-boolean v7, v0, Landroid/support/constraint/a/a/c;->aa:Z

    if-nez v7, :cond_3

    .line 1485
    invoke-virtual {p0, v0, p2}, Landroid/support/constraint/a/a/d;->b(Landroid/support/constraint/a/a/c;[Z)V

    .line 1487
    :cond_3
    const/4 v7, 0x0

    aget-boolean v7, p2, v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v7, :cond_4

    .line 1515
    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, v10, :cond_a

    .line 1516
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/a/a/c;

    .line 1517
    const/4 v2, 0x0

    iput-boolean v2, v0, Landroid/support/constraint/a/a/c;->Z:Z

    .line 1518
    const/4 v2, 0x0

    iput-boolean v2, v0, Landroid/support/constraint/a/a/c;->aa:Z

    .line 1519
    const/4 v2, 0x0

    iput-boolean v2, v0, Landroid/support/constraint/a/a/c;->V:Z

    .line 1520
    const/4 v2, 0x0

    iput-boolean v2, v0, Landroid/support/constraint/a/a/c;->W:Z

    .line 1521
    const/4 v2, 0x0

    iput-boolean v2, v0, Landroid/support/constraint/a/a/c;->X:Z

    .line 1522
    const/4 v2, 0x0

    iput-boolean v2, v0, Landroid/support/constraint/a/a/c;->Y:Z

    .line 1515
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1490
    :cond_4
    :try_start_2
    iget v7, v0, Landroid/support/constraint/a/a/c;->S:I

    iget v8, v0, Landroid/support/constraint/a/a/c;->T:I

    add-int/2addr v7, v8

    invoke-virtual {v0}, Landroid/support/constraint/a/a/c;->h()I

    move-result v8

    sub-int v8, v7, v8

    .line 1491
    iget v7, v0, Landroid/support/constraint/a/a/c;->R:I

    iget v11, v0, Landroid/support/constraint/a/a/c;->U:I

    add-int/2addr v7, v11

    invoke-virtual {v0}, Landroid/support/constraint/a/a/c;->l()I

    move-result v11

    sub-int/2addr v7, v11

    .line 1492
    iget-object v11, v0, Landroid/support/constraint/a/a/c;->P:Landroid/support/constraint/a/a/c$a;

    sget-object v12, Landroid/support/constraint/a/a/c$a;->d:Landroid/support/constraint/a/a/c$a;

    if-ne v11, v12, :cond_5

    .line 1493
    invoke-virtual {v0}, Landroid/support/constraint/a/a/c;->h()I

    move-result v8

    iget-object v11, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget v11, v11, Landroid/support/constraint/a/a/b;->d:I

    add-int/2addr v8, v11

    iget-object v11, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget v11, v11, Landroid/support/constraint/a/a/b;->d:I

    add-int/2addr v8, v11

    .line 1495
    :cond_5
    iget-object v11, v0, Landroid/support/constraint/a/a/c;->Q:Landroid/support/constraint/a/a/c$a;

    sget-object v12, Landroid/support/constraint/a/a/c$a;->d:Landroid/support/constraint/a/a/c$a;

    if-ne v11, v12, :cond_6

    .line 1497
    invoke-virtual {v0}, Landroid/support/constraint/a/a/c;->l()I

    move-result v7

    iget-object v11, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget v11, v11, Landroid/support/constraint/a/a/b;->d:I

    add-int/2addr v7, v11

    iget-object v11, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget v11, v11, Landroid/support/constraint/a/a/b;->d:I

    add-int/2addr v7, v11

    .line 1499
    :cond_6
    invoke-virtual {v0}, Landroid/support/constraint/a/a/c;->d()I

    move-result v11

    const/16 v12, 0x8

    if-ne v11, v12, :cond_7

    .line 1500
    const/4 v8, 0x0

    .line 1501
    const/4 v7, 0x0

    .line 1503
    :cond_7
    iget v11, v0, Landroid/support/constraint/a/a/c;->S:I

    invoke-static {v5, v11}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 1504
    iget v11, v0, Landroid/support/constraint/a/a/c;->T:I

    invoke-static {v4, v11}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 1505
    iget v11, v0, Landroid/support/constraint/a/a/c;->U:I

    invoke-static {v3, v11}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1506
    iget v0, v0, Landroid/support/constraint/a/a/c;->R:I

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 1507
    invoke-static {v2, v8}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1508
    invoke-static {v1, v7}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v2

    move v2, v3

    move v3, v4

    move v4, v5

    move v5, v6

    goto/16 :goto_1

    .line 1510
    :cond_8
    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1511
    iget v4, p0, Landroid/support/constraint/a/a/d;->K:I

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/a/a/d;->am:I

    .line 1512
    invoke-static {v6, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1513
    iget v2, p0, Landroid/support/constraint/a/a/d;->L:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/a/a/d;->an:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1515
    const/4 v0, 0x0

    move v1, v0

    :goto_4
    if-ge v1, v10, :cond_a

    .line 1516
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/a/a/c;

    .line 1517
    const/4 v2, 0x0

    iput-boolean v2, v0, Landroid/support/constraint/a/a/c;->Z:Z

    .line 1518
    const/4 v2, 0x0

    iput-boolean v2, v0, Landroid/support/constraint/a/a/c;->aa:Z

    .line 1519
    const/4 v2, 0x0

    iput-boolean v2, v0, Landroid/support/constraint/a/a/c;->V:Z

    .line 1520
    const/4 v2, 0x0

    iput-boolean v2, v0, Landroid/support/constraint/a/a/c;->W:Z

    .line 1521
    const/4 v2, 0x0

    iput-boolean v2, v0, Landroid/support/constraint/a/a/c;->X:Z

    .line 1522
    const/4 v2, 0x0

    iput-boolean v2, v0, Landroid/support/constraint/a/a/c;->Y:Z

    .line 1515
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :catchall_0
    move-exception v0

    move-object v1, v0

    const/4 v0, 0x0

    move v2, v0

    :goto_5
    if-ge v2, v10, :cond_9

    .line 1516
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/a/a/c;

    .line 1517
    const/4 v3, 0x0

    iput-boolean v3, v0, Landroid/support/constraint/a/a/c;->Z:Z

    .line 1518
    const/4 v3, 0x0

    iput-boolean v3, v0, Landroid/support/constraint/a/a/c;->aa:Z

    .line 1519
    const/4 v3, 0x0

    iput-boolean v3, v0, Landroid/support/constraint/a/a/c;->V:Z

    .line 1520
    const/4 v3, 0x0

    iput-boolean v3, v0, Landroid/support/constraint/a/a/c;->W:Z

    .line 1521
    const/4 v3, 0x0

    iput-boolean v3, v0, Landroid/support/constraint/a/a/c;->X:Z

    .line 1522
    const/4 v3, 0x0

    iput-boolean v3, v0, Landroid/support/constraint/a/a/c;->Y:Z

    .line 1515
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    :cond_9
    throw v1

    .line 1525
    :cond_a
    return-void
.end method

.method public b(Landroid/support/constraint/a/a/c;[Z)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/16 v8, 0x8

    const/4 v4, -0x1

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 1328
    iget-object v0, p1, Landroid/support/constraint/a/a/c;->Q:Landroid/support/constraint/a/a/c$a;

    sget-object v2, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-ne v0, v2, :cond_0

    .line 1329
    iget-object v0, p1, Landroid/support/constraint/a/a/c;->P:Landroid/support/constraint/a/a/c$a;

    sget-object v2, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-eq v0, v2, :cond_0

    iget v0, p1, Landroid/support/constraint/a/a/c;->z:F

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    .line 1331
    aput-boolean v1, p2, v1

    .line 1454
    :goto_0
    return-void

    .line 1338
    :cond_0
    iget-object v0, p1, Landroid/support/constraint/a/a/c;->Q:Landroid/support/constraint/a/a/c$a;

    sget-object v2, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-ne v0, v2, :cond_1

    iget v0, p1, Landroid/support/constraint/a/a/c;->d:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    .line 1340
    aput-boolean v1, p2, v1

    goto :goto_0

    .line 1344
    :cond_1
    invoke-virtual {p1}, Landroid/support/constraint/a/a/c;->j()I

    move-result v2

    .line 1350
    iput-boolean v6, p1, Landroid/support/constraint/a/a/c;->aa:Z

    .line 1352
    instance-of v0, p1, Landroid/support/constraint/a/a/e;

    if-eqz v0, :cond_6

    move-object v0, p1

    .line 1353
    check-cast v0, Landroid/support/constraint/a/a/e;

    .line 1354
    invoke-virtual {v0}, Landroid/support/constraint/a/a/e;->D()I

    move-result v3

    if-nez v3, :cond_1d

    .line 1357
    invoke-virtual {v0}, Landroid/support/constraint/a/a/e;->F()I

    move-result v2

    if-eq v2, v4, :cond_4

    .line 1358
    invoke-virtual {v0}, Landroid/support/constraint/a/a/e;->F()I

    move-result v2

    move v0, v1

    move v1, v2

    :goto_1
    move v2, v0

    move v4, v1

    .line 1447
    :cond_2
    :goto_2
    invoke-virtual {p1}, Landroid/support/constraint/a/a/c;->d()I

    move-result v0

    if-ne v0, v8, :cond_3

    .line 1448
    iget v0, p1, Landroid/support/constraint/a/a/c;->y:I

    sub-int/2addr v4, v0

    .line 1449
    iget v0, p1, Landroid/support/constraint/a/a/c;->y:I

    sub-int/2addr v2, v0

    .line 1452
    :cond_3
    iput v4, p1, Landroid/support/constraint/a/a/c;->R:I

    .line 1453
    iput v2, p1, Landroid/support/constraint/a/a/c;->U:I

    goto :goto_0

    .line 1359
    :cond_4
    invoke-virtual {v0}, Landroid/support/constraint/a/a/e;->G()I

    move-result v2

    if-eq v2, v4, :cond_5

    .line 1360
    invoke-virtual {v0}, Landroid/support/constraint/a/a/e;->G()I

    move-result v0

    goto :goto_1

    .line 1361
    :cond_5
    invoke-virtual {v0}, Landroid/support/constraint/a/a/e;->E()F

    move-result v0

    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_1c

    .line 1362
    aput-boolean v1, p2, v1

    goto :goto_0

    .line 1366
    :cond_6
    iget-object v0, p1, Landroid/support/constraint/a/a/c;->q:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-nez v0, :cond_7

    iget-object v0, p1, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-nez v0, :cond_7

    iget-object v0, p1, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-nez v0, :cond_7

    .line 1367
    invoke-virtual {p1}, Landroid/support/constraint/a/a/c;->g()I

    move-result v0

    add-int v4, v2, v0

    goto :goto_2

    .line 1369
    :cond_7
    iget-object v0, p1, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v0, :cond_8

    iget-object v0, p1, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v0, :cond_8

    iget-boolean v0, p1, Landroid/support/constraint/a/a/c;->l:Z

    if-eqz v0, :cond_8

    iget-object v0, p1, Landroid/support/constraint/a/a/c;->P:Landroid/support/constraint/a/a/c$a;

    sget-object v4, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-ne v0, v4, :cond_8

    .line 1372
    aput-boolean v1, p2, v1

    goto/16 :goto_0

    .line 1375
    :cond_8
    iget-object v0, p1, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v0, :cond_a

    iget-object v0, p1, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v0, :cond_a

    iget-object v0, p1, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v4, p1, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eq v0, v4, :cond_9

    iget-object v0, p1, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v4, p1, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    if-ne v0, v4, :cond_a

    iget-object v0, p1, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    iget-object v4, p1, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    if-eq v0, v4, :cond_a

    .line 1379
    :cond_9
    aput-boolean v1, p2, v1

    goto/16 :goto_0

    .line 1382
    :cond_a
    iget-object v0, p1, Landroid/support/constraint/a/a/c;->q:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0}, Landroid/support/constraint/a/a/b;->j()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1383
    iget-object v0, p1, Landroid/support/constraint/a/a/c;->q:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0}, Landroid/support/constraint/a/a/b;->b()Landroid/support/constraint/a/a/c;

    move-result-object v0

    .line 1384
    iget-boolean v1, v0, Landroid/support/constraint/a/a/c;->aa:Z

    if-nez v1, :cond_b

    .line 1385
    invoke-virtual {p0, v0, p2}, Landroid/support/constraint/a/a/d;->b(Landroid/support/constraint/a/a/c;[Z)V

    .line 1387
    :cond_b
    iget v1, v0, Landroid/support/constraint/a/a/c;->R:I

    iget v3, v0, Landroid/support/constraint/a/a/c;->y:I

    sub-int/2addr v1, v3

    add-int/2addr v1, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1388
    iget v3, v0, Landroid/support/constraint/a/a/c;->U:I

    iget v0, v0, Landroid/support/constraint/a/a/c;->y:I

    sub-int v0, v3, v0

    add-int/2addr v0, v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1389
    invoke-virtual {p1}, Landroid/support/constraint/a/a/c;->d()I

    move-result v2

    if-ne v2, v8, :cond_c

    .line 1390
    iget v2, p1, Landroid/support/constraint/a/a/c;->y:I

    sub-int/2addr v1, v2

    .line 1391
    iget v2, p1, Landroid/support/constraint/a/a/c;->y:I

    sub-int/2addr v0, v2

    .line 1393
    :cond_c
    iput v1, p1, Landroid/support/constraint/a/a/c;->R:I

    .line 1394
    iput v0, p1, Landroid/support/constraint/a/a/c;->U:I

    goto/16 :goto_0

    .line 1397
    :cond_d
    iget-object v0, p1, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0}, Landroid/support/constraint/a/a/b;->j()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 1398
    iget-object v0, p1, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0}, Landroid/support/constraint/a/a/b;->b()Landroid/support/constraint/a/a/c;

    move-result-object v0

    .line 1399
    iget-object v4, p1, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v4

    add-int/2addr v4, v2

    .line 1400
    invoke-virtual {v0}, Landroid/support/constraint/a/a/c;->b()Z

    move-result v5

    if-nez v5, :cond_e

    iget-boolean v5, v0, Landroid/support/constraint/a/a/c;->aa:Z

    if-nez v5, :cond_e

    .line 1401
    invoke-virtual {p0, v0, p2}, Landroid/support/constraint/a/a/d;->b(Landroid/support/constraint/a/a/c;[Z)V

    .line 1404
    :cond_e
    :goto_3
    iget-object v5, p1, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    invoke-virtual {v5}, Landroid/support/constraint/a/a/b;->j()Z

    move-result v5

    if-eqz v5, :cond_f

    .line 1405
    iget-object v3, p1, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v3, v3, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    invoke-virtual {v3}, Landroid/support/constraint/a/a/b;->b()Landroid/support/constraint/a/a/c;

    move-result-object v3

    .line 1406
    iget-object v5, p1, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    invoke-virtual {v5}, Landroid/support/constraint/a/a/b;->d()I

    move-result v5

    add-int/2addr v2, v5

    .line 1407
    invoke-virtual {v3}, Landroid/support/constraint/a/a/c;->b()Z

    move-result v5

    if-nez v5, :cond_f

    iget-boolean v5, v3, Landroid/support/constraint/a/a/c;->aa:Z

    if-nez v5, :cond_f

    .line 1408
    invoke-virtual {p0, v3, p2}, Landroid/support/constraint/a/a/d;->b(Landroid/support/constraint/a/a/c;[Z)V

    .line 1412
    :cond_f
    iget-object v5, p1, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v5, :cond_12

    invoke-virtual {v0}, Landroid/support/constraint/a/a/c;->b()Z

    move-result v5

    if-nez v5, :cond_12

    .line 1413
    iget-object v5, p1, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    invoke-virtual {v5}, Landroid/support/constraint/a/a/b;->c()Landroid/support/constraint/a/a/b$c;

    move-result-object v5

    sget-object v7, Landroid/support/constraint/a/a/b$c;->c:Landroid/support/constraint/a/a/b$c;

    if-ne v5, v7, :cond_16

    .line 1414
    iget v5, v0, Landroid/support/constraint/a/a/c;->R:I

    invoke-virtual {v0}, Landroid/support/constraint/a/a/c;->j()I

    move-result v7

    sub-int/2addr v5, v7

    add-int/2addr v4, v5

    .line 1419
    :cond_10
    :goto_4
    iget-boolean v5, v0, Landroid/support/constraint/a/a/c;->X:Z

    if-nez v5, :cond_11

    iget-object v5, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v5, :cond_17

    iget-object v5, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    if-eq v5, p1, :cond_17

    iget-object v5, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v5, :cond_17

    iget-object v5, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    if-eq v5, p1, :cond_17

    iget-object v5, v0, Landroid/support/constraint/a/a/c;->Q:Landroid/support/constraint/a/a/c$a;

    sget-object v7, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-eq v5, v7, :cond_17

    :cond_11
    move v5, v6

    :goto_5
    iput-boolean v5, p1, Landroid/support/constraint/a/a/c;->X:Z

    .line 1424
    iget-boolean v5, p1, Landroid/support/constraint/a/a/c;->X:Z

    if-eqz v5, :cond_12

    iget-object v5, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-nez v5, :cond_18

    .line 1426
    :goto_6
    iget v0, v0, Landroid/support/constraint/a/a/c;->R:I

    sub-int v0, v4, v0

    add-int/2addr v4, v0

    .line 1429
    :cond_12
    iget-object v0, p1, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v0, :cond_2

    invoke-virtual {v3}, Landroid/support/constraint/a/a/c;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1430
    iget-object v0, p1, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0}, Landroid/support/constraint/a/a/b;->c()Landroid/support/constraint/a/a/b$c;

    move-result-object v0

    sget-object v5, Landroid/support/constraint/a/a/b$c;->e:Landroid/support/constraint/a/a/b$c;

    if-ne v0, v5, :cond_19

    .line 1431
    iget v0, v3, Landroid/support/constraint/a/a/c;->U:I

    invoke-virtual {v3}, Landroid/support/constraint/a/a/c;->j()I

    move-result v5

    sub-int/2addr v0, v5

    add-int/2addr v2, v0

    .line 1436
    :cond_13
    :goto_7
    iget-boolean v0, v3, Landroid/support/constraint/a/a/c;->Y:Z

    if-nez v0, :cond_14

    iget-object v0, v3, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v0, :cond_15

    iget-object v0, v3, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    if-eq v0, p1, :cond_15

    iget-object v0, v3, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v0, :cond_15

    iget-object v0, v3, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    if-eq v0, p1, :cond_15

    iget-object v0, v3, Landroid/support/constraint/a/a/c;->Q:Landroid/support/constraint/a/a/c$a;

    sget-object v5, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-eq v0, v5, :cond_15

    :cond_14
    move v1, v6

    :cond_15
    iput-boolean v1, p1, Landroid/support/constraint/a/a/c;->Y:Z

    .line 1441
    iget-boolean v0, p1, Landroid/support/constraint/a/a/c;->Y:Z

    if-eqz v0, :cond_2

    iget-object v0, v3, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-nez v0, :cond_1a

    .line 1443
    :goto_8
    iget v0, v3, Landroid/support/constraint/a/a/c;->U:I

    sub-int v0, v2, v0

    add-int/2addr v2, v0

    goto/16 :goto_2

    .line 1415
    :cond_16
    iget-object v5, p1, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    invoke-virtual {v5}, Landroid/support/constraint/a/a/b;->c()Landroid/support/constraint/a/a/b$c;

    move-result-object v5

    sget-object v7, Landroid/support/constraint/a/a/b$c;->e:Landroid/support/constraint/a/a/b$c;

    if-ne v5, v7, :cond_10

    .line 1416
    iget v5, v0, Landroid/support/constraint/a/a/c;->R:I

    add-int/2addr v4, v5

    goto/16 :goto_4

    :cond_17
    move v5, v1

    .line 1419
    goto :goto_5

    .line 1424
    :cond_18
    iget-object v5, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    if-eq v5, p1, :cond_12

    goto :goto_6

    .line 1432
    :cond_19
    iget-object v0, p1, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0}, Landroid/support/constraint/a/a/b;->c()Landroid/support/constraint/a/a/b$c;

    move-result-object v0

    sget-object v5, Landroid/support/constraint/a/a/b$c;->c:Landroid/support/constraint/a/a/b$c;

    if-ne v0, v5, :cond_13

    .line 1433
    iget v0, v3, Landroid/support/constraint/a/a/c;->U:I

    add-int/2addr v2, v0

    goto :goto_7

    .line 1441
    :cond_1a
    iget-object v0, v3, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v0, v0, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    if-eq v0, p1, :cond_2

    goto :goto_8

    :cond_1b
    move-object v0, v3

    move v4, v2

    goto/16 :goto_3

    :cond_1c
    move v0, v1

    goto/16 :goto_1

    :cond_1d
    move v0, v2

    move v1, v2

    goto/16 :goto_1
.end method

.method public c(Landroid/support/constraint/a/e;I)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 210
    invoke-virtual {p0, p1, p2}, Landroid/support/constraint/a/a/d;->a(Landroid/support/constraint/a/e;I)V

    .line 211
    iget-object v1, p0, Landroid/support/constraint/a/a/d;->as:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 213
    iget v1, p0, Landroid/support/constraint/a/a/d;->az:I

    const/4 v3, 0x2

    if-eq v1, v3, :cond_0

    iget v1, p0, Landroid/support/constraint/a/a/d;->az:I

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 215
    :cond_0
    invoke-direct {p0, p1}, Landroid/support/constraint/a/a/d;->a(Landroid/support/constraint/a/e;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 253
    :goto_0
    return v0

    :cond_1
    move v1, v2

    :goto_1
    move v3, v0

    .line 222
    :goto_2
    if-ge v3, v4, :cond_8

    .line 223
    iget-object v0, p0, Landroid/support/constraint/a/a/d;->as:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/a/a/c;

    .line 224
    instance-of v5, v0, Landroid/support/constraint/a/a/d;

    if-eqz v5, :cond_6

    .line 225
    iget-object v5, v0, Landroid/support/constraint/a/a/c;->P:Landroid/support/constraint/a/a/c$a;

    .line 226
    iget-object v6, v0, Landroid/support/constraint/a/a/c;->Q:Landroid/support/constraint/a/a/c$a;

    .line 227
    sget-object v7, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    if-ne v5, v7, :cond_2

    .line 228
    sget-object v7, Landroid/support/constraint/a/a/c$a;->a:Landroid/support/constraint/a/a/c$a;

    invoke-virtual {v0, v7}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/c$a;)V

    .line 230
    :cond_2
    sget-object v7, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    if-ne v6, v7, :cond_3

    .line 231
    sget-object v7, Landroid/support/constraint/a/a/c$a;->a:Landroid/support/constraint/a/a/c$a;

    invoke-virtual {v0, v7}, Landroid/support/constraint/a/a/c;->b(Landroid/support/constraint/a/a/c$a;)V

    .line 233
    :cond_3
    invoke-virtual {v0, p1, p2}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/e;I)V

    .line 234
    sget-object v7, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    if-ne v5, v7, :cond_4

    .line 235
    invoke-virtual {v0, v5}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/c$a;)V

    .line 237
    :cond_4
    sget-object v5, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    if-ne v6, v5, :cond_5

    .line 238
    invoke-virtual {v0, v6}, Landroid/support/constraint/a/a/c;->b(Landroid/support/constraint/a/a/c$a;)V

    .line 222
    :cond_5
    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 241
    :cond_6
    if-eqz v1, :cond_7

    .line 242
    invoke-static {p0, p1, v0}, Landroid/support/constraint/a/a/g;->a(Landroid/support/constraint/a/a/d;Landroid/support/constraint/a/e;Landroid/support/constraint/a/a/c;)V

    .line 244
    :cond_7
    invoke-virtual {v0, p1, p2}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/e;I)V

    goto :goto_3

    .line 247
    :cond_8
    iget v0, p0, Landroid/support/constraint/a/a/d;->au:I

    if-lez v0, :cond_9

    .line 248
    invoke-direct {p0, p1}, Landroid/support/constraint/a/a/d;->b(Landroid/support/constraint/a/e;)V

    .line 250
    :cond_9
    iget v0, p0, Landroid/support/constraint/a/a/d;->av:I

    if-lez v0, :cond_a

    .line 251
    invoke-direct {p0, p1}, Landroid/support/constraint/a/a/d;->c(Landroid/support/constraint/a/e;)V

    :cond_a
    move v0, v2

    .line 253
    goto :goto_0

    :cond_b
    move v1, v0

    goto :goto_1
.end method
