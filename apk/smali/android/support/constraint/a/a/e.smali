.class public Landroid/support/constraint/a/a/e;
.super Landroid/support/constraint/a/a/c;
.source "Guideline.java"


# instance fields
.field protected aj:F

.field protected ak:I

.field protected al:I

.field private am:Landroid/support/constraint/a/a/b;

.field private an:I

.field private ao:Z

.field private ap:I

.field private aq:Landroid/support/constraint/a/a/h;

.field private ar:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 44
    invoke-direct {p0}, Landroid/support/constraint/a/a/c;-><init>()V

    .line 32
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Landroid/support/constraint/a/a/e;->aj:F

    .line 33
    iput v2, p0, Landroid/support/constraint/a/a/e;->ak:I

    .line 34
    iput v2, p0, Landroid/support/constraint/a/a/e;->al:I

    .line 36
    iget-object v0, p0, Landroid/support/constraint/a/a/e;->n:Landroid/support/constraint/a/a/b;

    iput-object v0, p0, Landroid/support/constraint/a/a/e;->am:Landroid/support/constraint/a/a/b;

    .line 37
    iput v1, p0, Landroid/support/constraint/a/a/e;->an:I

    .line 38
    iput-boolean v1, p0, Landroid/support/constraint/a/a/e;->ao:Z

    .line 39
    iput v1, p0, Landroid/support/constraint/a/a/e;->ap:I

    .line 41
    new-instance v0, Landroid/support/constraint/a/a/h;

    invoke-direct {v0}, Landroid/support/constraint/a/a/h;-><init>()V

    iput-object v0, p0, Landroid/support/constraint/a/a/e;->aq:Landroid/support/constraint/a/a/h;

    .line 42
    const/16 v0, 0x8

    iput v0, p0, Landroid/support/constraint/a/a/e;->ar:I

    .line 45
    iget-object v0, p0, Landroid/support/constraint/a/a/e;->v:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 46
    iget-object v0, p0, Landroid/support/constraint/a/a/e;->v:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/constraint/a/a/e;->am:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 47
    return-void
.end method


# virtual methods
.method public D()I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Landroid/support/constraint/a/a/e;->an:I

    return v0
.end method

.method public E()F
    .locals 1

    .prologue
    .line 177
    iget v0, p0, Landroid/support/constraint/a/a/e;->aj:F

    return v0
.end method

.method public F()I
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Landroid/support/constraint/a/a/e;->ak:I

    return v0
.end method

.method public G()I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Landroid/support/constraint/a/a/e;->al:I

    return v0
.end method

.method public a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;
    .locals 2

    .prologue
    .line 118
    sget-object v0, Landroid/support/constraint/a/a/e$1;->a:[I

    invoke-virtual {p1}, Landroid/support/constraint/a/a/b$c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 140
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-virtual {p1}, Landroid/support/constraint/a/a/b$c;->name()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 121
    :pswitch_0
    iget v0, p0, Landroid/support/constraint/a/a/e;->an:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 122
    iget-object v0, p0, Landroid/support/constraint/a/a/e;->am:Landroid/support/constraint/a/a/b;

    .line 138
    :goto_0
    return-object v0

    .line 128
    :pswitch_1
    iget v0, p0, Landroid/support/constraint/a/a/e;->an:I

    if-nez v0, :cond_0

    .line 129
    iget-object v0, p0, Landroid/support/constraint/a/a/e;->am:Landroid/support/constraint/a/a/b;

    goto :goto_0

    .line 138
    :pswitch_2
    const/4 v0, 0x0

    goto :goto_0

    .line 118
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 74
    iget v0, p0, Landroid/support/constraint/a/a/e;->an:I

    if-ne v0, p1, :cond_0

    .line 85
    :goto_0
    return-void

    .line 77
    :cond_0
    iput p1, p0, Landroid/support/constraint/a/a/e;->an:I

    .line 78
    iget-object v0, p0, Landroid/support/constraint/a/a/e;->v:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 79
    iget v0, p0, Landroid/support/constraint/a/a/e;->an:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 80
    iget-object v0, p0, Landroid/support/constraint/a/a/e;->m:Landroid/support/constraint/a/a/b;

    iput-object v0, p0, Landroid/support/constraint/a/a/e;->am:Landroid/support/constraint/a/a/b;

    .line 84
    :goto_1
    iget-object v0, p0, Landroid/support/constraint/a/a/e;->v:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/constraint/a/a/e;->am:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 82
    :cond_1
    iget-object v0, p0, Landroid/support/constraint/a/a/e;->n:Landroid/support/constraint/a/a/b;

    iput-object v0, p0, Landroid/support/constraint/a/a/e;->am:Landroid/support/constraint/a/a/b;

    goto :goto_1
.end method

.method public a(Landroid/support/constraint/a/e;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 190
    invoke-virtual {p0}, Landroid/support/constraint/a/a/e;->c()Landroid/support/constraint/a/a/c;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/a/a/d;

    .line 191
    if-nez v0, :cond_1

    .line 220
    :cond_0
    :goto_0
    return-void

    .line 194
    :cond_1
    sget-object v1, Landroid/support/constraint/a/a/b$c;->b:Landroid/support/constraint/a/a/b$c;

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/d;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v2

    .line 195
    sget-object v1, Landroid/support/constraint/a/a/b$c;->d:Landroid/support/constraint/a/a/b$c;

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/d;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v1

    .line 196
    iget v3, p0, Landroid/support/constraint/a/a/e;->an:I

    if-nez v3, :cond_4

    .line 197
    sget-object v1, Landroid/support/constraint/a/a/b$c;->c:Landroid/support/constraint/a/a/b$c;

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/d;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v1

    .line 198
    sget-object v2, Landroid/support/constraint/a/a/b$c;->e:Landroid/support/constraint/a/a/b$c;

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/a/d;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v0

    move-object v2, v1

    .line 200
    :goto_1
    iget v1, p0, Landroid/support/constraint/a/a/e;->ak:I

    if-eq v1, v4, :cond_2

    .line 201
    iget-object v0, p0, Landroid/support/constraint/a/a/e;->am:Landroid/support/constraint/a/a/b;

    invoke-virtual {p1, v0}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v0

    .line 202
    invoke-virtual {p1, v2}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v1

    .line 203
    iget v2, p0, Landroid/support/constraint/a/a/e;->ak:I

    .line 205
    invoke-static {p1, v0, v1, v2, v5}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/e;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;IZ)Landroid/support/constraint/a/b;

    move-result-object v0

    .line 203
    invoke-virtual {p1, v0}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    goto :goto_0

    .line 206
    :cond_2
    iget v1, p0, Landroid/support/constraint/a/a/e;->al:I

    if-eq v1, v4, :cond_3

    .line 207
    iget-object v1, p0, Landroid/support/constraint/a/a/e;->am:Landroid/support/constraint/a/a/b;

    invoke-virtual {p1, v1}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v1

    .line 208
    invoke-virtual {p1, v0}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v0

    .line 209
    iget v2, p0, Landroid/support/constraint/a/a/e;->al:I

    neg-int v2, v2

    .line 211
    invoke-static {p1, v1, v0, v2, v5}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/e;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;IZ)Landroid/support/constraint/a/b;

    move-result-object v0

    .line 209
    invoke-virtual {p1, v0}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    goto :goto_0

    .line 212
    :cond_3
    iget v1, p0, Landroid/support/constraint/a/a/e;->aj:F

    const/high16 v3, -0x40800000    # -1.0f

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_0

    .line 213
    iget-object v1, p0, Landroid/support/constraint/a/a/e;->am:Landroid/support/constraint/a/a/b;

    invoke-virtual {p1, v1}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v1

    .line 214
    invoke-virtual {p1, v2}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v2

    .line 215
    invoke-virtual {p1, v0}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v3

    .line 216
    iget v4, p0, Landroid/support/constraint/a/a/e;->aj:F

    iget-boolean v5, p0, Landroid/support/constraint/a/a/e;->ao:Z

    move-object v0, p1

    .line 217
    invoke-static/range {v0 .. v5}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/e;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;FZ)Landroid/support/constraint/a/b;

    move-result-object v0

    .line 216
    invoke-virtual {p1, v0}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method public b(Landroid/support/constraint/a/e;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 224
    invoke-virtual {p0}, Landroid/support/constraint/a/a/e;->c()Landroid/support/constraint/a/a/c;

    move-result-object v0

    if-nez v0, :cond_0

    .line 239
    :goto_0
    return-void

    .line 227
    :cond_0
    iget-object v0, p0, Landroid/support/constraint/a/a/e;->am:Landroid/support/constraint/a/a/b;

    invoke-virtual {p1, v0}, Landroid/support/constraint/a/e;->b(Ljava/lang/Object;)I

    move-result v0

    .line 228
    iget v1, p0, Landroid/support/constraint/a/a/e;->an:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 229
    invoke-virtual {p0, v0}, Landroid/support/constraint/a/a/e;->c(I)V

    .line 230
    invoke-virtual {p0, v3}, Landroid/support/constraint/a/a/e;->d(I)V

    .line 231
    invoke-virtual {p0}, Landroid/support/constraint/a/a/e;->c()Landroid/support/constraint/a/a/c;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/constraint/a/a/c;->l()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/constraint/a/a/e;->f(I)V

    .line 232
    invoke-virtual {p0, v3}, Landroid/support/constraint/a/a/e;->e(I)V

    goto :goto_0

    .line 234
    :cond_1
    invoke-virtual {p0, v3}, Landroid/support/constraint/a/a/e;->c(I)V

    .line 235
    invoke-virtual {p0, v0}, Landroid/support/constraint/a/a/e;->d(I)V

    .line 236
    invoke-virtual {p0}, Landroid/support/constraint/a/a/e;->c()Landroid/support/constraint/a/a/c;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/constraint/a/a/c;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/constraint/a/a/e;->e(I)V

    .line 237
    invoke-virtual {p0, v3}, Landroid/support/constraint/a/a/e;->f(I)V

    goto :goto_0
.end method

.method public e(F)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 153
    const/high16 v0, -0x40800000    # -1.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    .line 154
    iput p1, p0, Landroid/support/constraint/a/a/e;->aj:F

    .line 155
    iput v1, p0, Landroid/support/constraint/a/a/e;->ak:I

    .line 156
    iput v1, p0, Landroid/support/constraint/a/a/e;->al:I

    .line 158
    :cond_0
    return-void
.end method

.method public n(I)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 161
    if-le p1, v1, :cond_0

    .line 162
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Landroid/support/constraint/a/a/e;->aj:F

    .line 163
    iput p1, p0, Landroid/support/constraint/a/a/e;->ak:I

    .line 164
    iput v1, p0, Landroid/support/constraint/a/a/e;->al:I

    .line 166
    :cond_0
    return-void
.end method

.method public o(I)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 169
    if-le p1, v1, :cond_0

    .line 170
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Landroid/support/constraint/a/a/e;->aj:F

    .line 171
    iput v1, p0, Landroid/support/constraint/a/a/e;->ak:I

    .line 172
    iput p1, p0, Landroid/support/constraint/a/a/e;->al:I

    .line 174
    :cond_0
    return-void
.end method

.method public y()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/constraint/a/a/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145
    iget-object v0, p0, Landroid/support/constraint/a/a/e;->v:Ljava/util/ArrayList;

    return-object v0
.end method
