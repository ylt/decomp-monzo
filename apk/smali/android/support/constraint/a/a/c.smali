.class public Landroid/support/constraint/a/a/c;
.super Ljava/lang/Object;
.source "ConstraintWidget.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/constraint/a/a/c$a;
    }
.end annotation


# static fields
.field public static M:F


# instance fields
.field protected A:I

.field protected B:I

.field protected C:I

.field protected D:I

.field protected E:I

.field protected F:I

.field protected G:I

.field protected H:I

.field protected I:I

.field J:I

.field protected K:I

.field protected L:I

.field N:F

.field O:F

.field P:Landroid/support/constraint/a/a/c$a;

.field Q:Landroid/support/constraint/a/a/c$a;

.field R:I

.field S:I

.field T:I

.field U:I

.field V:Z

.field W:Z

.field X:Z

.field Y:Z

.field Z:Z

.field public a:I

.field aa:Z

.field ab:I

.field ac:I

.field ad:Z

.field ae:Z

.field af:F

.field ag:F

.field ah:Landroid/support/constraint/a/a/c;

.field ai:Landroid/support/constraint/a/a/c;

.field private aj:I

.field private ak:I

.field private al:I

.field private am:I

.field private an:I

.field private ao:I

.field private ap:Ljava/lang/Object;

.field private aq:I

.field private ar:I

.field private as:Ljava/lang/String;

.field private at:Ljava/lang/String;

.field public b:I

.field c:I

.field d:I

.field e:I

.field f:I

.field g:F

.field h:I

.field i:I

.field j:F

.field k:Z

.field l:Z

.field m:Landroid/support/constraint/a/a/b;

.field n:Landroid/support/constraint/a/a/b;

.field o:Landroid/support/constraint/a/a/b;

.field p:Landroid/support/constraint/a/a/b;

.field q:Landroid/support/constraint/a/a/b;

.field r:Landroid/support/constraint/a/a/b;

.field s:Landroid/support/constraint/a/a/b;

.field t:Landroid/support/constraint/a/a/b;

.field protected u:[Landroid/support/constraint/a/a/b;

.field protected v:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/constraint/a/a/b;",
            ">;"
        }
    .end annotation
.end field

.field w:Landroid/support/constraint/a/a/c;

.field x:I

.field y:I

.field protected z:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 151
    const/high16 v0, 0x3f000000    # 0.5f

    sput v0, Landroid/support/constraint/a/a/c;->M:F

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v6, -0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput v6, p0, Landroid/support/constraint/a/a/c;->a:I

    .line 66
    iput v6, p0, Landroid/support/constraint/a/a/c;->b:I

    .line 70
    iput v3, p0, Landroid/support/constraint/a/a/c;->c:I

    .line 71
    iput v3, p0, Landroid/support/constraint/a/a/c;->d:I

    .line 72
    iput v3, p0, Landroid/support/constraint/a/a/c;->e:I

    .line 73
    iput v3, p0, Landroid/support/constraint/a/a/c;->f:I

    .line 74
    iput v0, p0, Landroid/support/constraint/a/a/c;->g:F

    .line 75
    iput v3, p0, Landroid/support/constraint/a/a/c;->h:I

    .line 76
    iput v3, p0, Landroid/support/constraint/a/a/c;->i:I

    .line 77
    iput v0, p0, Landroid/support/constraint/a/a/c;->j:F

    .line 97
    new-instance v0, Landroid/support/constraint/a/a/b;

    sget-object v1, Landroid/support/constraint/a/a/b$c;->b:Landroid/support/constraint/a/a/b$c;

    invoke-direct {v0, p0, v1}, Landroid/support/constraint/a/a/b;-><init>(Landroid/support/constraint/a/a/c;Landroid/support/constraint/a/a/b$c;)V

    iput-object v0, p0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    .line 98
    new-instance v0, Landroid/support/constraint/a/a/b;

    sget-object v1, Landroid/support/constraint/a/a/b$c;->c:Landroid/support/constraint/a/a/b$c;

    invoke-direct {v0, p0, v1}, Landroid/support/constraint/a/a/b;-><init>(Landroid/support/constraint/a/a/c;Landroid/support/constraint/a/a/b$c;)V

    iput-object v0, p0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    .line 99
    new-instance v0, Landroid/support/constraint/a/a/b;

    sget-object v1, Landroid/support/constraint/a/a/b$c;->d:Landroid/support/constraint/a/a/b$c;

    invoke-direct {v0, p0, v1}, Landroid/support/constraint/a/a/b;-><init>(Landroid/support/constraint/a/a/c;Landroid/support/constraint/a/a/b$c;)V

    iput-object v0, p0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    .line 100
    new-instance v0, Landroid/support/constraint/a/a/b;

    sget-object v1, Landroid/support/constraint/a/a/b$c;->e:Landroid/support/constraint/a/a/b$c;

    invoke-direct {v0, p0, v1}, Landroid/support/constraint/a/a/b;-><init>(Landroid/support/constraint/a/a/c;Landroid/support/constraint/a/a/b$c;)V

    iput-object v0, p0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    .line 101
    new-instance v0, Landroid/support/constraint/a/a/b;

    sget-object v1, Landroid/support/constraint/a/a/b$c;->f:Landroid/support/constraint/a/a/b$c;

    invoke-direct {v0, p0, v1}, Landroid/support/constraint/a/a/b;-><init>(Landroid/support/constraint/a/a/c;Landroid/support/constraint/a/a/b$c;)V

    iput-object v0, p0, Landroid/support/constraint/a/a/c;->q:Landroid/support/constraint/a/a/b;

    .line 102
    new-instance v0, Landroid/support/constraint/a/a/b;

    sget-object v1, Landroid/support/constraint/a/a/b$c;->h:Landroid/support/constraint/a/a/b$c;

    invoke-direct {v0, p0, v1}, Landroid/support/constraint/a/a/b;-><init>(Landroid/support/constraint/a/a/c;Landroid/support/constraint/a/a/b$c;)V

    iput-object v0, p0, Landroid/support/constraint/a/a/c;->r:Landroid/support/constraint/a/a/b;

    .line 103
    new-instance v0, Landroid/support/constraint/a/a/b;

    sget-object v1, Landroid/support/constraint/a/a/b$c;->i:Landroid/support/constraint/a/a/b$c;

    invoke-direct {v0, p0, v1}, Landroid/support/constraint/a/a/b;-><init>(Landroid/support/constraint/a/a/c;Landroid/support/constraint/a/a/b$c;)V

    iput-object v0, p0, Landroid/support/constraint/a/a/c;->s:Landroid/support/constraint/a/a/b;

    .line 104
    new-instance v0, Landroid/support/constraint/a/a/b;

    sget-object v1, Landroid/support/constraint/a/a/b$c;->g:Landroid/support/constraint/a/a/b$c;

    invoke-direct {v0, p0, v1}, Landroid/support/constraint/a/a/b;-><init>(Landroid/support/constraint/a/a/c;Landroid/support/constraint/a/a/b$c;)V

    iput-object v0, p0, Landroid/support/constraint/a/a/c;->t:Landroid/support/constraint/a/a/b;

    .line 106
    const/4 v0, 0x5

    new-array v0, v0, [Landroid/support/constraint/a/a/b;

    iget-object v1, p0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    aput-object v1, v0, v3

    const/4 v1, 0x1

    iget-object v2, p0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Landroid/support/constraint/a/a/c;->q:Landroid/support/constraint/a/a/b;

    aput-object v2, v0, v1

    iput-object v0, p0, Landroid/support/constraint/a/a/c;->u:[Landroid/support/constraint/a/a/b;

    .line 108
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/constraint/a/a/c;->v:Ljava/util/ArrayList;

    .line 111
    iput-object v4, p0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    .line 114
    iput v3, p0, Landroid/support/constraint/a/a/c;->x:I

    .line 115
    iput v3, p0, Landroid/support/constraint/a/a/c;->y:I

    .line 116
    iput v5, p0, Landroid/support/constraint/a/a/c;->z:F

    .line 117
    iput v6, p0, Landroid/support/constraint/a/a/c;->A:I

    .line 119
    iput v3, p0, Landroid/support/constraint/a/a/c;->B:I

    .line 120
    iput v3, p0, Landroid/support/constraint/a/a/c;->C:I

    .line 121
    iput v3, p0, Landroid/support/constraint/a/a/c;->D:I

    .line 122
    iput v3, p0, Landroid/support/constraint/a/a/c;->E:I

    .line 125
    iput v3, p0, Landroid/support/constraint/a/a/c;->F:I

    .line 126
    iput v3, p0, Landroid/support/constraint/a/a/c;->G:I

    .line 129
    iput v3, p0, Landroid/support/constraint/a/a/c;->aj:I

    .line 130
    iput v3, p0, Landroid/support/constraint/a/a/c;->ak:I

    .line 131
    iput v3, p0, Landroid/support/constraint/a/a/c;->al:I

    .line 132
    iput v3, p0, Landroid/support/constraint/a/a/c;->am:I

    .line 135
    iput v3, p0, Landroid/support/constraint/a/a/c;->H:I

    .line 136
    iput v3, p0, Landroid/support/constraint/a/a/c;->I:I

    .line 139
    iput v3, p0, Landroid/support/constraint/a/a/c;->J:I

    .line 152
    sget v0, Landroid/support/constraint/a/a/c;->M:F

    iput v0, p0, Landroid/support/constraint/a/a/c;->N:F

    .line 153
    sget v0, Landroid/support/constraint/a/a/c;->M:F

    iput v0, p0, Landroid/support/constraint/a/a/c;->O:F

    .line 156
    sget-object v0, Landroid/support/constraint/a/a/c$a;->a:Landroid/support/constraint/a/a/c$a;

    iput-object v0, p0, Landroid/support/constraint/a/a/c;->P:Landroid/support/constraint/a/a/c$a;

    .line 157
    sget-object v0, Landroid/support/constraint/a/a/c$a;->a:Landroid/support/constraint/a/a/c$a;

    iput-object v0, p0, Landroid/support/constraint/a/a/c;->Q:Landroid/support/constraint/a/a/c$a;

    .line 165
    iput v3, p0, Landroid/support/constraint/a/a/c;->aq:I

    .line 168
    iput v3, p0, Landroid/support/constraint/a/a/c;->ar:I

    .line 170
    iput-object v4, p0, Landroid/support/constraint/a/a/c;->as:Ljava/lang/String;

    .line 171
    iput-object v4, p0, Landroid/support/constraint/a/a/c;->at:Ljava/lang/String;

    .line 185
    iput v3, p0, Landroid/support/constraint/a/a/c;->ab:I

    .line 186
    iput v3, p0, Landroid/support/constraint/a/a/c;->ac:I

    .line 189
    iput v5, p0, Landroid/support/constraint/a/a/c;->af:F

    .line 190
    iput v5, p0, Landroid/support/constraint/a/a/c;->ag:F

    .line 191
    iput-object v4, p0, Landroid/support/constraint/a/a/c;->ah:Landroid/support/constraint/a/a/c;

    .line 192
    iput-object v4, p0, Landroid/support/constraint/a/a/c;->ai:Landroid/support/constraint/a/a/c;

    .line 251
    invoke-direct {p0}, Landroid/support/constraint/a/a/c;->D()V

    .line 252
    return-void
.end method

.method private D()V
    .locals 2

    .prologue
    .line 309
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->v:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 310
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->v:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 311
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->v:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 312
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->v:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 313
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->v:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/constraint/a/a/c;->r:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 314
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->v:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/constraint/a/a/c;->s:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 318
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->v:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/constraint/a/a/c;->q:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 319
    return-void
.end method

.method private a(Landroid/support/constraint/a/e;ZZLandroid/support/constraint/a/a/b;Landroid/support/constraint/a/a/b;IIIIFZZIIIF)V
    .locals 19

    .prologue
    .line 2309
    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v4

    .line 2310
    move-object/from16 v0, p1

    move-object/from16 v1, p5

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v3

    .line 2311
    invoke-virtual/range {p4 .. p4}, Landroid/support/constraint/a/a/b;->f()Landroid/support/constraint/a/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v7

    .line 2312
    invoke-virtual/range {p5 .. p5}, Landroid/support/constraint/a/a/b;->f()Landroid/support/constraint/a/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v10

    .line 2314
    invoke-virtual/range {p4 .. p4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v8

    .line 2315
    invoke-virtual/range {p5 .. p5}, Landroid/support/constraint/a/a/b;->d()I

    move-result v12

    .line 2316
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/constraint/a/a/c;->ar:I

    const/16 v5, 0x8

    if-ne v2, v5, :cond_28

    .line 2317
    const/4 v2, 0x0

    .line 2318
    const/16 p3, 0x1

    .line 2320
    :goto_0
    const/4 v5, -0x2

    move/from16 v0, p14

    if-ne v0, v5, :cond_27

    move v5, v2

    .line 2323
    :goto_1
    const/4 v6, -0x2

    move/from16 v0, p15

    if-ne v0, v6, :cond_0

    move/from16 p15, v2

    .line 2326
    :cond_0
    invoke-static {v2, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 2327
    if-nez v7, :cond_7

    if-nez v10, :cond_7

    .line 2328
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->b()Landroid/support/constraint/a/b;

    move-result-object v5

    move/from16 v0, p6

    invoke-virtual {v5, v4, v0}, Landroid/support/constraint/a/b;->b(Landroid/support/constraint/a/g;I)Landroid/support/constraint/a/b;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    .line 2329
    const/4 v5, 0x2

    move/from16 v0, p13

    if-ne v0, v5, :cond_4

    .line 2332
    invoke-virtual/range {p4 .. p4}, Landroid/support/constraint/a/a/b;->c()Landroid/support/constraint/a/a/b$c;

    move-result-object v2

    sget-object v5, Landroid/support/constraint/a/a/b$c;->c:Landroid/support/constraint/a/a/b$c;

    if-eq v2, v5, :cond_1

    invoke-virtual/range {p4 .. p4}, Landroid/support/constraint/a/a/b;->c()Landroid/support/constraint/a/a/b$c;

    move-result-object v2

    sget-object v5, Landroid/support/constraint/a/a/b$c;->e:Landroid/support/constraint/a/a/b$c;

    if-ne v2, v5, :cond_3

    .line 2334
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    sget-object v5, Landroid/support/constraint/a/a/b$c;->c:Landroid/support/constraint/a/a/b$c;

    invoke-virtual {v2, v5}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v6

    .line 2335
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    sget-object v5, Landroid/support/constraint/a/a/b$c;->e:Landroid/support/constraint/a/a/b$c;

    invoke-virtual {v2, v5}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v5

    .line 2340
    :goto_2
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->b()Landroid/support/constraint/a/b;

    move-result-object v2

    move/from16 v7, p16

    invoke-virtual/range {v2 .. v7}, Landroid/support/constraint/a/b;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;F)Landroid/support/constraint/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    .line 2507
    :cond_2
    :goto_3
    return-void

    .line 2337
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    sget-object v5, Landroid/support/constraint/a/a/b$c;->b:Landroid/support/constraint/a/a/b$c;

    invoke-virtual {v2, v5}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v6

    .line 2338
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    sget-object v5, Landroid/support/constraint/a/a/b$c;->d:Landroid/support/constraint/a/a/b$c;

    invoke-virtual {v2, v5}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v5

    goto :goto_2

    .line 2342
    :cond_4
    if-nez p11, :cond_2

    .line 2343
    if-eqz p2, :cond_5

    .line 2344
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, p9

    invoke-static {v0, v3, v4, v1, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/e;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;IZ)Landroid/support/constraint/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    goto :goto_3

    .line 2346
    :cond_5
    if-eqz p3, :cond_6

    .line 2347
    const/4 v5, 0x0

    .line 2348
    move-object/from16 v0, p1

    invoke-static {v0, v3, v4, v2, v5}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/e;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;IZ)Landroid/support/constraint/a/b;

    move-result-object v2

    .line 2347
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    goto :goto_3

    .line 2350
    :cond_6
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->b()Landroid/support/constraint/a/b;

    move-result-object v2

    move/from16 v0, p7

    invoke-virtual {v2, v3, v0}, Landroid/support/constraint/a/b;->b(Landroid/support/constraint/a/g;I)Landroid/support/constraint/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    goto :goto_3

    .line 2355
    :cond_7
    if-eqz v7, :cond_d

    if-nez v10, :cond_d

    .line 2356
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->b()Landroid/support/constraint/a/b;

    move-result-object v5

    invoke-virtual {v5, v4, v7, v8}, Landroid/support/constraint/a/b;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;I)Landroid/support/constraint/a/b;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    .line 2357
    const/4 v5, 0x2

    move/from16 v0, p13

    if-ne v0, v5, :cond_a

    .line 2360
    invoke-virtual/range {p4 .. p4}, Landroid/support/constraint/a/a/b;->c()Landroid/support/constraint/a/a/b$c;

    move-result-object v2

    sget-object v5, Landroid/support/constraint/a/a/b$c;->c:Landroid/support/constraint/a/a/b$c;

    if-eq v2, v5, :cond_8

    invoke-virtual/range {p4 .. p4}, Landroid/support/constraint/a/a/b;->c()Landroid/support/constraint/a/a/b$c;

    move-result-object v2

    sget-object v5, Landroid/support/constraint/a/a/b$c;->e:Landroid/support/constraint/a/a/b$c;

    if-ne v2, v5, :cond_9

    .line 2362
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    sget-object v5, Landroid/support/constraint/a/a/b$c;->c:Landroid/support/constraint/a/a/b$c;

    invoke-virtual {v2, v5}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v6

    .line 2363
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    sget-object v5, Landroid/support/constraint/a/a/b$c;->e:Landroid/support/constraint/a/a/b$c;

    invoke-virtual {v2, v5}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v5

    .line 2368
    :goto_4
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->b()Landroid/support/constraint/a/b;

    move-result-object v2

    move/from16 v7, p16

    invoke-virtual/range {v2 .. v7}, Landroid/support/constraint/a/b;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;F)Landroid/support/constraint/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    goto/16 :goto_3

    .line 2365
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    sget-object v5, Landroid/support/constraint/a/a/b$c;->b:Landroid/support/constraint/a/a/b$c;

    invoke-virtual {v2, v5}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v6

    .line 2366
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    sget-object v5, Landroid/support/constraint/a/a/b$c;->d:Landroid/support/constraint/a/a/b$c;

    invoke-virtual {v2, v5}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v5

    goto :goto_4

    .line 2370
    :cond_a
    if-eqz p2, :cond_b

    .line 2371
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, p9

    invoke-static {v0, v3, v4, v1, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/e;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;IZ)Landroid/support/constraint/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    goto/16 :goto_3

    .line 2372
    :cond_b
    if-nez p11, :cond_2

    .line 2373
    if-eqz p3, :cond_c

    .line 2374
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->b()Landroid/support/constraint/a/b;

    move-result-object v5

    invoke-virtual {v5, v3, v4, v2}, Landroid/support/constraint/a/b;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;I)Landroid/support/constraint/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    goto/16 :goto_3

    .line 2376
    :cond_c
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->b()Landroid/support/constraint/a/b;

    move-result-object v2

    move/from16 v0, p7

    invoke-virtual {v2, v3, v0}, Landroid/support/constraint/a/b;->b(Landroid/support/constraint/a/g;I)Landroid/support/constraint/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    goto/16 :goto_3

    .line 2380
    :cond_d
    if-nez v7, :cond_13

    if-eqz v10, :cond_13

    .line 2381
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->b()Landroid/support/constraint/a/b;

    move-result-object v5

    mul-int/lit8 v6, v12, -0x1

    invoke-virtual {v5, v3, v10, v6}, Landroid/support/constraint/a/b;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;I)Landroid/support/constraint/a/b;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    .line 2382
    const/4 v5, 0x2

    move/from16 v0, p13

    if-ne v0, v5, :cond_10

    .line 2385
    invoke-virtual/range {p4 .. p4}, Landroid/support/constraint/a/a/b;->c()Landroid/support/constraint/a/a/b$c;

    move-result-object v2

    sget-object v5, Landroid/support/constraint/a/a/b$c;->c:Landroid/support/constraint/a/a/b$c;

    if-eq v2, v5, :cond_e

    invoke-virtual/range {p4 .. p4}, Landroid/support/constraint/a/a/b;->c()Landroid/support/constraint/a/a/b$c;

    move-result-object v2

    sget-object v5, Landroid/support/constraint/a/a/b$c;->e:Landroid/support/constraint/a/a/b$c;

    if-ne v2, v5, :cond_f

    .line 2387
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    sget-object v5, Landroid/support/constraint/a/a/b$c;->c:Landroid/support/constraint/a/a/b$c;

    invoke-virtual {v2, v5}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v6

    .line 2388
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    sget-object v5, Landroid/support/constraint/a/a/b$c;->e:Landroid/support/constraint/a/a/b$c;

    invoke-virtual {v2, v5}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v5

    .line 2393
    :goto_5
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->b()Landroid/support/constraint/a/b;

    move-result-object v2

    move/from16 v7, p16

    invoke-virtual/range {v2 .. v7}, Landroid/support/constraint/a/b;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;F)Landroid/support/constraint/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    goto/16 :goto_3

    .line 2390
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    sget-object v5, Landroid/support/constraint/a/a/b$c;->b:Landroid/support/constraint/a/a/b$c;

    invoke-virtual {v2, v5}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v6

    .line 2391
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    sget-object v5, Landroid/support/constraint/a/a/b$c;->d:Landroid/support/constraint/a/a/b$c;

    invoke-virtual {v2, v5}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v5

    goto :goto_5

    .line 2395
    :cond_10
    if-eqz p2, :cond_11

    .line 2396
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, p9

    invoke-static {v0, v3, v4, v1, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/e;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;IZ)Landroid/support/constraint/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    goto/16 :goto_3

    .line 2397
    :cond_11
    if-nez p11, :cond_2

    .line 2398
    if-eqz p3, :cond_12

    .line 2399
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->b()Landroid/support/constraint/a/b;

    move-result-object v5

    invoke-virtual {v5, v3, v4, v2}, Landroid/support/constraint/a/b;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;I)Landroid/support/constraint/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    goto/16 :goto_3

    .line 2401
    :cond_12
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->b()Landroid/support/constraint/a/b;

    move-result-object v2

    move/from16 v0, p6

    invoke-virtual {v2, v4, v0}, Landroid/support/constraint/a/b;->b(Landroid/support/constraint/a/g;I)Landroid/support/constraint/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    goto/16 :goto_3

    .line 2406
    :cond_13
    if-eqz p3, :cond_1a

    .line 2407
    if-eqz p2, :cond_14

    .line 2408
    const/4 v2, 0x1

    .line 2409
    move-object/from16 v0, p1

    move/from16 v1, p9

    invoke-static {v0, v3, v4, v1, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/e;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;IZ)Landroid/support/constraint/a/b;

    move-result-object v2

    .line 2408
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    .line 2414
    :goto_6
    invoke-virtual/range {p4 .. p4}, Landroid/support/constraint/a/a/b;->e()Landroid/support/constraint/a/a/b$b;

    move-result-object v2

    invoke-virtual/range {p5 .. p5}, Landroid/support/constraint/a/a/b;->e()Landroid/support/constraint/a/a/b$b;

    move-result-object v5

    if-eq v2, v5, :cond_16

    .line 2415
    invoke-virtual/range {p4 .. p4}, Landroid/support/constraint/a/a/b;->e()Landroid/support/constraint/a/a/b$b;

    move-result-object v2

    sget-object v5, Landroid/support/constraint/a/a/b$b;->b:Landroid/support/constraint/a/a/b$b;

    if-ne v2, v5, :cond_15

    .line 2416
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->b()Landroid/support/constraint/a/b;

    move-result-object v2

    invoke-virtual {v2, v4, v7, v8}, Landroid/support/constraint/a/b;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;I)Landroid/support/constraint/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    .line 2417
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->c()Landroid/support/constraint/a/g;

    move-result-object v2

    .line 2418
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->b()Landroid/support/constraint/a/b;

    move-result-object v4

    .line 2419
    mul-int/lit8 v5, v12, -0x1

    invoke-virtual {v4, v3, v10, v2, v5}, Landroid/support/constraint/a/b;->b(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;I)Landroid/support/constraint/a/b;

    .line 2420
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    goto/16 :goto_3

    .line 2411
    :cond_14
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->b()Landroid/support/constraint/a/b;

    move-result-object v5

    invoke-virtual {v5, v3, v4, v2}, Landroid/support/constraint/a/b;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;I)Landroid/support/constraint/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    goto :goto_6

    .line 2422
    :cond_15
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->c()Landroid/support/constraint/a/g;

    move-result-object v2

    .line 2423
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->b()Landroid/support/constraint/a/b;

    move-result-object v5

    .line 2424
    invoke-virtual {v5, v4, v7, v2, v8}, Landroid/support/constraint/a/b;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;I)Landroid/support/constraint/a/b;

    .line 2425
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    .line 2426
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->b()Landroid/support/constraint/a/b;

    move-result-object v2

    mul-int/lit8 v4, v12, -0x1

    invoke-virtual {v2, v3, v10, v4}, Landroid/support/constraint/a/b;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;I)Landroid/support/constraint/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    goto/16 :goto_3

    .line 2430
    :cond_16
    if-ne v7, v10, :cond_17

    .line 2431
    const/4 v8, 0x0

    const/high16 v9, 0x3f000000    # 0.5f

    const/4 v12, 0x0

    const/4 v13, 0x1

    move-object/from16 v5, p1

    move-object v6, v4

    move-object v11, v3

    .line 2432
    invoke-static/range {v5 .. v13}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/e;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;IFLandroid/support/constraint/a/g;Landroid/support/constraint/a/g;IZ)Landroid/support/constraint/a/b;

    move-result-object v2

    .line 2431
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    goto/16 :goto_3

    .line 2434
    :cond_17
    if-nez p12, :cond_2

    .line 2435
    invoke-virtual/range {p4 .. p4}, Landroid/support/constraint/a/a/b;->g()Landroid/support/constraint/a/a/b$a;

    move-result-object v2

    sget-object v5, Landroid/support/constraint/a/a/b$a;->b:Landroid/support/constraint/a/a/b$a;

    if-eq v2, v5, :cond_18

    const/4 v2, 0x1

    .line 2438
    :goto_7
    move-object/from16 v0, p1

    invoke-static {v0, v4, v7, v8, v2}, Landroid/support/constraint/a/e;->b(Landroid/support/constraint/a/e;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;IZ)Landroid/support/constraint/a/b;

    move-result-object v2

    .line 2437
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    .line 2440
    invoke-virtual/range {p5 .. p5}, Landroid/support/constraint/a/a/b;->g()Landroid/support/constraint/a/a/b$a;

    move-result-object v2

    sget-object v5, Landroid/support/constraint/a/a/b$a;->b:Landroid/support/constraint/a/a/b$a;

    if-eq v2, v5, :cond_19

    const/4 v2, 0x1

    .line 2442
    :goto_8
    mul-int/lit8 v5, v12, -0x1

    .line 2443
    move-object/from16 v0, p1

    invoke-static {v0, v3, v10, v5, v2}, Landroid/support/constraint/a/e;->c(Landroid/support/constraint/a/e;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;IZ)Landroid/support/constraint/a/b;

    move-result-object v2

    .line 2442
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    .line 2446
    const/4 v13, 0x0

    move-object/from16 v5, p1

    move-object v6, v4

    move/from16 v9, p10

    move-object v11, v3

    .line 2447
    invoke-static/range {v5 .. v13}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/e;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;IFLandroid/support/constraint/a/g;Landroid/support/constraint/a/g;IZ)Landroid/support/constraint/a/b;

    move-result-object v2

    .line 2446
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    goto/16 :goto_3

    .line 2435
    :cond_18
    const/4 v2, 0x0

    goto :goto_7

    .line 2440
    :cond_19
    const/4 v2, 0x0

    goto :goto_8

    .line 2452
    :cond_1a
    if-eqz p11, :cond_1b

    .line 2453
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7, v8, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 2454
    mul-int/lit8 v2, v12, -0x1

    const/4 v5, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10, v2, v5}, Landroid/support/constraint/a/e;->b(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 2455
    const/4 v13, 0x1

    move-object/from16 v5, p1

    move-object v6, v4

    move/from16 v9, p10

    move-object v11, v3

    .line 2456
    invoke-static/range {v5 .. v13}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/e;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;IFLandroid/support/constraint/a/g;Landroid/support/constraint/a/g;IZ)Landroid/support/constraint/a/b;

    move-result-object v2

    .line 2455
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    goto/16 :goto_3

    .line 2458
    :cond_1b
    if-nez p12, :cond_2

    .line 2459
    const/4 v6, 0x1

    move/from16 v0, p13

    if-eq v0, v6, :cond_1c

    const/4 v6, 0x2

    move/from16 v0, p13

    if-ne v0, v6, :cond_23

    .line 2460
    :cond_1c
    const/4 v6, 0x2

    move/from16 v0, p13

    if-ne v0, v6, :cond_1e

    .line 2463
    invoke-virtual/range {p4 .. p4}, Landroid/support/constraint/a/a/b;->c()Landroid/support/constraint/a/a/b$c;

    move-result-object v6

    sget-object v9, Landroid/support/constraint/a/a/b$c;->c:Landroid/support/constraint/a/a/b$c;

    if-eq v6, v9, :cond_1d

    invoke-virtual/range {p4 .. p4}, Landroid/support/constraint/a/a/b;->c()Landroid/support/constraint/a/a/b$c;

    move-result-object v6

    sget-object v9, Landroid/support/constraint/a/a/b$c;->e:Landroid/support/constraint/a/a/b$c;

    if-ne v6, v9, :cond_20

    .line 2465
    :cond_1d
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    sget-object v9, Landroid/support/constraint/a/a/b$c;->c:Landroid/support/constraint/a/a/b$c;

    invoke-virtual {v6, v9}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v17

    .line 2466
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    sget-object v9, Landroid/support/constraint/a/a/b$c;->e:Landroid/support/constraint/a/a/b$c;

    invoke-virtual {v6, v9}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v16

    .line 2471
    :goto_9
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->b()Landroid/support/constraint/a/b;

    move-result-object v13

    move-object v14, v3

    move-object v15, v4

    move/from16 v18, p16

    invoke-virtual/range {v13 .. v18}, Landroid/support/constraint/a/b;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;F)Landroid/support/constraint/a/b;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    .line 2473
    :cond_1e
    if-le v5, v2, :cond_1f

    move v2, v5

    .line 2476
    :cond_1f
    if-lez p15, :cond_22

    .line 2477
    move/from16 v0, p15

    if-ge v0, v2, :cond_21

    .line 2483
    :goto_a
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, p15

    invoke-virtual {v0, v3, v4, v1, v2}, Landroid/support/constraint/a/e;->c(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)Landroid/support/constraint/a/b;

    .line 2484
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7, v8, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 2485
    neg-int v2, v12

    const/4 v5, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10, v2, v5}, Landroid/support/constraint/a/e;->b(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 2486
    const/4 v13, 0x4

    move-object/from16 v5, p1

    move-object v6, v4

    move/from16 v9, p10

    move-object v11, v3

    invoke-virtual/range {v5 .. v13}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;IFLandroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    goto/16 :goto_3

    .line 2468
    :cond_20
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    sget-object v9, Landroid/support/constraint/a/a/b$c;->b:Landroid/support/constraint/a/a/b$c;

    invoke-virtual {v6, v9}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v17

    .line 2469
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    sget-object v9, Landroid/support/constraint/a/a/b$c;->d:Landroid/support/constraint/a/a/b$c;

    invoke-virtual {v6, v9}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v16

    goto :goto_9

    .line 2480
    :cond_21
    const/4 v5, 0x3

    move-object/from16 v0, p1

    move/from16 v1, p15

    invoke-virtual {v0, v3, v4, v1, v5}, Landroid/support/constraint/a/e;->b(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    :cond_22
    move/from16 p15, v2

    goto :goto_a

    .line 2489
    :cond_23
    if-nez v5, :cond_24

    if-nez p15, :cond_24

    .line 2490
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->b()Landroid/support/constraint/a/b;

    move-result-object v2

    invoke-virtual {v2, v4, v7, v8}, Landroid/support/constraint/a/b;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;I)Landroid/support/constraint/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    .line 2491
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->b()Landroid/support/constraint/a/b;

    move-result-object v2

    mul-int/lit8 v4, v12, -0x1

    invoke-virtual {v2, v3, v10, v4}, Landroid/support/constraint/a/b;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;I)Landroid/support/constraint/a/b;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    goto/16 :goto_3

    .line 2493
    :cond_24
    if-lez p15, :cond_25

    .line 2494
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, p15

    invoke-virtual {v0, v3, v4, v1, v2}, Landroid/support/constraint/a/e;->b(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 2496
    :cond_25
    if-lez v5, :cond_26

    .line 2497
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 2499
    :cond_26
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7, v8, v2}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 2500
    neg-int v2, v12

    const/4 v5, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10, v2, v5}, Landroid/support/constraint/a/e;->b(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 2501
    const/4 v13, 0x4

    move-object/from16 v5, p1

    move-object v6, v4

    move/from16 v9, p10

    move-object v11, v3

    invoke-virtual/range {v5 .. v13}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;IFLandroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    goto/16 :goto_3

    :cond_27
    move/from16 v5, p14

    goto/16 :goto_1

    :cond_28
    move/from16 v2, p8

    goto/16 :goto_0
.end method


# virtual methods
.method public A()V
    .locals 3

    .prologue
    .line 1774
    invoke-virtual {p0}, Landroid/support/constraint/a/a/c;->c()Landroid/support/constraint/a/a/c;

    move-result-object v0

    .line 1775
    if-eqz v0, :cond_1

    instance-of v0, v0, Landroid/support/constraint/a/a/d;

    if-eqz v0, :cond_1

    .line 1776
    invoke-virtual {p0}, Landroid/support/constraint/a/a/c;->c()Landroid/support/constraint/a/a/c;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/a/a/d;

    .line 1777
    invoke-virtual {v0}, Landroid/support/constraint/a/a/d;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1785
    :cond_0
    return-void

    .line 1781
    :cond_1
    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/constraint/a/a/c;->v:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 1782
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->v:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/a/a/b;

    .line 1783
    invoke-virtual {v0}, Landroid/support/constraint/a/a/b;->i()V

    .line 1781
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public B()Landroid/support/constraint/a/a/c$a;
    .locals 1

    .prologue
    .line 1886
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->P:Landroid/support/constraint/a/a/c$a;

    return-object v0
.end method

.method public C()Landroid/support/constraint/a/a/c$a;
    .locals 1

    .prologue
    .line 1895
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->Q:Landroid/support/constraint/a/a/c$a;

    return-object v0
.end method

.method public a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;
    .locals 2

    .prologue
    .line 1849
    sget-object v0, Landroid/support/constraint/a/a/c$1;->a:[I

    invoke-virtual {p1}, Landroid/support/constraint/a/a/b$c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1877
    new-instance v0, Ljava/lang/AssertionError;

    invoke-virtual {p1}, Landroid/support/constraint/a/a/b$c;->name()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 1851
    :pswitch_0
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    .line 1875
    :goto_0
    return-object v0

    .line 1854
    :pswitch_1
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    goto :goto_0

    .line 1857
    :pswitch_2
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    goto :goto_0

    .line 1860
    :pswitch_3
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    goto :goto_0

    .line 1863
    :pswitch_4
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->q:Landroid/support/constraint/a/a/b;

    goto :goto_0

    .line 1866
    :pswitch_5
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->r:Landroid/support/constraint/a/a/b;

    goto :goto_0

    .line 1869
    :pswitch_6
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->s:Landroid/support/constraint/a/a/b;

    goto :goto_0

    .line 1872
    :pswitch_7
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->t:Landroid/support/constraint/a/a/b;

    goto :goto_0

    .line 1875
    :pswitch_8
    const/4 v0, 0x0

    goto :goto_0

    .line 1849
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_7
        :pswitch_5
        :pswitch_6
        :pswitch_8
    .end packed-switch
.end method

.method public a()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 196
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0}, Landroid/support/constraint/a/a/b;->i()V

    .line 197
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0}, Landroid/support/constraint/a/a/b;->i()V

    .line 198
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0}, Landroid/support/constraint/a/a/b;->i()V

    .line 199
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0}, Landroid/support/constraint/a/a/b;->i()V

    .line 200
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->q:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0}, Landroid/support/constraint/a/a/b;->i()V

    .line 201
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->r:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0}, Landroid/support/constraint/a/a/b;->i()V

    .line 202
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->s:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0}, Landroid/support/constraint/a/a/b;->i()V

    .line 203
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->t:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0}, Landroid/support/constraint/a/a/b;->i()V

    .line 204
    iput-object v2, p0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    .line 205
    iput v1, p0, Landroid/support/constraint/a/a/c;->x:I

    .line 206
    iput v1, p0, Landroid/support/constraint/a/a/c;->y:I

    .line 207
    iput v3, p0, Landroid/support/constraint/a/a/c;->z:F

    .line 208
    iput v4, p0, Landroid/support/constraint/a/a/c;->A:I

    .line 209
    iput v1, p0, Landroid/support/constraint/a/a/c;->F:I

    .line 210
    iput v1, p0, Landroid/support/constraint/a/a/c;->G:I

    .line 211
    iput v1, p0, Landroid/support/constraint/a/a/c;->aj:I

    .line 212
    iput v1, p0, Landroid/support/constraint/a/a/c;->ak:I

    .line 213
    iput v1, p0, Landroid/support/constraint/a/a/c;->al:I

    .line 214
    iput v1, p0, Landroid/support/constraint/a/a/c;->am:I

    .line 215
    iput v1, p0, Landroid/support/constraint/a/a/c;->H:I

    .line 216
    iput v1, p0, Landroid/support/constraint/a/a/c;->I:I

    .line 217
    iput v1, p0, Landroid/support/constraint/a/a/c;->J:I

    .line 218
    iput v1, p0, Landroid/support/constraint/a/a/c;->K:I

    .line 219
    iput v1, p0, Landroid/support/constraint/a/a/c;->L:I

    .line 220
    iput v1, p0, Landroid/support/constraint/a/a/c;->an:I

    .line 221
    iput v1, p0, Landroid/support/constraint/a/a/c;->ao:I

    .line 222
    sget v0, Landroid/support/constraint/a/a/c;->M:F

    iput v0, p0, Landroid/support/constraint/a/a/c;->N:F

    .line 223
    sget v0, Landroid/support/constraint/a/a/c;->M:F

    iput v0, p0, Landroid/support/constraint/a/a/c;->O:F

    .line 224
    sget-object v0, Landroid/support/constraint/a/a/c$a;->a:Landroid/support/constraint/a/a/c$a;

    iput-object v0, p0, Landroid/support/constraint/a/a/c;->P:Landroid/support/constraint/a/a/c$a;

    .line 225
    sget-object v0, Landroid/support/constraint/a/a/c$a;->a:Landroid/support/constraint/a/a/c$a;

    iput-object v0, p0, Landroid/support/constraint/a/a/c;->Q:Landroid/support/constraint/a/a/c$a;

    .line 226
    iput-object v2, p0, Landroid/support/constraint/a/a/c;->ap:Ljava/lang/Object;

    .line 227
    iput v1, p0, Landroid/support/constraint/a/a/c;->aq:I

    .line 228
    iput v1, p0, Landroid/support/constraint/a/a/c;->ar:I

    .line 229
    iput-object v2, p0, Landroid/support/constraint/a/a/c;->as:Ljava/lang/String;

    .line 230
    iput-object v2, p0, Landroid/support/constraint/a/a/c;->at:Ljava/lang/String;

    .line 231
    iput-boolean v1, p0, Landroid/support/constraint/a/a/c;->Z:Z

    .line 232
    iput-boolean v1, p0, Landroid/support/constraint/a/a/c;->aa:Z

    .line 233
    iput v1, p0, Landroid/support/constraint/a/a/c;->ab:I

    .line 234
    iput v1, p0, Landroid/support/constraint/a/a/c;->ac:I

    .line 235
    iput-boolean v1, p0, Landroid/support/constraint/a/a/c;->ad:Z

    .line 236
    iput-boolean v1, p0, Landroid/support/constraint/a/a/c;->ae:Z

    .line 237
    iput v3, p0, Landroid/support/constraint/a/a/c;->af:F

    .line 238
    iput v3, p0, Landroid/support/constraint/a/a/c;->ag:F

    .line 239
    iput v4, p0, Landroid/support/constraint/a/a/c;->a:I

    .line 240
    iput v4, p0, Landroid/support/constraint/a/a/c;->b:I

    .line 241
    return-void
.end method

.method public a(F)V
    .locals 0

    .prologue
    .line 1153
    iput p1, p0, Landroid/support/constraint/a/a/c;->N:F

    .line 1154
    return-void
.end method

.method public a(II)V
    .locals 0

    .prologue
    .line 873
    iput p1, p0, Landroid/support/constraint/a/a/c;->F:I

    .line 874
    iput p2, p0, Landroid/support/constraint/a/a/c;->G:I

    .line 875
    return-void
.end method

.method public a(IIIF)V
    .locals 0

    .prologue
    .line 1035
    iput p1, p0, Landroid/support/constraint/a/a/c;->c:I

    .line 1036
    iput p2, p0, Landroid/support/constraint/a/a/c;->e:I

    .line 1037
    iput p3, p0, Landroid/support/constraint/a/a/c;->f:I

    .line 1038
    iput p4, p0, Landroid/support/constraint/a/a/c;->g:F

    .line 1039
    return-void
.end method

.method public a(IIII)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1236
    sub-int v0, p3, p1

    .line 1237
    sub-int v1, p4, p2

    .line 1239
    iput p1, p0, Landroid/support/constraint/a/a/c;->F:I

    .line 1240
    iput p2, p0, Landroid/support/constraint/a/a/c;->G:I

    .line 1242
    iget v2, p0, Landroid/support/constraint/a/a/c;->ar:I

    const/16 v3, 0x8

    if-ne v2, v3, :cond_1

    .line 1243
    iput v4, p0, Landroid/support/constraint/a/a/c;->x:I

    .line 1244
    iput v4, p0, Landroid/support/constraint/a/a/c;->y:I

    .line 1265
    :cond_0
    :goto_0
    return-void

    .line 1249
    :cond_1
    iget-object v2, p0, Landroid/support/constraint/a/a/c;->P:Landroid/support/constraint/a/a/c$a;

    sget-object v3, Landroid/support/constraint/a/a/c$a;->a:Landroid/support/constraint/a/a/c$a;

    if-ne v2, v3, :cond_2

    iget v2, p0, Landroid/support/constraint/a/a/c;->x:I

    if-ge v0, v2, :cond_2

    .line 1250
    iget v0, p0, Landroid/support/constraint/a/a/c;->x:I

    .line 1252
    :cond_2
    iget-object v2, p0, Landroid/support/constraint/a/a/c;->Q:Landroid/support/constraint/a/a/c$a;

    sget-object v3, Landroid/support/constraint/a/a/c$a;->a:Landroid/support/constraint/a/a/c$a;

    if-ne v2, v3, :cond_3

    iget v2, p0, Landroid/support/constraint/a/a/c;->y:I

    if-ge v1, v2, :cond_3

    .line 1253
    iget v1, p0, Landroid/support/constraint/a/a/c;->y:I

    .line 1256
    :cond_3
    iput v0, p0, Landroid/support/constraint/a/a/c;->x:I

    .line 1257
    iput v1, p0, Landroid/support/constraint/a/a/c;->y:I

    .line 1259
    iget v0, p0, Landroid/support/constraint/a/a/c;->y:I

    iget v1, p0, Landroid/support/constraint/a/a/c;->L:I

    if-ge v0, v1, :cond_4

    .line 1260
    iget v0, p0, Landroid/support/constraint/a/a/c;->L:I

    iput v0, p0, Landroid/support/constraint/a/a/c;->y:I

    .line 1262
    :cond_4
    iget v0, p0, Landroid/support/constraint/a/a/c;->x:I

    iget v1, p0, Landroid/support/constraint/a/a/c;->K:I

    if-ge v0, v1, :cond_0

    .line 1263
    iget v0, p0, Landroid/support/constraint/a/a/c;->K:I

    iput v0, p0, Landroid/support/constraint/a/a/c;->x:I

    goto :goto_0
.end method

.method public a(Landroid/support/constraint/a/a/b$c;Landroid/support/constraint/a/a/c;Landroid/support/constraint/a/a/b$c;II)V
    .locals 7

    .prologue
    .line 1421
    invoke-virtual {p0, p1}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v0

    .line 1422
    invoke-virtual {p2, p3}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v1

    .line 1423
    sget-object v4, Landroid/support/constraint/a/a/b$b;->b:Landroid/support/constraint/a/a/b$b;

    const/4 v5, 0x0

    const/4 v6, 0x1

    move v2, p4

    move v3, p5

    invoke-virtual/range {v0 .. v6}, Landroid/support/constraint/a/a/b;->a(Landroid/support/constraint/a/a/b;IILandroid/support/constraint/a/a/b$b;IZ)Z

    .line 1425
    return-void
.end method

.method public a(Landroid/support/constraint/a/a/c$a;)V
    .locals 2

    .prologue
    .line 1904
    iput-object p1, p0, Landroid/support/constraint/a/a/c;->P:Landroid/support/constraint/a/a/c$a;

    .line 1905
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->P:Landroid/support/constraint/a/a/c$a;

    sget-object v1, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    if-ne v0, v1, :cond_0

    .line 1906
    iget v0, p0, Landroid/support/constraint/a/a/c;->an:I

    invoke-virtual {p0, v0}, Landroid/support/constraint/a/a/c;->e(I)V

    .line 1908
    :cond_0
    return-void
.end method

.method public a(Landroid/support/constraint/a/a/c;)V
    .locals 0

    .prologue
    .line 419
    iput-object p1, p0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    .line 420
    return-void
.end method

.method public a(Landroid/support/constraint/a/c;)V
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0, p1}, Landroid/support/constraint/a/a/b;->a(Landroid/support/constraint/a/c;)V

    .line 286
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0, p1}, Landroid/support/constraint/a/a/b;->a(Landroid/support/constraint/a/c;)V

    .line 287
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0, p1}, Landroid/support/constraint/a/a/b;->a(Landroid/support/constraint/a/c;)V

    .line 288
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0, p1}, Landroid/support/constraint/a/a/b;->a(Landroid/support/constraint/a/c;)V

    .line 289
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->q:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0, p1}, Landroid/support/constraint/a/a/b;->a(Landroid/support/constraint/a/c;)V

    .line 290
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->t:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0, p1}, Landroid/support/constraint/a/a/b;->a(Landroid/support/constraint/a/c;)V

    .line 291
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->r:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0, p1}, Landroid/support/constraint/a/a/b;->a(Landroid/support/constraint/a/c;)V

    .line 292
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->s:Landroid/support/constraint/a/a/b;

    invoke-virtual {v0, p1}, Landroid/support/constraint/a/a/b;->a(Landroid/support/constraint/a/c;)V

    .line 293
    return-void
.end method

.method public a(Landroid/support/constraint/a/e;I)V
    .locals 32

    .prologue
    .line 2021
    const/4 v8, 0x0

    .line 2022
    const/4 v7, 0x0

    .line 2023
    const/4 v6, 0x0

    .line 2024
    const/4 v5, 0x0

    .line 2025
    const/4 v4, 0x0

    .line 2026
    const v9, 0x7fffffff

    move/from16 v0, p2

    if-eq v0, v9, :cond_0

    move-object/from16 v0, p0

    iget-object v9, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget v9, v9, Landroid/support/constraint/a/a/b;->g:I

    move/from16 v0, p2

    if-ne v9, v0, :cond_43

    .line 2027
    :cond_0
    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v8

    move-object/from16 v31, v8

    .line 2029
    :goto_0
    const v8, 0x7fffffff

    move/from16 v0, p2

    if-eq v0, v8, :cond_1

    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget v8, v8, Landroid/support/constraint/a/a/b;->g:I

    move/from16 v0, p2

    if-ne v8, v0, :cond_42

    .line 2030
    :cond_1
    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v7

    move-object/from16 v30, v7

    .line 2032
    :goto_1
    const v7, 0x7fffffff

    move/from16 v0, p2

    if-eq v0, v7, :cond_2

    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget v7, v7, Landroid/support/constraint/a/a/b;->g:I

    move/from16 v0, p2

    if-ne v7, v0, :cond_41

    .line 2033
    :cond_2
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v6

    move-object/from16 v29, v6

    .line 2035
    :goto_2
    const v6, 0x7fffffff

    move/from16 v0, p2

    if-eq v0, v6, :cond_3

    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget v6, v6, Landroid/support/constraint/a/a/b;->g:I

    move/from16 v0, p2

    if-ne v6, v0, :cond_40

    .line 2036
    :cond_3
    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v5

    move-object/from16 v28, v5

    .line 2038
    :goto_3
    const v5, 0x7fffffff

    move/from16 v0, p2

    if-eq v0, v5, :cond_4

    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/constraint/a/a/c;->q:Landroid/support/constraint/a/a/b;

    iget v5, v5, Landroid/support/constraint/a/a/b;->g:I

    move/from16 v0, p2

    if-ne v5, v0, :cond_3f

    .line 2039
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->q:Landroid/support/constraint/a/a/b;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v4

    move-object/from16 v21, v4

    .line 2042
    :goto_4
    const/4 v4, 0x0

    .line 2043
    const/4 v6, 0x0

    .line 2045
    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    if-eqz v5, :cond_3e

    .line 2047
    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v5, :cond_5

    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    if-eq v5, v7, :cond_6

    :cond_5
    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v5, :cond_3d

    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v5, v5, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    if-ne v5, v7, :cond_3d

    .line 2049
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    check-cast v4, Landroid/support/constraint/a/a/d;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v4, v0, v5}, Landroid/support/constraint/a/a/d;->a(Landroid/support/constraint/a/a/c;I)V

    .line 2050
    const/4 v4, 0x1

    move v5, v4

    .line 2053
    :goto_5
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v4, :cond_7

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    if-eq v4, v7, :cond_8

    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v4, :cond_3c

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    if-ne v4, v7, :cond_3c

    .line 2055
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    check-cast v4, Landroid/support/constraint/a/a/d;

    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v4, v0, v6}, Landroid/support/constraint/a/a/d;->a(Landroid/support/constraint/a/a/c;I)V

    .line 2056
    const/4 v4, 0x1

    .line 2065
    :goto_6
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    invoke-virtual {v6}, Landroid/support/constraint/a/a/c;->B()Landroid/support/constraint/a/a/c$a;

    move-result-object v6

    sget-object v7, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    if-ne v6, v7, :cond_c

    if-nez v5, :cond_c

    .line 2067
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v6, :cond_9

    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    if-eq v6, v7, :cond_19

    .line 2069
    :cond_9
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    iget-object v6, v6, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v6

    .line 2070
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->b()Landroid/support/constraint/a/b;

    move-result-object v7

    .line 2071
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->c()Landroid/support/constraint/a/g;

    move-result-object v8

    const/4 v9, 0x0

    move-object/from16 v0, v31

    invoke-virtual {v7, v0, v6, v8, v9}, Landroid/support/constraint/a/b;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;I)Landroid/support/constraint/a/b;

    .line 2072
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    .line 2078
    :cond_a
    :goto_7
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v6, :cond_b

    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    if-eq v6, v7, :cond_1a

    .line 2080
    :cond_b
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    iget-object v6, v6, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v6

    .line 2081
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->b()Landroid/support/constraint/a/b;

    move-result-object v7

    .line 2082
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->c()Landroid/support/constraint/a/g;

    move-result-object v8

    const/4 v9, 0x0

    move-object/from16 v0, v30

    invoke-virtual {v7, v6, v0, v8, v9}, Landroid/support/constraint/a/b;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;I)Landroid/support/constraint/a/b;

    .line 2083
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    .line 2090
    :cond_c
    :goto_8
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    invoke-virtual {v6}, Landroid/support/constraint/a/a/c;->C()Landroid/support/constraint/a/a/c$a;

    move-result-object v6

    sget-object v7, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    if-ne v6, v7, :cond_1d

    if-nez v4, :cond_1d

    .line 2092
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v6, :cond_d

    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    if-eq v6, v7, :cond_1b

    .line 2094
    :cond_d
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    iget-object v6, v6, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v6

    .line 2095
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->b()Landroid/support/constraint/a/b;

    move-result-object v7

    .line 2096
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->c()Landroid/support/constraint/a/g;

    move-result-object v8

    const/4 v9, 0x0

    move-object/from16 v0, v29

    invoke-virtual {v7, v0, v6, v8, v9}, Landroid/support/constraint/a/b;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;I)Landroid/support/constraint/a/b;

    .line 2097
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    .line 2102
    :cond_e
    :goto_9
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v6, :cond_f

    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    if-eq v6, v7, :cond_1c

    .line 2104
    :cond_f
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    iget-object v6, v6, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v6

    .line 2105
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->b()Landroid/support/constraint/a/b;

    move-result-object v7

    .line 2106
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->c()Landroid/support/constraint/a/g;

    move-result-object v8

    const/4 v9, 0x0

    move-object/from16 v0, v28

    invoke-virtual {v7, v6, v0, v8, v9}, Landroid/support/constraint/a/b;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;I)Landroid/support/constraint/a/b;

    .line 2107
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    move/from16 v22, v4

    move/from16 v16, v5

    .line 2115
    :goto_a
    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/constraint/a/a/c;->x:I

    .line 2116
    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/constraint/a/a/c;->K:I

    if-ge v4, v5, :cond_10

    .line 2117
    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/constraint/a/a/c;->K:I

    .line 2119
    :cond_10
    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/constraint/a/a/c;->y:I

    .line 2120
    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/constraint/a/a/c;->L:I

    if-ge v5, v6, :cond_11

    .line 2121
    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/constraint/a/a/c;->L:I

    .line 2125
    :cond_11
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->P:Landroid/support/constraint/a/a/c$a;

    sget-object v7, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-eq v6, v7, :cond_1e

    const/4 v6, 0x1

    .line 2126
    :goto_b
    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/constraint/a/a/c;->Q:Landroid/support/constraint/a/a/c$a;

    sget-object v8, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-eq v7, v8, :cond_1f

    const/4 v7, 0x1

    .line 2128
    :goto_c
    if-nez v6, :cond_3b

    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    if-eqz v8, :cond_3b

    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    if-eqz v8, :cond_3b

    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v8, v8, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v8, :cond_12

    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v8, v8, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-nez v8, :cond_3b

    .line 2130
    :cond_12
    const/4 v6, 0x1

    move v10, v6

    .line 2132
    :goto_d
    if-nez v7, :cond_3a

    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    if-eqz v6, :cond_3a

    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    if-eqz v6, :cond_3a

    .line 2133
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v6, :cond_13

    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-nez v6, :cond_3a

    .line 2135
    :cond_13
    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/constraint/a/a/c;->J:I

    if-eqz v6, :cond_14

    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->q:Landroid/support/constraint/a/a/b;

    if-eqz v6, :cond_3a

    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v6, :cond_14

    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->q:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-nez v6, :cond_3a

    .line 2138
    :cond_14
    const/4 v6, 0x1

    .line 2145
    :goto_e
    const/4 v9, 0x0

    .line 2146
    move-object/from16 v0, p0

    iget v8, v0, Landroid/support/constraint/a/a/c;->A:I

    .line 2147
    move-object/from16 v0, p0

    iget v7, v0, Landroid/support/constraint/a/a/c;->z:F

    .line 2148
    move-object/from16 v0, p0

    iget v11, v0, Landroid/support/constraint/a/a/c;->z:F

    const/4 v12, 0x0

    cmpl-float v11, v11, v12

    if-lez v11, :cond_39

    move-object/from16 v0, p0

    iget v11, v0, Landroid/support/constraint/a/a/c;->ar:I

    const/16 v12, 0x8

    if-eq v11, v12, :cond_39

    .line 2149
    move-object/from16 v0, p0

    iget-object v11, v0, Landroid/support/constraint/a/a/c;->P:Landroid/support/constraint/a/a/c$a;

    sget-object v12, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-ne v11, v12, :cond_21

    move-object/from16 v0, p0

    iget-object v11, v0, Landroid/support/constraint/a/a/c;->Q:Landroid/support/constraint/a/a/c$a;

    sget-object v12, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-ne v11, v12, :cond_21

    .line 2151
    const/4 v9, 0x1

    .line 2152
    if-eqz v10, :cond_20

    if-nez v6, :cond_20

    .line 2153
    const/4 v8, 0x0

    move/from16 v24, v7

    move/from16 v25, v8

    move/from16 v26, v9

    move/from16 v27, v6

    move/from16 v23, v5

    move v12, v4

    move v7, v10

    .line 2176
    :goto_f
    if-eqz v26, :cond_23

    if-eqz v25, :cond_15

    const/4 v4, -0x1

    move/from16 v0, v25

    if-ne v0, v4, :cond_23

    :cond_15
    const/4 v15, 0x1

    .line 2180
    :goto_10
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->P:Landroid/support/constraint/a/a/c$a;

    sget-object v5, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    if-ne v4, v5, :cond_24

    move-object/from16 v0, p0

    instance-of v4, v0, Landroid/support/constraint/a/a/d;

    if-eqz v4, :cond_24

    const/4 v6, 0x1

    .line 2182
    :goto_11
    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/constraint/a/a/c;->a:I

    const/4 v5, 0x2

    if-eq v4, v5, :cond_17

    const v4, 0x7fffffff

    move/from16 v0, p2

    if-eq v0, v4, :cond_16

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget v4, v4, Landroid/support/constraint/a/a/b;->g:I

    move/from16 v0, p2

    if-ne v4, v0, :cond_17

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget v4, v4, Landroid/support/constraint/a/a/b;->g:I

    move/from16 v0, p2

    if-ne v4, v0, :cond_17

    .line 2184
    :cond_16
    if-eqz v15, :cond_25

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v4, :cond_25

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v4, :cond_25

    .line 2185
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v5

    .line 2186
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v10

    .line 2187
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->f()Landroid/support/constraint/a/a/b;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v6

    .line 2188
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->f()Landroid/support/constraint/a/a/b;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v9

    .line 2189
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v4

    const/4 v7, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6, v4, v7}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 2190
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v4

    mul-int/lit8 v4, v4, -0x1

    const/4 v7, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9, v4, v7}, Landroid/support/constraint/a/e;->b(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 2191
    if-nez v16, :cond_17

    .line 2192
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v7

    move-object/from16 v0, p0

    iget v8, v0, Landroid/support/constraint/a/a/c;->N:F

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v11

    const/4 v12, 0x4

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v12}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;IFLandroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 2203
    :cond_17
    :goto_12
    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/constraint/a/a/c;->b:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_26

    .line 2289
    :cond_18
    :goto_13
    return-void

    .line 2073
    :cond_19
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v6, :cond_a

    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    if-ne v6, v7, :cond_a

    .line 2075
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    sget-object v7, Landroid/support/constraint/a/a/b$a;->b:Landroid/support/constraint/a/a/b$a;

    invoke-virtual {v6, v7}, Landroid/support/constraint/a/a/b;->a(Landroid/support/constraint/a/a/b$a;)V

    goto/16 :goto_7

    .line 2084
    :cond_1a
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v6, :cond_c

    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    if-ne v6, v7, :cond_c

    .line 2086
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    sget-object v7, Landroid/support/constraint/a/a/b$a;->b:Landroid/support/constraint/a/a/b$a;

    invoke-virtual {v6, v7}, Landroid/support/constraint/a/a/b;->a(Landroid/support/constraint/a/a/b$a;)V

    goto/16 :goto_8

    .line 2098
    :cond_1b
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v6, :cond_e

    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    if-ne v6, v7, :cond_e

    .line 2100
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    sget-object v7, Landroid/support/constraint/a/a/b$a;->b:Landroid/support/constraint/a/a/b$a;

    invoke-virtual {v6, v7}, Landroid/support/constraint/a/a/b;->a(Landroid/support/constraint/a/a/b$a;)V

    goto/16 :goto_9

    .line 2108
    :cond_1c
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v6, :cond_1d

    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    iget-object v6, v6, Landroid/support/constraint/a/a/b;->a:Landroid/support/constraint/a/a/c;

    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    if-ne v6, v7, :cond_1d

    .line 2110
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    sget-object v7, Landroid/support/constraint/a/a/b$a;->b:Landroid/support/constraint/a/a/b$a;

    invoke-virtual {v6, v7}, Landroid/support/constraint/a/a/b;->a(Landroid/support/constraint/a/a/b$a;)V

    :cond_1d
    move/from16 v22, v4

    move/from16 v16, v5

    goto/16 :goto_a

    .line 2125
    :cond_1e
    const/4 v6, 0x0

    goto/16 :goto_b

    .line 2126
    :cond_1f
    const/4 v7, 0x0

    goto/16 :goto_c

    .line 2154
    :cond_20
    if-nez v10, :cond_39

    if-eqz v6, :cond_39

    .line 2155
    const/4 v8, 0x1

    .line 2156
    move-object/from16 v0, p0

    iget v11, v0, Landroid/support/constraint/a/a/c;->A:I

    const/4 v12, -0x1

    if-ne v11, v12, :cond_39

    .line 2158
    const/high16 v11, 0x3f800000    # 1.0f

    div-float v7, v11, v7

    move/from16 v24, v7

    move/from16 v25, v8

    move/from16 v26, v9

    move/from16 v27, v6

    move/from16 v23, v5

    move v12, v4

    move v7, v10

    goto/16 :goto_f

    .line 2161
    :cond_21
    move-object/from16 v0, p0

    iget-object v11, v0, Landroid/support/constraint/a/a/c;->P:Landroid/support/constraint/a/a/c$a;

    sget-object v12, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-ne v11, v12, :cond_22

    .line 2162
    const/4 v4, 0x0

    .line 2163
    move-object/from16 v0, p0

    iget v8, v0, Landroid/support/constraint/a/a/c;->y:I

    int-to-float v8, v8

    mul-float/2addr v8, v7

    float-to-int v12, v8

    .line 2164
    const/4 v8, 0x1

    move/from16 v24, v7

    move/from16 v25, v4

    move/from16 v26, v9

    move/from16 v27, v6

    move/from16 v23, v5

    move v7, v8

    goto/16 :goto_f

    .line 2165
    :cond_22
    move-object/from16 v0, p0

    iget-object v11, v0, Landroid/support/constraint/a/a/c;->Q:Landroid/support/constraint/a/a/c$a;

    sget-object v12, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-ne v11, v12, :cond_39

    .line 2166
    const/4 v6, 0x1

    .line 2167
    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/constraint/a/a/c;->A:I

    const/4 v8, -0x1

    if-ne v5, v8, :cond_38

    .line 2169
    const/high16 v5, 0x3f800000    # 1.0f

    div-float/2addr v5, v7

    .line 2171
    :goto_14
    move-object/from16 v0, p0

    iget v7, v0, Landroid/support/constraint/a/a/c;->x:I

    int-to-float v7, v7

    mul-float/2addr v7, v5

    float-to-int v0, v7

    move/from16 v23, v0

    .line 2172
    const/4 v7, 0x1

    move/from16 v24, v5

    move/from16 v25, v6

    move/from16 v26, v9

    move/from16 v27, v7

    move v12, v4

    move v7, v10

    goto/16 :goto_f

    .line 2176
    :cond_23
    const/4 v15, 0x0

    goto/16 :goto_10

    .line 2180
    :cond_24
    const/4 v6, 0x0

    goto/16 :goto_11

    .line 2196
    :cond_25
    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    move-object/from16 v0, p0

    iget-object v9, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    move-object/from16 v0, p0

    iget v10, v0, Landroid/support/constraint/a/a/c;->F:I

    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/constraint/a/a/c;->F:I

    add-int v11, v4, v12

    move-object/from16 v0, p0

    iget v13, v0, Landroid/support/constraint/a/a/c;->K:I

    move-object/from16 v0, p0

    iget v14, v0, Landroid/support/constraint/a/a/c;->N:F

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/constraint/a/a/c;->c:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/constraint/a/a/c;->e:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/constraint/a/a/c;->f:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/constraint/a/a/c;->g:F

    move/from16 v20, v0

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    invoke-direct/range {v4 .. v20}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/e;ZZLandroid/support/constraint/a/a/b;Landroid/support/constraint/a/a/b;IIIIFZZIIIF)V

    goto/16 :goto_12

    .line 2207
    :cond_26
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->Q:Landroid/support/constraint/a/a/c$a;

    sget-object v5, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    if-ne v4, v5, :cond_2d

    move-object/from16 v0, p0

    instance-of v4, v0, Landroid/support/constraint/a/a/d;

    if-eqz v4, :cond_2d

    const/4 v6, 0x1

    .line 2210
    :goto_15
    if-eqz v26, :cond_2e

    const/4 v4, 0x1

    move/from16 v0, v25

    if-eq v0, v4, :cond_27

    const/4 v4, -0x1

    move/from16 v0, v25

    if-ne v0, v4, :cond_2e

    :cond_27
    const/4 v15, 0x1

    .line 2212
    :goto_16
    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/constraint/a/a/c;->J:I

    if-lez v4, :cond_30

    .line 2213
    move-object/from16 v0, p0

    iget-object v9, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    .line 2214
    const v4, 0x7fffffff

    move/from16 v0, p2

    if-eq v0, v4, :cond_28

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget v4, v4, Landroid/support/constraint/a/a/b;->g:I

    move/from16 v0, p2

    if-ne v4, v0, :cond_29

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->q:Landroid/support/constraint/a/a/b;

    iget v4, v4, Landroid/support/constraint/a/a/b;->g:I

    move/from16 v0, p2

    if-ne v4, v0, :cond_29

    .line 2215
    :cond_28
    invoke-virtual/range {p0 .. p0}, Landroid/support/constraint/a/a/c;->w()I

    move-result v4

    const/4 v5, 0x5

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2, v4, v5}, Landroid/support/constraint/a/e;->c(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)Landroid/support/constraint/a/b;

    .line 2218
    :cond_29
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->q:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v4, :cond_37

    .line 2219
    move-object/from16 v0, p0

    iget v12, v0, Landroid/support/constraint/a/a/c;->J:I

    .line 2220
    move-object/from16 v0, p0

    iget-object v9, v0, Landroid/support/constraint/a/a/c;->q:Landroid/support/constraint/a/a/b;

    .line 2222
    :goto_17
    const v4, 0x7fffffff

    move/from16 v0, p2

    if-eq v0, v4, :cond_2a

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget v4, v4, Landroid/support/constraint/a/a/b;->g:I

    move/from16 v0, p2

    if-ne v4, v0, :cond_2b

    iget v4, v9, Landroid/support/constraint/a/a/b;->g:I

    move/from16 v0, p2

    if-ne v4, v0, :cond_2b

    .line 2223
    :cond_2a
    if-eqz v15, :cond_2f

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v4, :cond_2f

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v4, :cond_2f

    .line 2224
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v5

    .line 2225
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v10

    .line 2226
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->f()Landroid/support/constraint/a/a/b;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v6

    .line 2227
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->f()Landroid/support/constraint/a/a/b;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v9

    .line 2228
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v4

    const/4 v7, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6, v4, v7}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 2229
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v4

    mul-int/lit8 v4, v4, -0x1

    const/4 v7, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9, v4, v7}, Landroid/support/constraint/a/e;->b(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 2230
    if-nez v22, :cond_2b

    .line 2231
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v7

    move-object/from16 v0, p0

    iget v8, v0, Landroid/support/constraint/a/a/c;->O:F

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v11

    const/4 v12, 0x4

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v12}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;IFLandroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 2264
    :cond_2b
    :goto_18
    if-eqz v26, :cond_18

    .line 2265
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->b()Landroid/support/constraint/a/b;

    move-result-object v4

    .line 2266
    const v5, 0x7fffffff

    move/from16 v0, p2

    if-eq v0, v5, :cond_2c

    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget v5, v5, Landroid/support/constraint/a/a/b;->g:I

    move/from16 v0, p2

    if-ne v5, v0, :cond_18

    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget v5, v5, Landroid/support/constraint/a/a/b;->g:I

    move/from16 v0, p2

    if-ne v5, v0, :cond_18

    .line 2267
    :cond_2c
    if-nez v25, :cond_33

    move-object/from16 v5, v30

    move-object/from16 v6, v31

    move-object/from16 v7, v28

    move-object/from16 v8, v29

    move/from16 v9, v24

    .line 2268
    invoke-virtual/range {v4 .. v9}, Landroid/support/constraint/a/b;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;F)Landroid/support/constraint/a/b;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    goto/16 :goto_13

    .line 2207
    :cond_2d
    const/4 v6, 0x0

    goto/16 :goto_15

    .line 2210
    :cond_2e
    const/4 v15, 0x0

    goto/16 :goto_16

    .line 2235
    :cond_2f
    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    move-object/from16 v0, p0

    iget v10, v0, Landroid/support/constraint/a/a/c;->G:I

    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/constraint/a/a/c;->G:I

    add-int v11, v4, v12

    move-object/from16 v0, p0

    iget v13, v0, Landroid/support/constraint/a/a/c;->L:I

    move-object/from16 v0, p0

    iget v14, v0, Landroid/support/constraint/a/a/c;->O:F

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/constraint/a/a/c;->d:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/constraint/a/a/c;->h:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/constraint/a/a/c;->i:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/constraint/a/a/c;->j:F

    move/from16 v20, v0

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move/from16 v7, v27

    move/from16 v16, v22

    invoke-direct/range {v4 .. v20}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/e;ZZLandroid/support/constraint/a/a/b;Landroid/support/constraint/a/a/b;IIIIFZZIIIF)V

    .line 2239
    const/4 v4, 0x5

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    move/from16 v3, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/support/constraint/a/e;->c(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)Landroid/support/constraint/a/b;

    goto/16 :goto_18

    .line 2243
    :cond_30
    const v4, 0x7fffffff

    move/from16 v0, p2

    if-eq v0, v4, :cond_31

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget v4, v4, Landroid/support/constraint/a/a/b;->g:I

    move/from16 v0, p2

    if-ne v4, v0, :cond_2b

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget v4, v4, Landroid/support/constraint/a/a/b;->g:I

    move/from16 v0, p2

    if-ne v4, v0, :cond_2b

    .line 2244
    :cond_31
    if-eqz v15, :cond_32

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v4, :cond_32

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget-object v4, v4, Landroid/support/constraint/a/a/b;->c:Landroid/support/constraint/a/a/b;

    if-eqz v4, :cond_32

    .line 2245
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v5

    .line 2246
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v10

    .line 2247
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->f()Landroid/support/constraint/a/a/b;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v6

    .line 2248
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->f()Landroid/support/constraint/a/a/b;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/support/constraint/a/e;->a(Ljava/lang/Object;)Landroid/support/constraint/a/g;

    move-result-object v9

    .line 2249
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v4

    const/4 v7, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6, v4, v7}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 2250
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v4

    mul-int/lit8 v4, v4, -0x1

    const/4 v7, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9, v4, v7}, Landroid/support/constraint/a/e;->b(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 2251
    if-nez v22, :cond_2b

    .line 2252
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v7

    move-object/from16 v0, p0

    iget v8, v0, Landroid/support/constraint/a/a/c;->O:F

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/b;->d()I

    move-result v11

    const/4 v12, 0x4

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v12}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;IFLandroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    goto/16 :goto_18

    .line 2256
    :cond_32
    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    move-object/from16 v0, p0

    iget-object v9, v0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    move-object/from16 v0, p0

    iget v10, v0, Landroid/support/constraint/a/a/c;->G:I

    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/constraint/a/a/c;->G:I

    add-int v11, v4, v23

    move-object/from16 v0, p0

    iget v13, v0, Landroid/support/constraint/a/a/c;->L:I

    move-object/from16 v0, p0

    iget v14, v0, Landroid/support/constraint/a/a/c;->O:F

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/constraint/a/a/c;->d:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/constraint/a/a/c;->h:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/constraint/a/a/c;->i:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/constraint/a/a/c;->j:F

    move/from16 v20, v0

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move/from16 v7, v27

    move/from16 v12, v23

    move/from16 v16, v22

    invoke-direct/range {v4 .. v20}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/e;ZZLandroid/support/constraint/a/a/b;Landroid/support/constraint/a/a/b;IIIIFZZIIIF)V

    goto/16 :goto_18

    .line 2269
    :cond_33
    const/4 v5, 0x1

    move/from16 v0, v25

    if-ne v0, v5, :cond_34

    move-object/from16 v5, v28

    move-object/from16 v6, v29

    move-object/from16 v7, v30

    move-object/from16 v8, v31

    move/from16 v9, v24

    .line 2270
    invoke-virtual/range {v4 .. v9}, Landroid/support/constraint/a/b;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;F)Landroid/support/constraint/a/b;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    goto/16 :goto_13

    .line 2272
    :cond_34
    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/constraint/a/a/c;->e:I

    if-lez v5, :cond_35

    .line 2273
    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/constraint/a/a/c;->e:I

    const/4 v6, 0x3

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2, v5, v6}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 2275
    :cond_35
    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/constraint/a/a/c;->h:I

    if-lez v5, :cond_36

    .line 2276
    move-object/from16 v0, p0

    iget v5, v0, Landroid/support/constraint/a/a/c;->h:I

    const/4 v6, 0x3

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2, v5, v6}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;II)V

    .line 2278
    :cond_36
    const/4 v10, 0x4

    move-object/from16 v5, v30

    move-object/from16 v6, v31

    move-object/from16 v7, v28

    move-object/from16 v8, v29

    move/from16 v9, v24

    .line 2279
    invoke-virtual/range {v4 .. v9}, Landroid/support/constraint/a/b;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;F)Landroid/support/constraint/a/b;

    .line 2280
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->d()Landroid/support/constraint/a/g;

    move-result-object v5

    .line 2281
    invoke-virtual/range {p1 .. p1}, Landroid/support/constraint/a/e;->d()Landroid/support/constraint/a/g;

    move-result-object v6

    .line 2282
    iput v10, v5, Landroid/support/constraint/a/g;->c:I

    .line 2283
    iput v10, v6, Landroid/support/constraint/a/g;->c:I

    .line 2284
    invoke-virtual {v4, v5, v6}, Landroid/support/constraint/a/b;->a(Landroid/support/constraint/a/g;Landroid/support/constraint/a/g;)Landroid/support/constraint/a/b;

    .line 2285
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/support/constraint/a/e;->a(Landroid/support/constraint/a/b;)V

    goto/16 :goto_13

    :cond_37
    move/from16 v12, v23

    goto/16 :goto_17

    :cond_38
    move v5, v7

    goto/16 :goto_14

    :cond_39
    move/from16 v24, v7

    move/from16 v25, v8

    move/from16 v26, v9

    move/from16 v27, v6

    move/from16 v23, v5

    move v12, v4

    move v7, v10

    goto/16 :goto_f

    :cond_3a
    move v6, v7

    goto/16 :goto_e

    :cond_3b
    move v10, v6

    goto/16 :goto_d

    :cond_3c
    move v4, v6

    goto/16 :goto_6

    :cond_3d
    move v5, v4

    goto/16 :goto_5

    :cond_3e
    move/from16 v22, v6

    move/from16 v16, v4

    goto/16 :goto_a

    :cond_3f
    move-object/from16 v21, v4

    goto/16 :goto_4

    :cond_40
    move-object/from16 v28, v5

    goto/16 :goto_3

    :cond_41
    move-object/from16 v29, v6

    goto/16 :goto_2

    :cond_42
    move-object/from16 v30, v7

    goto/16 :goto_1

    :cond_43
    move-object/from16 v31, v8

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1311
    iput-object p1, p0, Landroid/support/constraint/a/a/c;->ap:Ljava/lang/Object;

    .line 1312
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 1062
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_2

    .line 1063
    :cond_0
    iput v3, p0, Landroid/support/constraint/a/a/c;->z:F

    .line 1116
    :cond_1
    :goto_0
    return-void

    .line 1066
    :cond_2
    const/4 v2, -0x1

    .line 1068
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    .line 1069
    const/16 v5, 0x2c

    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    .line 1070
    if-lez v5, :cond_3

    add-int/lit8 v6, v4, -0x1

    if-ge v5, v6, :cond_3

    .line 1071
    invoke-virtual {p1, v0, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 1072
    const-string v7, "W"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1077
    :goto_1
    add-int/lit8 v2, v5, 0x1

    move v8, v2

    move v2, v0

    move v0, v8

    .line 1081
    :cond_3
    const/16 v5, 0x3a

    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    .line 1083
    if-ltz v5, :cond_6

    add-int/lit8 v4, v4, -0x1

    if-ge v5, v4, :cond_6

    .line 1084
    invoke-virtual {p1, v0, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1085
    add-int/lit8 v4, v5, 0x1

    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 1086
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_7

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_7

    .line 1088
    :try_start_0
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    .line 1089
    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    .line 1090
    cmpl-float v5, v0, v3

    if-lez v5, :cond_7

    cmpl-float v5, v4, v3

    if-lez v5, :cond_7

    .line 1091
    if-ne v2, v1, :cond_5

    .line 1092
    div-float v0, v4, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1112
    :goto_2
    cmpl-float v1, v0, v3

    if-lez v1, :cond_1

    .line 1113
    iput v0, p0, Landroid/support/constraint/a/a/c;->z:F

    .line 1114
    iput v2, p0, Landroid/support/constraint/a/a/c;->A:I

    goto :goto_0

    .line 1074
    :cond_4
    const-string v0, "H"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v1

    .line 1075
    goto :goto_1

    .line 1094
    :cond_5
    div-float/2addr v0, v4

    :try_start_1
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    goto :goto_2

    .line 1097
    :catch_0
    move-exception v0

    move v0, v3

    goto :goto_2

    .line 1102
    :cond_6
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 1103
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_7

    .line 1105
    :try_start_2
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1

    move-result v0

    goto :goto_2

    .line 1106
    :catch_1
    move-exception v0

    move v0, v3

    goto :goto_2

    :cond_7
    move v0, v3

    goto :goto_2

    :cond_8
    move v0, v2

    goto :goto_1
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 427
    iput-boolean p1, p0, Landroid/support/constraint/a/a/c;->k:Z

    .line 428
    return-void
.end method

.method public b(F)V
    .locals 0

    .prologue
    .line 1163
    iput p1, p0, Landroid/support/constraint/a/a/c;->O:F

    .line 1164
    return-void
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 477
    iput p1, p0, Landroid/support/constraint/a/a/c;->ar:I

    .line 478
    return-void
.end method

.method public b(II)V
    .locals 0

    .prologue
    .line 884
    iput p1, p0, Landroid/support/constraint/a/a/c;->H:I

    .line 885
    iput p2, p0, Landroid/support/constraint/a/a/c;->I:I

    .line 886
    return-void
.end method

.method public b(IIIF)V
    .locals 0

    .prologue
    .line 1050
    iput p1, p0, Landroid/support/constraint/a/a/c;->d:I

    .line 1051
    iput p2, p0, Landroid/support/constraint/a/a/c;->h:I

    .line 1052
    iput p3, p0, Landroid/support/constraint/a/a/c;->i:I

    .line 1053
    iput p4, p0, Landroid/support/constraint/a/a/c;->j:F

    .line 1054
    return-void
.end method

.method public b(Landroid/support/constraint/a/a/c$a;)V
    .locals 2

    .prologue
    .line 1916
    iput-object p1, p0, Landroid/support/constraint/a/a/c;->Q:Landroid/support/constraint/a/a/c$a;

    .line 1917
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->Q:Landroid/support/constraint/a/a/c$a;

    sget-object v1, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    if-ne v0, v1, :cond_0

    .line 1918
    iget v0, p0, Landroid/support/constraint/a/a/c;->ao:I

    invoke-virtual {p0, v0}, Landroid/support/constraint/a/a/c;->f(I)V

    .line 1920
    :cond_0
    return-void
.end method

.method public b(Landroid/support/constraint/a/e;I)V
    .locals 4

    .prologue
    .line 2515
    const v0, 0x7fffffff

    if-ne p2, v0, :cond_1

    .line 2516
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    invoke-virtual {p1, v0}, Landroid/support/constraint/a/e;->b(Ljava/lang/Object;)I

    move-result v0

    .line 2517
    iget-object v1, p0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    invoke-virtual {p1, v1}, Landroid/support/constraint/a/e;->b(Ljava/lang/Object;)I

    move-result v1

    .line 2518
    iget-object v2, p0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    invoke-virtual {p1, v2}, Landroid/support/constraint/a/e;->b(Ljava/lang/Object;)I

    move-result v2

    .line 2519
    iget-object v3, p0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    invoke-virtual {p1, v3}, Landroid/support/constraint/a/e;->b(Ljava/lang/Object;)I

    move-result v3

    .line 2520
    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/support/constraint/a/a/c;->a(IIII)V

    .line 2537
    :cond_0
    :goto_0
    return-void

    .line 2521
    :cond_1
    const/4 v0, -0x2

    if-ne p2, v0, :cond_2

    .line 2522
    iget v0, p0, Landroid/support/constraint/a/a/c;->B:I

    iget v1, p0, Landroid/support/constraint/a/a/c;->C:I

    iget v2, p0, Landroid/support/constraint/a/a/c;->D:I

    iget v3, p0, Landroid/support/constraint/a/a/c;->E:I

    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/support/constraint/a/a/c;->a(IIII)V

    goto :goto_0

    .line 2524
    :cond_2
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    iget v0, v0, Landroid/support/constraint/a/a/b;->g:I

    if-ne v0, p2, :cond_3

    .line 2525
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->m:Landroid/support/constraint/a/a/b;

    invoke-virtual {p1, v0}, Landroid/support/constraint/a/e;->b(Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/a/a/c;->B:I

    .line 2527
    :cond_3
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    iget v0, v0, Landroid/support/constraint/a/a/b;->g:I

    if-ne v0, p2, :cond_4

    .line 2528
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->n:Landroid/support/constraint/a/a/b;

    invoke-virtual {p1, v0}, Landroid/support/constraint/a/e;->b(Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/a/a/c;->C:I

    .line 2530
    :cond_4
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    iget v0, v0, Landroid/support/constraint/a/a/b;->g:I

    if-ne v0, p2, :cond_5

    .line 2531
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->o:Landroid/support/constraint/a/a/b;

    invoke-virtual {p1, v0}, Landroid/support/constraint/a/e;->b(Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/a/a/c;->D:I

    .line 2533
    :cond_5
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    iget v0, v0, Landroid/support/constraint/a/a/b;->g:I

    if-ne v0, p2, :cond_0

    .line 2534
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->p:Landroid/support/constraint/a/a/b;

    invoke-virtual {p1, v0}, Landroid/support/constraint/a/e;->b(Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Landroid/support/constraint/a/a/c;->E:I

    goto :goto_0
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 443
    iput-boolean p1, p0, Landroid/support/constraint/a/a/c;->l:Z

    .line 444
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Landroid/support/constraint/a/a/c;
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->w:Landroid/support/constraint/a/a/c;

    return-object v0
.end method

.method public c(F)V
    .locals 0

    .prologue
    .line 1345
    iput p1, p0, Landroid/support/constraint/a/a/c;->af:F

    .line 1346
    return-void
.end method

.method public c(I)V
    .locals 0

    .prologue
    .line 854
    iput p1, p0, Landroid/support/constraint/a/a/c;->F:I

    .line 855
    return-void
.end method

.method public c(II)V
    .locals 2

    .prologue
    .line 1274
    iput p1, p0, Landroid/support/constraint/a/a/c;->F:I

    .line 1275
    sub-int v0, p2, p1

    iput v0, p0, Landroid/support/constraint/a/a/c;->x:I

    .line 1276
    iget v0, p0, Landroid/support/constraint/a/a/c;->x:I

    iget v1, p0, Landroid/support/constraint/a/a/c;->K:I

    if-ge v0, v1, :cond_0

    .line 1277
    iget v0, p0, Landroid/support/constraint/a/a/c;->K:I

    iput v0, p0, Landroid/support/constraint/a/a/c;->x:I

    .line 1279
    :cond_0
    return-void
.end method

.method public d()I
    .locals 1

    .prologue
    .line 486
    iget v0, p0, Landroid/support/constraint/a/a/c;->ar:I

    return v0
.end method

.method public d(F)V
    .locals 0

    .prologue
    .line 1354
    iput p1, p0, Landroid/support/constraint/a/a/c;->ag:F

    .line 1355
    return-void
.end method

.method public d(I)V
    .locals 0

    .prologue
    .line 863
    iput p1, p0, Landroid/support/constraint/a/a/c;->G:I

    .line 864
    return-void
.end method

.method public d(II)V
    .locals 2

    .prologue
    .line 1288
    iput p1, p0, Landroid/support/constraint/a/a/c;->G:I

    .line 1289
    sub-int v0, p2, p1

    iput v0, p0, Landroid/support/constraint/a/a/c;->y:I

    .line 1290
    iget v0, p0, Landroid/support/constraint/a/a/c;->y:I

    iget v1, p0, Landroid/support/constraint/a/a/c;->L:I

    if-ge v0, v1, :cond_0

    .line 1291
    iget v0, p0, Landroid/support/constraint/a/a/c;->L:I

    iput v0, p0, Landroid/support/constraint/a/a/c;->y:I

    .line 1293
    :cond_0
    return-void
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 495
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->as:Ljava/lang/String;

    return-object v0
.end method

.method public e(I)V
    .locals 2

    .prologue
    .line 1008
    iput p1, p0, Landroid/support/constraint/a/a/c;->x:I

    .line 1009
    iget v0, p0, Landroid/support/constraint/a/a/c;->x:I

    iget v1, p0, Landroid/support/constraint/a/a/c;->K:I

    if-ge v0, v1, :cond_0

    .line 1010
    iget v0, p0, Landroid/support/constraint/a/a/c;->K:I

    iput v0, p0, Landroid/support/constraint/a/a/c;->x:I

    .line 1012
    :cond_0
    return-void
.end method

.method public f()I
    .locals 1

    .prologue
    .line 583
    iget v0, p0, Landroid/support/constraint/a/a/c;->F:I

    return v0
.end method

.method public f(I)V
    .locals 2

    .prologue
    .line 1020
    iput p1, p0, Landroid/support/constraint/a/a/c;->y:I

    .line 1021
    iget v0, p0, Landroid/support/constraint/a/a/c;->y:I

    iget v1, p0, Landroid/support/constraint/a/a/c;->L:I

    if-ge v0, v1, :cond_0

    .line 1022
    iget v0, p0, Landroid/support/constraint/a/a/c;->L:I

    iput v0, p0, Landroid/support/constraint/a/a/c;->y:I

    .line 1024
    :cond_0
    return-void
.end method

.method public g()I
    .locals 1

    .prologue
    .line 592
    iget v0, p0, Landroid/support/constraint/a/a/c;->G:I

    return v0
.end method

.method public g(I)V
    .locals 1

    .prologue
    .line 1172
    if-gez p1, :cond_0

    .line 1173
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/constraint/a/a/c;->K:I

    .line 1177
    :goto_0
    return-void

    .line 1175
    :cond_0
    iput p1, p0, Landroid/support/constraint/a/a/c;->K:I

    goto :goto_0
.end method

.method public h()I
    .locals 2

    .prologue
    .line 601
    iget v0, p0, Landroid/support/constraint/a/a/c;->ar:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 602
    const/4 v0, 0x0

    .line 604
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Landroid/support/constraint/a/a/c;->x:I

    goto :goto_0
.end method

.method public h(I)V
    .locals 1

    .prologue
    .line 1185
    if-gez p1, :cond_0

    .line 1186
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/constraint/a/a/c;->L:I

    .line 1190
    :goto_0
    return-void

    .line 1188
    :cond_0
    iput p1, p0, Landroid/support/constraint/a/a/c;->L:I

    goto :goto_0
.end method

.method public i()I
    .locals 3

    .prologue
    .line 608
    iget v0, p0, Landroid/support/constraint/a/a/c;->x:I

    .line 609
    iget-object v1, p0, Landroid/support/constraint/a/a/c;->P:Landroid/support/constraint/a/a/c$a;

    sget-object v2, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-ne v1, v2, :cond_0

    .line 610
    iget v1, p0, Landroid/support/constraint/a/a/c;->c:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 611
    iget v1, p0, Landroid/support/constraint/a/a/c;->e:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 618
    :goto_0
    iget v1, p0, Landroid/support/constraint/a/a/c;->f:I

    if-lez v1, :cond_0

    iget v1, p0, Landroid/support/constraint/a/a/c;->f:I

    if-ge v1, v0, :cond_0

    .line 619
    iget v0, p0, Landroid/support/constraint/a/a/c;->f:I

    .line 622
    :cond_0
    return v0

    .line 612
    :cond_1
    iget v0, p0, Landroid/support/constraint/a/a/c;->e:I

    if-lez v0, :cond_2

    .line 613
    iget v0, p0, Landroid/support/constraint/a/a/c;->e:I

    .line 614
    iput v0, p0, Landroid/support/constraint/a/a/c;->x:I

    goto :goto_0

    .line 616
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i(I)V
    .locals 0

    .prologue
    .line 1198
    iput p1, p0, Landroid/support/constraint/a/a/c;->an:I

    .line 1199
    return-void
.end method

.method public j()I
    .locals 3

    .prologue
    .line 626
    iget v0, p0, Landroid/support/constraint/a/a/c;->y:I

    .line 627
    iget-object v1, p0, Landroid/support/constraint/a/a/c;->Q:Landroid/support/constraint/a/a/c$a;

    sget-object v2, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    if-ne v1, v2, :cond_0

    .line 628
    iget v1, p0, Landroid/support/constraint/a/a/c;->d:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 629
    iget v1, p0, Landroid/support/constraint/a/a/c;->h:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 636
    :goto_0
    iget v1, p0, Landroid/support/constraint/a/a/c;->i:I

    if-lez v1, :cond_0

    iget v1, p0, Landroid/support/constraint/a/a/c;->i:I

    if-ge v1, v0, :cond_0

    .line 637
    iget v0, p0, Landroid/support/constraint/a/a/c;->i:I

    .line 640
    :cond_0
    return v0

    .line 630
    :cond_1
    iget v0, p0, Landroid/support/constraint/a/a/c;->h:I

    if-lez v0, :cond_2

    .line 631
    iget v0, p0, Landroid/support/constraint/a/a/c;->h:I

    .line 632
    iput v0, p0, Landroid/support/constraint/a/a/c;->y:I

    goto :goto_0

    .line 634
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j(I)V
    .locals 0

    .prologue
    .line 1207
    iput p1, p0, Landroid/support/constraint/a/a/c;->ao:I

    .line 1208
    return-void
.end method

.method public k()I
    .locals 1

    .prologue
    .line 649
    iget v0, p0, Landroid/support/constraint/a/a/c;->an:I

    return v0
.end method

.method public k(I)V
    .locals 0

    .prologue
    .line 1301
    iput p1, p0, Landroid/support/constraint/a/a/c;->J:I

    .line 1302
    return-void
.end method

.method public l()I
    .locals 2

    .prologue
    .line 658
    iget v0, p0, Landroid/support/constraint/a/a/c;->ar:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 659
    const/4 v0, 0x0

    .line 661
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Landroid/support/constraint/a/a/c;->y:I

    goto :goto_0
.end method

.method public l(I)V
    .locals 0

    .prologue
    .line 1364
    iput p1, p0, Landroid/support/constraint/a/a/c;->ab:I

    .line 1365
    return-void
.end method

.method public m()I
    .locals 1

    .prologue
    .line 670
    iget v0, p0, Landroid/support/constraint/a/a/c;->ao:I

    return v0
.end method

.method public m(I)V
    .locals 0

    .prologue
    .line 1384
    iput p1, p0, Landroid/support/constraint/a/a/c;->ac:I

    .line 1385
    return-void
.end method

.method public n()I
    .locals 2

    .prologue
    .line 679
    iget v0, p0, Landroid/support/constraint/a/a/c;->aj:I

    iget v1, p0, Landroid/support/constraint/a/a/c;->H:I

    add-int/2addr v0, v1

    return v0
.end method

.method public o()I
    .locals 2

    .prologue
    .line 688
    iget v0, p0, Landroid/support/constraint/a/a/c;->ak:I

    iget v1, p0, Landroid/support/constraint/a/a/c;->I:I

    add-int/2addr v0, v1

    return v0
.end method

.method public p()I
    .locals 2

    .prologue
    .line 705
    invoke-virtual {p0}, Landroid/support/constraint/a/a/c;->o()I

    move-result v0

    iget v1, p0, Landroid/support/constraint/a/a/c;->am:I

    add-int/2addr v0, v1

    return v0
.end method

.method public q()I
    .locals 2

    .prologue
    .line 714
    invoke-virtual {p0}, Landroid/support/constraint/a/a/c;->n()I

    move-result v0

    iget v1, p0, Landroid/support/constraint/a/a/c;->al:I

    add-int/2addr v0, v1

    return v0
.end method

.method protected r()I
    .locals 2

    .prologue
    .line 724
    iget v0, p0, Landroid/support/constraint/a/a/c;->F:I

    iget v1, p0, Landroid/support/constraint/a/a/c;->H:I

    add-int/2addr v0, v1

    return v0
.end method

.method protected s()I
    .locals 2

    .prologue
    .line 734
    iget v0, p0, Landroid/support/constraint/a/a/c;->G:I

    iget v1, p0, Landroid/support/constraint/a/a/c;->I:I

    add-int/2addr v0, v1

    return v0
.end method

.method public t()I
    .locals 2

    .prologue
    .line 779
    invoke-virtual {p0}, Landroid/support/constraint/a/a/c;->f()I

    move-result v0

    iget v1, p0, Landroid/support/constraint/a/a/c;->x:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 544
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Landroid/support/constraint/a/a/c;->at:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "type: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Landroid/support/constraint/a/a/c;->at:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Landroid/support/constraint/a/a/c;->as:Ljava/lang/String;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "id: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Landroid/support/constraint/a/a/c;->as:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/constraint/a/a/c;->F:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/constraint/a/a/c;->G:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") - ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/constraint/a/a/c;->x:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " x "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/constraint/a/a/c;->y:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") wrap: ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/constraint/a/a/c;->an:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " x "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/constraint/a/a/c;->ao:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0

    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method public u()I
    .locals 2

    .prologue
    .line 788
    invoke-virtual {p0}, Landroid/support/constraint/a/a/c;->g()I

    move-result v0

    iget v1, p0, Landroid/support/constraint/a/a/c;->y:I

    add-int/2addr v0, v1

    return v0
.end method

.method public v()Z
    .locals 1

    .prologue
    .line 817
    iget v0, p0, Landroid/support/constraint/a/a/c;->J:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public w()I
    .locals 1

    .prologue
    .line 826
    iget v0, p0, Landroid/support/constraint/a/a/c;->J:I

    return v0
.end method

.method public x()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 836
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->ap:Ljava/lang/Object;

    return-object v0
.end method

.method public y()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/constraint/a/a/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 845
    iget-object v0, p0, Landroid/support/constraint/a/a/c;->v:Ljava/util/ArrayList;

    return-object v0
.end method

.method public z()V
    .locals 5

    .prologue
    .line 927
    iget v0, p0, Landroid/support/constraint/a/a/c;->F:I

    .line 928
    iget v1, p0, Landroid/support/constraint/a/a/c;->G:I

    .line 929
    iget v2, p0, Landroid/support/constraint/a/a/c;->F:I

    iget v3, p0, Landroid/support/constraint/a/a/c;->x:I

    add-int/2addr v2, v3

    .line 930
    iget v3, p0, Landroid/support/constraint/a/a/c;->G:I

    iget v4, p0, Landroid/support/constraint/a/a/c;->y:I

    add-int/2addr v3, v4

    .line 931
    iput v0, p0, Landroid/support/constraint/a/a/c;->aj:I

    .line 932
    iput v1, p0, Landroid/support/constraint/a/a/c;->ak:I

    .line 933
    sub-int v0, v2, v0

    iput v0, p0, Landroid/support/constraint/a/a/c;->al:I

    .line 934
    sub-int v0, v3, v1

    iput v0, p0, Landroid/support/constraint/a/a/c;->am:I

    .line 935
    return-void
.end method
