.class public Landroid/support/constraint/Barrier;
.super Landroid/support/constraint/a;
.source "Barrier.java"


# instance fields
.field private f:I

.field private g:I

.field private h:Landroid/support/constraint/a/a/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 51
    invoke-direct {p0, p1}, Landroid/support/constraint/a;-><init>(Landroid/content/Context;)V

    .line 46
    iput v0, p0, Landroid/support/constraint/Barrier;->f:I

    .line 47
    iput v0, p0, Landroid/support/constraint/Barrier;->g:I

    .line 52
    const/16 v0, 0x8

    invoke-super {p0, v0}, Landroid/support/constraint/a;->setVisibility(I)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 56
    invoke-direct {p0, p1, p2}, Landroid/support/constraint/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    iput v0, p0, Landroid/support/constraint/Barrier;->f:I

    .line 47
    iput v0, p0, Landroid/support/constraint/Barrier;->g:I

    .line 57
    const/16 v0, 0x8

    invoke-super {p0, v0}, Landroid/support/constraint/a;->setVisibility(I)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-direct {p0, p1, p2, p3}, Landroid/support/constraint/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    iput v0, p0, Landroid/support/constraint/Barrier;->f:I

    .line 47
    iput v0, p0, Landroid/support/constraint/Barrier;->g:I

    .line 62
    const/16 v0, 0x8

    invoke-super {p0, v0}, Landroid/support/constraint/a;->setVisibility(I)V

    .line 63
    return-void
.end method


# virtual methods
.method protected a(Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 106
    invoke-super {p0, p1}, Landroid/support/constraint/a;->a(Landroid/util/AttributeSet;)V

    .line 107
    new-instance v0, Landroid/support/constraint/a/a/a;

    invoke-direct {v0}, Landroid/support/constraint/a/a/a;-><init>()V

    iput-object v0, p0, Landroid/support/constraint/Barrier;->h:Landroid/support/constraint/a/a/a;

    .line 108
    if-eqz p1, :cond_1

    .line 109
    invoke-virtual {p0}, Landroid/support/constraint/Barrier;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v2, Landroid/support/constraint/e$b;->ConstraintLayout_Layout:[I

    invoke-virtual {v0, p1, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 110
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v3

    move v0, v1

    .line 111
    :goto_0
    if-ge v0, v3, :cond_1

    .line 112
    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v4

    .line 113
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_barrierDirection:I

    if-ne v4, v5, :cond_0

    .line 114
    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    invoke-virtual {p0, v4}, Landroid/support/constraint/Barrier;->setType(I)V

    .line 111
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 118
    :cond_1
    iget-object v0, p0, Landroid/support/constraint/Barrier;->h:Landroid/support/constraint/a/a/a;

    iput-object v0, p0, Landroid/support/constraint/Barrier;->d:Landroid/support/constraint/a/a/f;

    .line 119
    invoke-virtual {p0}, Landroid/support/constraint/Barrier;->a()V

    .line 120
    return-void
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Landroid/support/constraint/Barrier;->f:I

    return v0
.end method

.method public setType(I)V
    .locals 6

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x5

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 73
    iput p1, p0, Landroid/support/constraint/Barrier;->f:I

    .line 74
    iput p1, p0, Landroid/support/constraint/Barrier;->g:I

    .line 75
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-ge v0, v3, :cond_2

    .line 78
    iget v0, p0, Landroid/support/constraint/Barrier;->f:I

    if-ne v0, v4, :cond_1

    .line 79
    iput v2, p0, Landroid/support/constraint/Barrier;->g:I

    .line 101
    :cond_0
    :goto_0
    iget-object v0, p0, Landroid/support/constraint/Barrier;->h:Landroid/support/constraint/a/a/a;

    iget v1, p0, Landroid/support/constraint/Barrier;->g:I

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/a;->a(I)V

    .line 102
    return-void

    .line 80
    :cond_1
    iget v0, p0, Landroid/support/constraint/Barrier;->f:I

    if-ne v0, v5, :cond_0

    .line 81
    iput v1, p0, Landroid/support/constraint/Barrier;->g:I

    goto :goto_0

    .line 85
    :cond_2
    invoke-virtual {p0}, Landroid/support/constraint/Barrier;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v0

    if-ne v1, v0, :cond_3

    move v0, v1

    .line 87
    :goto_1
    if-eqz v0, :cond_5

    .line 88
    iget v0, p0, Landroid/support/constraint/Barrier;->f:I

    if-ne v0, v4, :cond_4

    .line 89
    iput v1, p0, Landroid/support/constraint/Barrier;->g:I

    goto :goto_0

    :cond_3
    move v0, v2

    .line 86
    goto :goto_1

    .line 90
    :cond_4
    iget v0, p0, Landroid/support/constraint/Barrier;->f:I

    if-ne v0, v5, :cond_0

    .line 91
    iput v2, p0, Landroid/support/constraint/Barrier;->g:I

    goto :goto_0

    .line 94
    :cond_5
    iget v0, p0, Landroid/support/constraint/Barrier;->f:I

    if-ne v0, v4, :cond_6

    .line 95
    iput v2, p0, Landroid/support/constraint/Barrier;->g:I

    goto :goto_0

    .line 96
    :cond_6
    iget v0, p0, Landroid/support/constraint/Barrier;->f:I

    if-ne v0, v5, :cond_0

    .line 97
    iput v1, p0, Landroid/support/constraint/Barrier;->g:I

    goto :goto_0
.end method
