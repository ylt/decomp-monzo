.class Landroid/support/constraint/b$a;
.super Ljava/lang/Object;
.source "ConstraintSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/constraint/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field public A:I

.field public B:I

.field public C:I

.field public D:I

.field public E:I

.field public F:I

.field public G:I

.field public H:I

.field public I:I

.field public J:I

.field public K:I

.field public L:I

.field public M:I

.field public N:F

.field public O:F

.field public P:I

.field public Q:I

.field public R:F

.field public S:Z

.field public T:F

.field public U:F

.field public V:F

.field public W:F

.field public X:F

.field public Y:F

.field public Z:F

.field a:Z

.field public aa:F

.field public ab:F

.field public ac:F

.field public ad:F

.field public ae:I

.field public af:I

.field public ag:I

.field public ah:I

.field public ai:I

.field public aj:I

.field public ak:I

.field public al:I

.field public am:[I

.field public b:I

.field public c:I

.field d:I

.field public e:I

.field public f:I

.field public g:F

.field public h:I

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field public m:I

.field public n:I

.field public o:I

.field public p:I

.field public q:I

.field public r:I

.field public s:I

.field public t:I

.field public u:F

.field public v:F

.field public w:Ljava/lang/String;

.field public x:I

.field public y:I

.field public z:I


# direct methods
.method private constructor <init>()V
    .locals 6

    .prologue
    const/high16 v5, 0x3f000000    # 0.5f

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 333
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 334
    iput-boolean v3, p0, Landroid/support/constraint/b$a;->a:Z

    .line 339
    iput v1, p0, Landroid/support/constraint/b$a;->e:I

    .line 340
    iput v1, p0, Landroid/support/constraint/b$a;->f:I

    .line 341
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Landroid/support/constraint/b$a;->g:F

    .line 343
    iput v1, p0, Landroid/support/constraint/b$a;->h:I

    .line 344
    iput v1, p0, Landroid/support/constraint/b$a;->i:I

    .line 345
    iput v1, p0, Landroid/support/constraint/b$a;->j:I

    .line 346
    iput v1, p0, Landroid/support/constraint/b$a;->k:I

    .line 347
    iput v1, p0, Landroid/support/constraint/b$a;->l:I

    .line 348
    iput v1, p0, Landroid/support/constraint/b$a;->m:I

    .line 349
    iput v1, p0, Landroid/support/constraint/b$a;->n:I

    .line 350
    iput v1, p0, Landroid/support/constraint/b$a;->o:I

    .line 351
    iput v1, p0, Landroid/support/constraint/b$a;->p:I

    .line 353
    iput v1, p0, Landroid/support/constraint/b$a;->q:I

    .line 354
    iput v1, p0, Landroid/support/constraint/b$a;->r:I

    .line 355
    iput v1, p0, Landroid/support/constraint/b$a;->s:I

    .line 356
    iput v1, p0, Landroid/support/constraint/b$a;->t:I

    .line 358
    iput v5, p0, Landroid/support/constraint/b$a;->u:F

    .line 359
    iput v5, p0, Landroid/support/constraint/b$a;->v:F

    .line 360
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/constraint/b$a;->w:Ljava/lang/String;

    .line 362
    iput v1, p0, Landroid/support/constraint/b$a;->x:I

    .line 363
    iput v1, p0, Landroid/support/constraint/b$a;->y:I

    .line 365
    iput v1, p0, Landroid/support/constraint/b$a;->z:I

    .line 366
    iput v1, p0, Landroid/support/constraint/b$a;->A:I

    .line 367
    iput v1, p0, Landroid/support/constraint/b$a;->B:I

    .line 368
    iput v1, p0, Landroid/support/constraint/b$a;->C:I

    .line 369
    iput v1, p0, Landroid/support/constraint/b$a;->D:I

    .line 370
    iput v1, p0, Landroid/support/constraint/b$a;->E:I

    .line 371
    iput v1, p0, Landroid/support/constraint/b$a;->F:I

    .line 372
    iput v3, p0, Landroid/support/constraint/b$a;->G:I

    .line 373
    iput v1, p0, Landroid/support/constraint/b$a;->H:I

    .line 374
    iput v1, p0, Landroid/support/constraint/b$a;->I:I

    .line 375
    iput v1, p0, Landroid/support/constraint/b$a;->J:I

    .line 376
    iput v1, p0, Landroid/support/constraint/b$a;->K:I

    .line 377
    iput v1, p0, Landroid/support/constraint/b$a;->L:I

    .line 378
    iput v1, p0, Landroid/support/constraint/b$a;->M:I

    .line 379
    iput v2, p0, Landroid/support/constraint/b$a;->N:F

    .line 380
    iput v2, p0, Landroid/support/constraint/b$a;->O:F

    .line 381
    iput v3, p0, Landroid/support/constraint/b$a;->P:I

    .line 382
    iput v3, p0, Landroid/support/constraint/b$a;->Q:I

    .line 383
    iput v4, p0, Landroid/support/constraint/b$a;->R:F

    .line 384
    iput-boolean v3, p0, Landroid/support/constraint/b$a;->S:Z

    .line 385
    iput v2, p0, Landroid/support/constraint/b$a;->T:F

    .line 386
    iput v2, p0, Landroid/support/constraint/b$a;->U:F

    .line 387
    iput v2, p0, Landroid/support/constraint/b$a;->V:F

    .line 388
    iput v2, p0, Landroid/support/constraint/b$a;->W:F

    .line 389
    iput v4, p0, Landroid/support/constraint/b$a;->X:F

    .line 390
    iput v4, p0, Landroid/support/constraint/b$a;->Y:F

    .line 391
    iput v2, p0, Landroid/support/constraint/b$a;->Z:F

    .line 392
    iput v2, p0, Landroid/support/constraint/b$a;->aa:F

    .line 393
    iput v2, p0, Landroid/support/constraint/b$a;->ab:F

    .line 394
    iput v2, p0, Landroid/support/constraint/b$a;->ac:F

    .line 395
    iput v2, p0, Landroid/support/constraint/b$a;->ad:F

    .line 396
    iput v1, p0, Landroid/support/constraint/b$a;->ae:I

    .line 397
    iput v1, p0, Landroid/support/constraint/b$a;->af:I

    .line 398
    iput v1, p0, Landroid/support/constraint/b$a;->ag:I

    .line 399
    iput v1, p0, Landroid/support/constraint/b$a;->ah:I

    .line 400
    iput v1, p0, Landroid/support/constraint/b$a;->ai:I

    .line 401
    iput v1, p0, Landroid/support/constraint/b$a;->aj:I

    .line 402
    iput v1, p0, Landroid/support/constraint/b$a;->ak:I

    .line 403
    iput v1, p0, Landroid/support/constraint/b$a;->al:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/constraint/b$1;)V
    .locals 0

    .prologue
    .line 333
    invoke-direct {p0}, Landroid/support/constraint/b$a;-><init>()V

    return-void
.end method

.method private a(ILandroid/support/constraint/ConstraintLayout$a;)V
    .locals 2

    .prologue
    .line 510
    iput p1, p0, Landroid/support/constraint/b$a;->d:I

    .line 511
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->d:I

    iput v0, p0, Landroid/support/constraint/b$a;->h:I

    .line 512
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->e:I

    iput v0, p0, Landroid/support/constraint/b$a;->i:I

    .line 513
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->f:I

    iput v0, p0, Landroid/support/constraint/b$a;->j:I

    .line 514
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->g:I

    iput v0, p0, Landroid/support/constraint/b$a;->k:I

    .line 515
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->h:I

    iput v0, p0, Landroid/support/constraint/b$a;->l:I

    .line 516
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->i:I

    iput v0, p0, Landroid/support/constraint/b$a;->m:I

    .line 517
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->j:I

    iput v0, p0, Landroid/support/constraint/b$a;->n:I

    .line 518
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->k:I

    iput v0, p0, Landroid/support/constraint/b$a;->o:I

    .line 519
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->l:I

    iput v0, p0, Landroid/support/constraint/b$a;->p:I

    .line 520
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->m:I

    iput v0, p0, Landroid/support/constraint/b$a;->q:I

    .line 521
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->n:I

    iput v0, p0, Landroid/support/constraint/b$a;->r:I

    .line 522
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->o:I

    iput v0, p0, Landroid/support/constraint/b$a;->s:I

    .line 523
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->p:I

    iput v0, p0, Landroid/support/constraint/b$a;->t:I

    .line 525
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->w:F

    iput v0, p0, Landroid/support/constraint/b$a;->u:F

    .line 526
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->x:F

    iput v0, p0, Landroid/support/constraint/b$a;->v:F

    .line 527
    iget-object v0, p2, Landroid/support/constraint/ConstraintLayout$a;->y:Ljava/lang/String;

    iput-object v0, p0, Landroid/support/constraint/b$a;->w:Ljava/lang/String;

    .line 528
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->N:I

    iput v0, p0, Landroid/support/constraint/b$a;->x:I

    .line 529
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->O:I

    iput v0, p0, Landroid/support/constraint/b$a;->y:I

    .line 530
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->P:I

    iput v0, p0, Landroid/support/constraint/b$a;->z:I

    .line 531
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->c:F

    iput v0, p0, Landroid/support/constraint/b$a;->g:F

    .line 532
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->a:I

    iput v0, p0, Landroid/support/constraint/b$a;->e:I

    .line 533
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->b:I

    iput v0, p0, Landroid/support/constraint/b$a;->f:I

    .line 534
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->width:I

    iput v0, p0, Landroid/support/constraint/b$a;->b:I

    .line 535
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->height:I

    iput v0, p0, Landroid/support/constraint/b$a;->c:I

    .line 536
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->leftMargin:I

    iput v0, p0, Landroid/support/constraint/b$a;->A:I

    .line 537
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->rightMargin:I

    iput v0, p0, Landroid/support/constraint/b$a;->B:I

    .line 538
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->topMargin:I

    iput v0, p0, Landroid/support/constraint/b$a;->C:I

    .line 539
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->bottomMargin:I

    iput v0, p0, Landroid/support/constraint/b$a;->D:I

    .line 540
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->C:F

    iput v0, p0, Landroid/support/constraint/b$a;->N:F

    .line 541
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->B:F

    iput v0, p0, Landroid/support/constraint/b$a;->O:F

    .line 542
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->E:I

    iput v0, p0, Landroid/support/constraint/b$a;->Q:I

    .line 543
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->D:I

    iput v0, p0, Landroid/support/constraint/b$a;->P:I

    .line 544
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->F:I

    iput v0, p0, Landroid/support/constraint/b$a;->ae:I

    .line 545
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->G:I

    iput v0, p0, Landroid/support/constraint/b$a;->af:I

    .line 546
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->J:I

    iput v0, p0, Landroid/support/constraint/b$a;->ag:I

    .line 547
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->K:I

    iput v0, p0, Landroid/support/constraint/b$a;->ah:I

    .line 548
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->H:I

    iput v0, p0, Landroid/support/constraint/b$a;->ai:I

    .line 549
    iget v0, p2, Landroid/support/constraint/ConstraintLayout$a;->I:I

    iput v0, p0, Landroid/support/constraint/b$a;->aj:I

    .line 551
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 552
    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 553
    invoke-virtual {p2}, Landroid/support/constraint/ConstraintLayout$a;->getMarginEnd()I

    move-result v0

    iput v0, p0, Landroid/support/constraint/b$a;->E:I

    .line 554
    invoke-virtual {p2}, Landroid/support/constraint/ConstraintLayout$a;->getMarginStart()I

    move-result v0

    iput v0, p0, Landroid/support/constraint/b$a;->F:I

    .line 556
    :cond_0
    return-void
.end method

.method private a(ILandroid/support/constraint/c$a;)V
    .locals 1

    .prologue
    .line 493
    invoke-direct {p0, p1, p2}, Landroid/support/constraint/b$a;->a(ILandroid/support/constraint/ConstraintLayout$a;)V

    .line 494
    iget v0, p2, Landroid/support/constraint/c$a;->af:F

    iput v0, p0, Landroid/support/constraint/b$a;->R:F

    .line 495
    iget v0, p2, Landroid/support/constraint/c$a;->ai:F

    iput v0, p0, Landroid/support/constraint/b$a;->U:F

    .line 496
    iget v0, p2, Landroid/support/constraint/c$a;->aj:F

    iput v0, p0, Landroid/support/constraint/b$a;->V:F

    .line 497
    iget v0, p2, Landroid/support/constraint/c$a;->ak:F

    iput v0, p0, Landroid/support/constraint/b$a;->W:F

    .line 498
    iget v0, p2, Landroid/support/constraint/c$a;->al:F

    iput v0, p0, Landroid/support/constraint/b$a;->X:F

    .line 499
    iget v0, p2, Landroid/support/constraint/c$a;->am:F

    iput v0, p0, Landroid/support/constraint/b$a;->Y:F

    .line 500
    iget v0, p2, Landroid/support/constraint/c$a;->an:F

    iput v0, p0, Landroid/support/constraint/b$a;->Z:F

    .line 501
    iget v0, p2, Landroid/support/constraint/c$a;->ao:F

    iput v0, p0, Landroid/support/constraint/b$a;->aa:F

    .line 502
    iget v0, p2, Landroid/support/constraint/c$a;->ap:F

    iput v0, p0, Landroid/support/constraint/b$a;->ab:F

    .line 503
    iget v0, p2, Landroid/support/constraint/c$a;->aq:F

    iput v0, p0, Landroid/support/constraint/b$a;->ac:F

    .line 504
    iget v0, p2, Landroid/support/constraint/c$a;->ar:F

    iput v0, p0, Landroid/support/constraint/b$a;->ad:F

    .line 505
    iget v0, p2, Landroid/support/constraint/c$a;->ah:F

    iput v0, p0, Landroid/support/constraint/b$a;->T:F

    .line 506
    iget-boolean v0, p2, Landroid/support/constraint/c$a;->ag:Z

    iput-boolean v0, p0, Landroid/support/constraint/b$a;->S:Z

    .line 507
    return-void
.end method

.method private a(Landroid/support/constraint/a;ILandroid/support/constraint/c$a;)V
    .locals 1

    .prologue
    .line 483
    invoke-direct {p0, p2, p3}, Landroid/support/constraint/b$a;->a(ILandroid/support/constraint/c$a;)V

    .line 484
    instance-of v0, p1, Landroid/support/constraint/Barrier;

    if-eqz v0, :cond_0

    .line 485
    const/4 v0, 0x1

    iput v0, p0, Landroid/support/constraint/b$a;->al:I

    .line 486
    check-cast p1, Landroid/support/constraint/Barrier;

    .line 487
    invoke-virtual {p1}, Landroid/support/constraint/Barrier;->getType()I

    move-result v0

    iput v0, p0, Landroid/support/constraint/b$a;->ak:I

    .line 488
    invoke-virtual {p1}, Landroid/support/constraint/Barrier;->getReferencedIds()[I

    move-result-object v0

    iput-object v0, p0, Landroid/support/constraint/b$a;->am:[I

    .line 490
    :cond_0
    return-void
.end method

.method static synthetic a(Landroid/support/constraint/b$a;ILandroid/support/constraint/ConstraintLayout$a;)V
    .locals 0

    .prologue
    .line 333
    invoke-direct {p0, p1, p2}, Landroid/support/constraint/b$a;->a(ILandroid/support/constraint/ConstraintLayout$a;)V

    return-void
.end method

.method static synthetic a(Landroid/support/constraint/b$a;ILandroid/support/constraint/c$a;)V
    .locals 0

    .prologue
    .line 333
    invoke-direct {p0, p1, p2}, Landroid/support/constraint/b$a;->a(ILandroid/support/constraint/c$a;)V

    return-void
.end method

.method static synthetic a(Landroid/support/constraint/b$a;Landroid/support/constraint/a;ILandroid/support/constraint/c$a;)V
    .locals 0

    .prologue
    .line 333
    invoke-direct {p0, p1, p2, p3}, Landroid/support/constraint/b$a;->a(Landroid/support/constraint/a;ILandroid/support/constraint/c$a;)V

    return-void
.end method


# virtual methods
.method public a()Landroid/support/constraint/b$a;
    .locals 3

    .prologue
    .line 407
    new-instance v0, Landroid/support/constraint/b$a;

    invoke-direct {v0}, Landroid/support/constraint/b$a;-><init>()V

    .line 408
    iget-boolean v1, p0, Landroid/support/constraint/b$a;->a:Z

    iput-boolean v1, v0, Landroid/support/constraint/b$a;->a:Z

    .line 409
    iget v1, p0, Landroid/support/constraint/b$a;->b:I

    iput v1, v0, Landroid/support/constraint/b$a;->b:I

    .line 410
    iget v1, p0, Landroid/support/constraint/b$a;->c:I

    iput v1, v0, Landroid/support/constraint/b$a;->c:I

    .line 411
    iget v1, p0, Landroid/support/constraint/b$a;->e:I

    iput v1, v0, Landroid/support/constraint/b$a;->e:I

    .line 412
    iget v1, p0, Landroid/support/constraint/b$a;->f:I

    iput v1, v0, Landroid/support/constraint/b$a;->f:I

    .line 413
    iget v1, p0, Landroid/support/constraint/b$a;->g:F

    iput v1, v0, Landroid/support/constraint/b$a;->g:F

    .line 414
    iget v1, p0, Landroid/support/constraint/b$a;->h:I

    iput v1, v0, Landroid/support/constraint/b$a;->h:I

    .line 415
    iget v1, p0, Landroid/support/constraint/b$a;->i:I

    iput v1, v0, Landroid/support/constraint/b$a;->i:I

    .line 416
    iget v1, p0, Landroid/support/constraint/b$a;->j:I

    iput v1, v0, Landroid/support/constraint/b$a;->j:I

    .line 417
    iget v1, p0, Landroid/support/constraint/b$a;->k:I

    iput v1, v0, Landroid/support/constraint/b$a;->k:I

    .line 418
    iget v1, p0, Landroid/support/constraint/b$a;->l:I

    iput v1, v0, Landroid/support/constraint/b$a;->l:I

    .line 419
    iget v1, p0, Landroid/support/constraint/b$a;->m:I

    iput v1, v0, Landroid/support/constraint/b$a;->m:I

    .line 420
    iget v1, p0, Landroid/support/constraint/b$a;->n:I

    iput v1, v0, Landroid/support/constraint/b$a;->n:I

    .line 421
    iget v1, p0, Landroid/support/constraint/b$a;->o:I

    iput v1, v0, Landroid/support/constraint/b$a;->o:I

    .line 422
    iget v1, p0, Landroid/support/constraint/b$a;->p:I

    iput v1, v0, Landroid/support/constraint/b$a;->p:I

    .line 423
    iget v1, p0, Landroid/support/constraint/b$a;->q:I

    iput v1, v0, Landroid/support/constraint/b$a;->q:I

    .line 424
    iget v1, p0, Landroid/support/constraint/b$a;->r:I

    iput v1, v0, Landroid/support/constraint/b$a;->r:I

    .line 425
    iget v1, p0, Landroid/support/constraint/b$a;->s:I

    iput v1, v0, Landroid/support/constraint/b$a;->s:I

    .line 426
    iget v1, p0, Landroid/support/constraint/b$a;->t:I

    iput v1, v0, Landroid/support/constraint/b$a;->t:I

    .line 427
    iget v1, p0, Landroid/support/constraint/b$a;->u:F

    iput v1, v0, Landroid/support/constraint/b$a;->u:F

    .line 428
    iget v1, p0, Landroid/support/constraint/b$a;->v:F

    iput v1, v0, Landroid/support/constraint/b$a;->v:F

    .line 429
    iget-object v1, p0, Landroid/support/constraint/b$a;->w:Ljava/lang/String;

    iput-object v1, v0, Landroid/support/constraint/b$a;->w:Ljava/lang/String;

    .line 430
    iget v1, p0, Landroid/support/constraint/b$a;->x:I

    iput v1, v0, Landroid/support/constraint/b$a;->x:I

    .line 431
    iget v1, p0, Landroid/support/constraint/b$a;->y:I

    iput v1, v0, Landroid/support/constraint/b$a;->y:I

    .line 432
    iget v1, p0, Landroid/support/constraint/b$a;->u:F

    iput v1, v0, Landroid/support/constraint/b$a;->u:F

    .line 433
    iget v1, p0, Landroid/support/constraint/b$a;->u:F

    iput v1, v0, Landroid/support/constraint/b$a;->u:F

    .line 434
    iget v1, p0, Landroid/support/constraint/b$a;->u:F

    iput v1, v0, Landroid/support/constraint/b$a;->u:F

    .line 435
    iget v1, p0, Landroid/support/constraint/b$a;->u:F

    iput v1, v0, Landroid/support/constraint/b$a;->u:F

    .line 436
    iget v1, p0, Landroid/support/constraint/b$a;->u:F

    iput v1, v0, Landroid/support/constraint/b$a;->u:F

    .line 437
    iget v1, p0, Landroid/support/constraint/b$a;->z:I

    iput v1, v0, Landroid/support/constraint/b$a;->z:I

    .line 438
    iget v1, p0, Landroid/support/constraint/b$a;->A:I

    iput v1, v0, Landroid/support/constraint/b$a;->A:I

    .line 439
    iget v1, p0, Landroid/support/constraint/b$a;->B:I

    iput v1, v0, Landroid/support/constraint/b$a;->B:I

    .line 440
    iget v1, p0, Landroid/support/constraint/b$a;->C:I

    iput v1, v0, Landroid/support/constraint/b$a;->C:I

    .line 441
    iget v1, p0, Landroid/support/constraint/b$a;->D:I

    iput v1, v0, Landroid/support/constraint/b$a;->D:I

    .line 442
    iget v1, p0, Landroid/support/constraint/b$a;->E:I

    iput v1, v0, Landroid/support/constraint/b$a;->E:I

    .line 443
    iget v1, p0, Landroid/support/constraint/b$a;->F:I

    iput v1, v0, Landroid/support/constraint/b$a;->F:I

    .line 444
    iget v1, p0, Landroid/support/constraint/b$a;->G:I

    iput v1, v0, Landroid/support/constraint/b$a;->G:I

    .line 445
    iget v1, p0, Landroid/support/constraint/b$a;->H:I

    iput v1, v0, Landroid/support/constraint/b$a;->H:I

    .line 446
    iget v1, p0, Landroid/support/constraint/b$a;->I:I

    iput v1, v0, Landroid/support/constraint/b$a;->I:I

    .line 447
    iget v1, p0, Landroid/support/constraint/b$a;->J:I

    iput v1, v0, Landroid/support/constraint/b$a;->J:I

    .line 448
    iget v1, p0, Landroid/support/constraint/b$a;->K:I

    iput v1, v0, Landroid/support/constraint/b$a;->K:I

    .line 449
    iget v1, p0, Landroid/support/constraint/b$a;->L:I

    iput v1, v0, Landroid/support/constraint/b$a;->L:I

    .line 450
    iget v1, p0, Landroid/support/constraint/b$a;->M:I

    iput v1, v0, Landroid/support/constraint/b$a;->M:I

    .line 451
    iget v1, p0, Landroid/support/constraint/b$a;->N:F

    iput v1, v0, Landroid/support/constraint/b$a;->N:F

    .line 452
    iget v1, p0, Landroid/support/constraint/b$a;->O:F

    iput v1, v0, Landroid/support/constraint/b$a;->O:F

    .line 453
    iget v1, p0, Landroid/support/constraint/b$a;->P:I

    iput v1, v0, Landroid/support/constraint/b$a;->P:I

    .line 454
    iget v1, p0, Landroid/support/constraint/b$a;->Q:I

    iput v1, v0, Landroid/support/constraint/b$a;->Q:I

    .line 455
    iget v1, p0, Landroid/support/constraint/b$a;->R:F

    iput v1, v0, Landroid/support/constraint/b$a;->R:F

    .line 456
    iget-boolean v1, p0, Landroid/support/constraint/b$a;->S:Z

    iput-boolean v1, v0, Landroid/support/constraint/b$a;->S:Z

    .line 457
    iget v1, p0, Landroid/support/constraint/b$a;->T:F

    iput v1, v0, Landroid/support/constraint/b$a;->T:F

    .line 458
    iget v1, p0, Landroid/support/constraint/b$a;->U:F

    iput v1, v0, Landroid/support/constraint/b$a;->U:F

    .line 459
    iget v1, p0, Landroid/support/constraint/b$a;->V:F

    iput v1, v0, Landroid/support/constraint/b$a;->V:F

    .line 460
    iget v1, p0, Landroid/support/constraint/b$a;->W:F

    iput v1, v0, Landroid/support/constraint/b$a;->W:F

    .line 461
    iget v1, p0, Landroid/support/constraint/b$a;->X:F

    iput v1, v0, Landroid/support/constraint/b$a;->X:F

    .line 462
    iget v1, p0, Landroid/support/constraint/b$a;->Y:F

    iput v1, v0, Landroid/support/constraint/b$a;->Y:F

    .line 463
    iget v1, p0, Landroid/support/constraint/b$a;->Z:F

    iput v1, v0, Landroid/support/constraint/b$a;->Z:F

    .line 464
    iget v1, p0, Landroid/support/constraint/b$a;->aa:F

    iput v1, v0, Landroid/support/constraint/b$a;->aa:F

    .line 465
    iget v1, p0, Landroid/support/constraint/b$a;->ab:F

    iput v1, v0, Landroid/support/constraint/b$a;->ab:F

    .line 466
    iget v1, p0, Landroid/support/constraint/b$a;->ac:F

    iput v1, v0, Landroid/support/constraint/b$a;->ac:F

    .line 467
    iget v1, p0, Landroid/support/constraint/b$a;->ad:F

    iput v1, v0, Landroid/support/constraint/b$a;->ad:F

    .line 468
    iget v1, p0, Landroid/support/constraint/b$a;->ae:I

    iput v1, v0, Landroid/support/constraint/b$a;->ae:I

    .line 469
    iget v1, p0, Landroid/support/constraint/b$a;->af:I

    iput v1, v0, Landroid/support/constraint/b$a;->af:I

    .line 470
    iget v1, p0, Landroid/support/constraint/b$a;->ag:I

    iput v1, v0, Landroid/support/constraint/b$a;->ag:I

    .line 471
    iget v1, p0, Landroid/support/constraint/b$a;->ah:I

    iput v1, v0, Landroid/support/constraint/b$a;->ah:I

    .line 472
    iget v1, p0, Landroid/support/constraint/b$a;->ai:I

    iput v1, v0, Landroid/support/constraint/b$a;->ai:I

    .line 473
    iget v1, p0, Landroid/support/constraint/b$a;->aj:I

    iput v1, v0, Landroid/support/constraint/b$a;->aj:I

    .line 474
    iget v1, p0, Landroid/support/constraint/b$a;->ak:I

    iput v1, v0, Landroid/support/constraint/b$a;->ak:I

    .line 475
    iget v1, p0, Landroid/support/constraint/b$a;->al:I

    iput v1, v0, Landroid/support/constraint/b$a;->al:I

    .line 476
    iget-object v1, p0, Landroid/support/constraint/b$a;->am:[I

    if-eqz v1, :cond_0

    .line 477
    iget-object v1, p0, Landroid/support/constraint/b$a;->am:[I

    iget-object v2, p0, Landroid/support/constraint/b$a;->am:[I

    array-length v2, v2

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v1

    iput-object v1, v0, Landroid/support/constraint/b$a;->am:[I

    .line 479
    :cond_0
    return-object v0
.end method

.method public a(Landroid/support/constraint/ConstraintLayout$a;)V
    .locals 2

    .prologue
    .line 559
    iget v0, p0, Landroid/support/constraint/b$a;->h:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->d:I

    .line 560
    iget v0, p0, Landroid/support/constraint/b$a;->i:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->e:I

    .line 561
    iget v0, p0, Landroid/support/constraint/b$a;->j:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->f:I

    .line 562
    iget v0, p0, Landroid/support/constraint/b$a;->k:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->g:I

    .line 564
    iget v0, p0, Landroid/support/constraint/b$a;->l:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->h:I

    .line 565
    iget v0, p0, Landroid/support/constraint/b$a;->m:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->i:I

    .line 566
    iget v0, p0, Landroid/support/constraint/b$a;->n:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->j:I

    .line 567
    iget v0, p0, Landroid/support/constraint/b$a;->o:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->k:I

    .line 569
    iget v0, p0, Landroid/support/constraint/b$a;->p:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->l:I

    .line 571
    iget v0, p0, Landroid/support/constraint/b$a;->q:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->m:I

    .line 572
    iget v0, p0, Landroid/support/constraint/b$a;->r:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->n:I

    .line 573
    iget v0, p0, Landroid/support/constraint/b$a;->s:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->o:I

    .line 574
    iget v0, p0, Landroid/support/constraint/b$a;->t:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->p:I

    .line 576
    iget v0, p0, Landroid/support/constraint/b$a;->A:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->leftMargin:I

    .line 577
    iget v0, p0, Landroid/support/constraint/b$a;->B:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->rightMargin:I

    .line 578
    iget v0, p0, Landroid/support/constraint/b$a;->C:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->topMargin:I

    .line 579
    iget v0, p0, Landroid/support/constraint/b$a;->D:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->bottomMargin:I

    .line 580
    iget v0, p0, Landroid/support/constraint/b$a;->M:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->u:I

    .line 581
    iget v0, p0, Landroid/support/constraint/b$a;->L:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->v:I

    .line 583
    iget v0, p0, Landroid/support/constraint/b$a;->u:F

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->w:F

    .line 584
    iget v0, p0, Landroid/support/constraint/b$a;->v:F

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->x:F

    .line 586
    iget-object v0, p0, Landroid/support/constraint/b$a;->w:Ljava/lang/String;

    iput-object v0, p1, Landroid/support/constraint/ConstraintLayout$a;->y:Ljava/lang/String;

    .line 587
    iget v0, p0, Landroid/support/constraint/b$a;->x:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->N:I

    .line 588
    iget v0, p0, Landroid/support/constraint/b$a;->y:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->O:I

    .line 589
    iget v0, p0, Landroid/support/constraint/b$a;->N:F

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->C:F

    .line 590
    iget v0, p0, Landroid/support/constraint/b$a;->O:F

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->B:F

    .line 591
    iget v0, p0, Landroid/support/constraint/b$a;->Q:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->E:I

    .line 592
    iget v0, p0, Landroid/support/constraint/b$a;->P:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->D:I

    .line 593
    iget v0, p0, Landroid/support/constraint/b$a;->ae:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->F:I

    .line 594
    iget v0, p0, Landroid/support/constraint/b$a;->af:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->G:I

    .line 595
    iget v0, p0, Landroid/support/constraint/b$a;->ag:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->J:I

    .line 596
    iget v0, p0, Landroid/support/constraint/b$a;->ah:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->K:I

    .line 597
    iget v0, p0, Landroid/support/constraint/b$a;->ai:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->H:I

    .line 598
    iget v0, p0, Landroid/support/constraint/b$a;->aj:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->I:I

    .line 599
    iget v0, p0, Landroid/support/constraint/b$a;->z:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->P:I

    .line 600
    iget v0, p0, Landroid/support/constraint/b$a;->g:F

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->c:F

    .line 601
    iget v0, p0, Landroid/support/constraint/b$a;->e:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->a:I

    .line 602
    iget v0, p0, Landroid/support/constraint/b$a;->f:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->b:I

    .line 603
    iget v0, p0, Landroid/support/constraint/b$a;->b:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->width:I

    .line 604
    iget v0, p0, Landroid/support/constraint/b$a;->c:I

    iput v0, p1, Landroid/support/constraint/ConstraintLayout$a;->height:I

    .line 605
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 606
    iget v0, p0, Landroid/support/constraint/b$a;->F:I

    invoke-virtual {p1, v0}, Landroid/support/constraint/ConstraintLayout$a;->setMarginStart(I)V

    .line 607
    iget v0, p0, Landroid/support/constraint/b$a;->E:I

    invoke-virtual {p1, v0}, Landroid/support/constraint/ConstraintLayout$a;->setMarginEnd(I)V

    .line 610
    :cond_0
    invoke-virtual {p1}, Landroid/support/constraint/ConstraintLayout$a;->a()V

    .line 611
    return-void
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 333
    invoke-virtual {p0}, Landroid/support/constraint/b$a;->a()Landroid/support/constraint/b$a;

    move-result-object v0

    return-object v0
.end method
