.class public Landroid/support/constraint/b;
.super Ljava/lang/Object;
.source "ConstraintSet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/constraint/b$a;
    }
.end annotation


# static fields
.field private static final a:[I

.field private static c:Landroid/util/SparseIntArray;


# instance fields
.field private b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/support/constraint/b$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/16 v3, 0x3d

    .line 193
    new-array v0, v4, [I

    fill-array-data v0, :array_0

    sput-object v0, Landroid/support/constraint/b;->a:[I

    .line 198
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    sput-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    .line 263
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintLeft_toLeftOf:I

    const/16 v2, 0x19

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 264
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintLeft_toRightOf:I

    const/16 v2, 0x1a

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 265
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintRight_toLeftOf:I

    const/16 v2, 0x1d

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 266
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintRight_toRightOf:I

    const/16 v2, 0x1e

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 267
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintTop_toTopOf:I

    const/16 v2, 0x24

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 268
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintTop_toBottomOf:I

    const/16 v2, 0x23

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 269
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintBottom_toTopOf:I

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 270
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintBottom_toBottomOf:I

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseIntArray;->append(II)V

    .line 271
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintBaseline_toBaselineOf:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 273
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_editor_absoluteX:I

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 274
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_editor_absoluteY:I

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 275
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintGuide_begin:I

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 276
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintGuide_end:I

    const/16 v2, 0x12

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 277
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintGuide_percent:I

    const/16 v2, 0x13

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 278
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_android_orientation:I

    const/16 v2, 0x1b

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 279
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintStart_toEndOf:I

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 280
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintStart_toStartOf:I

    const/16 v2, 0x21

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 281
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintEnd_toStartOf:I

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 282
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintEnd_toEndOf:I

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 283
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_goneMarginLeft:I

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 284
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_goneMarginTop:I

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 285
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_goneMarginRight:I

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 286
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_goneMarginBottom:I

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 287
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_goneMarginStart:I

    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 288
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_goneMarginEnd:I

    const/16 v2, 0xc

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 289
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintVertical_weight:I

    const/16 v2, 0x28

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 290
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintHorizontal_weight:I

    const/16 v2, 0x27

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 291
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintHorizontal_chainStyle:I

    const/16 v2, 0x29

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 292
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintVertical_chainStyle:I

    const/16 v2, 0x2a

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 294
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintHorizontal_bias:I

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 295
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintVertical_bias:I

    const/16 v2, 0x25

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 296
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintDimensionRatio:I

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 297
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintLeft_creator:I

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->append(II)V

    .line 298
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintTop_creator:I

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->append(II)V

    .line 299
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintRight_creator:I

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->append(II)V

    .line 300
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintBottom_creator:I

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->append(II)V

    .line 301
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintBaseline_creator:I

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->append(II)V

    .line 302
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_android_layout_marginLeft:I

    const/16 v2, 0x18

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 303
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_android_layout_marginRight:I

    const/16 v2, 0x1c

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 304
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_android_layout_marginStart:I

    const/16 v2, 0x1f

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 305
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_android_layout_marginEnd:I

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 306
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_android_layout_marginTop:I

    const/16 v2, 0x22

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 307
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_android_layout_marginBottom:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 308
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_android_layout_width:I

    const/16 v2, 0x17

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 309
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_android_layout_height:I

    const/16 v2, 0x15

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 310
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_android_visibility:I

    const/16 v2, 0x16

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 311
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_android_alpha:I

    const/16 v2, 0x2b

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 312
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_android_elevation:I

    const/16 v2, 0x2c

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 313
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_android_rotationX:I

    const/16 v2, 0x2d

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 314
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_android_rotationY:I

    const/16 v2, 0x2e

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 315
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_android_rotation:I

    const/16 v2, 0x3c

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 316
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_android_scaleX:I

    const/16 v2, 0x2f

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 317
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_android_scaleY:I

    const/16 v2, 0x30

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 318
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_android_transformPivotX:I

    const/16 v2, 0x31

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 319
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_android_transformPivotY:I

    const/16 v2, 0x32

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 320
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_android_translationX:I

    const/16 v2, 0x33

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 321
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_android_translationY:I

    const/16 v2, 0x34

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 322
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_android_translationZ:I

    const/16 v2, 0x35

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 323
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintWidth_default:I

    const/16 v2, 0x36

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 324
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintHeight_default:I

    const/16 v2, 0x37

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 325
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintWidth_max:I

    const/16 v2, 0x38

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 326
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintHeight_max:I

    const/16 v2, 0x39

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 327
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintWidth_min:I

    const/16 v2, 0x3a

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 328
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_layout_constraintHeight_min:I

    const/16 v2, 0x3b

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 330
    sget-object v0, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    sget v1, Landroid/support/constraint/e$b;->ConstraintSet_android_id:I

    const/16 v2, 0x26

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 331
    return-void

    .line 193
    nop

    :array_0
    .array-data 4
        0x0
        0x4
        0x8
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 196
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Landroid/support/constraint/b;->b:Ljava/util/HashMap;

    return-void
.end method

.method private static a(Landroid/content/res/TypedArray;II)I
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 2015
    invoke-virtual {p0, p1, p2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 2016
    if-ne v0, v1, :cond_0

    .line 2017
    invoke-virtual {p0, p1, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 2019
    :cond_0
    return v0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/support/constraint/b$a;
    .locals 2

    .prologue
    .line 2023
    new-instance v0, Landroid/support/constraint/b$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/support/constraint/b$a;-><init>(Landroid/support/constraint/b$1;)V

    .line 2024
    sget-object v1, Landroid/support/constraint/e$b;->ConstraintSet:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 2025
    invoke-direct {p0, v0, v1}, Landroid/support/constraint/b;->a(Landroid/support/constraint/b$a;Landroid/content/res/TypedArray;)V

    .line 2026
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 2027
    return-object v0
.end method

.method private a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1953
    packed-switch p1, :pswitch_data_0

    .line 1969
    const-string v0, "undefined"

    :goto_0
    return-object v0

    .line 1955
    :pswitch_0
    const-string v0, "left"

    goto :goto_0

    .line 1957
    :pswitch_1
    const-string v0, "right"

    goto :goto_0

    .line 1959
    :pswitch_2
    const-string v0, "top"

    goto :goto_0

    .line 1961
    :pswitch_3
    const-string v0, "bottom"

    goto :goto_0

    .line 1963
    :pswitch_4
    const-string v0, "baseline"

    goto :goto_0

    .line 1965
    :pswitch_5
    const-string v0, "start"

    goto :goto_0

    .line 1967
    :pswitch_6
    const-string v0, "end"

    goto :goto_0

    .line 1953
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private a(Landroid/support/constraint/b$a;Landroid/content/res/TypedArray;)V
    .locals 6

    .prologue
    .line 2031
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v1

    .line 2032
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 2033
    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v2

    .line 2072
    sget-object v3, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 2241
    :pswitch_0
    const-string v3, "ConstraintSet"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown attribute 0x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 2242
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "   "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    invoke-virtual {v5, v2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2241
    invoke-static {v3, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2032
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2074
    :pswitch_1
    iget v3, p1, Landroid/support/constraint/b$a;->h:I

    invoke-static {p2, v2, v3}, Landroid/support/constraint/b;->a(Landroid/content/res/TypedArray;II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->h:I

    goto :goto_1

    .line 2077
    :pswitch_2
    iget v3, p1, Landroid/support/constraint/b$a;->i:I

    invoke-static {p2, v2, v3}, Landroid/support/constraint/b;->a(Landroid/content/res/TypedArray;II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->i:I

    goto :goto_1

    .line 2080
    :pswitch_3
    iget v3, p1, Landroid/support/constraint/b$a;->j:I

    invoke-static {p2, v2, v3}, Landroid/support/constraint/b;->a(Landroid/content/res/TypedArray;II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->j:I

    goto :goto_1

    .line 2083
    :pswitch_4
    iget v3, p1, Landroid/support/constraint/b$a;->k:I

    invoke-static {p2, v2, v3}, Landroid/support/constraint/b;->a(Landroid/content/res/TypedArray;II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->k:I

    goto :goto_1

    .line 2086
    :pswitch_5
    iget v3, p1, Landroid/support/constraint/b$a;->l:I

    invoke-static {p2, v2, v3}, Landroid/support/constraint/b;->a(Landroid/content/res/TypedArray;II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->l:I

    goto :goto_1

    .line 2089
    :pswitch_6
    iget v3, p1, Landroid/support/constraint/b$a;->m:I

    invoke-static {p2, v2, v3}, Landroid/support/constraint/b;->a(Landroid/content/res/TypedArray;II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->m:I

    goto :goto_1

    .line 2092
    :pswitch_7
    iget v3, p1, Landroid/support/constraint/b$a;->n:I

    invoke-static {p2, v2, v3}, Landroid/support/constraint/b;->a(Landroid/content/res/TypedArray;II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->n:I

    goto :goto_1

    .line 2095
    :pswitch_8
    iget v3, p1, Landroid/support/constraint/b$a;->o:I

    invoke-static {p2, v2, v3}, Landroid/support/constraint/b;->a(Landroid/content/res/TypedArray;II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->o:I

    goto :goto_1

    .line 2098
    :pswitch_9
    iget v3, p1, Landroid/support/constraint/b$a;->p:I

    invoke-static {p2, v2, v3}, Landroid/support/constraint/b;->a(Landroid/content/res/TypedArray;II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->p:I

    goto :goto_1

    .line 2101
    :pswitch_a
    iget v3, p1, Landroid/support/constraint/b$a;->x:I

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->x:I

    goto :goto_1

    .line 2104
    :pswitch_b
    iget v3, p1, Landroid/support/constraint/b$a;->y:I

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->y:I

    goto :goto_1

    .line 2107
    :pswitch_c
    iget v3, p1, Landroid/support/constraint/b$a;->e:I

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->e:I

    goto :goto_1

    .line 2110
    :pswitch_d
    iget v3, p1, Landroid/support/constraint/b$a;->f:I

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->f:I

    goto :goto_1

    .line 2113
    :pswitch_e
    iget v3, p1, Landroid/support/constraint/b$a;->g:F

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->g:F

    goto :goto_1

    .line 2116
    :pswitch_f
    iget v3, p1, Landroid/support/constraint/b$a;->z:I

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->z:I

    goto/16 :goto_1

    .line 2119
    :pswitch_10
    iget v3, p1, Landroid/support/constraint/b$a;->q:I

    invoke-static {p2, v2, v3}, Landroid/support/constraint/b;->a(Landroid/content/res/TypedArray;II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->q:I

    goto/16 :goto_1

    .line 2122
    :pswitch_11
    iget v3, p1, Landroid/support/constraint/b$a;->r:I

    invoke-static {p2, v2, v3}, Landroid/support/constraint/b;->a(Landroid/content/res/TypedArray;II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->r:I

    goto/16 :goto_1

    .line 2125
    :pswitch_12
    iget v3, p1, Landroid/support/constraint/b$a;->s:I

    invoke-static {p2, v2, v3}, Landroid/support/constraint/b;->a(Landroid/content/res/TypedArray;II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->s:I

    goto/16 :goto_1

    .line 2128
    :pswitch_13
    iget v3, p1, Landroid/support/constraint/b$a;->t:I

    invoke-static {p2, v2, v3}, Landroid/support/constraint/b;->a(Landroid/content/res/TypedArray;II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->n:I

    goto/16 :goto_1

    .line 2131
    :pswitch_14
    iget v3, p1, Landroid/support/constraint/b$a;->H:I

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->H:I

    goto/16 :goto_1

    .line 2134
    :pswitch_15
    iget v3, p1, Landroid/support/constraint/b$a;->I:I

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->I:I

    goto/16 :goto_1

    .line 2137
    :pswitch_16
    iget v3, p1, Landroid/support/constraint/b$a;->J:I

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->J:I

    goto/16 :goto_1

    .line 2140
    :pswitch_17
    iget v3, p1, Landroid/support/constraint/b$a;->K:I

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->K:I

    goto/16 :goto_1

    .line 2143
    :pswitch_18
    iget v3, p1, Landroid/support/constraint/b$a;->M:I

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->M:I

    goto/16 :goto_1

    .line 2146
    :pswitch_19
    iget v3, p1, Landroid/support/constraint/b$a;->L:I

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->L:I

    goto/16 :goto_1

    .line 2149
    :pswitch_1a
    iget v3, p1, Landroid/support/constraint/b$a;->u:F

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->u:F

    goto/16 :goto_1

    .line 2152
    :pswitch_1b
    iget v3, p1, Landroid/support/constraint/b$a;->v:F

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->v:F

    goto/16 :goto_1

    .line 2155
    :pswitch_1c
    iget v3, p1, Landroid/support/constraint/b$a;->A:I

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->A:I

    goto/16 :goto_1

    .line 2158
    :pswitch_1d
    iget v3, p1, Landroid/support/constraint/b$a;->B:I

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->B:I

    goto/16 :goto_1

    .line 2161
    :pswitch_1e
    iget v3, p1, Landroid/support/constraint/b$a;->F:I

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->F:I

    goto/16 :goto_1

    .line 2164
    :pswitch_1f
    iget v3, p1, Landroid/support/constraint/b$a;->E:I

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->E:I

    goto/16 :goto_1

    .line 2167
    :pswitch_20
    iget v3, p1, Landroid/support/constraint/b$a;->C:I

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->C:I

    goto/16 :goto_1

    .line 2170
    :pswitch_21
    iget v3, p1, Landroid/support/constraint/b$a;->D:I

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->D:I

    goto/16 :goto_1

    .line 2173
    :pswitch_22
    iget v3, p1, Landroid/support/constraint/b$a;->b:I

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->b:I

    goto/16 :goto_1

    .line 2176
    :pswitch_23
    iget v3, p1, Landroid/support/constraint/b$a;->c:I

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->c:I

    goto/16 :goto_1

    .line 2179
    :pswitch_24
    iget v3, p1, Landroid/support/constraint/b$a;->G:I

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->G:I

    .line 2180
    sget-object v2, Landroid/support/constraint/b;->a:[I

    iget v3, p1, Landroid/support/constraint/b$a;->G:I

    aget v2, v2, v3

    iput v2, p1, Landroid/support/constraint/b$a;->G:I

    goto/16 :goto_1

    .line 2183
    :pswitch_25
    iget v3, p1, Landroid/support/constraint/b$a;->R:F

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->R:F

    goto/16 :goto_1

    .line 2186
    :pswitch_26
    const/4 v3, 0x1

    iput-boolean v3, p1, Landroid/support/constraint/b$a;->S:Z

    .line 2187
    iget v3, p1, Landroid/support/constraint/b$a;->T:F

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->T:F

    goto/16 :goto_1

    .line 2190
    :pswitch_27
    iget v3, p1, Landroid/support/constraint/b$a;->U:F

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    iput v3, p1, Landroid/support/constraint/b$a;->U:F

    .line 2192
    :pswitch_28
    iget v3, p1, Landroid/support/constraint/b$a;->V:F

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->V:F

    goto/16 :goto_1

    .line 2195
    :pswitch_29
    iget v3, p1, Landroid/support/constraint/b$a;->W:F

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->W:F

    goto/16 :goto_1

    .line 2198
    :pswitch_2a
    iget v3, p1, Landroid/support/constraint/b$a;->X:F

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->X:F

    goto/16 :goto_1

    .line 2201
    :pswitch_2b
    iget v3, p1, Landroid/support/constraint/b$a;->Y:F

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->Y:F

    goto/16 :goto_1

    .line 2204
    :pswitch_2c
    iget v3, p1, Landroid/support/constraint/b$a;->Z:F

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->Z:F

    goto/16 :goto_1

    .line 2207
    :pswitch_2d
    iget v3, p1, Landroid/support/constraint/b$a;->aa:F

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->aa:F

    goto/16 :goto_1

    .line 2210
    :pswitch_2e
    iget v3, p1, Landroid/support/constraint/b$a;->ab:F

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->ab:F

    goto/16 :goto_1

    .line 2213
    :pswitch_2f
    iget v3, p1, Landroid/support/constraint/b$a;->ac:F

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->ac:F

    goto/16 :goto_1

    .line 2216
    :pswitch_30
    iget v3, p1, Landroid/support/constraint/b$a;->ad:F

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->ad:F

    goto/16 :goto_1

    .line 2219
    :pswitch_31
    iget v3, p1, Landroid/support/constraint/b$a;->N:F

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->N:F

    goto/16 :goto_1

    .line 2222
    :pswitch_32
    iget v3, p1, Landroid/support/constraint/b$a;->O:F

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->O:F

    goto/16 :goto_1

    .line 2225
    :pswitch_33
    iget v3, p1, Landroid/support/constraint/b$a;->Q:I

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->Q:I

    goto/16 :goto_1

    .line 2228
    :pswitch_34
    iget v3, p1, Landroid/support/constraint/b$a;->P:I

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->P:I

    goto/16 :goto_1

    .line 2231
    :pswitch_35
    iget v3, p1, Landroid/support/constraint/b$a;->d:I

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p1, Landroid/support/constraint/b$a;->d:I

    goto/16 :goto_1

    .line 2234
    :pswitch_36
    invoke-virtual {p2, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Landroid/support/constraint/b$a;->w:Ljava/lang/String;

    goto/16 :goto_1

    .line 2237
    :pswitch_37
    const-string v3, "ConstraintSet"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "unused attribute 0x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 2238
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "   "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Landroid/support/constraint/b;->c:Landroid/util/SparseIntArray;

    invoke-virtual {v5, v2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2237
    invoke-static {v3, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 2245
    :cond_0
    return-void

    .line 2072
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_21
        :pswitch_8
        :pswitch_7
        :pswitch_36
        :pswitch_a
        :pswitch_b
        :pswitch_1f
        :pswitch_13
        :pswitch_12
        :pswitch_17
        :pswitch_19
        :pswitch_14
        :pswitch_16
        :pswitch_18
        :pswitch_15
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_1a
        :pswitch_23
        :pswitch_24
        :pswitch_22
        :pswitch_1c
        :pswitch_1
        :pswitch_2
        :pswitch_f
        :pswitch_1d
        :pswitch_3
        :pswitch_4
        :pswitch_1e
        :pswitch_10
        :pswitch_11
        :pswitch_20
        :pswitch_6
        :pswitch_5
        :pswitch_1b
        :pswitch_35
        :pswitch_32
        :pswitch_31
        :pswitch_34
        :pswitch_33
        :pswitch_25
        :pswitch_26
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_27
        :pswitch_37
    .end packed-switch
.end method


# virtual methods
.method public a(IIIII)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, -0x1

    .line 1005
    iget-object v0, p0, Landroid/support/constraint/b;->b:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1006
    iget-object v0, p0, Landroid/support/constraint/b;->b:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Landroid/support/constraint/b$a;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Landroid/support/constraint/b$a;-><init>(Landroid/support/constraint/b$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1008
    :cond_0
    iget-object v0, p0, Landroid/support/constraint/b;->b:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/b$a;

    .line 1009
    packed-switch p2, :pswitch_data_0

    .line 1104
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1105
    invoke-direct {p0, p2}, Landroid/support/constraint/b;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, p4}, Landroid/support/constraint/b;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " unknown"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1011
    :pswitch_0
    if-ne p4, v5, :cond_1

    .line 1012
    iput p3, v0, Landroid/support/constraint/b$a;->h:I

    .line 1013
    iput v4, v0, Landroid/support/constraint/b$a;->i:I

    .line 1021
    :goto_0
    iput p5, v0, Landroid/support/constraint/b$a;->A:I

    .line 1107
    :goto_1
    return-void

    .line 1014
    :cond_1
    if-ne p4, v6, :cond_2

    .line 1015
    iput p3, v0, Landroid/support/constraint/b$a;->i:I

    .line 1016
    iput v4, v0, Landroid/support/constraint/b$a;->h:I

    goto :goto_0

    .line 1019
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Left to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, p4}, Landroid/support/constraint/b;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " undefined"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1024
    :pswitch_1
    if-ne p4, v5, :cond_3

    .line 1025
    iput p3, v0, Landroid/support/constraint/b$a;->j:I

    .line 1026
    iput v4, v0, Landroid/support/constraint/b$a;->k:I

    .line 1035
    :goto_2
    iput p5, v0, Landroid/support/constraint/b$a;->B:I

    goto :goto_1

    .line 1028
    :cond_3
    if-ne p4, v6, :cond_4

    .line 1029
    iput p3, v0, Landroid/support/constraint/b$a;->k:I

    .line 1030
    iput v4, v0, Landroid/support/constraint/b$a;->j:I

    goto :goto_2

    .line 1033
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "right to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, p4}, Landroid/support/constraint/b;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " undefined"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1038
    :pswitch_2
    if-ne p4, v7, :cond_5

    .line 1039
    iput p3, v0, Landroid/support/constraint/b$a;->l:I

    .line 1040
    iput v4, v0, Landroid/support/constraint/b$a;->m:I

    .line 1041
    iput v4, v0, Landroid/support/constraint/b$a;->p:I

    .line 1050
    :goto_3
    iput p5, v0, Landroid/support/constraint/b$a;->C:I

    goto :goto_1

    .line 1042
    :cond_5
    if-ne p4, v8, :cond_6

    .line 1043
    iput p3, v0, Landroid/support/constraint/b$a;->m:I

    .line 1044
    iput v4, v0, Landroid/support/constraint/b$a;->l:I

    .line 1045
    iput v4, v0, Landroid/support/constraint/b$a;->p:I

    goto :goto_3

    .line 1048
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "right to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, p4}, Landroid/support/constraint/b;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " undefined"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1053
    :pswitch_3
    if-ne p4, v8, :cond_7

    .line 1054
    iput p3, v0, Landroid/support/constraint/b$a;->o:I

    .line 1055
    iput v4, v0, Landroid/support/constraint/b$a;->n:I

    .line 1056
    iput v4, v0, Landroid/support/constraint/b$a;->p:I

    .line 1066
    :goto_4
    iput p5, v0, Landroid/support/constraint/b$a;->D:I

    goto/16 :goto_1

    .line 1058
    :cond_7
    if-ne p4, v7, :cond_8

    .line 1059
    iput p3, v0, Landroid/support/constraint/b$a;->n:I

    .line 1060
    iput v4, v0, Landroid/support/constraint/b$a;->o:I

    .line 1061
    iput v4, v0, Landroid/support/constraint/b$a;->p:I

    goto :goto_4

    .line 1064
    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "right to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, p4}, Landroid/support/constraint/b;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " undefined"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1069
    :pswitch_4
    const/4 v1, 0x5

    if-ne p4, v1, :cond_9

    .line 1070
    iput p3, v0, Landroid/support/constraint/b$a;->p:I

    .line 1071
    iput v4, v0, Landroid/support/constraint/b$a;->o:I

    .line 1072
    iput v4, v0, Landroid/support/constraint/b$a;->n:I

    .line 1073
    iput v4, v0, Landroid/support/constraint/b$a;->l:I

    .line 1074
    iput v4, v0, Landroid/support/constraint/b$a;->m:I

    goto/16 :goto_1

    .line 1076
    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "right to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, p4}, Landroid/support/constraint/b;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " undefined"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1080
    :pswitch_5
    const/4 v1, 0x6

    if-ne p4, v1, :cond_a

    .line 1081
    iput p3, v0, Landroid/support/constraint/b$a;->r:I

    .line 1082
    iput v4, v0, Landroid/support/constraint/b$a;->q:I

    .line 1089
    :goto_5
    iput p5, v0, Landroid/support/constraint/b$a;->F:I

    goto/16 :goto_1

    .line 1083
    :cond_a
    const/4 v1, 0x7

    if-ne p4, v1, :cond_b

    .line 1084
    iput p3, v0, Landroid/support/constraint/b$a;->q:I

    .line 1085
    iput v4, v0, Landroid/support/constraint/b$a;->r:I

    goto :goto_5

    .line 1087
    :cond_b
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "right to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, p4}, Landroid/support/constraint/b;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " undefined"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1092
    :pswitch_6
    const/4 v1, 0x7

    if-ne p4, v1, :cond_c

    .line 1093
    iput p3, v0, Landroid/support/constraint/b$a;->t:I

    .line 1094
    iput v4, v0, Landroid/support/constraint/b$a;->s:I

    .line 1101
    :goto_6
    iput p5, v0, Landroid/support/constraint/b$a;->E:I

    goto/16 :goto_1

    .line 1095
    :cond_c
    const/4 v1, 0x6

    if-ne p4, v1, :cond_d

    .line 1096
    iput p3, v0, Landroid/support/constraint/b$a;->s:I

    .line 1097
    iput v4, v0, Landroid/support/constraint/b$a;->t:I

    goto :goto_6

    .line 1099
    :cond_d
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "right to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, p4}, Landroid/support/constraint/b;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " undefined"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1009
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public a(Landroid/content/Context;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1979
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1980
    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v1

    .line 1985
    :try_start_0
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 1986
    :goto_0
    if-eq v0, v4, :cond_0

    .line 1988
    packed-switch v0, :pswitch_data_0

    .line 1987
    :goto_1
    :pswitch_0
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_0

    .line 1990
    :pswitch_1
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 2007
    :catch_0
    move-exception v0

    .line 2008
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 2012
    :cond_0
    :goto_2
    return-void

    .line 1993
    :pswitch_2
    :try_start_1
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1994
    invoke-static {v1}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Landroid/support/constraint/b;->a(Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/support/constraint/b$a;

    move-result-object v2

    .line 1995
    const-string v3, "Guideline"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1996
    const/4 v0, 0x1

    iput-boolean v0, v2, Landroid/support/constraint/b$a;->a:Z

    .line 1998
    :cond_1
    iget-object v0, p0, Landroid/support/constraint/b;->b:Ljava/util/HashMap;

    iget v3, v2, Landroid/support/constraint/b$a;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 2009
    :catch_1
    move-exception v0

    .line 2010
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 1988
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/support/constraint/ConstraintLayout;)V
    .locals 9

    .prologue
    .line 642
    invoke-virtual {p1}, Landroid/support/constraint/ConstraintLayout;->getChildCount()I

    move-result v3

    .line 643
    iget-object v0, p0, Landroid/support/constraint/b;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 644
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_2

    .line 645
    invoke-virtual {p1, v2}, Landroid/support/constraint/ConstraintLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 646
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/ConstraintLayout$a;

    .line 648
    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v5

    .line 649
    iget-object v1, p0, Landroid/support/constraint/b;->b:Ljava/util/HashMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 650
    iget-object v1, p0, Landroid/support/constraint/b;->b:Ljava/util/HashMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    new-instance v7, Landroid/support/constraint/b$a;

    const/4 v8, 0x0

    invoke-direct {v7, v8}, Landroid/support/constraint/b$a;-><init>(Landroid/support/constraint/b$1;)V

    invoke-virtual {v1, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 652
    :cond_0
    iget-object v1, p0, Landroid/support/constraint/b;->b:Ljava/util/HashMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/constraint/b$a;

    .line 653
    invoke-static {v1, v5, v0}, Landroid/support/constraint/b$a;->a(Landroid/support/constraint/b$a;ILandroid/support/constraint/ConstraintLayout$a;)V

    .line 654
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v0

    iput v0, v1, Landroid/support/constraint/b$a;->G:I

    .line 655
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x11

    if-lt v0, v5, :cond_1

    .line 656
    invoke-virtual {v4}, Landroid/view/View;->getAlpha()F

    move-result v0

    iput v0, v1, Landroid/support/constraint/b$a;->R:F

    .line 657
    invoke-virtual {v4}, Landroid/view/View;->getRotation()F

    move-result v0

    iput v0, v1, Landroid/support/constraint/b$a;->U:F

    .line 658
    invoke-virtual {v4}, Landroid/view/View;->getRotationX()F

    move-result v0

    iput v0, v1, Landroid/support/constraint/b$a;->V:F

    .line 659
    invoke-virtual {v4}, Landroid/view/View;->getRotationY()F

    move-result v0

    iput v0, v1, Landroid/support/constraint/b$a;->W:F

    .line 660
    invoke-virtual {v4}, Landroid/view/View;->getScaleX()F

    move-result v0

    iput v0, v1, Landroid/support/constraint/b$a;->X:F

    .line 661
    invoke-virtual {v4}, Landroid/view/View;->getScaleY()F

    move-result v0

    iput v0, v1, Landroid/support/constraint/b$a;->Y:F

    .line 662
    invoke-virtual {v4}, Landroid/view/View;->getPivotX()F

    move-result v0

    iput v0, v1, Landroid/support/constraint/b$a;->Z:F

    .line 663
    invoke-virtual {v4}, Landroid/view/View;->getPivotY()F

    move-result v0

    iput v0, v1, Landroid/support/constraint/b$a;->aa:F

    .line 664
    invoke-virtual {v4}, Landroid/view/View;->getTranslationX()F

    move-result v0

    iput v0, v1, Landroid/support/constraint/b$a;->ab:F

    .line 665
    invoke-virtual {v4}, Landroid/view/View;->getTranslationY()F

    move-result v0

    iput v0, v1, Landroid/support/constraint/b$a;->ac:F

    .line 666
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x15

    if-lt v0, v5, :cond_1

    .line 667
    invoke-virtual {v4}, Landroid/view/View;->getTranslationZ()F

    move-result v0

    iput v0, v1, Landroid/support/constraint/b$a;->ad:F

    .line 668
    iget-boolean v0, v1, Landroid/support/constraint/b$a;->S:Z

    if-eqz v0, :cond_1

    .line 669
    invoke-virtual {v4}, Landroid/view/View;->getElevation()F

    move-result v0

    iput v0, v1, Landroid/support/constraint/b$a;->T:F

    .line 644
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    .line 674
    :cond_2
    return-void
.end method

.method public a(Landroid/support/constraint/c;)V
    .locals 9

    .prologue
    .line 682
    invoke-virtual {p1}, Landroid/support/constraint/c;->getChildCount()I

    move-result v4

    .line 683
    iget-object v0, p0, Landroid/support/constraint/b;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 684
    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-ge v3, v4, :cond_2

    .line 685
    invoke-virtual {p1, v3}, Landroid/support/constraint/c;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 686
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/c$a;

    .line 688
    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v5

    .line 689
    iget-object v1, p0, Landroid/support/constraint/b;->b:Ljava/util/HashMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 690
    iget-object v1, p0, Landroid/support/constraint/b;->b:Ljava/util/HashMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    new-instance v7, Landroid/support/constraint/b$a;

    const/4 v8, 0x0

    invoke-direct {v7, v8}, Landroid/support/constraint/b$a;-><init>(Landroid/support/constraint/b$1;)V

    invoke-virtual {v1, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 692
    :cond_0
    iget-object v1, p0, Landroid/support/constraint/b;->b:Ljava/util/HashMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/constraint/b$a;

    .line 693
    instance-of v6, v2, Landroid/support/constraint/a;

    if-eqz v6, :cond_1

    .line 694
    check-cast v2, Landroid/support/constraint/a;

    .line 695
    invoke-static {v1, v2, v5, v0}, Landroid/support/constraint/b$a;->a(Landroid/support/constraint/b$a;Landroid/support/constraint/a;ILandroid/support/constraint/c$a;)V

    .line 697
    :cond_1
    invoke-static {v1, v5, v0}, Landroid/support/constraint/b$a;->a(Landroid/support/constraint/b$a;ILandroid/support/constraint/c$a;)V

    .line 684
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 699
    :cond_2
    return-void
.end method

.method public b(Landroid/support/constraint/ConstraintLayout;)V
    .locals 1

    .prologue
    .line 707
    invoke-virtual {p0, p1}, Landroid/support/constraint/b;->c(Landroid/support/constraint/ConstraintLayout;)V

    .line 708
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/support/constraint/ConstraintLayout;->setConstraintSet(Landroid/support/constraint/b;)V

    .line 709
    return-void
.end method

.method c(Landroid/support/constraint/ConstraintLayout;)V
    .locals 8

    .prologue
    const/4 v7, -0x1

    .line 715
    invoke-virtual {p1}, Landroid/support/constraint/ConstraintLayout;->getChildCount()I

    move-result v4

    .line 716
    new-instance v5, Ljava/util/HashSet;

    iget-object v0, p0, Landroid/support/constraint/b;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 718
    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-ge v3, v4, :cond_2

    .line 719
    invoke-virtual {p1, v3}, Landroid/support/constraint/ConstraintLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 720
    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v6

    .line 721
    iget-object v0, p0, Landroid/support/constraint/b;->b:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 722
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 723
    iget-object v0, p0, Landroid/support/constraint/b;->b:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/b$a;

    .line 724
    iget v1, v0, Landroid/support/constraint/b$a;->al:I

    if-eq v1, v7, :cond_0

    .line 725
    iget v1, v0, Landroid/support/constraint/b$a;->al:I

    packed-switch v1, :pswitch_data_0

    .line 739
    :cond_0
    :goto_1
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/constraint/ConstraintLayout$a;

    .line 740
    invoke-virtual {v0, v1}, Landroid/support/constraint/b$a;->a(Landroid/support/constraint/ConstraintLayout$a;)V

    .line 741
    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 742
    iget v1, v0, Landroid/support/constraint/b$a;->G:I

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 743
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x11

    if-lt v1, v6, :cond_1

    .line 744
    iget v1, v0, Landroid/support/constraint/b$a;->R:F

    invoke-virtual {v2, v1}, Landroid/view/View;->setAlpha(F)V

    .line 745
    iget v1, v0, Landroid/support/constraint/b$a;->U:F

    invoke-virtual {v2, v1}, Landroid/view/View;->setRotation(F)V

    .line 746
    iget v1, v0, Landroid/support/constraint/b$a;->V:F

    invoke-virtual {v2, v1}, Landroid/view/View;->setRotationX(F)V

    .line 747
    iget v1, v0, Landroid/support/constraint/b$a;->W:F

    invoke-virtual {v2, v1}, Landroid/view/View;->setRotationY(F)V

    .line 748
    iget v1, v0, Landroid/support/constraint/b$a;->X:F

    invoke-virtual {v2, v1}, Landroid/view/View;->setScaleX(F)V

    .line 749
    iget v1, v0, Landroid/support/constraint/b$a;->Y:F

    invoke-virtual {v2, v1}, Landroid/view/View;->setScaleY(F)V

    .line 750
    iget v1, v0, Landroid/support/constraint/b$a;->Z:F

    invoke-virtual {v2, v1}, Landroid/view/View;->setPivotX(F)V

    .line 751
    iget v1, v0, Landroid/support/constraint/b$a;->aa:F

    invoke-virtual {v2, v1}, Landroid/view/View;->setPivotY(F)V

    .line 752
    iget v1, v0, Landroid/support/constraint/b$a;->ab:F

    invoke-virtual {v2, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 753
    iget v1, v0, Landroid/support/constraint/b$a;->ac:F

    invoke-virtual {v2, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 754
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x15

    if-lt v1, v6, :cond_1

    .line 755
    iget v1, v0, Landroid/support/constraint/b$a;->ad:F

    invoke-virtual {v2, v1}, Landroid/view/View;->setTranslationZ(F)V

    .line 756
    iget-boolean v1, v0, Landroid/support/constraint/b$a;->S:Z

    if-eqz v1, :cond_1

    .line 757
    iget v0, v0, Landroid/support/constraint/b$a;->T:F

    invoke-virtual {v2, v0}, Landroid/view/View;->setElevation(F)V

    .line 718
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_0

    :pswitch_0
    move-object v1, v2

    .line 727
    check-cast v1, Landroid/support/constraint/Barrier;

    .line 728
    invoke-virtual {v1, v6}, Landroid/support/constraint/Barrier;->setId(I)V

    .line 729
    iget-object v6, v0, Landroid/support/constraint/b$a;->am:[I

    invoke-virtual {v1, v6}, Landroid/support/constraint/Barrier;->setReferencedIds([I)V

    .line 730
    iget v6, v0, Landroid/support/constraint/b$a;->ak:I

    invoke-virtual {v1, v6}, Landroid/support/constraint/Barrier;->setType(I)V

    .line 732
    invoke-virtual {p1}, Landroid/support/constraint/ConstraintLayout;->b()Landroid/support/constraint/ConstraintLayout$a;

    move-result-object v1

    .line 733
    invoke-virtual {v0, v1}, Landroid/support/constraint/b$a;->a(Landroid/support/constraint/ConstraintLayout$a;)V

    goto :goto_1

    .line 763
    :cond_2
    invoke-virtual {v5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 764
    iget-object v1, p0, Landroid/support/constraint/b;->b:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/constraint/b$a;

    .line 765
    iget v3, v1, Landroid/support/constraint/b$a;->al:I

    if-eq v3, v7, :cond_4

    .line 766
    iget v3, v1, Landroid/support/constraint/b$a;->al:I

    packed-switch v3, :pswitch_data_1

    .line 780
    :cond_4
    :goto_3
    iget-boolean v3, v1, Landroid/support/constraint/b$a;->a:Z

    if-eqz v3, :cond_3

    .line 781
    new-instance v3, Landroid/support/constraint/Guideline;

    invoke-virtual {p1}, Landroid/support/constraint/ConstraintLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/support/constraint/Guideline;-><init>(Landroid/content/Context;)V

    .line 782
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/support/constraint/Guideline;->setId(I)V

    .line 783
    invoke-virtual {p1}, Landroid/support/constraint/ConstraintLayout;->b()Landroid/support/constraint/ConstraintLayout$a;

    move-result-object v0

    .line 784
    invoke-virtual {v1, v0}, Landroid/support/constraint/b$a;->a(Landroid/support/constraint/ConstraintLayout$a;)V

    .line 785
    invoke-virtual {p1, v3, v0}, Landroid/support/constraint/ConstraintLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2

    .line 768
    :pswitch_1
    new-instance v3, Landroid/support/constraint/Barrier;

    invoke-virtual {p1}, Landroid/support/constraint/ConstraintLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/support/constraint/Barrier;-><init>(Landroid/content/Context;)V

    .line 769
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/support/constraint/Barrier;->setId(I)V

    .line 770
    iget-object v4, v1, Landroid/support/constraint/b$a;->am:[I

    invoke-virtual {v3, v4}, Landroid/support/constraint/Barrier;->setReferencedIds([I)V

    .line 771
    iget v4, v1, Landroid/support/constraint/b$a;->ak:I

    invoke-virtual {v3, v4}, Landroid/support/constraint/Barrier;->setType(I)V

    .line 773
    invoke-virtual {p1}, Landroid/support/constraint/ConstraintLayout;->b()Landroid/support/constraint/ConstraintLayout$a;

    move-result-object v4

    .line 774
    invoke-virtual {v1, v4}, Landroid/support/constraint/b$a;->a(Landroid/support/constraint/ConstraintLayout$a;)V

    .line 775
    invoke-virtual {p1, v3, v4}, Landroid/support/constraint/ConstraintLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_3

    .line 788
    :cond_5
    return-void

    .line 725
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 766
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch
.end method
