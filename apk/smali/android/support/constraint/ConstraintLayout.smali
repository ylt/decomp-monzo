.class public Landroid/support/constraint/ConstraintLayout;
.super Landroid/view/ViewGroup;
.source "ConstraintLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/constraint/ConstraintLayout$a;
    }
.end annotation


# instance fields
.field a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field b:Landroid/support/constraint/a/a/d;

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/constraint/a;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/constraint/a/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:Z

.field private j:I

.field private k:Landroid/support/constraint/b;

.field private l:Ljava/lang/String;

.field private m:I

.field private n:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const v3, 0x7fffffff

    const/4 v2, 0x0

    .line 460
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 397
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Landroid/support/constraint/ConstraintLayout;->a:Landroid/util/SparseArray;

    .line 400
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Landroid/support/constraint/ConstraintLayout;->c:Ljava/util/ArrayList;

    .line 405
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Landroid/support/constraint/ConstraintLayout;->d:Ljava/util/ArrayList;

    .line 407
    new-instance v0, Landroid/support/constraint/a/a/d;

    invoke-direct {v0}, Landroid/support/constraint/a/a/d;-><init>()V

    iput-object v0, p0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    .line 409
    iput v2, p0, Landroid/support/constraint/ConstraintLayout;->e:I

    .line 410
    iput v2, p0, Landroid/support/constraint/ConstraintLayout;->f:I

    .line 411
    iput v3, p0, Landroid/support/constraint/ConstraintLayout;->g:I

    .line 412
    iput v3, p0, Landroid/support/constraint/ConstraintLayout;->h:I

    .line 414
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/constraint/ConstraintLayout;->i:Z

    .line 415
    const/4 v0, 0x2

    iput v0, p0, Landroid/support/constraint/ConstraintLayout;->j:I

    .line 416
    iput-object v4, p0, Landroid/support/constraint/ConstraintLayout;->k:Landroid/support/constraint/b;

    .line 419
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/constraint/ConstraintLayout;->m:I

    .line 421
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Landroid/support/constraint/ConstraintLayout;->n:Ljava/util/HashMap;

    .line 461
    invoke-direct {p0, v4}, Landroid/support/constraint/ConstraintLayout;->b(Landroid/util/AttributeSet;)V

    .line 462
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const v3, 0x7fffffff

    const/4 v2, 0x0

    .line 465
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 397
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Landroid/support/constraint/ConstraintLayout;->a:Landroid/util/SparseArray;

    .line 400
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Landroid/support/constraint/ConstraintLayout;->c:Ljava/util/ArrayList;

    .line 405
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Landroid/support/constraint/ConstraintLayout;->d:Ljava/util/ArrayList;

    .line 407
    new-instance v0, Landroid/support/constraint/a/a/d;

    invoke-direct {v0}, Landroid/support/constraint/a/a/d;-><init>()V

    iput-object v0, p0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    .line 409
    iput v2, p0, Landroid/support/constraint/ConstraintLayout;->e:I

    .line 410
    iput v2, p0, Landroid/support/constraint/ConstraintLayout;->f:I

    .line 411
    iput v3, p0, Landroid/support/constraint/ConstraintLayout;->g:I

    .line 412
    iput v3, p0, Landroid/support/constraint/ConstraintLayout;->h:I

    .line 414
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/constraint/ConstraintLayout;->i:Z

    .line 415
    const/4 v0, 0x2

    iput v0, p0, Landroid/support/constraint/ConstraintLayout;->j:I

    .line 416
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/constraint/ConstraintLayout;->k:Landroid/support/constraint/b;

    .line 419
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/constraint/ConstraintLayout;->m:I

    .line 421
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Landroid/support/constraint/ConstraintLayout;->n:Ljava/util/HashMap;

    .line 466
    invoke-direct {p0, p2}, Landroid/support/constraint/ConstraintLayout;->b(Landroid/util/AttributeSet;)V

    .line 467
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const v3, 0x7fffffff

    const/4 v2, 0x0

    .line 470
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 397
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Landroid/support/constraint/ConstraintLayout;->a:Landroid/util/SparseArray;

    .line 400
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Landroid/support/constraint/ConstraintLayout;->c:Ljava/util/ArrayList;

    .line 405
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Landroid/support/constraint/ConstraintLayout;->d:Ljava/util/ArrayList;

    .line 407
    new-instance v0, Landroid/support/constraint/a/a/d;

    invoke-direct {v0}, Landroid/support/constraint/a/a/d;-><init>()V

    iput-object v0, p0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    .line 409
    iput v2, p0, Landroid/support/constraint/ConstraintLayout;->e:I

    .line 410
    iput v2, p0, Landroid/support/constraint/ConstraintLayout;->f:I

    .line 411
    iput v3, p0, Landroid/support/constraint/ConstraintLayout;->g:I

    .line 412
    iput v3, p0, Landroid/support/constraint/ConstraintLayout;->h:I

    .line 414
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/constraint/ConstraintLayout;->i:Z

    .line 415
    const/4 v0, 0x2

    iput v0, p0, Landroid/support/constraint/ConstraintLayout;->j:I

    .line 416
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/constraint/ConstraintLayout;->k:Landroid/support/constraint/b;

    .line 419
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/constraint/ConstraintLayout;->m:I

    .line 421
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Landroid/support/constraint/ConstraintLayout;->n:Ljava/util/HashMap;

    .line 471
    invoke-direct {p0, p2}, Landroid/support/constraint/ConstraintLayout;->b(Landroid/util/AttributeSet;)V

    .line 472
    return-void
.end method

.method private a(II)V
    .locals 15

    .prologue
    .line 1001
    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->getPaddingBottom()I

    move-result v2

    add-int v9, v1, v2

    .line 1002
    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->getPaddingRight()I

    move-result v2

    add-int v10, v1, v2

    .line 1004
    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->getChildCount()I

    move-result v11

    .line 1005
    const/4 v1, 0x0

    move v8, v1

    :goto_0
    if-ge v8, v11, :cond_d

    .line 1006
    invoke-virtual {p0, v8}, Landroid/support/constraint/ConstraintLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v12

    .line 1007
    invoke-virtual {v12}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_1

    .line 1005
    :cond_0
    :goto_1
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto :goto_0

    .line 1010
    :cond_1
    invoke-virtual {v12}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/constraint/ConstraintLayout$a;

    .line 1011
    iget-object v13, v1, Landroid/support/constraint/ConstraintLayout$a;->ad:Landroid/support/constraint/a/a/c;

    .line 1012
    iget-boolean v2, v1, Landroid/support/constraint/ConstraintLayout$a;->T:Z

    if-nez v2, :cond_0

    iget-boolean v2, v1, Landroid/support/constraint/ConstraintLayout$a;->U:Z

    if-nez v2, :cond_0

    .line 1016
    iget v6, v1, Landroid/support/constraint/ConstraintLayout$a;->width:I

    .line 1017
    iget v5, v1, Landroid/support/constraint/ConstraintLayout$a;->height:I

    .line 1021
    iget-boolean v2, v1, Landroid/support/constraint/ConstraintLayout$a;->Q:Z

    if-nez v2, :cond_3

    iget-boolean v2, v1, Landroid/support/constraint/ConstraintLayout$a;->R:Z

    if-nez v2, :cond_3

    iget-boolean v2, v1, Landroid/support/constraint/ConstraintLayout$a;->Q:Z

    if-nez v2, :cond_2

    iget v2, v1, Landroid/support/constraint/ConstraintLayout$a;->F:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_3

    :cond_2
    iget v2, v1, Landroid/support/constraint/ConstraintLayout$a;->width:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    iget-boolean v2, v1, Landroid/support/constraint/ConstraintLayout$a;->R:Z

    if-nez v2, :cond_8

    iget v2, v1, Landroid/support/constraint/ConstraintLayout$a;->G:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_3

    iget v2, v1, Landroid/support/constraint/ConstraintLayout$a;->height:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_8

    :cond_3
    const/4 v2, 0x1

    move v4, v2

    .line 1031
    :goto_2
    const/4 v2, 0x0

    .line 1032
    const/4 v3, 0x0

    .line 1034
    if-eqz v4, :cond_11

    .line 1038
    if-eqz v6, :cond_4

    const/4 v4, -0x1

    if-ne v6, v4, :cond_9

    .line 1039
    :cond_4
    const/4 v2, -0x2

    move/from16 v0, p1

    invoke-static {v0, v10, v2}, Landroid/support/constraint/ConstraintLayout;->getChildMeasureSpec(III)I

    move-result v2

    .line 1041
    const/4 v4, 0x1

    move v7, v2

    .line 1046
    :goto_3
    if-eqz v5, :cond_5

    const/4 v2, -0x1

    if-ne v5, v2, :cond_a

    .line 1047
    :cond_5
    const/4 v2, -0x2

    move/from16 v0, p2

    invoke-static {v0, v9, v2}, Landroid/support/constraint/ConstraintLayout;->getChildMeasureSpec(III)I

    move-result v2

    .line 1049
    const/4 v3, 0x1

    .line 1054
    :goto_4
    invoke-virtual {v12, v7, v2}, Landroid/view/View;->measure(II)V

    .line 1056
    const/4 v2, -0x2

    if-ne v6, v2, :cond_b

    const/4 v2, 0x1

    :goto_5
    invoke-virtual {v13, v2}, Landroid/support/constraint/a/a/c;->a(Z)V

    .line 1057
    const/4 v2, -0x2

    if-ne v5, v2, :cond_c

    const/4 v2, 0x1

    :goto_6
    invoke-virtual {v13, v2}, Landroid/support/constraint/a/a/c;->b(Z)V

    .line 1058
    invoke-virtual {v12}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    .line 1059
    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    move v14, v4

    move v4, v2

    move v2, v14

    .line 1062
    :goto_7
    invoke-virtual {v13, v5}, Landroid/support/constraint/a/a/c;->e(I)V

    .line 1063
    invoke-virtual {v13, v4}, Landroid/support/constraint/a/a/c;->f(I)V

    .line 1064
    if-eqz v2, :cond_6

    .line 1065
    invoke-virtual {v13, v5}, Landroid/support/constraint/a/a/c;->i(I)V

    .line 1067
    :cond_6
    if-eqz v3, :cond_7

    .line 1068
    invoke-virtual {v13, v4}, Landroid/support/constraint/a/a/c;->j(I)V

    .line 1071
    :cond_7
    iget-boolean v1, v1, Landroid/support/constraint/ConstraintLayout$a;->S:Z

    if-eqz v1, :cond_0

    .line 1072
    invoke-virtual {v12}, Landroid/view/View;->getBaseline()I

    move-result v1

    .line 1073
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 1074
    invoke-virtual {v13, v1}, Landroid/support/constraint/a/a/c;->k(I)V

    goto/16 :goto_1

    .line 1021
    :cond_8
    const/4 v2, 0x0

    move v4, v2

    goto :goto_2

    .line 1043
    :cond_9
    move/from16 v0, p1

    invoke-static {v0, v10, v6}, Landroid/support/constraint/ConstraintLayout;->getChildMeasureSpec(III)I

    move-result v4

    move v7, v4

    move v4, v2

    goto :goto_3

    .line 1051
    :cond_a
    move/from16 v0, p2

    invoke-static {v0, v9, v5}, Landroid/support/constraint/ConstraintLayout;->getChildMeasureSpec(III)I

    move-result v2

    goto :goto_4

    .line 1056
    :cond_b
    const/4 v2, 0x0

    goto :goto_5

    .line 1057
    :cond_c
    const/4 v2, 0x0

    goto :goto_6

    .line 1078
    :cond_d
    const/4 v1, 0x0

    move v2, v1

    :goto_8
    if-ge v2, v11, :cond_f

    .line 1079
    invoke-virtual {p0, v2}, Landroid/support/constraint/ConstraintLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1080
    instance-of v3, v1, Landroid/support/constraint/d;

    if-eqz v3, :cond_e

    .line 1081
    check-cast v1, Landroid/support/constraint/d;

    invoke-virtual {v1, p0}, Landroid/support/constraint/d;->b(Landroid/support/constraint/ConstraintLayout;)V

    .line 1078
    :cond_e
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_8

    .line 1085
    :cond_f
    iget-object v1, p0, Landroid/support/constraint/ConstraintLayout;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1086
    if-lez v3, :cond_10

    .line 1087
    const/4 v1, 0x0

    move v2, v1

    :goto_9
    if-ge v2, v3, :cond_10

    .line 1088
    iget-object v1, p0, Landroid/support/constraint/ConstraintLayout;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/constraint/a;

    .line 1089
    invoke-virtual {v1, p0}, Landroid/support/constraint/a;->b(Landroid/support/constraint/ConstraintLayout;)V

    .line 1087
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_9

    .line 1092
    :cond_10
    return-void

    :cond_11
    move v4, v5

    move v5, v6

    goto :goto_7
.end method

.method private final b(I)Landroid/support/constraint/a/a/c;
    .locals 1

    .prologue
    .line 982
    if-nez p1, :cond_0

    .line 983
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    .line 989
    :goto_0
    return-object v0

    .line 985
    :cond_0
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 986
    if-ne v0, p0, :cond_1

    .line 987
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    goto :goto_0

    .line 989
    :cond_1
    if-nez v0, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/ConstraintLayout$a;

    iget-object v0, v0, Landroid/support/constraint/ConstraintLayout$a;->ad:Landroid/support/constraint/a/a/c;

    goto :goto_0
.end method

.method private b(II)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1257
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    .line 1258
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 1259
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v6

    .line 1260
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 1262
    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->getPaddingBottom()I

    move-result v4

    add-int v7, v2, v4

    .line 1263
    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->getPaddingRight()I

    move-result v4

    add-int v8, v2, v4

    .line 1265
    sget-object v4, Landroid/support/constraint/a/a/c$a;->a:Landroid/support/constraint/a/a/c$a;

    .line 1266
    sget-object v2, Landroid/support/constraint/a/a/c$a;->a:Landroid/support/constraint/a/a/c$a;

    .line 1270
    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 1271
    sparse-switch v5, :sswitch_data_0

    move v3, v1

    .line 1285
    :goto_0
    sparse-switch v6, :sswitch_data_1

    move v0, v1

    .line 1300
    :goto_1
    iget-object v5, p0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {v5, v1}, Landroid/support/constraint/a/a/d;->g(I)V

    .line 1301
    iget-object v5, p0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {v5, v1}, Landroid/support/constraint/a/a/d;->h(I)V

    .line 1302
    iget-object v1, p0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {v1, v4}, Landroid/support/constraint/a/a/d;->a(Landroid/support/constraint/a/a/c$a;)V

    .line 1303
    iget-object v1, p0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {v1, v3}, Landroid/support/constraint/a/a/d;->e(I)V

    .line 1304
    iget-object v1, p0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {v1, v2}, Landroid/support/constraint/a/a/d;->b(Landroid/support/constraint/a/a/c$a;)V

    .line 1305
    iget-object v1, p0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {v1, v0}, Landroid/support/constraint/a/a/d;->f(I)V

    .line 1306
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    iget v1, p0, Landroid/support/constraint/ConstraintLayout;->e:I

    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/d;->g(I)V

    .line 1307
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    iget v1, p0, Landroid/support/constraint/ConstraintLayout;->f:I

    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/d;->h(I)V

    .line 1308
    return-void

    .line 1273
    :sswitch_0
    sget-object v4, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    goto :goto_0

    .line 1278
    :sswitch_1
    sget-object v3, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    move-object v4, v3

    move v3, v1

    .line 1280
    goto :goto_0

    .line 1282
    :sswitch_2
    iget v5, p0, Landroid/support/constraint/ConstraintLayout;->g:I

    invoke-static {v5, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    sub-int/2addr v3, v8

    goto :goto_0

    .line 1287
    :sswitch_3
    sget-object v2, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    goto :goto_1

    .line 1292
    :sswitch_4
    sget-object v0, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    move-object v2, v0

    move v0, v1

    .line 1294
    goto :goto_1

    .line 1296
    :sswitch_5
    iget v5, p0, Landroid/support/constraint/ConstraintLayout;->h:I

    invoke-static {v5, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    sub-int/2addr v0, v7

    goto :goto_1

    .line 1271
    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_0
        0x0 -> :sswitch_1
        0x40000000 -> :sswitch_2
    .end sparse-switch

    .line 1285
    :sswitch_data_1
    .sparse-switch
        -0x80000000 -> :sswitch_3
        0x0 -> :sswitch_4
        0x40000000 -> :sswitch_5
    .end sparse-switch
.end method

.method private b(Landroid/util/AttributeSet;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x0

    .line 490
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {v0, p0}, Landroid/support/constraint/a/a/d;->a(Ljava/lang/Object;)V

    .line 491
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->a:Landroid/util/SparseArray;

    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->getId()I

    move-result v2

    invoke-virtual {v0, v2, p0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 492
    iput-object v7, p0, Landroid/support/constraint/ConstraintLayout;->k:Landroid/support/constraint/b;

    .line 493
    if-eqz p1, :cond_8

    .line 494
    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v2, Landroid/support/constraint/e$b;->ConstraintLayout_Layout:[I

    invoke-virtual {v0, p1, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 495
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v3

    move v0, v1

    .line 496
    :goto_0
    if-ge v0, v3, :cond_7

    .line 497
    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v4

    .line 498
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_android_minWidth:I

    if-ne v4, v5, :cond_1

    .line 499
    iget v5, p0, Landroid/support/constraint/ConstraintLayout;->e:I

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v4

    iput v4, p0, Landroid/support/constraint/ConstraintLayout;->e:I

    .line 496
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 500
    :cond_1
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_android_minHeight:I

    if-ne v4, v5, :cond_2

    .line 501
    iget v5, p0, Landroid/support/constraint/ConstraintLayout;->f:I

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v4

    iput v4, p0, Landroid/support/constraint/ConstraintLayout;->f:I

    goto :goto_1

    .line 502
    :cond_2
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_android_maxWidth:I

    if-ne v4, v5, :cond_3

    .line 503
    iget v5, p0, Landroid/support/constraint/ConstraintLayout;->g:I

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v4

    iput v4, p0, Landroid/support/constraint/ConstraintLayout;->g:I

    goto :goto_1

    .line 504
    :cond_3
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_android_maxHeight:I

    if-ne v4, v5, :cond_4

    .line 505
    iget v5, p0, Landroid/support/constraint/ConstraintLayout;->h:I

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v4

    iput v4, p0, Landroid/support/constraint/ConstraintLayout;->h:I

    goto :goto_1

    .line 506
    :cond_4
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_layout_optimizationLevel:I

    if-ne v4, v5, :cond_5

    .line 507
    iget v5, p0, Landroid/support/constraint/ConstraintLayout;->j:I

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    iput v4, p0, Landroid/support/constraint/ConstraintLayout;->j:I

    goto :goto_1

    .line 508
    :cond_5
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_title:I

    if-ne v4, v5, :cond_6

    .line 509
    invoke-virtual {v2, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Landroid/support/constraint/ConstraintLayout;->l:Ljava/lang/String;

    goto :goto_1

    .line 510
    :cond_6
    sget v5, Landroid/support/constraint/e$b;->ConstraintLayout_Layout_constraintSet:I

    if-ne v4, v5, :cond_0

    .line 511
    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    .line 513
    :try_start_0
    new-instance v5, Landroid/support/constraint/b;

    invoke-direct {v5}, Landroid/support/constraint/b;-><init>()V

    iput-object v5, p0, Landroid/support/constraint/ConstraintLayout;->k:Landroid/support/constraint/b;

    .line 514
    iget-object v5, p0, Landroid/support/constraint/ConstraintLayout;->k:Landroid/support/constraint/b;

    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, Landroid/support/constraint/b;->a(Landroid/content/Context;I)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 518
    :goto_2
    iput v4, p0, Landroid/support/constraint/ConstraintLayout;->m:I

    goto :goto_1

    .line 515
    :catch_0
    move-exception v5

    .line 516
    iput-object v7, p0, Landroid/support/constraint/ConstraintLayout;->k:Landroid/support/constraint/b;

    goto :goto_2

    .line 521
    :cond_7
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 523
    :cond_8
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    iget v1, p0, Landroid/support/constraint/ConstraintLayout;->j:I

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/d;->a(I)V

    .line 524
    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 689
    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->getChildCount()I

    move-result v2

    move v1, v0

    .line 692
    :goto_0
    if-ge v1, v2, :cond_0

    .line 693
    invoke-virtual {p0, v1}, Landroid/support/constraint/ConstraintLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 694
    invoke-virtual {v3}, Landroid/view/View;->isLayoutRequested()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 695
    const/4 v0, 0x1

    .line 699
    :cond_0
    if-eqz v0, :cond_1

    .line 700
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 701
    invoke-direct {p0}, Landroid/support/constraint/ConstraintLayout;->d()V

    .line 703
    :cond_1
    return-void

    .line 692
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private d()V
    .locals 14

    .prologue
    .line 706
    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->isInEditMode()Z

    move-result v12

    .line 708
    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->getChildCount()I

    move-result v13

    .line 709
    if-eqz v12, :cond_0

    .line 713
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v13, :cond_0

    .line 714
    invoke-virtual {p0, v0}, Landroid/support/constraint/ConstraintLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 716
    :try_start_0
    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v2

    .line 717
    const/4 v3, 0x0

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v3, v2, v1}, Landroid/support/constraint/ConstraintLayout;->a(ILjava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 713
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 724
    :cond_0
    iget v0, p0, Landroid/support/constraint/ConstraintLayout;->m:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 725
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v13, :cond_2

    .line 726
    invoke-virtual {p0, v1}, Landroid/support/constraint/ConstraintLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 727
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v2

    iget v3, p0, Landroid/support/constraint/ConstraintLayout;->m:I

    if-ne v2, v3, :cond_1

    instance-of v2, v0, Landroid/support/constraint/c;

    if-eqz v2, :cond_1

    .line 728
    check-cast v0, Landroid/support/constraint/c;

    invoke-virtual {v0}, Landroid/support/constraint/c;->getConstraintSet()Landroid/support/constraint/b;

    move-result-object v0

    iput-object v0, p0, Landroid/support/constraint/ConstraintLayout;->k:Landroid/support/constraint/b;

    .line 725
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 732
    :cond_2
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->k:Landroid/support/constraint/b;

    if-eqz v0, :cond_3

    .line 733
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->k:Landroid/support/constraint/b;

    invoke-virtual {v0, p0}, Landroid/support/constraint/b;->c(Landroid/support/constraint/ConstraintLayout;)V

    .line 736
    :cond_3
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {v0}, Landroid/support/constraint/a/a/d;->I()V

    .line 738
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 739
    if-lez v2, :cond_4

    .line 740
    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, v2, :cond_4

    .line 741
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/a;

    .line 742
    invoke-virtual {v0, p0}, Landroid/support/constraint/a;->a(Landroid/support/constraint/ConstraintLayout;)V

    .line 740
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 746
    :cond_4
    const/4 v0, 0x0

    move v1, v0

    :goto_4
    if-ge v1, v13, :cond_6

    .line 747
    invoke-virtual {p0, v1}, Landroid/support/constraint/ConstraintLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 748
    instance-of v2, v0, Landroid/support/constraint/d;

    if-eqz v2, :cond_5

    .line 749
    check-cast v0, Landroid/support/constraint/d;

    invoke-virtual {v0, p0}, Landroid/support/constraint/d;->a(Landroid/support/constraint/ConstraintLayout;)V

    .line 746
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 752
    :cond_6
    const/4 v0, 0x0

    move v11, v0

    :goto_5
    if-ge v11, v13, :cond_26

    .line 753
    invoke-virtual {p0, v11}, Landroid/support/constraint/ConstraintLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 754
    invoke-virtual {p0, v2}, Landroid/support/constraint/ConstraintLayout;->a(Landroid/view/View;)Landroid/support/constraint/a/a/c;

    move-result-object v0

    .line 755
    if-nez v0, :cond_8

    .line 752
    :cond_7
    :goto_6
    add-int/lit8 v0, v11, 0x1

    move v11, v0

    goto :goto_5

    .line 758
    :cond_8
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Landroid/support/constraint/ConstraintLayout$a;

    .line 759
    invoke-virtual {v8}, Landroid/support/constraint/ConstraintLayout$a;->a()V

    .line 760
    iget-boolean v1, v8, Landroid/support/constraint/ConstraintLayout$a;->ae:Z

    if-eqz v1, :cond_e

    .line 761
    const/4 v1, 0x0

    iput-boolean v1, v8, Landroid/support/constraint/ConstraintLayout$a;->ae:Z

    .line 765
    :goto_7
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/c;->b(I)V

    .line 766
    iget-boolean v1, v8, Landroid/support/constraint/ConstraintLayout$a;->V:Z

    if-eqz v1, :cond_9

    .line 767
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/c;->b(I)V

    .line 769
    :cond_9
    invoke-virtual {v0, v2}, Landroid/support/constraint/a/a/c;->a(Ljava/lang/Object;)V

    .line 770
    iget-object v1, p0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {v1, v0}, Landroid/support/constraint/a/a/d;->b(Landroid/support/constraint/a/a/c;)V

    .line 772
    iget-boolean v1, v8, Landroid/support/constraint/ConstraintLayout$a;->R:Z

    if-eqz v1, :cond_a

    iget-boolean v1, v8, Landroid/support/constraint/ConstraintLayout$a;->Q:Z

    if-nez v1, :cond_b

    .line 773
    :cond_a
    iget-object v1, p0, Landroid/support/constraint/ConstraintLayout;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 776
    :cond_b
    iget-boolean v1, v8, Landroid/support/constraint/ConstraintLayout$a;->T:Z

    if-eqz v1, :cond_f

    .line 777
    check-cast v0, Landroid/support/constraint/a/a/e;

    .line 778
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->a:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_c

    .line 779
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->a:I

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/e;->n(I)V

    .line 781
    :cond_c
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->b:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_d

    .line 782
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->b:I

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/e;->o(I)V

    .line 784
    :cond_d
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->c:F

    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_7

    .line 785
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->c:F

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/e;->e(F)V

    goto :goto_6

    .line 763
    :cond_e
    invoke-virtual {v0}, Landroid/support/constraint/a/a/c;->a()V

    goto :goto_7

    .line 787
    :cond_f
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->W:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_10

    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->X:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_10

    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->Y:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_10

    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->Z:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_10

    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->h:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_10

    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->i:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_10

    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->j:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_10

    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->k:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_10

    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->l:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_10

    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->N:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_10

    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->O:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_10

    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->width:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_10

    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->height:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_7

    .line 802
    :cond_10
    iget v7, v8, Landroid/support/constraint/ConstraintLayout$a;->W:I

    .line 803
    iget v6, v8, Landroid/support/constraint/ConstraintLayout$a;->X:I

    .line 804
    iget v4, v8, Landroid/support/constraint/ConstraintLayout$a;->Y:I

    .line 805
    iget v3, v8, Landroid/support/constraint/ConstraintLayout$a;->Z:I

    .line 806
    iget v5, v8, Landroid/support/constraint/ConstraintLayout$a;->aa:I

    .line 807
    iget v2, v8, Landroid/support/constraint/ConstraintLayout$a;->ab:I

    .line 808
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->ac:F

    .line 810
    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v10, 0x11

    if-ge v9, v10, :cond_27

    .line 813
    iget v7, v8, Landroid/support/constraint/ConstraintLayout$a;->d:I

    .line 814
    iget v6, v8, Landroid/support/constraint/ConstraintLayout$a;->e:I

    .line 815
    iget v4, v8, Landroid/support/constraint/ConstraintLayout$a;->f:I

    .line 816
    iget v3, v8, Landroid/support/constraint/ConstraintLayout$a;->g:I

    .line 817
    iget v5, v8, Landroid/support/constraint/ConstraintLayout$a;->q:I

    .line 818
    iget v2, v8, Landroid/support/constraint/ConstraintLayout$a;->s:I

    .line 819
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->w:F

    .line 821
    const/4 v9, -0x1

    if-ne v7, v9, :cond_11

    const/4 v9, -0x1

    if-ne v6, v9, :cond_11

    .line 822
    iget v9, v8, Landroid/support/constraint/ConstraintLayout$a;->n:I

    const/4 v10, -0x1

    if-eq v9, v10, :cond_1c

    .line 823
    iget v7, v8, Landroid/support/constraint/ConstraintLayout$a;->n:I

    .line 828
    :cond_11
    :goto_8
    const/4 v9, -0x1

    if-ne v4, v9, :cond_27

    const/4 v9, -0x1

    if-ne v3, v9, :cond_27

    .line 829
    iget v9, v8, Landroid/support/constraint/ConstraintLayout$a;->o:I

    const/4 v10, -0x1

    if-eq v9, v10, :cond_1d

    .line 830
    iget v4, v8, Landroid/support/constraint/ConstraintLayout$a;->o:I

    move v9, v1

    move v10, v4

    move v1, v6

    move v6, v2

    move v2, v7

    move v7, v3

    .line 838
    :goto_9
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1e

    .line 839
    invoke-direct {p0, v2}, Landroid/support/constraint/ConstraintLayout;->b(I)Landroid/support/constraint/a/a/c;

    move-result-object v2

    .line 840
    if-eqz v2, :cond_12

    .line 841
    sget-object v1, Landroid/support/constraint/a/a/b$c;->b:Landroid/support/constraint/a/a/b$c;

    sget-object v3, Landroid/support/constraint/a/a/b$c;->b:Landroid/support/constraint/a/a/b$c;

    iget v4, v8, Landroid/support/constraint/ConstraintLayout$a;->leftMargin:I

    invoke-virtual/range {v0 .. v5}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;Landroid/support/constraint/a/a/c;Landroid/support/constraint/a/a/b$c;II)V

    .line 855
    :cond_12
    :goto_a
    const/4 v1, -0x1

    if-eq v10, v1, :cond_1f

    .line 856
    invoke-direct {p0, v10}, Landroid/support/constraint/ConstraintLayout;->b(I)Landroid/support/constraint/a/a/c;

    move-result-object v2

    .line 857
    if-eqz v2, :cond_13

    .line 858
    sget-object v1, Landroid/support/constraint/a/a/b$c;->d:Landroid/support/constraint/a/a/b$c;

    sget-object v3, Landroid/support/constraint/a/a/b$c;->b:Landroid/support/constraint/a/a/b$c;

    iget v4, v8, Landroid/support/constraint/ConstraintLayout$a;->rightMargin:I

    move v5, v6

    invoke-virtual/range {v0 .. v5}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;Landroid/support/constraint/a/a/c;Landroid/support/constraint/a/a/b$c;II)V

    .line 872
    :cond_13
    :goto_b
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->h:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_20

    .line 873
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->h:I

    invoke-direct {p0, v1}, Landroid/support/constraint/ConstraintLayout;->b(I)Landroid/support/constraint/a/a/c;

    move-result-object v2

    .line 874
    if-eqz v2, :cond_14

    .line 875
    sget-object v1, Landroid/support/constraint/a/a/b$c;->c:Landroid/support/constraint/a/a/b$c;

    sget-object v3, Landroid/support/constraint/a/a/b$c;->c:Landroid/support/constraint/a/a/b$c;

    iget v4, v8, Landroid/support/constraint/ConstraintLayout$a;->topMargin:I

    iget v5, v8, Landroid/support/constraint/ConstraintLayout$a;->r:I

    invoke-virtual/range {v0 .. v5}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;Landroid/support/constraint/a/a/c;Landroid/support/constraint/a/a/b$c;II)V

    .line 889
    :cond_14
    :goto_c
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->j:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_21

    .line 890
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->j:I

    invoke-direct {p0, v1}, Landroid/support/constraint/ConstraintLayout;->b(I)Landroid/support/constraint/a/a/c;

    move-result-object v2

    .line 891
    if-eqz v2, :cond_15

    .line 892
    sget-object v1, Landroid/support/constraint/a/a/b$c;->e:Landroid/support/constraint/a/a/b$c;

    sget-object v3, Landroid/support/constraint/a/a/b$c;->c:Landroid/support/constraint/a/a/b$c;

    iget v4, v8, Landroid/support/constraint/ConstraintLayout$a;->bottomMargin:I

    iget v5, v8, Landroid/support/constraint/ConstraintLayout$a;->t:I

    invoke-virtual/range {v0 .. v5}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;Landroid/support/constraint/a/a/c;Landroid/support/constraint/a/a/b$c;II)V

    .line 906
    :cond_15
    :goto_d
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->l:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_16

    .line 907
    iget-object v1, p0, Landroid/support/constraint/ConstraintLayout;->a:Landroid/util/SparseArray;

    iget v2, v8, Landroid/support/constraint/ConstraintLayout$a;->l:I

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 908
    iget v2, v8, Landroid/support/constraint/ConstraintLayout$a;->l:I

    invoke-direct {p0, v2}, Landroid/support/constraint/ConstraintLayout;->b(I)Landroid/support/constraint/a/a/c;

    move-result-object v2

    .line 909
    if-eqz v2, :cond_16

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    instance-of v3, v3, Landroid/support/constraint/ConstraintLayout$a;

    if-eqz v3, :cond_16

    .line 910
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/constraint/ConstraintLayout$a;

    .line 911
    const/4 v3, 0x1

    iput-boolean v3, v8, Landroid/support/constraint/ConstraintLayout$a;->S:Z

    .line 912
    const/4 v3, 0x1

    iput-boolean v3, v1, Landroid/support/constraint/ConstraintLayout$a;->S:Z

    .line 913
    sget-object v1, Landroid/support/constraint/a/a/b$c;->f:Landroid/support/constraint/a/a/b$c;

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v1

    .line 914
    sget-object v3, Landroid/support/constraint/a/a/b$c;->f:Landroid/support/constraint/a/a/b$c;

    .line 915
    invoke-virtual {v2, v3}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v2

    .line 916
    const/4 v3, 0x0

    const/4 v4, -0x1

    sget-object v5, Landroid/support/constraint/a/a/b$b;->b:Landroid/support/constraint/a/a/b$b;

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-virtual/range {v1 .. v7}, Landroid/support/constraint/a/a/b;->a(Landroid/support/constraint/a/a/b;IILandroid/support/constraint/a/a/b$b;IZ)Z

    .line 919
    sget-object v1, Landroid/support/constraint/a/a/b$c;->c:Landroid/support/constraint/a/a/b$c;

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/constraint/a/a/b;->i()V

    .line 920
    sget-object v1, Landroid/support/constraint/a/a/b$c;->e:Landroid/support/constraint/a/a/b$c;

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/constraint/a/a/b;->i()V

    .line 924
    :cond_16
    const/4 v1, 0x0

    cmpl-float v1, v9, v1

    if-ltz v1, :cond_17

    const/high16 v1, 0x3f000000    # 0.5f

    cmpl-float v1, v9, v1

    if-eqz v1, :cond_17

    .line 925
    invoke-virtual {v0, v9}, Landroid/support/constraint/a/a/c;->a(F)V

    .line 927
    :cond_17
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->x:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_18

    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->x:F

    const/high16 v2, 0x3f000000    # 0.5f

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_18

    .line 928
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->x:F

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/c;->b(F)V

    .line 931
    :cond_18
    if-eqz v12, :cond_1a

    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->N:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_19

    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->O:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1a

    .line 933
    :cond_19
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->N:I

    iget v2, v8, Landroid/support/constraint/ConstraintLayout$a;->O:I

    invoke-virtual {v0, v1, v2}, Landroid/support/constraint/a/a/c;->a(II)V

    .line 937
    :cond_1a
    iget-boolean v1, v8, Landroid/support/constraint/ConstraintLayout$a;->Q:Z

    if-nez v1, :cond_23

    .line 938
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->width:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_22

    .line 939
    sget-object v1, Landroid/support/constraint/a/a/c$a;->d:Landroid/support/constraint/a/a/c$a;

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/c$a;)V

    .line 940
    sget-object v1, Landroid/support/constraint/a/a/b$c;->b:Landroid/support/constraint/a/a/b$c;

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v1

    iget v2, v8, Landroid/support/constraint/ConstraintLayout$a;->leftMargin:I

    iput v2, v1, Landroid/support/constraint/a/a/b;->d:I

    .line 941
    sget-object v1, Landroid/support/constraint/a/a/b$c;->d:Landroid/support/constraint/a/a/b$c;

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v1

    iget v2, v8, Landroid/support/constraint/ConstraintLayout$a;->rightMargin:I

    iput v2, v1, Landroid/support/constraint/a/a/b;->d:I

    .line 950
    :goto_e
    iget-boolean v1, v8, Landroid/support/constraint/ConstraintLayout$a;->R:Z

    if-nez v1, :cond_25

    .line 951
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->height:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_24

    .line 952
    sget-object v1, Landroid/support/constraint/a/a/c$a;->d:Landroid/support/constraint/a/a/c$a;

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/c;->b(Landroid/support/constraint/a/a/c$a;)V

    .line 953
    sget-object v1, Landroid/support/constraint/a/a/b$c;->c:Landroid/support/constraint/a/a/b$c;

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v1

    iget v2, v8, Landroid/support/constraint/ConstraintLayout$a;->topMargin:I

    iput v2, v1, Landroid/support/constraint/a/a/b;->d:I

    .line 954
    sget-object v1, Landroid/support/constraint/a/a/b$c;->e:Landroid/support/constraint/a/a/b$c;

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v1

    iget v2, v8, Landroid/support/constraint/ConstraintLayout$a;->bottomMargin:I

    iput v2, v1, Landroid/support/constraint/a/a/b;->d:I

    .line 964
    :goto_f
    iget-object v1, v8, Landroid/support/constraint/ConstraintLayout$a;->y:Ljava/lang/String;

    if-eqz v1, :cond_1b

    .line 965
    iget-object v1, v8, Landroid/support/constraint/ConstraintLayout$a;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/c;->a(Ljava/lang/String;)V

    .line 967
    :cond_1b
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->B:F

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/c;->c(F)V

    .line 968
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->C:F

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/c;->d(F)V

    .line 969
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->D:I

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/c;->l(I)V

    .line 970
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->E:I

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/c;->m(I)V

    .line 971
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->F:I

    iget v2, v8, Landroid/support/constraint/ConstraintLayout$a;->H:I

    iget v3, v8, Landroid/support/constraint/ConstraintLayout$a;->J:I

    iget v4, v8, Landroid/support/constraint/ConstraintLayout$a;->L:F

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/support/constraint/a/a/c;->a(IIIF)V

    .line 974
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->G:I

    iget v2, v8, Landroid/support/constraint/ConstraintLayout$a;->I:I

    iget v3, v8, Landroid/support/constraint/ConstraintLayout$a;->K:I

    iget v4, v8, Landroid/support/constraint/ConstraintLayout$a;->M:F

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/support/constraint/a/a/c;->b(IIIF)V

    goto/16 :goto_6

    .line 824
    :cond_1c
    iget v9, v8, Landroid/support/constraint/ConstraintLayout$a;->m:I

    const/4 v10, -0x1

    if-eq v9, v10, :cond_11

    .line 825
    iget v6, v8, Landroid/support/constraint/ConstraintLayout$a;->m:I

    goto/16 :goto_8

    .line 831
    :cond_1d
    iget v9, v8, Landroid/support/constraint/ConstraintLayout$a;->p:I

    const/4 v10, -0x1

    if-eq v9, v10, :cond_27

    .line 832
    iget v3, v8, Landroid/support/constraint/ConstraintLayout$a;->p:I

    move v9, v1

    move v10, v4

    move v1, v6

    move v6, v2

    move v2, v7

    move v7, v3

    goto/16 :goto_9

    .line 845
    :cond_1e
    const/4 v2, -0x1

    if-eq v1, v2, :cond_12

    .line 846
    invoke-direct {p0, v1}, Landroid/support/constraint/ConstraintLayout;->b(I)Landroid/support/constraint/a/a/c;

    move-result-object v2

    .line 847
    if-eqz v2, :cond_12

    .line 848
    sget-object v1, Landroid/support/constraint/a/a/b$c;->b:Landroid/support/constraint/a/a/b$c;

    sget-object v3, Landroid/support/constraint/a/a/b$c;->d:Landroid/support/constraint/a/a/b$c;

    iget v4, v8, Landroid/support/constraint/ConstraintLayout$a;->leftMargin:I

    invoke-virtual/range {v0 .. v5}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;Landroid/support/constraint/a/a/c;Landroid/support/constraint/a/a/b$c;II)V

    goto/16 :goto_a

    .line 862
    :cond_1f
    const/4 v1, -0x1

    if-eq v7, v1, :cond_13

    .line 863
    invoke-direct {p0, v7}, Landroid/support/constraint/ConstraintLayout;->b(I)Landroid/support/constraint/a/a/c;

    move-result-object v2

    .line 864
    if-eqz v2, :cond_13

    .line 865
    sget-object v1, Landroid/support/constraint/a/a/b$c;->d:Landroid/support/constraint/a/a/b$c;

    sget-object v3, Landroid/support/constraint/a/a/b$c;->d:Landroid/support/constraint/a/a/b$c;

    iget v4, v8, Landroid/support/constraint/ConstraintLayout$a;->rightMargin:I

    move v5, v6

    invoke-virtual/range {v0 .. v5}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;Landroid/support/constraint/a/a/c;Landroid/support/constraint/a/a/b$c;II)V

    goto/16 :goto_b

    .line 879
    :cond_20
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->i:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_14

    .line 880
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->i:I

    invoke-direct {p0, v1}, Landroid/support/constraint/ConstraintLayout;->b(I)Landroid/support/constraint/a/a/c;

    move-result-object v2

    .line 881
    if-eqz v2, :cond_14

    .line 882
    sget-object v1, Landroid/support/constraint/a/a/b$c;->c:Landroid/support/constraint/a/a/b$c;

    sget-object v3, Landroid/support/constraint/a/a/b$c;->e:Landroid/support/constraint/a/a/b$c;

    iget v4, v8, Landroid/support/constraint/ConstraintLayout$a;->topMargin:I

    iget v5, v8, Landroid/support/constraint/ConstraintLayout$a;->r:I

    invoke-virtual/range {v0 .. v5}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;Landroid/support/constraint/a/a/c;Landroid/support/constraint/a/a/b$c;II)V

    goto/16 :goto_c

    .line 896
    :cond_21
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->k:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_15

    .line 897
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->k:I

    invoke-direct {p0, v1}, Landroid/support/constraint/ConstraintLayout;->b(I)Landroid/support/constraint/a/a/c;

    move-result-object v2

    .line 898
    if-eqz v2, :cond_15

    .line 899
    sget-object v1, Landroid/support/constraint/a/a/b$c;->e:Landroid/support/constraint/a/a/b$c;

    sget-object v3, Landroid/support/constraint/a/a/b$c;->e:Landroid/support/constraint/a/a/b$c;

    iget v4, v8, Landroid/support/constraint/ConstraintLayout$a;->bottomMargin:I

    iget v5, v8, Landroid/support/constraint/ConstraintLayout$a;->t:I

    invoke-virtual/range {v0 .. v5}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;Landroid/support/constraint/a/a/c;Landroid/support/constraint/a/a/b$c;II)V

    goto/16 :goto_d

    .line 943
    :cond_22
    sget-object v1, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/c$a;)V

    .line 944
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/c;->e(I)V

    goto/16 :goto_e

    .line 947
    :cond_23
    sget-object v1, Landroid/support/constraint/a/a/c$a;->a:Landroid/support/constraint/a/a/c$a;

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/c$a;)V

    .line 948
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->width:I

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/c;->e(I)V

    goto/16 :goto_e

    .line 956
    :cond_24
    sget-object v1, Landroid/support/constraint/a/a/c$a;->c:Landroid/support/constraint/a/a/c$a;

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/c;->b(Landroid/support/constraint/a/a/c$a;)V

    .line 957
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/c;->f(I)V

    goto/16 :goto_f

    .line 960
    :cond_25
    sget-object v1, Landroid/support/constraint/a/a/c$a;->a:Landroid/support/constraint/a/a/c$a;

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/c;->b(Landroid/support/constraint/a/a/c$a;)V

    .line 961
    iget v1, v8, Landroid/support/constraint/ConstraintLayout$a;->height:I

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/c;->f(I)V

    goto/16 :goto_f

    .line 979
    :cond_26
    return-void

    .line 718
    :catch_0
    move-exception v1

    goto/16 :goto_1

    :cond_27
    move v9, v1

    move v10, v4

    move v1, v6

    move v6, v2

    move v2, v7

    move v7, v3

    goto/16 :goto_9
.end method


# virtual methods
.method public a(Landroid/util/AttributeSet;)Landroid/support/constraint/ConstraintLayout$a;
    .locals 2

    .prologue
    .line 1406
    new-instance v0, Landroid/support/constraint/ConstraintLayout$a;

    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/support/constraint/ConstraintLayout$a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public final a(Landroid/view/View;)Landroid/support/constraint/a/a/c;
    .locals 1

    .prologue
    .line 994
    if-ne p1, p0, :cond_0

    .line 995
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    .line 997
    :goto_0
    return-object v0

    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/ConstraintLayout$a;

    iget-object v0, v0, Landroid/support/constraint/ConstraintLayout$a;->ad:Landroid/support/constraint/a/a/c;

    goto :goto_0
.end method

.method public a(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 1448
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public a(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 450
    if-nez p1, :cond_0

    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 451
    check-cast p2, Ljava/lang/String;

    .line 452
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->n:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->n:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 453
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->n:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 456
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a()V
    .locals 1

    .prologue
    .line 1317
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {v0}, Landroid/support/constraint/a/a/d;->F()V

    .line 1322
    return-void
.end method

.method public a(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 432
    if-nez p1, :cond_2

    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_2

    instance-of v0, p3, Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 433
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->n:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 434
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Landroid/support/constraint/ConstraintLayout;->n:Ljava/util/HashMap;

    .line 436
    :cond_0
    check-cast p2, Ljava/lang/String;

    .line 437
    const-string v0, "/"

    invoke-virtual {p2, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 438
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 439
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p2

    .line 441
    :cond_1
    check-cast p3, Ljava/lang/Integer;

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 442
    iget-object v1, p0, Landroid/support/constraint/ConstraintLayout;->n:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 444
    :cond_2
    return-void
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    .line 531
    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 532
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    .line 533
    invoke-virtual {p0, p1}, Landroid/support/constraint/ConstraintLayout;->onViewAdded(Landroid/view/View;)V

    .line 535
    :cond_0
    return-void
.end method

.method protected b()Landroid/support/constraint/ConstraintLayout$a;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 1414
    new-instance v0, Landroid/support/constraint/ConstraintLayout$a;

    invoke-direct {v0, v1, v1}, Landroid/support/constraint/ConstraintLayout$a;-><init>(II)V

    return-object v0
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 1430
    instance-of v0, p1, Landroid/support/constraint/ConstraintLayout$a;

    return v0
.end method

.method protected synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 384
    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->b()Landroid/support/constraint/ConstraintLayout$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 384
    invoke-virtual {p0, p1}, Landroid/support/constraint/ConstraintLayout;->a(Landroid/util/AttributeSet;)Landroid/support/constraint/ConstraintLayout$a;

    move-result-object v0

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 1422
    new-instance v0, Landroid/support/constraint/ConstraintLayout$a;

    invoke-direct {v0, p1}, Landroid/support/constraint/ConstraintLayout$a;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public getMaxHeight()I
    .locals 1

    .prologue
    .line 685
    iget v0, p0, Landroid/support/constraint/ConstraintLayout;->h:I

    return v0
.end method

.method public getMaxWidth()I
    .locals 1

    .prologue
    .line 674
    iget v0, p0, Landroid/support/constraint/ConstraintLayout;->g:I

    return v0
.end method

.method public getMinHeight()I
    .locals 1

    .prologue
    .line 637
    iget v0, p0, Landroid/support/constraint/ConstraintLayout;->f:I

    return v0
.end method

.method public getMinWidth()I
    .locals 1

    .prologue
    .line 626
    iget v0, p0, Landroid/support/constraint/ConstraintLayout;->e:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->l:Ljava/lang/String;

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 1329
    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->getChildCount()I

    move-result v4

    .line 1330
    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->isInEditMode()Z

    move-result v5

    .line 1331
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1332
    if-lez v2, :cond_0

    move v1, v3

    .line 1333
    :goto_0
    if-ge v1, v2, :cond_0

    .line 1334
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/a;

    .line 1335
    invoke-virtual {v0, p0}, Landroid/support/constraint/a;->c(Landroid/support/constraint/ConstraintLayout;)V

    .line 1333
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v2, v3

    .line 1338
    :goto_1
    if-ge v2, v4, :cond_3

    .line 1339
    invoke-virtual {p0, v2}, Landroid/support/constraint/ConstraintLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1340
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/ConstraintLayout$a;

    .line 1341
    iget-object v6, v0, Landroid/support/constraint/ConstraintLayout$a;->ad:Landroid/support/constraint/a/a/c;

    .line 1343
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v7

    const/16 v8, 0x8

    if-ne v7, v8, :cond_2

    iget-boolean v7, v0, Landroid/support/constraint/ConstraintLayout$a;->T:Z

    if-nez v7, :cond_2

    iget-boolean v7, v0, Landroid/support/constraint/ConstraintLayout$a;->U:Z

    if-nez v7, :cond_2

    if-nez v5, :cond_2

    .line 1338
    :cond_1
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1348
    :cond_2
    iget-boolean v0, v0, Landroid/support/constraint/ConstraintLayout$a;->V:Z

    if-nez v0, :cond_1

    .line 1351
    invoke-virtual {v6}, Landroid/support/constraint/a/a/c;->n()I

    move-result v7

    .line 1352
    invoke-virtual {v6}, Landroid/support/constraint/a/a/c;->o()I

    move-result v8

    .line 1353
    invoke-virtual {v6}, Landroid/support/constraint/a/a/c;->h()I

    move-result v0

    add-int v9, v7, v0

    .line 1354
    invoke-virtual {v6}, Landroid/support/constraint/a/a/c;->l()I

    move-result v0

    add-int v6, v8, v0

    .line 1372
    invoke-virtual {v1, v7, v8, v9, v6}, Landroid/view/View;->layout(IIII)V

    .line 1373
    instance-of v0, v1, Landroid/support/constraint/d;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 1374
    check-cast v0, Landroid/support/constraint/d;

    .line 1375
    invoke-virtual {v0}, Landroid/support/constraint/d;->getContent()Landroid/view/View;

    move-result-object v0

    .line 1376
    if-eqz v0, :cond_1

    .line 1377
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1378
    invoke-virtual {v0, v7, v8, v9, v6}, Landroid/view/View;->layout(IIII)V

    goto :goto_2

    .line 1382
    :cond_3
    return-void
.end method

.method protected onMeasure(II)V
    .locals 20

    .prologue
    .line 1099
    invoke-virtual/range {p0 .. p0}, Landroid/support/constraint/ConstraintLayout;->getPaddingLeft()I

    move-result v2

    .line 1100
    invoke-virtual/range {p0 .. p0}, Landroid/support/constraint/ConstraintLayout;->getPaddingTop()I

    move-result v3

    .line 1102
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {v4, v2}, Landroid/support/constraint/a/a/d;->c(I)V

    .line 1103
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {v4, v3}, Landroid/support/constraint/a/a/d;->d(I)V

    .line 1104
    invoke-direct/range {p0 .. p2}, Landroid/support/constraint/ConstraintLayout;->b(II)V

    .line 1105
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/d;->h()I

    move-result v14

    .line 1106
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/d;->l()I

    move-result v15

    .line 1107
    move-object/from16 v0, p0

    iget-boolean v4, v0, Landroid/support/constraint/ConstraintLayout;->i:Z

    if-eqz v4, :cond_0

    .line 1108
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Landroid/support/constraint/ConstraintLayout;->i:Z

    .line 1109
    invoke-direct/range {p0 .. p0}, Landroid/support/constraint/ConstraintLayout;->c()V

    .line 1111
    :cond_0
    invoke-direct/range {p0 .. p2}, Landroid/support/constraint/ConstraintLayout;->a(II)V

    .line 1120
    invoke-virtual/range {p0 .. p0}, Landroid/support/constraint/ConstraintLayout;->getChildCount()I

    move-result v4

    if-lez v4, :cond_1

    .line 1121
    invoke-virtual/range {p0 .. p0}, Landroid/support/constraint/ConstraintLayout;->a()V

    .line 1123
    :cond_1
    const/4 v12, 0x0

    .line 1126
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/ConstraintLayout;->d:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v16

    .line 1128
    invoke-virtual/range {p0 .. p0}, Landroid/support/constraint/ConstraintLayout;->getPaddingBottom()I

    move-result v4

    add-int v17, v3, v4

    .line 1129
    invoke-virtual/range {p0 .. p0}, Landroid/support/constraint/ConstraintLayout;->getPaddingRight()I

    move-result v3

    add-int v18, v2, v3

    .line 1131
    if-lez v16, :cond_11

    .line 1132
    const/4 v10, 0x0

    .line 1133
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {v2}, Landroid/support/constraint/a/a/d;->B()Landroid/support/constraint/a/a/c$a;

    move-result-object v2

    sget-object v3, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    if-ne v2, v3, :cond_2

    const/4 v2, 0x1

    move v5, v2

    .line 1135
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {v2}, Landroid/support/constraint/a/a/d;->C()Landroid/support/constraint/a/a/c$a;

    move-result-object v2

    sget-object v3, Landroid/support/constraint/a/a/c$a;->b:Landroid/support/constraint/a/a/c$a;

    if-ne v2, v3, :cond_3

    const/4 v2, 0x1

    move v6, v2

    .line 1137
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {v2}, Landroid/support/constraint/a/a/d;->h()I

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/constraint/ConstraintLayout;->e:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v9

    .line 1138
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {v2}, Landroid/support/constraint/a/a/d;->l()I

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/constraint/ConstraintLayout;->f:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v11

    .line 1139
    const/4 v2, 0x0

    move v13, v2

    :goto_2
    move/from16 v0, v16

    if-ge v13, v0, :cond_a

    .line 1140
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/constraint/ConstraintLayout;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/constraint/a/a/c;

    .line 1141
    invoke-virtual {v2}, Landroid/support/constraint/a/a/c;->x()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 1142
    if-nez v3, :cond_4

    move v2, v12

    .line 1139
    :goto_3
    add-int/lit8 v3, v13, 0x1

    move v13, v3

    move v12, v2

    goto :goto_2

    .line 1133
    :cond_2
    const/4 v2, 0x0

    move v5, v2

    goto :goto_0

    .line 1135
    :cond_3
    const/4 v2, 0x0

    move v6, v2

    goto :goto_1

    .line 1145
    :cond_4
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/support/constraint/ConstraintLayout$a;

    .line 1146
    iget-boolean v7, v4, Landroid/support/constraint/ConstraintLayout$a;->U:Z

    if-nez v7, :cond_1a

    iget-boolean v7, v4, Landroid/support/constraint/ConstraintLayout$a;->T:Z

    if-eqz v7, :cond_5

    move v2, v12

    .line 1147
    goto :goto_3

    .line 1149
    :cond_5
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v7

    const/16 v8, 0x8

    if-ne v7, v8, :cond_6

    move v2, v12

    .line 1150
    goto :goto_3

    .line 1156
    :cond_6
    iget v7, v4, Landroid/support/constraint/ConstraintLayout$a;->width:I

    const/4 v8, -0x2

    if-ne v7, v8, :cond_8

    .line 1157
    iget v7, v4, Landroid/support/constraint/ConstraintLayout$a;->width:I

    move/from16 v0, p1

    move/from16 v1, v18

    invoke-static {v0, v1, v7}, Landroid/support/constraint/ConstraintLayout;->getChildMeasureSpec(III)I

    move-result v7

    .line 1161
    :goto_4
    iget v8, v4, Landroid/support/constraint/ConstraintLayout$a;->height:I

    const/16 v19, -0x2

    move/from16 v0, v19

    if-ne v8, v0, :cond_9

    .line 1162
    iget v8, v4, Landroid/support/constraint/ConstraintLayout$a;->height:I

    move/from16 v0, p2

    move/from16 v1, v17

    invoke-static {v0, v1, v8}, Landroid/support/constraint/ConstraintLayout;->getChildMeasureSpec(III)I

    move-result v8

    .line 1168
    :goto_5
    invoke-virtual {v3, v7, v8}, Landroid/view/View;->measure(II)V

    .line 1170
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    .line 1171
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v19

    .line 1172
    invoke-virtual {v2}, Landroid/support/constraint/a/a/c;->h()I

    move-result v8

    if-eq v7, v8, :cond_19

    .line 1173
    invoke-virtual {v2, v7}, Landroid/support/constraint/a/a/c;->e(I)V

    .line 1174
    if-eqz v5, :cond_18

    invoke-virtual {v2}, Landroid/support/constraint/a/a/c;->t()I

    move-result v7

    if-le v7, v9, :cond_18

    .line 1175
    invoke-virtual {v2}, Landroid/support/constraint/a/a/c;->t()I

    move-result v7

    sget-object v8, Landroid/support/constraint/a/a/b$c;->d:Landroid/support/constraint/a/a/b$c;

    .line 1176
    invoke-virtual {v2, v8}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/constraint/a/a/b;->d()I

    move-result v8

    add-int/2addr v7, v8

    .line 1177
    invoke-static {v9, v7}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 1179
    :goto_6
    const/4 v8, 0x1

    .line 1181
    :goto_7
    invoke-virtual {v2}, Landroid/support/constraint/a/a/c;->l()I

    move-result v9

    move/from16 v0, v19

    if-eq v0, v9, :cond_17

    .line 1182
    move/from16 v0, v19

    invoke-virtual {v2, v0}, Landroid/support/constraint/a/a/c;->f(I)V

    .line 1183
    if-eqz v6, :cond_16

    invoke-virtual {v2}, Landroid/support/constraint/a/a/c;->u()I

    move-result v8

    if-le v8, v11, :cond_16

    .line 1184
    invoke-virtual {v2}, Landroid/support/constraint/a/a/c;->u()I

    move-result v8

    sget-object v9, Landroid/support/constraint/a/a/b$c;->e:Landroid/support/constraint/a/a/b$c;

    .line 1185
    invoke-virtual {v2, v9}, Landroid/support/constraint/a/a/c;->a(Landroid/support/constraint/a/a/b$c;)Landroid/support/constraint/a/a/b;

    move-result-object v9

    invoke-virtual {v9}, Landroid/support/constraint/a/a/b;->d()I

    move-result v9

    add-int/2addr v8, v9

    .line 1186
    invoke-static {v11, v8}, Ljava/lang/Math;->max(II)I

    move-result v8

    .line 1188
    :goto_8
    const/4 v9, 0x1

    .line 1190
    :goto_9
    iget-boolean v4, v4, Landroid/support/constraint/ConstraintLayout$a;->S:Z

    if-eqz v4, :cond_7

    .line 1191
    invoke-virtual {v3}, Landroid/view/View;->getBaseline()I

    move-result v4

    .line 1192
    const/4 v10, -0x1

    if-eq v4, v10, :cond_7

    invoke-virtual {v2}, Landroid/support/constraint/a/a/c;->w()I

    move-result v10

    if-eq v4, v10, :cond_7

    .line 1193
    invoke-virtual {v2, v4}, Landroid/support/constraint/a/a/c;->k(I)V

    .line 1194
    const/4 v9, 0x1

    .line 1198
    :cond_7
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v2, v4, :cond_15

    .line 1199
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredState()I

    move-result v2

    invoke-static {v12, v2}, Landroid/support/constraint/ConstraintLayout;->combineMeasuredStates(II)I

    move-result v2

    move v11, v8

    move v10, v9

    move v9, v7

    goto/16 :goto_3

    .line 1159
    :cond_8
    invoke-virtual {v2}, Landroid/support/constraint/a/a/c;->h()I

    move-result v7

    const/high16 v8, 0x40000000    # 2.0f

    invoke-static {v7, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    goto/16 :goto_4

    .line 1164
    :cond_9
    invoke-virtual {v2}, Landroid/support/constraint/a/a/c;->l()I

    move-result v8

    const/high16 v19, 0x40000000    # 2.0f

    move/from16 v0, v19

    invoke-static {v8, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    goto/16 :goto_5

    .line 1202
    :cond_a
    if-eqz v10, :cond_d

    .line 1203
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {v2, v14}, Landroid/support/constraint/a/a/d;->e(I)V

    .line 1204
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {v2, v15}, Landroid/support/constraint/a/a/d;->f(I)V

    .line 1205
    invoke-virtual/range {p0 .. p0}, Landroid/support/constraint/ConstraintLayout;->a()V

    .line 1206
    const/4 v2, 0x0

    .line 1207
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {v3}, Landroid/support/constraint/a/a/d;->h()I

    move-result v3

    if-ge v3, v9, :cond_b

    .line 1208
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {v2, v9}, Landroid/support/constraint/a/a/d;->e(I)V

    .line 1209
    const/4 v2, 0x1

    .line 1211
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {v3}, Landroid/support/constraint/a/a/d;->l()I

    move-result v3

    if-ge v3, v11, :cond_c

    .line 1212
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {v2, v11}, Landroid/support/constraint/a/a/d;->f(I)V

    .line 1213
    const/4 v2, 0x1

    .line 1215
    :cond_c
    if-eqz v2, :cond_d

    .line 1216
    invoke-virtual/range {p0 .. p0}, Landroid/support/constraint/ConstraintLayout;->a()V

    .line 1219
    :cond_d
    const/4 v2, 0x0

    move v4, v2

    :goto_a
    move/from16 v0, v16

    if-ge v4, v0, :cond_11

    .line 1220
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/constraint/ConstraintLayout;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/constraint/a/a/c;

    .line 1221
    invoke-virtual {v2}, Landroid/support/constraint/a/a/c;->x()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 1222
    if-nez v3, :cond_f

    .line 1219
    :cond_e
    :goto_b
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_a

    .line 1225
    :cond_f
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/support/constraint/a/a/c;->h()I

    move-result v6

    if-ne v5, v6, :cond_10

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v5

    invoke-virtual {v2}, Landroid/support/constraint/a/a/c;->l()I

    move-result v6

    if-eq v5, v6, :cond_e

    .line 1226
    :cond_10
    invoke-virtual {v2}, Landroid/support/constraint/a/a/c;->h()I

    move-result v5

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v5, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 1227
    invoke-virtual {v2}, Landroid/support/constraint/a/a/c;->l()I

    move-result v2

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 1228
    invoke-virtual {v3, v5, v2}, Landroid/view/View;->measure(II)V

    goto :goto_b

    .line 1233
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {v2}, Landroid/support/constraint/a/a/d;->h()I

    move-result v2

    add-int v2, v2, v18

    .line 1234
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {v3}, Landroid/support/constraint/a/a/d;->l()I

    move-result v3

    add-int v3, v3, v17

    .line 1236
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xb

    if-lt v4, v5, :cond_14

    .line 1237
    move/from16 v0, p1

    invoke-static {v2, v0, v12}, Landroid/support/constraint/ConstraintLayout;->resolveSizeAndState(III)I

    move-result v2

    .line 1238
    shl-int/lit8 v4, v12, 0x10

    move/from16 v0, p2

    invoke-static {v3, v0, v4}, Landroid/support/constraint/ConstraintLayout;->resolveSizeAndState(III)I

    move-result v3

    .line 1240
    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/constraint/ConstraintLayout;->g:I

    invoke-static {v4, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1241
    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/constraint/ConstraintLayout;->h:I

    invoke-static {v4, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 1242
    const v4, 0xffffff

    and-int/2addr v2, v4

    .line 1243
    const v4, 0xffffff

    and-int/2addr v3, v4

    .line 1244
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/d;->D()Z

    move-result v4

    if-eqz v4, :cond_12

    .line 1245
    const/high16 v4, 0x1000000

    or-int/2addr v2, v4

    .line 1247
    :cond_12
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {v4}, Landroid/support/constraint/a/a/d;->E()Z

    move-result v4

    if-eqz v4, :cond_13

    .line 1248
    const/high16 v4, 0x1000000

    or-int/2addr v3, v4

    .line 1250
    :cond_13
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Landroid/support/constraint/ConstraintLayout;->setMeasuredDimension(II)V

    .line 1254
    :goto_c
    return-void

    .line 1252
    :cond_14
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Landroid/support/constraint/ConstraintLayout;->setMeasuredDimension(II)V

    goto :goto_c

    :cond_15
    move v11, v8

    move v10, v9

    move v2, v12

    move v9, v7

    goto/16 :goto_3

    :cond_16
    move v8, v11

    goto/16 :goto_8

    :cond_17
    move v9, v8

    move v8, v11

    goto/16 :goto_9

    :cond_18
    move v7, v9

    goto/16 :goto_6

    :cond_19
    move v7, v9

    move v8, v10

    goto/16 :goto_7

    :cond_1a
    move v2, v12

    goto/16 :goto_3
.end method

.method public onViewAdded(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 553
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 554
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onViewAdded(Landroid/view/View;)V

    .line 556
    :cond_0
    invoke-virtual {p0, p1}, Landroid/support/constraint/ConstraintLayout;->a(Landroid/view/View;)Landroid/support/constraint/a/a/c;

    move-result-object v0

    .line 557
    instance-of v1, p1, Landroid/support/constraint/Guideline;

    if-eqz v1, :cond_1

    .line 558
    instance-of v0, v0, Landroid/support/constraint/a/a/e;

    if-nez v0, :cond_1

    .line 559
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/constraint/ConstraintLayout$a;

    .line 560
    new-instance v1, Landroid/support/constraint/a/a/e;

    invoke-direct {v1}, Landroid/support/constraint/a/a/e;-><init>()V

    iput-object v1, v0, Landroid/support/constraint/ConstraintLayout$a;->ad:Landroid/support/constraint/a/a/c;

    .line 561
    iput-boolean v2, v0, Landroid/support/constraint/ConstraintLayout$a;->T:Z

    .line 562
    iget-object v1, v0, Landroid/support/constraint/ConstraintLayout$a;->ad:Landroid/support/constraint/a/a/c;

    check-cast v1, Landroid/support/constraint/a/a/e;

    iget v0, v0, Landroid/support/constraint/ConstraintLayout$a;->P:I

    invoke-virtual {v1, v0}, Landroid/support/constraint/a/a/e;->a(I)V

    .line 565
    :cond_1
    instance-of v0, p1, Landroid/support/constraint/a;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 566
    check-cast v0, Landroid/support/constraint/a;

    .line 567
    invoke-virtual {v0}, Landroid/support/constraint/a;->a()V

    .line 568
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/constraint/ConstraintLayout$a;

    .line 569
    iput-boolean v2, v1, Landroid/support/constraint/ConstraintLayout$a;->U:Z

    .line 570
    iget-object v1, p0, Landroid/support/constraint/ConstraintLayout;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 571
    iget-object v1, p0, Landroid/support/constraint/ConstraintLayout;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 574
    :cond_2
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->a:Landroid/util/SparseArray;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 575
    iput-boolean v2, p0, Landroid/support/constraint/ConstraintLayout;->i:Z

    .line 576
    return-void
.end method

.method public onViewRemoved(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 583
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 584
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onViewRemoved(Landroid/view/View;)V

    .line 586
    :cond_0
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->a:Landroid/util/SparseArray;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->remove(I)V

    .line 587
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {p0, p1}, Landroid/support/constraint/ConstraintLayout;->a(Landroid/view/View;)Landroid/support/constraint/a/a/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/constraint/a/a/d;->c(Landroid/support/constraint/a/a/c;)V

    .line 588
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 589
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/constraint/ConstraintLayout;->i:Z

    .line 590
    return-void
.end method

.method public removeView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 542
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 543
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    .line 544
    invoke-virtual {p0, p1}, Landroid/support/constraint/ConstraintLayout;->onViewRemoved(Landroid/view/View;)V

    .line 546
    :cond_0
    return-void
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 2213
    invoke-super {p0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 2214
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/constraint/ConstraintLayout;->i:Z

    .line 2215
    return-void
.end method

.method public setConstraintSet(Landroid/support/constraint/b;)V
    .locals 0

    .prologue
    .line 1438
    iput-object p1, p0, Landroid/support/constraint/ConstraintLayout;->k:Landroid/support/constraint/b;

    .line 1439
    return-void
.end method

.method public setId(I)V
    .locals 2

    .prologue
    .line 476
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->a:Landroid/util/SparseArray;

    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->remove(I)V

    .line 477
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setId(I)V

    .line 478
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->a:Landroid/util/SparseArray;

    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->getId()I

    move-result v1

    invoke-virtual {v0, v1, p0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 479
    return-void
.end method

.method public setMaxHeight(I)V
    .locals 1

    .prologue
    .line 659
    iget v0, p0, Landroid/support/constraint/ConstraintLayout;->h:I

    if-ne p1, v0, :cond_0

    .line 664
    :goto_0
    return-void

    .line 662
    :cond_0
    iput p1, p0, Landroid/support/constraint/ConstraintLayout;->h:I

    .line 663
    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->requestLayout()V

    goto :goto_0
.end method

.method public setMaxWidth(I)V
    .locals 1

    .prologue
    .line 646
    iget v0, p0, Landroid/support/constraint/ConstraintLayout;->g:I

    if-ne p1, v0, :cond_0

    .line 651
    :goto_0
    return-void

    .line 649
    :cond_0
    iput p1, p0, Landroid/support/constraint/ConstraintLayout;->g:I

    .line 650
    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->requestLayout()V

    goto :goto_0
.end method

.method public setMinHeight(I)V
    .locals 1

    .prologue
    .line 611
    iget v0, p0, Landroid/support/constraint/ConstraintLayout;->f:I

    if-ne p1, v0, :cond_0

    .line 616
    :goto_0
    return-void

    .line 614
    :cond_0
    iput p1, p0, Landroid/support/constraint/ConstraintLayout;->f:I

    .line 615
    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->requestLayout()V

    goto :goto_0
.end method

.method public setMinWidth(I)V
    .locals 1

    .prologue
    .line 598
    iget v0, p0, Landroid/support/constraint/ConstraintLayout;->e:I

    if-ne p1, v0, :cond_0

    .line 603
    :goto_0
    return-void

    .line 601
    :cond_0
    iput p1, p0, Landroid/support/constraint/ConstraintLayout;->e:I

    .line 602
    invoke-virtual {p0}, Landroid/support/constraint/ConstraintLayout;->requestLayout()V

    goto :goto_0
.end method

.method public setOptimizationLevel(I)V
    .locals 1

    .prologue
    .line 1398
    iget-object v0, p0, Landroid/support/constraint/ConstraintLayout;->b:Landroid/support/constraint/a/a/d;

    invoke-virtual {v0, p1}, Landroid/support/constraint/a/a/d;->a(I)V

    .line 1399
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 482
    iput-object p1, p0, Landroid/support/constraint/ConstraintLayout;->l:Ljava/lang/String;

    .line 483
    return-void
.end method
