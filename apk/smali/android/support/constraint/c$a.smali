.class public Landroid/support/constraint/c$a;
.super Landroid/support/constraint/ConstraintLayout$a;
.source "Constraints.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/constraint/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public af:F

.field public ag:Z

.field public ah:F

.field public ai:F

.field public aj:F

.field public ak:F

.field public al:F

.field public am:F

.field public an:F

.field public ao:F

.field public ap:F

.field public aq:F

.field public ar:F


# direct methods
.method public constructor <init>(II)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 81
    invoke-direct {p0, p1, p2}, Landroid/support/constraint/ConstraintLayout$a;-><init>(II)V

    .line 66
    iput v2, p0, Landroid/support/constraint/c$a;->af:F

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/constraint/c$a;->ag:Z

    .line 68
    iput v1, p0, Landroid/support/constraint/c$a;->ah:F

    .line 69
    iput v1, p0, Landroid/support/constraint/c$a;->ai:F

    .line 70
    iput v1, p0, Landroid/support/constraint/c$a;->aj:F

    .line 71
    iput v1, p0, Landroid/support/constraint/c$a;->ak:F

    .line 72
    iput v2, p0, Landroid/support/constraint/c$a;->al:F

    .line 73
    iput v2, p0, Landroid/support/constraint/c$a;->am:F

    .line 74
    iput v1, p0, Landroid/support/constraint/c$a;->an:F

    .line 75
    iput v1, p0, Landroid/support/constraint/c$a;->ao:F

    .line 76
    iput v1, p0, Landroid/support/constraint/c$a;->ap:F

    .line 77
    iput v1, p0, Landroid/support/constraint/c$a;->aq:F

    .line 78
    iput v1, p0, Landroid/support/constraint/c$a;->ar:F

    .line 82
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 89
    invoke-direct {p0, p1, p2}, Landroid/support/constraint/ConstraintLayout$a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 66
    iput v2, p0, Landroid/support/constraint/c$a;->af:F

    .line 67
    iput-boolean v0, p0, Landroid/support/constraint/c$a;->ag:Z

    .line 68
    iput v1, p0, Landroid/support/constraint/c$a;->ah:F

    .line 69
    iput v1, p0, Landroid/support/constraint/c$a;->ai:F

    .line 70
    iput v1, p0, Landroid/support/constraint/c$a;->aj:F

    .line 71
    iput v1, p0, Landroid/support/constraint/c$a;->ak:F

    .line 72
    iput v2, p0, Landroid/support/constraint/c$a;->al:F

    .line 73
    iput v2, p0, Landroid/support/constraint/c$a;->am:F

    .line 74
    iput v1, p0, Landroid/support/constraint/c$a;->an:F

    .line 75
    iput v1, p0, Landroid/support/constraint/c$a;->ao:F

    .line 76
    iput v1, p0, Landroid/support/constraint/c$a;->ap:F

    .line 77
    iput v1, p0, Landroid/support/constraint/c$a;->aq:F

    .line 78
    iput v1, p0, Landroid/support/constraint/c$a;->ar:F

    .line 90
    sget-object v1, Landroid/support/constraint/e$b;->ConstraintSet:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 91
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v2

    .line 92
    :goto_0
    if-ge v0, v2, :cond_c

    .line 93
    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v3

    .line 94
    sget v4, Landroid/support/constraint/e$b;->ConstraintSet_android_alpha:I

    if-ne v3, v4, :cond_1

    .line 95
    iget v4, p0, Landroid/support/constraint/c$a;->af:F

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    iput v3, p0, Landroid/support/constraint/c$a;->af:F

    .line 92
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 96
    :cond_1
    sget v4, Landroid/support/constraint/e$b;->ConstraintSet_android_elevation:I

    if-ne v3, v4, :cond_2

    .line 97
    iget v4, p0, Landroid/support/constraint/c$a;->ah:F

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    iput v3, p0, Landroid/support/constraint/c$a;->ah:F

    .line 98
    const/4 v3, 0x1

    iput-boolean v3, p0, Landroid/support/constraint/c$a;->ag:Z

    goto :goto_1

    .line 99
    :cond_2
    sget v4, Landroid/support/constraint/e$b;->ConstraintSet_android_rotationX:I

    if-ne v3, v4, :cond_3

    .line 100
    iget v4, p0, Landroid/support/constraint/c$a;->aj:F

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    iput v3, p0, Landroid/support/constraint/c$a;->aj:F

    goto :goto_1

    .line 101
    :cond_3
    sget v4, Landroid/support/constraint/e$b;->ConstraintSet_android_rotationY:I

    if-ne v3, v4, :cond_4

    .line 102
    iget v4, p0, Landroid/support/constraint/c$a;->ak:F

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    iput v3, p0, Landroid/support/constraint/c$a;->ak:F

    goto :goto_1

    .line 103
    :cond_4
    sget v4, Landroid/support/constraint/e$b;->ConstraintSet_android_rotation:I

    if-ne v3, v4, :cond_5

    .line 104
    iget v4, p0, Landroid/support/constraint/c$a;->ai:F

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    iput v3, p0, Landroid/support/constraint/c$a;->ai:F

    goto :goto_1

    .line 105
    :cond_5
    sget v4, Landroid/support/constraint/e$b;->ConstraintSet_android_scaleX:I

    if-ne v3, v4, :cond_6

    .line 106
    iget v4, p0, Landroid/support/constraint/c$a;->al:F

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    iput v3, p0, Landroid/support/constraint/c$a;->al:F

    goto :goto_1

    .line 107
    :cond_6
    sget v4, Landroid/support/constraint/e$b;->ConstraintSet_android_scaleY:I

    if-ne v3, v4, :cond_7

    .line 108
    iget v4, p0, Landroid/support/constraint/c$a;->am:F

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    iput v3, p0, Landroid/support/constraint/c$a;->am:F

    goto :goto_1

    .line 109
    :cond_7
    sget v4, Landroid/support/constraint/e$b;->ConstraintSet_android_transformPivotX:I

    if-ne v3, v4, :cond_8

    .line 110
    iget v4, p0, Landroid/support/constraint/c$a;->an:F

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    iput v3, p0, Landroid/support/constraint/c$a;->an:F

    goto :goto_1

    .line 111
    :cond_8
    sget v4, Landroid/support/constraint/e$b;->ConstraintSet_android_transformPivotY:I

    if-ne v3, v4, :cond_9

    .line 112
    iget v4, p0, Landroid/support/constraint/c$a;->ao:F

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    iput v3, p0, Landroid/support/constraint/c$a;->ao:F

    goto :goto_1

    .line 113
    :cond_9
    sget v4, Landroid/support/constraint/e$b;->ConstraintSet_android_translationX:I

    if-ne v3, v4, :cond_a

    .line 114
    iget v4, p0, Landroid/support/constraint/c$a;->ap:F

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    iput v3, p0, Landroid/support/constraint/c$a;->ap:F

    goto :goto_1

    .line 115
    :cond_a
    sget v4, Landroid/support/constraint/e$b;->ConstraintSet_android_translationY:I

    if-ne v3, v4, :cond_b

    .line 116
    iget v4, p0, Landroid/support/constraint/c$a;->aq:F

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    iput v3, p0, Landroid/support/constraint/c$a;->aq:F

    goto/16 :goto_1

    .line 117
    :cond_b
    sget v4, Landroid/support/constraint/e$b;->ConstraintSet_android_translationZ:I

    if-ne v3, v4, :cond_0

    .line 118
    iget v4, p0, Landroid/support/constraint/c$a;->ar:F

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    iput v3, p0, Landroid/support/constraint/c$a;->ap:F

    goto/16 :goto_1

    .line 121
    :cond_c
    return-void
.end method
