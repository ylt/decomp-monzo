.class public final Lc/m;
.super Ljava/util/AbstractList;
.source "Options.java"

# interfaces
.implements Ljava/util/RandomAccess;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractList",
        "<",
        "Lc/f;",
        ">;",
        "Ljava/util/RandomAccess;"
    }
.end annotation


# instance fields
.field final a:[Lc/f;


# direct methods
.method private constructor <init>([Lc/f;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 26
    iput-object p1, p0, Lc/m;->a:[Lc/f;

    .line 27
    return-void
.end method

.method public static varargs a([Lc/f;)Lc/m;
    .locals 2

    .prologue
    .line 30
    new-instance v1, Lc/m;

    invoke-virtual {p0}, [Lc/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lc/f;

    invoke-direct {v1, v0}, Lc/m;-><init>([Lc/f;)V

    return-object v1
.end method


# virtual methods
.method public a(I)Lc/f;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lc/m;->a:[Lc/f;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public synthetic get(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lc/m;->a(I)Lc/f;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lc/m;->a:[Lc/f;

    array-length v0, v0

    return v0
.end method
