.class public La/a/a/a/a/b;
.super Landroid/support/v7/widget/RecyclerView$g;
.source "StickyHeaderDecoration.java"


# instance fields
.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Landroid/support/v7/widget/RecyclerView$w;",
            ">;"
        }
    .end annotation
.end field

.field private b:La/a/a/a/a/a;

.field private c:Z


# direct methods
.method public constructor <init>(La/a/a/a/a/a;)V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, La/a/a/a/a/b;-><init>(La/a/a/a/a/a;Z)V

    .line 47
    return-void
.end method

.method public constructor <init>(La/a/a/a/a/a;Z)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$g;-><init>()V

    .line 54
    iput-object p1, p0, La/a/a/a/a/b;->b:La/a/a/a/a/a;

    .line 55
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, La/a/a/a/a/b;->a:Ljava/util/Map;

    .line 56
    iput-boolean p2, p0, La/a/a/a/a/b;->c:Z

    .line 57
    return-void
.end method

.method private a(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;Landroid/view/View;II)I
    .locals 10

    .prologue
    .line 177
    invoke-direct {p0, p3}, La/a/a/a/a/b;->a(Landroid/view/View;)I

    move-result v2

    .line 178
    invoke-virtual {p2}, Landroid/view/View;->getY()F

    move-result v0

    float-to-int v0, v0

    sub-int/2addr v0, v2

    .line 179
    if-nez p5, :cond_0

    .line 180
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v3

    .line 181
    iget-object v1, p0, La/a/a/a/a/b;->b:La/a/a/a/a/a;

    invoke-interface {v1, p4}, La/a/a/a/a/a;->a(I)J

    move-result-wide v4

    .line 183
    const/4 v1, 0x1

    :goto_0
    if-ge v1, v3, :cond_2

    .line 184
    invoke-virtual {p1, v1}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {p1, v6}, Landroid/support/v7/widget/RecyclerView;->f(Landroid/view/View;)I

    move-result v6

    .line 185
    const/4 v7, -0x1

    if-eq v6, v7, :cond_1

    .line 186
    iget-object v7, p0, La/a/a/a/a/b;->b:La/a/a/a/a/a;

    invoke-interface {v7, v6}, La/a/a/a/a/a;->a(I)J

    move-result-wide v8

    .line 187
    cmp-long v7, v8, v4

    if-eqz v7, :cond_1

    .line 188
    invoke-virtual {p1, v1}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 189
    invoke-virtual {v1}, Landroid/view/View;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-direct {p0, p1, v6}, La/a/a/a/a/b;->a(Landroid/support/v7/widget/RecyclerView;I)Landroid/support/v7/widget/RecyclerView$w;

    move-result-object v3

    iget-object v3, v3, Landroid/support/v7/widget/RecyclerView$w;->itemView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    sub-int/2addr v1, v2

    .line 190
    if-gez v1, :cond_2

    move v0, v1

    .line 202
    :cond_0
    :goto_1
    return v0

    .line 183
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 199
    :cond_2
    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_1
.end method

.method private a(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 206
    iget-boolean v0, p0, La/a/a/a/a/b;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    goto :goto_0
.end method

.method private a(Landroid/support/v7/widget/RecyclerView;I)Landroid/support/v7/widget/RecyclerView$w;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 115
    iget-object v0, p0, La/a/a/a/a/b;->b:La/a/a/a/a/a;

    invoke-interface {v0, p2}, La/a/a/a/a/a;->a(I)J

    move-result-wide v2

    .line 117
    iget-object v0, p0, La/a/a/a/a/b;->a:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, La/a/a/a/a/b;->a:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$w;

    .line 139
    :goto_0
    return-object v0

    .line 120
    :cond_0
    iget-object v0, p0, La/a/a/a/a/b;->b:La/a/a/a/a/a;

    invoke-interface {v0, p1}, La/a/a/a/a/a;->a(Landroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$w;

    move-result-object v0

    .line 121
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView$w;->itemView:Landroid/view/View;

    .line 124
    iget-object v4, p0, La/a/a/a/a/b;->b:La/a/a/a/a/a;

    invoke-interface {v4, v0, p2}, La/a/a/a/a/a;->a(Landroid/support/v7/widget/RecyclerView$w;I)V

    .line 126
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 127
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v5

    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 130
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v6

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    iget v7, v7, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 129
    invoke-static {v4, v6, v7}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v4

    .line 132
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v6

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    iget v7, v7, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 131
    invoke-static {v5, v6, v7}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v5

    .line 134
    invoke-virtual {v1, v4, v5}, Landroid/view/View;->measure(II)V

    .line 135
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    invoke-virtual {v1, v8, v8, v4, v5}, Landroid/view/View;->layout(IIII)V

    .line 137
    iget-object v1, p0, La/a/a/a/a/b;->a:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private a(I)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 79
    if-nez p1, :cond_1

    .line 82
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, La/a/a/a/a/b;->b:La/a/a/a/a/a;

    add-int/lit8 v2, p1, -0x1

    invoke-interface {v1, v2}, La/a/a/a/a/a;->a(I)J

    move-result-wide v2

    iget-object v1, p0, La/a/a/a/a/b;->b:La/a/a/a/a/a;

    invoke-interface {v1, p1}, La/a/a/a/a/a;->a(I)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(I)Z
    .locals 4

    .prologue
    .line 111
    iget-object v0, p0, La/a/a/a/a/b;->b:La/a/a/a/a/a;

    invoke-interface {v0, p1}, La/a/a/a/a/a;->a(I)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$t;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 64
    invoke-virtual {p3, p2}, Landroid/support/v7/widget/RecyclerView;->f(Landroid/view/View;)I

    move-result v0

    .line 67
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 68
    invoke-direct {p0, v0}, La/a/a/a/a/b;->b(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 69
    invoke-direct {p0, v0}, La/a/a/a/a/b;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 71
    invoke-direct {p0, p3, v0}, La/a/a/a/a/b;->a(Landroid/support/v7/widget/RecyclerView;I)Landroid/support/v7/widget/RecyclerView$w;

    move-result-object v0

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$w;->itemView:Landroid/view/View;

    .line 72
    invoke-direct {p0, v0}, La/a/a/a/a/b;->a(Landroid/view/View;)I

    move-result v0

    .line 75
    :goto_0
    invoke-virtual {p1, v1, v0, v1, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 76
    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public onDrawOver(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$t;)V
    .locals 10

    .prologue
    .line 148
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v8

    .line 149
    const-wide/16 v0, -0x1

    .line 151
    const/4 v5, 0x0

    :goto_0
    if-ge v5, v8, :cond_1

    .line 152
    invoke-virtual {p2, v5}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 153
    invoke-virtual {p2, v2}, Landroid/support/v7/widget/RecyclerView;->f(Landroid/view/View;)I

    move-result v4

    .line 155
    const/4 v3, -0x1

    if-eq v4, v3, :cond_0

    invoke-direct {p0, v4}, La/a/a/a/a/b;->b(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 156
    iget-object v3, p0, La/a/a/a/a/b;->b:La/a/a/a/a/a;

    invoke-interface {v3, v4}, La/a/a/a/a/a;->a(I)J

    move-result-wide v6

    .line 158
    cmp-long v3, v6, v0

    if-eqz v3, :cond_0

    .line 160
    invoke-direct {p0, p2, v4}, La/a/a/a/a/b;->a(Landroid/support/v7/widget/RecyclerView;I)Landroid/support/v7/widget/RecyclerView$w;

    move-result-object v0

    iget-object v3, v0, Landroid/support/v7/widget/RecyclerView$w;->itemView:Landroid/view/View;

    .line 161
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 163
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v9

    move-object v0, p0

    move-object v1, p2

    .line 164
    invoke-direct/range {v0 .. v5}, La/a/a/a/a/b;->a(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;Landroid/view/View;II)I

    move-result v0

    .line 165
    int-to-float v1, v9

    int-to-float v2, v0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 167
    int-to-float v1, v9

    invoke-virtual {v3, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 168
    int-to-float v0, v0

    invoke-virtual {v3, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 169
    invoke-virtual {v3, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 170
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    move-wide v0, v6

    .line 151
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 174
    :cond_1
    return-void
.end method
