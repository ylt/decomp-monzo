.class public final Lcom/google/gson/g;
.super Ljava/lang/Object;
.source "GsonBuilder.java"


# instance fields
.field private a:Lcom/google/gson/b/d;

.field private b:Lcom/google/gson/r;

.field private c:Lcom/google/gson/e;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/reflect/Type;",
            "Lcom/google/gson/h",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/gson/t;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/gson/t;",
            ">;"
        }
    .end annotation
.end field

.field private g:Z

.field private h:Ljava/lang/String;

.field private i:I

.field private j:I

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    sget-object v0, Lcom/google/gson/b/d;->a:Lcom/google/gson/b/d;

    iput-object v0, p0, Lcom/google/gson/g;->a:Lcom/google/gson/b/d;

    .line 80
    sget-object v0, Lcom/google/gson/r;->a:Lcom/google/gson/r;

    iput-object v0, p0, Lcom/google/gson/g;->b:Lcom/google/gson/r;

    .line 81
    sget-object v0, Lcom/google/gson/d;->a:Lcom/google/gson/d;

    iput-object v0, p0, Lcom/google/gson/g;->c:Lcom/google/gson/e;

    .line 82
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/gson/g;->d:Ljava/util/Map;

    .line 84
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/gson/g;->e:Ljava/util/List;

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/gson/g;->f:Ljava/util/List;

    .line 87
    iput-boolean v1, p0, Lcom/google/gson/g;->g:Z

    .line 89
    iput v2, p0, Lcom/google/gson/g;->i:I

    .line 90
    iput v2, p0, Lcom/google/gson/g;->j:I

    .line 91
    iput-boolean v1, p0, Lcom/google/gson/g;->k:Z

    .line 92
    iput-boolean v1, p0, Lcom/google/gson/g;->l:Z

    .line 93
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/gson/g;->m:Z

    .line 94
    iput-boolean v1, p0, Lcom/google/gson/g;->n:Z

    .line 95
    iput-boolean v1, p0, Lcom/google/gson/g;->o:Z

    .line 96
    iput-boolean v1, p0, Lcom/google/gson/g;->p:Z

    .line 105
    return-void
.end method

.method private a(Ljava/lang/String;IILjava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/List",
            "<",
            "Lcom/google/gson/t;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 581
    if-eqz p1, :cond_1

    const-string v0, ""

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 582
    new-instance v2, Lcom/google/gson/a;

    const-class v0, Ljava/util/Date;

    invoke-direct {v2, v0, p1}, Lcom/google/gson/a;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    .line 583
    new-instance v1, Lcom/google/gson/a;

    const-class v0, Ljava/sql/Timestamp;

    invoke-direct {v1, v0, p1}, Lcom/google/gson/a;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    .line 584
    new-instance v0, Lcom/google/gson/a;

    const-class v3, Ljava/sql/Date;

    invoke-direct {v0, v3, p1}, Lcom/google/gson/a;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    .line 593
    :goto_0
    const-class v3, Ljava/util/Date;

    invoke-static {v3, v2}, Lcom/google/gson/b/a/n;->a(Ljava/lang/Class;Lcom/google/gson/s;)Lcom/google/gson/t;

    move-result-object v2

    invoke-interface {p4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 594
    const-class v2, Ljava/sql/Timestamp;

    invoke-static {v2, v1}, Lcom/google/gson/b/a/n;->a(Ljava/lang/Class;Lcom/google/gson/s;)Lcom/google/gson/t;

    move-result-object v1

    invoke-interface {p4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 595
    const-class v1, Ljava/sql/Date;

    invoke-static {v1, v0}, Lcom/google/gson/b/a/n;->a(Ljava/lang/Class;Lcom/google/gson/s;)Lcom/google/gson/t;

    move-result-object v0

    invoke-interface {p4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 596
    :cond_0
    return-void

    .line 585
    :cond_1
    if-eq p2, v2, :cond_0

    if-eq p3, v2, :cond_0

    .line 586
    new-instance v2, Lcom/google/gson/a;

    const-class v0, Ljava/util/Date;

    invoke-direct {v2, v0, p2, p3}, Lcom/google/gson/a;-><init>(Ljava/lang/Class;II)V

    .line 587
    new-instance v1, Lcom/google/gson/a;

    const-class v0, Ljava/sql/Timestamp;

    invoke-direct {v1, v0, p2, p3}, Lcom/google/gson/a;-><init>(Ljava/lang/Class;II)V

    .line 588
    new-instance v0, Lcom/google/gson/a;

    const-class v3, Ljava/sql/Date;

    invoke-direct {v0, v3, p2, p3}, Lcom/google/gson/a;-><init>(Ljava/lang/Class;II)V

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/google/gson/f;
    .locals 13

    .prologue
    .line 562
    new-instance v12, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/gson/g;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/gson/g;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x3

    invoke-direct {v12, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 563
    iget-object v0, p0, Lcom/google/gson/g;->e:Ljava/util/List;

    invoke-interface {v12, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 564
    invoke-static {v12}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 565
    iget-object v0, p0, Lcom/google/gson/g;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 566
    iget-object v0, p0, Lcom/google/gson/g;->f:Ljava/util/List;

    invoke-interface {v12, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 567
    iget-object v0, p0, Lcom/google/gson/g;->h:Ljava/lang/String;

    iget v1, p0, Lcom/google/gson/g;->i:I

    iget v2, p0, Lcom/google/gson/g;->j:I

    invoke-direct {p0, v0, v1, v2, v12}, Lcom/google/gson/g;->a(Ljava/lang/String;IILjava/util/List;)V

    .line 569
    new-instance v0, Lcom/google/gson/f;

    iget-object v1, p0, Lcom/google/gson/g;->a:Lcom/google/gson/b/d;

    iget-object v2, p0, Lcom/google/gson/g;->c:Lcom/google/gson/e;

    iget-object v3, p0, Lcom/google/gson/g;->d:Ljava/util/Map;

    iget-boolean v4, p0, Lcom/google/gson/g;->g:Z

    iget-boolean v5, p0, Lcom/google/gson/g;->k:Z

    iget-boolean v6, p0, Lcom/google/gson/g;->o:Z

    iget-boolean v7, p0, Lcom/google/gson/g;->m:Z

    iget-boolean v8, p0, Lcom/google/gson/g;->n:Z

    iget-boolean v9, p0, Lcom/google/gson/g;->p:Z

    iget-boolean v10, p0, Lcom/google/gson/g;->l:Z

    iget-object v11, p0, Lcom/google/gson/g;->b:Lcom/google/gson/r;

    invoke-direct/range {v0 .. v12}, Lcom/google/gson/f;-><init>(Lcom/google/gson/b/d;Lcom/google/gson/e;Ljava/util/Map;ZZZZZZZLcom/google/gson/r;Ljava/util/List;)V

    return-object v0
.end method

.method public a(Lcom/google/gson/d;)Lcom/google/gson/g;
    .locals 0

    .prologue
    .line 286
    iput-object p1, p0, Lcom/google/gson/g;->c:Lcom/google/gson/e;

    .line 287
    return-object p0
.end method

.method public a(Lcom/google/gson/t;)Lcom/google/gson/g;
    .locals 1

    .prologue
    .line 498
    iget-object v0, p0, Lcom/google/gson/g;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 499
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/google/gson/g;
    .locals 0

    .prologue
    .line 408
    iput-object p1, p0, Lcom/google/gson/g;->h:Ljava/lang/String;

    .line 409
    return-object p0
.end method

.method public a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/g;
    .locals 2

    .prologue
    .line 472
    instance-of v0, p2, Lcom/google/gson/q;

    if-nez v0, :cond_0

    instance-of v0, p2, Lcom/google/gson/k;

    if-nez v0, :cond_0

    instance-of v0, p2, Lcom/google/gson/h;

    if-nez v0, :cond_0

    instance-of v0, p2, Lcom/google/gson/s;

    if-eqz v0, :cond_5

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/gson/b/a;->a(Z)V

    .line 476
    instance-of v0, p2, Lcom/google/gson/h;

    if-eqz v0, :cond_1

    .line 477
    iget-object v1, p0, Lcom/google/gson/g;->d:Ljava/util/Map;

    move-object v0, p2

    check-cast v0, Lcom/google/gson/h;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 479
    :cond_1
    instance-of v0, p2, Lcom/google/gson/q;

    if-nez v0, :cond_2

    instance-of v0, p2, Lcom/google/gson/k;

    if-eqz v0, :cond_3

    .line 480
    :cond_2
    invoke-static {p1}, Lcom/google/gson/c/a;->a(Ljava/lang/reflect/Type;)Lcom/google/gson/c/a;

    move-result-object v0

    .line 481
    iget-object v1, p0, Lcom/google/gson/g;->e:Ljava/util/List;

    invoke-static {v0, p2}, Lcom/google/gson/b/a/l;->a(Lcom/google/gson/c/a;Ljava/lang/Object;)Lcom/google/gson/t;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 483
    :cond_3
    instance-of v0, p2, Lcom/google/gson/s;

    if-eqz v0, :cond_4

    .line 484
    iget-object v0, p0, Lcom/google/gson/g;->e:Ljava/util/List;

    invoke-static {p1}, Lcom/google/gson/c/a;->a(Ljava/lang/reflect/Type;)Lcom/google/gson/c/a;

    move-result-object v1

    check-cast p2, Lcom/google/gson/s;

    invoke-static {v1, p2}, Lcom/google/gson/b/a/n;->a(Lcom/google/gson/c/a;Lcom/google/gson/s;)Lcom/google/gson/t;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 486
    :cond_4
    return-object p0

    .line 472
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method
