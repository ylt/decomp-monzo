.class public final Lcom/google/android/exoplayer2/upstream/g;
.super Ljava/lang/Object;
.source "DefaultDataSource.java"

# interfaces
.implements Lcom/google/android/exoplayer2/upstream/c;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/exoplayer2/upstream/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/exoplayer2/upstream/l",
            "<-",
            "Lcom/google/android/exoplayer2/upstream/c;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/android/exoplayer2/upstream/c;

.field private d:Lcom/google/android/exoplayer2/upstream/c;

.field private e:Lcom/google/android/exoplayer2/upstream/c;

.field private f:Lcom/google/android/exoplayer2/upstream/c;

.field private g:Lcom/google/android/exoplayer2/upstream/c;

.field private h:Lcom/google/android/exoplayer2/upstream/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/exoplayer2/upstream/l;Lcom/google/android/exoplayer2/upstream/c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/exoplayer2/upstream/l",
            "<-",
            "Lcom/google/android/exoplayer2/upstream/c;",
            ">;",
            "Lcom/google/android/exoplayer2/upstream/c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->a:Landroid/content/Context;

    .line 112
    iput-object p2, p0, Lcom/google/android/exoplayer2/upstream/g;->b:Lcom/google/android/exoplayer2/upstream/l;

    .line 113
    invoke-static {p3}, Lcom/google/android/exoplayer2/util/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/upstream/c;

    iput-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->c:Lcom/google/android/exoplayer2/upstream/c;

    .line 114
    return-void
.end method

.method private c()Lcom/google/android/exoplayer2/upstream/c;
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->d:Lcom/google/android/exoplayer2/upstream/c;

    if-nez v0, :cond_0

    .line 163
    new-instance v0, Lcom/google/android/exoplayer2/upstream/FileDataSource;

    iget-object v1, p0, Lcom/google/android/exoplayer2/upstream/g;->b:Lcom/google/android/exoplayer2/upstream/l;

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/upstream/FileDataSource;-><init>(Lcom/google/android/exoplayer2/upstream/l;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->d:Lcom/google/android/exoplayer2/upstream/c;

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->d:Lcom/google/android/exoplayer2/upstream/c;

    return-object v0
.end method

.method private d()Lcom/google/android/exoplayer2/upstream/c;
    .locals 3

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->e:Lcom/google/android/exoplayer2/upstream/c;

    if-nez v0, :cond_0

    .line 170
    new-instance v0, Lcom/google/android/exoplayer2/upstream/AssetDataSource;

    iget-object v1, p0, Lcom/google/android/exoplayer2/upstream/g;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/exoplayer2/upstream/g;->b:Lcom/google/android/exoplayer2/upstream/l;

    invoke-direct {v0, v1, v2}, Lcom/google/android/exoplayer2/upstream/AssetDataSource;-><init>(Landroid/content/Context;Lcom/google/android/exoplayer2/upstream/l;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->e:Lcom/google/android/exoplayer2/upstream/c;

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->e:Lcom/google/android/exoplayer2/upstream/c;

    return-object v0
.end method

.method private e()Lcom/google/android/exoplayer2/upstream/c;
    .locals 3

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->f:Lcom/google/android/exoplayer2/upstream/c;

    if-nez v0, :cond_0

    .line 177
    new-instance v0, Lcom/google/android/exoplayer2/upstream/ContentDataSource;

    iget-object v1, p0, Lcom/google/android/exoplayer2/upstream/g;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/exoplayer2/upstream/g;->b:Lcom/google/android/exoplayer2/upstream/l;

    invoke-direct {v0, v1, v2}, Lcom/google/android/exoplayer2/upstream/ContentDataSource;-><init>(Landroid/content/Context;Lcom/google/android/exoplayer2/upstream/l;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->f:Lcom/google/android/exoplayer2/upstream/c;

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->f:Lcom/google/android/exoplayer2/upstream/c;

    return-object v0
.end method

.method private f()Lcom/google/android/exoplayer2/upstream/c;
    .locals 3

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->g:Lcom/google/android/exoplayer2/upstream/c;

    if-nez v0, :cond_0

    .line 185
    :try_start_0
    const-string v0, "com.google.android.exoplayer2.ext.rtmp.RtmpDataSource"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 186
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/upstream/c;

    iput-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->g:Lcom/google/android/exoplayer2/upstream/c;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4

    .line 198
    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->g:Lcom/google/android/exoplayer2/upstream/c;

    if-nez v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->c:Lcom/google/android/exoplayer2/upstream/c;

    iput-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->g:Lcom/google/android/exoplayer2/upstream/c;

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->g:Lcom/google/android/exoplayer2/upstream/c;

    return-object v0

    .line 187
    :catch_0
    move-exception v0

    .line 188
    const-string v0, "DefaultDataSource"

    const-string v1, "Attempting to play RTMP stream without depending on the RTMP extension"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 189
    :catch_1
    move-exception v0

    .line 190
    const-string v1, "DefaultDataSource"

    const-string v2, "Error instantiating RtmpDataSource"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 191
    :catch_2
    move-exception v0

    .line 192
    const-string v1, "DefaultDataSource"

    const-string v2, "Error instantiating RtmpDataSource"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 193
    :catch_3
    move-exception v0

    .line 194
    const-string v1, "DefaultDataSource"

    const-string v2, "Error instantiating RtmpDataSource"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 195
    :catch_4
    move-exception v0

    .line 196
    const-string v1, "DefaultDataSource"

    const-string v2, "Error instantiating RtmpDataSource"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public a([BII)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->h:Lcom/google/android/exoplayer2/upstream/c;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/exoplayer2/upstream/c;->a([BII)I

    move-result v0

    return v0
.end method

.method public a(Lcom/google/android/exoplayer2/upstream/e;)J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->h:Lcom/google/android/exoplayer2/upstream/c;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 120
    iget-object v0, p1, Lcom/google/android/exoplayer2/upstream/e;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 121
    iget-object v1, p1, Lcom/google/android/exoplayer2/upstream/e;->a:Landroid/net/Uri;

    invoke-static {v1}, Lcom/google/android/exoplayer2/util/s;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 122
    iget-object v0, p1, Lcom/google/android/exoplayer2/upstream/e;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/android_asset/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 123
    invoke-direct {p0}, Lcom/google/android/exoplayer2/upstream/g;->d()Lcom/google/android/exoplayer2/upstream/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->h:Lcom/google/android/exoplayer2/upstream/c;

    .line 137
    :goto_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->h:Lcom/google/android/exoplayer2/upstream/c;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/upstream/c;->a(Lcom/google/android/exoplayer2/upstream/e;)J

    move-result-wide v0

    return-wide v0

    .line 118
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 125
    :cond_1
    invoke-direct {p0}, Lcom/google/android/exoplayer2/upstream/g;->c()Lcom/google/android/exoplayer2/upstream/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->h:Lcom/google/android/exoplayer2/upstream/c;

    goto :goto_1

    .line 127
    :cond_2
    const-string v1, "asset"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 128
    invoke-direct {p0}, Lcom/google/android/exoplayer2/upstream/g;->d()Lcom/google/android/exoplayer2/upstream/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->h:Lcom/google/android/exoplayer2/upstream/c;

    goto :goto_1

    .line 129
    :cond_3
    const-string v1, "content"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 130
    invoke-direct {p0}, Lcom/google/android/exoplayer2/upstream/g;->e()Lcom/google/android/exoplayer2/upstream/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->h:Lcom/google/android/exoplayer2/upstream/c;

    goto :goto_1

    .line 131
    :cond_4
    const-string v1, "rtmp"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 132
    invoke-direct {p0}, Lcom/google/android/exoplayer2/upstream/g;->f()Lcom/google/android/exoplayer2/upstream/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->h:Lcom/google/android/exoplayer2/upstream/c;

    goto :goto_1

    .line 134
    :cond_5
    iget-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->c:Lcom/google/android/exoplayer2/upstream/c;

    iput-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->h:Lcom/google/android/exoplayer2/upstream/c;

    goto :goto_1
.end method

.method public a()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->h:Lcom/google/android/exoplayer2/upstream/c;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->h:Lcom/google/android/exoplayer2/upstream/c;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/upstream/c;->a()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public b()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 152
    iget-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->h:Lcom/google/android/exoplayer2/upstream/c;

    if-eqz v0, :cond_0

    .line 154
    :try_start_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/upstream/g;->h:Lcom/google/android/exoplayer2/upstream/c;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/upstream/c;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    iput-object v1, p0, Lcom/google/android/exoplayer2/upstream/g;->h:Lcom/google/android/exoplayer2/upstream/c;

    .line 159
    :cond_0
    return-void

    .line 156
    :catchall_0
    move-exception v0

    iput-object v1, p0, Lcom/google/android/exoplayer2/upstream/g;->h:Lcom/google/android/exoplayer2/upstream/c;

    throw v0
.end method
