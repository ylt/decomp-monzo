.class public final Lcom/google/android/exoplayer2/drm/a;
.super Ljava/lang/Object;
.source "DrmInitData.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/drm/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/exoplayer2/drm/a$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/exoplayer2/drm/a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field private final b:[Lcom/google/android/exoplayer2/drm/a$a;

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 170
    new-instance v0, Lcom/google/android/exoplayer2/drm/a$1;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/drm/a$1;-><init>()V

    sput-object v0, Lcom/google/android/exoplayer2/drm/a;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    sget-object v0, Lcom/google/android/exoplayer2/drm/a$a;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/exoplayer2/drm/a$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/drm/a;->b:[Lcom/google/android/exoplayer2/drm/a$a;

    .line 78
    iget-object v0, p0, Lcom/google/android/exoplayer2/drm/a;->b:[Lcom/google/android/exoplayer2/drm/a$a;

    array-length v0, v0

    iput v0, p0, Lcom/google/android/exoplayer2/drm/a;->a:I

    .line 79
    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer2/drm/a$a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 49
    const/4 v1, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/exoplayer2/drm/a$a;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/exoplayer2/drm/a$a;

    invoke-direct {p0, v1, v0}, Lcom/google/android/exoplayer2/drm/a;-><init>(Z[Lcom/google/android/exoplayer2/drm/a$a;)V

    .line 50
    return-void
.end method

.method private varargs constructor <init>(Z[Lcom/google/android/exoplayer2/drm/a$a;)V
    .locals 5

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    if-eqz p1, :cond_2

    .line 61
    invoke-virtual {p2}, [Lcom/google/android/exoplayer2/drm/a$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/exoplayer2/drm/a$a;

    .line 65
    :goto_0
    invoke-static {v0, p0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 67
    const/4 v1, 0x1

    :goto_1
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 68
    add-int/lit8 v2, v1, -0x1

    aget-object v2, v0, v2

    invoke-static {v2}, Lcom/google/android/exoplayer2/drm/a$a;->a(Lcom/google/android/exoplayer2/drm/a$a;)Ljava/util/UUID;

    move-result-object v2

    aget-object v3, v0, v1

    invoke-static {v3}, Lcom/google/android/exoplayer2/drm/a$a;->a(Lcom/google/android/exoplayer2/drm/a$a;)Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 69
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Duplicate data for uuid: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v0, v0, v1

    invoke-static {v0}, Lcom/google/android/exoplayer2/drm/a$a;->a(Lcom/google/android/exoplayer2/drm/a$a;)Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 67
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 72
    :cond_1
    iput-object v0, p0, Lcom/google/android/exoplayer2/drm/a;->b:[Lcom/google/android/exoplayer2/drm/a$a;

    .line 73
    array-length v0, v0

    iput v0, p0, Lcom/google/android/exoplayer2/drm/a;->a:I

    .line 74
    return-void

    :cond_2
    move-object v0, p2

    goto :goto_0
.end method

.method public varargs constructor <init>([Lcom/google/android/exoplayer2/drm/a$a;)V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/google/android/exoplayer2/drm/a;-><init>(Z[Lcom/google/android/exoplayer2/drm/a$a;)V

    .line 57
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/exoplayer2/drm/a$a;Lcom/google/android/exoplayer2/drm/a$a;)I
    .locals 2

    .prologue
    .line 154
    sget-object v0, Lcom/google/android/exoplayer2/b;->b:Ljava/util/UUID;

    invoke-static {p1}, Lcom/google/android/exoplayer2/drm/a$a;->a(Lcom/google/android/exoplayer2/drm/a$a;)Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/exoplayer2/b;->b:Ljava/util/UUID;

    invoke-static {p2}, Lcom/google/android/exoplayer2/drm/a$a;->a(Lcom/google/android/exoplayer2/drm/a$a;)Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 155
    :goto_0
    return v0

    .line 154
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 155
    :cond_1
    invoke-static {p1}, Lcom/google/android/exoplayer2/drm/a$a;->a(Lcom/google/android/exoplayer2/drm/a$a;)Ljava/util/UUID;

    move-result-object v0

    invoke-static {p2}, Lcom/google/android/exoplayer2/drm/a$a;->a(Lcom/google/android/exoplayer2/drm/a$a;)Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/UUID;->compareTo(Ljava/util/UUID;)I

    move-result v0

    goto :goto_0
.end method

.method public a(I)Lcom/google/android/exoplayer2/drm/a$a;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/exoplayer2/drm/a;->b:[Lcom/google/android/exoplayer2/drm/a$a;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/google/android/exoplayer2/drm/a;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 115
    .line 116
    iget-object v2, p0, Lcom/google/android/exoplayer2/drm/a;->b:[Lcom/google/android/exoplayer2/drm/a$a;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 117
    iget-object v4, v4, Lcom/google/android/exoplayer2/drm/a$a;->a:Ljava/lang/String;

    invoke-static {v4, p1}, Lcom/google/android/exoplayer2/util/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 118
    const/4 v1, 0x1

    .line 122
    :goto_1
    if-eqz v1, :cond_2

    .line 123
    iget-object v1, p0, Lcom/google/android/exoplayer2/drm/a;->b:[Lcom/google/android/exoplayer2/drm/a$a;

    array-length v1, v1

    new-array v1, v1, [Lcom/google/android/exoplayer2/drm/a$a;

    .line 124
    :goto_2
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 125
    iget-object v2, p0, Lcom/google/android/exoplayer2/drm/a;->b:[Lcom/google/android/exoplayer2/drm/a$a;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Lcom/google/android/exoplayer2/drm/a$a;->a(Ljava/lang/String;)Lcom/google/android/exoplayer2/drm/a$a;

    move-result-object v2

    aput-object v2, v1, v0

    .line 124
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 116
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 127
    :cond_1
    new-instance p0, Lcom/google/android/exoplayer2/drm/a;

    invoke-direct {p0, v1}, Lcom/google/android/exoplayer2/drm/a;-><init>([Lcom/google/android/exoplayer2/drm/a$a;)V

    .line 129
    :cond_2
    return-object p0

    :cond_3
    move v1, v0

    goto :goto_1
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 33
    check-cast p1, Lcom/google/android/exoplayer2/drm/a$a;

    check-cast p2, Lcom/google/android/exoplayer2/drm/a$a;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/exoplayer2/drm/a;->a(Lcom/google/android/exoplayer2/drm/a$a;Lcom/google/android/exoplayer2/drm/a$a;)I

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 162
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 143
    if-ne p0, p1, :cond_0

    .line 144
    const/4 v0, 0x1

    .line 149
    :goto_0
    return v0

    .line 146
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 147
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 149
    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/drm/a;->b:[Lcom/google/android/exoplayer2/drm/a$a;

    check-cast p1, Lcom/google/android/exoplayer2/drm/a;

    iget-object v1, p1, Lcom/google/android/exoplayer2/drm/a;->b:[Lcom/google/android/exoplayer2/drm/a$a;

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 135
    iget v0, p0, Lcom/google/android/exoplayer2/drm/a;->c:I

    if-nez v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/google/android/exoplayer2/drm/a;->b:[Lcom/google/android/exoplayer2/drm/a$a;

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/google/android/exoplayer2/drm/a;->c:I

    .line 138
    :cond_0
    iget v0, p0, Lcom/google/android/exoplayer2/drm/a;->c:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/exoplayer2/drm/a;->b:[Lcom/google/android/exoplayer2/drm/a$a;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 168
    return-void
.end method
