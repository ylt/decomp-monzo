.class public Lcom/google/android/exoplayer2/b/b;
.super Lcom/google/android/exoplayer2/b/d;
.source "DefaultTrackSelector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/b/b$a;,
        Lcom/google/android/exoplayer2/b/b$b;
    }
.end annotation


# static fields
.field private static final a:[I


# instance fields
.field private final b:Lcom/google/android/exoplayer2/b/e$a;

.field private final c:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lcom/google/android/exoplayer2/b/b$b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 439
    const/4 v0, 0x0

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/exoplayer2/b/b;->a:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 449
    const/4 v0, 0x0

    check-cast v0, Lcom/google/android/exoplayer2/b/e$a;

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/b/b;-><init>(Lcom/google/android/exoplayer2/b/e$a;)V

    .line 450
    return-void
.end method

.method public constructor <init>(Lcom/google/android/exoplayer2/b/e$a;)V
    .locals 2

    .prologue
    .line 469
    invoke-direct {p0}, Lcom/google/android/exoplayer2/b/d;-><init>()V

    .line 470
    iput-object p1, p0, Lcom/google/android/exoplayer2/b/b;->b:Lcom/google/android/exoplayer2/b/e$a;

    .line 471
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v1, Lcom/google/android/exoplayer2/b/b$b;

    invoke-direct {v1}, Lcom/google/android/exoplayer2/b/b$b;-><init>()V

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/b/b;->c:Ljava/util/concurrent/atomic/AtomicReference;

    .line 472
    return-void
.end method

.method private static a(II)I
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 750
    if-ne p0, v0, :cond_1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-ne p1, v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    sub-int v0, p0, p1

    goto :goto_0
.end method

.method private static a(ILjava/lang/String;Lcom/google/android/exoplayer2/j;)I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 811
    iget v0, p2, Lcom/google/android/exoplayer2/j;->x:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    move v0, v1

    .line 813
    :goto_0
    invoke-static {p2, p1}, Lcom/google/android/exoplayer2/b/b;->a(Lcom/google/android/exoplayer2/j;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 814
    if-eqz v0, :cond_3

    .line 815
    const/4 v1, 0x4

    .line 824
    :cond_0
    :goto_1
    invoke-static {p0, v2}, Lcom/google/android/exoplayer2/b/b;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 825
    add-int/lit16 v1, v1, 0x3e8

    .line 827
    :cond_1
    return v1

    :cond_2
    move v0, v2

    .line 811
    goto :goto_0

    .line 817
    :cond_3
    const/4 v1, 0x3

    goto :goto_1

    .line 819
    :cond_4
    if-eqz v0, :cond_0

    .line 820
    const/4 v1, 0x2

    goto :goto_1
.end method

.method private static a(Lcom/google/android/exoplayer2/source/p;[IILjava/lang/String;IIILjava/util/List;)I
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/source/p;",
            "[II",
            "Ljava/lang/String;",
            "III",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 646
    const/4 v2, 0x0

    .line 647
    const/4 v1, 0x0

    move v8, v1

    move v9, v2

    :goto_0
    invoke-interface/range {p7 .. p7}, Ljava/util/List;->size()I

    move-result v1

    if-ge v8, v1, :cond_0

    .line 648
    move-object/from16 v0, p7

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 649
    invoke-virtual {p0, v2}, Lcom/google/android/exoplayer2/source/p;->a(I)Lcom/google/android/exoplayer2/j;

    move-result-object v1

    aget v3, p1, v2

    move-object v2, p3

    move v4, p2

    move v5, p4

    move v6, p5

    move/from16 v7, p6

    invoke-static/range {v1 .. v7}, Lcom/google/android/exoplayer2/b/b;->a(Lcom/google/android/exoplayer2/j;Ljava/lang/String;IIIII)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 652
    add-int/lit8 v2, v9, 0x1

    .line 647
    :goto_1
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    move v9, v2

    goto :goto_0

    .line 655
    :cond_0
    return v9

    :cond_1
    move v2, v9

    goto :goto_1
.end method

.method private static a(Lcom/google/android/exoplayer2/source/p;[ILcom/google/android/exoplayer2/b/b$a;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 865
    move v1, v0

    .line 866
    :goto_0
    iget v2, p0, Lcom/google/android/exoplayer2/source/p;->a:I

    if-ge v0, v2, :cond_1

    .line 867
    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/source/p;->a(I)Lcom/google/android/exoplayer2/j;

    move-result-object v2

    aget v3, p1, v0

    invoke-static {v2, v3, p2}, Lcom/google/android/exoplayer2/b/b;->a(Lcom/google/android/exoplayer2/j;ILcom/google/android/exoplayer2/b/b$a;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 868
    add-int/lit8 v1, v1, 0x1

    .line 866
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 871
    :cond_1
    return v1
.end method

.method private static a(ZIIII)Landroid/graphics/Point;
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1079
    if-eqz p0, :cond_0

    if-le p3, p4, :cond_1

    move v2, v0

    :goto_0
    if-le p1, p2, :cond_2

    :goto_1
    if-eq v2, v0, :cond_0

    move v3, p1

    move p1, p2

    move p2, v3

    .line 1086
    :cond_0
    mul-int v0, p3, p2

    mul-int v1, p4, p1

    if-lt v0, v1, :cond_3

    .line 1088
    new-instance v0, Landroid/graphics/Point;

    mul-int v1, p1, p4

    invoke-static {v1, p3}, Lcom/google/android/exoplayer2/util/s;->a(II)I

    move-result v1

    invoke-direct {v0, p1, v1}, Landroid/graphics/Point;-><init>(II)V

    .line 1091
    :goto_2
    return-object v0

    :cond_1
    move v2, v1

    .line 1079
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 1091
    :cond_3
    new-instance v0, Landroid/graphics/Point;

    mul-int v1, p2, p3

    invoke-static {v1, p4}, Lcom/google/android/exoplayer2/util/s;->a(II)I

    move-result v1

    invoke-direct {v0, v1, p2}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_2
.end method

.method private static a(Lcom/google/android/exoplayer2/source/p;IIZ)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/source/p;",
            "IIZ)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const v9, 0x3f7ae148    # 0.98f

    const v3, 0x7fffffff

    .line 1029
    new-instance v4, Ljava/util/ArrayList;

    iget v0, p0, Lcom/google/android/exoplayer2/source/p;->a:I

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v1

    .line 1030
    :goto_0
    iget v2, p0, Lcom/google/android/exoplayer2/source/p;->a:I

    if-ge v0, v2, :cond_0

    .line 1031
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1030
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1034
    :cond_0
    if-eq p1, v3, :cond_1

    if-ne p2, v3, :cond_2

    :cond_1
    move-object v0, v4

    .line 1070
    :goto_1
    return-object v0

    :cond_2
    move v2, v3

    .line 1040
    :goto_2
    iget v0, p0, Lcom/google/android/exoplayer2/source/p;->a:I

    if-ge v1, v0, :cond_3

    .line 1041
    invoke-virtual {p0, v1}, Lcom/google/android/exoplayer2/source/p;->a(I)Lcom/google/android/exoplayer2/j;

    move-result-object v5

    .line 1045
    iget v0, v5, Lcom/google/android/exoplayer2/j;->j:I

    if-lez v0, :cond_7

    iget v0, v5, Lcom/google/android/exoplayer2/j;->k:I

    if-lez v0, :cond_7

    .line 1046
    iget v0, v5, Lcom/google/android/exoplayer2/j;->j:I

    iget v6, v5, Lcom/google/android/exoplayer2/j;->k:I

    invoke-static {p3, p1, p2, v0, v6}, Lcom/google/android/exoplayer2/b/b;->a(ZIIII)Landroid/graphics/Point;

    move-result-object v6

    .line 1048
    iget v0, v5, Lcom/google/android/exoplayer2/j;->j:I

    iget v7, v5, Lcom/google/android/exoplayer2/j;->k:I

    mul-int/2addr v0, v7

    .line 1049
    iget v7, v5, Lcom/google/android/exoplayer2/j;->j:I

    iget v8, v6, Landroid/graphics/Point;->x:I

    int-to-float v8, v8

    mul-float/2addr v8, v9

    float-to-int v8, v8

    if-lt v7, v8, :cond_7

    iget v5, v5, Lcom/google/android/exoplayer2/j;->k:I

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    mul-float/2addr v6, v9

    float-to-int v6, v6

    if-lt v5, v6, :cond_7

    if-ge v0, v2, :cond_7

    .line 1040
    :goto_3
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 1060
    :cond_3
    if-eq v2, v3, :cond_6

    .line 1061
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_4
    if-ltz v1, :cond_6

    .line 1062
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/source/p;->a(I)Lcom/google/android/exoplayer2/j;

    move-result-object v0

    .line 1063
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/j;->a()I

    move-result v0

    .line 1064
    const/4 v3, -0x1

    if-eq v0, v3, :cond_4

    if-le v0, v2, :cond_5

    .line 1065
    :cond_4
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1061
    :cond_5
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_4

    :cond_6
    move-object v0, v4

    .line 1070
    goto :goto_1

    :cond_7
    move v0, v2

    goto :goto_3
.end method

.method protected static a(IZ)Z
    .locals 2

    .prologue
    .line 1005
    and-int/lit8 v0, p0, 0x7

    .line 1006
    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    if-eqz p1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/google/android/exoplayer2/j;ILcom/google/android/exoplayer2/b/b$a;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 876
    invoke-static {p1, v0}, Lcom/google/android/exoplayer2/b/b;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/google/android/exoplayer2/j;->r:I

    iget v2, p2, Lcom/google/android/exoplayer2/b/b$a;->a:I

    if-ne v1, v2, :cond_1

    iget v1, p0, Lcom/google/android/exoplayer2/j;->s:I

    iget v2, p2, Lcom/google/android/exoplayer2/b/b$a;->b:I

    if-ne v1, v2, :cond_1

    iget-object v1, p2, Lcom/google/android/exoplayer2/b/b$a;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p2, Lcom/google/android/exoplayer2/b/b$a;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/exoplayer2/j;->f:Ljava/lang/String;

    .line 879
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method protected static a(Lcom/google/android/exoplayer2/j;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1020
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/j;->y:Ljava/lang/String;

    .line 1021
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/s;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/google/android/exoplayer2/j;Ljava/lang/String;IIIII)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    .line 674
    invoke-static {p2, v0}, Lcom/google/android/exoplayer2/b/b;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_4

    and-int v1, p2, p3

    if-eqz v1, :cond_4

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer2/j;->f:Ljava/lang/String;

    .line 675
    invoke-static {v1, p1}, Lcom/google/android/exoplayer2/util/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_0
    iget v1, p0, Lcom/google/android/exoplayer2/j;->j:I

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/google/android/exoplayer2/j;->j:I

    if-gt v1, p4, :cond_4

    :cond_1
    iget v1, p0, Lcom/google/android/exoplayer2/j;->k:I

    if-eq v1, v2, :cond_2

    iget v1, p0, Lcom/google/android/exoplayer2/j;->k:I

    if-gt v1, p5, :cond_4

    :cond_2
    iget v1, p0, Lcom/google/android/exoplayer2/j;->b:I

    if-eq v1, v2, :cond_3

    iget v1, p0, Lcom/google/android/exoplayer2/j;->b:I

    if-gt v1, p6, :cond_4

    :cond_3
    const/4 v0, 0x1

    :cond_4
    return v0
.end method

.method private static a(Lcom/google/android/exoplayer2/source/p;[IZ)[I
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 832
    .line 834
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    move v4, v5

    move-object v1, v6

    move v3, v5

    .line 835
    :goto_0
    iget v0, p0, Lcom/google/android/exoplayer2/source/p;->a:I

    if-ge v4, v0, :cond_1

    .line 836
    invoke-virtual {p0, v4}, Lcom/google/android/exoplayer2/source/p;->a(I)Lcom/google/android/exoplayer2/j;

    move-result-object v2

    .line 837
    new-instance v0, Lcom/google/android/exoplayer2/b/b$a;

    iget v8, v2, Lcom/google/android/exoplayer2/j;->r:I

    iget v9, v2, Lcom/google/android/exoplayer2/j;->s:I

    if-eqz p2, :cond_0

    move-object v2, v6

    :goto_1
    invoke-direct {v0, v8, v9, v2}, Lcom/google/android/exoplayer2/b/b$a;-><init>(IILjava/lang/String;)V

    .line 840
    invoke-virtual {v7, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 841
    invoke-static {p0, p1, v0}, Lcom/google/android/exoplayer2/b/b;->a(Lcom/google/android/exoplayer2/source/p;[ILcom/google/android/exoplayer2/b/b$a;)I

    move-result v2

    .line 842
    if-le v2, v3, :cond_5

    move v1, v2

    .line 835
    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v3, v1

    move-object v1, v0

    goto :goto_0

    .line 837
    :cond_0
    iget-object v2, v2, Lcom/google/android/exoplayer2/j;->f:Ljava/lang/String;

    goto :goto_1

    .line 849
    :cond_1
    const/4 v0, 0x1

    if-le v3, v0, :cond_4

    .line 850
    new-array v3, v3, [I

    move v0, v5

    .line 852
    :goto_3
    iget v2, p0, Lcom/google/android/exoplayer2/source/p;->a:I

    if-ge v5, v2, :cond_3

    .line 853
    invoke-virtual {p0, v5}, Lcom/google/android/exoplayer2/source/p;->a(I)Lcom/google/android/exoplayer2/j;

    move-result-object v2

    aget v4, p1, v5

    invoke-static {v2, v4, v1}, Lcom/google/android/exoplayer2/b/b;->a(Lcom/google/android/exoplayer2/j;ILcom/google/android/exoplayer2/b/b$a;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 855
    add-int/lit8 v2, v0, 0x1

    aput v5, v3, v0

    move v0, v2

    .line 852
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_3
    move-object v0, v3

    .line 860
    :goto_4
    return-object v0

    :cond_4
    sget-object v0, Lcom/google/android/exoplayer2/b/b;->a:[I

    goto :goto_4

    :cond_5
    move-object v0, v1

    move v1, v3

    goto :goto_2
.end method

.method private static a(Lcom/google/android/exoplayer2/source/p;[IZIIIIIIZ)[I
    .locals 15

    .prologue
    .line 606
    iget v3, p0, Lcom/google/android/exoplayer2/source/p;->a:I

    const/4 v4, 0x2

    if-ge v3, v4, :cond_0

    .line 607
    sget-object v3, Lcom/google/android/exoplayer2/b/b;->a:[I

    .line 640
    :goto_0
    return-object v3

    .line 610
    :cond_0
    move/from16 v0, p7

    move/from16 v1, p8

    move/from16 v2, p9

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/exoplayer2/b/b;->a(Lcom/google/android/exoplayer2/source/p;IIZ)Ljava/util/List;

    move-result-object v10

    .line 612
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x2

    if-ge v3, v4, :cond_1

    .line 613
    sget-object v3, Lcom/google/android/exoplayer2/b/b;->a:[I

    goto :goto_0

    .line 616
    :cond_1
    const/4 v12, 0x0

    .line 617
    if-nez p2, :cond_5

    .line 619
    new-instance v14, Ljava/util/HashSet;

    invoke-direct {v14}, Ljava/util/HashSet;-><init>()V

    .line 620
    const/4 v11, 0x0

    .line 621
    const/4 v3, 0x0

    move v13, v3

    :goto_1
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v3

    if-ge v13, v3, :cond_2

    .line 622
    invoke-interface {v10, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 623
    invoke-virtual {p0, v3}, Lcom/google/android/exoplayer2/source/p;->a(I)Lcom/google/android/exoplayer2/j;

    move-result-object v3

    iget-object v6, v3, Lcom/google/android/exoplayer2/j;->f:Ljava/lang/String;

    .line 624
    invoke-virtual {v14, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    move-object v3, p0

    move-object/from16 v4, p1

    move/from16 v5, p3

    move/from16 v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    .line 625
    invoke-static/range {v3 .. v10}, Lcom/google/android/exoplayer2/b/b;->a(Lcom/google/android/exoplayer2/source/p;[IILjava/lang/String;IIILjava/util/List;)I

    move-result v3

    .line 628
    if-le v3, v11, :cond_4

    .line 621
    :goto_2
    add-int/lit8 v4, v13, 0x1

    move v13, v4

    move v11, v3

    move-object v12, v6

    goto :goto_1

    :cond_2
    move-object v6, v12

    :goto_3
    move-object v3, p0

    move-object/from16 v4, p1

    move/from16 v5, p3

    move/from16 v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    .line 637
    invoke-static/range {v3 .. v10}, Lcom/google/android/exoplayer2/b/b;->b(Lcom/google/android/exoplayer2/source/p;[IILjava/lang/String;IIILjava/util/List;)V

    .line 640
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x2

    if-ge v3, v4, :cond_3

    sget-object v3, Lcom/google/android/exoplayer2/b/b;->a:[I

    goto :goto_0

    :cond_3
    invoke-static {v10}, Lcom/google/android/exoplayer2/util/s;->a(Ljava/util/List;)[I

    move-result-object v3

    goto :goto_0

    :cond_4
    move v3, v11

    move-object v6, v12

    goto :goto_2

    :cond_5
    move-object v6, v12

    goto :goto_3
.end method

.method private static b(Lcom/google/android/exoplayer2/q;Lcom/google/android/exoplayer2/source/q;[[ILcom/google/android/exoplayer2/b/b$b;Lcom/google/android/exoplayer2/b/e$a;)Lcom/google/android/exoplayer2/b/e;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 584
    iget-boolean v1, p3, Lcom/google/android/exoplayer2/b/b$b;->k:Z

    if-eqz v1, :cond_0

    const/16 v3, 0x18

    .line 587
    :goto_0
    iget-boolean v1, p3, Lcom/google/android/exoplayer2/b/b$b;->j:Z

    if-eqz v1, :cond_1

    .line 588
    invoke-interface {p0}, Lcom/google/android/exoplayer2/q;->m()I

    move-result v1

    and-int/2addr v1, v3

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    :goto_1
    move v10, v0

    .line 589
    :goto_2
    iget v0, p1, Lcom/google/android/exoplayer2/source/q;->b:I

    if-ge v10, v0, :cond_3

    .line 590
    invoke-virtual {p1, v10}, Lcom/google/android/exoplayer2/source/q;->a(I)Lcom/google/android/exoplayer2/source/p;

    move-result-object v0

    .line 591
    aget-object v1, p2, v10

    iget v4, p3, Lcom/google/android/exoplayer2/b/b$b;->c:I

    iget v5, p3, Lcom/google/android/exoplayer2/b/b$b;->d:I

    iget v6, p3, Lcom/google/android/exoplayer2/b/b$b;->e:I

    iget v7, p3, Lcom/google/android/exoplayer2/b/b$b;->g:I

    iget v8, p3, Lcom/google/android/exoplayer2/b/b$b;->h:I

    iget-boolean v9, p3, Lcom/google/android/exoplayer2/b/b$b;->i:Z

    invoke-static/range {v0 .. v9}, Lcom/google/android/exoplayer2/b/b;->a(Lcom/google/android/exoplayer2/source/p;[IZIIIIIIZ)[I

    move-result-object v1

    .line 595
    array-length v4, v1

    if-lez v4, :cond_2

    .line 596
    invoke-interface {p4, v0, v1}, Lcom/google/android/exoplayer2/b/e$a;->a(Lcom/google/android/exoplayer2/source/p;[I)Lcom/google/android/exoplayer2/b/e;

    move-result-object v0

    .line 599
    :goto_3
    return-object v0

    .line 584
    :cond_0
    const/16 v3, 0x10

    goto :goto_0

    :cond_1
    move v2, v0

    .line 588
    goto :goto_1

    .line 589
    :cond_2
    add-int/lit8 v0, v10, 0x1

    move v10, v0

    goto :goto_2

    .line 599
    :cond_3
    const/4 v0, 0x0

    goto :goto_3
.end method

.method private static b(Lcom/google/android/exoplayer2/source/q;[[ILcom/google/android/exoplayer2/b/b$b;)Lcom/google/android/exoplayer2/b/e;
    .locals 16

    .prologue
    .line 683
    const/4 v7, 0x0

    .line 684
    const/4 v5, 0x0

    .line 685
    const/4 v4, 0x0

    .line 686
    const/4 v3, -0x1

    .line 687
    const/4 v2, -0x1

    .line 688
    const/4 v1, 0x0

    move v10, v1

    move v1, v2

    move v2, v3

    move v3, v4

    :goto_0
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/exoplayer2/source/q;->b:I

    if-ge v10, v4, :cond_e

    .line 689
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/exoplayer2/source/q;->a(I)Lcom/google/android/exoplayer2/source/p;

    move-result-object v8

    .line 690
    move-object/from16 v0, p2

    iget v4, v0, Lcom/google/android/exoplayer2/b/b$b;->g:I

    move-object/from16 v0, p2

    iget v6, v0, Lcom/google/android/exoplayer2/b/b$b;->h:I

    move-object/from16 v0, p2

    iget-boolean v9, v0, Lcom/google/android/exoplayer2/b/b$b;->i:Z

    invoke-static {v8, v4, v6, v9}, Lcom/google/android/exoplayer2/b/b;->a(Lcom/google/android/exoplayer2/source/p;IIZ)Ljava/util/List;

    move-result-object v12

    .line 692
    aget-object v13, p1, v10

    .line 693
    const/4 v6, 0x0

    :goto_1
    iget v4, v8, Lcom/google/android/exoplayer2/source/p;->a:I

    if-ge v6, v4, :cond_d

    .line 694
    aget v4, v13, v6

    move-object/from16 v0, p2

    iget-boolean v9, v0, Lcom/google/android/exoplayer2/b/b$b;->l:Z

    invoke-static {v4, v9}, Lcom/google/android/exoplayer2/b/b;->a(IZ)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 696
    invoke-virtual {v8, v6}, Lcom/google/android/exoplayer2/source/p;->a(I)Lcom/google/android/exoplayer2/j;

    move-result-object v14

    .line 697
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v12, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget v4, v14, Lcom/google/android/exoplayer2/j;->j:I

    const/4 v9, -0x1

    if-eq v4, v9, :cond_0

    iget v4, v14, Lcom/google/android/exoplayer2/j;->j:I

    move-object/from16 v0, p2

    iget v9, v0, Lcom/google/android/exoplayer2/b/b$b;->c:I

    if-gt v4, v9, :cond_3

    :cond_0
    iget v4, v14, Lcom/google/android/exoplayer2/j;->k:I

    const/4 v9, -0x1

    if-eq v4, v9, :cond_1

    iget v4, v14, Lcom/google/android/exoplayer2/j;->k:I

    move-object/from16 v0, p2

    iget v9, v0, Lcom/google/android/exoplayer2/b/b$b;->d:I

    if-gt v4, v9, :cond_3

    :cond_1
    iget v4, v14, Lcom/google/android/exoplayer2/j;->b:I

    const/4 v9, -0x1

    if-eq v4, v9, :cond_2

    iget v4, v14, Lcom/google/android/exoplayer2/j;->b:I

    move-object/from16 v0, p2

    iget v9, v0, Lcom/google/android/exoplayer2/b/b$b;->e:I

    if-gt v4, v9, :cond_3

    :cond_2
    const/4 v4, 0x1

    move v11, v4

    .line 701
    :goto_2
    if-nez v11, :cond_4

    move-object/from16 v0, p2

    iget-boolean v4, v0, Lcom/google/android/exoplayer2/b/b$b;->f:Z

    if-nez v4, :cond_4

    move v4, v5

    move-object v5, v7

    .line 693
    :goto_3
    add-int/lit8 v6, v6, 0x1

    move-object v7, v5

    move v5, v4

    goto :goto_1

    .line 697
    :cond_3
    const/4 v4, 0x0

    move v11, v4

    goto :goto_2

    .line 705
    :cond_4
    if-eqz v11, :cond_7

    const/4 v4, 0x2

    .line 706
    :goto_4
    aget v9, v13, v6

    const/4 v15, 0x0

    invoke-static {v9, v15}, Lcom/google/android/exoplayer2/b/b;->a(IZ)Z

    move-result v15

    .line 707
    if-eqz v15, :cond_5

    .line 708
    add-int/lit16 v4, v4, 0x3e8

    .line 710
    :cond_5
    if-le v4, v3, :cond_8

    const/4 v9, 0x1

    .line 711
    :goto_5
    if-ne v4, v3, :cond_6

    .line 717
    invoke-virtual {v14}, Lcom/google/android/exoplayer2/j;->a()I

    move-result v9

    .line 718
    if-eq v9, v1, :cond_9

    .line 719
    invoke-virtual {v14}, Lcom/google/android/exoplayer2/j;->a()I

    move-result v9

    invoke-static {v9, v1}, Lcom/google/android/exoplayer2/b/b;->a(II)I

    move-result v9

    .line 723
    :goto_6
    if-eqz v15, :cond_b

    if-eqz v11, :cond_b

    if-lez v9, :cond_a

    const/4 v9, 0x1

    .line 726
    :cond_6
    :goto_7
    if-eqz v9, :cond_10

    .line 730
    iget v2, v14, Lcom/google/android/exoplayer2/j;->b:I

    .line 731
    invoke-virtual {v14}, Lcom/google/android/exoplayer2/j;->a()I

    move-result v1

    move v3, v4

    move-object v5, v8

    move v4, v6

    goto :goto_3

    .line 705
    :cond_7
    const/4 v4, 0x1

    goto :goto_4

    .line 710
    :cond_8
    const/4 v9, 0x0

    goto :goto_5

    .line 721
    :cond_9
    iget v9, v14, Lcom/google/android/exoplayer2/j;->b:I

    invoke-static {v9, v2}, Lcom/google/android/exoplayer2/b/b;->a(II)I

    move-result v9

    goto :goto_6

    .line 723
    :cond_a
    const/4 v9, 0x0

    goto :goto_7

    :cond_b
    if-gez v9, :cond_c

    const/4 v9, 0x1

    goto :goto_7

    :cond_c
    const/4 v9, 0x0

    goto :goto_7

    .line 688
    :cond_d
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    goto/16 :goto_0

    .line 736
    :cond_e
    if-nez v7, :cond_f

    const/4 v1, 0x0

    :goto_8
    return-object v1

    :cond_f
    new-instance v1, Lcom/google/android/exoplayer2/b/c;

    invoke-direct {v1, v7, v5}, Lcom/google/android/exoplayer2/b/c;-><init>(Lcom/google/android/exoplayer2/source/p;I)V

    goto :goto_8

    :cond_10
    move v4, v5

    move-object v5, v7

    goto :goto_3
.end method

.method private static b(Lcom/google/android/exoplayer2/source/p;[IILjava/lang/String;IIILjava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/source/p;",
            "[II",
            "Ljava/lang/String;",
            "III",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 661
    invoke-interface {p7}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v7, v0

    :goto_0
    if-ltz v7, :cond_1

    .line 662
    invoke-interface {p7, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 663
    invoke-virtual {p0, v1}, Lcom/google/android/exoplayer2/source/p;->a(I)Lcom/google/android/exoplayer2/j;

    move-result-object v0

    aget v2, p1, v1

    move-object v1, p3

    move v3, p2

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-static/range {v0 .. v6}, Lcom/google/android/exoplayer2/b/b;->a(Lcom/google/android/exoplayer2/j;Ljava/lang/String;IIIII)Z

    move-result v0

    if-nez v0, :cond_0

    .line 666
    invoke-interface {p7, v7}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 661
    :cond_0
    add-int/lit8 v0, v7, -0x1

    move v7, v0

    goto :goto_0

    .line 669
    :cond_1
    return-void
.end method


# virtual methods
.method protected a(ILcom/google/android/exoplayer2/source/q;[[ILcom/google/android/exoplayer2/b/b$b;)Lcom/google/android/exoplayer2/b/e;
    .locals 10

    .prologue
    .line 964
    const/4 v5, 0x0

    .line 965
    const/4 v3, 0x0

    .line 966
    const/4 v1, 0x0

    .line 967
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    iget v0, p2, Lcom/google/android/exoplayer2/source/q;->b:I

    if-ge v6, v0, :cond_4

    .line 968
    invoke-virtual {p2, v6}, Lcom/google/android/exoplayer2/source/q;->a(I)Lcom/google/android/exoplayer2/source/p;

    move-result-object v4

    .line 969
    aget-object v7, p3, v6

    .line 970
    const/4 v2, 0x0

    :goto_1
    iget v0, v4, Lcom/google/android/exoplayer2/source/p;->a:I

    if-ge v2, v0, :cond_3

    .line 971
    aget v0, v7, v2

    iget-boolean v8, p4, Lcom/google/android/exoplayer2/b/b$b;->l:Z

    invoke-static {v0, v8}, Lcom/google/android/exoplayer2/b/b;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 973
    invoke-virtual {v4, v2}, Lcom/google/android/exoplayer2/source/p;->a(I)Lcom/google/android/exoplayer2/j;

    move-result-object v0

    .line 974
    iget v0, v0, Lcom/google/android/exoplayer2/j;->x:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 975
    :goto_2
    if-eqz v0, :cond_2

    const/4 v0, 0x2

    .line 976
    :goto_3
    aget v8, v7, v2

    const/4 v9, 0x0

    invoke-static {v8, v9}, Lcom/google/android/exoplayer2/b/b;->a(IZ)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 977
    add-int/lit16 v0, v0, 0x3e8

    .line 979
    :cond_0
    if-le v0, v1, :cond_6

    move v1, v2

    move-object v3, v4

    .line 970
    :goto_4
    add-int/lit8 v2, v2, 0x1

    move-object v5, v3

    move v3, v1

    move v1, v0

    goto :goto_1

    .line 974
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 975
    :cond_2
    const/4 v0, 0x1

    goto :goto_3

    .line 967
    :cond_3
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 987
    :cond_4
    if-nez v5, :cond_5

    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_5
    new-instance v0, Lcom/google/android/exoplayer2/b/c;

    invoke-direct {v0, v5, v3}, Lcom/google/android/exoplayer2/b/c;-><init>(Lcom/google/android/exoplayer2/source/p;I)V

    goto :goto_5

    :cond_6
    move v0, v1

    move v1, v3

    move-object v3, v5

    goto :goto_4
.end method

.method protected a(Lcom/google/android/exoplayer2/q;Lcom/google/android/exoplayer2/source/q;[[ILcom/google/android/exoplayer2/b/b$b;Lcom/google/android/exoplayer2/b/e$a;)Lcom/google/android/exoplayer2/b/e;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    .line 570
    const/4 v0, 0x0

    .line 571
    if-eqz p5, :cond_0

    .line 572
    invoke-static {p1, p2, p3, p4, p5}, Lcom/google/android/exoplayer2/b/b;->b(Lcom/google/android/exoplayer2/q;Lcom/google/android/exoplayer2/source/q;[[ILcom/google/android/exoplayer2/b/b$b;Lcom/google/android/exoplayer2/b/e$a;)Lcom/google/android/exoplayer2/b/e;

    move-result-object v0

    .line 575
    :cond_0
    if-nez v0, :cond_1

    .line 576
    invoke-static {p2, p3, p4}, Lcom/google/android/exoplayer2/b/b;->b(Lcom/google/android/exoplayer2/source/q;[[ILcom/google/android/exoplayer2/b/b$b;)Lcom/google/android/exoplayer2/b/e;

    move-result-object v0

    .line 578
    :cond_1
    return-object v0
.end method

.method protected a(Lcom/google/android/exoplayer2/source/q;[[ILcom/google/android/exoplayer2/b/b$b;)Lcom/google/android/exoplayer2/b/e;
    .locals 11

    .prologue
    .line 897
    const/4 v4, 0x0

    .line 898
    const/4 v2, 0x0

    .line 899
    const/4 v1, 0x0

    .line 900
    const/4 v0, 0x0

    move v7, v0

    move v0, v1

    :goto_0
    iget v1, p1, Lcom/google/android/exoplayer2/source/q;->b:I

    if-ge v7, v1, :cond_9

    .line 901
    invoke-virtual {p1, v7}, Lcom/google/android/exoplayer2/source/q;->a(I)Lcom/google/android/exoplayer2/source/p;

    move-result-object v5

    .line 902
    aget-object v8, p2, v7

    .line 903
    const/4 v3, 0x0

    :goto_1
    iget v1, v5, Lcom/google/android/exoplayer2/source/p;->a:I

    if-ge v3, v1, :cond_8

    .line 904
    aget v1, v8, v3

    iget-boolean v6, p3, Lcom/google/android/exoplayer2/b/b$b;->l:Z

    invoke-static {v1, v6}, Lcom/google/android/exoplayer2/b/b;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 906
    invoke-virtual {v5, v3}, Lcom/google/android/exoplayer2/source/p;->a(I)Lcom/google/android/exoplayer2/j;

    move-result-object v9

    .line 907
    iget v1, v9, Lcom/google/android/exoplayer2/j;->x:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    .line 908
    :goto_2
    iget v6, v9, Lcom/google/android/exoplayer2/j;->x:I

    and-int/lit8 v6, v6, 0x2

    if-eqz v6, :cond_2

    const/4 v6, 0x1

    .line 910
    :goto_3
    iget-object v10, p3, Lcom/google/android/exoplayer2/b/b$b;->b:Ljava/lang/String;

    invoke-static {v9, v10}, Lcom/google/android/exoplayer2/b/b;->a(Lcom/google/android/exoplayer2/j;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 911
    if-eqz v1, :cond_3

    .line 912
    const/4 v1, 0x6

    .line 933
    :goto_4
    aget v6, v8, v3

    const/4 v9, 0x0

    invoke-static {v6, v9}, Lcom/google/android/exoplayer2/b/b;->a(IZ)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 934
    add-int/lit16 v1, v1, 0x3e8

    .line 936
    :cond_0
    if-le v1, v0, :cond_b

    move v0, v1

    move-object v2, v5

    move v1, v3

    .line 903
    :goto_5
    add-int/lit8 v3, v3, 0x1

    move-object v4, v2

    move v2, v1

    goto :goto_1

    .line 907
    :cond_1
    const/4 v1, 0x0

    goto :goto_2

    .line 908
    :cond_2
    const/4 v6, 0x0

    goto :goto_3

    .line 913
    :cond_3
    if-nez v6, :cond_4

    .line 917
    const/4 v1, 0x5

    goto :goto_4

    .line 919
    :cond_4
    const/4 v1, 0x4

    goto :goto_4

    .line 921
    :cond_5
    if-eqz v1, :cond_6

    .line 922
    const/4 v1, 0x3

    goto :goto_4

    .line 923
    :cond_6
    if-eqz v6, :cond_b

    .line 924
    iget-object v1, p3, Lcom/google/android/exoplayer2/b/b$b;->a:Ljava/lang/String;

    invoke-static {v9, v1}, Lcom/google/android/exoplayer2/b/b;->a(Lcom/google/android/exoplayer2/j;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 925
    const/4 v1, 0x2

    goto :goto_4

    .line 927
    :cond_7
    const/4 v1, 0x1

    goto :goto_4

    .line 900
    :cond_8
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_0

    .line 944
    :cond_9
    if-nez v4, :cond_a

    const/4 v0, 0x0

    :goto_6
    return-object v0

    :cond_a
    new-instance v0, Lcom/google/android/exoplayer2/b/c;

    invoke-direct {v0, v4, v2}, Lcom/google/android/exoplayer2/b/c;-><init>(Lcom/google/android/exoplayer2/source/p;I)V

    goto :goto_6

    :cond_b
    move v1, v2

    move-object v2, v4

    goto :goto_5
.end method

.method protected a(Lcom/google/android/exoplayer2/source/q;[[ILcom/google/android/exoplayer2/b/b$b;Lcom/google/android/exoplayer2/b/e$a;)Lcom/google/android/exoplayer2/b/e;
    .locals 10

    .prologue
    .line 771
    const/4 v3, -0x1

    .line 772
    const/4 v2, -0x1

    .line 773
    const/4 v1, 0x0

    .line 774
    const/4 v0, 0x0

    :goto_0
    iget v4, p1, Lcom/google/android/exoplayer2/source/q;->b:I

    if-ge v0, v4, :cond_1

    .line 775
    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/source/q;->a(I)Lcom/google/android/exoplayer2/source/p;

    move-result-object v6

    .line 776
    aget-object v7, p2, v0

    .line 777
    const/4 v4, 0x0

    :goto_1
    iget v5, v6, Lcom/google/android/exoplayer2/source/p;->a:I

    if-ge v4, v5, :cond_0

    .line 778
    aget v5, v7, v4

    iget-boolean v8, p3, Lcom/google/android/exoplayer2/b/b$b;->l:Z

    invoke-static {v5, v8}, Lcom/google/android/exoplayer2/b/b;->a(IZ)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 780
    invoke-virtual {v6, v4}, Lcom/google/android/exoplayer2/source/p;->a(I)Lcom/google/android/exoplayer2/j;

    move-result-object v5

    .line 781
    aget v8, v7, v4

    iget-object v9, p3, Lcom/google/android/exoplayer2/b/b$b;->a:Ljava/lang/String;

    invoke-static {v8, v9, v5}, Lcom/google/android/exoplayer2/b/b;->a(ILjava/lang/String;Lcom/google/android/exoplayer2/j;)I

    move-result v5

    .line 783
    if-le v5, v1, :cond_4

    move v2, v5

    move v3, v4

    move v5, v0

    .line 777
    :goto_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v1, v2

    move v2, v3

    move v3, v5

    goto :goto_1

    .line 774
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 792
    :cond_1
    const/4 v0, -0x1

    if-ne v3, v0, :cond_2

    .line 793
    const/4 v0, 0x0

    .line 806
    :goto_3
    return-object v0

    .line 796
    :cond_2
    invoke-virtual {p1, v3}, Lcom/google/android/exoplayer2/source/q;->a(I)Lcom/google/android/exoplayer2/source/p;

    move-result-object v1

    .line 797
    if-eqz p4, :cond_3

    .line 799
    aget-object v0, p2, v3

    iget-boolean v3, p3, Lcom/google/android/exoplayer2/b/b$b;->j:Z

    invoke-static {v1, v0, v3}, Lcom/google/android/exoplayer2/b/b;->a(Lcom/google/android/exoplayer2/source/p;[IZ)[I

    move-result-object v0

    .line 801
    array-length v3, v0

    if-lez v3, :cond_3

    .line 802
    invoke-interface {p4, v1, v0}, Lcom/google/android/exoplayer2/b/e$a;->a(Lcom/google/android/exoplayer2/source/p;[I)Lcom/google/android/exoplayer2/b/e;

    move-result-object v0

    goto :goto_3

    .line 806
    :cond_3
    new-instance v0, Lcom/google/android/exoplayer2/b/c;

    invoke-direct {v0, v1, v2}, Lcom/google/android/exoplayer2/b/c;-><init>(Lcom/google/android/exoplayer2/source/p;I)V

    goto :goto_3

    :cond_4
    move v5, v3

    move v3, v2

    move v2, v1

    goto :goto_2
.end method

.method protected a([Lcom/google/android/exoplayer2/q;[Lcom/google/android/exoplayer2/source/q;[[[I)[Lcom/google/android/exoplayer2/b/e;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    .line 502
    array-length v8, p1

    .line 503
    new-array v9, v8, [Lcom/google/android/exoplayer2/b/e;

    .line 504
    iget-object v0, p0, Lcom/google/android/exoplayer2/b/b;->c:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/exoplayer2/b/b$b;

    .line 506
    const/4 v6, 0x0

    .line 507
    const/4 v1, 0x0

    .line 508
    const/4 v0, 0x0

    move v7, v0

    move v0, v1

    :goto_0
    if-ge v7, v8, :cond_3

    .line 509
    const/4 v1, 0x2

    aget-object v2, p1, v7

    invoke-interface {v2}, Lcom/google/android/exoplayer2/q;->a()I

    move-result v2

    if-ne v1, v2, :cond_9

    .line 510
    if-nez v0, :cond_0

    .line 511
    aget-object v1, p1, v7

    aget-object v2, p2, v7

    aget-object v3, p3, v7

    iget-object v5, p0, Lcom/google/android/exoplayer2/b/b;->b:Lcom/google/android/exoplayer2/b/e$a;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/exoplayer2/b/b;->a(Lcom/google/android/exoplayer2/q;Lcom/google/android/exoplayer2/source/q;[[ILcom/google/android/exoplayer2/b/b$b;Lcom/google/android/exoplayer2/b/e$a;)Lcom/google/android/exoplayer2/b/e;

    move-result-object v0

    aput-object v0, v9, v7

    .line 514
    aget-object v0, v9, v7

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 516
    :cond_0
    :goto_1
    aget-object v1, p2, v7

    iget v1, v1, Lcom/google/android/exoplayer2/source/q;->b:I

    if-lez v1, :cond_2

    const/4 v1, 0x1

    :goto_2
    or-int/2addr v1, v6

    .line 508
    :goto_3
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    move v6, v1

    goto :goto_0

    .line 514
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 516
    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    .line 520
    :cond_3
    const/4 v2, 0x0

    .line 521
    const/4 v1, 0x0

    .line 522
    const/4 v0, 0x0

    move v3, v0

    :goto_4
    if-ge v3, v8, :cond_8

    .line 523
    aget-object v0, p1, v3

    invoke-interface {v0}, Lcom/google/android/exoplayer2/q;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 543
    aget-object v0, p1, v3

    invoke-interface {v0}, Lcom/google/android/exoplayer2/q;->a()I

    move-result v0

    aget-object v5, p2, v3

    aget-object v7, p3, v3

    invoke-virtual {p0, v0, v5, v7, v4}, Lcom/google/android/exoplayer2/b/b;->a(ILcom/google/android/exoplayer2/source/q;[[ILcom/google/android/exoplayer2/b/b$b;)Lcom/google/android/exoplayer2/b/e;

    move-result-object v0

    aput-object v0, v9, v3

    :cond_4
    move v0, v1

    move v1, v2

    .line 522
    :goto_5
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    move v1, v0

    goto :goto_4

    :pswitch_0
    move v0, v1

    move v1, v2

    .line 526
    goto :goto_5

    .line 528
    :pswitch_1
    if-nez v2, :cond_4

    .line 529
    aget-object v2, p2, v3

    aget-object v5, p3, v3

    if-eqz v6, :cond_5

    const/4 v0, 0x0

    :goto_6
    invoke-virtual {p0, v2, v5, v4, v0}, Lcom/google/android/exoplayer2/b/b;->a(Lcom/google/android/exoplayer2/source/q;[[ILcom/google/android/exoplayer2/b/b$b;Lcom/google/android/exoplayer2/b/e$a;)Lcom/google/android/exoplayer2/b/e;

    move-result-object v0

    aput-object v0, v9, v3

    .line 532
    aget-object v0, v9, v3

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_7
    move v10, v1

    move v1, v0

    move v0, v10

    goto :goto_5

    .line 529
    :cond_5
    iget-object v0, p0, Lcom/google/android/exoplayer2/b/b;->b:Lcom/google/android/exoplayer2/b/e$a;

    goto :goto_6

    .line 532
    :cond_6
    const/4 v0, 0x0

    goto :goto_7

    .line 536
    :pswitch_2
    if-nez v1, :cond_4

    .line 537
    aget-object v0, p2, v3

    aget-object v1, p3, v3

    invoke-virtual {p0, v0, v1, v4}, Lcom/google/android/exoplayer2/b/b;->a(Lcom/google/android/exoplayer2/source/q;[[ILcom/google/android/exoplayer2/b/b$b;)Lcom/google/android/exoplayer2/b/e;

    move-result-object v0

    aput-object v0, v9, v3

    .line 539
    aget-object v0, v9, v3

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    :goto_8
    move v1, v2

    goto :goto_5

    :cond_7
    const/4 v0, 0x0

    goto :goto_8

    .line 548
    :cond_8
    return-object v9

    :cond_9
    move v1, v6

    goto :goto_3

    .line 523
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
