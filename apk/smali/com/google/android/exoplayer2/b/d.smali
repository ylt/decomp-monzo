.class public abstract Lcom/google/android/exoplayer2/b/d;
.super Lcom/google/android/exoplayer2/b/g;
.source "MappingTrackSelector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/b/d$b;,
        Lcom/google/android/exoplayer2/b/d$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/exoplayer2/source/q;",
            "Lcom/google/android/exoplayer2/b/d$b;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Landroid/util/SparseBooleanArray;

.field private c:I

.field private d:Lcom/google/android/exoplayer2/b/d$a;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 342
    invoke-direct {p0}, Lcom/google/android/exoplayer2/b/g;-><init>()V

    .line 343
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/b/d;->a:Landroid/util/SparseArray;

    .line 344
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/b/d;->b:Landroid/util/SparseBooleanArray;

    .line 345
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer2/b/d;->c:I

    .line 346
    return-void
.end method

.method private static a([Lcom/google/android/exoplayer2/q;Lcom/google/android/exoplayer2/source/p;)I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 637
    array-length v0, p0

    move v2, v1

    move v3, v0

    move v0, v1

    .line 639
    :goto_0
    array-length v4, p0

    if-ge v0, v4, :cond_2

    .line 640
    aget-object v6, p0, v0

    move v4, v1

    .line 641
    :goto_1
    iget v5, p1, Lcom/google/android/exoplayer2/source/p;->a:I

    if-ge v4, v5, :cond_1

    .line 642
    invoke-virtual {p1, v4}, Lcom/google/android/exoplayer2/source/p;->a(I)Lcom/google/android/exoplayer2/j;

    move-result-object v5

    invoke-interface {v6, v5}, Lcom/google/android/exoplayer2/q;->a(Lcom/google/android/exoplayer2/j;)I

    move-result v5

    and-int/lit8 v5, v5, 0x7

    .line 644
    if-le v5, v2, :cond_3

    .line 647
    const/4 v2, 0x4

    if-ne v5, v2, :cond_0

    .line 654
    :goto_2
    return v0

    :cond_0
    move v3, v5

    move v5, v0

    .line 641
    :goto_3
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v3

    move v3, v5

    goto :goto_1

    .line 639
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v3

    .line 654
    goto :goto_2

    :cond_3
    move v5, v3

    move v3, v2

    goto :goto_3
.end method

.method private static a([Lcom/google/android/exoplayer2/q;[Lcom/google/android/exoplayer2/source/q;[[[I[Lcom/google/android/exoplayer2/r;[Lcom/google/android/exoplayer2/b/e;I)V
    .locals 10

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 715
    if-nez p5, :cond_1

    .line 755
    :cond_0
    :goto_0
    return-void

    :cond_1
    move v0, v1

    move v2, v3

    move v4, v3

    .line 723
    :goto_1
    array-length v6, p0

    if-ge v0, v6, :cond_8

    .line 724
    aget-object v6, p0, v0

    invoke-interface {v6}, Lcom/google/android/exoplayer2/q;->a()I

    move-result v6

    .line 725
    aget-object v7, p4, v0

    .line 726
    if-eq v6, v5, :cond_2

    const/4 v8, 0x2

    if-ne v6, v8, :cond_5

    :cond_2
    if-eqz v7, :cond_5

    .line 728
    aget-object v8, p2, v0

    aget-object v9, p1, v0

    invoke-static {v8, v9, v7}, Lcom/google/android/exoplayer2/b/d;->a([[ILcom/google/android/exoplayer2/source/q;Lcom/google/android/exoplayer2/b/e;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 730
    if-ne v6, v5, :cond_6

    .line 731
    if-eq v4, v3, :cond_4

    move v0, v1

    .line 748
    :goto_2
    if-eq v4, v3, :cond_3

    if-eq v2, v3, :cond_3

    move v1, v5

    :cond_3
    and-int/2addr v0, v1

    .line 749
    if-eqz v0, :cond_0

    .line 750
    new-instance v0, Lcom/google/android/exoplayer2/r;

    invoke-direct {v0, p5}, Lcom/google/android/exoplayer2/r;-><init>(I)V

    .line 752
    aput-object v0, p3, v4

    .line 753
    aput-object v0, p3, v2

    goto :goto_0

    :cond_4
    move v4, v0

    .line 723
    :cond_5
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 738
    :cond_6
    if-eq v2, v3, :cond_7

    move v0, v1

    .line 740
    goto :goto_2

    :cond_7
    move v2, v0

    .line 742
    goto :goto_3

    :cond_8
    move v0, v5

    goto :goto_2
.end method

.method private static a([[ILcom/google/android/exoplayer2/source/q;Lcom/google/android/exoplayer2/b/e;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 768
    if-nez p2, :cond_1

    .line 779
    :cond_0
    :goto_0
    return v1

    .line 771
    :cond_1
    invoke-interface {p2}, Lcom/google/android/exoplayer2/b/e;->a()Lcom/google/android/exoplayer2/source/p;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/source/q;->a(Lcom/google/android/exoplayer2/source/p;)I

    move-result v2

    move v0, v1

    .line 772
    :goto_1
    invoke-interface {p2}, Lcom/google/android/exoplayer2/b/e;->b()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 773
    aget-object v3, p0, v2

    invoke-interface {p2, v0}, Lcom/google/android/exoplayer2/b/e;->b(I)I

    move-result v4

    aget v3, v3, v4

    .line 774
    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_0

    .line 772
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 779
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static a(Lcom/google/android/exoplayer2/q;Lcom/google/android/exoplayer2/source/p;)[I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    .line 669
    iget v0, p1, Lcom/google/android/exoplayer2/source/p;->a:I

    new-array v1, v0, [I

    .line 670
    const/4 v0, 0x0

    :goto_0
    iget v2, p1, Lcom/google/android/exoplayer2/source/p;->a:I

    if-ge v0, v2, :cond_0

    .line 671
    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/source/p;->a(I)Lcom/google/android/exoplayer2/j;

    move-result-object v2

    invoke-interface {p0, v2}, Lcom/google/android/exoplayer2/q;->a(Lcom/google/android/exoplayer2/j;)I

    move-result v2

    aput v2, v1, v0

    .line 670
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 673
    :cond_0
    return-object v1
.end method

.method private static a([Lcom/google/android/exoplayer2/q;)[I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    .line 687
    array-length v0, p0

    new-array v1, v0, [I

    .line 688
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 689
    aget-object v2, p0, v0

    invoke-interface {v2}, Lcom/google/android/exoplayer2/q;->m()I

    move-result v2

    aput v2, v1, v0

    .line 688
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 691
    :cond_0
    return-object v1
.end method


# virtual methods
.method public final a([Lcom/google/android/exoplayer2/q;Lcom/google/android/exoplayer2/source/q;)Lcom/google/android/exoplayer2/b/h;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v6, 0x0

    .line 514
    array-length v0, p1

    add-int/lit8 v0, v0, 0x1

    new-array v7, v0, [I

    .line 515
    array-length v0, p1

    add-int/lit8 v0, v0, 0x1

    new-array v8, v0, [[Lcom/google/android/exoplayer2/source/p;

    .line 516
    array-length v0, p1

    add-int/lit8 v0, v0, 0x1

    new-array v4, v0, [[[I

    move v0, v6

    .line 517
    :goto_0
    array-length v1, v8

    if-ge v0, v1, :cond_0

    .line 518
    iget v1, p2, Lcom/google/android/exoplayer2/source/q;->b:I

    new-array v1, v1, [Lcom/google/android/exoplayer2/source/p;

    aput-object v1, v8, v0

    .line 519
    iget v1, p2, Lcom/google/android/exoplayer2/source/q;->b:I

    new-array v1, v1, [[I

    aput-object v1, v4, v0

    .line 517
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 523
    :cond_0
    invoke-static {p1}, Lcom/google/android/exoplayer2/b/d;->a([Lcom/google/android/exoplayer2/q;)[I

    move-result-object v3

    move v0, v6

    .line 527
    :goto_1
    iget v1, p2, Lcom/google/android/exoplayer2/source/q;->b:I

    if-ge v0, v1, :cond_2

    .line 528
    invoke-virtual {p2, v0}, Lcom/google/android/exoplayer2/source/q;->a(I)Lcom/google/android/exoplayer2/source/p;

    move-result-object v2

    .line 530
    invoke-static {p1, v2}, Lcom/google/android/exoplayer2/b/d;->a([Lcom/google/android/exoplayer2/q;Lcom/google/android/exoplayer2/source/p;)I

    move-result v5

    .line 532
    array-length v1, p1

    if-ne v5, v1, :cond_1

    iget v1, v2, Lcom/google/android/exoplayer2/source/p;->a:I

    new-array v1, v1, [I

    .line 535
    :goto_2
    aget v9, v7, v5

    .line 536
    aget-object v11, v8, v5

    aput-object v2, v11, v9

    .line 537
    aget-object v2, v4, v5

    aput-object v1, v2, v9

    .line 538
    aget v1, v7, v5

    add-int/lit8 v1, v1, 0x1

    aput v1, v7, v5

    .line 527
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 532
    :cond_1
    aget-object v1, p1, v5

    .line 533
    invoke-static {v1, v2}, Lcom/google/android/exoplayer2/b/d;->a(Lcom/google/android/exoplayer2/q;Lcom/google/android/exoplayer2/source/p;)[I

    move-result-object v1

    goto :goto_2

    .line 542
    :cond_2
    array-length v0, p1

    new-array v2, v0, [Lcom/google/android/exoplayer2/source/q;

    .line 543
    array-length v0, p1

    new-array v1, v0, [I

    move v5, v6

    .line 544
    :goto_3
    array-length v0, p1

    if-ge v5, v0, :cond_3

    .line 545
    aget v9, v7, v5

    .line 546
    new-instance v11, Lcom/google/android/exoplayer2/source/q;

    aget-object v0, v8, v5

    .line 547
    invoke-static {v0, v9}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/exoplayer2/source/p;

    invoke-direct {v11, v0}, Lcom/google/android/exoplayer2/source/q;-><init>([Lcom/google/android/exoplayer2/source/p;)V

    aput-object v11, v2, v5

    .line 548
    aget-object v0, v4, v5

    invoke-static {v0, v9}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    aput-object v0, v4, v5

    .line 549
    aget-object v0, p1, v5

    invoke-interface {v0}, Lcom/google/android/exoplayer2/q;->a()I

    move-result v0

    aput v0, v1, v5

    .line 544
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_3

    .line 553
    :cond_3
    array-length v0, p1

    aget v0, v7, v0

    .line 554
    new-instance v5, Lcom/google/android/exoplayer2/source/q;

    array-length v7, p1

    aget-object v7, v8, v7

    invoke-static {v7, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/exoplayer2/source/p;

    invoke-direct {v5, v0}, Lcom/google/android/exoplayer2/source/q;-><init>([Lcom/google/android/exoplayer2/source/p;)V

    .line 557
    invoke-virtual {p0, p1, v2, v4}, Lcom/google/android/exoplayer2/b/d;->a([Lcom/google/android/exoplayer2/q;[Lcom/google/android/exoplayer2/source/q;[[[I)[Lcom/google/android/exoplayer2/b/e;

    move-result-object v9

    move v7, v6

    .line 561
    :goto_4
    array-length v0, p1

    if-ge v7, v0, :cond_7

    .line 562
    iget-object v0, p0, Lcom/google/android/exoplayer2/b/d;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, v7}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 563
    aput-object v10, v9, v7

    .line 561
    :cond_4
    :goto_5
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_4

    .line 565
    :cond_5
    aget-object v8, v2, v7

    .line 566
    invoke-virtual {p0, v7, v8}, Lcom/google/android/exoplayer2/b/d;->a(ILcom/google/android/exoplayer2/source/q;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 567
    iget-object v0, p0, Lcom/google/android/exoplayer2/b/d;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/b/d$b;

    .line 568
    if-nez v0, :cond_6

    move-object v0, v10

    .line 569
    :goto_6
    aput-object v0, v9, v7

    goto :goto_5

    :cond_6
    invoke-virtual {v0, v8}, Lcom/google/android/exoplayer2/b/d$b;->a(Lcom/google/android/exoplayer2/source/q;)Lcom/google/android/exoplayer2/b/e;

    move-result-object v0

    goto :goto_6

    .line 575
    :cond_7
    new-instance v0, Lcom/google/android/exoplayer2/b/d$a;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/b/d$a;-><init>([I[Lcom/google/android/exoplayer2/source/q;[I[[[ILcom/google/android/exoplayer2/source/q;)V

    .line 581
    array-length v1, p1

    new-array v8, v1, [Lcom/google/android/exoplayer2/r;

    .line 583
    :goto_7
    array-length v1, p1

    if-ge v6, v1, :cond_9

    .line 584
    aget-object v1, v9, v6

    if-eqz v1, :cond_8

    sget-object v1, Lcom/google/android/exoplayer2/r;->a:Lcom/google/android/exoplayer2/r;

    :goto_8
    aput-object v1, v8, v6

    .line 583
    add-int/lit8 v6, v6, 0x1

    goto :goto_7

    :cond_8
    move-object v1, v10

    .line 584
    goto :goto_8

    .line 587
    :cond_9
    iget v10, p0, Lcom/google/android/exoplayer2/b/d;->c:I

    move-object v5, p1

    move-object v6, v2

    move-object v7, v4

    invoke-static/range {v5 .. v10}, Lcom/google/android/exoplayer2/b/d;->a([Lcom/google/android/exoplayer2/q;[Lcom/google/android/exoplayer2/source/q;[[[I[Lcom/google/android/exoplayer2/r;[Lcom/google/android/exoplayer2/b/e;I)V

    .line 590
    new-instance v1, Lcom/google/android/exoplayer2/b/h;

    new-instance v2, Lcom/google/android/exoplayer2/b/f;

    invoke-direct {v2, v9}, Lcom/google/android/exoplayer2/b/f;-><init>([Lcom/google/android/exoplayer2/b/e;)V

    invoke-direct {v1, p2, v2, v0, v8}, Lcom/google/android/exoplayer2/b/h;-><init>(Lcom/google/android/exoplayer2/source/q;Lcom/google/android/exoplayer2/b/f;Ljava/lang/Object;[Lcom/google/android/exoplayer2/r;)V

    return-object v1
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 596
    check-cast p1, Lcom/google/android/exoplayer2/b/d$a;

    iput-object p1, p0, Lcom/google/android/exoplayer2/b/d;->d:Lcom/google/android/exoplayer2/b/d$a;

    .line 597
    return-void
.end method

.method public final a(ILcom/google/android/exoplayer2/source/q;)Z
    .locals 1

    .prologue
    .line 427
    iget-object v0, p0, Lcom/google/android/exoplayer2/b/d;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 428
    if-eqz v0, :cond_0

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract a([Lcom/google/android/exoplayer2/q;[Lcom/google/android/exoplayer2/source/q;[[[I)[Lcom/google/android/exoplayer2/b/e;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation
.end method
