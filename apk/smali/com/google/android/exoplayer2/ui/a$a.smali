.class final Lcom/google/android/exoplayer2/ui/a$a;
.super Ljava/lang/Object;
.source "PlaybackControlView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/exoplayer2/o$a;
.implements Lcom/google/android/exoplayer2/ui/d$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/ui/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/exoplayer2/ui/a;


# direct methods
.method private constructor <init>(Lcom/google/android/exoplayer2/ui/a;)V
    .locals 0

    .prologue
    .line 1018
    iput-object p1, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/exoplayer2/ui/a;Lcom/google/android/exoplayer2/ui/a$1;)V
    .locals 0

    .prologue
    .line 1018
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/ui/a$a;-><init>(Lcom/google/android/exoplayer2/ui/a;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 1057
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/a;->j(Lcom/google/android/exoplayer2/ui/a;)V

    .line 1058
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/a;->a(Lcom/google/android/exoplayer2/ui/a;)V

    .line 1059
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 1051
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/a;->i(Lcom/google/android/exoplayer2/ui/a;)V

    .line 1052
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/a;->j(Lcom/google/android/exoplayer2/ui/a;)V

    .line 1053
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/ExoPlaybackException;)V
    .locals 0

    .prologue
    .line 1086
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/n;)V
    .locals 0

    .prologue
    .line 1064
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/source/q;Lcom/google/android/exoplayer2/b/f;)V
    .locals 0

    .prologue
    .line 1081
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/u;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1068
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/a;->j(Lcom/google/android/exoplayer2/ui/a;)V

    .line 1069
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/a;->k(Lcom/google/android/exoplayer2/ui/a;)V

    .line 1070
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/a;->a(Lcom/google/android/exoplayer2/ui/a;)V

    .line 1071
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/ui/d;J)V
    .locals 2

    .prologue
    .line 1023
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v1}, Lcom/google/android/exoplayer2/ui/a;->b(Lcom/google/android/exoplayer2/ui/a;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/ui/a;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1024
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/ui/a;->a(Lcom/google/android/exoplayer2/ui/a;Z)Z

    .line 1025
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/ui/d;JZ)V
    .locals 2

    .prologue
    .line 1036
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/ui/a;->a(Lcom/google/android/exoplayer2/ui/a;Z)Z

    .line 1037
    if-nez p4, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/a;->f(Lcom/google/android/exoplayer2/ui/a;)Lcom/google/android/exoplayer2/o;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1038
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0, p2, p3}, Lcom/google/android/exoplayer2/ui/a;->a(Lcom/google/android/exoplayer2/ui/a;J)V

    .line 1040
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/a;->g(Lcom/google/android/exoplayer2/ui/a;)V

    .line 1041
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 1076
    return-void
.end method

.method public a(ZI)V
    .locals 1

    .prologue
    .line 1045
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/a;->h(Lcom/google/android/exoplayer2/ui/a;)V

    .line 1046
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/a;->a(Lcom/google/android/exoplayer2/ui/a;)V

    .line 1047
    return-void
.end method

.method public b(Lcom/google/android/exoplayer2/ui/d;J)V
    .locals 4

    .prologue
    .line 1029
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/a;->c(Lcom/google/android/exoplayer2/ui/a;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1030
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/a;->c(Lcom/google/android/exoplayer2/ui/a;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v1}, Lcom/google/android/exoplayer2/ui/a;->d(Lcom/google/android/exoplayer2/ui/a;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v2}, Lcom/google/android/exoplayer2/ui/a;->e(Lcom/google/android/exoplayer2/ui/a;)Ljava/util/Formatter;

    move-result-object v2

    invoke-static {v1, v2, p2, p3}, Lcom/google/android/exoplayer2/util/s;->a(Ljava/lang/StringBuilder;Ljava/util/Formatter;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1032
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1090
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/a;->f(Lcom/google/android/exoplayer2/ui/a;)Lcom/google/android/exoplayer2/o;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1091
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/a;->l(Lcom/google/android/exoplayer2/ui/a;)Landroid/view/View;

    move-result-object v0

    if-ne v0, p1, :cond_1

    .line 1092
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/a;->m(Lcom/google/android/exoplayer2/ui/a;)V

    .line 1108
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/a;->g(Lcom/google/android/exoplayer2/ui/a;)V

    .line 1109
    return-void

    .line 1093
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/a;->n(Lcom/google/android/exoplayer2/ui/a;)Landroid/view/View;

    move-result-object v0

    if-ne v0, p1, :cond_2

    .line 1094
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/a;->o(Lcom/google/android/exoplayer2/ui/a;)V

    goto :goto_0

    .line 1095
    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/a;->p(Lcom/google/android/exoplayer2/ui/a;)Landroid/view/View;

    move-result-object v0

    if-ne v0, p1, :cond_3

    .line 1096
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/a;->q(Lcom/google/android/exoplayer2/ui/a;)V

    goto :goto_0

    .line 1097
    :cond_3
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/a;->r(Lcom/google/android/exoplayer2/ui/a;)Landroid/view/View;

    move-result-object v0

    if-ne v0, p1, :cond_4

    .line 1098
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/a;->s(Lcom/google/android/exoplayer2/ui/a;)V

    goto :goto_0

    .line 1099
    :cond_4
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/a;->t(Lcom/google/android/exoplayer2/ui/a;)Landroid/view/View;

    move-result-object v0

    if-ne v0, p1, :cond_5

    .line 1100
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/a;->u(Lcom/google/android/exoplayer2/ui/a;)Lcom/google/android/exoplayer2/ui/a$b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v1}, Lcom/google/android/exoplayer2/ui/a;->f(Lcom/google/android/exoplayer2/ui/a;)Lcom/google/android/exoplayer2/o;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/google/android/exoplayer2/ui/a$b;->a(Lcom/google/android/exoplayer2/o;Z)Z

    goto :goto_0

    .line 1101
    :cond_5
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/a;->v(Lcom/google/android/exoplayer2/ui/a;)Landroid/view/View;

    move-result-object v0

    if-ne v0, p1, :cond_6

    .line 1102
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/a;->u(Lcom/google/android/exoplayer2/ui/a;)Lcom/google/android/exoplayer2/ui/a$b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v1}, Lcom/google/android/exoplayer2/ui/a;->f(Lcom/google/android/exoplayer2/ui/a;)Lcom/google/android/exoplayer2/o;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/exoplayer2/ui/a$b;->a(Lcom/google/android/exoplayer2/o;Z)Z

    goto :goto_0

    .line 1103
    :cond_6
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/a;->w(Lcom/google/android/exoplayer2/ui/a;)Landroid/widget/ImageView;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 1104
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/a;->u(Lcom/google/android/exoplayer2/ui/a;)Lcom/google/android/exoplayer2/ui/a$b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v1}, Lcom/google/android/exoplayer2/ui/a;->f(Lcom/google/android/exoplayer2/ui/a;)Lcom/google/android/exoplayer2/o;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    .line 1105
    invoke-static {v2}, Lcom/google/android/exoplayer2/ui/a;->f(Lcom/google/android/exoplayer2/ui/a;)Lcom/google/android/exoplayer2/o;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/exoplayer2/o;->c()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/exoplayer2/ui/a$a;->a:Lcom/google/android/exoplayer2/ui/a;

    invoke-static {v3}, Lcom/google/android/exoplayer2/ui/a;->x(Lcom/google/android/exoplayer2/ui/a;)I

    move-result v3

    .line 1104
    invoke-static {v2, v3}, Lcom/google/android/exoplayer2/util/n;->a(II)I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/exoplayer2/ui/a$b;->a(Lcom/google/android/exoplayer2/o;I)Z

    goto/16 :goto_0
.end method
