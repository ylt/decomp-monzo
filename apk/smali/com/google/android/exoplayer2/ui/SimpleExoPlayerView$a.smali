.class final Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView$a;
.super Ljava/lang/Object;
.source "SimpleExoPlayerView.java"

# interfaces
.implements Lcom/google/android/exoplayer2/o$a;
.implements Lcom/google/android/exoplayer2/t$b;
.implements Lcom/google/android/exoplayer2/text/j$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;


# direct methods
.method private constructor <init>(Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;)V
    .locals 0

    .prologue
    .line 844
    iput-object p1, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView$a;->a:Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView$1;)V
    .locals 0

    .prologue
    .line 844
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView$a;-><init>(Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 904
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 894
    return-void
.end method

.method public a(IIIF)V
    .locals 2

    .prologue
    .line 861
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView$a;->a:Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->b(Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;)Lcom/google/android/exoplayer2/ui/AspectRatioFrameLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 862
    if-nez p2, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    .line 863
    :goto_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView$a;->a:Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;

    invoke-static {v1}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->b(Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;)Lcom/google/android/exoplayer2/ui/AspectRatioFrameLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/exoplayer2/ui/AspectRatioFrameLayout;->setAspectRatio(F)V

    .line 865
    :cond_0
    return-void

    .line 862
    :cond_1
    int-to-float v0, p1

    mul-float/2addr v0, p4

    int-to-float v1, p2

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method public a(Lcom/google/android/exoplayer2/ExoPlaybackException;)V
    .locals 0

    .prologue
    .line 899
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/n;)V
    .locals 0

    .prologue
    .line 909
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/source/q;Lcom/google/android/exoplayer2/b/f;)V
    .locals 1

    .prologue
    .line 876
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView$a;->a:Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->d(Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;)V

    .line 877
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/u;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 914
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer2/text/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 851
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView$a;->a:Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->a(Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;)Lcom/google/android/exoplayer2/ui/SubtitleView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 852
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView$a;->a:Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->a(Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;)Lcom/google/android/exoplayer2/ui/SubtitleView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/ui/SubtitleView;->a(Ljava/util/List;)V

    .line 854
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 884
    return-void
.end method

.method public a(ZI)V
    .locals 2

    .prologue
    .line 888
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView$a;->a:Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->a(Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;Z)V

    .line 889
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 869
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView$a;->a:Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->c(Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 870
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView$a;->a:Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->c(Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 872
    :cond_0
    return-void
.end method
