.class public final Lcom/google/android/exoplayer2/ui/b$f;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/ui/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "f"
.end annotation


# static fields
.field public static final AspectRatioFrameLayout:[I

.field public static final AspectRatioFrameLayout_resize_mode:I = 0x0

.field public static final DefaultTimeBar:[I

.field public static final DefaultTimeBar_ad_marker_color:I = 0xa

.field public static final DefaultTimeBar_ad_marker_width:I = 0x2

.field public static final DefaultTimeBar_bar_height:I = 0x0

.field public static final DefaultTimeBar_buffered_color:I = 0x8

.field public static final DefaultTimeBar_played_ad_marker_color:I = 0xb

.field public static final DefaultTimeBar_played_color:I = 0x6

.field public static final DefaultTimeBar_scrubber_color:I = 0x7

.field public static final DefaultTimeBar_scrubber_disabled_size:I = 0x4

.field public static final DefaultTimeBar_scrubber_dragged_size:I = 0x5

.field public static final DefaultTimeBar_scrubber_enabled_size:I = 0x3

.field public static final DefaultTimeBar_touch_target_height:I = 0x1

.field public static final DefaultTimeBar_unplayed_color:I = 0x9

.field public static final PlaybackControlView:[I

.field public static final PlaybackControlView_controller_layout_id:I = 0x0

.field public static final PlaybackControlView_fastforward_increment:I = 0x1

.field public static final PlaybackControlView_repeat_toggle_modes:I = 0x2

.field public static final PlaybackControlView_rewind_increment:I = 0x3

.field public static final PlaybackControlView_show_timeout:I = 0x4

.field public static final SimpleExoPlayerView:[I

.field public static final SimpleExoPlayerView_auto_show:I = 0xb

.field public static final SimpleExoPlayerView_controller_layout_id:I = 0x0

.field public static final SimpleExoPlayerView_default_artwork:I = 0x8

.field public static final SimpleExoPlayerView_fastforward_increment:I = 0x1

.field public static final SimpleExoPlayerView_hide_on_touch:I = 0xa

.field public static final SimpleExoPlayerView_player_layout_id:I = 0x2

.field public static final SimpleExoPlayerView_resize_mode:I = 0x3

.field public static final SimpleExoPlayerView_rewind_increment:I = 0x4

.field public static final SimpleExoPlayerView_show_timeout:I = 0x5

.field public static final SimpleExoPlayerView_surface_type:I = 0x6

.field public static final SimpleExoPlayerView_use_artwork:I = 0x7

.field public static final SimpleExoPlayerView_use_controller:I = 0x9


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0xc

    .line 110
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f01003b

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/exoplayer2/ui/b$f;->AspectRatioFrameLayout:[I

    .line 112
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/exoplayer2/ui/b$f;->DefaultTimeBar:[I

    .line 125
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/exoplayer2/ui/b$f;->PlaybackControlView:[I

    .line 131
    new-array v0, v3, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/exoplayer2/ui/b$f;->SimpleExoPlayerView:[I

    return-void

    .line 112
    :array_0
    .array-data 4
        0x7f010145
        0x7f010146
        0x7f010147
        0x7f010148
        0x7f010149
        0x7f01014a
        0x7f01014b
        0x7f01014c
        0x7f01014d
        0x7f01014e
        0x7f01014f
        0x7f010150
    .end array-data

    .line 125
    :array_1
    .array-data 4
        0x7f010005
        0x7f010007
        0x7f01003a
        0x7f01003c
        0x7f01003d
    .end array-data

    .line 131
    :array_2
    .array-data 4
        0x7f010005
        0x7f010007
        0x7f010039
        0x7f01003b
        0x7f01003c
        0x7f01003d
        0x7f01003e
        0x7f0101f5
        0x7f0101f6
        0x7f0101f7
        0x7f0101f8
        0x7f0101f9
    .end array-data
.end method
