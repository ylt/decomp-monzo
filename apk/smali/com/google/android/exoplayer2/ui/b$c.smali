.class public final Lcom/google/android/exoplayer2/ui/b$c;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/ui/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation


# static fields
.field public static final all:I = 0x7f11003b

.field public static final exo_artwork:I = 0x7f110004

.field public static final exo_content_frame:I = 0x7f110005

.field public static final exo_controller:I = 0x7f110006

.field public static final exo_controller_placeholder:I = 0x7f110007

.field public static final exo_duration:I = 0x7f110008

.field public static final exo_ffwd:I = 0x7f110009

.field public static final exo_next:I = 0x7f11000a

.field public static final exo_overlay:I = 0x7f11000b

.field public static final exo_pause:I = 0x7f11000c

.field public static final exo_play:I = 0x7f11000d

.field public static final exo_position:I = 0x7f11000e

.field public static final exo_prev:I = 0x7f11000f

.field public static final exo_progress:I = 0x7f110010

.field public static final exo_repeat_toggle:I = 0x7f110011

.field public static final exo_rew:I = 0x7f110012

.field public static final exo_shutter:I = 0x7f110013

.field public static final exo_subtitles:I = 0x7f110014

.field public static final fill:I = 0x7f110040

.field public static final fit:I = 0x7f110041

.field public static final fixed_height:I = 0x7f110042

.field public static final fixed_width:I = 0x7f110043

.field public static final none:I = 0x7f11003e

.field public static final one:I = 0x7f11003f

.field public static final surface_view:I = 0x7f110044

.field public static final texture_view:I = 0x7f110045
