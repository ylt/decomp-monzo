.class public final Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;
.super Landroid/widget/FrameLayout;
.source "SimpleExoPlayerView.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/exoplayer2/ui/AspectRatioFrameLayout;

.field private final b:Landroid/view/View;

.field private final c:Landroid/view/View;

.field private final d:Landroid/widget/ImageView;

.field private final e:Lcom/google/android/exoplayer2/ui/SubtitleView;

.field private final f:Lcom/google/android/exoplayer2/ui/a;

.field private final g:Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView$a;

.field private final h:Landroid/widget/FrameLayout;

.field private i:Lcom/google/android/exoplayer2/t;

.field private j:Z

.field private k:Z

.field private l:Landroid/graphics/Bitmap;

.field private m:I

.field private n:Z

.field private o:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 223
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 224
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 227
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 228
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 16

    .prologue
    .line 231
    invoke-direct/range {p0 .. p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 233
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->isInEditMode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 234
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->a:Lcom/google/android/exoplayer2/ui/AspectRatioFrameLayout;

    .line 235
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->b:Landroid/view/View;

    .line 236
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->c:Landroid/view/View;

    .line 237
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->d:Landroid/widget/ImageView;

    .line 238
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->e:Lcom/google/android/exoplayer2/ui/SubtitleView;

    .line 239
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    .line 240
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->g:Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView$a;

    .line 241
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->h:Landroid/widget/FrameLayout;

    .line 242
    new-instance v2, Landroid/widget/ImageView;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v2, v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 243
    sget v3, Lcom/google/android/exoplayer2/util/s;->a:I

    const/16 v4, 0x17

    if-lt v3, v4, :cond_0

    .line 244
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->a(Landroid/content/res/Resources;Landroid/widget/ImageView;)V

    .line 248
    :goto_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->addView(Landroid/view/View;)V

    .line 348
    :goto_1
    return-void

    .line 246
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->b(Landroid/content/res/Resources;Landroid/widget/ImageView;)V

    goto :goto_0

    .line 252
    :cond_1
    sget v10, Lcom/google/android/exoplayer2/ui/b$d;->exo_simple_player_view:I

    .line 253
    const/4 v9, 0x1

    .line 254
    const/4 v8, 0x0

    .line 255
    const/4 v7, 0x1

    .line 256
    const/4 v6, 0x1

    .line 257
    const/4 v5, 0x0

    .line 258
    const/16 v4, 0x1388

    .line 259
    const/4 v3, 0x1

    .line 260
    const/4 v2, 0x1

    .line 261
    if-eqz p2, :cond_c

    .line 262
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v11

    sget-object v12, Lcom/google/android/exoplayer2/ui/b$f;->SimpleExoPlayerView:[I

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v11, v0, v12, v13, v14}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v11

    .line 265
    :try_start_0
    sget v12, Lcom/google/android/exoplayer2/ui/b$f;->SimpleExoPlayerView_player_layout_id:I

    invoke-virtual {v11, v12, v10}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v10

    .line 267
    sget v12, Lcom/google/android/exoplayer2/ui/b$f;->SimpleExoPlayerView_use_artwork:I

    invoke-virtual {v11, v12, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v9

    .line 268
    sget v12, Lcom/google/android/exoplayer2/ui/b$f;->SimpleExoPlayerView_default_artwork:I

    invoke-virtual {v11, v12, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v8

    .line 270
    sget v12, Lcom/google/android/exoplayer2/ui/b$f;->SimpleExoPlayerView_use_controller:I

    invoke-virtual {v11, v12, v7}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v7

    .line 271
    sget v12, Lcom/google/android/exoplayer2/ui/b$f;->SimpleExoPlayerView_surface_type:I

    invoke-virtual {v11, v12, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v6

    .line 272
    sget v12, Lcom/google/android/exoplayer2/ui/b$f;->SimpleExoPlayerView_resize_mode:I

    invoke-virtual {v11, v12, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v5

    .line 273
    sget v12, Lcom/google/android/exoplayer2/ui/b$f;->SimpleExoPlayerView_show_timeout:I

    invoke-virtual {v11, v12, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    .line 275
    sget v12, Lcom/google/android/exoplayer2/ui/b$f;->SimpleExoPlayerView_hide_on_touch:I

    invoke-virtual {v11, v12, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    .line 277
    sget v12, Lcom/google/android/exoplayer2/ui/b$f;->SimpleExoPlayerView_auto_show:I

    invoke-virtual {v11, v12, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 280
    invoke-virtual {v11}, Landroid/content/res/TypedArray;->recycle()V

    move v15, v2

    move v2, v10

    move v10, v9

    move v9, v8

    move v8, v7

    move v7, v6

    move v6, v5

    move v5, v4

    move v4, v3

    move v3, v15

    .line 284
    :goto_2
    invoke-static/range {p1 .. p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v11

    move-object/from16 v0, p0

    invoke-virtual {v11, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 285
    new-instance v2, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView$a;

    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v11}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView$a;-><init>(Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView$1;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->g:Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView$a;

    .line 286
    const/high16 v2, 0x40000

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->setDescendantFocusability(I)V

    .line 289
    sget v2, Lcom/google/android/exoplayer2/ui/b$c;->exo_content_frame:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/ui/AspectRatioFrameLayout;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->a:Lcom/google/android/exoplayer2/ui/AspectRatioFrameLayout;

    .line 290
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->a:Lcom/google/android/exoplayer2/ui/AspectRatioFrameLayout;

    if-eqz v2, :cond_2

    .line 291
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->a:Lcom/google/android/exoplayer2/ui/AspectRatioFrameLayout;

    invoke-static {v2, v6}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->a(Lcom/google/android/exoplayer2/ui/AspectRatioFrameLayout;I)V

    .line 295
    :cond_2
    sget v2, Lcom/google/android/exoplayer2/ui/b$c;->exo_shutter:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->b:Landroid/view/View;

    .line 298
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->a:Lcom/google/android/exoplayer2/ui/AspectRatioFrameLayout;

    if-eqz v2, :cond_6

    if-eqz v7, :cond_6

    .line 299
    new-instance v6, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    const/4 v11, -0x1

    invoke-direct {v6, v2, v11}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 301
    const/4 v2, 0x2

    if-ne v7, v2, :cond_5

    new-instance v2, Landroid/view/TextureView;

    move-object/from16 v0, p1

    invoke-direct {v2, v0}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    :goto_3
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->c:Landroid/view/View;

    .line 303
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->c:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 304
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->a:Lcom/google/android/exoplayer2/ui/AspectRatioFrameLayout;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->c:Landroid/view/View;

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, Lcom/google/android/exoplayer2/ui/AspectRatioFrameLayout;->addView(Landroid/view/View;I)V

    .line 310
    :goto_4
    sget v2, Lcom/google/android/exoplayer2/ui/b$c;->exo_overlay:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->h:Landroid/widget/FrameLayout;

    .line 313
    sget v2, Lcom/google/android/exoplayer2/ui/b$c;->exo_artwork:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->d:Landroid/widget/ImageView;

    .line 314
    if-eqz v10, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->d:Landroid/widget/ImageView;

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    :goto_5
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->k:Z

    .line 315
    if-eqz v9, :cond_3

    .line 316
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->l:Landroid/graphics/Bitmap;

    .line 320
    :cond_3
    sget v2, Lcom/google/android/exoplayer2/ui/b$c;->exo_subtitles:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/ui/SubtitleView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->e:Lcom/google/android/exoplayer2/ui/SubtitleView;

    .line 321
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->e:Lcom/google/android/exoplayer2/ui/SubtitleView;

    if-eqz v2, :cond_4

    .line 322
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->e:Lcom/google/android/exoplayer2/ui/SubtitleView;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/ui/SubtitleView;->b()V

    .line 323
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->e:Lcom/google/android/exoplayer2/ui/SubtitleView;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/ui/SubtitleView;->a()V

    .line 327
    :cond_4
    sget v2, Lcom/google/android/exoplayer2/ui/b$c;->exo_controller:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/ui/a;

    .line 328
    sget v6, Lcom/google/android/exoplayer2/ui/b$c;->exo_controller_placeholder:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 329
    if-eqz v2, :cond_8

    .line 330
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    .line 343
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    if-eqz v2, :cond_a

    :goto_7
    move-object/from16 v0, p0

    iput v5, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->m:I

    .line 344
    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->o:Z

    .line 345
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->n:Z

    .line 346
    if-eqz v8, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    if-eqz v2, :cond_b

    const/4 v2, 0x1

    :goto_8
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->j:Z

    .line 347
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->a()V

    goto/16 :goto_1

    .line 280
    :catchall_0
    move-exception v2

    invoke-virtual {v11}, Landroid/content/res/TypedArray;->recycle()V

    throw v2

    .line 301
    :cond_5
    new-instance v2, Landroid/view/SurfaceView;

    move-object/from16 v0, p1

    invoke-direct {v2, v0}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    goto/16 :goto_3

    .line 306
    :cond_6
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->c:Landroid/view/View;

    goto/16 :goto_4

    .line 314
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_5

    .line 331
    :cond_8
    if-eqz v6, :cond_9

    .line 334
    new-instance v2, Lcom/google/android/exoplayer2/ui/a;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v2, v0, v1}, Lcom/google/android/exoplayer2/ui/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    .line 335
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/google/android/exoplayer2/ui/a;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 336
    invoke-virtual {v6}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 337
    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v7

    .line 338
    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 339
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    invoke-virtual {v2, v6, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    goto :goto_6

    .line 341
    :cond_9
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    goto :goto_6

    .line 343
    :cond_a
    const/4 v5, 0x0

    goto :goto_7

    .line 346
    :cond_b
    const/4 v2, 0x0

    goto :goto_8

    :cond_c
    move v15, v2

    move v2, v10

    move v10, v9

    move v9, v8

    move v8, v7

    move v7, v6

    move v6, v5

    move v5, v4

    move v4, v3

    move v3, v15

    goto/16 :goto_2
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;)Lcom/google/android/exoplayer2/ui/SubtitleView;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->e:Lcom/google/android/exoplayer2/ui/SubtitleView;

    return-object v0
.end method

.method private static a(Landroid/content/res/Resources;Landroid/widget/ImageView;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 828
    sget v0, Lcom/google/android/exoplayer2/ui/b$b;->exo_edit_mode_logo:I

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 829
    sget v0, Lcom/google/android/exoplayer2/ui/b$a;->exo_edit_mode_background_color:I

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 830
    return-void
.end method

.method private static a(Lcom/google/android/exoplayer2/ui/AspectRatioFrameLayout;I)V
    .locals 0

    .prologue
    .line 841
    invoke-virtual {p0, p1}, Lcom/google/android/exoplayer2/ui/AspectRatioFrameLayout;->setResizeMode(I)V

    .line 842
    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;Z)V
    .locals 0

    .prologue
    .line 199
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 727
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->j:Z

    if-eqz v0, :cond_1

    .line 728
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ui/a;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ui/a;->getShowTimeoutMs()I

    move-result v0

    if-gtz v0, :cond_2

    const/4 v0, 0x1

    .line 729
    :goto_0
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->b()Z

    move-result v1

    .line 730
    if-nez p1, :cond_0

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    .line 731
    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->b(Z)V

    .line 734
    :cond_1
    return-void

    .line 728
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/graphics/Bitmap;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 804
    if-eqz p1, :cond_1

    .line 805
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 806
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 807
    if-lez v1, :cond_1

    if-lez v2, :cond_1

    .line 808
    iget-object v3, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->a:Lcom/google/android/exoplayer2/ui/AspectRatioFrameLayout;

    if-eqz v3, :cond_0

    .line 809
    iget-object v3, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->a:Lcom/google/android/exoplayer2/ui/AspectRatioFrameLayout;

    int-to-float v1, v1

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {v3, v1}, Lcom/google/android/exoplayer2/ui/AspectRatioFrameLayout;->setAspectRatio(F)V

    .line 811
    :cond_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 812
    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 813
    const/4 v0, 0x1

    .line 816
    :cond_1
    return v0
.end method

.method private a(Lcom/google/android/exoplayer2/metadata/a;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 792
    move v1, v2

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/metadata/a;->a()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 793
    invoke-virtual {p1, v1}, Lcom/google/android/exoplayer2/metadata/a;->a(I)Lcom/google/android/exoplayer2/metadata/a$a;

    move-result-object v0

    .line 794
    instance-of v3, v0, Lcom/google/android/exoplayer2/metadata/b/a;

    if-eqz v3, :cond_1

    .line 795
    check-cast v0, Lcom/google/android/exoplayer2/metadata/b/a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/metadata/b/a;->d:[B

    .line 796
    array-length v1, v0

    invoke-static {v0, v2, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 797
    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->a(Landroid/graphics/Bitmap;)Z

    move-result v2

    .line 800
    :cond_0
    return v2

    .line 792
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;)Lcom/google/android/exoplayer2/ui/AspectRatioFrameLayout;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->a:Lcom/google/android/exoplayer2/ui/AspectRatioFrameLayout;

    return-object v0
.end method

.method private static b(Landroid/content/res/Resources;Landroid/widget/ImageView;)V
    .locals 1

    .prologue
    .line 834
    sget v0, Lcom/google/android/exoplayer2/ui/b$b;->exo_edit_mode_logo:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 835
    sget v0, Lcom/google/android/exoplayer2/ui/b$a;->exo_edit_mode_background_color:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 836
    return-void
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 746
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->j:Z

    if-nez v0, :cond_0

    .line 751
    :goto_0
    return-void

    .line 749
    :cond_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/exoplayer2/ui/a;->setShowTimeoutMs(I)V

    .line 750
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ui/a;->a()V

    goto :goto_0

    .line 749
    :cond_1
    iget v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->m:I

    goto :goto_1
.end method

.method private b()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 737
    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->i:Lcom/google/android/exoplayer2/t;

    if-nez v1, :cond_1

    .line 742
    :cond_0
    :goto_0
    return v0

    .line 740
    :cond_1
    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->i:Lcom/google/android/exoplayer2/t;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/t;->a()I

    move-result v1

    .line 741
    iget-boolean v2, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->n:Z

    if-eqz v2, :cond_2

    if-eq v1, v0, :cond_0

    const/4 v2, 0x4

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->i:Lcom/google/android/exoplayer2/t;

    .line 742
    invoke-virtual {v1}, Lcom/google/android/exoplayer2/t;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->b:Landroid/view/View;

    return-object v0
.end method

.method private c()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 754
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->i:Lcom/google/android/exoplayer2/t;

    if-nez v0, :cond_1

    .line 789
    :cond_0
    :goto_0
    return-void

    .line 757
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->i:Lcom/google/android/exoplayer2/t;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/t;->k()Lcom/google/android/exoplayer2/b/f;

    move-result-object v3

    move v0, v1

    .line 758
    :goto_1
    iget v2, v3, Lcom/google/android/exoplayer2/b/f;->a:I

    if-ge v0, v2, :cond_3

    .line 759
    iget-object v2, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->i:Lcom/google/android/exoplayer2/t;

    invoke-virtual {v2, v0}, Lcom/google/android/exoplayer2/t;->b(I)I

    move-result v2

    const/4 v4, 0x2

    if-ne v2, v4, :cond_2

    invoke-virtual {v3, v0}, Lcom/google/android/exoplayer2/b/f;->a(I)Lcom/google/android/exoplayer2/b/e;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 762
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->d()V

    goto :goto_0

    .line 758
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 767
    :cond_3
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->b:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 768
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 771
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->k:Z

    if-eqz v0, :cond_8

    move v0, v1

    .line 772
    :goto_2
    iget v2, v3, Lcom/google/android/exoplayer2/b/f;->a:I

    if-ge v0, v2, :cond_7

    .line 773
    invoke-virtual {v3, v0}, Lcom/google/android/exoplayer2/b/f;->a(I)Lcom/google/android/exoplayer2/b/e;

    move-result-object v4

    .line 774
    if-eqz v4, :cond_6

    move v2, v1

    .line 775
    :goto_3
    invoke-interface {v4}, Lcom/google/android/exoplayer2/b/e;->b()I

    move-result v5

    if-ge v2, v5, :cond_6

    .line 776
    invoke-interface {v4, v2}, Lcom/google/android/exoplayer2/b/e;->a(I)Lcom/google/android/exoplayer2/j;

    move-result-object v5

    iget-object v5, v5, Lcom/google/android/exoplayer2/j;->d:Lcom/google/android/exoplayer2/metadata/a;

    .line 777
    if-eqz v5, :cond_5

    invoke-direct {p0, v5}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->a(Lcom/google/android/exoplayer2/metadata/a;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 775
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 772
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 783
    :cond_7
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->l:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->a(Landroid/graphics/Bitmap;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 788
    :cond_8
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->d()V

    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 820
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->d:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 821
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->d:Landroid/widget/ImageView;

    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 822
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->d:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 824
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;)V
    .locals 0

    .prologue
    .line 199
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->c()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 541
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    if-eqz v0, :cond_0

    .line 542
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ui/a;->b()V

    .line 544
    :cond_0
    return-void
.end method

.method public a(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 523
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->j:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/ui/a;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 511
    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->a(Z)V

    .line 512
    invoke-virtual {p0, p1}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->a(Landroid/view/KeyEvent;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getControllerAutoShow()Z
    .locals 1

    .prologue
    .line 593
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->n:Z

    return v0
.end method

.method public getControllerHideOnTouch()Z
    .locals 1

    .prologue
    .line 574
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->o:Z

    return v0
.end method

.method public getControllerShowTimeoutMs()I
    .locals 1

    .prologue
    .line 555
    iget v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->m:I

    return v0
.end method

.method public getDefaultArtwork()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->l:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getOverlayFrameLayout()Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 688
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->h:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public getPlayer()Lcom/google/android/exoplayer2/t;
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->i:Lcom/google/android/exoplayer2/t;

    return-object v0
.end method

.method public getSubtitleView()Lcom/google/android/exoplayer2/ui/SubtitleView;
    .locals 1

    .prologue
    .line 698
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->e:Lcom/google/android/exoplayer2/ui/SubtitleView;

    return-object v0
.end method

.method public getUseArtwork()Z
    .locals 1

    .prologue
    .line 446
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->k:Z

    return v0
.end method

.method public getUseController()Z
    .locals 1

    .prologue
    .line 486
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->j:Z

    return v0
.end method

.method public getVideoSurfaceView()Landroid/view/View;
    .locals 1

    .prologue
    .line 677
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->c:Landroid/view/View;

    return-object v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 703
    iget-boolean v1, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->j:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->i:Lcom/google/android/exoplayer2/t;

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    if-eqz v1, :cond_2

    .line 704
    :cond_0
    const/4 v0, 0x0

    .line 711
    :cond_1
    :goto_0
    return v0

    .line 706
    :cond_2
    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/ui/a;->c()Z

    move-result v1

    if-nez v1, :cond_3

    .line 707
    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->a(Z)V

    goto :goto_0

    .line 708
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->o:Z

    if-eqz v1, :cond_1

    .line 709
    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/ui/a;->b()V

    goto :goto_0
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 716
    iget-boolean v1, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->j:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->i:Lcom/google/android/exoplayer2/t;

    if-nez v1, :cond_1

    .line 717
    :cond_0
    const/4 v0, 0x0

    .line 720
    :goto_0
    return v0

    .line 719
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->a(Z)V

    goto :goto_0
.end method

.method public setControlDispatcher(Lcom/google/android/exoplayer2/ui/a$b;)V
    .locals 1

    .prologue
    .line 624
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 625
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/ui/a;->setControlDispatcher(Lcom/google/android/exoplayer2/ui/a$b;)V

    .line 626
    return-void

    .line 624
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setControllerAutoShow(Z)V
    .locals 0

    .prologue
    .line 604
    iput-boolean p1, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->n:Z

    .line 605
    return-void
.end method

.method public setControllerHideOnTouch(Z)V
    .locals 1

    .prologue
    .line 583
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 584
    iput-boolean p1, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->o:Z

    .line 585
    return-void

    .line 583
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setControllerShowTimeoutMs(I)V
    .locals 1

    .prologue
    .line 566
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 567
    iput p1, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->m:I

    .line 568
    return-void

    .line 566
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setControllerVisibilityListener(Lcom/google/android/exoplayer2/ui/a$c;)V
    .locals 1

    .prologue
    .line 613
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 614
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/ui/a;->setVisibilityListener(Lcom/google/android/exoplayer2/ui/a$c;)V

    .line 615
    return-void

    .line 613
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDefaultArtwork(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->l:Landroid/graphics/Bitmap;

    if-eq v0, p1, :cond_0

    .line 477
    iput-object p1, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->l:Landroid/graphics/Bitmap;

    .line 478
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->c()V

    .line 480
    :cond_0
    return-void
.end method

.method public setFastForwardIncrementMs(I)V
    .locals 1

    .prologue
    .line 646
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 647
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/ui/a;->setFastForwardIncrementMs(I)V

    .line 648
    return-void

    .line 646
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setPlayer(Lcom/google/android/exoplayer2/t;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 395
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->i:Lcom/google/android/exoplayer2/t;

    if-ne v0, p1, :cond_0

    .line 430
    :goto_0
    return-void

    .line 398
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->i:Lcom/google/android/exoplayer2/t;

    if-eqz v0, :cond_1

    .line 399
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->i:Lcom/google/android/exoplayer2/t;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->g:Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView$a;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/t;->b(Lcom/google/android/exoplayer2/o$a;)V

    .line 400
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->i:Lcom/google/android/exoplayer2/t;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->g:Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView$a;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/t;->b(Lcom/google/android/exoplayer2/text/j$a;)V

    .line 401
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->i:Lcom/google/android/exoplayer2/t;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->g:Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView$a;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/t;->b(Lcom/google/android/exoplayer2/t$b;)V

    .line 402
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->c:Landroid/view/View;

    instance-of v0, v0, Landroid/view/TextureView;

    if-eqz v0, :cond_5

    .line 403
    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->i:Lcom/google/android/exoplayer2/t;

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->c:Landroid/view/View;

    check-cast v0, Landroid/view/TextureView;

    invoke-virtual {v1, v0}, Lcom/google/android/exoplayer2/t;->b(Landroid/view/TextureView;)V

    .line 408
    :cond_1
    :goto_1
    iput-object p1, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->i:Lcom/google/android/exoplayer2/t;

    .line 409
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->j:Z

    if-eqz v0, :cond_2

    .line 410
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/ui/a;->setPlayer(Lcom/google/android/exoplayer2/o;)V

    .line 412
    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->b:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 413
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 415
    :cond_3
    if-eqz p1, :cond_7

    .line 416
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->c:Landroid/view/View;

    instance-of v0, v0, Landroid/view/TextureView;

    if-eqz v0, :cond_6

    .line 417
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->c:Landroid/view/View;

    check-cast v0, Landroid/view/TextureView;

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/t;->a(Landroid/view/TextureView;)V

    .line 421
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->g:Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView$a;

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/t$b;)V

    .line 422
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->g:Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView$a;

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/text/j$a;)V

    .line 423
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->g:Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView$a;

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/o$a;)V

    .line 424
    invoke-direct {p0, v2}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->a(Z)V

    .line 425
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->c()V

    goto :goto_0

    .line 404
    :cond_5
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->c:Landroid/view/View;

    instance-of v0, v0, Landroid/view/SurfaceView;

    if-eqz v0, :cond_1

    .line 405
    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->i:Lcom/google/android/exoplayer2/t;

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->c:Landroid/view/View;

    check-cast v0, Landroid/view/SurfaceView;

    invoke-virtual {v1, v0}, Lcom/google/android/exoplayer2/t;->b(Landroid/view/SurfaceView;)V

    goto :goto_1

    .line 418
    :cond_6
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->c:Landroid/view/View;

    instance-of v0, v0, Landroid/view/SurfaceView;

    if-eqz v0, :cond_4

    .line 419
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->c:Landroid/view/View;

    check-cast v0, Landroid/view/SurfaceView;

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/t;->a(Landroid/view/SurfaceView;)V

    goto :goto_2

    .line 427
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->a()V

    .line 428
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->d()V

    goto/16 :goto_0
.end method

.method public setRepeatToggleModes(I)V
    .locals 1

    .prologue
    .line 656
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 657
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/ui/a;->setRepeatToggleModes(I)V

    .line 658
    return-void

    .line 656
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setResizeMode(I)V
    .locals 1

    .prologue
    .line 438
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->a:Lcom/google/android/exoplayer2/ui/AspectRatioFrameLayout;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 439
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->a:Lcom/google/android/exoplayer2/ui/AspectRatioFrameLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/ui/AspectRatioFrameLayout;->setResizeMode(I)V

    .line 440
    return-void

    .line 438
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setRewindIncrementMs(I)V
    .locals 1

    .prologue
    .line 635
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 636
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/ui/a;->setRewindIncrementMs(I)V

    .line 637
    return-void

    .line 635
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setShowMultiWindowTimeBar(Z)V
    .locals 1

    .prologue
    .line 666
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 667
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/ui/a;->setShowMultiWindowTimeBar(Z)V

    .line 668
    return-void

    .line 666
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setUseArtwork(Z)V
    .locals 1

    .prologue
    .line 455
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->d:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 456
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->k:Z

    if-eq v0, p1, :cond_1

    .line 457
    iput-boolean p1, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->k:Z

    .line 458
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->c()V

    .line 460
    :cond_1
    return-void

    .line 455
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setUseController(Z)V
    .locals 2

    .prologue
    .line 496
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 497
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->j:Z

    if-ne v0, p1, :cond_3

    .line 507
    :cond_1
    :goto_1
    return-void

    .line 496
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 500
    :cond_3
    iput-boolean p1, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->j:Z

    .line 501
    if-eqz p1, :cond_4

    .line 502
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->i:Lcom/google/android/exoplayer2/t;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/ui/a;->setPlayer(Lcom/google/android/exoplayer2/o;)V

    goto :goto_1

    .line 503
    :cond_4
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    if-eqz v0, :cond_1

    .line 504
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ui/a;->b()V

    .line 505
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/SimpleExoPlayerView;->f:Lcom/google/android/exoplayer2/ui/a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/ui/a;->setPlayer(Lcom/google/android/exoplayer2/o;)V

    goto :goto_1
.end method
