.class public Lcom/google/android/exoplayer2/ui/a;
.super Landroid/widget/FrameLayout;
.source "PlaybackControlView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/ui/a$a;,
        Lcom/google/android/exoplayer2/ui/a$b;,
        Lcom/google/android/exoplayer2/ui/a$c;
    }
.end annotation


# static fields
.field public static final a:Lcom/google/android/exoplayer2/ui/a$b;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Z

.field private D:I

.field private E:I

.field private F:I

.field private G:I

.field private H:J

.field private I:[J

.field private J:[Z

.field private final K:Ljava/lang/Runnable;

.field private final L:Ljava/lang/Runnable;

.field private final b:Lcom/google/android/exoplayer2/ui/a$a;

.field private final c:Landroid/view/View;

.field private final d:Landroid/view/View;

.field private final e:Landroid/view/View;

.field private final f:Landroid/view/View;

.field private final g:Landroid/view/View;

.field private final h:Landroid/view/View;

.field private final i:Landroid/widget/ImageView;

.field private final j:Landroid/widget/TextView;

.field private final k:Landroid/widget/TextView;

.field private final l:Lcom/google/android/exoplayer2/ui/d;

.field private final m:Ljava/lang/StringBuilder;

.field private final n:Ljava/util/Formatter;

.field private final o:Lcom/google/android/exoplayer2/u$a;

.field private final p:Lcom/google/android/exoplayer2/u$b;

.field private final q:Landroid/graphics/drawable/Drawable;

.field private final r:Landroid/graphics/drawable/Drawable;

.field private final s:Landroid/graphics/drawable/Drawable;

.field private final t:Ljava/lang/String;

.field private final u:Ljava/lang/String;

.field private final v:Ljava/lang/String;

.field private w:Lcom/google/android/exoplayer2/o;

.field private x:Lcom/google/android/exoplayer2/ui/a$b;

.field private y:Lcom/google/android/exoplayer2/ui/a$c;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 169
    const-string v0, "goog.exo.ui"

    invoke-static {v0}, Lcom/google/android/exoplayer2/i;->a(Ljava/lang/String;)V

    .line 230
    new-instance v0, Lcom/google/android/exoplayer2/ui/a$1;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/ui/a$1;-><init>()V

    sput-object v0, Lcom/google/android/exoplayer2/ui/a;->a:Lcom/google/android/exoplayer2/ui/a$b;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 335
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/exoplayer2/ui/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 336
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/16 v2, 0x1388

    const/4 v4, 0x0

    .line 339
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 316
    new-instance v0, Lcom/google/android/exoplayer2/ui/a$2;

    invoke-direct {v0, p0}, Lcom/google/android/exoplayer2/ui/a$2;-><init>(Lcom/google/android/exoplayer2/ui/a;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->K:Ljava/lang/Runnable;

    .line 323
    new-instance v0, Lcom/google/android/exoplayer2/ui/a$3;

    invoke-direct {v0, p0}, Lcom/google/android/exoplayer2/ui/a$3;-><init>(Lcom/google/android/exoplayer2/ui/a;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->L:Ljava/lang/Runnable;

    .line 341
    sget v0, Lcom/google/android/exoplayer2/ui/b$d;->exo_playback_control_view:I

    .line 342
    iput v2, p0, Lcom/google/android/exoplayer2/ui/a;->D:I

    .line 343
    const/16 v1, 0x3a98

    iput v1, p0, Lcom/google/android/exoplayer2/ui/a;->E:I

    .line 344
    iput v2, p0, Lcom/google/android/exoplayer2/ui/a;->F:I

    .line 345
    iput v4, p0, Lcom/google/android/exoplayer2/ui/a;->G:I

    .line 346
    if-eqz p2, :cond_0

    .line 347
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget-object v2, Lcom/google/android/exoplayer2/ui/b$f;->PlaybackControlView:[I

    invoke-virtual {v1, p2, v2, v4, v4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 350
    :try_start_0
    sget v2, Lcom/google/android/exoplayer2/ui/b$f;->PlaybackControlView_rewind_increment:I

    iget v3, p0, Lcom/google/android/exoplayer2/ui/a;->D:I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/exoplayer2/ui/a;->D:I

    .line 351
    sget v2, Lcom/google/android/exoplayer2/ui/b$f;->PlaybackControlView_fastforward_increment:I

    iget v3, p0, Lcom/google/android/exoplayer2/ui/a;->E:I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/exoplayer2/ui/a;->E:I

    .line 353
    sget v2, Lcom/google/android/exoplayer2/ui/b$f;->PlaybackControlView_show_timeout:I

    iget v3, p0, Lcom/google/android/exoplayer2/ui/a;->F:I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/exoplayer2/ui/a;->F:I

    .line 354
    sget v2, Lcom/google/android/exoplayer2/ui/b$f;->PlaybackControlView_controller_layout_id:I

    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 356
    iget v2, p0, Lcom/google/android/exoplayer2/ui/a;->G:I

    invoke-static {v1, v2}, Lcom/google/android/exoplayer2/ui/a;->a(Landroid/content/res/TypedArray;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/exoplayer2/ui/a;->G:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 358
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 361
    :cond_0
    new-instance v1, Lcom/google/android/exoplayer2/u$a;

    invoke-direct {v1}, Lcom/google/android/exoplayer2/u$a;-><init>()V

    iput-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->o:Lcom/google/android/exoplayer2/u$a;

    .line 362
    new-instance v1, Lcom/google/android/exoplayer2/u$b;

    invoke-direct {v1}, Lcom/google/android/exoplayer2/u$b;-><init>()V

    iput-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->p:Lcom/google/android/exoplayer2/u$b;

    .line 363
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->m:Ljava/lang/StringBuilder;

    .line 364
    new-instance v1, Ljava/util/Formatter;

    iget-object v2, p0, Lcom/google/android/exoplayer2/ui/a;->m:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    iput-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->n:Ljava/util/Formatter;

    .line 365
    new-array v1, v4, [J

    iput-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->I:[J

    .line 366
    new-array v1, v4, [Z

    iput-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->J:[Z

    .line 367
    new-instance v1, Lcom/google/android/exoplayer2/ui/a$a;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/exoplayer2/ui/a$a;-><init>(Lcom/google/android/exoplayer2/ui/a;Lcom/google/android/exoplayer2/ui/a$1;)V

    iput-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->b:Lcom/google/android/exoplayer2/ui/a$a;

    .line 368
    sget-object v1, Lcom/google/android/exoplayer2/ui/a;->a:Lcom/google/android/exoplayer2/ui/a$b;

    iput-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->x:Lcom/google/android/exoplayer2/ui/a$b;

    .line 370
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v0, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 371
    const/high16 v0, 0x40000

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/ui/a;->setDescendantFocusability(I)V

    .line 373
    sget v0, Lcom/google/android/exoplayer2/ui/b$c;->exo_duration:I

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/ui/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->j:Landroid/widget/TextView;

    .line 374
    sget v0, Lcom/google/android/exoplayer2/ui/b$c;->exo_position:I

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/ui/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->k:Landroid/widget/TextView;

    .line 375
    sget v0, Lcom/google/android/exoplayer2/ui/b$c;->exo_progress:I

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/ui/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/ui/d;

    iput-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->l:Lcom/google/android/exoplayer2/ui/d;

    .line 376
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->l:Lcom/google/android/exoplayer2/ui/d;

    if-eqz v0, :cond_1

    .line 377
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->l:Lcom/google/android/exoplayer2/ui/d;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->b:Lcom/google/android/exoplayer2/ui/a$a;

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/ui/d;->setListener(Lcom/google/android/exoplayer2/ui/d$a;)V

    .line 379
    :cond_1
    sget v0, Lcom/google/android/exoplayer2/ui/b$c;->exo_play:I

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/ui/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->e:Landroid/view/View;

    .line 380
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->e:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 381
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->e:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->b:Lcom/google/android/exoplayer2/ui/a$a;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 383
    :cond_2
    sget v0, Lcom/google/android/exoplayer2/ui/b$c;->exo_pause:I

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/ui/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->f:Landroid/view/View;

    .line 384
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->f:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 385
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->f:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->b:Lcom/google/android/exoplayer2/ui/a$a;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 387
    :cond_3
    sget v0, Lcom/google/android/exoplayer2/ui/b$c;->exo_prev:I

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/ui/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->c:Landroid/view/View;

    .line 388
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->c:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 389
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->c:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->b:Lcom/google/android/exoplayer2/ui/a$a;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 391
    :cond_4
    sget v0, Lcom/google/android/exoplayer2/ui/b$c;->exo_next:I

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/ui/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->d:Landroid/view/View;

    .line 392
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->d:Landroid/view/View;

    if-eqz v0, :cond_5

    .line 393
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->d:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->b:Lcom/google/android/exoplayer2/ui/a$a;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 395
    :cond_5
    sget v0, Lcom/google/android/exoplayer2/ui/b$c;->exo_rew:I

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/ui/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->h:Landroid/view/View;

    .line 396
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->h:Landroid/view/View;

    if-eqz v0, :cond_6

    .line 397
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->h:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->b:Lcom/google/android/exoplayer2/ui/a$a;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 399
    :cond_6
    sget v0, Lcom/google/android/exoplayer2/ui/b$c;->exo_ffwd:I

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/ui/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->g:Landroid/view/View;

    .line 400
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->g:Landroid/view/View;

    if-eqz v0, :cond_7

    .line 401
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->g:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->b:Lcom/google/android/exoplayer2/ui/a$a;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 403
    :cond_7
    sget v0, Lcom/google/android/exoplayer2/ui/b$c;->exo_repeat_toggle:I

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/ui/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->i:Landroid/widget/ImageView;

    .line 404
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->i:Landroid/widget/ImageView;

    if-eqz v0, :cond_8

    .line 405
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->i:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->b:Lcom/google/android/exoplayer2/ui/a$a;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 407
    :cond_8
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 408
    sget v1, Lcom/google/android/exoplayer2/ui/b$b;->exo_controls_repeat_off:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->q:Landroid/graphics/drawable/Drawable;

    .line 409
    sget v1, Lcom/google/android/exoplayer2/ui/b$b;->exo_controls_repeat_one:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->r:Landroid/graphics/drawable/Drawable;

    .line 410
    sget v1, Lcom/google/android/exoplayer2/ui/b$b;->exo_controls_repeat_all:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->s:Landroid/graphics/drawable/Drawable;

    .line 411
    sget v1, Lcom/google/android/exoplayer2/ui/b$e;->exo_controls_repeat_off_description:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->t:Ljava/lang/String;

    .line 413
    sget v1, Lcom/google/android/exoplayer2/ui/b$e;->exo_controls_repeat_one_description:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->u:Ljava/lang/String;

    .line 415
    sget v1, Lcom/google/android/exoplayer2/ui/b$e;->exo_controls_repeat_all_description:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->v:Ljava/lang/String;

    .line 417
    return-void

    .line 358
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method private static a(Landroid/content/res/TypedArray;I)I
    .locals 1

    .prologue
    .line 422
    sget v0, Lcom/google/android/exoplayer2/ui/b$f;->PlaybackControlView_repeat_toggle_modes:I

    invoke-virtual {p0, v0, p1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    return v0
.end method

.method private a(IJ)V
    .locals 2

    .prologue
    .line 885
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->x:Lcom/google/android/exoplayer2/ui/a$b;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v0, v1, p1, p2, p3}, Lcom/google/android/exoplayer2/ui/a$b;->a(Lcom/google/android/exoplayer2/o;IJ)Z

    move-result v0

    .line 886
    if-nez v0, :cond_0

    .line 889
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/a;->j()V

    .line 891
    :cond_0
    return-void
.end method

.method private a(J)V
    .locals 1

    .prologue
    .line 881
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/o;->e()I

    move-result v0

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/exoplayer2/ui/a;->a(IJ)V

    .line 882
    return-void
.end method

.method private a(Landroid/view/View;F)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 827
    invoke-virtual {p1, p2}, Landroid/view/View;->setAlpha(F)V

    .line 828
    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/ui/a;)V
    .locals 0

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/a;->j()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/ui/a;J)V
    .locals 1

    .prologue
    .line 166
    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/ui/a;->b(J)V

    return-void
.end method

.method private a(ZLandroid/view/View;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 813
    if-nez p2, :cond_0

    .line 823
    :goto_0
    return-void

    .line 816
    :cond_0
    invoke-virtual {p2, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 817
    sget v0, Lcom/google/android/exoplayer2/util/s;->a:I

    const/16 v2, 0xb

    if-lt v0, v2, :cond_2

    .line 818
    if-eqz p1, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_1
    invoke-direct {p0, p2, v0}, Lcom/google/android/exoplayer2/ui/a;->a(Landroid/view/View;F)V

    .line 819
    invoke-virtual {p2, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 818
    :cond_1
    const v0, 0x3e99999a    # 0.3f

    goto :goto_1

    .line 821
    :cond_2
    if-eqz p1, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x4

    goto :goto_2
.end method

.method private static a(I)Z
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation

    .prologue
    .line 989
    const/16 v0, 0x5a

    if-eq p0, v0, :cond_0

    const/16 v0, 0x59

    if-eq p0, v0, :cond_0

    const/16 v0, 0x55

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7e

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7f

    if-eq p0, v0, :cond_0

    const/16 v0, 0x57

    if-eq p0, v0, :cond_0

    const/16 v0, 0x58

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/google/android/exoplayer2/u;Lcom/google/android/exoplayer2/u$b;)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1006
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/u;->b()I

    move-result v1

    const/16 v2, 0x64

    if-le v1, v2, :cond_1

    .line 1015
    :cond_0
    :goto_0
    return v0

    .line 1009
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/u;->b()I

    move-result v2

    move v1, v0

    .line 1010
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1011
    invoke-virtual {p0, v1, p1}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$b;)Lcom/google/android/exoplayer2/u$b;

    move-result-object v3

    iget-wide v4, v3, Lcom/google/android/exoplayer2/u$b;->i:J

    const-wide v6, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 1010
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1015
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/ui/a;Z)Z
    .locals 0

    .prologue
    .line 166
    iput-boolean p1, p0, Lcom/google/android/exoplayer2/ui/a;->C:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/exoplayer2/ui/a;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->L:Ljava/lang/Runnable;

    return-object v0
.end method

.method private b(J)V
    .locals 7

    .prologue
    .line 895
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/o;->l()Lcom/google/android/exoplayer2/u;

    move-result-object v1

    .line 896
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/ui/a;->B:Z

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/u;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 897
    invoke-virtual {v1}, Lcom/google/android/exoplayer2/u;->b()I

    move-result v4

    .line 898
    const/4 v0, 0x0

    .line 900
    :goto_0
    iget-object v2, p0, Lcom/google/android/exoplayer2/ui/a;->p:Lcom/google/android/exoplayer2/u$b;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$b;)Lcom/google/android/exoplayer2/u$b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/u$b;->b()J

    move-result-wide v2

    .line 901
    cmp-long v5, p1, v2

    if-gez v5, :cond_0

    .line 914
    :goto_1
    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/exoplayer2/ui/a;->a(IJ)V

    .line 915
    return-void

    .line 903
    :cond_0
    add-int/lit8 v5, v4, -0x1

    if-ne v0, v5, :cond_1

    move-wide p1, v2

    .line 906
    goto :goto_1

    .line 908
    :cond_1
    sub-long/2addr p1, v2

    .line 909
    add-int/lit8 v0, v0, 0x1

    .line 910
    goto :goto_0

    .line 912
    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/o;->e()I

    move-result v0

    goto :goto_1
.end method

.method static synthetic c(Lcom/google/android/exoplayer2/ui/a;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->k:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/exoplayer2/ui/a;)Ljava/lang/StringBuilder;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->m:Ljava/lang/StringBuilder;

    return-object v0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 600
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->L:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/ui/a;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 601
    iget v0, p0, Lcom/google/android/exoplayer2/ui/a;->F:I

    if-lez v0, :cond_1

    .line 602
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget v2, p0, Lcom/google/android/exoplayer2/ui/a;->F:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/exoplayer2/ui/a;->H:J

    .line 603
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/ui/a;->z:Z

    if-eqz v0, :cond_0

    .line 604
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->L:Ljava/lang/Runnable;

    iget v1, p0, Lcom/google/android/exoplayer2/ui/a;->F:I

    int-to-long v2, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/exoplayer2/ui/a;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 609
    :cond_0
    :goto_0
    return-void

    .line 607
    :cond_1
    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v0, p0, Lcom/google/android/exoplayer2/ui/a;->H:J

    goto :goto_0
.end method

.method static synthetic e(Lcom/google/android/exoplayer2/ui/a;)Ljava/util/Formatter;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->n:Ljava/util/Formatter;

    return-object v0
.end method

.method private e()V
    .locals 0

    .prologue
    .line 612
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/a;->f()V

    .line 613
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/a;->g()V

    .line 614
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/a;->h()V

    .line 615
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/a;->j()V

    .line 616
    return-void
.end method

.method static synthetic f(Lcom/google/android/exoplayer2/ui/a;)Lcom/google/android/exoplayer2/o;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    return-object v0
.end method

.method private f()V
    .locals 7

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 619
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/ui/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/ui/a;->z:Z

    if-nez v0, :cond_1

    .line 635
    :cond_0
    :goto_0
    return-void

    .line 623
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/o;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 624
    :goto_1
    iget-object v3, p0, Lcom/google/android/exoplayer2/ui/a;->e:Landroid/view/View;

    if-eqz v3, :cond_8

    .line 625
    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/android/exoplayer2/ui/a;->e:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->isFocused()Z

    move-result v3

    if-eqz v3, :cond_4

    move v3, v1

    :goto_2
    or-int v5, v2, v3

    .line 626
    iget-object v6, p0, Lcom/google/android/exoplayer2/ui/a;->e:Landroid/view/View;

    if-eqz v0, :cond_5

    move v3, v4

    :goto_3
    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    move v3, v5

    .line 628
    :goto_4
    iget-object v5, p0, Lcom/google/android/exoplayer2/ui/a;->f:Landroid/view/View;

    if-eqz v5, :cond_2

    .line 629
    if-nez v0, :cond_6

    iget-object v5, p0, Lcom/google/android/exoplayer2/ui/a;->f:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->isFocused()Z

    move-result v5

    if-eqz v5, :cond_6

    :goto_5
    or-int/2addr v3, v1

    .line 630
    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->f:Landroid/view/View;

    if-nez v0, :cond_7

    :goto_6
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 632
    :cond_2
    if-eqz v3, :cond_0

    .line 633
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/a;->k()V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 623
    goto :goto_1

    :cond_4
    move v3, v2

    .line 625
    goto :goto_2

    :cond_5
    move v3, v2

    .line 626
    goto :goto_3

    :cond_6
    move v1, v2

    .line 629
    goto :goto_5

    :cond_7
    move v4, v2

    .line 630
    goto :goto_6

    :cond_8
    move v3, v2

    goto :goto_4
.end method

.method private g()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 638
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/ui/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/ui/a;->z:Z

    if-nez v0, :cond_1

    .line 665
    :cond_0
    :goto_0
    return-void

    .line 641
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/o;->l()Lcom/google/android/exoplayer2/u;

    move-result-object v0

    move-object v3, v0

    .line 642
    :goto_1
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Lcom/google/android/exoplayer2/u;->a()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v2

    .line 646
    :goto_2
    if-eqz v0, :cond_b

    .line 647
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/o;->e()I

    move-result v5

    .line 648
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->p:Lcom/google/android/exoplayer2/u$b;

    invoke-virtual {v3, v5, v0}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$b;)Lcom/google/android/exoplayer2/u$b;

    .line 649
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->p:Lcom/google/android/exoplayer2/u$b;

    iget-boolean v4, v0, Lcom/google/android/exoplayer2/u$b;->d:Z

    .line 650
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/o;->c()I

    move-result v0

    invoke-virtual {v3, v5, v0}, Lcom/google/android/exoplayer2/u;->d(II)Z

    move-result v0

    if-eqz v0, :cond_2

    if-nez v4, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->p:Lcom/google/android/exoplayer2/u$b;

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/u$b;->e:Z

    if-nez v0, :cond_7

    :cond_2
    move v0, v2

    .line 652
    :goto_3
    iget-object v6, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v6}, Lcom/google/android/exoplayer2/o;->c()I

    move-result v6

    invoke-virtual {v3, v5, v6}, Lcom/google/android/exoplayer2/u;->c(II)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/exoplayer2/ui/a;->p:Lcom/google/android/exoplayer2/u$b;

    iget-boolean v3, v3, Lcom/google/android/exoplayer2/u$b;->e:Z

    if-eqz v3, :cond_8

    :cond_3
    move v3, v2

    .line 653
    :goto_4
    iget-object v5, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v5}, Lcom/google/android/exoplayer2/o;->i()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 655
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/ui/a;->b()V

    .line 658
    :cond_4
    :goto_5
    iget-object v5, p0, Lcom/google/android/exoplayer2/ui/a;->c:Landroid/view/View;

    invoke-direct {p0, v0, v5}, Lcom/google/android/exoplayer2/ui/a;->a(ZLandroid/view/View;)V

    .line 659
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->d:Landroid/view/View;

    invoke-direct {p0, v3, v0}, Lcom/google/android/exoplayer2/ui/a;->a(ZLandroid/view/View;)V

    .line 660
    iget v0, p0, Lcom/google/android/exoplayer2/ui/a;->E:I

    if-lez v0, :cond_9

    if-eqz v4, :cond_9

    move v0, v2

    :goto_6
    iget-object v3, p0, Lcom/google/android/exoplayer2/ui/a;->g:Landroid/view/View;

    invoke-direct {p0, v0, v3}, Lcom/google/android/exoplayer2/ui/a;->a(ZLandroid/view/View;)V

    .line 661
    iget v0, p0, Lcom/google/android/exoplayer2/ui/a;->D:I

    if-lez v0, :cond_a

    if-eqz v4, :cond_a

    :goto_7
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->h:Landroid/view/View;

    invoke-direct {p0, v2, v0}, Lcom/google/android/exoplayer2/ui/a;->a(ZLandroid/view/View;)V

    .line 662
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->l:Lcom/google/android/exoplayer2/ui/d;

    if-eqz v0, :cond_0

    .line 663
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->l:Lcom/google/android/exoplayer2/ui/d;

    invoke-interface {v0, v4}, Lcom/google/android/exoplayer2/ui/d;->setEnabled(Z)V

    goto/16 :goto_0

    .line 641
    :cond_5
    const/4 v0, 0x0

    move-object v3, v0

    goto :goto_1

    :cond_6
    move v0, v1

    .line 642
    goto :goto_2

    :cond_7
    move v0, v1

    .line 650
    goto :goto_3

    :cond_8
    move v3, v1

    .line 652
    goto :goto_4

    :cond_9
    move v0, v1

    .line 660
    goto :goto_6

    :cond_a
    move v2, v1

    .line 661
    goto :goto_7

    :cond_b
    move v3, v1

    move v0, v1

    move v4, v1

    goto :goto_5
.end method

.method static synthetic g(Lcom/google/android/exoplayer2/ui/a;)V
    .locals 0

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/a;->d()V

    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 668
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/ui/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/ui/a;->z:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->i:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    .line 695
    :cond_0
    :goto_0
    return-void

    .line 671
    :cond_1
    iget v0, p0, Lcom/google/android/exoplayer2/ui/a;->G:I

    if-nez v0, :cond_2

    .line 672
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->i:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 675
    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    if-nez v0, :cond_3

    .line 676
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->i:Landroid/widget/ImageView;

    invoke-direct {p0, v2, v0}, Lcom/google/android/exoplayer2/ui/a;->a(ZLandroid/view/View;)V

    goto :goto_0

    .line 679
    :cond_3
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->i:Landroid/widget/ImageView;

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer2/ui/a;->a(ZLandroid/view/View;)V

    .line 680
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/o;->c()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 694
    :goto_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 682
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->i:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 683
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->i:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 686
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->i:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->r:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 687
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->i:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->u:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 690
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->i:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 691
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->i:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 680
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic h(Lcom/google/android/exoplayer2/ui/a;)V
    .locals 0

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/a;->f()V

    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 698
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    if-nez v0, :cond_0

    .line 703
    :goto_0
    return-void

    .line 701
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/ui/a;->A:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    .line 702
    invoke-interface {v0}, Lcom/google/android/exoplayer2/o;->l()Lcom/google/android/exoplayer2/u;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->p:Lcom/google/android/exoplayer2/u$b;

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/ui/a;->a(Lcom/google/android/exoplayer2/u;Lcom/google/android/exoplayer2/u$b;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/exoplayer2/ui/a;->B:Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic i(Lcom/google/android/exoplayer2/ui/a;)V
    .locals 0

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/a;->h()V

    return-void
.end method

.method private j()V
    .locals 18

    .prologue
    .line 706
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/ui/a;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/exoplayer2/ui/a;->z:Z

    if-nez v2, :cond_1

    .line 801
    :cond_0
    :goto_0
    return-void

    .line 710
    :cond_1
    const-wide/16 v6, 0x0

    .line 711
    const-wide/16 v4, 0x0

    .line 712
    const-wide/16 v2, 0x0

    .line 713
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    if-eqz v8, :cond_5

    .line 714
    const-wide/16 v6, 0x0

    .line 715
    const-wide/16 v8, 0x0

    .line 716
    const/4 v5, 0x0

    .line 717
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v2}, Lcom/google/android/exoplayer2/o;->l()Lcom/google/android/exoplayer2/u;

    move-result-object v14

    .line 718
    invoke-virtual {v14}, Lcom/google/android/exoplayer2/u;->a()Z

    move-result v2

    if-nez v2, :cond_3

    .line 719
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v2}, Lcom/google/android/exoplayer2/o;->e()I

    move-result v3

    .line 720
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/exoplayer2/ui/a;->B:Z

    if-eqz v2, :cond_a

    const/4 v2, 0x0

    .line 721
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/exoplayer2/ui/a;->B:Z

    if-eqz v4, :cond_b

    .line 722
    invoke-virtual {v14}, Lcom/google/android/exoplayer2/u;->b()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    :goto_2
    move v13, v2

    .line 723
    :goto_3
    if-gt v13, v4, :cond_3

    .line 724
    if-ne v13, v3, :cond_2

    move-wide v6, v8

    .line 727
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/a;->p:Lcom/google/android/exoplayer2/u$b;

    invoke-virtual {v14, v13, v2}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$b;)Lcom/google/android/exoplayer2/u$b;

    .line 728
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/a;->p:Lcom/google/android/exoplayer2/u$b;

    iget-wide v10, v2, Lcom/google/android/exoplayer2/u$b;->i:J

    const-wide v16, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v2, v10, v16

    if-nez v2, :cond_d

    .line 729
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/exoplayer2/ui/a;->B:Z

    if-nez v2, :cond_c

    const/4 v2, 0x1

    :goto_4
    invoke-static {v2}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 760
    :cond_3
    invoke-static {v8, v9}, Lcom/google/android/exoplayer2/b;->a(J)J

    move-result-wide v8

    .line 761
    invoke-static {v6, v7}, Lcom/google/android/exoplayer2/b;->a(J)J

    move-result-wide v2

    .line 763
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v4}, Lcom/google/android/exoplayer2/o;->i()Z

    move-result v4

    if-eqz v4, :cond_15

    .line 764
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v4}, Lcom/google/android/exoplayer2/o;->j()J

    move-result-wide v6

    add-long/2addr v2, v6

    move-wide v6, v2

    .line 770
    :goto_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/exoplayer2/ui/a;->l:Lcom/google/android/exoplayer2/ui/d;

    if-eqz v4, :cond_4

    .line 771
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/exoplayer2/ui/a;->l:Lcom/google/android/exoplayer2/ui/d;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/exoplayer2/ui/a;->I:[J

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/exoplayer2/ui/a;->J:[Z

    invoke-interface {v4, v10, v11, v5}, Lcom/google/android/exoplayer2/ui/d;->a([J[ZI)V

    :cond_4
    move-wide v4, v2

    move-wide v2, v8

    .line 774
    :cond_5
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/exoplayer2/ui/a;->j:Landroid/widget/TextView;

    if-eqz v8, :cond_6

    .line 775
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/exoplayer2/ui/a;->j:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/exoplayer2/ui/a;->m:Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/exoplayer2/ui/a;->n:Ljava/util/Formatter;

    invoke-static {v9, v10, v2, v3}, Lcom/google/android/exoplayer2/util/s;->a(Ljava/lang/StringBuilder;Ljava/util/Formatter;J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 777
    :cond_6
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/exoplayer2/ui/a;->k:Landroid/widget/TextView;

    if-eqz v8, :cond_7

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/google/android/exoplayer2/ui/a;->C:Z

    if-nez v8, :cond_7

    .line 778
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/exoplayer2/ui/a;->k:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/exoplayer2/ui/a;->m:Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/exoplayer2/ui/a;->n:Ljava/util/Formatter;

    invoke-static {v9, v10, v6, v7}, Lcom/google/android/exoplayer2/util/s;->a(Ljava/lang/StringBuilder;Ljava/util/Formatter;J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 780
    :cond_7
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/exoplayer2/ui/a;->l:Lcom/google/android/exoplayer2/ui/d;

    if-eqz v8, :cond_8

    .line 781
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/exoplayer2/ui/a;->l:Lcom/google/android/exoplayer2/ui/d;

    invoke-interface {v8, v6, v7}, Lcom/google/android/exoplayer2/ui/d;->setPosition(J)V

    .line 782
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/exoplayer2/ui/a;->l:Lcom/google/android/exoplayer2/ui/d;

    invoke-interface {v8, v4, v5}, Lcom/google/android/exoplayer2/ui/d;->setBufferedPosition(J)V

    .line 783
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/exoplayer2/ui/a;->l:Lcom/google/android/exoplayer2/ui/d;

    invoke-interface {v4, v2, v3}, Lcom/google/android/exoplayer2/ui/d;->setDuration(J)V

    .line 787
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/a;->K:Ljava/lang/Runnable;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/ui/a;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 788
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    if-nez v2, :cond_16

    const/4 v2, 0x1

    .line 789
    :goto_6
    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    const/4 v3, 0x4

    if-eq v2, v3, :cond_0

    .line 791
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v3}, Lcom/google/android/exoplayer2/o;->b()Z

    move-result v3

    if-eqz v3, :cond_17

    const/4 v3, 0x3

    if-ne v2, v3, :cond_17

    .line 792
    const-wide/16 v2, 0x3e8

    const-wide/16 v4, 0x3e8

    rem-long v4, v6, v4

    sub-long/2addr v2, v4

    .line 793
    const-wide/16 v4, 0xc8

    cmp-long v4, v2, v4

    if-gez v4, :cond_9

    .line 794
    const-wide/16 v4, 0x3e8

    add-long/2addr v2, v4

    .line 799
    :cond_9
    :goto_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/exoplayer2/ui/a;->K:Ljava/lang/Runnable;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v2, v3}, Lcom/google/android/exoplayer2/ui/a;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    :cond_a
    move v2, v3

    .line 720
    goto/16 :goto_1

    :cond_b
    move v4, v3

    .line 722
    goto/16 :goto_2

    .line 729
    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 732
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/a;->p:Lcom/google/android/exoplayer2/u$b;

    iget v2, v2, Lcom/google/android/exoplayer2/u$b;->f:I

    :goto_8
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/exoplayer2/ui/a;->p:Lcom/google/android/exoplayer2/u$b;

    iget v10, v10, Lcom/google/android/exoplayer2/u$b;->g:I

    if-gt v2, v10, :cond_14

    .line 733
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/exoplayer2/ui/a;->o:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v14, v2, v10}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;)Lcom/google/android/exoplayer2/u$a;

    .line 734
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/exoplayer2/ui/a;->o:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v10}, Lcom/google/android/exoplayer2/u$a;->d()I

    move-result v15

    .line 735
    const/4 v10, 0x0

    move v12, v10

    :goto_9
    if-ge v12, v15, :cond_13

    .line 736
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/exoplayer2/ui/a;->o:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v10, v12}, Lcom/google/android/exoplayer2/u$a;->a(I)J

    move-result-wide v10

    .line 737
    const-wide/high16 v16, -0x8000000000000000L

    cmp-long v16, v10, v16

    if-nez v16, :cond_10

    .line 738
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/exoplayer2/ui/a;->o:Lcom/google/android/exoplayer2/u$a;

    iget-wide v10, v10, Lcom/google/android/exoplayer2/u$a;->d:J

    const-wide v16, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v10, v10, v16

    if-nez v10, :cond_f

    .line 735
    :cond_e
    :goto_a
    add-int/lit8 v10, v12, 0x1

    move v12, v10

    goto :goto_9

    .line 742
    :cond_f
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/exoplayer2/ui/a;->o:Lcom/google/android/exoplayer2/u$a;

    iget-wide v10, v10, Lcom/google/android/exoplayer2/u$a;->d:J

    .line 744
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/exoplayer2/ui/a;->o:Lcom/google/android/exoplayer2/u$a;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/exoplayer2/u$a;->c()J

    move-result-wide v16

    add-long v16, v16, v10

    .line 745
    const-wide/16 v10, 0x0

    cmp-long v10, v16, v10

    if-ltz v10, :cond_e

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/exoplayer2/ui/a;->p:Lcom/google/android/exoplayer2/u$b;

    iget-wide v10, v10, Lcom/google/android/exoplayer2/u$b;->i:J

    cmp-long v10, v16, v10

    if-gtz v10, :cond_e

    .line 746
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/exoplayer2/ui/a;->I:[J

    array-length v10, v10

    if-ne v5, v10, :cond_11

    .line 747
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/exoplayer2/ui/a;->I:[J

    array-length v10, v10

    if-nez v10, :cond_12

    const/4 v10, 0x1

    .line 748
    :goto_b
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/exoplayer2/ui/a;->I:[J

    invoke-static {v11, v10}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v11

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/google/android/exoplayer2/ui/a;->I:[J

    .line 749
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/exoplayer2/ui/a;->J:[Z

    invoke-static {v11, v10}, Ljava/util/Arrays;->copyOf([ZI)[Z

    move-result-object v10

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/google/android/exoplayer2/ui/a;->J:[Z

    .line 751
    :cond_11
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/exoplayer2/ui/a;->I:[J

    add-long v16, v16, v8

    invoke-static/range {v16 .. v17}, Lcom/google/android/exoplayer2/b;->a(J)J

    move-result-wide v16

    aput-wide v16, v10, v5

    .line 752
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/exoplayer2/ui/a;->J:[Z

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/exoplayer2/ui/a;->o:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v11, v12}, Lcom/google/android/exoplayer2/u$a;->c(I)Z

    move-result v11

    aput-boolean v11, v10, v5

    .line 753
    add-int/lit8 v5, v5, 0x1

    goto :goto_a

    .line 747
    :cond_12
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/exoplayer2/ui/a;->I:[J

    array-length v10, v10

    mul-int/lit8 v10, v10, 0x2

    goto :goto_b

    .line 732
    :cond_13
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_8

    .line 757
    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/a;->p:Lcom/google/android/exoplayer2/u$b;

    iget-wide v10, v2, Lcom/google/android/exoplayer2/u$b;->i:J

    add-long/2addr v8, v10

    .line 723
    add-int/lit8 v2, v13, 0x1

    move v13, v2

    goto/16 :goto_3

    .line 767
    :cond_15
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v4}, Lcom/google/android/exoplayer2/o;->g()J

    move-result-wide v6

    add-long/2addr v6, v2

    .line 768
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v4}, Lcom/google/android/exoplayer2/o;->h()J

    move-result-wide v10

    add-long/2addr v2, v10

    goto/16 :goto_5

    .line 788
    :cond_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v2}, Lcom/google/android/exoplayer2/o;->a()I

    move-result v2

    goto/16 :goto_6

    .line 797
    :cond_17
    const-wide/16 v2, 0x3e8

    goto/16 :goto_7
.end method

.method static synthetic j(Lcom/google/android/exoplayer2/ui/a;)V
    .locals 0

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/a;->g()V

    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    .line 804
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/o;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 805
    :goto_0
    if-nez v0, :cond_2

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->e:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 806
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 810
    :cond_0
    :goto_1
    return-void

    .line 804
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 807
    :cond_2
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 808
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_1
.end method

.method static synthetic k(Lcom/google/android/exoplayer2/ui/a;)V
    .locals 0

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/a;->i()V

    return-void
.end method

.method static synthetic l(Lcom/google/android/exoplayer2/ui/a;)Landroid/view/View;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->d:Landroid/view/View;

    return-object v0
.end method

.method private l()V
    .locals 6

    .prologue
    .line 831
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/o;->l()Lcom/google/android/exoplayer2/u;

    move-result-object v0

    .line 832
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/u;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 845
    :goto_0
    return-void

    .line 835
    :cond_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/o;->e()I

    move-result v1

    .line 836
    iget-object v2, p0, Lcom/google/android/exoplayer2/ui/a;->p:Lcom/google/android/exoplayer2/u$b;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$b;)Lcom/google/android/exoplayer2/u$b;

    .line 837
    iget-object v2, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v2}, Lcom/google/android/exoplayer2/o;->c()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/u;->b(II)I

    move-result v0

    .line 838
    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    .line 839
    invoke-interface {v1}, Lcom/google/android/exoplayer2/o;->g()J

    move-result-wide v2

    const-wide/16 v4, 0xbb8

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->p:Lcom/google/android/exoplayer2/u$b;

    iget-boolean v1, v1, Lcom/google/android/exoplayer2/u$b;->e:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->p:Lcom/google/android/exoplayer2/u$b;

    iget-boolean v1, v1, Lcom/google/android/exoplayer2/u$b;->d:Z

    if-nez v1, :cond_2

    .line 841
    :cond_1
    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/exoplayer2/ui/a;->a(IJ)V

    goto :goto_0

    .line 843
    :cond_2
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer2/ui/a;->a(J)V

    goto :goto_0
.end method

.method private m()V
    .locals 6

    .prologue
    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    .line 848
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/o;->l()Lcom/google/android/exoplayer2/u;

    move-result-object v0

    .line 849
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/u;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 859
    :cond_0
    :goto_0
    return-void

    .line 852
    :cond_1
    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/o;->e()I

    move-result v1

    .line 853
    iget-object v2, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v2}, Lcom/google/android/exoplayer2/o;->c()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/u;->a(II)I

    move-result v2

    .line 854
    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    .line 855
    invoke-direct {p0, v2, v4, v5}, Lcom/google/android/exoplayer2/ui/a;->a(IJ)V

    goto :goto_0

    .line 856
    :cond_2
    iget-object v2, p0, Lcom/google/android/exoplayer2/ui/a;->p:Lcom/google/android/exoplayer2/u$b;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$b;Z)Lcom/google/android/exoplayer2/u$b;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/u$b;->e:Z

    if-eqz v0, :cond_0

    .line 857
    invoke-direct {p0, v1, v4, v5}, Lcom/google/android/exoplayer2/ui/a;->a(IJ)V

    goto :goto_0
.end method

.method static synthetic m(Lcom/google/android/exoplayer2/ui/a;)V
    .locals 0

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/a;->m()V

    return-void
.end method

.method static synthetic n(Lcom/google/android/exoplayer2/ui/a;)Landroid/view/View;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->c:Landroid/view/View;

    return-object v0
.end method

.method private n()V
    .locals 4

    .prologue
    .line 862
    iget v0, p0, Lcom/google/android/exoplayer2/ui/a;->D:I

    if-gtz v0, :cond_0

    .line 866
    :goto_0
    return-void

    .line 865
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/o;->g()J

    move-result-wide v0

    iget v2, p0, Lcom/google/android/exoplayer2/ui/a;->D:I

    int-to-long v2, v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer2/ui/a;->a(J)V

    goto :goto_0
.end method

.method private o()V
    .locals 6

    .prologue
    .line 869
    iget v0, p0, Lcom/google/android/exoplayer2/ui/a;->E:I

    if-gtz v0, :cond_0

    .line 878
    :goto_0
    return-void

    .line 872
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/o;->f()J

    move-result-wide v2

    .line 873
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/o;->g()J

    move-result-wide v0

    iget v4, p0, Lcom/google/android/exoplayer2/ui/a;->E:I

    int-to-long v4, v4

    add-long/2addr v0, v4

    .line 874
    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v4, v2, v4

    if-eqz v4, :cond_1

    .line 875
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 877
    :cond_1
    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer2/ui/a;->a(J)V

    goto :goto_0
.end method

.method static synthetic o(Lcom/google/android/exoplayer2/ui/a;)V
    .locals 0

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/a;->l()V

    return-void
.end method

.method static synthetic p(Lcom/google/android/exoplayer2/ui/a;)Landroid/view/View;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->g:Landroid/view/View;

    return-object v0
.end method

.method static synthetic q(Lcom/google/android/exoplayer2/ui/a;)V
    .locals 0

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/a;->o()V

    return-void
.end method

.method static synthetic r(Lcom/google/android/exoplayer2/ui/a;)Landroid/view/View;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->h:Landroid/view/View;

    return-object v0
.end method

.method static synthetic s(Lcom/google/android/exoplayer2/ui/a;)V
    .locals 0

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/a;->n()V

    return-void
.end method

.method static synthetic t(Lcom/google/android/exoplayer2/ui/a;)Landroid/view/View;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->e:Landroid/view/View;

    return-object v0
.end method

.method static synthetic u(Lcom/google/android/exoplayer2/ui/a;)Lcom/google/android/exoplayer2/ui/a$b;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->x:Lcom/google/android/exoplayer2/ui/a$b;

    return-object v0
.end method

.method static synthetic v(Lcom/google/android/exoplayer2/ui/a;)Landroid/view/View;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->f:Landroid/view/View;

    return-object v0
.end method

.method static synthetic w(Lcom/google/android/exoplayer2/ui/a;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->i:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic x(Lcom/google/android/exoplayer2/ui/a;)I
    .locals 1

    .prologue
    .line 166
    iget v0, p0, Lcom/google/android/exoplayer2/ui/a;->G:I

    return v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 565
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/ui/a;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 566
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/ui/a;->setVisibility(I)V

    .line 567
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->y:Lcom/google/android/exoplayer2/ui/a$c;

    if-eqz v0, :cond_0

    .line 568
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->y:Lcom/google/android/exoplayer2/ui/a$c;

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/ui/a;->getVisibility()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/ui/a$c;->a(I)V

    .line 570
    :cond_0
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/a;->e()V

    .line 571
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/a;->k()V

    .line 574
    :cond_1
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/a;->d()V

    .line 575
    return-void
.end method

.method public a(Landroid/view/KeyEvent;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 953
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    .line 954
    iget-object v3, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    if-eqz v3, :cond_0

    invoke-static {v2}, Lcom/google/android/exoplayer2/ui/a;->a(I)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_0
    move v1, v0

    .line 984
    :cond_1
    :goto_0
    return v1

    .line 957
    :cond_2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_1

    .line 958
    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    .line 966
    :sswitch_0
    iget-object v2, p0, Lcom/google/android/exoplayer2/ui/a;->x:Lcom/google/android/exoplayer2/ui/a$b;

    iget-object v3, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    iget-object v4, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v4}, Lcom/google/android/exoplayer2/o;->b()Z

    move-result v4

    if-nez v4, :cond_3

    move v0, v1

    :cond_3
    invoke-interface {v2, v3, v0}, Lcom/google/android/exoplayer2/ui/a$b;->a(Lcom/google/android/exoplayer2/o;Z)Z

    goto :goto_0

    .line 960
    :sswitch_1
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/a;->o()V

    goto :goto_0

    .line 963
    :sswitch_2
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/a;->n()V

    goto :goto_0

    .line 969
    :sswitch_3
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->x:Lcom/google/android/exoplayer2/ui/a$b;

    iget-object v2, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v0, v2, v1}, Lcom/google/android/exoplayer2/ui/a$b;->a(Lcom/google/android/exoplayer2/o;Z)Z

    goto :goto_0

    .line 972
    :sswitch_4
    iget-object v2, p0, Lcom/google/android/exoplayer2/ui/a;->x:Lcom/google/android/exoplayer2/ui/a$b;

    iget-object v3, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v2, v3, v0}, Lcom/google/android/exoplayer2/ui/a$b;->a(Lcom/google/android/exoplayer2/o;Z)Z

    goto :goto_0

    .line 975
    :sswitch_5
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/a;->m()V

    goto :goto_0

    .line 978
    :sswitch_6
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/a;->l()V

    goto :goto_0

    .line 958
    nop

    :sswitch_data_0
    .sparse-switch
        0x55 -> :sswitch_0
        0x57 -> :sswitch_5
        0x58 -> :sswitch_6
        0x59 -> :sswitch_2
        0x5a -> :sswitch_1
        0x7e -> :sswitch_3
        0x7f -> :sswitch_4
    .end sparse-switch
.end method

.method public b()V
    .locals 2

    .prologue
    .line 581
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/ui/a;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 582
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/ui/a;->setVisibility(I)V

    .line 583
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->y:Lcom/google/android/exoplayer2/ui/a$c;

    if-eqz v0, :cond_0

    .line 584
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->y:Lcom/google/android/exoplayer2/ui/a$c;

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/ui/a;->getVisibility()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/ui/a$c;->a(I)V

    .line 586
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->K:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/ui/a;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 587
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->L:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/ui/a;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 588
    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v0, p0, Lcom/google/android/exoplayer2/ui/a;->H:J

    .line 590
    :cond_1
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 596
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/ui/a;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 942
    invoke-virtual {p0, p1}, Lcom/google/android/exoplayer2/ui/a;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPlayer()Lcom/google/android/exoplayer2/o;
    .locals 1

    .prologue
    .line 430
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    return-object v0
.end method

.method public getRepeatToggleModes()I
    .locals 1

    .prologue
    .line 535
    iget v0, p0, Lcom/google/android/exoplayer2/ui/a;->G:I

    return v0
.end method

.method public getShowTimeoutMs()I
    .locals 1

    .prologue
    .line 515
    iget v0, p0, Lcom/google/android/exoplayer2/ui/a;->F:I

    return v0
.end method

.method public onAttachedToWindow()V
    .locals 4

    .prologue
    .line 919
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 920
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/ui/a;->z:Z

    .line 921
    iget-wide v0, p0, Lcom/google/android/exoplayer2/ui/a;->H:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 922
    iget-wide v0, p0, Lcom/google/android/exoplayer2/ui/a;->H:J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 923
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_1

    .line 924
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/ui/a;->b()V

    .line 929
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/a;->e()V

    .line 930
    return-void

    .line 926
    :cond_1
    iget-object v2, p0, Lcom/google/android/exoplayer2/ui/a;->L:Ljava/lang/Runnable;

    invoke-virtual {p0, v2, v0, v1}, Lcom/google/android/exoplayer2/ui/a;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 934
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 935
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/ui/a;->z:Z

    .line 936
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->K:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/ui/a;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 937
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->L:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/ui/a;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 938
    return-void
.end method

.method public setControlDispatcher(Lcom/google/android/exoplayer2/ui/a$b;)V
    .locals 0

    .prologue
    .line 481
    if-nez p1, :cond_0

    sget-object p1, Lcom/google/android/exoplayer2/ui/a;->a:Lcom/google/android/exoplayer2/ui/a$b;

    :cond_0
    iput-object p1, p0, Lcom/google/android/exoplayer2/ui/a;->x:Lcom/google/android/exoplayer2/ui/a$b;

    .line 483
    return-void
.end method

.method public setFastForwardIncrementMs(I)V
    .locals 0

    .prologue
    .line 503
    iput p1, p0, Lcom/google/android/exoplayer2/ui/a;->E:I

    .line 504
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/a;->g()V

    .line 505
    return-void
.end method

.method public setPlayer(Lcom/google/android/exoplayer2/o;)V
    .locals 2

    .prologue
    .line 439
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    if-ne v0, p1, :cond_0

    .line 450
    :goto_0
    return-void

    .line 442
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    if-eqz v0, :cond_1

    .line 443
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->b:Lcom/google/android/exoplayer2/ui/a$a;

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/o;->b(Lcom/google/android/exoplayer2/o$a;)V

    .line 445
    :cond_1
    iput-object p1, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    .line 446
    if-eqz p1, :cond_2

    .line 447
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->b:Lcom/google/android/exoplayer2/ui/a$a;

    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/o;->a(Lcom/google/android/exoplayer2/o$a;)V

    .line 449
    :cond_2
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/a;->e()V

    goto :goto_0
.end method

.method public setRepeatToggleModes(I)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 544
    iput p1, p0, Lcom/google/android/exoplayer2/ui/a;->G:I

    .line 545
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    if-eqz v0, :cond_0

    .line 546
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/o;->c()I

    move-result v0

    .line 547
    if-nez p1, :cond_1

    if-eqz v0, :cond_1

    .line 549
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->x:Lcom/google/android/exoplayer2/ui/a$b;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/exoplayer2/ui/a$b;->a(Lcom/google/android/exoplayer2/o;I)Z

    .line 558
    :cond_0
    :goto_0
    return-void

    .line 550
    :cond_1
    if-ne p1, v2, :cond_2

    if-ne v0, v3, :cond_2

    .line 552
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->x:Lcom/google/android/exoplayer2/ui/a$b;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v0, v1, v2}, Lcom/google/android/exoplayer2/ui/a$b;->a(Lcom/google/android/exoplayer2/o;I)Z

    goto :goto_0

    .line 553
    :cond_2
    if-ne p1, v3, :cond_0

    if-ne v0, v2, :cond_0

    .line 555
    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/a;->x:Lcom/google/android/exoplayer2/ui/a$b;

    iget-object v1, p0, Lcom/google/android/exoplayer2/ui/a;->w:Lcom/google/android/exoplayer2/o;

    invoke-interface {v0, v1, v3}, Lcom/google/android/exoplayer2/ui/a$b;->a(Lcom/google/android/exoplayer2/o;I)Z

    goto :goto_0
.end method

.method public setRewindIncrementMs(I)V
    .locals 0

    .prologue
    .line 492
    iput p1, p0, Lcom/google/android/exoplayer2/ui/a;->D:I

    .line 493
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/a;->g()V

    .line 494
    return-void
.end method

.method public setShowMultiWindowTimeBar(Z)V
    .locals 0

    .prologue
    .line 461
    iput-boolean p1, p0, Lcom/google/android/exoplayer2/ui/a;->A:Z

    .line 462
    invoke-direct {p0}, Lcom/google/android/exoplayer2/ui/a;->i()V

    .line 463
    return-void
.end method

.method public setShowTimeoutMs(I)V
    .locals 0

    .prologue
    .line 526
    iput p1, p0, Lcom/google/android/exoplayer2/ui/a;->F:I

    .line 527
    return-void
.end method

.method public setVisibilityListener(Lcom/google/android/exoplayer2/ui/a$c;)V
    .locals 0

    .prologue
    .line 471
    iput-object p1, p0, Lcom/google/android/exoplayer2/ui/a;->y:Lcom/google/android/exoplayer2/ui/a$c;

    .line 472
    return-void
.end method
