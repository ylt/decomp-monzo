.class public Lcom/google/android/exoplayer2/c/e;
.super Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;
.source "MediaCodecVideoRenderer.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/c/e$b;,
        Lcom/google/android/exoplayer2/c/e$a;
    }
.end annotation


# static fields
.field private static final c:[I


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:F

.field private E:Z

.field private F:I

.field private G:J

.field private H:I

.field b:Lcom/google/android/exoplayer2/c/e$b;

.field private final d:Landroid/content/Context;

.field private final e:Lcom/google/android/exoplayer2/c/f;

.field private final f:Lcom/google/android/exoplayer2/c/g$a;

.field private final g:J

.field private final h:I

.field private final i:Z

.field private final j:[J

.field private k:[Lcom/google/android/exoplayer2/j;

.field private l:Lcom/google/android/exoplayer2/c/e$a;

.field private m:Landroid/view/Surface;

.field private n:Landroid/view/Surface;

.field private o:I

.field private p:Z

.field private q:J

.field private r:J

.field private s:I

.field private t:I

.field private u:I

.field private v:F

.field private w:I

.field private x:I

.field private y:I

.field private z:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/exoplayer2/c/e;->c:[I

    return-void

    :array_0
    .array-data 4
        0x780
        0x640
        0x5a0
        0x500
        0x3c0
        0x356
        0x280
        0x21c
        0x1e0
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/exoplayer2/mediacodec/b;JLcom/google/android/exoplayer2/drm/b;ZLandroid/os/Handler;Lcom/google/android/exoplayer2/c/g;I)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/exoplayer2/mediacodec/b;",
            "J",
            "Lcom/google/android/exoplayer2/drm/b",
            "<",
            "Lcom/google/android/exoplayer2/drm/d;",
            ">;Z",
            "Landroid/os/Handler;",
            "Lcom/google/android/exoplayer2/c/g;",
            "I)V"
        }
    .end annotation

    .prologue
    const-wide v6, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v4, -0x1

    const/high16 v3, -0x40800000    # -1.0f

    .line 168
    const/4 v2, 0x2

    invoke-direct {p0, v2, p2, p5, p6}, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;-><init>(ILcom/google/android/exoplayer2/mediacodec/b;Lcom/google/android/exoplayer2/drm/b;Z)V

    .line 169
    iput-wide p3, p0, Lcom/google/android/exoplayer2/c/e;->g:J

    .line 170
    move/from16 v0, p9

    iput v0, p0, Lcom/google/android/exoplayer2/c/e;->h:I

    .line 171
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/exoplayer2/c/e;->d:Landroid/content/Context;

    .line 172
    new-instance v2, Lcom/google/android/exoplayer2/c/f;

    invoke-direct {v2, p1}, Lcom/google/android/exoplayer2/c/f;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/exoplayer2/c/e;->e:Lcom/google/android/exoplayer2/c/f;

    .line 173
    new-instance v2, Lcom/google/android/exoplayer2/c/g$a;

    move-object/from16 v0, p7

    move-object/from16 v1, p8

    invoke-direct {v2, v0, v1}, Lcom/google/android/exoplayer2/c/g$a;-><init>(Landroid/os/Handler;Lcom/google/android/exoplayer2/c/g;)V

    iput-object v2, p0, Lcom/google/android/exoplayer2/c/e;->f:Lcom/google/android/exoplayer2/c/g$a;

    .line 174
    invoke-static {}, Lcom/google/android/exoplayer2/c/e;->K()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/exoplayer2/c/e;->i:Z

    .line 175
    const/16 v2, 0xa

    new-array v2, v2, [J

    iput-object v2, p0, Lcom/google/android/exoplayer2/c/e;->j:[J

    .line 176
    iput-wide v6, p0, Lcom/google/android/exoplayer2/c/e;->G:J

    .line 177
    iput-wide v6, p0, Lcom/google/android/exoplayer2/c/e;->q:J

    .line 178
    iput v4, p0, Lcom/google/android/exoplayer2/c/e;->w:I

    .line 179
    iput v4, p0, Lcom/google/android/exoplayer2/c/e;->x:I

    .line 180
    iput v3, p0, Lcom/google/android/exoplayer2/c/e;->z:F

    .line 181
    iput v3, p0, Lcom/google/android/exoplayer2/c/e;->v:F

    .line 182
    const/4 v2, 0x1

    iput v2, p0, Lcom/google/android/exoplayer2/c/e;->o:I

    .line 183
    invoke-direct {p0}, Lcom/google/android/exoplayer2/c/e;->G()V

    .line 184
    return-void
.end method

.method private F()V
    .locals 2

    .prologue
    .line 690
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/c/e;->p:Z

    if-eqz v0, :cond_0

    .line 691
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/e;->f:Lcom/google/android/exoplayer2/c/g$a;

    iget-object v1, p0, Lcom/google/android/exoplayer2/c/e;->m:Landroid/view/Surface;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/c/g$a;->a(Landroid/view/Surface;)V

    .line 693
    :cond_0
    return-void
.end method

.method private G()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 696
    iput v1, p0, Lcom/google/android/exoplayer2/c/e;->A:I

    .line 697
    iput v1, p0, Lcom/google/android/exoplayer2/c/e;->B:I

    .line 698
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/exoplayer2/c/e;->D:F

    .line 699
    iput v1, p0, Lcom/google/android/exoplayer2/c/e;->C:I

    .line 700
    return-void
.end method

.method private H()V
    .locals 5

    .prologue
    const/4 v1, -0x1

    .line 703
    iget v0, p0, Lcom/google/android/exoplayer2/c/e;->w:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer2/c/e;->x:I

    if-eq v0, v1, :cond_2

    :cond_0
    iget v0, p0, Lcom/google/android/exoplayer2/c/e;->A:I

    iget v1, p0, Lcom/google/android/exoplayer2/c/e;->w:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/exoplayer2/c/e;->B:I

    iget v1, p0, Lcom/google/android/exoplayer2/c/e;->x:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/exoplayer2/c/e;->C:I

    iget v1, p0, Lcom/google/android/exoplayer2/c/e;->y:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/exoplayer2/c/e;->D:F

    iget v1, p0, Lcom/google/android/exoplayer2/c/e;->z:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_2

    .line 707
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/e;->f:Lcom/google/android/exoplayer2/c/g$a;

    iget v1, p0, Lcom/google/android/exoplayer2/c/e;->w:I

    iget v2, p0, Lcom/google/android/exoplayer2/c/e;->x:I

    iget v3, p0, Lcom/google/android/exoplayer2/c/e;->y:I

    iget v4, p0, Lcom/google/android/exoplayer2/c/e;->z:F

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/exoplayer2/c/g$a;->a(IIIF)V

    .line 709
    iget v0, p0, Lcom/google/android/exoplayer2/c/e;->w:I

    iput v0, p0, Lcom/google/android/exoplayer2/c/e;->A:I

    .line 710
    iget v0, p0, Lcom/google/android/exoplayer2/c/e;->x:I

    iput v0, p0, Lcom/google/android/exoplayer2/c/e;->B:I

    .line 711
    iget v0, p0, Lcom/google/android/exoplayer2/c/e;->y:I

    iput v0, p0, Lcom/google/android/exoplayer2/c/e;->C:I

    .line 712
    iget v0, p0, Lcom/google/android/exoplayer2/c/e;->z:F

    iput v0, p0, Lcom/google/android/exoplayer2/c/e;->D:F

    .line 714
    :cond_2
    return-void
.end method

.method private I()V
    .locals 5

    .prologue
    const/4 v1, -0x1

    .line 717
    iget v0, p0, Lcom/google/android/exoplayer2/c/e;->A:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer2/c/e;->B:I

    if-eq v0, v1, :cond_1

    .line 718
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/e;->f:Lcom/google/android/exoplayer2/c/g$a;

    iget v1, p0, Lcom/google/android/exoplayer2/c/e;->A:I

    iget v2, p0, Lcom/google/android/exoplayer2/c/e;->B:I

    iget v3, p0, Lcom/google/android/exoplayer2/c/e;->C:I

    iget v4, p0, Lcom/google/android/exoplayer2/c/e;->D:F

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/exoplayer2/c/g$a;->a(IIIF)V

    .line 721
    :cond_1
    return-void
.end method

.method private J()V
    .locals 6

    .prologue
    .line 724
    iget v0, p0, Lcom/google/android/exoplayer2/c/e;->s:I

    if-lez v0, :cond_0

    .line 725
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 726
    iget-wide v2, p0, Lcom/google/android/exoplayer2/c/e;->r:J

    sub-long v2, v0, v2

    .line 727
    iget-object v4, p0, Lcom/google/android/exoplayer2/c/e;->f:Lcom/google/android/exoplayer2/c/g$a;

    iget v5, p0, Lcom/google/android/exoplayer2/c/e;->s:I

    invoke-virtual {v4, v5, v2, v3}, Lcom/google/android/exoplayer2/c/g$a;->a(IJ)V

    .line 728
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/exoplayer2/c/e;->s:I

    .line 729
    iput-wide v0, p0, Lcom/google/android/exoplayer2/c/e;->r:J

    .line 731
    :cond_0
    return-void
.end method

.method private static K()Z
    .locals 2

    .prologue
    .line 951
    sget v0, Lcom/google/android/exoplayer2/util/s;->a:I

    const/16 v1, 0x16

    if-gt v0, v1, :cond_0

    const-string v0, "foster"

    sget-object v1, Lcom/google/android/exoplayer2/util/s;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "NVIDIA"

    sget-object v1, Lcom/google/android/exoplayer2/util/s;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;II)I
    .locals 6

    .prologue
    const/16 v4, 0x10

    const/4 v1, 0x4

    const/4 v0, 0x2

    const/4 v2, -0x1

    .line 890
    if-eq p1, v2, :cond_0

    if-ne p2, v2, :cond_1

    :cond_0
    move v0, v2

    .line 929
    :goto_0
    return v0

    .line 898
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_2
    move v3, v2

    :goto_1
    packed-switch v3, :pswitch_data_0

    move v0, v2

    .line 926
    goto :goto_0

    .line 898
    :sswitch_0
    const-string v3, "video/3gpp"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x0

    goto :goto_1

    :sswitch_1
    const-string v3, "video/mp4v-es"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    goto :goto_1

    :sswitch_2
    const-string v3, "video/avc"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, v0

    goto :goto_1

    :sswitch_3
    const-string v3, "video/x-vnd.on2.vp8"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    goto :goto_1

    :sswitch_4
    const-string v3, "video/hevc"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, v1

    goto :goto_1

    :sswitch_5
    const-string v3, "video/x-vnd.on2.vp9"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x5

    goto :goto_1

    .line 901
    :pswitch_0
    mul-int v1, p1, p2

    .line 929
    :goto_2
    mul-int/lit8 v1, v1, 0x3

    mul-int/lit8 v0, v0, 0x2

    div-int v0, v1, v0

    goto :goto_0

    .line 905
    :pswitch_1
    const-string v1, "BRAVIA 4K 2015"

    sget-object v3, Lcom/google/android/exoplayer2/util/s;->d:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v0, v2

    .line 908
    goto :goto_0

    .line 911
    :cond_3
    invoke-static {p1, v4}, Lcom/google/android/exoplayer2/util/s;->a(II)I

    move-result v1

    invoke-static {p2, v4}, Lcom/google/android/exoplayer2/util/s;->a(II)I

    move-result v2

    mul-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x10

    mul-int/lit8 v1, v1, 0x10

    .line 913
    goto :goto_2

    .line 916
    :pswitch_2
    mul-int v1, p1, p2

    .line 918
    goto :goto_2

    .line 921
    :pswitch_3
    mul-int v0, p1, p2

    move v5, v1

    move v1, v0

    move v0, v5

    .line 923
    goto :goto_2

    .line 898
    :sswitch_data_0
    .sparse-switch
        -0x63306f58 -> :sswitch_0
        -0x63185e82 -> :sswitch_4
        0x46cdc642 -> :sswitch_1
        0x4f62373a -> :sswitch_2
        0x5f50bed8 -> :sswitch_3
        0x5f50bed9 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private static a(Lcom/google/android/exoplayer2/mediacodec/a;Lcom/google/android/exoplayer2/j;)Landroid/graphics/Point;
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/mediacodec/MediaCodecUtil$DecoderQueryException;
        }
    .end annotation

    .prologue
    .line 827
    iget v0, p1, Lcom/google/android/exoplayer2/j;->k:I

    iget v1, p1, Lcom/google/android/exoplayer2/j;->j:I

    if-le v0, v1, :cond_1

    const/4 v0, 0x1

    move v6, v0

    .line 828
    :goto_0
    if-eqz v6, :cond_2

    iget v0, p1, Lcom/google/android/exoplayer2/j;->k:I

    move v5, v0

    .line 829
    :goto_1
    if-eqz v6, :cond_3

    iget v0, p1, Lcom/google/android/exoplayer2/j;->j:I

    .line 830
    :goto_2
    int-to-float v1, v0

    int-to-float v2, v5

    div-float v7, v1, v2

    .line 831
    sget-object v8, Lcom/google/android/exoplayer2/c/e;->c:[I

    array-length v9, v8

    const/4 v1, 0x0

    move v4, v1

    :goto_3
    if-ge v4, v9, :cond_b

    aget v1, v8, v4

    .line 832
    int-to-float v2, v1

    mul-float/2addr v2, v7

    float-to-int v2, v2

    .line 833
    if-le v1, v5, :cond_0

    if-gt v2, v0, :cond_4

    .line 835
    :cond_0
    const/4 v0, 0x0

    .line 853
    :goto_4
    return-object v0

    .line 827
    :cond_1
    const/4 v0, 0x0

    move v6, v0

    goto :goto_0

    .line 828
    :cond_2
    iget v0, p1, Lcom/google/android/exoplayer2/j;->j:I

    move v5, v0

    goto :goto_1

    .line 829
    :cond_3
    iget v0, p1, Lcom/google/android/exoplayer2/j;->k:I

    goto :goto_2

    .line 836
    :cond_4
    sget v3, Lcom/google/android/exoplayer2/util/s;->a:I

    const/16 v10, 0x15

    if-lt v3, v10, :cond_7

    .line 837
    if-eqz v6, :cond_5

    move v3, v2

    :goto_5
    if-eqz v6, :cond_6

    :goto_6
    invoke-virtual {p0, v3, v1}, Lcom/google/android/exoplayer2/mediacodec/a;->a(II)Landroid/graphics/Point;

    move-result-object v1

    .line 839
    iget v2, p1, Lcom/google/android/exoplayer2/j;->l:F

    .line 840
    iget v3, v1, Landroid/graphics/Point;->x:I

    iget v10, v1, Landroid/graphics/Point;->y:I

    float-to-double v12, v2

    invoke-virtual {p0, v3, v10, v12, v13}, Lcom/google/android/exoplayer2/mediacodec/a;->a(IID)Z

    move-result v2

    if-eqz v2, :cond_a

    move-object v0, v1

    .line 841
    goto :goto_4

    :cond_5
    move v3, v1

    .line 837
    goto :goto_5

    :cond_6
    move v1, v2

    goto :goto_6

    .line 845
    :cond_7
    const/16 v3, 0x10

    invoke-static {v1, v3}, Lcom/google/android/exoplayer2/util/s;->a(II)I

    move-result v1

    mul-int/lit8 v1, v1, 0x10

    .line 846
    const/16 v3, 0x10

    invoke-static {v2, v3}, Lcom/google/android/exoplayer2/util/s;->a(II)I

    move-result v2

    mul-int/lit8 v2, v2, 0x10

    .line 847
    mul-int v3, v1, v2

    invoke-static {}, Lcom/google/android/exoplayer2/mediacodec/MediaCodecUtil;->b()I

    move-result v10

    if-gt v3, v10, :cond_a

    .line 848
    new-instance v3, Landroid/graphics/Point;

    if-eqz v6, :cond_8

    move v4, v2

    :goto_7
    if-eqz v6, :cond_9

    move v0, v1

    :goto_8
    invoke-direct {v3, v4, v0}, Landroid/graphics/Point;-><init>(II)V

    move-object v0, v3

    goto :goto_4

    :cond_8
    move v4, v1

    goto :goto_7

    :cond_9
    move v0, v2

    goto :goto_8

    .line 831
    :cond_a
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_3

    .line 853
    :cond_b
    const/4 v0, 0x0

    goto :goto_4
.end method

.method private static a(Lcom/google/android/exoplayer2/j;Lcom/google/android/exoplayer2/c/e$a;ZI)Landroid/media/MediaFormat;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation

    .prologue
    .line 741
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/j;->b()Landroid/media/MediaFormat;

    move-result-object v0

    .line 743
    const-string v1, "max-width"

    iget v2, p1, Lcom/google/android/exoplayer2/c/e$a;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 744
    const-string v1, "max-height"

    iget v2, p1, Lcom/google/android/exoplayer2/c/e$a;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 746
    iget v1, p1, Lcom/google/android/exoplayer2/c/e$a;->c:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 747
    const-string v1, "max-input-size"

    iget v2, p1, Lcom/google/android/exoplayer2/c/e$a;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 750
    :cond_0
    if-eqz p2, :cond_1

    .line 751
    const-string v1, "auto-frc"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 754
    :cond_1
    if-eqz p3, :cond_2

    .line 755
    invoke-static {v0, p3}, Lcom/google/android/exoplayer2/c/e;->a(Landroid/media/MediaFormat;I)V

    .line 757
    :cond_2
    return-object v0
.end method

.method private static a(Landroid/media/MediaCodec;I)V
    .locals 0

    .prologue
    .line 933
    invoke-virtual {p0, p1}, Landroid/media/MediaCodec;->setVideoScalingMode(I)V

    .line 934
    return-void
.end method

.method private static a(Landroid/media/MediaCodec;Landroid/view/Surface;)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    .line 762
    invoke-virtual {p0, p1}, Landroid/media/MediaCodec;->setOutputSurface(Landroid/view/Surface;)V

    .line 763
    return-void
.end method

.method private static a(Landroid/media/MediaFormat;I)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 767
    const-string v0, "tunneled-playback"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/media/MediaFormat;->setFeatureEnabled(Ljava/lang/String;Z)V

    .line 768
    const-string v0, "audio-session-id"

    invoke-virtual {p0, v0, p1}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 769
    return-void
.end method

.method private a(Landroid/view/Surface;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    .line 339
    if-nez p1, :cond_0

    .line 341
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/e;->n:Landroid/view/Surface;

    if-eqz v0, :cond_4

    .line 342
    iget-object p1, p0, Lcom/google/android/exoplayer2/c/e;->n:Landroid/view/Surface;

    .line 352
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/e;->m:Landroid/view/Surface;

    if-eq v0, p1, :cond_7

    .line 353
    iput-object p1, p0, Lcom/google/android/exoplayer2/c/e;->m:Landroid/view/Surface;

    .line 354
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/c/e;->d()I

    move-result v0

    .line 355
    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v4, :cond_2

    .line 356
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/c/e;->A()Landroid/media/MediaCodec;

    move-result-object v1

    .line 357
    sget v2, Lcom/google/android/exoplayer2/util/s;->a:I

    const/16 v3, 0x17

    if-lt v2, v3, :cond_5

    if-eqz v1, :cond_5

    if-eqz p1, :cond_5

    .line 358
    invoke-static {v1, p1}, Lcom/google/android/exoplayer2/c/e;->a(Landroid/media/MediaCodec;Landroid/view/Surface;)V

    .line 364
    :cond_2
    :goto_1
    if-eqz p1, :cond_6

    iget-object v1, p0, Lcom/google/android/exoplayer2/c/e;->n:Landroid/view/Surface;

    if-eq p1, v1, :cond_6

    .line 366
    invoke-direct {p0}, Lcom/google/android/exoplayer2/c/e;->I()V

    .line 368
    invoke-direct {p0}, Lcom/google/android/exoplayer2/c/e;->x()V

    .line 369
    if-ne v0, v4, :cond_3

    .line 370
    invoke-direct {p0}, Lcom/google/android/exoplayer2/c/e;->w()V

    .line 383
    :cond_3
    :goto_2
    return-void

    .line 344
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/c/e;->B()Lcom/google/android/exoplayer2/mediacodec/a;

    move-result-object v0

    .line 345
    if-eqz v0, :cond_0

    iget-boolean v1, v0, Lcom/google/android/exoplayer2/mediacodec/a;->d:Z

    invoke-direct {p0, v1}, Lcom/google/android/exoplayer2/c/e;->b(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 346
    iget-object v1, p0, Lcom/google/android/exoplayer2/c/e;->d:Landroid/content/Context;

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/mediacodec/a;->d:Z

    invoke-static {v1, v0}, Lcom/google/android/exoplayer2/c/c;->a(Landroid/content/Context;Z)Lcom/google/android/exoplayer2/c/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/c/e;->n:Landroid/view/Surface;

    .line 347
    iget-object p1, p0, Lcom/google/android/exoplayer2/c/e;->n:Landroid/view/Surface;

    goto :goto_0

    .line 360
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/c/e;->C()V

    .line 361
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/c/e;->z()V

    goto :goto_1

    .line 374
    :cond_6
    invoke-direct {p0}, Lcom/google/android/exoplayer2/c/e;->G()V

    .line 375
    invoke-direct {p0}, Lcom/google/android/exoplayer2/c/e;->x()V

    goto :goto_2

    .line 377
    :cond_7
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/google/android/exoplayer2/c/e;->n:Landroid/view/Surface;

    if-eq p1, v0, :cond_3

    .line 380
    invoke-direct {p0}, Lcom/google/android/exoplayer2/c/e;->I()V

    .line 381
    invoke-direct {p0}, Lcom/google/android/exoplayer2/c/e;->F()V

    goto :goto_2
.end method

.method private static a(ZLcom/google/android/exoplayer2/j;Lcom/google/android/exoplayer2/j;)Z
    .locals 2

    .prologue
    .line 965
    iget-object v0, p1, Lcom/google/android/exoplayer2/j;->f:Ljava/lang/String;

    iget-object v1, p2, Lcom/google/android/exoplayer2/j;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 966
    invoke-static {p1}, Lcom/google/android/exoplayer2/c/e;->e(Lcom/google/android/exoplayer2/j;)I

    move-result v0

    invoke-static {p2}, Lcom/google/android/exoplayer2/c/e;->e(Lcom/google/android/exoplayer2/j;)I

    move-result v1

    if-ne v0, v1, :cond_1

    if-nez p0, :cond_0

    iget v0, p1, Lcom/google/android/exoplayer2/j;->j:I

    iget v1, p2, Lcom/google/android/exoplayer2/j;->j:I

    if-ne v0, v1, :cond_1

    iget v0, p1, Lcom/google/android/exoplayer2/j;->k:I

    iget v1, p2, Lcom/google/android/exoplayer2/j;->k:I

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Z)Z
    .locals 2

    .prologue
    .line 658
    sget v0, Lcom/google/android/exoplayer2/util/s;->a:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/c/e;->E:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/c/e;->d:Landroid/content/Context;

    .line 659
    invoke-static {v0}, Lcom/google/android/exoplayer2/c/c;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Lcom/google/android/exoplayer2/j;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 864
    iget v1, p0, Lcom/google/android/exoplayer2/j;->g:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 868
    iget-object v1, p0, Lcom/google/android/exoplayer2/j;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    move v2, v0

    .line 869
    :goto_0
    if-ge v1, v3, :cond_0

    .line 870
    iget-object v0, p0, Lcom/google/android/exoplayer2/j;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    array-length v0, v0

    add-int/2addr v2, v0

    .line 869
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 872
    :cond_0
    iget v0, p0, Lcom/google/android/exoplayer2/j;->g:I

    add-int/2addr v0, v2

    .line 876
    :goto_1
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/j;->f:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/exoplayer2/j;->j:I

    iget v2, p0, Lcom/google/android/exoplayer2/j;->k:I

    invoke-static {v0, v1, v2}, Lcom/google/android/exoplayer2/c/e;->a(Ljava/lang/String;II)I

    move-result v0

    goto :goto_1
.end method

.method private static d(Lcom/google/android/exoplayer2/j;)F
    .locals 2

    .prologue
    .line 971
    iget v0, p0, Lcom/google/android/exoplayer2/j;->n:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/exoplayer2/j;->n:F

    goto :goto_0
.end method

.method private static d(J)Z
    .locals 2

    .prologue
    .line 735
    const-wide/16 v0, -0x7530

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static e(Lcom/google/android/exoplayer2/j;)I
    .locals 2

    .prologue
    .line 975
    iget v0, p0, Lcom/google/android/exoplayer2/j;->m:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/exoplayer2/j;->m:I

    goto :goto_0
.end method

.method private w()V
    .locals 4

    .prologue
    .line 663
    iget-wide v0, p0, Lcom/google/android/exoplayer2/c/e;->g:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 664
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/exoplayer2/c/e;->g:J

    add-long/2addr v0, v2

    :goto_0
    iput-wide v0, p0, Lcom/google/android/exoplayer2/c/e;->q:J

    .line 665
    return-void

    .line 664
    :cond_0
    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    goto :goto_0
.end method

.method private x()V
    .locals 3

    .prologue
    .line 668
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/c/e;->p:Z

    .line 673
    sget v0, Lcom/google/android/exoplayer2/util/s;->a:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/c/e;->E:Z

    if-eqz v0, :cond_0

    .line 674
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/c/e;->A()Landroid/media/MediaCodec;

    move-result-object v0

    .line 676
    if-eqz v0, :cond_0

    .line 677
    new-instance v1, Lcom/google/android/exoplayer2/c/e$b;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v0, v2}, Lcom/google/android/exoplayer2/c/e$b;-><init>(Lcom/google/android/exoplayer2/c/e;Landroid/media/MediaCodec;Lcom/google/android/exoplayer2/c/e$1;)V

    iput-object v1, p0, Lcom/google/android/exoplayer2/c/e;->b:Lcom/google/android/exoplayer2/c/e$b;

    .line 680
    :cond_0
    return-void
.end method


# virtual methods
.method protected C()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 412
    :try_start_0
    invoke-super {p0}, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;->C()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 414
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/e;->n:Landroid/view/Surface;

    if-eqz v0, :cond_1

    .line 415
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/e;->m:Landroid/view/Surface;

    iget-object v1, p0, Lcom/google/android/exoplayer2/c/e;->n:Landroid/view/Surface;

    if-ne v0, v1, :cond_0

    .line 416
    iput-object v3, p0, Lcom/google/android/exoplayer2/c/e;->m:Landroid/view/Surface;

    .line 418
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/e;->n:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 419
    iput-object v3, p0, Lcom/google/android/exoplayer2/c/e;->n:Landroid/view/Surface;

    .line 422
    :cond_1
    return-void

    .line 414
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/c/e;->n:Landroid/view/Surface;

    if-eqz v1, :cond_3

    .line 415
    iget-object v1, p0, Lcom/google/android/exoplayer2/c/e;->m:Landroid/view/Surface;

    iget-object v2, p0, Lcom/google/android/exoplayer2/c/e;->n:Landroid/view/Surface;

    if-ne v1, v2, :cond_2

    .line 416
    iput-object v3, p0, Lcom/google/android/exoplayer2/c/e;->m:Landroid/view/Surface;

    .line 418
    :cond_2
    iget-object v1, p0, Lcom/google/android/exoplayer2/c/e;->n:Landroid/view/Surface;

    invoke-virtual {v1}, Landroid/view/Surface;->release()V

    .line 419
    iput-object v3, p0, Lcom/google/android/exoplayer2/c/e;->n:Landroid/view/Surface;

    :cond_3
    throw v0
.end method

.method protected a(Lcom/google/android/exoplayer2/mediacodec/b;Lcom/google/android/exoplayer2/j;)I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/mediacodec/MediaCodecUtil$DecoderQueryException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 189
    iget-object v4, p2, Lcom/google/android/exoplayer2/j;->f:Ljava/lang/String;

    .line 190
    invoke-static {v4}, Lcom/google/android/exoplayer2/util/h;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 223
    :goto_0
    return v1

    .line 194
    :cond_0
    iget-object v5, p2, Lcom/google/android/exoplayer2/j;->i:Lcom/google/android/exoplayer2/drm/a;

    .line 195
    if-eqz v5, :cond_1

    move v0, v1

    move v2, v1

    .line 196
    :goto_1
    iget v6, v5, Lcom/google/android/exoplayer2/drm/a;->a:I

    if-ge v0, v6, :cond_2

    .line 197
    invoke-virtual {v5, v0}, Lcom/google/android/exoplayer2/drm/a;->a(I)Lcom/google/android/exoplayer2/drm/a$a;

    move-result-object v6

    iget-boolean v6, v6, Lcom/google/android/exoplayer2/drm/a$a;->d:Z

    or-int/2addr v2, v6

    .line 196
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v2, v1

    .line 200
    :cond_2
    invoke-interface {p1, v4, v2}, Lcom/google/android/exoplayer2/mediacodec/b;->a(Ljava/lang/String;Z)Lcom/google/android/exoplayer2/mediacodec/a;

    move-result-object v4

    .line 202
    if-nez v4, :cond_3

    move v1, v3

    .line 203
    goto :goto_0

    .line 206
    :cond_3
    iget-object v0, p2, Lcom/google/android/exoplayer2/j;->c:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/google/android/exoplayer2/mediacodec/a;->b(Ljava/lang/String;)Z

    move-result v0

    .line 207
    if-eqz v0, :cond_4

    iget v2, p2, Lcom/google/android/exoplayer2/j;->j:I

    if-lez v2, :cond_4

    iget v2, p2, Lcom/google/android/exoplayer2/j;->k:I

    if-lez v2, :cond_4

    .line 208
    sget v0, Lcom/google/android/exoplayer2/util/s;->a:I

    const/16 v2, 0x15

    if-lt v0, v2, :cond_6

    .line 209
    iget v0, p2, Lcom/google/android/exoplayer2/j;->j:I

    iget v2, p2, Lcom/google/android/exoplayer2/j;->k:I

    iget v3, p2, Lcom/google/android/exoplayer2/j;->l:F

    float-to-double v6, v3

    invoke-virtual {v4, v0, v2, v6, v7}, Lcom/google/android/exoplayer2/mediacodec/a;->a(IID)Z

    move-result v0

    .line 220
    :cond_4
    :goto_2
    iget-boolean v2, v4, Lcom/google/android/exoplayer2/mediacodec/a;->b:Z

    if-eqz v2, :cond_8

    const/16 v2, 0x10

    .line 221
    :goto_3
    iget-boolean v3, v4, Lcom/google/android/exoplayer2/mediacodec/a;->c:Z

    if-eqz v3, :cond_5

    const/16 v1, 0x20

    .line 222
    :cond_5
    if-eqz v0, :cond_9

    const/4 v0, 0x4

    .line 223
    :goto_4
    or-int/2addr v1, v2

    or-int/2addr v1, v0

    goto :goto_0

    .line 212
    :cond_6
    iget v0, p2, Lcom/google/android/exoplayer2/j;->j:I

    iget v2, p2, Lcom/google/android/exoplayer2/j;->k:I

    mul-int/2addr v0, v2

    invoke-static {}, Lcom/google/android/exoplayer2/mediacodec/MediaCodecUtil;->b()I

    move-result v2

    if-gt v0, v2, :cond_7

    move v0, v3

    .line 213
    :goto_5
    if-nez v0, :cond_4

    .line 214
    const-string v2, "MediaCodecVideoRenderer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FalseCheck [legacyFrameSize, "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v5, p2, Lcom/google/android/exoplayer2/j;->j:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "x"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v5, p2, Lcom/google/android/exoplayer2/j;->k:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "] ["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v5, Lcom/google/android/exoplayer2/util/s;->e:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "]"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_7
    move v0, v1

    .line 212
    goto :goto_5

    .line 220
    :cond_8
    const/16 v2, 0x8

    goto :goto_3

    .line 222
    :cond_9
    const/4 v0, 0x3

    goto :goto_4
.end method

.method protected a(Lcom/google/android/exoplayer2/mediacodec/a;Lcom/google/android/exoplayer2/j;[Lcom/google/android/exoplayer2/j;)Lcom/google/android/exoplayer2/c/e$a;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/mediacodec/MediaCodecUtil$DecoderQueryException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v10, -0x1

    const/4 v1, 0x0

    .line 783
    iget v6, p2, Lcom/google/android/exoplayer2/j;->j:I

    .line 784
    iget v5, p2, Lcom/google/android/exoplayer2/j;->k:I

    .line 785
    invoke-static {p2}, Lcom/google/android/exoplayer2/c/e;->c(Lcom/google/android/exoplayer2/j;)I

    move-result v4

    .line 786
    array-length v0, p3

    if-ne v0, v2, :cond_0

    .line 789
    new-instance v0, Lcom/google/android/exoplayer2/c/e$a;

    invoke-direct {v0, v6, v5, v4}, Lcom/google/android/exoplayer2/c/e$a;-><init>(III)V

    .line 812
    :goto_0
    return-object v0

    .line 792
    :cond_0
    array-length v8, p3

    move v7, v1

    move v3, v1

    :goto_1
    if-ge v7, v8, :cond_3

    aget-object v9, p3, v7

    .line 793
    iget-boolean v0, p1, Lcom/google/android/exoplayer2/mediacodec/a;->b:Z

    invoke-static {v0, p2, v9}, Lcom/google/android/exoplayer2/c/e;->a(ZLcom/google/android/exoplayer2/j;Lcom/google/android/exoplayer2/j;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 794
    iget v0, v9, Lcom/google/android/exoplayer2/j;->j:I

    if-eq v0, v10, :cond_1

    iget v0, v9, Lcom/google/android/exoplayer2/j;->k:I

    if-ne v0, v10, :cond_2

    :cond_1
    move v0, v2

    :goto_2
    or-int/2addr v0, v3

    .line 796
    iget v3, v9, Lcom/google/android/exoplayer2/j;->j:I

    invoke-static {v6, v3}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 797
    iget v3, v9, Lcom/google/android/exoplayer2/j;->k:I

    invoke-static {v5, v3}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 798
    invoke-static {v9}, Lcom/google/android/exoplayer2/c/e;->c(Lcom/google/android/exoplayer2/j;)I

    move-result v3

    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    move v4, v5

    move v5, v6

    .line 792
    :goto_3
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    move v6, v5

    move v5, v4

    move v4, v3

    move v3, v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 794
    goto :goto_2

    .line 801
    :cond_3
    if-eqz v3, :cond_4

    .line 802
    const-string v0, "MediaCodecVideoRenderer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Resolutions unknown. Codec max resolution: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 803
    invoke-static {p1, p2}, Lcom/google/android/exoplayer2/c/e;->a(Lcom/google/android/exoplayer2/mediacodec/a;Lcom/google/android/exoplayer2/j;)Landroid/graphics/Point;

    move-result-object v0

    .line 804
    if-eqz v0, :cond_4

    .line 805
    iget v1, v0, Landroid/graphics/Point;->x:I

    invoke-static {v6, v1}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 806
    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 807
    iget-object v0, p2, Lcom/google/android/exoplayer2/j;->f:Ljava/lang/String;

    .line 808
    invoke-static {v0, v6, v5}, Lcom/google/android/exoplayer2/c/e;->a(Ljava/lang/String;II)I

    move-result v0

    .line 807
    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 809
    const-string v0, "MediaCodecVideoRenderer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Codec max resolution adjusted to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 812
    :cond_4
    new-instance v0, Lcom/google/android/exoplayer2/c/e$a;

    invoke-direct {v0, v6, v5, v4}, Lcom/google/android/exoplayer2/c/e$a;-><init>(III)V

    goto/16 :goto_0

    :cond_5
    move v0, v3

    move v3, v4

    move v4, v5

    move v5, v6

    goto :goto_3
.end method

.method public a(ILjava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    .line 325
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 326
    check-cast p2, Landroid/view/Surface;

    invoke-direct {p0, p2}, Lcom/google/android/exoplayer2/c/e;->a(Landroid/view/Surface;)V

    .line 336
    :cond_0
    :goto_0
    return-void

    .line 327
    :cond_1
    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    .line 328
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/exoplayer2/c/e;->o:I

    .line 329
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/c/e;->A()Landroid/media/MediaCodec;

    move-result-object v0

    .line 330
    if-eqz v0, :cond_0

    .line 331
    iget v1, p0, Lcom/google/android/exoplayer2/c/e;->o:I

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/c/e;->a(Landroid/media/MediaCodec;I)V

    goto :goto_0

    .line 334
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;->a(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method protected a(JZ)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 254
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;->a(JZ)V

    .line 255
    invoke-direct {p0}, Lcom/google/android/exoplayer2/c/e;->x()V

    .line 256
    iput v2, p0, Lcom/google/android/exoplayer2/c/e;->t:I

    .line 257
    iget v0, p0, Lcom/google/android/exoplayer2/c/e;->H:I

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/e;->j:[J

    iget v1, p0, Lcom/google/android/exoplayer2/c/e;->H:I

    add-int/lit8 v1, v1, -0x1

    aget-wide v0, v0, v1

    iput-wide v0, p0, Lcom/google/android/exoplayer2/c/e;->G:J

    .line 259
    iput v2, p0, Lcom/google/android/exoplayer2/c/e;->H:I

    .line 261
    :cond_0
    if-eqz p3, :cond_1

    .line 262
    invoke-direct {p0}, Lcom/google/android/exoplayer2/c/e;->w()V

    .line 266
    :goto_0
    return-void

    .line 264
    :cond_1
    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v0, p0, Lcom/google/android/exoplayer2/c/e;->q:J

    goto :goto_0
.end method

.method protected a(Landroid/media/MediaCodec;IJ)V
    .locals 2

    .prologue
    .line 591
    const-string v0, "skipVideoBuffer"

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/q;->a(Ljava/lang/String;)V

    .line 592
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 593
    invoke-static {}, Lcom/google/android/exoplayer2/util/q;->a()V

    .line 594
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/e;->a:Lcom/google/android/exoplayer2/a/d;

    iget v1, v0, Lcom/google/android/exoplayer2/a/d;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/exoplayer2/a/d;->e:I

    .line 595
    return-void
.end method

.method protected a(Landroid/media/MediaCodec;IJJ)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 648
    invoke-direct {p0}, Lcom/google/android/exoplayer2/c/e;->H()V

    .line 649
    const-string v0, "releaseOutputBuffer"

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/q;->a(Ljava/lang/String;)V

    .line 650
    invoke-virtual {p1, p2, p5, p6}, Landroid/media/MediaCodec;->releaseOutputBuffer(IJ)V

    .line 651
    invoke-static {}, Lcom/google/android/exoplayer2/util/q;->a()V

    .line 652
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/e;->a:Lcom/google/android/exoplayer2/a/d;

    iget v1, v0, Lcom/google/android/exoplayer2/a/d;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/exoplayer2/a/d;->d:I

    .line 653
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer2/c/e;->t:I

    .line 654
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/c/e;->v()V

    .line 655
    return-void
.end method

.method protected a(Landroid/media/MediaCodec;Landroid/media/MediaFormat;)V
    .locals 3

    .prologue
    .line 447
    const-string v0, "crop-right"

    invoke-virtual {p2, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "crop-left"

    .line 448
    invoke-virtual {p2, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "crop-bottom"

    invoke-virtual {p2, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "crop-top"

    .line 449
    invoke-virtual {p2, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    move v1, v0

    .line 450
    :goto_0
    if-eqz v1, :cond_3

    const-string v0, "crop-right"

    .line 451
    invoke-virtual {p2, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    const-string v2, "crop-left"

    invoke-virtual {p2, v2}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v2

    sub-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x1

    .line 452
    :goto_1
    iput v0, p0, Lcom/google/android/exoplayer2/c/e;->w:I

    .line 453
    if-eqz v1, :cond_4

    const-string v0, "crop-bottom"

    .line 454
    invoke-virtual {p2, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    const-string v1, "crop-top"

    invoke-virtual {p2, v1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    .line 455
    :goto_2
    iput v0, p0, Lcom/google/android/exoplayer2/c/e;->x:I

    .line 456
    iget v0, p0, Lcom/google/android/exoplayer2/c/e;->v:F

    iput v0, p0, Lcom/google/android/exoplayer2/c/e;->z:F

    .line 457
    sget v0, Lcom/google/android/exoplayer2/util/s;->a:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_5

    .line 461
    iget v0, p0, Lcom/google/android/exoplayer2/c/e;->u:I

    const/16 v1, 0x5a

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer2/c/e;->u:I

    const/16 v1, 0x10e

    if-ne v0, v1, :cond_1

    .line 462
    :cond_0
    iget v0, p0, Lcom/google/android/exoplayer2/c/e;->w:I

    .line 463
    iget v1, p0, Lcom/google/android/exoplayer2/c/e;->x:I

    iput v1, p0, Lcom/google/android/exoplayer2/c/e;->w:I

    .line 464
    iput v0, p0, Lcom/google/android/exoplayer2/c/e;->x:I

    .line 465
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Lcom/google/android/exoplayer2/c/e;->z:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/exoplayer2/c/e;->z:F

    .line 472
    :cond_1
    :goto_3
    iget v0, p0, Lcom/google/android/exoplayer2/c/e;->o:I

    invoke-static {p1, v0}, Lcom/google/android/exoplayer2/c/e;->a(Landroid/media/MediaCodec;I)V

    .line 473
    return-void

    .line 449
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 451
    :cond_3
    const-string v0, "width"

    .line 452
    invoke-virtual {p2, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 454
    :cond_4
    const-string v0, "height"

    .line 455
    invoke-virtual {p2, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    goto :goto_2

    .line 469
    :cond_5
    iget v0, p0, Lcom/google/android/exoplayer2/c/e;->u:I

    iput v0, p0, Lcom/google/android/exoplayer2/c/e;->y:I

    goto :goto_3
.end method

.method protected a(Lcom/google/android/exoplayer2/a/e;)V
    .locals 2

    .prologue
    .line 440
    sget v0, Lcom/google/android/exoplayer2/util/s;->a:I

    const/16 v1, 0x17

    if-ge v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/c/e;->E:Z

    if-eqz v0, :cond_0

    .line 441
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/c/e;->v()V

    .line 443
    :cond_0
    return-void
.end method

.method protected a(Lcom/google/android/exoplayer2/mediacodec/a;Landroid/media/MediaCodec;Lcom/google/android/exoplayer2/j;Landroid/media/MediaCrypto;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/mediacodec/MediaCodecUtil$DecoderQueryException;
        }
    .end annotation

    .prologue
    .line 393
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/e;->k:[Lcom/google/android/exoplayer2/j;

    invoke-virtual {p0, p1, p3, v0}, Lcom/google/android/exoplayer2/c/e;->a(Lcom/google/android/exoplayer2/mediacodec/a;Lcom/google/android/exoplayer2/j;[Lcom/google/android/exoplayer2/j;)Lcom/google/android/exoplayer2/c/e$a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/c/e;->l:Lcom/google/android/exoplayer2/c/e$a;

    .line 394
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/e;->l:Lcom/google/android/exoplayer2/c/e$a;

    iget-boolean v1, p0, Lcom/google/android/exoplayer2/c/e;->i:Z

    iget v2, p0, Lcom/google/android/exoplayer2/c/e;->F:I

    invoke-static {p3, v0, v1, v2}, Lcom/google/android/exoplayer2/c/e;->a(Lcom/google/android/exoplayer2/j;Lcom/google/android/exoplayer2/c/e$a;ZI)Landroid/media/MediaFormat;

    move-result-object v0

    .line 396
    iget-object v1, p0, Lcom/google/android/exoplayer2/c/e;->m:Landroid/view/Surface;

    if-nez v1, :cond_1

    .line 397
    iget-boolean v1, p1, Lcom/google/android/exoplayer2/mediacodec/a;->d:Z

    invoke-direct {p0, v1}, Lcom/google/android/exoplayer2/c/e;->b(Z)Z

    move-result v1

    invoke-static {v1}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 398
    iget-object v1, p0, Lcom/google/android/exoplayer2/c/e;->n:Landroid/view/Surface;

    if-nez v1, :cond_0

    .line 399
    iget-object v1, p0, Lcom/google/android/exoplayer2/c/e;->d:Landroid/content/Context;

    iget-boolean v2, p1, Lcom/google/android/exoplayer2/mediacodec/a;->d:Z

    invoke-static {v1, v2}, Lcom/google/android/exoplayer2/c/c;->a(Landroid/content/Context;Z)Lcom/google/android/exoplayer2/c/c;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/exoplayer2/c/e;->n:Landroid/view/Surface;

    .line 401
    :cond_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/c/e;->n:Landroid/view/Surface;

    iput-object v1, p0, Lcom/google/android/exoplayer2/c/e;->m:Landroid/view/Surface;

    .line 403
    :cond_1
    iget-object v1, p0, Lcom/google/android/exoplayer2/c/e;->m:Landroid/view/Surface;

    const/4 v2, 0x0

    invoke-virtual {p2, v0, v1, p4, v2}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 404
    sget v0, Lcom/google/android/exoplayer2/util/s;->a:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/c/e;->E:Z

    if-eqz v0, :cond_2

    .line 405
    new-instance v0, Lcom/google/android/exoplayer2/c/e$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p2, v1}, Lcom/google/android/exoplayer2/c/e$b;-><init>(Lcom/google/android/exoplayer2/c/e;Landroid/media/MediaCodec;Lcom/google/android/exoplayer2/c/e$1;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/c/e;->b:Lcom/google/android/exoplayer2/c/e$b;

    .line 407
    :cond_2
    return-void
.end method

.method protected a(Ljava/lang/String;JJ)V
    .locals 6

    .prologue
    .line 427
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/e;->f:Lcom/google/android/exoplayer2/c/g$a;

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/exoplayer2/c/g$a;->a(Ljava/lang/String;JJ)V

    .line 428
    return-void
.end method

.method protected a(Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    .line 228
    invoke-super {p0, p1}, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;->a(Z)V

    .line 229
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/c/e;->q()Lcom/google/android/exoplayer2/r;

    move-result-object v0

    iget v0, v0, Lcom/google/android/exoplayer2/r;->b:I

    iput v0, p0, Lcom/google/android/exoplayer2/c/e;->F:I

    .line 230
    iget v0, p0, Lcom/google/android/exoplayer2/c/e;->F:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/exoplayer2/c/e;->E:Z

    .line 231
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/e;->f:Lcom/google/android/exoplayer2/c/g$a;

    iget-object v1, p0, Lcom/google/android/exoplayer2/c/e;->a:Lcom/google/android/exoplayer2/a/d;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/c/g$a;->a(Lcom/google/android/exoplayer2/a/d;)V

    .line 232
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/e;->e:Lcom/google/android/exoplayer2/c/f;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/c/f;->a()V

    .line 233
    return-void

    .line 230
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a([Lcom/google/android/exoplayer2/j;J)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    .line 237
    iput-object p1, p0, Lcom/google/android/exoplayer2/c/e;->k:[Lcom/google/android/exoplayer2/j;

    .line 238
    iget-wide v0, p0, Lcom/google/android/exoplayer2/c/e;->G:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 239
    iput-wide p2, p0, Lcom/google/android/exoplayer2/c/e;->G:J

    .line 249
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;->a([Lcom/google/android/exoplayer2/j;J)V

    .line 250
    return-void

    .line 241
    :cond_0
    iget v0, p0, Lcom/google/android/exoplayer2/c/e;->H:I

    iget-object v1, p0, Lcom/google/android/exoplayer2/c/e;->j:[J

    array-length v1, v1

    if-ne v0, v1, :cond_1

    .line 242
    const-string v0, "MediaCodecVideoRenderer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Too many stream changes, so dropping offset: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/exoplayer2/c/e;->j:[J

    iget v3, p0, Lcom/google/android/exoplayer2/c/e;->H:I

    add-int/lit8 v3, v3, -0x1

    aget-wide v2, v2, v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    :goto_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/e;->j:[J

    iget v1, p0, Lcom/google/android/exoplayer2/c/e;->H:I

    add-int/lit8 v1, v1, -0x1

    aput-wide p2, v0, v1

    goto :goto_0

    .line 245
    :cond_1
    iget v0, p0, Lcom/google/android/exoplayer2/c/e;->H:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/exoplayer2/c/e;->H:I

    goto :goto_1
.end method

.method protected a(JJLandroid/media/MediaCodec;Ljava/nio/ByteBuffer;IIJZ)Z
    .locals 11

    .prologue
    .line 487
    :goto_0
    iget v2, p0, Lcom/google/android/exoplayer2/c/e;->H:I

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/exoplayer2/c/e;->j:[J

    const/4 v3, 0x0

    aget-wide v2, v2, v3

    cmp-long v2, p9, v2

    if-ltz v2, :cond_0

    .line 489
    iget-object v2, p0, Lcom/google/android/exoplayer2/c/e;->j:[J

    const/4 v3, 0x0

    aget-wide v2, v2, v3

    iput-wide v2, p0, Lcom/google/android/exoplayer2/c/e;->G:J

    .line 490
    iget v2, p0, Lcom/google/android/exoplayer2/c/e;->H:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/google/android/exoplayer2/c/e;->H:I

    .line 491
    iget-object v2, p0, Lcom/google/android/exoplayer2/c/e;->j:[J

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/exoplayer2/c/e;->j:[J

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/exoplayer2/c/e;->H:I

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 494
    :cond_0
    iget-wide v2, p0, Lcom/google/android/exoplayer2/c/e;->G:J

    sub-long v6, p9, v2

    .line 496
    if-eqz p11, :cond_1

    .line 497
    move-object/from16 v0, p5

    move/from16 v1, p7

    invoke-virtual {p0, v0, v1, v6, v7}, Lcom/google/android/exoplayer2/c/e;->a(Landroid/media/MediaCodec;IJ)V

    .line 498
    const/4 v2, 0x1

    .line 568
    :goto_1
    return v2

    .line 501
    :cond_1
    sub-long v2, p9, p1

    .line 502
    iget-object v4, p0, Lcom/google/android/exoplayer2/c/e;->m:Landroid/view/Surface;

    iget-object v5, p0, Lcom/google/android/exoplayer2/c/e;->n:Landroid/view/Surface;

    if-ne v4, v5, :cond_3

    .line 504
    invoke-static {v2, v3}, Lcom/google/android/exoplayer2/c/e;->d(J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 505
    move-object/from16 v0, p5

    move/from16 v1, p7

    invoke-virtual {p0, v0, v1, v6, v7}, Lcom/google/android/exoplayer2/c/e;->a(Landroid/media/MediaCodec;IJ)V

    .line 506
    const/4 v2, 0x1

    goto :goto_1

    .line 508
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 511
    :cond_3
    iget-boolean v4, p0, Lcom/google/android/exoplayer2/c/e;->p:Z

    if-nez v4, :cond_5

    .line 512
    sget v2, Lcom/google/android/exoplayer2/util/s;->a:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_4

    .line 513
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    move-object v3, p0

    move-object/from16 v4, p5

    move/from16 v5, p7

    invoke-virtual/range {v3 .. v9}, Lcom/google/android/exoplayer2/c/e;->a(Landroid/media/MediaCodec;IJJ)V

    .line 517
    :goto_2
    const/4 v2, 0x1

    goto :goto_1

    .line 515
    :cond_4
    move-object/from16 v0, p5

    move/from16 v1, p7

    invoke-virtual {p0, v0, v1, v6, v7}, Lcom/google/android/exoplayer2/c/e;->c(Landroid/media/MediaCodec;IJ)V

    goto :goto_2

    .line 520
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/c/e;->d()I

    move-result v4

    const/4 v5, 0x2

    if-eq v4, v5, :cond_6

    .line 521
    const/4 v2, 0x0

    goto :goto_1

    .line 526
    :cond_6
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    const-wide/16 v8, 0x3e8

    mul-long/2addr v4, v8

    sub-long/2addr v4, p3

    .line 527
    sub-long/2addr v2, v4

    .line 530
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    .line 531
    const-wide/16 v8, 0x3e8

    mul-long/2addr v2, v8

    add-long/2addr v2, v4

    .line 534
    iget-object v8, p0, Lcom/google/android/exoplayer2/c/e;->e:Lcom/google/android/exoplayer2/c/f;

    move-wide/from16 v0, p9

    invoke-virtual {v8, v0, v1, v2, v3}, Lcom/google/android/exoplayer2/c/f;->a(JJ)J

    move-result-wide v8

    .line 536
    sub-long v2, v8, v4

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 538
    invoke-virtual {p0, v2, v3, p3, p4}, Lcom/google/android/exoplayer2/c/e;->b(JJ)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 539
    move-object/from16 v0, p5

    move/from16 v1, p7

    invoke-virtual {p0, v0, v1, v6, v7}, Lcom/google/android/exoplayer2/c/e;->b(Landroid/media/MediaCodec;IJ)V

    .line 540
    const/4 v2, 0x1

    goto :goto_1

    .line 543
    :cond_7
    sget v4, Lcom/google/android/exoplayer2/util/s;->a:I

    const/16 v5, 0x15

    if-lt v4, v5, :cond_8

    .line 545
    const-wide/32 v4, 0xc350

    cmp-long v2, v2, v4

    if-gez v2, :cond_a

    move-object v3, p0

    move-object/from16 v4, p5

    move/from16 v5, p7

    .line 546
    invoke-virtual/range {v3 .. v9}, Lcom/google/android/exoplayer2/c/e;->a(Landroid/media/MediaCodec;IJJ)V

    .line 547
    const/4 v2, 0x1

    goto/16 :goto_1

    .line 551
    :cond_8
    const-wide/16 v4, 0x7530

    cmp-long v4, v2, v4

    if-gez v4, :cond_a

    .line 552
    const-wide/16 v4, 0x2af8

    cmp-long v4, v2, v4

    if-lez v4, :cond_9

    .line 557
    const-wide/16 v4, 0x2710

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    :try_start_0
    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 562
    :cond_9
    :goto_3
    move-object/from16 v0, p5

    move/from16 v1, p7

    invoke-virtual {p0, v0, v1, v6, v7}, Lcom/google/android/exoplayer2/c/e;->c(Landroid/media/MediaCodec;IJ)V

    .line 563
    const/4 v2, 0x1

    goto/16 :goto_1

    .line 558
    :catch_0
    move-exception v2

    .line 559
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_3

    .line 568
    :cond_a
    const/4 v2, 0x0

    goto/16 :goto_1
.end method

.method protected a(Landroid/media/MediaCodec;ZLcom/google/android/exoplayer2/j;Lcom/google/android/exoplayer2/j;)Z
    .locals 2

    .prologue
    .line 478
    invoke-static {p2, p3, p4}, Lcom/google/android/exoplayer2/c/e;->a(ZLcom/google/android/exoplayer2/j;Lcom/google/android/exoplayer2/j;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p4, Lcom/google/android/exoplayer2/j;->j:I

    iget-object v1, p0, Lcom/google/android/exoplayer2/c/e;->l:Lcom/google/android/exoplayer2/c/e$a;

    iget v1, v1, Lcom/google/android/exoplayer2/c/e$a;->a:I

    if-gt v0, v1, :cond_0

    iget v0, p4, Lcom/google/android/exoplayer2/j;->k:I

    iget-object v1, p0, Lcom/google/android/exoplayer2/c/e;->l:Lcom/google/android/exoplayer2/c/e$a;

    iget v1, v1, Lcom/google/android/exoplayer2/c/e$a;->b:I

    if-gt v0, v1, :cond_0

    .line 480
    invoke-static {p4}, Lcom/google/android/exoplayer2/c/e;->c(Lcom/google/android/exoplayer2/j;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/c/e;->l:Lcom/google/android/exoplayer2/c/e$a;

    iget v1, v1, Lcom/google/android/exoplayer2/c/e$a;->c:I

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lcom/google/android/exoplayer2/mediacodec/a;)Z
    .locals 1

    .prologue
    .line 387
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/e;->m:Landroid/view/Surface;

    if-nez v0, :cond_0

    iget-boolean v0, p1, Lcom/google/android/exoplayer2/mediacodec/a;->d:Z

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/c/e;->b(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(Landroid/media/MediaCodec;IJ)V
    .locals 3

    .prologue
    .line 605
    const-string v0, "dropVideoBuffer"

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/q;->a(Ljava/lang/String;)V

    .line 606
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 607
    invoke-static {}, Lcom/google/android/exoplayer2/util/q;->a()V

    .line 608
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/e;->a:Lcom/google/android/exoplayer2/a/d;

    iget v1, v0, Lcom/google/android/exoplayer2/a/d;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/exoplayer2/a/d;->f:I

    .line 609
    iget v0, p0, Lcom/google/android/exoplayer2/c/e;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/exoplayer2/c/e;->s:I

    .line 610
    iget v0, p0, Lcom/google/android/exoplayer2/c/e;->t:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/exoplayer2/c/e;->t:I

    .line 611
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/e;->a:Lcom/google/android/exoplayer2/a/d;

    iget v1, p0, Lcom/google/android/exoplayer2/c/e;->t:I

    iget-object v2, p0, Lcom/google/android/exoplayer2/c/e;->a:Lcom/google/android/exoplayer2/a/d;

    iget v2, v2, Lcom/google/android/exoplayer2/a/d;->g:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v0, Lcom/google/android/exoplayer2/a/d;->g:I

    .line 613
    iget v0, p0, Lcom/google/android/exoplayer2/c/e;->s:I

    iget v1, p0, Lcom/google/android/exoplayer2/c/e;->h:I

    if-ne v0, v1, :cond_0

    .line 614
    invoke-direct {p0}, Lcom/google/android/exoplayer2/c/e;->J()V

    .line 616
    :cond_0
    return-void
.end method

.method protected b(Lcom/google/android/exoplayer2/j;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    .line 432
    invoke-super {p0, p1}, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;->b(Lcom/google/android/exoplayer2/j;)V

    .line 433
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/e;->f:Lcom/google/android/exoplayer2/c/g$a;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/c/g$a;->a(Lcom/google/android/exoplayer2/j;)V

    .line 434
    invoke-static {p1}, Lcom/google/android/exoplayer2/c/e;->d(Lcom/google/android/exoplayer2/j;)F

    move-result v0

    iput v0, p0, Lcom/google/android/exoplayer2/c/e;->v:F

    .line 435
    invoke-static {p1}, Lcom/google/android/exoplayer2/c/e;->e(Lcom/google/android/exoplayer2/j;)I

    move-result v0

    iput v0, p0, Lcom/google/android/exoplayer2/c/e;->u:I

    .line 436
    return-void
.end method

.method protected b(JJ)Z
    .locals 1

    .prologue
    .line 580
    invoke-static {p1, p2}, Lcom/google/android/exoplayer2/c/e;->d(J)Z

    move-result v0

    return v0
.end method

.method protected c(Landroid/media/MediaCodec;IJ)V
    .locals 2

    .prologue
    .line 627
    invoke-direct {p0}, Lcom/google/android/exoplayer2/c/e;->H()V

    .line 628
    const-string v0, "releaseOutputBuffer"

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/q;->a(Ljava/lang/String;)V

    .line 629
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 630
    invoke-static {}, Lcom/google/android/exoplayer2/util/q;->a()V

    .line 631
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/e;->a:Lcom/google/android/exoplayer2/a/d;

    iget v1, v0, Lcom/google/android/exoplayer2/a/d;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/exoplayer2/a/d;->d:I

    .line 632
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer2/c/e;->t:I

    .line 633
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/c/e;->v()V

    .line 634
    return-void
.end method

.method protected n()V
    .locals 2

    .prologue
    .line 290
    invoke-super {p0}, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;->n()V

    .line 291
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer2/c/e;->s:I

    .line 292
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/c/e;->r:J

    .line 293
    return-void
.end method

.method protected o()V
    .locals 2

    .prologue
    .line 297
    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v0, p0, Lcom/google/android/exoplayer2/c/e;->q:J

    .line 298
    invoke-direct {p0}, Lcom/google/android/exoplayer2/c/e;->J()V

    .line 299
    invoke-super {p0}, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;->o()V

    .line 300
    return-void
.end method

.method protected p()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    const/high16 v0, -0x40800000    # -1.0f

    .line 304
    iput v1, p0, Lcom/google/android/exoplayer2/c/e;->w:I

    .line 305
    iput v1, p0, Lcom/google/android/exoplayer2/c/e;->x:I

    .line 306
    iput v0, p0, Lcom/google/android/exoplayer2/c/e;->z:F

    .line 307
    iput v0, p0, Lcom/google/android/exoplayer2/c/e;->v:F

    .line 308
    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v0, p0, Lcom/google/android/exoplayer2/c/e;->G:J

    .line 309
    iput v2, p0, Lcom/google/android/exoplayer2/c/e;->H:I

    .line 310
    invoke-direct {p0}, Lcom/google/android/exoplayer2/c/e;->G()V

    .line 311
    invoke-direct {p0}, Lcom/google/android/exoplayer2/c/e;->x()V

    .line 312
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/e;->e:Lcom/google/android/exoplayer2/c/f;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/c/f;->b()V

    .line 313
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer2/c/e;->b:Lcom/google/android/exoplayer2/c/e$b;

    .line 314
    iput-boolean v2, p0, Lcom/google/android/exoplayer2/c/e;->E:Z

    .line 316
    :try_start_0
    invoke-super {p0}, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;->p()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 318
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/e;->a:Lcom/google/android/exoplayer2/a/d;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/a/d;->a()V

    .line 319
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/e;->f:Lcom/google/android/exoplayer2/c/g$a;

    iget-object v1, p0, Lcom/google/android/exoplayer2/c/e;->a:Lcom/google/android/exoplayer2/a/d;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/c/g$a;->b(Lcom/google/android/exoplayer2/a/d;)V

    .line 321
    return-void

    .line 318
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/c/e;->a:Lcom/google/android/exoplayer2/a/d;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/a/d;->a()V

    .line 319
    iget-object v1, p0, Lcom/google/android/exoplayer2/c/e;->f:Lcom/google/android/exoplayer2/c/g$a;

    iget-object v2, p0, Lcom/google/android/exoplayer2/c/e;->a:Lcom/google/android/exoplayer2/a/d;

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/c/g$a;->b(Lcom/google/android/exoplayer2/a/d;)V

    throw v0
.end method

.method public t()Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const-wide v6, -0x7fffffffffffffffL    # -4.9E-324

    .line 270
    invoke-super {p0}, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer;->t()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/google/android/exoplayer2/c/e;->p:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/exoplayer2/c/e;->n:Landroid/view/Surface;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/exoplayer2/c/e;->m:Landroid/view/Surface;

    iget-object v3, p0, Lcom/google/android/exoplayer2/c/e;->n:Landroid/view/Surface;

    if-eq v2, v3, :cond_1

    .line 271
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/c/e;->A()Landroid/media/MediaCodec;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/exoplayer2/c/e;->E:Z

    if-eqz v2, :cond_3

    .line 273
    :cond_1
    iput-wide v6, p0, Lcom/google/android/exoplayer2/c/e;->q:J

    .line 284
    :cond_2
    :goto_0
    return v0

    .line 275
    :cond_3
    iget-wide v2, p0, Lcom/google/android/exoplayer2/c/e;->q:J

    cmp-long v2, v2, v6

    if-nez v2, :cond_4

    move v0, v1

    .line 277
    goto :goto_0

    .line 278
    :cond_4
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/exoplayer2/c/e;->q:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_2

    .line 283
    iput-wide v6, p0, Lcom/google/android/exoplayer2/c/e;->q:J

    move v0, v1

    .line 284
    goto :goto_0
.end method

.method v()V
    .locals 2

    .prologue
    .line 683
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/c/e;->p:Z

    if-nez v0, :cond_0

    .line 684
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/c/e;->p:Z

    .line 685
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/e;->f:Lcom/google/android/exoplayer2/c/g$a;

    iget-object v1, p0, Lcom/google/android/exoplayer2/c/e;->m:Landroid/view/Surface;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/c/g$a;->a(Landroid/view/Surface;)V

    .line 687
    :cond_0
    return-void
.end method
