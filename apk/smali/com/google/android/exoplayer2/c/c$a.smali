.class Lcom/google/android/exoplayer2/c/c$a;
.super Landroid/os/HandlerThread;
.source "DummySurface.java"

# interfaces
.implements Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/c/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:[I

.field private b:Landroid/os/Handler;

.field private c:Landroid/graphics/SurfaceTexture;

.field private d:Ljava/lang/Error;

.field private e:Ljava/lang/RuntimeException;

.field private f:Lcom/google/android/exoplayer2/c/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 182
    const-string v0, "dummySurface"

    invoke-direct {p0, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 183
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/exoplayer2/c/c$a;->a:[I

    .line 184
    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 326
    :try_start_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/c$a;->c:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 328
    iput-object v1, p0, Lcom/google/android/exoplayer2/c/c$a;->f:Lcom/google/android/exoplayer2/c/c;

    .line 329
    iput-object v1, p0, Lcom/google/android/exoplayer2/c/c$a;->c:Landroid/graphics/SurfaceTexture;

    .line 330
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/c$a;->a:[I

    invoke-static {v3, v0, v2}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 332
    return-void

    .line 328
    :catchall_0
    move-exception v0

    iput-object v1, p0, Lcom/google/android/exoplayer2/c/c$a;->f:Lcom/google/android/exoplayer2/c/c;

    .line 329
    iput-object v1, p0, Lcom/google/android/exoplayer2/c/c$a;->c:Landroid/graphics/SurfaceTexture;

    .line 330
    iget-object v1, p0, Lcom/google/android/exoplayer2/c/c$a;->a:[I

    invoke-static {v3, v1, v2}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    throw v0
.end method

.method private b(Z)V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 258
    invoke-static {v2}, Landroid/opengl/EGL14;->eglGetDisplay(I)Landroid/opengl/EGLDisplay;

    move-result-object v0

    .line 259
    if-eqz v0, :cond_0

    move v1, v5

    :goto_0
    const-string v3, "eglGetDisplay failed"

    invoke-static {v1, v3}, Lcom/google/android/exoplayer2/util/a;->b(ZLjava/lang/Object;)V

    .line 261
    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 262
    invoke-static {v0, v1, v2, v1, v5}, Landroid/opengl/EGL14;->eglInitialize(Landroid/opengl/EGLDisplay;[II[II)Z

    move-result v1

    .line 263
    const-string v3, "eglInitialize failed"

    invoke-static {v1, v3}, Lcom/google/android/exoplayer2/util/a;->b(ZLjava/lang/Object;)V

    .line 265
    const/16 v1, 0x11

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    .line 276
    new-array v3, v5, [Landroid/opengl/EGLConfig;

    .line 277
    new-array v6, v5, [I

    move v4, v2

    move v7, v2

    .line 278
    invoke-static/range {v0 .. v7}, Landroid/opengl/EGL14;->eglChooseConfig(Landroid/opengl/EGLDisplay;[II[Landroid/opengl/EGLConfig;II[II)Z

    move-result v1

    .line 280
    if-eqz v1, :cond_1

    aget v1, v6, v2

    if-lez v1, :cond_1

    aget-object v1, v3, v2

    if-eqz v1, :cond_1

    move v1, v5

    :goto_1
    const-string v4, "eglChooseConfig failed"

    invoke-static {v1, v4}, Lcom/google/android/exoplayer2/util/a;->b(ZLjava/lang/Object;)V

    .line 283
    aget-object v3, v3, v2

    .line 285
    if-eqz p1, :cond_2

    .line 286
    new-array v1, v8, [I

    fill-array-data v1, :array_1

    .line 295
    :goto_2
    sget-object v4, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    invoke-static {v0, v3, v4, v1, v2}, Landroid/opengl/EGL14;->eglCreateContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Landroid/opengl/EGLContext;[II)Landroid/opengl/EGLContext;

    move-result-object v4

    .line 297
    if-eqz v4, :cond_3

    move v1, v5

    :goto_3
    const-string v6, "eglCreateContext failed"

    invoke-static {v1, v6}, Lcom/google/android/exoplayer2/util/a;->b(ZLjava/lang/Object;)V

    .line 300
    if-eqz p1, :cond_4

    .line 301
    const/4 v1, 0x7

    new-array v1, v1, [I

    fill-array-data v1, :array_2

    .line 312
    :goto_4
    invoke-static {v0, v3, v1, v2}, Landroid/opengl/EGL14;->eglCreatePbufferSurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;[II)Landroid/opengl/EGLSurface;

    move-result-object v3

    .line 313
    if-eqz v3, :cond_5

    move v1, v5

    :goto_5
    const-string v6, "eglCreatePbufferSurface failed"

    invoke-static {v1, v6}, Lcom/google/android/exoplayer2/util/a;->b(ZLjava/lang/Object;)V

    .line 315
    invoke-static {v0, v3, v3, v4}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    move-result v0

    .line 316
    const-string v1, "eglMakeCurrent failed"

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/util/a;->b(ZLjava/lang/Object;)V

    .line 318
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/c$a;->a:[I

    invoke-static {v5, v0, v2}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 319
    new-instance v0, Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, Lcom/google/android/exoplayer2/c/c$a;->a:[I

    aget v1, v1, v2

    invoke-direct {v0, v1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/c/c$a;->c:Landroid/graphics/SurfaceTexture;

    .line 320
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/c$a;->c:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0, p0}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    .line 321
    new-instance v0, Lcom/google/android/exoplayer2/c/c;

    iget-object v1, p0, Lcom/google/android/exoplayer2/c/c$a;->c:Landroid/graphics/SurfaceTexture;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, p1, v2}, Lcom/google/android/exoplayer2/c/c;-><init>(Lcom/google/android/exoplayer2/c/c$a;Landroid/graphics/SurfaceTexture;ZLcom/google/android/exoplayer2/c/c$1;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/c/c$a;->f:Lcom/google/android/exoplayer2/c/c;

    .line 322
    return-void

    :cond_0
    move v1, v2

    .line 259
    goto/16 :goto_0

    :cond_1
    move v1, v2

    .line 280
    goto :goto_1

    .line 291
    :cond_2
    const/4 v1, 0x3

    new-array v1, v1, [I

    fill-array-data v1, :array_3

    goto :goto_2

    :cond_3
    move v1, v2

    .line 297
    goto :goto_3

    .line 307
    :cond_4
    new-array v1, v8, [I

    fill-array-data v1, :array_4

    goto :goto_4

    :cond_5
    move v1, v2

    .line 313
    goto :goto_5

    .line 265
    :array_0
    .array-data 4
        0x3040
        0x4
        0x3024
        0x8
        0x3023
        0x8
        0x3022
        0x8
        0x3021
        0x8
        0x3025
        0x0
        0x3027
        0x3038
        0x3033
        0x4
        0x3038
    .end array-data

    .line 286
    :array_1
    .array-data 4
        0x3098
        0x2
        0x32c0
        0x1
        0x3038
    .end array-data

    .line 301
    :array_2
    .array-data 4
        0x3057
        0x1
        0x3056
        0x1
        0x32c0
        0x1
        0x3038
    .end array-data

    .line 291
    :array_3
    .array-data 4
        0x3098
        0x2
        0x3038
    .end array-data

    .line 307
    :array_4
    .array-data 4
        0x3057
        0x1
        0x3056
        0x1
        0x3038
    .end array-data
.end method


# virtual methods
.method public a(Z)Lcom/google/android/exoplayer2/c/c;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 187
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/c/c$a;->start()V

    .line 188
    new-instance v2, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/c/c$a;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v2, p0, Lcom/google/android/exoplayer2/c/c$a;->b:Landroid/os/Handler;

    .line 190
    monitor-enter p0

    .line 191
    :try_start_0
    iget-object v3, p0, Lcom/google/android/exoplayer2/c/c$a;->b:Landroid/os/Handler;

    const/4 v4, 0x1

    if-eqz p1, :cond_0

    move v2, v1

    :goto_0
    const/4 v5, 0x0

    invoke-virtual {v3, v4, v2, v5}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    .line 192
    :goto_1
    iget-object v2, p0, Lcom/google/android/exoplayer2/c/c$a;->f:Lcom/google/android/exoplayer2/c/c;

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/exoplayer2/c/c$a;->e:Ljava/lang/RuntimeException;

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/exoplayer2/c/c$a;->d:Ljava/lang/Error;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    .line 194
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 195
    :catch_0
    move-exception v0

    move v0, v1

    .line 197
    goto :goto_1

    :cond_0
    move v2, v0

    .line 191
    goto :goto_0

    .line 199
    :cond_1
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 200
    if-eqz v0, :cond_2

    .line 202
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 204
    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/c$a;->e:Ljava/lang/RuntimeException;

    if-eqz v0, :cond_3

    .line 205
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/c$a;->e:Ljava/lang/RuntimeException;

    throw v0

    .line 199
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 206
    :cond_3
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/c$a;->d:Ljava/lang/Error;

    if-eqz v0, :cond_4

    .line 207
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/c$a;->d:Ljava/lang/Error;

    throw v0

    .line 209
    :cond_4
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/c$a;->f:Lcom/google/android/exoplayer2/c/c;

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/c$a;->b:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 215
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 224
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 253
    :goto_0
    return v1

    .line 227
    :pswitch_0
    :try_start_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/c/c$a;->b(Z)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 235
    monitor-enter p0

    .line 236
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 237
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 227
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 228
    :catch_0
    move-exception v0

    .line 229
    :try_start_2
    const-string v2, "DummySurface"

    const-string v3, "Failed to initialize dummy surface"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 230
    iput-object v0, p0, Lcom/google/android/exoplayer2/c/c$a;->e:Ljava/lang/RuntimeException;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 235
    monitor-enter p0

    .line 236
    :try_start_3
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 237
    monitor-exit p0

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 231
    :catch_1
    move-exception v0

    .line 232
    :try_start_4
    const-string v2, "DummySurface"

    const-string v3, "Failed to initialize dummy surface"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 233
    iput-object v0, p0, Lcom/google/android/exoplayer2/c/c$a;->d:Ljava/lang/Error;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 235
    monitor-enter p0

    .line 236
    :try_start_5
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 237
    monitor-exit p0

    goto :goto_0

    :catchall_2
    move-exception v0

    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    .line 235
    :catchall_3
    move-exception v0

    monitor-enter p0

    .line 236
    :try_start_6
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 237
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    throw v0

    :catchall_4
    move-exception v0

    :try_start_7
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    throw v0

    .line 241
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/c$a;->c:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    goto :goto_0

    .line 245
    :pswitch_2
    :try_start_8
    invoke-direct {p0}, Lcom/google/android/exoplayer2/c/c$a;->b()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_5

    .line 249
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/c/c$a;->quit()Z

    goto :goto_0

    .line 246
    :catch_2
    move-exception v0

    .line 247
    :try_start_9
    const-string v2, "DummySurface"

    const-string v3, "Failed to release dummy surface"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    .line 249
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/c/c$a;->quit()Z

    goto :goto_0

    :catchall_5
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/c/c$a;->quit()Z

    throw v0

    .line 224
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onFrameAvailable(Landroid/graphics/SurfaceTexture;)V
    .locals 2

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/c$a;->b:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 220
    return-void
.end method
