.class public final Lcom/google/android/exoplayer2/c/c;
.super Landroid/view/Surface;
.source "DummySurface.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x11
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/c/c$a;
    }
.end annotation


# static fields
.field private static b:Z

.field private static c:Z


# instance fields
.field public final a:Z

.field private final d:Lcom/google/android/exoplayer2/c/c$a;

.field private e:Z


# direct methods
.method private constructor <init>(Lcom/google/android/exoplayer2/c/c$a;Landroid/graphics/SurfaceTexture;Z)V
    .locals 0

    .prologue
    .line 123
    invoke-direct {p0, p2}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    .line 124
    iput-object p1, p0, Lcom/google/android/exoplayer2/c/c;->d:Lcom/google/android/exoplayer2/c/c$a;

    .line 125
    iput-boolean p3, p0, Lcom/google/android/exoplayer2/c/c;->a:Z

    .line 126
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/exoplayer2/c/c$a;Landroid/graphics/SurfaceTexture;ZLcom/google/android/exoplayer2/c/c$1;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/exoplayer2/c/c;-><init>(Lcom/google/android/exoplayer2/c/c$a;Landroid/graphics/SurfaceTexture;Z)V

    return-void
.end method

.method public static a(Landroid/content/Context;Z)Lcom/google/android/exoplayer2/c/c;
    .locals 1

    .prologue
    .line 116
    invoke-static {}, Lcom/google/android/exoplayer2/c/c;->a()V

    .line 117
    if-eqz p1, :cond_0

    invoke-static {p0}, Lcom/google/android/exoplayer2/c/c;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 118
    new-instance v0, Lcom/google/android/exoplayer2/c/c$a;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/c/c$a;-><init>()V

    .line 119
    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/c/c$a;->a(Z)Lcom/google/android/exoplayer2/c/c;

    move-result-object v0

    return-object v0

    .line 117
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a()V
    .locals 2

    .prologue
    .line 144
    sget v0, Lcom/google/android/exoplayer2/util/s;->a:I

    const/16 v1, 0x11

    if-ge v0, v1, :cond_0

    .line 145
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Unsupported prior to API level 17"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 147
    :cond_0
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 91
    const-class v2, Lcom/google/android/exoplayer2/c/c;

    monitor-enter v2

    :try_start_0
    sget-boolean v3, Lcom/google/android/exoplayer2/c/c;->c:Z

    if-nez v3, :cond_1

    .line 92
    sget v3, Lcom/google/android/exoplayer2/util/s;->a:I

    const/16 v4, 0x11

    if-lt v3, v4, :cond_0

    .line 93
    const/4 v3, 0x0

    invoke-static {v3}, Landroid/opengl/EGL14;->eglGetDisplay(I)Landroid/opengl/EGLDisplay;

    move-result-object v3

    .line 94
    const/16 v4, 0x3055

    invoke-static {v3, v4}, Landroid/opengl/EGL14;->eglQueryString(Landroid/opengl/EGLDisplay;I)Ljava/lang/String;

    move-result-object v3

    .line 95
    if-eqz v3, :cond_2

    const-string v4, "EGL_EXT_protected_content"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 96
    invoke-static {p0}, Lcom/google/android/exoplayer2/c/c;->b(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_2

    :goto_0
    sput-boolean v0, Lcom/google/android/exoplayer2/c/c;->b:Z

    .line 98
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/exoplayer2/c/c;->c:Z

    .line 100
    :cond_1
    sget-boolean v0, Lcom/google/android/exoplayer2/c/c;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    return v0

    :cond_2
    move v0, v1

    .line 96
    goto :goto_0

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method private static a(Landroid/content/pm/PackageManager;)Z
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x18
    .end annotation

    .prologue
    .line 163
    const-string v0, "android.hardware.vr.high_performance"

    invoke-virtual {p0, v0}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static b(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 156
    sget v0, Lcom/google/android/exoplayer2/util/s;->a:I

    const/16 v1, 0x18

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/google/android/exoplayer2/util/s;->d:Ljava/lang/String;

    const-string v1, "SM-G950"

    .line 157
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/exoplayer2/util/s;->d:Ljava/lang/String;

    const-string v1, "SM-G955"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 158
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/exoplayer2/c/c;->a(Landroid/content/pm/PackageManager;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public release()V
    .locals 2

    .prologue
    .line 130
    invoke-super {p0}, Landroid/view/Surface;->release()V

    .line 135
    iget-object v1, p0, Lcom/google/android/exoplayer2/c/c;->d:Lcom/google/android/exoplayer2/c/c$a;

    monitor-enter v1

    .line 136
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/c/c;->e:Z

    if-nez v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/google/android/exoplayer2/c/c;->d:Lcom/google/android/exoplayer2/c/c$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/c/c$a;->a()V

    .line 138
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/c/c;->e:Z

    .line 140
    :cond_0
    monitor-exit v1

    .line 141
    return-void

    .line 140
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
