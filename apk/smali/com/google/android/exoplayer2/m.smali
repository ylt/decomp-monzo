.class final Lcom/google/android/exoplayer2/m;
.super Ljava/lang/Object;
.source "MediaPeriodInfoSequence.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/m$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/exoplayer2/u$a;

.field private final b:Lcom/google/android/exoplayer2/u$b;

.field private c:Lcom/google/android/exoplayer2/u;

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    new-instance v0, Lcom/google/android/exoplayer2/u$a;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/u$a;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    .line 113
    new-instance v0, Lcom/google/android/exoplayer2/u$b;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/u$b;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/m;->b:Lcom/google/android/exoplayer2/u$b;

    .line 114
    return-void
.end method

.method private a(IIIJ)Lcom/google/android/exoplayer2/m$a;
    .locals 18

    .prologue
    .line 299
    new-instance v5, Lcom/google/android/exoplayer2/source/i$b;

    move/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-direct {v5, v0, v1, v2}, Lcom/google/android/exoplayer2/source/i$b;-><init>(III)V

    .line 300
    const-wide/high16 v6, -0x8000000000000000L

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6, v7}, Lcom/google/android/exoplayer2/m;->a(Lcom/google/android/exoplayer2/source/i$b;J)Z

    move-result v14

    .line 301
    move-object/from16 v0, p0

    invoke-direct {v0, v5, v14}, Lcom/google/android/exoplayer2/m;->a(Lcom/google/android/exoplayer2/source/i$b;Z)Z

    move-result v15

    .line 302
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/exoplayer2/m;->c:Lcom/google/android/exoplayer2/u;

    iget v6, v5, Lcom/google/android/exoplayer2/source/i$b;->b:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v4, v6, v7}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;)Lcom/google/android/exoplayer2/u$a;

    move-result-object v4

    iget v6, v5, Lcom/google/android/exoplayer2/source/i$b;->c:I

    iget v7, v5, Lcom/google/android/exoplayer2/source/i$b;->d:I

    .line 303
    invoke-virtual {v4, v6, v7}, Lcom/google/android/exoplayer2/u$a;->b(II)J

    move-result-wide v12

    .line 304
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    move/from16 v0, p2

    invoke-virtual {v4, v0}, Lcom/google/android/exoplayer2/u$a;->b(I)I

    move-result v4

    move/from16 v0, p3

    if-ne v0, v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    .line 305
    invoke-virtual {v4}, Lcom/google/android/exoplayer2/u$a;->e()J

    move-result-wide v6

    .line 306
    :goto_0
    new-instance v4, Lcom/google/android/exoplayer2/m$a;

    const-wide/high16 v8, -0x8000000000000000L

    const/16 v16, 0x0

    move-wide/from16 v10, p4

    invoke-direct/range {v4 .. v16}, Lcom/google/android/exoplayer2/m$a;-><init>(Lcom/google/android/exoplayer2/source/i$b;JJJJZZLcom/google/android/exoplayer2/m$1;)V

    return-object v4

    .line 305
    :cond_0
    const-wide/16 v6, 0x0

    goto :goto_0
.end method

.method private a(IJJ)Lcom/google/android/exoplayer2/m$a;
    .locals 18

    .prologue
    .line 312
    new-instance v5, Lcom/google/android/exoplayer2/source/i$b;

    move/from16 v0, p1

    invoke-direct {v5, v0}, Lcom/google/android/exoplayer2/source/i$b;-><init>(I)V

    .line 313
    move-object/from16 v0, p0

    move-wide/from16 v1, p4

    invoke-direct {v0, v5, v1, v2}, Lcom/google/android/exoplayer2/m;->a(Lcom/google/android/exoplayer2/source/i$b;J)Z

    move-result v14

    .line 314
    move-object/from16 v0, p0

    invoke-direct {v0, v5, v14}, Lcom/google/android/exoplayer2/m;->a(Lcom/google/android/exoplayer2/source/i$b;Z)Z

    move-result v15

    .line 315
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/exoplayer2/m;->c:Lcom/google/android/exoplayer2/u;

    iget v6, v5, Lcom/google/android/exoplayer2/source/i$b;->b:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v4, v6, v7}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;)Lcom/google/android/exoplayer2/u$a;

    .line 316
    const-wide/high16 v6, -0x8000000000000000L

    cmp-long v4, p4, v6

    if-nez v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/u$a;->a()J

    move-result-wide v12

    .line 317
    :goto_0
    new-instance v4, Lcom/google/android/exoplayer2/m$a;

    const-wide v10, -0x7fffffffffffffffL    # -4.9E-324

    const/16 v16, 0x0

    move-wide/from16 v6, p2

    move-wide/from16 v8, p4

    invoke-direct/range {v4 .. v16}, Lcom/google/android/exoplayer2/m$a;-><init>(Lcom/google/android/exoplayer2/source/i$b;JJJJZZLcom/google/android/exoplayer2/m$1;)V

    return-object v4

    :cond_0
    move-wide/from16 v12, p4

    .line 316
    goto :goto_0
.end method

.method private a(Lcom/google/android/exoplayer2/m$a;Lcom/google/android/exoplayer2/source/i$b;)Lcom/google/android/exoplayer2/m$a;
    .locals 13

    .prologue
    .line 268
    iget-wide v2, p1, Lcom/google/android/exoplayer2/m$a;->b:J

    .line 269
    iget-wide v4, p1, Lcom/google/android/exoplayer2/m$a;->c:J

    .line 270
    invoke-direct {p0, p2, v4, v5}, Lcom/google/android/exoplayer2/m;->a(Lcom/google/android/exoplayer2/source/i$b;J)Z

    move-result v10

    .line 271
    invoke-direct {p0, p2, v10}, Lcom/google/android/exoplayer2/m;->a(Lcom/google/android/exoplayer2/source/i$b;Z)Z

    move-result v11

    .line 272
    iget-object v0, p0, Lcom/google/android/exoplayer2/m;->c:Lcom/google/android/exoplayer2/u;

    iget v1, p2, Lcom/google/android/exoplayer2/source/i$b;->b:I

    iget-object v6, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v0, v1, v6}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;)Lcom/google/android/exoplayer2/u$a;

    .line 273
    invoke-virtual {p2}, Lcom/google/android/exoplayer2/source/i$b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    iget v1, p2, Lcom/google/android/exoplayer2/source/i$b;->c:I

    iget v6, p2, Lcom/google/android/exoplayer2/source/i$b;->d:I

    .line 274
    invoke-virtual {v0, v1, v6}, Lcom/google/android/exoplayer2/u$a;->b(II)J

    move-result-wide v8

    .line 276
    :goto_0
    new-instance v0, Lcom/google/android/exoplayer2/m$a;

    iget-wide v6, p1, Lcom/google/android/exoplayer2/m$a;->d:J

    const/4 v12, 0x0

    move-object v1, p2

    invoke-direct/range {v0 .. v12}, Lcom/google/android/exoplayer2/m$a;-><init>(Lcom/google/android/exoplayer2/source/i$b;JJJJZZLcom/google/android/exoplayer2/m$1;)V

    return-object v0

    .line 274
    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, v4, v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    .line 275
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/u$a;->a()J

    move-result-wide v8

    goto :goto_0

    :cond_1
    move-wide v8, v4

    goto :goto_0
.end method

.method private a(Lcom/google/android/exoplayer2/source/i$b;JJ)Lcom/google/android/exoplayer2/m$a;
    .locals 6

    .prologue
    .line 282
    iget-object v0, p0, Lcom/google/android/exoplayer2/m;->c:Lcom/google/android/exoplayer2/u;

    iget v1, p1, Lcom/google/android/exoplayer2/source/i$b;->b:I

    iget-object v2, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;)Lcom/google/android/exoplayer2/u$a;

    .line 283
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/source/i$b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 284
    iget-object v0, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    iget v1, p1, Lcom/google/android/exoplayer2/source/i$b;->c:I

    iget v2, p1, Lcom/google/android/exoplayer2/source/i$b;->d:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/u$a;->a(II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 285
    const/4 v0, 0x0

    .line 293
    :goto_0
    return-object v0

    .line 287
    :cond_0
    iget v1, p1, Lcom/google/android/exoplayer2/source/i$b;->b:I

    iget v2, p1, Lcom/google/android/exoplayer2/source/i$b;->c:I

    iget v3, p1, Lcom/google/android/exoplayer2/source/i$b;->d:I

    move-object v0, p0

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/m;->a(IIIJ)Lcom/google/android/exoplayer2/m$a;

    move-result-object v0

    goto :goto_0

    .line 290
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v0, p4, p5}, Lcom/google/android/exoplayer2/u$a;->b(J)I

    move-result v0

    .line 291
    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    const-wide/high16 v4, -0x8000000000000000L

    .line 293
    :goto_1
    iget v1, p1, Lcom/google/android/exoplayer2/source/i$b;->b:I

    move-object v0, p0

    move-wide v2, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/m;->a(IJJ)Lcom/google/android/exoplayer2/m$a;

    move-result-object v0

    goto :goto_0

    .line 291
    :cond_2
    iget-object v1, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    .line 292
    invoke-virtual {v1, v0}, Lcom/google/android/exoplayer2/u$a;->a(I)J

    move-result-wide v4

    goto :goto_1
.end method

.method private a(Lcom/google/android/exoplayer2/source/i$b;J)Z
    .locals 10

    .prologue
    const-wide/high16 v8, -0x8000000000000000L

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 322
    iget-object v2, p0, Lcom/google/android/exoplayer2/m;->c:Lcom/google/android/exoplayer2/u;

    iget v3, p1, Lcom/google/android/exoplayer2/source/i$b;->b:I

    iget-object v4, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;)Lcom/google/android/exoplayer2/u$a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/u$a;->d()I

    move-result v2

    .line 323
    if-nez v2, :cond_1

    .line 342
    :cond_0
    :goto_0
    return v0

    .line 327
    :cond_1
    add-int/lit8 v3, v2, -0x1

    .line 328
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/source/i$b;->a()Z

    move-result v4

    .line 329
    iget-object v2, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer2/u$a;->a(I)J

    move-result-wide v6

    cmp-long v2, v6, v8

    if-eqz v2, :cond_3

    .line 331
    if-nez v4, :cond_2

    cmp-long v2, p2, v8

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 334
    :cond_3
    iget-object v2, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer2/u$a;->d(I)I

    move-result v5

    .line 335
    const/4 v2, -0x1

    if-ne v5, v2, :cond_4

    move v0, v1

    .line 337
    goto :goto_0

    .line 340
    :cond_4
    if-eqz v4, :cond_7

    iget v2, p1, Lcom/google/android/exoplayer2/source/i$b;->c:I

    if-ne v2, v3, :cond_7

    iget v2, p1, Lcom/google/android/exoplayer2/source/i$b;->d:I

    add-int/lit8 v6, v5, -0x1

    if-ne v2, v6, :cond_7

    move v2, v0

    .line 342
    :goto_1
    if-nez v2, :cond_5

    if-nez v4, :cond_6

    iget-object v2, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer2/u$a;->b(I)I

    move-result v2

    if-ne v2, v5, :cond_6

    :cond_5
    move v1, v0

    :cond_6
    move v0, v1

    goto :goto_0

    :cond_7
    move v2, v1

    .line 340
    goto :goto_1
.end method

.method private a(Lcom/google/android/exoplayer2/source/i$b;Z)Z
    .locals 5

    .prologue
    .line 346
    iget-object v0, p0, Lcom/google/android/exoplayer2/m;->c:Lcom/google/android/exoplayer2/u;

    iget v1, p1, Lcom/google/android/exoplayer2/source/i$b;->b:I

    iget-object v2, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;)Lcom/google/android/exoplayer2/u$a;

    move-result-object v0

    iget v0, v0, Lcom/google/android/exoplayer2/u$a;->c:I

    .line 347
    iget-object v1, p0, Lcom/google/android/exoplayer2/m;->c:Lcom/google/android/exoplayer2/u;

    iget-object v2, p0, Lcom/google/android/exoplayer2/m;->b:Lcom/google/android/exoplayer2/u$b;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$b;)Lcom/google/android/exoplayer2/u$b;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/u$b;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/m;->c:Lcom/google/android/exoplayer2/u;

    iget v1, p1, Lcom/google/android/exoplayer2/source/i$b;->b:I

    iget-object v2, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    iget-object v3, p0, Lcom/google/android/exoplayer2/m;->b:Lcom/google/android/exoplayer2/u$b;

    iget v4, p0, Lcom/google/android/exoplayer2/m;->d:I

    .line 348
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/exoplayer2/u;->b(ILcom/google/android/exoplayer2/u$a;Lcom/google/android/exoplayer2/u$b;I)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/android/exoplayer2/h$b;)Lcom/google/android/exoplayer2/m$a;
    .locals 6

    .prologue
    .line 136
    iget-object v1, p1, Lcom/google/android/exoplayer2/h$b;->a:Lcom/google/android/exoplayer2/source/i$b;

    iget-wide v2, p1, Lcom/google/android/exoplayer2/h$b;->c:J

    iget-wide v4, p1, Lcom/google/android/exoplayer2/h$b;->b:J

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/m;->a(Lcom/google/android/exoplayer2/source/i$b;JJ)Lcom/google/android/exoplayer2/m$a;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/exoplayer2/m$a;)Lcom/google/android/exoplayer2/m$a;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p1, Lcom/google/android/exoplayer2/m$a;->a:Lcom/google/android/exoplayer2/source/i$b;

    invoke-direct {p0, p1, v0}, Lcom/google/android/exoplayer2/m;->a(Lcom/google/android/exoplayer2/m$a;Lcom/google/android/exoplayer2/source/i$b;)Lcom/google/android/exoplayer2/m$a;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/exoplayer2/m$a;I)Lcom/google/android/exoplayer2/m$a;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p1, Lcom/google/android/exoplayer2/m$a;->a:Lcom/google/android/exoplayer2/source/i$b;

    .line 262
    invoke-virtual {v0, p2}, Lcom/google/android/exoplayer2/source/i$b;->a(I)Lcom/google/android/exoplayer2/source/i$b;

    move-result-object v0

    .line 261
    invoke-direct {p0, p1, v0}, Lcom/google/android/exoplayer2/m;->a(Lcom/google/android/exoplayer2/m$a;Lcom/google/android/exoplayer2/source/i$b;)Lcom/google/android/exoplayer2/m$a;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/exoplayer2/m$a;JJ)Lcom/google/android/exoplayer2/m$a;
    .locals 10

    .prologue
    .line 155
    iget-boolean v0, p1, Lcom/google/android/exoplayer2/m$a;->f:Z

    if-eqz v0, :cond_3

    .line 156
    iget-object v0, p0, Lcom/google/android/exoplayer2/m;->c:Lcom/google/android/exoplayer2/u;

    iget-object v1, p1, Lcom/google/android/exoplayer2/m$a;->a:Lcom/google/android/exoplayer2/source/i$b;

    iget v1, v1, Lcom/google/android/exoplayer2/source/i$b;->b:I

    iget-object v2, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    iget-object v3, p0, Lcom/google/android/exoplayer2/m;->b:Lcom/google/android/exoplayer2/u$b;

    iget v4, p0, Lcom/google/android/exoplayer2/m;->d:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;Lcom/google/android/exoplayer2/u$b;I)I

    move-result v0

    .line 158
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 160
    const/4 v0, 0x0

    .line 227
    :goto_0
    return-object v0

    .line 164
    :cond_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/m;->c:Lcom/google/android/exoplayer2/u;

    iget-object v2, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;)Lcom/google/android/exoplayer2/u$a;

    move-result-object v1

    iget v3, v1, Lcom/google/android/exoplayer2/u$a;->c:I

    .line 165
    iget-object v1, p0, Lcom/google/android/exoplayer2/m;->c:Lcom/google/android/exoplayer2/u;

    iget-object v2, p0, Lcom/google/android/exoplayer2/m;->b:Lcom/google/android/exoplayer2/u$b;

    invoke-virtual {v1, v3, v2}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$b;)Lcom/google/android/exoplayer2/u$b;

    move-result-object v1

    iget v1, v1, Lcom/google/android/exoplayer2/u$b;->f:I

    if-ne v1, v0, :cond_2

    .line 171
    iget-wide v0, p1, Lcom/google/android/exoplayer2/m$a;->e:J

    add-long/2addr v0, p2

    sub-long v6, v0, p4

    .line 173
    iget-object v0, p0, Lcom/google/android/exoplayer2/m;->c:Lcom/google/android/exoplayer2/u;

    iget-object v1, p0, Lcom/google/android/exoplayer2/m;->b:Lcom/google/android/exoplayer2/u$b;

    iget-object v2, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    const-wide/16 v8, 0x0

    .line 174
    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    .line 173
    invoke-virtual/range {v0 .. v7}, Lcom/google/android/exoplayer2/u;->a(Lcom/google/android/exoplayer2/u$b;Lcom/google/android/exoplayer2/u$a;IJJ)Landroid/util/Pair;

    move-result-object v2

    .line 175
    if-nez v2, :cond_1

    .line 176
    const/4 v0, 0x0

    goto :goto_0

    .line 178
    :cond_1
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 179
    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    move v0, v1

    .line 183
    :goto_1
    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/exoplayer2/m;->a(IJ)Lcom/google/android/exoplayer2/source/i$b;

    move-result-object v1

    move-object v0, p0

    move-wide v4, v2

    .line 184
    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/m;->a(Lcom/google/android/exoplayer2/source/i$b;JJ)Lcom/google/android/exoplayer2/m$a;

    move-result-object v0

    goto :goto_0

    .line 181
    :cond_2
    const-wide/16 v2, 0x0

    goto :goto_1

    .line 187
    :cond_3
    iget-object v0, p1, Lcom/google/android/exoplayer2/m$a;->a:Lcom/google/android/exoplayer2/source/i$b;

    .line 188
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/i$b;->a()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 189
    iget v2, v0, Lcom/google/android/exoplayer2/source/i$b;->c:I

    .line 190
    iget-object v1, p0, Lcom/google/android/exoplayer2/m;->c:Lcom/google/android/exoplayer2/u;

    iget v3, v0, Lcom/google/android/exoplayer2/source/i$b;->b:I

    iget-object v4, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;)Lcom/google/android/exoplayer2/u$a;

    .line 191
    iget-object v1, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/u$a;->d(I)I

    move-result v1

    .line 192
    const/4 v3, -0x1

    if-ne v1, v3, :cond_4

    .line 193
    const/4 v0, 0x0

    goto :goto_0

    .line 195
    :cond_4
    iget v3, v0, Lcom/google/android/exoplayer2/source/i$b;->d:I

    add-int/lit8 v3, v3, 0x1

    .line 196
    if-ge v3, v1, :cond_6

    .line 198
    iget-object v1, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/exoplayer2/u$a;->a(II)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_5
    iget v1, v0, Lcom/google/android/exoplayer2/source/i$b;->b:I

    iget-wide v4, p1, Lcom/google/android/exoplayer2/m$a;->d:J

    move-object v0, p0

    .line 199
    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/m;->a(IIIJ)Lcom/google/android/exoplayer2/m$a;

    move-result-object v0

    goto/16 :goto_0

    .line 203
    :cond_6
    iget-object v1, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    iget-wide v2, p1, Lcom/google/android/exoplayer2/m$a;->d:J

    .line 204
    invoke-virtual {v1, v2, v3}, Lcom/google/android/exoplayer2/u$a;->b(J)I

    move-result v1

    .line 205
    const/4 v2, -0x1

    if-ne v1, v2, :cond_7

    const-wide/high16 v4, -0x8000000000000000L

    .line 207
    :goto_2
    iget v1, v0, Lcom/google/android/exoplayer2/source/i$b;->b:I

    iget-wide v2, p1, Lcom/google/android/exoplayer2/m$a;->d:J

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/m;->a(IJJ)Lcom/google/android/exoplayer2/m$a;

    move-result-object v0

    goto/16 :goto_0

    .line 205
    :cond_7
    iget-object v2, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    .line 206
    invoke-virtual {v2, v1}, Lcom/google/android/exoplayer2/u$a;->a(I)J

    move-result-wide v4

    goto :goto_2

    .line 210
    :cond_8
    iget-wide v2, p1, Lcom/google/android/exoplayer2/m$a;->c:J

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v1, v2, v4

    if-eqz v1, :cond_a

    .line 212
    iget-object v1, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    iget-wide v2, p1, Lcom/google/android/exoplayer2/m$a;->c:J

    .line 213
    invoke-virtual {v1, v2, v3}, Lcom/google/android/exoplayer2/u$a;->a(J)I

    move-result v2

    .line 214
    iget-object v1, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/exoplayer2/u$a;->a(II)Z

    move-result v1

    if-nez v1, :cond_9

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_9
    iget v1, v0, Lcom/google/android/exoplayer2/source/i$b;->b:I

    const/4 v3, 0x0

    iget-wide v4, p1, Lcom/google/android/exoplayer2/m$a;->c:J

    move-object v0, p0

    .line 215
    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/m;->a(IIIJ)Lcom/google/android/exoplayer2/m$a;

    move-result-object v0

    goto/16 :goto_0

    .line 219
    :cond_a
    iget-object v1, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/u$a;->d()I

    move-result v2

    .line 220
    if-eqz v2, :cond_b

    iget-object v1, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    add-int/lit8 v3, v2, -0x1

    .line 221
    invoke-virtual {v1, v3}, Lcom/google/android/exoplayer2/u$a;->a(I)J

    move-result-wide v4

    const-wide/high16 v6, -0x8000000000000000L

    cmp-long v1, v4, v6

    if-nez v1, :cond_b

    iget-object v1, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    add-int/lit8 v3, v2, -0x1

    .line 222
    invoke-virtual {v1, v3}, Lcom/google/android/exoplayer2/u$a;->c(I)Z

    move-result v1

    if-nez v1, :cond_b

    iget-object v1, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    add-int/lit8 v3, v2, -0x1

    const/4 v4, 0x0

    .line 223
    invoke-virtual {v1, v3, v4}, Lcom/google/android/exoplayer2/u$a;->a(II)Z

    move-result v1

    if-nez v1, :cond_c

    .line 224
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 226
    :cond_c
    iget-object v1, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/u$a;->a()J

    move-result-wide v4

    .line 227
    iget v1, v0, Lcom/google/android/exoplayer2/source/i$b;->b:I

    add-int/lit8 v2, v2, -0x1

    const/4 v3, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/m;->a(IIIJ)Lcom/google/android/exoplayer2/m$a;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public a(IJ)Lcom/google/android/exoplayer2/source/i$b;
    .locals 4

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/exoplayer2/m;->c:Lcom/google/android/exoplayer2/u;

    iget-object v1, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;)Lcom/google/android/exoplayer2/u$a;

    .line 239
    iget-object v0, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/exoplayer2/u$a;->a(J)I

    move-result v1

    .line 240
    const/4 v0, -0x1

    if-ne v1, v0, :cond_0

    .line 241
    new-instance v0, Lcom/google/android/exoplayer2/source/i$b;

    invoke-direct {v0, p1}, Lcom/google/android/exoplayer2/source/i$b;-><init>(I)V

    .line 244
    :goto_0
    return-object v0

    .line 243
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/m;->a:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/u$a;->b(I)I

    move-result v2

    .line 244
    new-instance v0, Lcom/google/android/exoplayer2/source/i$b;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/exoplayer2/source/i$b;-><init>(III)V

    goto :goto_0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 129
    iput p1, p0, Lcom/google/android/exoplayer2/m;->d:I

    .line 130
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/u;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/google/android/exoplayer2/m;->c:Lcom/google/android/exoplayer2/u;

    .line 122
    return-void
.end method
