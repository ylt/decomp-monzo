.class public abstract Lcom/google/android/exoplayer2/u;
.super Ljava/lang/Object;
.source "Timeline.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/u$a;,
        Lcom/google/android/exoplayer2/u$b;
    }
.end annotation


# static fields
.field public static final a:Lcom/google/android/exoplayer2/u;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 512
    new-instance v0, Lcom/google/android/exoplayer2/u$1;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/u$1;-><init>()V

    sput-object v0, Lcom/google/android/exoplayer2/u;->a:Lcom/google/android/exoplayer2/u;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 258
    return-void
.end method


# virtual methods
.method public a(II)I
    .locals 1

    .prologue
    .line 563
    packed-switch p2, :pswitch_data_0

    .line 571
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 565
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/u;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    .line 569
    :goto_0
    return v0

    .line 565
    :cond_0
    add-int/lit8 v0, p1, 0x1

    goto :goto_0

    :pswitch_1
    move v0, p1

    .line 567
    goto :goto_0

    .line 569
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/u;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    add-int/lit8 v0, p1, 0x1

    goto :goto_0

    .line 563
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(ILcom/google/android/exoplayer2/u$a;Lcom/google/android/exoplayer2/u$b;I)I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 676
    invoke-virtual {p0, p1, p2}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;)Lcom/google/android/exoplayer2/u$a;

    move-result-object v1

    iget v1, v1, Lcom/google/android/exoplayer2/u$a;->c:I

    .line 677
    invoke-virtual {p0, v1, p3}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$b;)Lcom/google/android/exoplayer2/u$b;

    move-result-object v2

    iget v2, v2, Lcom/google/android/exoplayer2/u$b;->g:I

    if-ne v2, p1, :cond_1

    .line 678
    invoke-virtual {p0, v1, p4}, Lcom/google/android/exoplayer2/u;->a(II)I

    move-result v1

    .line 679
    if-ne v1, v0, :cond_0

    .line 684
    :goto_0
    return v0

    .line 682
    :cond_0
    invoke-virtual {p0, v1, p3}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$b;)Lcom/google/android/exoplayer2/u$b;

    move-result-object v0

    iget v0, v0, Lcom/google/android/exoplayer2/u$b;->f:I

    goto :goto_0

    .line 684
    :cond_1
    add-int/lit8 v0, p1, 0x1

    goto :goto_0
.end method

.method public abstract a(Ljava/lang/Object;)I
.end method

.method public final a(Lcom/google/android/exoplayer2/u$b;Lcom/google/android/exoplayer2/u$a;IJ)Landroid/util/Pair;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/u$b;",
            "Lcom/google/android/exoplayer2/u$a;",
            "IJ)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 720
    const-wide/16 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/exoplayer2/u;->a(Lcom/google/android/exoplayer2/u$b;Lcom/google/android/exoplayer2/u$a;IJJ)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/exoplayer2/u$b;Lcom/google/android/exoplayer2/u$a;IJJ)Landroid/util/Pair;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/u$b;",
            "Lcom/google/android/exoplayer2/u$a;",
            "IJJ)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const-wide v6, -0x7fffffffffffffffL    # -4.9E-324

    .line 739
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/u;->b()I

    move-result v0

    invoke-static {p3, v3, v0}, Lcom/google/android/exoplayer2/util/a;->a(III)I

    move-object v0, p0

    move v1, p3

    move-object v2, p1

    move-wide v4, p6

    .line 740
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$b;ZJ)Lcom/google/android/exoplayer2/u$b;

    .line 741
    cmp-long v0, p4, v6

    if-nez v0, :cond_0

    .line 742
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/u$b;->a()J

    move-result-wide p4

    .line 743
    cmp-long v0, p4, v6

    if-nez v0, :cond_0

    .line 744
    const/4 v0, 0x0

    .line 755
    :goto_0
    return-object v0

    .line 747
    :cond_0
    iget v4, p1, Lcom/google/android/exoplayer2/u$b;->f:I

    .line 748
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/u$b;->c()J

    move-result-wide v0

    add-long v2, v0, p4

    .line 749
    invoke-virtual {p0, v4, p2}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;)Lcom/google/android/exoplayer2/u$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/u$a;->a()J

    move-result-wide v0

    .line 750
    :goto_1
    cmp-long v5, v0, v6

    if-eqz v5, :cond_1

    cmp-long v5, v2, v0

    if-ltz v5, :cond_1

    iget v5, p1, Lcom/google/android/exoplayer2/u$b;->g:I

    if-ge v4, v5, :cond_1

    .line 752
    sub-long/2addr v2, v0

    .line 753
    add-int/lit8 v4, v4, 0x1

    invoke-virtual {p0, v4, p2}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;)Lcom/google/android/exoplayer2/u$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/u$a;->a()J

    move-result-wide v0

    goto :goto_1

    .line 755
    :cond_1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(ILcom/google/android/exoplayer2/u$a;)Lcom/google/android/exoplayer2/u$a;
    .locals 1

    .prologue
    .line 711
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;Z)Lcom/google/android/exoplayer2/u$a;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(ILcom/google/android/exoplayer2/u$a;Z)Lcom/google/android/exoplayer2/u$a;
.end method

.method public final a(ILcom/google/android/exoplayer2/u$b;)Lcom/google/android/exoplayer2/u$b;
    .locals 1

    .prologue
    .line 629
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$b;Z)Lcom/google/android/exoplayer2/u$b;

    move-result-object v0

    return-object v0
.end method

.method public a(ILcom/google/android/exoplayer2/u$b;Z)Lcom/google/android/exoplayer2/u$b;
    .locals 6

    .prologue
    .line 642
    const-wide/16 v4, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$b;ZJ)Lcom/google/android/exoplayer2/u$b;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(ILcom/google/android/exoplayer2/u$b;ZJ)Lcom/google/android/exoplayer2/u$b;
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 546
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/u;->b()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract b()I
.end method

.method public b(II)I
    .locals 1

    .prologue
    .line 584
    packed-switch p2, :pswitch_data_0

    .line 592
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 586
    :pswitch_0
    if-nez p1, :cond_0

    const/4 v0, -0x1

    .line 590
    :goto_0
    return v0

    .line 586
    :cond_0
    add-int/lit8 v0, p1, -0x1

    goto :goto_0

    :pswitch_1
    move v0, p1

    .line 588
    goto :goto_0

    .line 590
    :pswitch_2
    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/u;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    add-int/lit8 v0, p1, -0x1

    goto :goto_0

    .line 584
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b(ILcom/google/android/exoplayer2/u$a;Lcom/google/android/exoplayer2/u$b;I)Z
    .locals 2

    .prologue
    .line 699
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;Lcom/google/android/exoplayer2/u$b;I)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract c()I
.end method

.method public final c(II)Z
    .locals 2

    .prologue
    .line 605
    invoke-virtual {p0, p1, p2}, Lcom/google/android/exoplayer2/u;->a(II)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(II)Z
    .locals 2

    .prologue
    .line 617
    invoke-virtual {p0, p1, p2}, Lcom/google/android/exoplayer2/u;->b(II)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
