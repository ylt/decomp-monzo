.class final Lcom/google/android/exoplayer2/h;
.super Ljava/lang/Object;
.source "ExoPlayerImplInternal.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lcom/google/android/exoplayer2/b/g$a;
.implements Lcom/google/android/exoplayer2/source/h$a;
.implements Lcom/google/android/exoplayer2/source/i$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/h$c;,
        Lcom/google/android/exoplayer2/h$a;,
        Lcom/google/android/exoplayer2/h$d;,
        Lcom/google/android/exoplayer2/h$b;
    }
.end annotation


# instance fields
.field private A:J

.field private B:I

.field private C:Lcom/google/android/exoplayer2/h$c;

.field private D:J

.field private E:Lcom/google/android/exoplayer2/h$a;

.field private F:Lcom/google/android/exoplayer2/h$a;

.field private G:Lcom/google/android/exoplayer2/h$a;

.field private H:Lcom/google/android/exoplayer2/u;

.field private final a:[Lcom/google/android/exoplayer2/p;

.field private final b:[Lcom/google/android/exoplayer2/q;

.field private final c:Lcom/google/android/exoplayer2/b/g;

.field private final d:Lcom/google/android/exoplayer2/l;

.field private final e:Lcom/google/android/exoplayer2/util/o;

.field private final f:Landroid/os/Handler;

.field private final g:Landroid/os/HandlerThread;

.field private final h:Landroid/os/Handler;

.field private final i:Lcom/google/android/exoplayer2/e;

.field private final j:Lcom/google/android/exoplayer2/u$b;

.field private final k:Lcom/google/android/exoplayer2/u$a;

.field private final l:Lcom/google/android/exoplayer2/m;

.field private m:Lcom/google/android/exoplayer2/h$b;

.field private n:Lcom/google/android/exoplayer2/n;

.field private o:Lcom/google/android/exoplayer2/p;

.field private p:Lcom/google/android/exoplayer2/util/g;

.field private q:Lcom/google/android/exoplayer2/source/i;

.field private r:[Lcom/google/android/exoplayer2/p;

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:I

.field private x:I

.field private y:I

.field private z:I


# direct methods
.method public constructor <init>([Lcom/google/android/exoplayer2/p;Lcom/google/android/exoplayer2/b/g;Lcom/google/android/exoplayer2/l;ZILandroid/os/Handler;Lcom/google/android/exoplayer2/h$b;Lcom/google/android/exoplayer2/e;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 193
    iput-object p1, p0, Lcom/google/android/exoplayer2/h;->a:[Lcom/google/android/exoplayer2/p;

    .line 194
    iput-object p2, p0, Lcom/google/android/exoplayer2/h;->c:Lcom/google/android/exoplayer2/b/g;

    .line 195
    iput-object p3, p0, Lcom/google/android/exoplayer2/h;->d:Lcom/google/android/exoplayer2/l;

    .line 196
    iput-boolean p4, p0, Lcom/google/android/exoplayer2/h;->t:Z

    .line 197
    iput p5, p0, Lcom/google/android/exoplayer2/h;->x:I

    .line 198
    iput-object p6, p0, Lcom/google/android/exoplayer2/h;->h:Landroid/os/Handler;

    .line 199
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/exoplayer2/h;->w:I

    .line 200
    iput-object p7, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    .line 201
    iput-object p8, p0, Lcom/google/android/exoplayer2/h;->i:Lcom/google/android/exoplayer2/e;

    .line 203
    array-length v0, p1

    new-array v0, v0, [Lcom/google/android/exoplayer2/q;

    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->b:[Lcom/google/android/exoplayer2/q;

    move v0, v1

    .line 204
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 205
    aget-object v2, p1, v0

    invoke-interface {v2, v0}, Lcom/google/android/exoplayer2/p;->a(I)V

    .line 206
    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->b:[Lcom/google/android/exoplayer2/q;

    aget-object v3, p1, v0

    invoke-interface {v3}, Lcom/google/android/exoplayer2/p;->b()Lcom/google/android/exoplayer2/q;

    move-result-object v3

    aput-object v3, v2, v0

    .line 204
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 208
    :cond_0
    new-instance v0, Lcom/google/android/exoplayer2/util/o;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/util/o;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->e:Lcom/google/android/exoplayer2/util/o;

    .line 209
    new-array v0, v1, [Lcom/google/android/exoplayer2/p;

    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->r:[Lcom/google/android/exoplayer2/p;

    .line 210
    new-instance v0, Lcom/google/android/exoplayer2/u$b;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/u$b;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->j:Lcom/google/android/exoplayer2/u$b;

    .line 211
    new-instance v0, Lcom/google/android/exoplayer2/u$a;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/u$a;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->k:Lcom/google/android/exoplayer2/u$a;

    .line 212
    new-instance v0, Lcom/google/android/exoplayer2/m;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/m;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->l:Lcom/google/android/exoplayer2/m;

    .line 213
    invoke-virtual {p2, p0}, Lcom/google/android/exoplayer2/b/g;->a(Lcom/google/android/exoplayer2/b/g$a;)V

    .line 214
    sget-object v0, Lcom/google/android/exoplayer2/n;->a:Lcom/google/android/exoplayer2/n;

    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->n:Lcom/google/android/exoplayer2/n;

    .line 218
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ExoPlayerImplInternal:Handler"

    const/16 v2, -0x10

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->g:Landroid/os/HandlerThread;

    .line 220
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->g:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 221
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->g:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->f:Landroid/os/Handler;

    .line 222
    return-void
.end method

.method private a(ILcom/google/android/exoplayer2/u;Lcom/google/android/exoplayer2/u;)I
    .locals 7

    .prologue
    const/4 v2, -0x1

    .line 1172
    .line 1173
    invoke-virtual {p2}, Lcom/google/android/exoplayer2/u;->c()I

    move-result v3

    .line 1174
    const/4 v0, 0x0

    move v1, v2

    :goto_0
    if-ge v0, v3, :cond_0

    if-ne v1, v2, :cond_0

    .line 1175
    iget-object v4, p0, Lcom/google/android/exoplayer2/h;->k:Lcom/google/android/exoplayer2/u$a;

    iget-object v5, p0, Lcom/google/android/exoplayer2/h;->j:Lcom/google/android/exoplayer2/u$b;

    iget v6, p0, Lcom/google/android/exoplayer2/h;->x:I

    invoke-virtual {p2, p1, v4, v5, v6}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;Lcom/google/android/exoplayer2/u$b;I)I

    move-result p1

    .line 1176
    if-ne p1, v2, :cond_1

    .line 1183
    :cond_0
    return v1

    .line 1180
    :cond_1
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->k:Lcom/google/android/exoplayer2/u$a;

    const/4 v4, 0x1

    .line 1181
    invoke-virtual {p2, p1, v1, v4}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;Z)Lcom/google/android/exoplayer2/u$a;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/exoplayer2/u$a;->b:Ljava/lang/Object;

    .line 1180
    invoke-virtual {p3, v1}, Lcom/google/android/exoplayer2/u;->a(Ljava/lang/Object;)I

    move-result v1

    .line 1174
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private a(Lcom/google/android/exoplayer2/source/i$b;J)J
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 698
    invoke-direct {p0}, Lcom/google/android/exoplayer2/h;->c()V

    .line 699
    iput-boolean v3, p0, Lcom/google/android/exoplayer2/h;->u:Z

    .line 700
    invoke-direct {p0, v7}, Lcom/google/android/exoplayer2/h;->b(I)V

    .line 703
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    if-nez v0, :cond_2

    .line 705
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    if-eqz v0, :cond_8

    .line 706
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/h$a;->e()V

    move-object v0, v2

    .line 723
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    if-ne v1, v0, :cond_1

    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget-object v4, p0, Lcom/google/android/exoplayer2/h;->F:Lcom/google/android/exoplayer2/h$a;

    if-eq v1, v4, :cond_5

    .line 725
    :cond_1
    iget-object v4, p0, Lcom/google/android/exoplayer2/h;->r:[Lcom/google/android/exoplayer2/p;

    array-length v5, v4

    move v1, v3

    :goto_1
    if-ge v1, v5, :cond_4

    aget-object v6, v4, v1

    .line 726
    invoke-interface {v6}, Lcom/google/android/exoplayer2/p;->l()V

    .line 725
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 710
    :cond_2
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    move-object v0, v2

    .line 711
    :goto_2
    if-eqz v1, :cond_0

    .line 712
    invoke-direct {p0, p1, p2, p3, v1}, Lcom/google/android/exoplayer2/h;->a(Lcom/google/android/exoplayer2/source/i$b;JLcom/google/android/exoplayer2/h$a;)Z

    move-result v4

    if-eqz v4, :cond_3

    move-object v0, v1

    .line 717
    :goto_3
    iget-object v1, v1, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    goto :goto_2

    .line 715
    :cond_3
    invoke-virtual {v1}, Lcom/google/android/exoplayer2/h$a;->e()V

    goto :goto_3

    .line 728
    :cond_4
    new-array v1, v3, [Lcom/google/android/exoplayer2/p;

    iput-object v1, p0, Lcom/google/android/exoplayer2/h;->r:[Lcom/google/android/exoplayer2/p;

    .line 729
    iput-object v2, p0, Lcom/google/android/exoplayer2/h;->p:Lcom/google/android/exoplayer2/util/g;

    .line 730
    iput-object v2, p0, Lcom/google/android/exoplayer2/h;->o:Lcom/google/android/exoplayer2/p;

    .line 731
    iput-object v2, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    .line 735
    :cond_5
    if-eqz v0, :cond_7

    .line 736
    iput-object v2, v0, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    .line 737
    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    .line 738
    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->F:Lcom/google/android/exoplayer2/h$a;

    .line 739
    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/h;->b(Lcom/google/android/exoplayer2/h$a;)V

    .line 740
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/h$a;->i:Z

    if-eqz v0, :cond_6

    .line 741
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->a:Lcom/google/android/exoplayer2/source/h;

    invoke-interface {v0, p2, p3}, Lcom/google/android/exoplayer2/source/h;->b(J)J

    move-result-wide p2

    .line 743
    :cond_6
    invoke-direct {p0, p2, p3}, Lcom/google/android/exoplayer2/h;->a(J)V

    .line 744
    invoke-direct {p0}, Lcom/google/android/exoplayer2/h;->l()V

    .line 752
    :goto_4
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->f:Landroid/os/Handler;

    invoke-virtual {v0, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 753
    return-wide p2

    .line 746
    :cond_7
    iput-object v2, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    .line 747
    iput-object v2, p0, Lcom/google/android/exoplayer2/h;->F:Lcom/google/android/exoplayer2/h$a;

    .line 748
    iput-object v2, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    .line 749
    invoke-direct {p0, p2, p3}, Lcom/google/android/exoplayer2/h;->a(J)V

    goto :goto_4

    :cond_8
    move-object v0, v2

    goto :goto_0
.end method

.method private a(IJ)Landroid/util/Pair;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1238
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->H:Lcom/google/android/exoplayer2/u;

    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->j:Lcom/google/android/exoplayer2/u$b;

    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->k:Lcom/google/android/exoplayer2/u$a;

    move v3, p1

    move-wide v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/exoplayer2/u;->a(Lcom/google/android/exoplayer2/u$b;Lcom/google/android/exoplayer2/u$a;IJ)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/exoplayer2/h$a;I)Lcom/google/android/exoplayer2/h$a;
    .locals 2

    .prologue
    .line 1126
    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->l:Lcom/google/android/exoplayer2/m;

    iget-object v1, p1, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    .line 1127
    invoke-virtual {v0, v1, p2}, Lcom/google/android/exoplayer2/m;->a(Lcom/google/android/exoplayer2/m$a;I)Lcom/google/android/exoplayer2/m$a;

    move-result-object v0

    iput-object v0, p1, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    .line 1128
    iget-object v0, p1, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/m$a;->f:Z

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    if-nez v0, :cond_1

    .line 1129
    :cond_0
    return-object p1

    .line 1131
    :cond_1
    iget-object p1, p1, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    goto :goto_0
.end method

.method private a(J)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    .line 770
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    if-nez v0, :cond_0

    const-wide/32 v0, 0x3938700

    add-long/2addr v0, p1

    .line 772
    :goto_0
    iput-wide v0, p0, Lcom/google/android/exoplayer2/h;->D:J

    .line 773
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->e:Lcom/google/android/exoplayer2/util/o;

    iget-wide v2, p0, Lcom/google/android/exoplayer2/h;->D:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/exoplayer2/util/o;->a(J)V

    .line 774
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->r:[Lcom/google/android/exoplayer2/p;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 775
    iget-wide v4, p0, Lcom/google/android/exoplayer2/h;->D:J

    invoke-interface {v3, v4, v5}, Lcom/google/android/exoplayer2/p;->a(J)V

    .line 774
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 770
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    .line 772
    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/h$a;->a(J)J

    move-result-wide v0

    goto :goto_0

    .line 777
    :cond_1
    return-void
.end method

.method private a(JJ)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 638
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->f:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 639
    add-long v0, p1, p3

    .line 640
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 641
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    .line 642
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->f:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 646
    :goto_0
    return-void

    .line 644
    :cond_0
    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->f:Landroid/os/Handler;

    invoke-virtual {v2, v4, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method private a(Landroid/util/Pair;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/exoplayer2/u;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    const/4 v6, 0x0

    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v3, 0x1

    const/4 v10, -0x1

    .line 973
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->H:Lcom/google/android/exoplayer2/u;

    .line 974
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/u;

    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->H:Lcom/google/android/exoplayer2/u;

    .line 975
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->l:Lcom/google/android/exoplayer2/m;

    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->H:Lcom/google/android/exoplayer2/u;

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/m;->a(Lcom/google/android/exoplayer2/u;)V

    .line 976
    iget-object v7, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    .line 978
    if-nez v1, :cond_6

    .line 979
    iget v0, p0, Lcom/google/android/exoplayer2/h;->B:I

    if-lez v0, :cond_2

    .line 980
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->C:Lcom/google/android/exoplayer2/h$c;

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/h;->b(Lcom/google/android/exoplayer2/h$c;)Landroid/util/Pair;

    move-result-object v1

    .line 981
    iget v8, p0, Lcom/google/android/exoplayer2/h;->B:I

    .line 982
    iput v6, p0, Lcom/google/android/exoplayer2/h;->B:I

    .line 983
    iput-object v11, p0, Lcom/google/android/exoplayer2/h;->C:Lcom/google/android/exoplayer2/h$c;

    .line 984
    if-nez v1, :cond_0

    .line 987
    invoke-direct {p0, v7, v8}, Lcom/google/android/exoplayer2/h;->a(Ljava/lang/Object;I)V

    .line 1122
    :goto_0
    return-void

    .line 989
    :cond_0
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 990
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 991
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->l:Lcom/google/android/exoplayer2/m;

    .line 992
    invoke-virtual {v0, v2, v4, v5}, Lcom/google/android/exoplayer2/m;->a(IJ)Lcom/google/android/exoplayer2/source/i$b;

    move-result-object v1

    .line 993
    new-instance v0, Lcom/google/android/exoplayer2/h$b;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/source/i$b;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    const-wide/16 v2, 0x0

    :goto_1
    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/h$b;-><init>(Lcom/google/android/exoplayer2/source/i$b;JJ)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    .line 994
    invoke-direct {p0, v7, v8}, Lcom/google/android/exoplayer2/h;->b(Ljava/lang/Object;I)V

    goto :goto_0

    :cond_1
    move-wide v2, v4

    .line 993
    goto :goto_1

    .line 996
    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/h$b;->b:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_5

    .line 997
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->H:Lcom/google/android/exoplayer2/u;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/u;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 998
    invoke-direct {p0, v7}, Lcom/google/android/exoplayer2/h;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 1000
    :cond_3
    invoke-direct {p0, v6, v4, v5}, Lcom/google/android/exoplayer2/h;->a(IJ)Landroid/util/Pair;

    move-result-object v1

    .line 1001
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1002
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 1003
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->l:Lcom/google/android/exoplayer2/m;

    invoke-virtual {v0, v2, v4, v5}, Lcom/google/android/exoplayer2/m;->a(IJ)Lcom/google/android/exoplayer2/source/i$b;

    move-result-object v1

    .line 1005
    new-instance v0, Lcom/google/android/exoplayer2/h$b;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/source/i$b;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    const-wide/16 v2, 0x0

    :goto_2
    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/h$b;-><init>(Lcom/google/android/exoplayer2/source/i$b;JJ)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    .line 1007
    invoke-direct {p0, v7}, Lcom/google/android/exoplayer2/h;->b(Ljava/lang/Object;)V

    goto :goto_0

    :cond_4
    move-wide v2, v4

    .line 1005
    goto :goto_2

    .line 1010
    :cond_5
    invoke-direct {p0, v7}, Lcom/google/android/exoplayer2/h;->b(Ljava/lang/Object;)V

    goto :goto_0

    .line 1015
    :cond_6
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$b;->a:Lcom/google/android/exoplayer2/source/i$b;

    iget v8, v0, Lcom/google/android/exoplayer2/source/i$b;->b:I

    .line 1016
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    if-eqz v0, :cond_7

    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    .line 1018
    :goto_3
    if-nez v2, :cond_8

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/u;->c()I

    move-result v0

    if-lt v8, v0, :cond_8

    .line 1019
    invoke-direct {p0, v7}, Lcom/google/android/exoplayer2/h;->b(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1016
    :cond_7
    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    goto :goto_3

    .line 1022
    :cond_8
    if-nez v2, :cond_9

    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->k:Lcom/google/android/exoplayer2/u$a;

    .line 1023
    invoke-virtual {v1, v8, v0, v3}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;Z)Lcom/google/android/exoplayer2/u$a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/exoplayer2/u$a;->b:Ljava/lang/Object;

    .line 1024
    :goto_4
    iget-object v9, p0, Lcom/google/android/exoplayer2/h;->H:Lcom/google/android/exoplayer2/u;

    invoke-virtual {v9, v0}, Lcom/google/android/exoplayer2/u;->a(Ljava/lang/Object;)I

    move-result v0

    .line 1025
    if-ne v0, v10, :cond_d

    .line 1028
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->H:Lcom/google/android/exoplayer2/u;

    invoke-direct {p0, v8, v1, v0}, Lcom/google/android/exoplayer2/h;->a(ILcom/google/android/exoplayer2/u;Lcom/google/android/exoplayer2/u;)I

    move-result v0

    .line 1029
    if-ne v0, v10, :cond_a

    .line 1031
    invoke-direct {p0, v7}, Lcom/google/android/exoplayer2/h;->a(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1023
    :cond_9
    iget-object v0, v2, Lcom/google/android/exoplayer2/h$a;->b:Ljava/lang/Object;

    goto :goto_4

    .line 1035
    :cond_a
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->H:Lcom/google/android/exoplayer2/u;

    iget-object v6, p0, Lcom/google/android/exoplayer2/h;->k:Lcom/google/android/exoplayer2/u$a;

    .line 1036
    invoke-virtual {v1, v0, v6}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;)Lcom/google/android/exoplayer2/u$a;

    move-result-object v0

    iget v0, v0, Lcom/google/android/exoplayer2/u$a;->c:I

    .line 1035
    invoke-direct {p0, v0, v4, v5}, Lcom/google/android/exoplayer2/h;->a(IJ)Landroid/util/Pair;

    move-result-object v1

    .line 1037
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 1038
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 1039
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->H:Lcom/google/android/exoplayer2/u;

    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->k:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v0, v4, v1, v3}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;Z)Lcom/google/android/exoplayer2/u$a;

    .line 1040
    if-eqz v2, :cond_c

    .line 1043
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->k:Lcom/google/android/exoplayer2/u$a;

    iget-object v1, v0, Lcom/google/android/exoplayer2/u$a;->b:Ljava/lang/Object;

    .line 1044
    iget-object v0, v2, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    invoke-virtual {v0, v10}, Lcom/google/android/exoplayer2/m$a;->a(I)Lcom/google/android/exoplayer2/m$a;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    move-object v0, v2

    .line 1045
    :goto_5
    iget-object v2, v0, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    if-eqz v2, :cond_c

    .line 1046
    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    .line 1047
    iget-object v2, v0, Lcom/google/android/exoplayer2/h$a;->b:Ljava/lang/Object;

    invoke-virtual {v2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1048
    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->l:Lcom/google/android/exoplayer2/m;

    iget-object v3, v0, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/exoplayer2/m;->a(Lcom/google/android/exoplayer2/m$a;I)Lcom/google/android/exoplayer2/m$a;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    goto :goto_5

    .line 1051
    :cond_b
    iget-object v2, v0, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    invoke-virtual {v2, v10}, Lcom/google/android/exoplayer2/m$a;->a(I)Lcom/google/android/exoplayer2/m$a;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    goto :goto_5

    .line 1056
    :cond_c
    new-instance v0, Lcom/google/android/exoplayer2/source/i$b;

    invoke-direct {v0, v4}, Lcom/google/android/exoplayer2/source/i$b;-><init>(I)V

    .line 1057
    invoke-direct {p0, v0, v8, v9}, Lcom/google/android/exoplayer2/h;->a(Lcom/google/android/exoplayer2/source/i$b;J)J

    move-result-wide v2

    .line 1058
    new-instance v1, Lcom/google/android/exoplayer2/h$b;

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/exoplayer2/h$b;-><init>(Lcom/google/android/exoplayer2/source/i$b;J)V

    iput-object v1, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    .line 1059
    invoke-direct {p0, v7}, Lcom/google/android/exoplayer2/h;->b(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1064
    :cond_d
    if-eq v0, v8, :cond_e

    .line 1065
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    invoke-virtual {v1, v0}, Lcom/google/android/exoplayer2/h$b;->a(I)Lcom/google/android/exoplayer2/h$b;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    .line 1068
    :cond_e
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    iget-object v1, v1, Lcom/google/android/exoplayer2/h$b;->a:Lcom/google/android/exoplayer2/source/i$b;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/source/i$b;->a()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 1070
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->l:Lcom/google/android/exoplayer2/m;

    iget-object v8, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    iget-wide v8, v8, Lcom/google/android/exoplayer2/h$b;->c:J

    invoke-virtual {v1, v0, v8, v9}, Lcom/google/android/exoplayer2/m;->a(IJ)Lcom/google/android/exoplayer2/source/i$b;

    move-result-object v1

    .line 1072
    invoke-virtual {v1}, Lcom/google/android/exoplayer2/source/i$b;->a()Z

    move-result v8

    if-eqz v8, :cond_f

    iget v8, v1, Lcom/google/android/exoplayer2/source/i$b;->d:I

    iget-object v9, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    iget-object v9, v9, Lcom/google/android/exoplayer2/h$b;->a:Lcom/google/android/exoplayer2/source/i$b;

    iget v9, v9, Lcom/google/android/exoplayer2/source/i$b;->d:I

    if-eq v8, v9, :cond_11

    .line 1073
    :cond_f
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    iget-wide v2, v0, Lcom/google/android/exoplayer2/h$b;->c:J

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/exoplayer2/h;->a(Lcom/google/android/exoplayer2/source/i$b;J)J

    move-result-wide v2

    .line 1074
    invoke-virtual {v1}, Lcom/google/android/exoplayer2/source/i$b;->a()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    iget-wide v4, v0, Lcom/google/android/exoplayer2/h$b;->c:J

    .line 1075
    :cond_10
    new-instance v0, Lcom/google/android/exoplayer2/h$b;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/h$b;-><init>(Lcom/google/android/exoplayer2/source/i$b;JJ)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    .line 1076
    invoke-direct {p0, v7}, Lcom/google/android/exoplayer2/h;->b(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1081
    :cond_11
    if-nez v2, :cond_12

    .line 1083
    invoke-direct {p0, v7}, Lcom/google/android/exoplayer2/h;->b(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1089
    :cond_12
    invoke-direct {p0, v2, v0}, Lcom/google/android/exoplayer2/h;->a(Lcom/google/android/exoplayer2/h$a;I)Lcom/google/android/exoplayer2/h$a;

    move-result-object v1

    .line 1090
    :goto_6
    iget-object v2, v1, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    if-eqz v2, :cond_14

    .line 1092
    iget-object v2, v1, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    .line 1093
    iget-object v4, p0, Lcom/google/android/exoplayer2/h;->H:Lcom/google/android/exoplayer2/u;

    iget-object v5, p0, Lcom/google/android/exoplayer2/h;->k:Lcom/google/android/exoplayer2/u$a;

    iget-object v8, p0, Lcom/google/android/exoplayer2/h;->j:Lcom/google/android/exoplayer2/u$b;

    iget v9, p0, Lcom/google/android/exoplayer2/h;->x:I

    invoke-virtual {v4, v0, v5, v8, v9}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;Lcom/google/android/exoplayer2/u$b;I)I

    move-result v0

    .line 1094
    if-eq v0, v10, :cond_13

    iget-object v4, v2, Lcom/google/android/exoplayer2/h$a;->b:Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/exoplayer2/h;->H:Lcom/google/android/exoplayer2/u;

    iget-object v8, p0, Lcom/google/android/exoplayer2/h;->k:Lcom/google/android/exoplayer2/u$a;

    .line 1095
    invoke-virtual {v5, v0, v8, v3}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;Z)Lcom/google/android/exoplayer2/u$a;

    move-result-object v5

    iget-object v5, v5, Lcom/google/android/exoplayer2/u$a;->b:Ljava/lang/Object;

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 1097
    invoke-direct {p0, v2, v0}, Lcom/google/android/exoplayer2/h;->a(Lcom/google/android/exoplayer2/h$a;I)Lcom/google/android/exoplayer2/h$a;

    move-result-object v1

    goto :goto_6

    .line 1100
    :cond_13
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->F:Lcom/google/android/exoplayer2/h$a;

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->F:Lcom/google/android/exoplayer2/h$a;

    iget v0, v0, Lcom/google/android/exoplayer2/h$a;->c:I

    iget v4, v2, Lcom/google/android/exoplayer2/h$a;->c:I

    if-ge v0, v4, :cond_15

    move v0, v3

    .line 1102
    :goto_7
    if-nez v0, :cond_16

    .line 1105
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/m$a;->a:Lcom/google/android/exoplayer2/source/i$b;

    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    iget-wide v2, v1, Lcom/google/android/exoplayer2/h$b;->d:J

    .line 1106
    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/exoplayer2/h;->a(Lcom/google/android/exoplayer2/source/i$b;J)J

    move-result-wide v2

    .line 1107
    new-instance v0, Lcom/google/android/exoplayer2/h$b;

    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget-object v1, v1, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-object v1, v1, Lcom/google/android/exoplayer2/m$a;->a:Lcom/google/android/exoplayer2/source/i$b;

    iget-object v4, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    iget-wide v4, v4, Lcom/google/android/exoplayer2/h$b;->c:J

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/h$b;-><init>(Lcom/google/android/exoplayer2/source/i$b;JJ)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    .line 1121
    :cond_14
    :goto_8
    invoke-direct {p0, v7}, Lcom/google/android/exoplayer2/h;->b(Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_15
    move v0, v6

    .line 1100
    goto :goto_7

    .line 1112
    :cond_16
    iput-object v1, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    .line 1113
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    iput-object v11, v0, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    .line 1115
    invoke-direct {p0, v2}, Lcom/google/android/exoplayer2/h;->a(Lcom/google/android/exoplayer2/h$a;)V

    goto :goto_8
.end method

.method private a(Lcom/google/android/exoplayer2/h$a;)V
    .locals 0

    .prologue
    .line 1408
    :goto_0
    if-eqz p1, :cond_0

    .line 1409
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/h$a;->e()V

    .line 1410
    iget-object p1, p1, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    goto :goto_0

    .line 1412
    :cond_0
    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/h$c;)V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    .line 649
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->H:Lcom/google/android/exoplayer2/u;

    if-nez v0, :cond_0

    .line 650
    iget v0, p0, Lcom/google/android/exoplayer2/h;->B:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/exoplayer2/h;->B:I

    .line 651
    iput-object p1, p0, Lcom/google/android/exoplayer2/h;->C:Lcom/google/android/exoplayer2/h$c;

    .line 694
    :goto_0
    return-void

    .line 655
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/h;->b(Lcom/google/android/exoplayer2/h$c;)Landroid/util/Pair;

    move-result-object v1

    .line 656
    if-nez v1, :cond_1

    .line 659
    new-instance v0, Lcom/google/android/exoplayer2/h$b;

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/exoplayer2/h$b;-><init>(IJ)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    .line 660
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->h:Landroid/os/Handler;

    const/4 v1, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 663
    new-instance v0, Lcom/google/android/exoplayer2/h$b;

    const/4 v1, 0x0

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/exoplayer2/h$b;-><init>(IJ)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    .line 664
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/h;->b(I)V

    .line 666
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/h;->d(Z)V

    goto :goto_0

    .line 670
    :cond_1
    iget-wide v2, p1, Lcom/google/android/exoplayer2/h$c;->c:J

    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, v2, v4

    if-nez v0, :cond_2

    const/4 v0, 0x1

    move v2, v0

    .line 671
    :goto_1
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 672
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 674
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->l:Lcom/google/android/exoplayer2/m;

    .line 675
    invoke-virtual {v0, v3, v4, v5}, Lcom/google/android/exoplayer2/m;->a(IJ)Lcom/google/android/exoplayer2/source/i$b;

    move-result-object v1

    .line 676
    invoke-virtual {v1}, Lcom/google/android/exoplayer2/source/i$b;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 677
    const/4 v0, 0x1

    .line 678
    const-wide/16 v2, 0x0

    move v7, v0

    .line 681
    :goto_2
    :try_start_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$b;->a:Lcom/google/android/exoplayer2/source/i$b;

    invoke-virtual {v1, v0}, Lcom/google/android/exoplayer2/source/i$b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-wide/16 v8, 0x3e8

    div-long v8, v2, v8

    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    iget-wide v10, v0, Lcom/google/android/exoplayer2/h$b;->d:J

    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v0, v8, v10

    if-nez v0, :cond_4

    .line 690
    new-instance v0, Lcom/google/android/exoplayer2/h$b;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/h$b;-><init>(Lcom/google/android/exoplayer2/source/i$b;JJ)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    .line 691
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->h:Landroid/os/Handler;

    const/4 v2, 0x4

    if-eqz v7, :cond_3

    const/4 v0, 0x1

    :goto_3
    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 692
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    .line 670
    :cond_2
    const/4 v0, 0x0

    move v2, v0

    goto :goto_1

    .line 691
    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    .line 686
    :cond_4
    :try_start_1
    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/exoplayer2/h;->a(Lcom/google/android/exoplayer2/source/i$b;J)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v8

    .line 687
    cmp-long v0, v2, v8

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_4
    or-int v6, v7, v0

    .line 690
    new-instance v0, Lcom/google/android/exoplayer2/h$b;

    move-wide v2, v8

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/h$b;-><init>(Lcom/google/android/exoplayer2/source/i$b;JJ)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    .line 691
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->h:Landroid/os/Handler;

    const/4 v2, 0x4

    if-eqz v6, :cond_6

    const/4 v0, 0x1

    :goto_5
    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 692
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    .line 687
    :cond_5
    const/4 v0, 0x0

    goto :goto_4

    .line 691
    :cond_6
    const/4 v0, 0x0

    goto :goto_5

    .line 690
    :catchall_0
    move-exception v0

    move-object v6, v0

    new-instance v0, Lcom/google/android/exoplayer2/h$b;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/h$b;-><init>(Lcom/google/android/exoplayer2/source/i$b;JJ)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    .line 691
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->h:Landroid/os/Handler;

    const/4 v2, 0x4

    if-eqz v7, :cond_7

    const/4 v0, 0x1

    :goto_6
    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 692
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    throw v6

    .line 691
    :cond_7
    const/4 v0, 0x0

    goto :goto_6

    :cond_8
    move v7, v2

    move-wide v2, v4

    goto :goto_2
.end method

.method private a(Lcom/google/android/exoplayer2/n;)V
    .locals 3

    .prologue
    .line 780
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->p:Lcom/google/android/exoplayer2/util/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->p:Lcom/google/android/exoplayer2/util/g;

    .line 781
    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/util/g;->a(Lcom/google/android/exoplayer2/n;)Lcom/google/android/exoplayer2/n;

    move-result-object v0

    .line 783
    :goto_0
    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->n:Lcom/google/android/exoplayer2/n;

    .line 784
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->h:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 785
    return-void

    .line 781
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->e:Lcom/google/android/exoplayer2/util/o;

    .line 782
    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/util/o;->a(Lcom/google/android/exoplayer2/n;)Lcom/google/android/exoplayer2/n;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/google/android/exoplayer2/p;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    .line 854
    invoke-interface {p1}, Lcom/google/android/exoplayer2/p;->d()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 855
    invoke-interface {p1}, Lcom/google/android/exoplayer2/p;->k()V

    .line 857
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1136
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/exoplayer2/h;->a(Ljava/lang/Object;I)V

    .line 1137
    return-void
.end method

.method private a(Ljava/lang/Object;I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1142
    new-instance v0, Lcom/google/android/exoplayer2/h$b;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/exoplayer2/h$b;-><init>(IJ)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    .line 1143
    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/h;->b(Ljava/lang/Object;I)V

    .line 1145
    new-instance v0, Lcom/google/android/exoplayer2/h$b;

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/exoplayer2/h$b;-><init>(IJ)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    .line 1146
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/h;->b(I)V

    .line 1148
    invoke-direct {p0, v1}, Lcom/google/android/exoplayer2/h;->d(Z)V

    .line 1149
    return-void
.end method

.method private a([ZI)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    .line 1452
    new-array v0, p2, [Lcom/google/android/exoplayer2/p;

    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->r:[Lcom/google/android/exoplayer2/p;

    .line 1453
    const/4 v1, 0x0

    .line 1454
    const/4 v0, 0x0

    move v9, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->a:[Lcom/google/android/exoplayer2/p;

    array-length v0, v0

    if-ge v9, v0, :cond_7

    .line 1455
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->a:[Lcom/google/android/exoplayer2/p;

    aget-object v0, v0, v9

    .line 1456
    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget-object v2, v2, Lcom/google/android/exoplayer2/h$a;->k:Lcom/google/android/exoplayer2/b/h;

    iget-object v2, v2, Lcom/google/android/exoplayer2/b/h;->b:Lcom/google/android/exoplayer2/b/f;

    invoke-virtual {v2, v9}, Lcom/google/android/exoplayer2/b/f;->a(I)Lcom/google/android/exoplayer2/b/e;

    move-result-object v4

    .line 1457
    if-eqz v4, :cond_6

    .line 1458
    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->r:[Lcom/google/android/exoplayer2/p;

    add-int/lit8 v11, v1, 0x1

    aput-object v0, v2, v1

    .line 1459
    invoke-interface {v0}, Lcom/google/android/exoplayer2/p;->d()I

    move-result v1

    if-nez v1, :cond_5

    .line 1460
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget-object v1, v1, Lcom/google/android/exoplayer2/h$a;->k:Lcom/google/android/exoplayer2/b/h;

    iget-object v1, v1, Lcom/google/android/exoplayer2/b/h;->d:[Lcom/google/android/exoplayer2/r;

    aget-object v1, v1, v9

    .line 1463
    iget-boolean v2, p0, Lcom/google/android/exoplayer2/h;->t:Z

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/android/exoplayer2/h;->w:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    move v10, v2

    .line 1465
    :goto_1
    aget-boolean v2, p1, v9

    if-nez v2, :cond_1

    if-eqz v10, :cond_1

    const/4 v6, 0x1

    .line 1467
    :goto_2
    invoke-interface {v4}, Lcom/google/android/exoplayer2/b/e;->b()I

    move-result v2

    new-array v2, v2, [Lcom/google/android/exoplayer2/j;

    .line 1468
    const/4 v3, 0x0

    :goto_3
    array-length v5, v2

    if-ge v3, v5, :cond_2

    .line 1469
    invoke-interface {v4, v3}, Lcom/google/android/exoplayer2/b/e;->a(I)Lcom/google/android/exoplayer2/j;

    move-result-object v5

    aput-object v5, v2, v3

    .line 1468
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 1463
    :cond_0
    const/4 v2, 0x0

    move v10, v2

    goto :goto_1

    .line 1465
    :cond_1
    const/4 v6, 0x0

    goto :goto_2

    .line 1472
    :cond_2
    iget-object v3, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget-object v3, v3, Lcom/google/android/exoplayer2/h$a;->d:[Lcom/google/android/exoplayer2/source/m;

    aget-object v3, v3, v9

    iget-wide v4, p0, Lcom/google/android/exoplayer2/h;->D:J

    iget-object v7, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    .line 1473
    invoke-virtual {v7}, Lcom/google/android/exoplayer2/h$a;->a()J

    move-result-wide v7

    .line 1472
    invoke-interface/range {v0 .. v8}, Lcom/google/android/exoplayer2/p;->a(Lcom/google/android/exoplayer2/r;[Lcom/google/android/exoplayer2/j;Lcom/google/android/exoplayer2/source/m;JZJ)V

    .line 1474
    invoke-interface {v0}, Lcom/google/android/exoplayer2/p;->c()Lcom/google/android/exoplayer2/util/g;

    move-result-object v1

    .line 1475
    if-eqz v1, :cond_4

    .line 1476
    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->p:Lcom/google/android/exoplayer2/util/g;

    if-eqz v2, :cond_3

    .line 1477
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Multiple renderer media clocks enabled."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/exoplayer2/ExoPlaybackException;->a(Ljava/lang/RuntimeException;)Lcom/google/android/exoplayer2/ExoPlaybackException;

    move-result-object v0

    throw v0

    .line 1480
    :cond_3
    iput-object v1, p0, Lcom/google/android/exoplayer2/h;->p:Lcom/google/android/exoplayer2/util/g;

    .line 1481
    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->o:Lcom/google/android/exoplayer2/p;

    .line 1482
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->p:Lcom/google/android/exoplayer2/util/g;

    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->n:Lcom/google/android/exoplayer2/n;

    invoke-interface {v1, v2}, Lcom/google/android/exoplayer2/util/g;->a(Lcom/google/android/exoplayer2/n;)Lcom/google/android/exoplayer2/n;

    .line 1485
    :cond_4
    if-eqz v10, :cond_5

    .line 1486
    invoke-interface {v0}, Lcom/google/android/exoplayer2/p;->e()V

    :cond_5
    move v1, v11

    .line 1454
    :cond_6
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto/16 :goto_0

    .line 1491
    :cond_7
    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/source/i$b;JLcom/google/android/exoplayer2/h$a;)Z
    .locals 4

    .prologue
    .line 758
    iget-object v0, p4, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/m$a;->a:Lcom/google/android/exoplayer2/source/i$b;

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/source/i$b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p4, Lcom/google/android/exoplayer2/h$a;->h:Z

    if-eqz v0, :cond_1

    .line 759
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->H:Lcom/google/android/exoplayer2/u;

    iget-object v1, p4, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-object v1, v1, Lcom/google/android/exoplayer2/m$a;->a:Lcom/google/android/exoplayer2/source/i$b;

    iget v1, v1, Lcom/google/android/exoplayer2/source/i$b;->b:I

    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->k:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;)Lcom/google/android/exoplayer2/u$a;

    .line 760
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->k:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/exoplayer2/u$a;->b(J)I

    move-result v0

    .line 761
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->k:Lcom/google/android/exoplayer2/u$a;

    .line 762
    invoke-virtual {v1, v0}, Lcom/google/android/exoplayer2/u$a;->a(I)J

    move-result-wide v0

    iget-object v2, p4, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-wide v2, v2, Lcom/google/android/exoplayer2/m$a;->c:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 763
    :cond_0
    const/4 v0, 0x1

    .line 766
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/google/android/exoplayer2/h$c;)Landroid/util/Pair;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/h$c;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, -0x1

    .line 1196
    iget-object v0, p1, Lcom/google/android/exoplayer2/h$c;->a:Lcom/google/android/exoplayer2/u;

    .line 1197
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/u;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1200
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->H:Lcom/google/android/exoplayer2/u;

    .line 1205
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->j:Lcom/google/android/exoplayer2/u$b;

    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->k:Lcom/google/android/exoplayer2/u$a;

    iget v3, p1, Lcom/google/android/exoplayer2/h$c;->b:I

    iget-wide v4, p1, Lcom/google/android/exoplayer2/h$c;->c:J

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/exoplayer2/u;->a(Lcom/google/android/exoplayer2/u$b;Lcom/google/android/exoplayer2/u$a;IJ)Landroid/util/Pair;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1212
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->H:Lcom/google/android/exoplayer2/u;

    if-ne v1, v0, :cond_1

    move-object v0, v2

    .line 1230
    :goto_0
    return-object v0

    .line 1207
    :catch_0
    move-exception v0

    .line 1209
    new-instance v0, Lcom/google/android/exoplayer2/IllegalSeekPositionException;

    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->H:Lcom/google/android/exoplayer2/u;

    iget v2, p1, Lcom/google/android/exoplayer2/h$c;->b:I

    iget-wide v4, p1, Lcom/google/android/exoplayer2/h$c;->c:J

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/google/android/exoplayer2/IllegalSeekPositionException;-><init>(Lcom/google/android/exoplayer2/u;IJ)V

    throw v0

    .line 1217
    :cond_1
    iget-object v3, p0, Lcom/google/android/exoplayer2/h;->H:Lcom/google/android/exoplayer2/u;

    iget-object v1, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    .line 1218
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v4, p0, Lcom/google/android/exoplayer2/h;->k:Lcom/google/android/exoplayer2/u$a;

    const/4 v5, 0x1

    invoke-virtual {v0, v1, v4, v5}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;Z)Lcom/google/android/exoplayer2/u$a;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/exoplayer2/u$a;->b:Ljava/lang/Object;

    .line 1217
    invoke-virtual {v3, v1}, Lcom/google/android/exoplayer2/u;->a(Ljava/lang/Object;)I

    move-result v1

    .line 1219
    if-eq v1, v6, :cond_2

    .line 1221
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0

    .line 1224
    :cond_2
    iget-object v1, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->H:Lcom/google/android/exoplayer2/u;

    invoke-direct {p0, v1, v0, v2}, Lcom/google/android/exoplayer2/h;->a(ILcom/google/android/exoplayer2/u;Lcom/google/android/exoplayer2/u;)I

    move-result v0

    .line 1225
    if-eq v0, v6, :cond_3

    .line 1227
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->H:Lcom/google/android/exoplayer2/u;

    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->k:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;)Lcom/google/android/exoplayer2/u$a;

    move-result-object v0

    iget v0, v0, Lcom/google/android/exoplayer2/u$a;->c:I

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/exoplayer2/h;->a(IJ)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0

    .line 1230
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 501
    iput-boolean v0, p0, Lcom/google/android/exoplayer2/h;->u:Z

    .line 502
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->e:Lcom/google/android/exoplayer2/util/o;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/o;->a()V

    .line 503
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->r:[Lcom/google/android/exoplayer2/p;

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 504
    invoke-interface {v3}, Lcom/google/android/exoplayer2/p;->e()V

    .line 503
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 506
    :cond_0
    return-void
.end method

.method private b(I)V
    .locals 3

    .prologue
    .line 404
    iget v0, p0, Lcom/google/android/exoplayer2/h;->w:I

    if-eq v0, p1, :cond_0

    .line 405
    iput p1, p0, Lcom/google/android/exoplayer2/h;->w:I

    .line 406
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->h:Landroid/os/Handler;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 408
    :cond_0
    return-void
.end method

.method private b(Lcom/google/android/exoplayer2/h$a;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x0

    .line 1415
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    if-ne v0, p1, :cond_0

    .line 1448
    :goto_0
    return-void

    .line 1420
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->a:[Lcom/google/android/exoplayer2/p;

    array-length v0, v0

    new-array v4, v0, [Z

    move v0, v1

    move v2, v1

    .line 1421
    :goto_1
    iget-object v3, p0, Lcom/google/android/exoplayer2/h;->a:[Lcom/google/android/exoplayer2/p;

    array-length v3, v3

    if-ge v0, v3, :cond_6

    .line 1422
    iget-object v3, p0, Lcom/google/android/exoplayer2/h;->a:[Lcom/google/android/exoplayer2/p;

    aget-object v5, v3, v0

    .line 1423
    invoke-interface {v5}, Lcom/google/android/exoplayer2/p;->d()I

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x1

    :goto_2
    aput-boolean v3, v4, v0

    .line 1424
    iget-object v3, p1, Lcom/google/android/exoplayer2/h$a;->k:Lcom/google/android/exoplayer2/b/h;

    iget-object v3, v3, Lcom/google/android/exoplayer2/b/h;->b:Lcom/google/android/exoplayer2/b/f;

    invoke-virtual {v3, v0}, Lcom/google/android/exoplayer2/b/f;->a(I)Lcom/google/android/exoplayer2/b/e;

    move-result-object v3

    .line 1425
    if-eqz v3, :cond_1

    .line 1426
    add-int/lit8 v2, v2, 0x1

    .line 1428
    :cond_1
    aget-boolean v6, v4, v0

    if-eqz v6, :cond_4

    if-eqz v3, :cond_2

    .line 1429
    invoke-interface {v5}, Lcom/google/android/exoplayer2/p;->i()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1430
    invoke-interface {v5}, Lcom/google/android/exoplayer2/p;->f()Lcom/google/android/exoplayer2/source/m;

    move-result-object v3

    iget-object v6, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget-object v6, v6, Lcom/google/android/exoplayer2/h$a;->d:[Lcom/google/android/exoplayer2/source/m;

    aget-object v6, v6, v0

    if-ne v3, v6, :cond_4

    .line 1434
    :cond_2
    iget-object v3, p0, Lcom/google/android/exoplayer2/h;->o:Lcom/google/android/exoplayer2/p;

    if-ne v5, v3, :cond_3

    .line 1436
    iget-object v3, p0, Lcom/google/android/exoplayer2/h;->e:Lcom/google/android/exoplayer2/util/o;

    iget-object v6, p0, Lcom/google/android/exoplayer2/h;->p:Lcom/google/android/exoplayer2/util/g;

    invoke-virtual {v3, v6}, Lcom/google/android/exoplayer2/util/o;->a(Lcom/google/android/exoplayer2/util/g;)V

    .line 1437
    iput-object v7, p0, Lcom/google/android/exoplayer2/h;->p:Lcom/google/android/exoplayer2/util/g;

    .line 1438
    iput-object v7, p0, Lcom/google/android/exoplayer2/h;->o:Lcom/google/android/exoplayer2/p;

    .line 1440
    :cond_3
    invoke-direct {p0, v5}, Lcom/google/android/exoplayer2/h;->a(Lcom/google/android/exoplayer2/p;)V

    .line 1441
    invoke-interface {v5}, Lcom/google/android/exoplayer2/p;->l()V

    .line 1421
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    move v3, v1

    .line 1423
    goto :goto_2

    .line 1445
    :cond_6
    iput-object p1, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    .line 1446
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->h:Landroid/os/Handler;

    const/4 v1, 0x3

    iget-object v3, p1, Lcom/google/android/exoplayer2/h$a;->k:Lcom/google/android/exoplayer2/b/h;

    invoke-virtual {v0, v1, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1447
    invoke-direct {p0, v4, v2}, Lcom/google/android/exoplayer2/h;->a([ZI)V

    goto :goto_0
.end method

.method private b(Lcom/google/android/exoplayer2/source/i;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 418
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->h:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 419
    invoke-direct {p0, v4}, Lcom/google/android/exoplayer2/h;->d(Z)V

    .line 420
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->d:Lcom/google/android/exoplayer2/l;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/l;->a()V

    .line 421
    if-eqz p2, :cond_0

    .line 422
    new-instance v0, Lcom/google/android/exoplayer2/h$b;

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/exoplayer2/h$b;-><init>(IJ)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    .line 424
    :cond_0
    iput-object p1, p0, Lcom/google/android/exoplayer2/h;->q:Lcom/google/android/exoplayer2/source/i;

    .line 425
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->i:Lcom/google/android/exoplayer2/e;

    invoke-interface {p1, v0, v4, p0}, Lcom/google/android/exoplayer2/source/i;->a(Lcom/google/android/exoplayer2/e;ZLcom/google/android/exoplayer2/source/i$a;)V

    .line 426
    invoke-direct {p0, v5}, Lcom/google/android/exoplayer2/h;->b(I)V

    .line 427
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->f:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 428
    return-void
.end method

.method private b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1152
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/exoplayer2/h;->b(Ljava/lang/Object;I)V

    .line 1153
    return-void
.end method

.method private b(Ljava/lang/Object;I)V
    .locals 5

    .prologue
    .line 1156
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->h:Landroid/os/Handler;

    const/4 v1, 0x6

    new-instance v2, Lcom/google/android/exoplayer2/h$d;

    iget-object v3, p0, Lcom/google/android/exoplayer2/h;->H:Lcom/google/android/exoplayer2/u;

    iget-object v4, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    invoke-direct {v2, v3, p1, v4, p2}, Lcom/google/android/exoplayer2/h$d;-><init>(Lcom/google/android/exoplayer2/u;Ljava/lang/Object;Lcom/google/android/exoplayer2/h$b;I)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1157
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1158
    return-void
.end method

.method private b(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 411
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/h;->v:Z

    if-eq v0, p1, :cond_0

    .line 412
    iput-boolean p1, p0, Lcom/google/android/exoplayer2/h;->v:Z

    .line 413
    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->h:Landroid/os/Handler;

    const/4 v3, 0x2

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 415
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 413
    goto :goto_0
.end method

.method private b(J)Z
    .locals 3

    .prologue
    .line 953
    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/h$b;->d:J

    cmp-long v0, v0, p1

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/h$a;->h:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/m$a;->a:Lcom/google/android/exoplayer2/source/i$b;

    .line 956
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/i$b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    .line 509
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->e:Lcom/google/android/exoplayer2/util/o;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/o;->b()V

    .line 510
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->r:[Lcom/google/android/exoplayer2/p;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 511
    invoke-direct {p0, v3}, Lcom/google/android/exoplayer2/h;->a(Lcom/google/android/exoplayer2/p;)V

    .line 510
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 513
    :cond_0
    return-void
.end method

.method private c(I)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v2, -0x1

    .line 448
    iput p1, p0, Lcom/google/android/exoplayer2/h;->x:I

    .line 449
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->l:Lcom/google/android/exoplayer2/m;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/m;->a(I)V

    .line 452
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    .line 454
    :goto_0
    if-nez v0, :cond_3

    .line 498
    :cond_0
    :goto_1
    return-void

    .line 452
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    goto :goto_0

    .line 468
    :cond_2
    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    .line 458
    :cond_3
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->H:Lcom/google/android/exoplayer2/u;

    iget-object v5, v0, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-object v5, v5, Lcom/google/android/exoplayer2/m$a;->a:Lcom/google/android/exoplayer2/source/i$b;

    iget v5, v5, Lcom/google/android/exoplayer2/source/i$b;->b:I

    iget-object v6, p0, Lcom/google/android/exoplayer2/h;->k:Lcom/google/android/exoplayer2/u$a;

    iget-object v7, p0, Lcom/google/android/exoplayer2/h;->j:Lcom/google/android/exoplayer2/u$b;

    invoke-virtual {v1, v5, v6, v7, p1}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;Lcom/google/android/exoplayer2/u$b;I)I

    move-result v1

    .line 460
    :goto_2
    iget-object v5, v0, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    if-eqz v5, :cond_4

    iget-object v5, v0, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-boolean v5, v5, Lcom/google/android/exoplayer2/m$a;->f:Z

    if-nez v5, :cond_4

    .line 462
    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    goto :goto_2

    .line 464
    :cond_4
    if-eq v1, v2, :cond_5

    iget-object v5, v0, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    if-eqz v5, :cond_5

    iget-object v5, v0, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    iget-object v5, v5, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-object v5, v5, Lcom/google/android/exoplayer2/m$a;->a:Lcom/google/android/exoplayer2/source/i$b;

    iget v5, v5, Lcom/google/android/exoplayer2/source/i$b;->b:I

    if-eq v5, v1, :cond_2

    .line 472
    :cond_5
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    iget v5, v1, Lcom/google/android/exoplayer2/h$a;->c:I

    .line 473
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->F:Lcom/google/android/exoplayer2/h$a;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->F:Lcom/google/android/exoplayer2/h$a;

    iget v1, v1, Lcom/google/android/exoplayer2/h$a;->c:I

    .line 475
    :goto_3
    iget-object v6, v0, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    if-eqz v6, :cond_6

    .line 476
    iget-object v6, v0, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    invoke-direct {p0, v6}, Lcom/google/android/exoplayer2/h;->a(Lcom/google/android/exoplayer2/h$a;)V

    .line 477
    const/4 v6, 0x0

    iput-object v6, v0, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    .line 481
    :cond_6
    iget-object v6, p0, Lcom/google/android/exoplayer2/h;->l:Lcom/google/android/exoplayer2/m;

    iget-object v7, v0, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    .line 482
    invoke-virtual {v6, v7}, Lcom/google/android/exoplayer2/m;->a(Lcom/google/android/exoplayer2/m$a;)Lcom/google/android/exoplayer2/m$a;

    move-result-object v6

    iput-object v6, v0, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    .line 485
    iget v6, v0, Lcom/google/android/exoplayer2/h$a;->c:I

    if-gt v5, v6, :cond_9

    move v5, v3

    .line 486
    :goto_4
    if-nez v5, :cond_7

    .line 487
    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    .line 489
    :cond_7
    if-eq v1, v2, :cond_a

    iget v0, v0, Lcom/google/android/exoplayer2/h$a;->c:I

    if-gt v1, v0, :cond_a

    move v0, v3

    .line 491
    :goto_5
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    if-eqz v0, :cond_0

    .line 494
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-object v1, v0, Lcom/google/android/exoplayer2/m$a;->a:Lcom/google/android/exoplayer2/source/i$b;

    .line 495
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    iget-wide v2, v0, Lcom/google/android/exoplayer2/h$b;->d:J

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/exoplayer2/h;->a(Lcom/google/android/exoplayer2/source/i$b;J)J

    move-result-wide v2

    .line 496
    new-instance v0, Lcom/google/android/exoplayer2/h$b;

    iget-object v4, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    iget-wide v4, v4, Lcom/google/android/exoplayer2/h$b;->c:J

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/h$b;-><init>(Lcom/google/android/exoplayer2/source/i$b;JJ)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    goto/16 :goto_1

    :cond_8
    move v1, v2

    .line 473
    goto :goto_3

    :cond_9
    move v5, v4

    .line 485
    goto :goto_4

    :cond_a
    move v0, v4

    .line 489
    goto :goto_5
.end method

.method private c(Lcom/google/android/exoplayer2/source/h;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    .line 1377
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->a:Lcom/google/android/exoplayer2/source/h;

    if-eq v0, p1, :cond_1

    .line 1389
    :cond_0
    :goto_0
    return-void

    .line 1381
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/h$a;->c()V

    .line 1382
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    if-nez v0, :cond_2

    .line 1384
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->F:Lcom/google/android/exoplayer2/h$a;

    .line 1385
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->F:Lcom/google/android/exoplayer2/h$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/m$a;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer2/h;->a(J)V

    .line 1386
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->F:Lcom/google/android/exoplayer2/h$a;

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/h;->b(Lcom/google/android/exoplayer2/h$a;)V

    .line 1388
    :cond_2
    invoke-direct {p0}, Lcom/google/android/exoplayer2/h;->l()V

    goto :goto_0
.end method

.method private c(Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 431
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/h;->u:Z

    .line 432
    iput-boolean p1, p0, Lcom/google/android/exoplayer2/h;->t:Z

    .line 433
    if-nez p1, :cond_1

    .line 434
    invoke-direct {p0}, Lcom/google/android/exoplayer2/h;->c()V

    .line 435
    invoke-direct {p0}, Lcom/google/android/exoplayer2/h;->d()V

    .line 444
    :cond_0
    :goto_0
    return-void

    .line 437
    :cond_1
    iget v0, p0, Lcom/google/android/exoplayer2/h;->w:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 438
    invoke-direct {p0}, Lcom/google/android/exoplayer2/h;->b()V

    .line 439
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->f:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 440
    :cond_2
    iget v0, p0, Lcom/google/android/exoplayer2/h;->w:I

    if-ne v0, v2, :cond_0

    .line 441
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->f:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method private c([Lcom/google/android/exoplayer2/e$c;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    .line 838
    :try_start_0
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 839
    iget-object v3, v2, Lcom/google/android/exoplayer2/e$c;->a:Lcom/google/android/exoplayer2/e$b;

    iget v4, v2, Lcom/google/android/exoplayer2/e$c;->b:I

    iget-object v2, v2, Lcom/google/android/exoplayer2/e$c;->c:Ljava/lang/Object;

    invoke-interface {v3, v4, v2}, Lcom/google/android/exoplayer2/e$b;->a(ILjava/lang/Object;)V

    .line 838
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 841
    :cond_0
    iget v0, p0, Lcom/google/android/exoplayer2/h;->w:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/exoplayer2/h;->w:I

    if-ne v0, v5, :cond_2

    .line 843
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->f:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 846
    :cond_2
    monitor-enter p0

    .line 847
    :try_start_1
    iget v0, p0, Lcom/google/android/exoplayer2/h;->z:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/exoplayer2/h;->z:I

    .line 848
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 849
    monitor-exit p0

    .line 851
    return-void

    .line 849
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 846
    :catchall_1
    move-exception v0

    monitor-enter p0

    .line 847
    :try_start_2
    iget v1, p0, Lcom/google/android/exoplayer2/h;->z:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/exoplayer2/h;->z:I

    .line 848
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 849
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    throw v0

    :catchall_2
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw v0
.end method

.method private d()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    const-wide/high16 v2, -0x8000000000000000L

    .line 516
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    if-nez v0, :cond_0

    .line 541
    :goto_0
    return-void

    .line 521
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->a:Lcom/google/android/exoplayer2/source/h;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/source/h;->c()J

    move-result-wide v0

    .line 522
    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v4, v0, v4

    if-eqz v4, :cond_2

    .line 523
    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer2/h;->a(J)V

    .line 533
    :goto_1
    iget-object v4, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    iput-wide v0, v4, Lcom/google/android/exoplayer2/h$b;->d:J

    .line 534
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-wide/16 v4, 0x3e8

    mul-long/2addr v0, v4

    iput-wide v0, p0, Lcom/google/android/exoplayer2/h;->A:J

    .line 537
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->r:[Lcom/google/android/exoplayer2/p;

    array-length v0, v0

    if-nez v0, :cond_4

    move-wide v0, v2

    .line 539
    :goto_2
    iget-object v4, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/m$a;->e:J

    :cond_1
    iput-wide v0, v4, Lcom/google/android/exoplayer2/h$b;->e:J

    goto :goto_0

    .line 525
    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->o:Lcom/google/android/exoplayer2/p;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->o:Lcom/google/android/exoplayer2/p;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/p;->u()Z

    move-result v0

    if-nez v0, :cond_3

    .line 526
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->p:Lcom/google/android/exoplayer2/util/g;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/util/g;->w()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/h;->D:J

    .line 527
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->e:Lcom/google/android/exoplayer2/util/o;

    iget-wide v4, p0, Lcom/google/android/exoplayer2/h;->D:J

    invoke-virtual {v0, v4, v5}, Lcom/google/android/exoplayer2/util/o;->a(J)V

    .line 531
    :goto_3
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget-wide v4, p0, Lcom/google/android/exoplayer2/h;->D:J

    invoke-virtual {v0, v4, v5}, Lcom/google/android/exoplayer2/h$a;->b(J)J

    move-result-wide v0

    goto :goto_1

    .line 529
    :cond_3
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->e:Lcom/google/android/exoplayer2/util/o;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/o;->w()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/h;->D:J

    goto :goto_3

    .line 537
    :cond_4
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->a:Lcom/google/android/exoplayer2/source/h;

    .line 538
    invoke-interface {v0}, Lcom/google/android/exoplayer2/source/h;->d()J

    move-result-wide v0

    goto :goto_2
.end method

.method private d(Lcom/google/android/exoplayer2/source/h;)V
    .locals 1

    .prologue
    .line 1392
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->a:Lcom/google/android/exoplayer2/source/h;

    if-eq v0, p1, :cond_1

    .line 1397
    :cond_0
    :goto_0
    return-void

    .line 1396
    :cond_1
    invoke-direct {p0}, Lcom/google/android/exoplayer2/h;->l()V

    goto :goto_0
.end method

.method private d(Z)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 804
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->f:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 805
    iput-boolean v2, p0, Lcom/google/android/exoplayer2/h;->u:Z

    .line 806
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->e:Lcom/google/android/exoplayer2/util/o;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/o;->b()V

    .line 807
    iput-object v7, p0, Lcom/google/android/exoplayer2/h;->p:Lcom/google/android/exoplayer2/util/g;

    .line 808
    iput-object v7, p0, Lcom/google/android/exoplayer2/h;->o:Lcom/google/android/exoplayer2/p;

    .line 809
    const-wide/32 v0, 0x3938700

    iput-wide v0, p0, Lcom/google/android/exoplayer2/h;->D:J

    .line 810
    iget-object v3, p0, Lcom/google/android/exoplayer2/h;->r:[Lcom/google/android/exoplayer2/p;

    array-length v4, v3

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v0, v3, v1

    .line 812
    :try_start_0
    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/h;->a(Lcom/google/android/exoplayer2/p;)V

    .line 813
    invoke-interface {v0}, Lcom/google/android/exoplayer2/p;->l()V
    :try_end_0
    .catch Lcom/google/android/exoplayer2/ExoPlaybackException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 810
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 814
    :catch_0
    move-exception v0

    .line 816
    :goto_2
    const-string v5, "ExoPlayerImplInternal"

    const-string v6, "Stop failed."

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 819
    :cond_0
    new-array v0, v2, [Lcom/google/android/exoplayer2/p;

    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->r:[Lcom/google/android/exoplayer2/p;

    .line 820
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    :goto_3
    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/h;->a(Lcom/google/android/exoplayer2/h$a;)V

    .line 822
    iput-object v7, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    .line 823
    iput-object v7, p0, Lcom/google/android/exoplayer2/h;->F:Lcom/google/android/exoplayer2/h$a;

    .line 824
    iput-object v7, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    .line 825
    invoke-direct {p0, v2}, Lcom/google/android/exoplayer2/h;->b(Z)V

    .line 826
    if-eqz p1, :cond_2

    .line 827
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->q:Lcom/google/android/exoplayer2/source/i;

    if-eqz v0, :cond_1

    .line 828
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->q:Lcom/google/android/exoplayer2/source/i;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/source/i;->b()V

    .line 829
    iput-object v7, p0, Lcom/google/android/exoplayer2/h;->q:Lcom/google/android/exoplayer2/source/i;

    .line 831
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->l:Lcom/google/android/exoplayer2/m;

    invoke-virtual {v0, v7}, Lcom/google/android/exoplayer2/m;->a(Lcom/google/android/exoplayer2/u;)V

    .line 832
    iput-object v7, p0, Lcom/google/android/exoplayer2/h;->H:Lcom/google/android/exoplayer2/u;

    .line 834
    :cond_2
    return-void

    .line 820
    :cond_3
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    goto :goto_3

    .line 814
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method private e()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 544
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 545
    invoke-direct {p0}, Lcom/google/android/exoplayer2/h;->j()V

    .line 546
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    if-nez v0, :cond_0

    .line 548
    invoke-direct {p0}, Lcom/google/android/exoplayer2/h;->i()V

    .line 549
    const-wide/16 v0, 0xa

    invoke-direct {p0, v4, v5, v0, v1}, Lcom/google/android/exoplayer2/h;->a(JJ)V

    .line 635
    :goto_0
    return-void

    .line 553
    :cond_0
    const-string v0, "doSomeWork"

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/q;->a(Ljava/lang/String;)V

    .line 555
    invoke-direct {p0}, Lcom/google/android/exoplayer2/h;->d()V

    .line 556
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->a:Lcom/google/android/exoplayer2/source/h;

    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    iget-wide v2, v1, Lcom/google/android/exoplayer2/h$b;->d:J

    invoke-interface {v0, v2, v3}, Lcom/google/android/exoplayer2/source/h;->a(J)V

    .line 558
    const/4 v2, 0x1

    .line 559
    const/4 v1, 0x1

    .line 560
    iget-object v6, p0, Lcom/google/android/exoplayer2/h;->r:[Lcom/google/android/exoplayer2/p;

    array-length v7, v6

    const/4 v0, 0x0

    move v3, v0

    move v0, v2

    :goto_1
    if-ge v3, v7, :cond_6

    aget-object v8, v6, v3

    .line 564
    iget-wide v10, p0, Lcom/google/android/exoplayer2/h;->D:J

    iget-wide v12, p0, Lcom/google/android/exoplayer2/h;->A:J

    invoke-interface {v8, v10, v11, v12, v13}, Lcom/google/android/exoplayer2/p;->a(JJ)V

    .line 565
    if-eqz v0, :cond_3

    invoke-interface {v8}, Lcom/google/android/exoplayer2/p;->u()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    .line 568
    :goto_2
    invoke-interface {v8}, Lcom/google/android/exoplayer2/p;->t()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-interface {v8}, Lcom/google/android/exoplayer2/p;->u()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_1
    const/4 v2, 0x1

    .line 569
    :goto_3
    if-nez v2, :cond_2

    .line 570
    invoke-interface {v8}, Lcom/google/android/exoplayer2/p;->j()V

    .line 572
    :cond_2
    if-eqz v1, :cond_5

    if-eqz v2, :cond_5

    const/4 v1, 0x1

    .line 560
    :goto_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 565
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 568
    :cond_4
    const/4 v2, 0x0

    goto :goto_3

    .line 572
    :cond_5
    const/4 v1, 0x0

    goto :goto_4

    .line 575
    :cond_6
    if-nez v1, :cond_7

    .line 576
    invoke-direct {p0}, Lcom/google/android/exoplayer2/h;->i()V

    .line 580
    :cond_7
    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->p:Lcom/google/android/exoplayer2/util/g;

    if-eqz v2, :cond_8

    .line 581
    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->p:Lcom/google/android/exoplayer2/util/g;

    invoke-interface {v2}, Lcom/google/android/exoplayer2/util/g;->x()Lcom/google/android/exoplayer2/n;

    move-result-object v2

    .line 582
    iget-object v3, p0, Lcom/google/android/exoplayer2/h;->n:Lcom/google/android/exoplayer2/n;

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer2/n;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 585
    iput-object v2, p0, Lcom/google/android/exoplayer2/h;->n:Lcom/google/android/exoplayer2/n;

    .line 586
    iget-object v3, p0, Lcom/google/android/exoplayer2/h;->e:Lcom/google/android/exoplayer2/util/o;

    iget-object v6, p0, Lcom/google/android/exoplayer2/h;->p:Lcom/google/android/exoplayer2/util/g;

    invoke-virtual {v3, v6}, Lcom/google/android/exoplayer2/util/o;->a(Lcom/google/android/exoplayer2/util/g;)V

    .line 587
    iget-object v3, p0, Lcom/google/android/exoplayer2/h;->h:Landroid/os/Handler;

    const/4 v6, 0x7

    invoke-virtual {v3, v6, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 588
    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    .line 592
    :cond_8
    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget-object v2, v2, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-wide v2, v2, Lcom/google/android/exoplayer2/m$a;->e:J

    .line 593
    if-eqz v0, :cond_b

    const-wide v6, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, v2, v6

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    iget-wide v6, v0, Lcom/google/android/exoplayer2/h$b;->d:J

    cmp-long v0, v2, v6

    if-gtz v0, :cond_b

    :cond_9
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/m$a;->g:Z

    if-eqz v0, :cond_b

    .line 597
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/h;->b(I)V

    .line 598
    invoke-direct {p0}, Lcom/google/android/exoplayer2/h;->c()V

    .line 620
    :cond_a
    :goto_5
    iget v0, p0, Lcom/google/android/exoplayer2/h;->w:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_10

    .line 621
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->r:[Lcom/google/android/exoplayer2/p;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_6
    if-ge v0, v2, :cond_10

    aget-object v3, v1, v0

    .line 622
    invoke-interface {v3}, Lcom/google/android/exoplayer2/p;->j()V

    .line 621
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 599
    :cond_b
    iget v0, p0, Lcom/google/android/exoplayer2/h;->w:I

    const/4 v6, 0x2

    if-ne v0, v6, :cond_e

    .line 600
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->r:[Lcom/google/android/exoplayer2/p;

    array-length v0, v0

    if-lez v0, :cond_d

    if-eqz v1, :cond_c

    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    iget-boolean v1, p0, Lcom/google/android/exoplayer2/h;->u:Z

    iget-wide v2, p0, Lcom/google/android/exoplayer2/h;->D:J

    .line 602
    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/exoplayer2/h$a;->a(ZJ)Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    .line 604
    :goto_7
    if-eqz v0, :cond_a

    .line 605
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/h;->b(I)V

    .line 606
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/h;->t:Z

    if-eqz v0, :cond_a

    .line 607
    invoke-direct {p0}, Lcom/google/android/exoplayer2/h;->b()V

    goto :goto_5

    .line 602
    :cond_c
    const/4 v0, 0x0

    goto :goto_7

    .line 603
    :cond_d
    invoke-direct {p0, v2, v3}, Lcom/google/android/exoplayer2/h;->b(J)Z

    move-result v0

    goto :goto_7

    .line 610
    :cond_e
    iget v0, p0, Lcom/google/android/exoplayer2/h;->w:I

    const/4 v6, 0x3

    if-ne v0, v6, :cond_a

    .line 611
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->r:[Lcom/google/android/exoplayer2/p;

    array-length v0, v0

    if-lez v0, :cond_f

    move v0, v1

    .line 613
    :goto_8
    if-nez v0, :cond_a

    .line 614
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/h;->t:Z

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/h;->u:Z

    .line 615
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/h;->b(I)V

    .line 616
    invoke-direct {p0}, Lcom/google/android/exoplayer2/h;->c()V

    goto :goto_5

    .line 612
    :cond_f
    invoke-direct {p0, v2, v3}, Lcom/google/android/exoplayer2/h;->b(J)Z

    move-result v0

    goto :goto_8

    .line 626
    :cond_10
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/h;->t:Z

    if-eqz v0, :cond_11

    iget v0, p0, Lcom/google/android/exoplayer2/h;->w:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_12

    :cond_11
    iget v0, p0, Lcom/google/android/exoplayer2/h;->w:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_13

    .line 627
    :cond_12
    const-wide/16 v0, 0xa

    invoke-direct {p0, v4, v5, v0, v1}, Lcom/google/android/exoplayer2/h;->a(JJ)V

    .line 634
    :goto_9
    invoke-static {}, Lcom/google/android/exoplayer2/util/q;->a()V

    goto/16 :goto_0

    .line 628
    :cond_13
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->r:[Lcom/google/android/exoplayer2/p;

    array-length v0, v0

    if-eqz v0, :cond_14

    iget v0, p0, Lcom/google/android/exoplayer2/h;->w:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_14

    .line 629
    const-wide/16 v0, 0x3e8

    invoke-direct {p0, v4, v5, v0, v1}, Lcom/google/android/exoplayer2/h;->a(JJ)V

    goto :goto_9

    .line 631
    :cond_14
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->f:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_9
.end method

.method private f()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 788
    invoke-direct {p0, v1}, Lcom/google/android/exoplayer2/h;->d(Z)V

    .line 789
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->d:Lcom/google/android/exoplayer2/l;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/l;->b()V

    .line 790
    invoke-direct {p0, v1}, Lcom/google/android/exoplayer2/h;->b(I)V

    .line 791
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 794
    invoke-direct {p0, v1}, Lcom/google/android/exoplayer2/h;->d(Z)V

    .line 795
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->d:Lcom/google/android/exoplayer2/l;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/l;->c()V

    .line 796
    invoke-direct {p0, v1}, Lcom/google/android/exoplayer2/h;->b(I)V

    .line 797
    monitor-enter p0

    .line 798
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/exoplayer2/h;->s:Z

    .line 799
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 800
    monitor-exit p0

    .line 801
    return-void

    .line 800
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private h()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v12, 0x0

    const/4 v1, 0x0

    .line 860
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    if-nez v0, :cond_1

    .line 950
    :cond_0
    :goto_0
    return-void

    .line 865
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    move-object v5, v0

    move v0, v2

    .line 868
    :goto_1
    if-eqz v5, :cond_0

    iget-boolean v3, v5, Lcom/google/android/exoplayer2/h$a;->h:Z

    if-eqz v3, :cond_0

    .line 872
    invoke-virtual {v5}, Lcom/google/android/exoplayer2/h$a;->d()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 883
    if-eqz v0, :cond_e

    .line 885
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->F:Lcom/google/android/exoplayer2/h$a;

    iget-object v3, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    if-eq v0, v3, :cond_9

    move v0, v2

    .line 886
    :goto_2
    iget-object v3, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget-object v3, v3, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    invoke-direct {p0, v3}, Lcom/google/android/exoplayer2/h;->a(Lcom/google/android/exoplayer2/h$a;)V

    .line 887
    iget-object v3, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iput-object v12, v3, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    .line 888
    iget-object v3, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iput-object v3, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    .line 889
    iget-object v3, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iput-object v3, p0, Lcom/google/android/exoplayer2/h;->F:Lcom/google/android/exoplayer2/h$a;

    .line 891
    iget-object v3, p0, Lcom/google/android/exoplayer2/h;->a:[Lcom/google/android/exoplayer2/p;

    array-length v3, v3

    new-array v6, v3, [Z

    .line 892
    iget-object v3, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget-object v4, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    iget-wide v8, v4, Lcom/google/android/exoplayer2/h$b;->d:J

    invoke-virtual {v3, v8, v9, v0, v6}, Lcom/google/android/exoplayer2/h$a;->a(JZ[Z)J

    move-result-wide v8

    .line 894
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    iget-wide v10, v0, Lcom/google/android/exoplayer2/h$b;->d:J

    cmp-long v0, v8, v10

    if-eqz v0, :cond_2

    .line 895
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    iput-wide v8, v0, Lcom/google/android/exoplayer2/h$b;->d:J

    .line 896
    invoke-direct {p0, v8, v9}, Lcom/google/android/exoplayer2/h;->a(J)V

    .line 900
    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->a:[Lcom/google/android/exoplayer2/p;

    array-length v0, v0

    new-array v7, v0, [Z

    move v0, v1

    move v3, v1

    .line 901
    :goto_3
    iget-object v4, p0, Lcom/google/android/exoplayer2/h;->a:[Lcom/google/android/exoplayer2/p;

    array-length v4, v4

    if-ge v0, v4, :cond_c

    .line 902
    iget-object v4, p0, Lcom/google/android/exoplayer2/h;->a:[Lcom/google/android/exoplayer2/p;

    aget-object v8, v4, v0

    .line 903
    invoke-interface {v8}, Lcom/google/android/exoplayer2/p;->d()I

    move-result v4

    if-eqz v4, :cond_a

    move v4, v2

    :goto_4
    aput-boolean v4, v7, v0

    .line 904
    iget-object v4, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget-object v4, v4, Lcom/google/android/exoplayer2/h$a;->d:[Lcom/google/android/exoplayer2/source/m;

    aget-object v4, v4, v0

    .line 905
    if-eqz v4, :cond_3

    .line 906
    add-int/lit8 v3, v3, 0x1

    .line 908
    :cond_3
    aget-boolean v9, v7, v0

    if-eqz v9, :cond_6

    .line 909
    invoke-interface {v8}, Lcom/google/android/exoplayer2/p;->f()Lcom/google/android/exoplayer2/source/m;

    move-result-object v9

    if-eq v4, v9, :cond_b

    .line 911
    iget-object v9, p0, Lcom/google/android/exoplayer2/h;->o:Lcom/google/android/exoplayer2/p;

    if-ne v8, v9, :cond_5

    .line 913
    if-nez v4, :cond_4

    .line 916
    iget-object v4, p0, Lcom/google/android/exoplayer2/h;->e:Lcom/google/android/exoplayer2/util/o;

    iget-object v9, p0, Lcom/google/android/exoplayer2/h;->p:Lcom/google/android/exoplayer2/util/g;

    invoke-virtual {v4, v9}, Lcom/google/android/exoplayer2/util/o;->a(Lcom/google/android/exoplayer2/util/g;)V

    .line 918
    :cond_4
    iput-object v12, p0, Lcom/google/android/exoplayer2/h;->p:Lcom/google/android/exoplayer2/util/g;

    .line 919
    iput-object v12, p0, Lcom/google/android/exoplayer2/h;->o:Lcom/google/android/exoplayer2/p;

    .line 921
    :cond_5
    invoke-direct {p0, v8}, Lcom/google/android/exoplayer2/h;->a(Lcom/google/android/exoplayer2/p;)V

    .line 922
    invoke-interface {v8}, Lcom/google/android/exoplayer2/p;->l()V

    .line 901
    :cond_6
    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 876
    :cond_7
    iget-object v3, p0, Lcom/google/android/exoplayer2/h;->F:Lcom/google/android/exoplayer2/h$a;

    if-ne v5, v3, :cond_8

    move v0, v1

    .line 880
    :cond_8
    iget-object v3, v5, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    move-object v5, v3

    goto/16 :goto_1

    :cond_9
    move v0, v1

    .line 885
    goto/16 :goto_2

    :cond_a
    move v4, v1

    .line 903
    goto :goto_4

    .line 923
    :cond_b
    aget-boolean v4, v6, v0

    if-eqz v4, :cond_6

    .line 925
    iget-wide v10, p0, Lcom/google/android/exoplayer2/h;->D:J

    invoke-interface {v8, v10, v11}, Lcom/google/android/exoplayer2/p;->a(J)V

    goto :goto_5

    .line 929
    :cond_c
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->h:Landroid/os/Handler;

    const/4 v1, 0x3

    iget-object v2, v5, Lcom/google/android/exoplayer2/h$a;->k:Lcom/google/android/exoplayer2/b/h;

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 930
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 931
    invoke-direct {p0, v7, v3}, Lcom/google/android/exoplayer2/h;->a([ZI)V

    .line 947
    :cond_d
    :goto_6
    invoke-direct {p0}, Lcom/google/android/exoplayer2/h;->l()V

    .line 948
    invoke-direct {p0}, Lcom/google/android/exoplayer2/h;->d()V

    .line 949
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->f:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 934
    :cond_e
    iput-object v5, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    .line 935
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    .line 936
    :goto_7
    if-eqz v0, :cond_f

    .line 937
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/h$a;->e()V

    .line 938
    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    goto :goto_7

    .line 940
    :cond_f
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    iput-object v12, v0, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    .line 941
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/h$a;->h:Z

    if-eqz v0, :cond_d

    .line 942
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-wide v2, v0, Lcom/google/android/exoplayer2/m$a;->b:J

    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    iget-wide v4, p0, Lcom/google/android/exoplayer2/h;->D:J

    .line 943
    invoke-virtual {v0, v4, v5}, Lcom/google/android/exoplayer2/h$a;->b(J)J

    move-result-wide v4

    .line 942
    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 944
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/exoplayer2/h$a;->a(JZ)J

    goto :goto_6
.end method

.method private i()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 960
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/h$a;->h:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->F:Lcom/google/android/exoplayer2/h$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->F:Lcom/google/android/exoplayer2/h$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    if-ne v0, v1, :cond_1

    .line 962
    :cond_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->r:[Lcom/google/android/exoplayer2/p;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 963
    invoke-interface {v3}, Lcom/google/android/exoplayer2/p;->g()Z

    move-result v3

    if-nez v3, :cond_2

    .line 969
    :cond_1
    :goto_1
    return-void

    .line 962
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 967
    :cond_3
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->a:Lcom/google/android/exoplayer2/source/h;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/source/h;->g_()V

    goto :goto_1
.end method

.method private j()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 1242
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->H:Lcom/google/android/exoplayer2/u;

    if-nez v0, :cond_1

    .line 1244
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->q:Lcom/google/android/exoplayer2/source/i;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/source/i;->a()V

    .line 1335
    :cond_0
    return-void

    .line 1249
    :cond_1
    invoke-direct {p0}, Lcom/google/android/exoplayer2/h;->k()V

    .line 1250
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/h$a;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1251
    :cond_2
    invoke-direct {p0, v6}, Lcom/google/android/exoplayer2/h;->b(Z)V

    .line 1256
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    if-eqz v0, :cond_0

    .line 1262
    :goto_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->F:Lcom/google/android/exoplayer2/h$a;

    if-eq v0, v1, :cond_5

    iget-wide v0, p0, Lcom/google/android/exoplayer2/h;->D:J

    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget-object v2, v2, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    iget-wide v2, v2, Lcom/google/android/exoplayer2/h$a;->f:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_5

    .line 1266
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/h$a;->e()V

    .line 1267
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/h;->b(Lcom/google/android/exoplayer2/h$a;)V

    .line 1268
    new-instance v0, Lcom/google/android/exoplayer2/h$b;

    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget-object v1, v1, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-object v1, v1, Lcom/google/android/exoplayer2/m$a;->a:Lcom/google/android/exoplayer2/source/i$b;

    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget-object v2, v2, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-wide v2, v2, Lcom/google/android/exoplayer2/m$a;->b:J

    iget-object v4, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget-object v4, v4, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-wide v4, v4, Lcom/google/android/exoplayer2/m$a;->d:J

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/h$b;-><init>(Lcom/google/android/exoplayer2/source/i$b;JJ)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    .line 1270
    invoke-direct {p0}, Lcom/google/android/exoplayer2/h;->d()V

    .line 1271
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->h:Landroid/os/Handler;

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_1

    .line 1252
    :cond_4
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/h;->v:Z

    if-nez v0, :cond_3

    .line 1253
    invoke-direct {p0}, Lcom/google/android/exoplayer2/h;->l()V

    goto :goto_0

    .line 1274
    :cond_5
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->F:Lcom/google/android/exoplayer2/h$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/m$a;->g:Z

    if-eqz v0, :cond_7

    move v0, v6

    .line 1275
    :goto_2
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->a:[Lcom/google/android/exoplayer2/p;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 1276
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->a:[Lcom/google/android/exoplayer2/p;

    aget-object v1, v1, v0

    .line 1277
    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->F:Lcom/google/android/exoplayer2/h$a;

    iget-object v2, v2, Lcom/google/android/exoplayer2/h$a;->d:[Lcom/google/android/exoplayer2/source/m;

    aget-object v2, v2, v0

    .line 1280
    if-eqz v2, :cond_6

    invoke-interface {v1}, Lcom/google/android/exoplayer2/p;->f()Lcom/google/android/exoplayer2/source/m;

    move-result-object v3

    if-ne v3, v2, :cond_6

    .line 1281
    invoke-interface {v1}, Lcom/google/android/exoplayer2/p;->g()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1282
    invoke-interface {v1}, Lcom/google/android/exoplayer2/p;->h()V

    .line 1275
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    move v0, v6

    .line 1288
    :goto_3
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->a:[Lcom/google/android/exoplayer2/p;

    array-length v1, v1

    if-ge v0, v1, :cond_9

    .line 1289
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->a:[Lcom/google/android/exoplayer2/p;

    aget-object v1, v1, v0

    .line 1290
    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->F:Lcom/google/android/exoplayer2/h$a;

    iget-object v2, v2, Lcom/google/android/exoplayer2/h$a;->d:[Lcom/google/android/exoplayer2/source/m;

    aget-object v2, v2, v0

    .line 1291
    invoke-interface {v1}, Lcom/google/android/exoplayer2/p;->f()Lcom/google/android/exoplayer2/source/m;

    move-result-object v3

    if-ne v3, v2, :cond_0

    if-eqz v2, :cond_8

    .line 1292
    invoke-interface {v1}, Lcom/google/android/exoplayer2/p;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1288
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1297
    :cond_9
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->F:Lcom/google/android/exoplayer2/h$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->F:Lcom/google/android/exoplayer2/h$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/h$a;->h:Z

    if-eqz v0, :cond_0

    .line 1298
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->F:Lcom/google/android/exoplayer2/h$a;

    iget-object v3, v0, Lcom/google/android/exoplayer2/h$a;->k:Lcom/google/android/exoplayer2/b/h;

    .line 1299
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->F:Lcom/google/android/exoplayer2/h$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/h;->F:Lcom/google/android/exoplayer2/h$a;

    .line 1300
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->F:Lcom/google/android/exoplayer2/h$a;

    iget-object v4, v0, Lcom/google/android/exoplayer2/h$a;->k:Lcom/google/android/exoplayer2/b/h;

    .line 1302
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->F:Lcom/google/android/exoplayer2/h$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->a:Lcom/google/android/exoplayer2/source/h;

    .line 1303
    invoke-interface {v0}, Lcom/google/android/exoplayer2/source/h;->c()J

    move-result-wide v0

    const-wide v8, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, v0, v8

    if-eqz v0, :cond_b

    const/4 v0, 0x1

    :goto_4
    move v1, v6

    .line 1304
    :goto_5
    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->a:[Lcom/google/android/exoplayer2/p;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 1305
    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->a:[Lcom/google/android/exoplayer2/p;

    aget-object v5, v2, v1

    .line 1306
    iget-object v2, v3, Lcom/google/android/exoplayer2/b/h;->b:Lcom/google/android/exoplayer2/b/f;

    invoke-virtual {v2, v1}, Lcom/google/android/exoplayer2/b/f;->a(I)Lcom/google/android/exoplayer2/b/e;

    move-result-object v2

    .line 1307
    if-nez v2, :cond_c

    .line 1304
    :cond_a
    :goto_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_b
    move v0, v6

    .line 1303
    goto :goto_4

    .line 1309
    :cond_c
    if-eqz v0, :cond_d

    .line 1312
    invoke-interface {v5}, Lcom/google/android/exoplayer2/p;->h()V

    goto :goto_6

    .line 1313
    :cond_d
    invoke-interface {v5}, Lcom/google/android/exoplayer2/p;->i()Z

    move-result v2

    if-nez v2, :cond_a

    .line 1314
    iget-object v2, v4, Lcom/google/android/exoplayer2/b/h;->b:Lcom/google/android/exoplayer2/b/f;

    invoke-virtual {v2, v1}, Lcom/google/android/exoplayer2/b/f;->a(I)Lcom/google/android/exoplayer2/b/e;

    move-result-object v7

    .line 1315
    iget-object v2, v3, Lcom/google/android/exoplayer2/b/h;->d:[Lcom/google/android/exoplayer2/r;

    aget-object v2, v2, v1

    .line 1316
    iget-object v8, v4, Lcom/google/android/exoplayer2/b/h;->d:[Lcom/google/android/exoplayer2/r;

    aget-object v8, v8, v1

    .line 1317
    if-eqz v7, :cond_f

    invoke-virtual {v8, v2}, Lcom/google/android/exoplayer2/r;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1320
    invoke-interface {v7}, Lcom/google/android/exoplayer2/b/e;->b()I

    move-result v2

    new-array v8, v2, [Lcom/google/android/exoplayer2/j;

    move v2, v6

    .line 1321
    :goto_7
    array-length v9, v8

    if-ge v2, v9, :cond_e

    .line 1322
    invoke-interface {v7, v2}, Lcom/google/android/exoplayer2/b/e;->a(I)Lcom/google/android/exoplayer2/j;

    move-result-object v9

    aput-object v9, v8, v2

    .line 1321
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 1324
    :cond_e
    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->F:Lcom/google/android/exoplayer2/h$a;

    iget-object v2, v2, Lcom/google/android/exoplayer2/h$a;->d:[Lcom/google/android/exoplayer2/source/m;

    aget-object v2, v2, v1

    iget-object v7, p0, Lcom/google/android/exoplayer2/h;->F:Lcom/google/android/exoplayer2/h$a;

    .line 1325
    invoke-virtual {v7}, Lcom/google/android/exoplayer2/h$a;->a()J

    move-result-wide v10

    .line 1324
    invoke-interface {v5, v8, v2, v10, v11}, Lcom/google/android/exoplayer2/p;->a([Lcom/google/android/exoplayer2/j;Lcom/google/android/exoplayer2/source/m;J)V

    goto :goto_6

    .line 1330
    :cond_f
    invoke-interface {v5}, Lcom/google/android/exoplayer2/p;->h()V

    goto :goto_6
.end method

.method private k()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x1

    .line 1339
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    if-nez v0, :cond_1

    .line 1340
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->l:Lcom/google/android/exoplayer2/m;

    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->m:Lcom/google/android/exoplayer2/h$b;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/m;->a(Lcom/google/android/exoplayer2/h$b;)Lcom/google/android/exoplayer2/m$a;

    move-result-object v11

    .line 1356
    :goto_0
    if-nez v11, :cond_3

    .line 1357
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->q:Lcom/google/android/exoplayer2/source/i;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/source/i;->a()V

    .line 1374
    :cond_0
    :goto_1
    return-void

    .line 1342
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/m$a;->g:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/h$a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/m$a;->e:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1346
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    if-eqz v0, :cond_2

    .line 1347
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    iget v0, v0, Lcom/google/android/exoplayer2/h$a;->c:I

    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->G:Lcom/google/android/exoplayer2/h$a;

    iget v1, v1, Lcom/google/android/exoplayer2/h$a;->c:I

    sub-int/2addr v0, v1

    .line 1348
    const/16 v1, 0x64

    if-eq v0, v1, :cond_0

    .line 1353
    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->l:Lcom/google/android/exoplayer2/m;

    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    iget-object v1, v1, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    .line 1354
    invoke-virtual {v2}, Lcom/google/android/exoplayer2/h$a;->a()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/exoplayer2/h;->D:J

    .line 1353
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/exoplayer2/m;->a(Lcom/google/android/exoplayer2/m$a;JJ)Lcom/google/android/exoplayer2/m$a;

    move-result-object v11

    goto :goto_0

    .line 1361
    :cond_3
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    if-nez v0, :cond_5

    const-wide/32 v4, 0x3938700

    .line 1364
    :goto_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    if-nez v0, :cond_6

    const/4 v10, 0x0

    .line 1365
    :goto_3
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->H:Lcom/google/android/exoplayer2/u;

    iget-object v1, v11, Lcom/google/android/exoplayer2/m$a;->a:Lcom/google/android/exoplayer2/source/i$b;

    iget v1, v1, Lcom/google/android/exoplayer2/source/i$b;->b:I

    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->k:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v0, v1, v2, v12}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;Z)Lcom/google/android/exoplayer2/u$a;

    move-result-object v0

    iget-object v9, v0, Lcom/google/android/exoplayer2/u$a;->b:Ljava/lang/Object;

    .line 1366
    new-instance v1, Lcom/google/android/exoplayer2/h$a;

    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->a:[Lcom/google/android/exoplayer2/p;

    iget-object v3, p0, Lcom/google/android/exoplayer2/h;->b:[Lcom/google/android/exoplayer2/q;

    iget-object v6, p0, Lcom/google/android/exoplayer2/h;->c:Lcom/google/android/exoplayer2/b/g;

    iget-object v7, p0, Lcom/google/android/exoplayer2/h;->d:Lcom/google/android/exoplayer2/l;

    iget-object v8, p0, Lcom/google/android/exoplayer2/h;->q:Lcom/google/android/exoplayer2/source/i;

    invoke-direct/range {v1 .. v11}, Lcom/google/android/exoplayer2/h$a;-><init>([Lcom/google/android/exoplayer2/p;[Lcom/google/android/exoplayer2/q;JLcom/google/android/exoplayer2/b/g;Lcom/google/android/exoplayer2/l;Lcom/google/android/exoplayer2/source/i;Ljava/lang/Object;ILcom/google/android/exoplayer2/m$a;)V

    .line 1368
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    if-eqz v0, :cond_4

    .line 1369
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    iput-object v1, v0, Lcom/google/android/exoplayer2/h$a;->j:Lcom/google/android/exoplayer2/h$a;

    .line 1371
    :cond_4
    iput-object v1, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    .line 1372
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$a;->a:Lcom/google/android/exoplayer2/source/h;

    iget-wide v2, v11, Lcom/google/android/exoplayer2/m$a;->b:J

    invoke-interface {v0, p0, v2, v3}, Lcom/google/android/exoplayer2/source/h;->a(Lcom/google/android/exoplayer2/source/h$a;J)V

    .line 1373
    invoke-direct {p0, v12}, Lcom/google/android/exoplayer2/h;->b(Z)V

    goto/16 :goto_1

    .line 1361
    :cond_5
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    .line 1363
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/h$a;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    iget-object v2, v2, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-wide v2, v2, Lcom/google/android/exoplayer2/m$a;->e:J

    add-long v4, v0, v2

    goto :goto_2

    .line 1364
    :cond_6
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    iget v0, v0, Lcom/google/android/exoplayer2/h$a;->c:I

    add-int/lit8 v10, v0, 0x1

    goto :goto_3
.end method

.method private l()V
    .locals 4

    .prologue
    .line 1400
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    iget-wide v2, p0, Lcom/google/android/exoplayer2/h;->D:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/exoplayer2/h$a;->c(J)Z

    move-result v0

    .line 1401
    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/h;->b(Z)V

    .line 1402
    if-eqz v0, :cond_0

    .line 1403
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->E:Lcom/google/android/exoplayer2/h$a;

    iget-wide v2, p0, Lcom/google/android/exoplayer2/h;->D:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/exoplayer2/h$a;->d(J)V

    .line 1405
    :cond_0
    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 2

    .prologue
    .line 276
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/h;->s:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 288
    :goto_0
    monitor-exit p0

    return-void

    .line 279
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->f:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 280
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/h;->s:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_1

    .line 282
    :try_start_2
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 283
    :catch_0
    move-exception v0

    .line 284
    :try_start_3
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 276
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 287
    :cond_1
    :try_start_4
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->g:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->f:Landroid/os/Handler;

    const/16 v1, 0xc

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 235
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/source/h;)V
    .locals 2

    .prologue
    .line 305
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->f:Landroid/os/Handler;

    const/16 v1, 0x8

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 306
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/source/i;Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 225
    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->f:Landroid/os/Handler;

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v1, v0, v1, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 226
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 227
    return-void

    :cond_0
    move v0, v1

    .line 225
    goto :goto_0
.end method

.method public synthetic a(Lcom/google/android/exoplayer2/source/n;)V
    .locals 0

    .prologue
    .line 46
    check-cast p1, Lcom/google/android/exoplayer2/source/h;

    invoke-virtual {p0, p1}, Lcom/google/android/exoplayer2/h;->b(Lcom/google/android/exoplayer2/source/h;)V

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/u;IJ)V
    .locals 3

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->f:Landroid/os/Handler;

    const/4 v1, 0x3

    new-instance v2, Lcom/google/android/exoplayer2/h$c;

    invoke-direct {v2, p1, p2, p3, p4}, Lcom/google/android/exoplayer2/h$c;-><init>(Lcom/google/android/exoplayer2/u;IJ)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 239
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 240
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/u;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 298
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->f:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 299
    return-void
.end method

.method public a(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 230
    iget-object v3, p0, Lcom/google/android/exoplayer2/h;->f:Landroid/os/Handler;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v1, v0, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 231
    return-void

    :cond_0
    move v0, v2

    .line 230
    goto :goto_0
.end method

.method public varargs a([Lcom/google/android/exoplayer2/e$c;)V
    .locals 2

    .prologue
    .line 251
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/h;->s:Z

    if-eqz v0, :cond_0

    .line 252
    const-string v0, "ExoPlayerImplInternal"

    const-string v1, "Ignoring messages sent after release."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    :goto_0
    return-void

    .line 255
    :cond_0
    iget v0, p0, Lcom/google/android/exoplayer2/h;->y:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/exoplayer2/h;->y:I

    .line 256
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->f:Landroid/os/Handler;

    const/16 v1, 0xb

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method public b(Lcom/google/android/exoplayer2/source/h;)V
    .locals 2

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/exoplayer2/h;->f:Landroid/os/Handler;

    const/16 v1, 0x9

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 311
    return-void
.end method

.method public varargs declared-synchronized b([Lcom/google/android/exoplayer2/e$c;)V
    .locals 3

    .prologue
    .line 260
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/h;->s:Z

    if-eqz v0, :cond_1

    .line 261
    const-string v0, "ExoPlayerImplInternal"

    const-string v1, "Ignoring messages sent after release."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 273
    :cond_0
    monitor-exit p0

    return-void

    .line 264
    :cond_1
    :try_start_1
    iget v0, p0, Lcom/google/android/exoplayer2/h;->y:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/exoplayer2/h;->y:I

    .line 265
    iget-object v1, p0, Lcom/google/android/exoplayer2/h;->f:Landroid/os/Handler;

    const/16 v2, 0xb

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 266
    :goto_0
    iget v1, p0, Lcom/google/android/exoplayer2/h;->z:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-gt v1, v0, :cond_0

    .line 268
    :try_start_2
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 269
    :catch_0
    move-exception v1

    .line 270
    :try_start_3
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 260
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 326
    :try_start_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    move v0, v2

    .line 397
    :goto_0
    return v0

    .line 328
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/source/i;

    iget v3, p1, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_0

    move v2, v1

    :cond_0
    invoke-direct {p0, v0, v2}, Lcom/google/android/exoplayer2/h;->b(Lcom/google/android/exoplayer2/source/i;Z)V

    move v0, v1

    .line 329
    goto :goto_0

    .line 332
    :pswitch_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_1

    move v2, v1

    :cond_1
    invoke-direct {p0, v2}, Lcom/google/android/exoplayer2/h;->c(Z)V

    move v0, v1

    .line 333
    goto :goto_0

    .line 336
    :pswitch_2
    iget v0, p1, Landroid/os/Message;->arg1:I

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/h;->c(I)V

    move v0, v1

    .line 337
    goto :goto_0

    .line 340
    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/exoplayer2/h;->e()V

    move v0, v1

    .line 341
    goto :goto_0

    .line 344
    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/h$c;

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/h;->a(Lcom/google/android/exoplayer2/h$c;)V

    move v0, v1

    .line 345
    goto :goto_0

    .line 348
    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/n;

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/h;->a(Lcom/google/android/exoplayer2/n;)V

    move v0, v1

    .line 349
    goto :goto_0

    .line 352
    :pswitch_6
    invoke-direct {p0}, Lcom/google/android/exoplayer2/h;->f()V

    move v0, v1

    .line 353
    goto :goto_0

    .line 356
    :pswitch_7
    invoke-direct {p0}, Lcom/google/android/exoplayer2/h;->g()V

    move v0, v1

    .line 357
    goto :goto_0

    .line 360
    :pswitch_8
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/source/h;

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/h;->c(Lcom/google/android/exoplayer2/source/h;)V

    move v0, v1

    .line 361
    goto :goto_0

    .line 364
    :pswitch_9
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/h;->a(Landroid/util/Pair;)V

    move v0, v1

    .line 365
    goto :goto_0

    .line 368
    :pswitch_a
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/source/h;

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/h;->d(Lcom/google/android/exoplayer2/source/h;)V

    move v0, v1

    .line 369
    goto :goto_0

    .line 372
    :pswitch_b
    invoke-direct {p0}, Lcom/google/android/exoplayer2/h;->h()V

    move v0, v1

    .line 373
    goto :goto_0

    .line 376
    :pswitch_c
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [Lcom/google/android/exoplayer2/e$c;

    check-cast v0, [Lcom/google/android/exoplayer2/e$c;

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/h;->c([Lcom/google/android/exoplayer2/e$c;)V
    :try_end_0
    .catch Lcom/google/android/exoplayer2/ExoPlaybackException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    move v0, v1

    .line 377
    goto :goto_0

    .line 382
    :catch_0
    move-exception v0

    .line 383
    const-string v2, "ExoPlayerImplInternal"

    const-string v3, "Renderer error."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 384
    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->h:Landroid/os/Handler;

    invoke-virtual {v2, v4, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 385
    invoke-direct {p0}, Lcom/google/android/exoplayer2/h;->f()V

    move v0, v1

    .line 386
    goto/16 :goto_0

    .line 387
    :catch_1
    move-exception v0

    .line 388
    const-string v2, "ExoPlayerImplInternal"

    const-string v3, "Source error."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 389
    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->h:Landroid/os/Handler;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ExoPlaybackException;->a(Ljava/io/IOException;)Lcom/google/android/exoplayer2/ExoPlaybackException;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 390
    invoke-direct {p0}, Lcom/google/android/exoplayer2/h;->f()V

    move v0, v1

    .line 391
    goto/16 :goto_0

    .line 392
    :catch_2
    move-exception v0

    .line 393
    const-string v2, "ExoPlayerImplInternal"

    const-string v3, "Internal runtime error."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 394
    iget-object v2, p0, Lcom/google/android/exoplayer2/h;->h:Landroid/os/Handler;

    invoke-static {v0}, Lcom/google/android/exoplayer2/ExoPlaybackException;->a(Ljava/lang/RuntimeException;)Lcom/google/android/exoplayer2/ExoPlaybackException;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 395
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 396
    invoke-direct {p0}, Lcom/google/android/exoplayer2/h;->f()V

    move v0, v1

    .line 397
    goto/16 :goto_0

    .line 326
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_9
        :pswitch_8
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_2
    .end packed-switch
.end method
