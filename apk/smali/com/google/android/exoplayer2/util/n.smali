.class public final Lcom/google/android/exoplayer2/util/n;
.super Ljava/lang/Object;
.source "RepeatModeUtil.java"


# direct methods
.method public static a(II)I
    .locals 3

    .prologue
    .line 61
    const/4 v0, 0x1

    move v1, v0

    :goto_0
    const/4 v0, 0x2

    if-gt v1, v0, :cond_0

    .line 62
    add-int v0, p0, v1

    rem-int/lit8 v0, v0, 0x3

    .line 63
    invoke-static {v0, p1}, Lcom/google/android/exoplayer2/util/n;->b(II)Z

    move-result v2

    if-eqz v2, :cond_1

    move p0, v0

    .line 67
    :cond_0
    return p0

    .line 61
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public static b(II)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 78
    packed-switch p0, :pswitch_data_0

    move v0, v1

    .line 86
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 82
    :pswitch_1
    and-int/lit8 v2, p1, 0x1

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 84
    :pswitch_2
    and-int/lit8 v2, p1, 0x2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 78
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
