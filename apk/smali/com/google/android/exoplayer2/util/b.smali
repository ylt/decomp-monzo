.class public final Lcom/google/android/exoplayer2/util/b;
.super Ljava/lang/Object;
.source "CodecSpecificDataUtil.java"


# static fields
.field private static final a:[B

.field private static final b:[I

.field private static final c:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/exoplayer2/util/b;->a:[B

    .line 32
    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/exoplayer2/util/b;->b:[I

    .line 50
    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/exoplayer2/util/b;->c:[I

    return-void

    .line 28
    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x1t
    .end array-data

    .line 32
    :array_1
    .array-data 4
        0x17700
        0x15888
        0xfa00
        0xbb80
        0xac44
        0x7d00
        0x5dc0
        0x5622
        0x3e80
        0x2ee0
        0x2b11
        0x1f40
        0x1cb6
    .end array-data

    .line 50
    :array_2
    .array-data 4
        0x0
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
        0x8
        -0x1
        -0x1
        -0x1
        0x7
        0x8
        -0x1
        0x8
        -0x1
    .end array-data
.end method

.method private static a(Lcom/google/android/exoplayer2/util/j;)I
    .locals 2

    .prologue
    .line 246
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/j;->c(I)I

    move-result v0

    .line 247
    const/16 v1, 0x1f

    if-ne v0, v1, :cond_0

    .line 248
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/j;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x20

    .line 250
    :cond_0
    return v0
.end method

.method private static a([BI)I
    .locals 3

    .prologue
    .line 211
    array-length v0, p0

    sget-object v1, Lcom/google/android/exoplayer2/util/b;->a:[B

    array-length v1, v1

    sub-int v1, v0, v1

    move v0, p1

    .line 212
    :goto_0
    if-gt v0, v1, :cond_1

    .line 213
    invoke-static {p0, v0}, Lcom/google/android/exoplayer2/util/b;->b([BI)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 217
    :goto_1
    return v0

    .line 212
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 217
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static a([B)Landroid/util/Pair;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x4

    .line 90
    new-instance v2, Lcom/google/android/exoplayer2/util/j;

    invoke-direct {v2, p0}, Lcom/google/android/exoplayer2/util/j;-><init>([B)V

    .line 91
    invoke-static {v2}, Lcom/google/android/exoplayer2/util/b;->a(Lcom/google/android/exoplayer2/util/j;)I

    move-result v3

    .line 92
    invoke-static {v2}, Lcom/google/android/exoplayer2/util/b;->b(Lcom/google/android/exoplayer2/util/j;)I

    move-result v1

    .line 93
    invoke-virtual {v2, v5}, Lcom/google/android/exoplayer2/util/j;->c(I)I

    move-result v0

    .line 94
    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    const/16 v4, 0x1d

    if-ne v3, v4, :cond_1

    .line 100
    :cond_0
    invoke-static {v2}, Lcom/google/android/exoplayer2/util/b;->b(Lcom/google/android/exoplayer2/util/j;)I

    move-result v1

    .line 101
    invoke-static {v2}, Lcom/google/android/exoplayer2/util/b;->a(Lcom/google/android/exoplayer2/util/j;)I

    move-result v3

    .line 102
    const/16 v4, 0x16

    if-ne v3, v4, :cond_1

    .line 104
    invoke-virtual {v2, v5}, Lcom/google/android/exoplayer2/util/j;->c(I)I

    move-result v0

    .line 107
    :cond_1
    sget-object v2, Lcom/google/android/exoplayer2/util/b;->c:[I

    aget v2, v2, v0

    .line 108
    const/4 v0, -0x1

    if-eq v2, v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/a;->a(Z)V

    .line 109
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    .line 108
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(II)[B
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 120
    move v0, v1

    move v2, v3

    .line 121
    :goto_0
    sget-object v4, Lcom/google/android/exoplayer2/util/b;->b:[I

    array-length v4, v4

    if-ge v0, v4, :cond_1

    .line 122
    sget-object v4, Lcom/google/android/exoplayer2/util/b;->b:[I

    aget v4, v4, v0

    if-ne p0, v4, :cond_0

    move v2, v0

    .line 121
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v3

    .line 127
    :goto_1
    sget-object v4, Lcom/google/android/exoplayer2/util/b;->c:[I

    array-length v4, v4

    if-ge v1, v4, :cond_3

    .line 128
    sget-object v4, Lcom/google/android/exoplayer2/util/b;->c:[I

    aget v4, v4, v1

    if-ne p1, v4, :cond_2

    move v0, v1

    .line 127
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 132
    :cond_3
    if-eq p0, v3, :cond_4

    if-ne v0, v3, :cond_5

    .line 133
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid sample rate or number of channels: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 136
    :cond_5
    const/4 v1, 0x2

    invoke-static {v1, v2, v0}, Lcom/google/android/exoplayer2/util/b;->a(III)[B

    move-result-object v0

    return-object v0
.end method

.method public static a(III)[B
    .locals 4

    .prologue
    .line 149
    const/4 v0, 0x2

    new-array v0, v0, [B

    .line 150
    const/4 v1, 0x0

    shl-int/lit8 v2, p0, 0x3

    and-int/lit16 v2, v2, 0xf8

    shr-int/lit8 v3, p1, 0x1

    and-int/lit8 v3, v3, 0x7

    or-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 151
    const/4 v1, 0x1

    shl-int/lit8 v2, p1, 0x7

    and-int/lit16 v2, v2, 0x80

    shl-int/lit8 v3, p2, 0x3

    and-int/lit8 v3, v3, 0x78

    or-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 152
    return-object v0
.end method

.method public static a([BII)[B
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 164
    sget-object v0, Lcom/google/android/exoplayer2/util/b;->a:[B

    array-length v0, v0

    add-int/2addr v0, p2

    new-array v0, v0, [B

    .line 165
    sget-object v1, Lcom/google/android/exoplayer2/util/b;->a:[B

    sget-object v2, Lcom/google/android/exoplayer2/util/b;->a:[B

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 166
    sget-object v1, Lcom/google/android/exoplayer2/util/b;->a:[B

    array-length v1, v1

    invoke-static {p0, p1, v0, v1, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 167
    return-object v0
.end method

.method private static b(Lcom/google/android/exoplayer2/util/j;)I
    .locals 2

    .prologue
    .line 262
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/j;->c(I)I

    move-result v1

    .line 263
    const/16 v0, 0xf

    if-ne v1, v0, :cond_0

    .line 264
    const/16 v0, 0x18

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/j;->c(I)I

    move-result v0

    .line 269
    :goto_0
    return v0

    .line 266
    :cond_0
    const/16 v0, 0xd

    if-ge v1, v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/a;->a(Z)V

    .line 267
    sget-object v0, Lcom/google/android/exoplayer2/util/b;->b:[I

    aget v0, v0, v1

    goto :goto_0

    .line 266
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static b([BI)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 228
    array-length v0, p0

    sub-int/2addr v0, p1

    sget-object v2, Lcom/google/android/exoplayer2/util/b;->a:[B

    array-length v2, v2

    if-gt v0, v2, :cond_1

    .line 236
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 231
    :goto_1
    sget-object v2, Lcom/google/android/exoplayer2/util/b;->a:[B

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 232
    add-int v2, p1, v0

    aget-byte v2, p0, v2

    sget-object v3, Lcom/google/android/exoplayer2/util/b;->a:[B

    aget-byte v3, v3, v0

    if-ne v2, v3, :cond_0

    .line 231
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 236
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static b([B)[[B
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 182
    invoke-static {p0, v1}, Lcom/google/android/exoplayer2/util/b;->b([BI)Z

    move-result v0

    if-nez v0, :cond_0

    .line 184
    const/4 v0, 0x0

    check-cast v0, [[B

    .line 200
    :goto_0
    return-object v0

    .line 186
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 189
    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 190
    sget-object v2, Lcom/google/android/exoplayer2/util/b;->a:[B

    array-length v2, v2

    add-int/2addr v0, v2

    invoke-static {p0, v0}, Lcom/google/android/exoplayer2/util/b;->a([BI)I

    move-result v0

    .line 191
    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    .line 192
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [[B

    move v2, v1

    .line 193
    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 194
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 195
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v2, v0, :cond_2

    add-int/lit8 v0, v2, 0x1

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 196
    :goto_2
    sub-int/2addr v0, v5

    new-array v0, v0, [B

    .line 197
    array-length v6, v0

    invoke-static {p0, v5, v0, v1, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 198
    aput-object v0, v3, v2

    .line 193
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 195
    :cond_2
    array-length v0, p0

    goto :goto_2

    :cond_3
    move-object v0, v3

    .line 200
    goto :goto_0
.end method
