.class final Lcom/google/android/exoplayer2/source/g$a;
.super Lcom/google/android/exoplayer2/u;
.source "LoopingMediaSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/source/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final b:Lcom/google/android/exoplayer2/u;


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/u;)V
    .locals 0

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/google/android/exoplayer2/u;-><init>()V

    .line 168
    iput-object p1, p0, Lcom/google/android/exoplayer2/source/g$a;->b:Lcom/google/android/exoplayer2/u;

    .line 169
    return-void
.end method


# virtual methods
.method public a(II)I
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/g$a;->b:Lcom/google/android/exoplayer2/u;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/u;->a(II)I

    move-result v0

    .line 179
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return v0
.end method

.method public a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/g$a;->b:Lcom/google/android/exoplayer2/u;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/u;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public a(ILcom/google/android/exoplayer2/u$a;Z)Lcom/google/android/exoplayer2/u$a;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/g$a;->b:Lcom/google/android/exoplayer2/u;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;Z)Lcom/google/android/exoplayer2/u$a;

    move-result-object v0

    return-object v0
.end method

.method public a(ILcom/google/android/exoplayer2/u$b;ZJ)Lcom/google/android/exoplayer2/u$b;
    .locals 6

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/g$a;->b:Lcom/google/android/exoplayer2/u;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$b;ZJ)Lcom/google/android/exoplayer2/u$b;

    move-result-object v0

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/g$a;->b:Lcom/google/android/exoplayer2/u;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/u;->b()I

    move-result v0

    return v0
.end method

.method public b(II)I
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/g$a;->b:Lcom/google/android/exoplayer2/u;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/u;->b(II)I

    move-result v0

    .line 185
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/source/g$a;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :cond_0
    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/g$a;->b:Lcom/google/android/exoplayer2/u;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/u;->c()I

    move-result v0

    return v0
.end method
