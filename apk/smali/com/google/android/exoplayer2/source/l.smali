.class public final Lcom/google/android/exoplayer2/source/l;
.super Ljava/lang/Object;
.source "SampleQueue.java"

# interfaces
.implements Lcom/google/android/exoplayer2/extractor/m;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/source/l$a;,
        Lcom/google/android/exoplayer2/source/l$b;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/exoplayer2/upstream/b;

.field private final b:I

.field private final c:Lcom/google/android/exoplayer2/source/k;

.field private final d:Lcom/google/android/exoplayer2/source/k$a;

.field private final e:Lcom/google/android/exoplayer2/util/k;

.field private f:Lcom/google/android/exoplayer2/source/l$a;

.field private g:Lcom/google/android/exoplayer2/source/l$a;

.field private h:Lcom/google/android/exoplayer2/source/l$a;

.field private i:Lcom/google/android/exoplayer2/j;

.field private j:Z

.field private k:Lcom/google/android/exoplayer2/j;

.field private l:J

.field private m:J

.field private n:Z

.field private o:Lcom/google/android/exoplayer2/source/l$b;


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/upstream/b;)V
    .locals 4

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p1, p0, Lcom/google/android/exoplayer2/source/l;->a:Lcom/google/android/exoplayer2/upstream/b;

    .line 81
    invoke-interface {p1}, Lcom/google/android/exoplayer2/upstream/b;->c()I

    move-result v0

    iput v0, p0, Lcom/google/android/exoplayer2/source/l;->b:I

    .line 82
    new-instance v0, Lcom/google/android/exoplayer2/source/k;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/source/k;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/l;->c:Lcom/google/android/exoplayer2/source/k;

    .line 83
    new-instance v0, Lcom/google/android/exoplayer2/source/k$a;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/source/k$a;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/l;->d:Lcom/google/android/exoplayer2/source/k$a;

    .line 84
    new-instance v0, Lcom/google/android/exoplayer2/util/k;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/util/k;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/l;->e:Lcom/google/android/exoplayer2/util/k;

    .line 85
    new-instance v0, Lcom/google/android/exoplayer2/source/l$a;

    const-wide/16 v2, 0x0

    iget v1, p0, Lcom/google/android/exoplayer2/source/l;->b:I

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/exoplayer2/source/l$a;-><init>(JI)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/l;->f:Lcom/google/android/exoplayer2/source/l$a;

    .line 86
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->f:Lcom/google/android/exoplayer2/source/l$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/l;->g:Lcom/google/android/exoplayer2/source/l$a;

    .line 87
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->f:Lcom/google/android/exoplayer2/source/l$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/l;->h:Lcom/google/android/exoplayer2/source/l$a;

    .line 88
    return-void
.end method

.method private a(I)I
    .locals 6

    .prologue
    .line 591
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->h:Lcom/google/android/exoplayer2/source/l$a;

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/source/l$a;->c:Z

    if-nez v0, :cond_0

    .line 592
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->h:Lcom/google/android/exoplayer2/source/l$a;

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/l;->a:Lcom/google/android/exoplayer2/upstream/b;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/upstream/b;->a()Lcom/google/android/exoplayer2/upstream/a;

    move-result-object v1

    new-instance v2, Lcom/google/android/exoplayer2/source/l$a;

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/l;->h:Lcom/google/android/exoplayer2/source/l$a;

    iget-wide v4, v3, Lcom/google/android/exoplayer2/source/l$a;->b:J

    iget v3, p0, Lcom/google/android/exoplayer2/source/l;->b:I

    invoke-direct {v2, v4, v5, v3}, Lcom/google/android/exoplayer2/source/l$a;-><init>(JI)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/source/l$a;->a(Lcom/google/android/exoplayer2/upstream/a;Lcom/google/android/exoplayer2/source/l$a;)V

    .line 595
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->h:Lcom/google/android/exoplayer2/source/l$a;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/source/l$a;->b:J

    iget-wide v2, p0, Lcom/google/android/exoplayer2/source/l;->m:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method private static a(Lcom/google/android/exoplayer2/j;J)Lcom/google/android/exoplayer2/j;
    .locals 5

    .prologue
    .line 618
    if-nez p0, :cond_1

    .line 619
    const/4 p0, 0x0

    .line 624
    :cond_0
    :goto_0
    return-object p0

    .line 621
    :cond_1
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/exoplayer2/j;->w:J

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 622
    iget-wide v0, p0, Lcom/google/android/exoplayer2/j;->w:J

    add-long/2addr v0, p1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/exoplayer2/j;->a(J)Lcom/google/android/exoplayer2/j;

    move-result-object p0

    goto :goto_0
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 449
    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->g:Lcom/google/android/exoplayer2/source/l$a;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/source/l$a;->b:J

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    .line 450
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->g:Lcom/google/android/exoplayer2/source/l$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/l$a;->e:Lcom/google/android/exoplayer2/source/l$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/l;->g:Lcom/google/android/exoplayer2/source/l$a;

    goto :goto_0

    .line 452
    :cond_0
    return-void
.end method

.method private a(JLjava/nio/ByteBuffer;I)V
    .locals 3

    .prologue
    .line 406
    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/source/l;->a(J)V

    .line 408
    :cond_0
    :goto_0
    if-lez p4, :cond_1

    .line 409
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->g:Lcom/google/android/exoplayer2/source/l$a;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/source/l$a;->b:J

    sub-long/2addr v0, p1

    long-to-int v0, v0

    invoke-static {p4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 410
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/l;->g:Lcom/google/android/exoplayer2/source/l$a;

    iget-object v1, v1, Lcom/google/android/exoplayer2/source/l$a;->d:Lcom/google/android/exoplayer2/upstream/a;

    .line 411
    iget-object v1, v1, Lcom/google/android/exoplayer2/upstream/a;->a:[B

    iget-object v2, p0, Lcom/google/android/exoplayer2/source/l;->g:Lcom/google/android/exoplayer2/source/l$a;

    invoke-virtual {v2, p1, p2}, Lcom/google/android/exoplayer2/source/l$a;->a(J)I

    move-result v2

    invoke-virtual {p3, v1, v2, v0}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 412
    sub-int/2addr p4, v0

    .line 413
    int-to-long v0, v0

    add-long/2addr p1, v0

    .line 414
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->g:Lcom/google/android/exoplayer2/source/l$a;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/source/l$a;->b:J

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 415
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->g:Lcom/google/android/exoplayer2/source/l$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/l$a;->e:Lcom/google/android/exoplayer2/source/l$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/l;->g:Lcom/google/android/exoplayer2/source/l$a;

    goto :goto_0

    .line 418
    :cond_1
    return-void
.end method

.method private a(J[BI)V
    .locals 5

    .prologue
    .line 428
    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/source/l;->a(J)V

    move v0, p4

    .line 430
    :cond_0
    :goto_0
    if-lez v0, :cond_1

    .line 431
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/l;->g:Lcom/google/android/exoplayer2/source/l$a;

    iget-wide v2, v1, Lcom/google/android/exoplayer2/source/l$a;->b:J

    sub-long/2addr v2, p1

    long-to-int v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 432
    iget-object v2, p0, Lcom/google/android/exoplayer2/source/l;->g:Lcom/google/android/exoplayer2/source/l$a;

    iget-object v2, v2, Lcom/google/android/exoplayer2/source/l$a;->d:Lcom/google/android/exoplayer2/upstream/a;

    .line 433
    iget-object v2, v2, Lcom/google/android/exoplayer2/upstream/a;->a:[B

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/l;->g:Lcom/google/android/exoplayer2/source/l$a;

    invoke-virtual {v3, p1, p2}, Lcom/google/android/exoplayer2/source/l$a;->a(J)I

    move-result v3

    sub-int v4, p4, v0

    invoke-static {v2, v3, p3, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 435
    sub-int/2addr v0, v1

    .line 436
    int-to-long v2, v1

    add-long/2addr p1, v2

    .line 437
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/l;->g:Lcom/google/android/exoplayer2/source/l$a;

    iget-wide v2, v1, Lcom/google/android/exoplayer2/source/l$a;->b:J

    cmp-long v1, p1, v2

    if-nez v1, :cond_0

    .line 438
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/l;->g:Lcom/google/android/exoplayer2/source/l$a;

    iget-object v1, v1, Lcom/google/android/exoplayer2/source/l$a;->e:Lcom/google/android/exoplayer2/source/l$a;

    iput-object v1, p0, Lcom/google/android/exoplayer2/source/l;->g:Lcom/google/android/exoplayer2/source/l$a;

    goto :goto_0

    .line 441
    :cond_1
    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/a/e;Lcom/google/android/exoplayer2/source/k$a;)V
    .locals 12

    .prologue
    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 334
    iget-wide v2, p2, Lcom/google/android/exoplayer2/source/k$a;->b:J

    .line 337
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->e:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/util/k;->a(I)V

    .line 338
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->e:Lcom/google/android/exoplayer2/util/k;

    iget-object v0, v0, Lcom/google/android/exoplayer2/util/k;->a:[B

    invoke-direct {p0, v2, v3, v0, v1}, Lcom/google/android/exoplayer2/source/l;->a(J[BI)V

    .line 339
    const-wide/16 v6, 0x1

    add-long/2addr v2, v6

    .line 340
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->e:Lcom/google/android/exoplayer2/util/k;

    iget-object v0, v0, Lcom/google/android/exoplayer2/util/k;->a:[B

    aget-byte v5, v0, v4

    .line 341
    and-int/lit16 v0, v5, 0x80

    if-eqz v0, :cond_5

    move v0, v1

    .line 342
    :goto_0
    and-int/lit8 v5, v5, 0x7f

    .line 345
    iget-object v6, p1, Lcom/google/android/exoplayer2/a/e;->a:Lcom/google/android/exoplayer2/a/b;

    iget-object v6, v6, Lcom/google/android/exoplayer2/a/b;->a:[B

    if-nez v6, :cond_0

    .line 346
    iget-object v6, p1, Lcom/google/android/exoplayer2/a/e;->a:Lcom/google/android/exoplayer2/a/b;

    const/16 v7, 0x10

    new-array v7, v7, [B

    iput-object v7, v6, Lcom/google/android/exoplayer2/a/b;->a:[B

    .line 348
    :cond_0
    iget-object v6, p1, Lcom/google/android/exoplayer2/a/e;->a:Lcom/google/android/exoplayer2/a/b;

    iget-object v6, v6, Lcom/google/android/exoplayer2/a/b;->a:[B

    invoke-direct {p0, v2, v3, v6, v5}, Lcom/google/android/exoplayer2/source/l;->a(J[BI)V

    .line 349
    int-to-long v6, v5

    add-long/2addr v2, v6

    .line 353
    if-eqz v0, :cond_6

    .line 354
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/l;->e:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v1, v8}, Lcom/google/android/exoplayer2/util/k;->a(I)V

    .line 355
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/l;->e:Lcom/google/android/exoplayer2/util/k;

    iget-object v1, v1, Lcom/google/android/exoplayer2/util/k;->a:[B

    invoke-direct {p0, v2, v3, v1, v8}, Lcom/google/android/exoplayer2/source/l;->a(J[BI)V

    .line 356
    const-wide/16 v6, 0x2

    add-long/2addr v2, v6

    .line 357
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/l;->e:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/k;->h()I

    move-result v1

    move-wide v6, v2

    .line 363
    :goto_1
    iget-object v2, p1, Lcom/google/android/exoplayer2/a/e;->a:Lcom/google/android/exoplayer2/a/b;

    iget-object v2, v2, Lcom/google/android/exoplayer2/a/b;->d:[I

    .line 364
    if-eqz v2, :cond_1

    array-length v3, v2

    if-ge v3, v1, :cond_2

    .line 365
    :cond_1
    new-array v2, v1, [I

    .line 367
    :cond_2
    iget-object v3, p1, Lcom/google/android/exoplayer2/a/e;->a:Lcom/google/android/exoplayer2/a/b;

    iget-object v3, v3, Lcom/google/android/exoplayer2/a/b;->e:[I

    .line 368
    if-eqz v3, :cond_3

    array-length v5, v3

    if-ge v5, v1, :cond_4

    .line 369
    :cond_3
    new-array v3, v1, [I

    .line 371
    :cond_4
    if-eqz v0, :cond_8

    .line 372
    mul-int/lit8 v0, v1, 0x6

    .line 373
    iget-object v5, p0, Lcom/google/android/exoplayer2/source/l;->e:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v5, v0}, Lcom/google/android/exoplayer2/util/k;->a(I)V

    .line 374
    iget-object v5, p0, Lcom/google/android/exoplayer2/source/l;->e:Lcom/google/android/exoplayer2/util/k;

    iget-object v5, v5, Lcom/google/android/exoplayer2/util/k;->a:[B

    invoke-direct {p0, v6, v7, v5, v0}, Lcom/google/android/exoplayer2/source/l;->a(J[BI)V

    .line 375
    int-to-long v8, v0

    add-long/2addr v6, v8

    .line 376
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->e:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v0, v4}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 377
    :goto_2
    if-ge v4, v1, :cond_7

    .line 378
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->e:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/k;->h()I

    move-result v0

    aput v0, v2, v4

    .line 379
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->e:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v0

    aput v0, v3, v4

    .line 377
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_5
    move v0, v4

    .line 341
    goto :goto_0

    :cond_6
    move-wide v6, v2

    .line 359
    goto :goto_1

    :cond_7
    move-wide v10, v6

    .line 387
    :goto_3
    iget-object v8, p2, Lcom/google/android/exoplayer2/source/k$a;->c:Lcom/google/android/exoplayer2/extractor/m$a;

    .line 388
    iget-object v0, p1, Lcom/google/android/exoplayer2/a/e;->a:Lcom/google/android/exoplayer2/a/b;

    iget-object v4, v8, Lcom/google/android/exoplayer2/extractor/m$a;->b:[B

    iget-object v5, p1, Lcom/google/android/exoplayer2/a/e;->a:Lcom/google/android/exoplayer2/a/b;

    iget-object v5, v5, Lcom/google/android/exoplayer2/a/b;->a:[B

    iget v6, v8, Lcom/google/android/exoplayer2/extractor/m$a;->a:I

    iget v7, v8, Lcom/google/android/exoplayer2/extractor/m$a;->c:I

    iget v8, v8, Lcom/google/android/exoplayer2/extractor/m$a;->d:I

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/exoplayer2/a/b;->a(I[I[I[B[BIII)V

    .line 393
    iget-wide v0, p2, Lcom/google/android/exoplayer2/source/k$a;->b:J

    sub-long v0, v10, v0

    long-to-int v0, v0

    .line 394
    iget-wide v2, p2, Lcom/google/android/exoplayer2/source/k$a;->b:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p2, Lcom/google/android/exoplayer2/source/k$a;->b:J

    .line 395
    iget v1, p2, Lcom/google/android/exoplayer2/source/k$a;->a:I

    sub-int v0, v1, v0

    iput v0, p2, Lcom/google/android/exoplayer2/source/k$a;->a:I

    .line 396
    return-void

    .line 382
    :cond_8
    aput v4, v2, v4

    .line 383
    iget v0, p2, Lcom/google/android/exoplayer2/source/k$a;->a:I

    iget-wide v8, p2, Lcom/google/android/exoplayer2/source/k$a;->b:J

    sub-long v8, v6, v8

    long-to-int v5, v8

    sub-int/2addr v0, v5

    aput v0, v3, v4

    move-wide v10, v6

    goto :goto_3
.end method

.method private a(Lcom/google/android/exoplayer2/source/l$a;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 565
    iget-boolean v0, p1, Lcom/google/android/exoplayer2/source/l$a;->c:Z

    if-nez v0, :cond_0

    .line 580
    :goto_0
    return-void

    .line 571
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->h:Lcom/google/android/exoplayer2/source/l$a;

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/source/l$a;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iget-object v2, p0, Lcom/google/android/exoplayer2/source/l;->h:Lcom/google/android/exoplayer2/source/l$a;

    iget-wide v2, v2, Lcom/google/android/exoplayer2/source/l$a;->a:J

    iget-wide v4, p1, Lcom/google/android/exoplayer2/source/l$a;->a:J

    sub-long/2addr v2, v4

    long-to-int v2, v2

    iget v3, p0, Lcom/google/android/exoplayer2/source/l;->b:I

    div-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 573
    new-array v0, v0, [Lcom/google/android/exoplayer2/upstream/a;

    .line 575
    :goto_2
    array-length v2, v0

    if-ge v1, v2, :cond_2

    .line 576
    iget-object v2, p1, Lcom/google/android/exoplayer2/source/l$a;->d:Lcom/google/android/exoplayer2/upstream/a;

    aput-object v2, v0, v1

    .line 577
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/source/l$a;->a()Lcom/google/android/exoplayer2/source/l$a;

    move-result-object p1

    .line 575
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_1
    move v0, v1

    .line 571
    goto :goto_1

    .line 579
    :cond_2
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/l;->a:Lcom/google/android/exoplayer2/upstream/b;

    invoke-interface {v1, v0}, Lcom/google/android/exoplayer2/upstream/b;->a([Lcom/google/android/exoplayer2/upstream/a;)V

    goto :goto_0
.end method

.method private b(I)V
    .locals 4

    .prologue
    .line 604
    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/l;->m:J

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/exoplayer2/source/l;->m:J

    .line 605
    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/l;->m:J

    iget-object v2, p0, Lcom/google/android/exoplayer2/source/l;->h:Lcom/google/android/exoplayer2/source/l$a;

    iget-wide v2, v2, Lcom/google/android/exoplayer2/source/l$a;->b:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 606
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->h:Lcom/google/android/exoplayer2/source/l$a;

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/l$a;->e:Lcom/google/android/exoplayer2/source/l$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/l;->h:Lcom/google/android/exoplayer2/source/l$a;

    .line 608
    :cond_0
    return-void
.end method

.method private b(J)V
    .locals 5

    .prologue
    .line 464
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    .line 476
    :cond_0
    :goto_0
    return-void

    .line 467
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->f:Lcom/google/android/exoplayer2/source/l$a;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/source/l$a;->b:J

    cmp-long v0, p1, v0

    if-ltz v0, :cond_2

    .line 468
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->a:Lcom/google/android/exoplayer2/upstream/b;

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/l;->f:Lcom/google/android/exoplayer2/source/l$a;

    iget-object v1, v1, Lcom/google/android/exoplayer2/source/l$a;->d:Lcom/google/android/exoplayer2/upstream/a;

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/upstream/b;->a(Lcom/google/android/exoplayer2/upstream/a;)V

    .line 469
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->f:Lcom/google/android/exoplayer2/source/l$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/l$a;->a()Lcom/google/android/exoplayer2/source/l$a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/l;->f:Lcom/google/android/exoplayer2/source/l$a;

    goto :goto_1

    .line 473
    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->g:Lcom/google/android/exoplayer2/source/l$a;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/source/l$a;->a:J

    iget-object v2, p0, Lcom/google/android/exoplayer2/source/l;->f:Lcom/google/android/exoplayer2/source/l$a;

    iget-wide v2, v2, Lcom/google/android/exoplayer2/source/l$a;->a:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 474
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->f:Lcom/google/android/exoplayer2/source/l$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/l;->g:Lcom/google/android/exoplayer2/source/l$a;

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/android/exoplayer2/extractor/f;IZ)I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 516
    invoke-direct {p0, p2}, Lcom/google/android/exoplayer2/source/l;->a(I)I

    move-result v1

    .line 517
    iget-object v2, p0, Lcom/google/android/exoplayer2/source/l;->h:Lcom/google/android/exoplayer2/source/l$a;

    iget-object v2, v2, Lcom/google/android/exoplayer2/source/l$a;->d:Lcom/google/android/exoplayer2/upstream/a;

    iget-object v2, v2, Lcom/google/android/exoplayer2/upstream/a;->a:[B

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/l;->h:Lcom/google/android/exoplayer2/source/l$a;

    iget-wide v4, p0, Lcom/google/android/exoplayer2/source/l;->m:J

    .line 518
    invoke-virtual {v3, v4, v5}, Lcom/google/android/exoplayer2/source/l$a;->a(J)I

    move-result v3

    .line 517
    invoke-interface {p1, v2, v3, v1}, Lcom/google/android/exoplayer2/extractor/f;->a([BII)I

    move-result v1

    .line 519
    if-ne v1, v0, :cond_1

    .line 520
    if-eqz p3, :cond_0

    .line 526
    :goto_0
    return v0

    .line 523
    :cond_0
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 525
    :cond_1
    invoke-direct {p0, v1}, Lcom/google/android/exoplayer2/source/l;->b(I)V

    move v0, v1

    .line 526
    goto :goto_0
.end method

.method public a(Lcom/google/android/exoplayer2/k;Lcom/google/android/exoplayer2/a/e;ZZJ)I
    .locals 7

    .prologue
    .line 296
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->c:Lcom/google/android/exoplayer2/source/k;

    iget-object v5, p0, Lcom/google/android/exoplayer2/source/l;->i:Lcom/google/android/exoplayer2/j;

    iget-object v6, p0, Lcom/google/android/exoplayer2/source/l;->d:Lcom/google/android/exoplayer2/source/k$a;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/exoplayer2/source/k;->a(Lcom/google/android/exoplayer2/k;Lcom/google/android/exoplayer2/a/e;ZZLcom/google/android/exoplayer2/j;Lcom/google/android/exoplayer2/source/k$a;)I

    move-result v0

    .line 298
    packed-switch v0, :pswitch_data_0

    .line 319
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 300
    :pswitch_0
    iget-object v0, p1, Lcom/google/android/exoplayer2/k;->a:Lcom/google/android/exoplayer2/j;

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/l;->i:Lcom/google/android/exoplayer2/j;

    .line 301
    const/4 v0, -0x5

    .line 317
    :goto_0
    return v0

    .line 303
    :pswitch_1
    invoke-virtual {p2}, Lcom/google/android/exoplayer2/a/e;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 304
    iget-wide v0, p2, Lcom/google/android/exoplayer2/a/e;->c:J

    cmp-long v0, v0, p5

    if-gez v0, :cond_0

    .line 305
    const/high16 v0, -0x80000000

    invoke-virtual {p2, v0}, Lcom/google/android/exoplayer2/a/e;->b(I)V

    .line 308
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/exoplayer2/a/e;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 309
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->d:Lcom/google/android/exoplayer2/source/k$a;

    invoke-direct {p0, p2, v0}, Lcom/google/android/exoplayer2/source/l;->a(Lcom/google/android/exoplayer2/a/e;Lcom/google/android/exoplayer2/source/k$a;)V

    .line 312
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->d:Lcom/google/android/exoplayer2/source/k$a;

    iget v0, v0, Lcom/google/android/exoplayer2/source/k$a;->a:I

    invoke-virtual {p2, v0}, Lcom/google/android/exoplayer2/a/e;->e(I)V

    .line 313
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->d:Lcom/google/android/exoplayer2/source/k$a;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/source/k$a;->b:J

    iget-object v2, p2, Lcom/google/android/exoplayer2/a/e;->b:Ljava/nio/ByteBuffer;

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/l;->d:Lcom/google/android/exoplayer2/source/k$a;

    iget v3, v3, Lcom/google/android/exoplayer2/source/k$a;->a:I

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/exoplayer2/source/l;->a(JLjava/nio/ByteBuffer;I)V

    .line 315
    :cond_2
    const/4 v0, -0x4

    goto :goto_0

    .line 317
    :pswitch_2
    const/4 v0, -0x3

    goto :goto_0

    .line 298
    nop

    :pswitch_data_0
    .packed-switch -0x5
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a()V
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/source/l;->a(Z)V

    .line 97
    return-void
.end method

.method public a(JIIILcom/google/android/exoplayer2/extractor/m$a;)V
    .locals 9

    .prologue
    .line 543
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/l;->j:Z

    if-eqz v0, :cond_0

    .line 544
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->k:Lcom/google/android/exoplayer2/j;

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/source/l;->a(Lcom/google/android/exoplayer2/j;)V

    .line 546
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/l;->n:Z

    if-eqz v0, :cond_3

    .line 547
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->c:Lcom/google/android/exoplayer2/source/k;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/source/k;->b(J)Z

    move-result v0

    if-nez v0, :cond_2

    .line 555
    :cond_1
    :goto_0
    return-void

    .line 550
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/l;->n:Z

    .line 552
    :cond_3
    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/l;->l:J

    add-long v1, p1, v0

    .line 553
    iget-wide v4, p0, Lcom/google/android/exoplayer2/source/l;->m:J

    int-to-long v6, p4

    sub-long/2addr v4, v6

    int-to-long v6, p5

    sub-long/2addr v4, v6

    .line 554
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->c:Lcom/google/android/exoplayer2/source/k;

    move v3, p3

    move v6, p4

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/exoplayer2/source/k;->a(JIJILcom/google/android/exoplayer2/extractor/m$a;)V

    goto :goto_0
.end method

.method public a(JZZ)V
    .locals 3

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->c:Lcom/google/android/exoplayer2/source/k;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/exoplayer2/source/k;->b(JZZ)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer2/source/l;->b(J)V

    .line 240
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/j;)V
    .locals 3

    .prologue
    .line 504
    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/l;->l:J

    invoke-static {p1, v0, v1}, Lcom/google/android/exoplayer2/source/l;->a(Lcom/google/android/exoplayer2/j;J)Lcom/google/android/exoplayer2/j;

    move-result-object v0

    .line 505
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/l;->c:Lcom/google/android/exoplayer2/source/k;

    invoke-virtual {v1, v0}, Lcom/google/android/exoplayer2/source/k;->a(Lcom/google/android/exoplayer2/j;)Z

    move-result v1

    .line 506
    iput-object p1, p0, Lcom/google/android/exoplayer2/source/l;->k:Lcom/google/android/exoplayer2/j;

    .line 507
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/exoplayer2/source/l;->j:Z

    .line 508
    iget-object v2, p0, Lcom/google/android/exoplayer2/source/l;->o:Lcom/google/android/exoplayer2/source/l$b;

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 509
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/l;->o:Lcom/google/android/exoplayer2/source/l$b;

    invoke-interface {v1, v0}, Lcom/google/android/exoplayer2/source/l$b;->a(Lcom/google/android/exoplayer2/j;)V

    .line 511
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/source/l$b;)V
    .locals 0

    .prologue
    .line 486
    iput-object p1, p0, Lcom/google/android/exoplayer2/source/l;->o:Lcom/google/android/exoplayer2/source/l$b;

    .line 487
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/util/k;I)V
    .locals 6

    .prologue
    .line 531
    :goto_0
    if-lez p2, :cond_0

    .line 532
    invoke-direct {p0, p2}, Lcom/google/android/exoplayer2/source/l;->a(I)I

    move-result v0

    .line 533
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/l;->h:Lcom/google/android/exoplayer2/source/l$a;

    iget-object v1, v1, Lcom/google/android/exoplayer2/source/l$a;->d:Lcom/google/android/exoplayer2/upstream/a;

    iget-object v1, v1, Lcom/google/android/exoplayer2/upstream/a;->a:[B

    iget-object v2, p0, Lcom/google/android/exoplayer2/source/l;->h:Lcom/google/android/exoplayer2/source/l$a;

    iget-wide v4, p0, Lcom/google/android/exoplayer2/source/l;->m:J

    .line 534
    invoke-virtual {v2, v4, v5}, Lcom/google/android/exoplayer2/source/l$a;->a(J)I

    move-result v2

    .line 533
    invoke-virtual {p1, v1, v2, v0}, Lcom/google/android/exoplayer2/util/k;->a([BII)V

    .line 535
    sub-int/2addr p2, v0

    .line 536
    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/source/l;->b(I)V

    goto :goto_0

    .line 538
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 108
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->c:Lcom/google/android/exoplayer2/source/k;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/source/k;->a(Z)V

    .line 109
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->f:Lcom/google/android/exoplayer2/source/l$a;

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/source/l;->a(Lcom/google/android/exoplayer2/source/l$a;)V

    .line 110
    new-instance v0, Lcom/google/android/exoplayer2/source/l$a;

    iget v1, p0, Lcom/google/android/exoplayer2/source/l;->b:I

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/exoplayer2/source/l$a;-><init>(JI)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/l;->f:Lcom/google/android/exoplayer2/source/l$a;

    .line 111
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->f:Lcom/google/android/exoplayer2/source/l$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/l;->g:Lcom/google/android/exoplayer2/source/l$a;

    .line 112
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->f:Lcom/google/android/exoplayer2/source/l$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/l;->h:Lcom/google/android/exoplayer2/source/l$a;

    .line 113
    iput-wide v2, p0, Lcom/google/android/exoplayer2/source/l;->m:J

    .line 114
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->a:Lcom/google/android/exoplayer2/upstream/b;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/upstream/b;->b()V

    .line 115
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->c:Lcom/google/android/exoplayer2/source/k;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/k;->a()I

    move-result v0

    return v0
.end method

.method public b(JZZ)Z
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->c:Lcom/google/android/exoplayer2/source/k;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/exoplayer2/source/k;->a(JZZ)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->c:Lcom/google/android/exoplayer2/source/k;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/k;->c()Z

    move-result v0

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->c:Lcom/google/android/exoplayer2/source/k;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/k;->b()I

    move-result v0

    return v0
.end method

.method public e()Lcom/google/android/exoplayer2/j;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->c:Lcom/google/android/exoplayer2/source/k;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/k;->d()Lcom/google/android/exoplayer2/j;

    move-result-object v0

    return-object v0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->c:Lcom/google/android/exoplayer2/source/k;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/k;->e()J

    move-result-wide v0

    return-wide v0
.end method

.method public g()V
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->c:Lcom/google/android/exoplayer2/source/k;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/k;->f()V

    .line 225
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->f:Lcom/google/android/exoplayer2/source/l$a;

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/l;->g:Lcom/google/android/exoplayer2/source/l$a;

    .line 226
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 246
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->c:Lcom/google/android/exoplayer2/source/k;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/k;->h()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer2/source/l;->b(J)V

    .line 247
    return-void
.end method

.method public i()V
    .locals 2

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->c:Lcom/google/android/exoplayer2/source/k;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/k;->i()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer2/source/l;->b(J)V

    .line 254
    return-void
.end method

.method public j()V
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/l;->c:Lcom/google/android/exoplayer2/source/k;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/k;->g()V

    .line 261
    return-void
.end method
