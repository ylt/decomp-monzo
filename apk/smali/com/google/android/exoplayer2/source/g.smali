.class public final Lcom/google/android/exoplayer2/source/g;
.super Ljava/lang/Object;
.source "LoopingMediaSource.java"

# interfaces
.implements Lcom/google/android/exoplayer2/source/i;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/source/g$a;,
        Lcom/google/android/exoplayer2/source/g$b;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/exoplayer2/source/i;

.field private final b:I

.field private c:I


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/source/i;)V
    .locals 1

    .prologue
    .line 46
    const v0, 0x7fffffff

    invoke-direct {p0, p1, v0}, Lcom/google/android/exoplayer2/source/g;-><init>(Lcom/google/android/exoplayer2/source/i;I)V

    .line 47
    return-void
.end method

.method public constructor <init>(Lcom/google/android/exoplayer2/source/i;I)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    if-lez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/a;->a(Z)V

    .line 57
    iput-object p1, p0, Lcom/google/android/exoplayer2/source/g;->a:Lcom/google/android/exoplayer2/source/i;

    .line 58
    iput p2, p0, Lcom/google/android/exoplayer2/source/g;->b:I

    .line 59
    return-void

    .line 56
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/source/g;)I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/google/android/exoplayer2/source/g;->b:I

    return v0
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/source/g;I)I
    .locals 0

    .prologue
    .line 32
    iput p1, p0, Lcom/google/android/exoplayer2/source/g;->c:I

    return p1
.end method


# virtual methods
.method public a(Lcom/google/android/exoplayer2/source/i$b;Lcom/google/android/exoplayer2/upstream/b;)Lcom/google/android/exoplayer2/source/h;
    .locals 4

    .prologue
    .line 81
    iget v0, p0, Lcom/google/android/exoplayer2/source/g;->b:I

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/g;->a:Lcom/google/android/exoplayer2/source/i;

    new-instance v1, Lcom/google/android/exoplayer2/source/i$b;

    iget v2, p1, Lcom/google/android/exoplayer2/source/i$b;->b:I

    iget v3, p0, Lcom/google/android/exoplayer2/source/g;->c:I

    rem-int/2addr v2, v3

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/source/i$b;-><init>(I)V

    .line 82
    invoke-interface {v0, v1, p2}, Lcom/google/android/exoplayer2/source/i;->a(Lcom/google/android/exoplayer2/source/i$b;Lcom/google/android/exoplayer2/upstream/b;)Lcom/google/android/exoplayer2/source/h;

    move-result-object v0

    .line 83
    :goto_0
    return-object v0

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/g;->a:Lcom/google/android/exoplayer2/source/i;

    .line 83
    invoke-interface {v0, p1, p2}, Lcom/google/android/exoplayer2/source/i;->a(Lcom/google/android/exoplayer2/source/i$b;Lcom/google/android/exoplayer2/upstream/b;)Lcom/google/android/exoplayer2/source/h;

    move-result-object v0

    goto :goto_0
.end method

.method public a()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/g;->a:Lcom/google/android/exoplayer2/source/i;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/source/i;->a()V

    .line 77
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/e;ZLcom/google/android/exoplayer2/source/i$a;)V
    .locals 3

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/g;->a:Lcom/google/android/exoplayer2/source/i;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/exoplayer2/source/g$1;

    invoke-direct {v2, p0, p3}, Lcom/google/android/exoplayer2/source/g$1;-><init>(Lcom/google/android/exoplayer2/source/g;Lcom/google/android/exoplayer2/source/i$a;)V

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/exoplayer2/source/i;->a(Lcom/google/android/exoplayer2/e;ZLcom/google/android/exoplayer2/source/i$a;)V

    .line 72
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/source/h;)V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/g;->a:Lcom/google/android/exoplayer2/source/i;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/source/i;->a(Lcom/google/android/exoplayer2/source/h;)V

    .line 89
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/g;->a:Lcom/google/android/exoplayer2/source/i;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/source/i;->b()V

    .line 94
    return-void
.end method
