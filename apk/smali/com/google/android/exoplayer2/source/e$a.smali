.class final Lcom/google/android/exoplayer2/source/e$a;
.super Ljava/lang/Object;
.source "ExtractorMediaPeriod.java"

# interfaces
.implements Lcom/google/android/exoplayer2/upstream/Loader$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/source/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/exoplayer2/source/e;

.field private final b:Landroid/net/Uri;

.field private final c:Lcom/google/android/exoplayer2/upstream/c;

.field private final d:Lcom/google/android/exoplayer2/source/e$b;

.field private final e:Lcom/google/android/exoplayer2/util/d;

.field private final f:Lcom/google/android/exoplayer2/extractor/k;

.field private volatile g:Z

.field private h:Z

.field private i:J

.field private j:J


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/source/e;Landroid/net/Uri;Lcom/google/android/exoplayer2/upstream/c;Lcom/google/android/exoplayer2/source/e$b;Lcom/google/android/exoplayer2/util/d;)V
    .locals 2

    .prologue
    .line 659
    iput-object p1, p0, Lcom/google/android/exoplayer2/source/e$a;->a:Lcom/google/android/exoplayer2/source/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 660
    invoke-static {p2}, Lcom/google/android/exoplayer2/util/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/e$a;->b:Landroid/net/Uri;

    .line 661
    invoke-static {p3}, Lcom/google/android/exoplayer2/util/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/upstream/c;

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/e$a;->c:Lcom/google/android/exoplayer2/upstream/c;

    .line 662
    invoke-static {p4}, Lcom/google/android/exoplayer2/util/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/source/e$b;

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/e$a;->d:Lcom/google/android/exoplayer2/source/e$b;

    .line 663
    iput-object p5, p0, Lcom/google/android/exoplayer2/source/e$a;->e:Lcom/google/android/exoplayer2/util/d;

    .line 664
    new-instance v0, Lcom/google/android/exoplayer2/extractor/k;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/extractor/k;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/e$a;->f:Lcom/google/android/exoplayer2/extractor/k;

    .line 665
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/e$a;->h:Z

    .line 666
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/exoplayer2/source/e$a;->j:J

    .line 667
    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/source/e$a;)J
    .locals 2

    .prologue
    .line 644
    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/e$a;->j:J

    return-wide v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 677
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/e$a;->g:Z

    .line 678
    return-void
.end method

.method public a(JJ)V
    .locals 1

    .prologue
    .line 670
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e$a;->f:Lcom/google/android/exoplayer2/extractor/k;

    iput-wide p1, v0, Lcom/google/android/exoplayer2/extractor/k;->a:J

    .line 671
    iput-wide p3, p0, Lcom/google/android/exoplayer2/source/e$a;->i:J

    .line 672
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/e$a;->h:Z

    .line 673
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 682
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/e$a;->g:Z

    return v0
.end method

.method public c()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 687
    const/4 v8, 0x0

    .line 688
    :goto_0
    if-nez v8, :cond_7

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/e$a;->g:Z

    if-nez v0, :cond_7

    .line 689
    const/4 v7, 0x0

    .line 691
    :try_start_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e$a;->f:Lcom/google/android/exoplayer2/extractor/k;

    iget-wide v2, v0, Lcom/google/android/exoplayer2/extractor/k;->a:J

    .line 692
    iget-object v9, p0, Lcom/google/android/exoplayer2/source/e$a;->c:Lcom/google/android/exoplayer2/upstream/c;

    new-instance v0, Lcom/google/android/exoplayer2/upstream/e;

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/e$a;->b:Landroid/net/Uri;

    const-wide/16 v4, -0x1

    iget-object v6, p0, Lcom/google/android/exoplayer2/source/e$a;->a:Lcom/google/android/exoplayer2/source/e;

    invoke-static {v6}, Lcom/google/android/exoplayer2/source/e;->e(Lcom/google/android/exoplayer2/source/e;)Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer2/upstream/e;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    invoke-interface {v9, v0}, Lcom/google/android/exoplayer2/upstream/c;->a(Lcom/google/android/exoplayer2/upstream/e;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/source/e$a;->j:J

    .line 693
    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/e$a;->j:J

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 694
    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/e$a;->j:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/exoplayer2/source/e$a;->j:J

    .line 696
    :cond_0
    new-instance v0, Lcom/google/android/exoplayer2/extractor/b;

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/e$a;->c:Lcom/google/android/exoplayer2/upstream/c;

    iget-wide v4, p0, Lcom/google/android/exoplayer2/source/e$a;->j:J

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/extractor/b;-><init>(Lcom/google/android/exoplayer2/upstream/c;JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 697
    :try_start_1
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/e$a;->d:Lcom/google/android/exoplayer2/source/e$b;

    iget-object v4, p0, Lcom/google/android/exoplayer2/source/e$a;->c:Lcom/google/android/exoplayer2/upstream/c;

    invoke-interface {v4}, Lcom/google/android/exoplayer2/upstream/c;->a()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v1, v0, v4}, Lcom/google/android/exoplayer2/source/e$b;->a(Lcom/google/android/exoplayer2/extractor/f;Landroid/net/Uri;)Lcom/google/android/exoplayer2/extractor/e;

    move-result-object v6

    .line 698
    iget-boolean v1, p0, Lcom/google/android/exoplayer2/source/e$a;->h:Z

    if-eqz v1, :cond_1

    .line 699
    iget-wide v4, p0, Lcom/google/android/exoplayer2/source/e$a;->i:J

    invoke-interface {v6, v2, v3, v4, v5}, Lcom/google/android/exoplayer2/extractor/e;->a(JJ)V

    .line 700
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/exoplayer2/source/e$a;->h:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_1
    move-wide v4, v2

    move v1, v8

    .line 702
    :goto_1
    if-nez v1, :cond_2

    :try_start_2
    iget-boolean v2, p0, Lcom/google/android/exoplayer2/source/e$a;->g:Z

    if-nez v2, :cond_2

    .line 703
    iget-object v2, p0, Lcom/google/android/exoplayer2/source/e$a;->e:Lcom/google/android/exoplayer2/util/d;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/util/d;->c()V

    .line 704
    iget-object v2, p0, Lcom/google/android/exoplayer2/source/e$a;->f:Lcom/google/android/exoplayer2/extractor/k;

    invoke-interface {v6, v0, v2}, Lcom/google/android/exoplayer2/extractor/e;->a(Lcom/google/android/exoplayer2/extractor/f;Lcom/google/android/exoplayer2/extractor/k;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result v2

    .line 705
    :try_start_3
    invoke-interface {v0}, Lcom/google/android/exoplayer2/extractor/f;->c()J

    move-result-wide v8

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/e$a;->a:Lcom/google/android/exoplayer2/source/e;

    invoke-static {v1}, Lcom/google/android/exoplayer2/source/e;->f(Lcom/google/android/exoplayer2/source/e;)J

    move-result-wide v10

    add-long/2addr v10, v4

    cmp-long v1, v8, v10

    if-lez v1, :cond_8

    .line 706
    invoke-interface {v0}, Lcom/google/android/exoplayer2/extractor/f;->c()J

    move-result-wide v4

    .line 707
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/e$a;->e:Lcom/google/android/exoplayer2/util/d;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/d;->b()Z

    .line 708
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/e$a;->a:Lcom/google/android/exoplayer2/source/e;

    invoke-static {v1}, Lcom/google/android/exoplayer2/source/e;->h(Lcom/google/android/exoplayer2/source/e;)Landroid/os/Handler;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/e$a;->a:Lcom/google/android/exoplayer2/source/e;

    invoke-static {v3}, Lcom/google/android/exoplayer2/source/e;->g(Lcom/google/android/exoplayer2/source/e;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move v1, v2

    goto :goto_1

    .line 712
    :cond_2
    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 713
    const/4 v0, 0x0

    .line 717
    :goto_2
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/e$a;->c:Lcom/google/android/exoplayer2/upstream/c;

    invoke-static {v1}, Lcom/google/android/exoplayer2/util/s;->a(Lcom/google/android/exoplayer2/upstream/c;)V

    move v8, v0

    .line 719
    goto/16 :goto_0

    .line 714
    :cond_3
    if-eqz v0, :cond_4

    .line 715
    iget-object v2, p0, Lcom/google/android/exoplayer2/source/e$a;->f:Lcom/google/android/exoplayer2/extractor/k;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/extractor/f;->c()J

    move-result-wide v4

    iput-wide v4, v2, Lcom/google/android/exoplayer2/extractor/k;->a:J

    :cond_4
    move v0, v1

    goto :goto_2

    .line 712
    :catchall_0
    move-exception v0

    move-object v1, v7

    move v2, v8

    :goto_3
    const/4 v3, 0x1

    if-ne v2, v3, :cond_6

    .line 717
    :cond_5
    :goto_4
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/e$a;->c:Lcom/google/android/exoplayer2/upstream/c;

    invoke-static {v1}, Lcom/google/android/exoplayer2/util/s;->a(Lcom/google/android/exoplayer2/upstream/c;)V

    throw v0

    .line 714
    :cond_6
    if-eqz v1, :cond_5

    .line 715
    iget-object v2, p0, Lcom/google/android/exoplayer2/source/e$a;->f:Lcom/google/android/exoplayer2/extractor/k;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/extractor/f;->c()J

    move-result-wide v4

    iput-wide v4, v2, Lcom/google/android/exoplayer2/extractor/k;->a:J

    goto :goto_4

    .line 720
    :cond_7
    return-void

    .line 712
    :catchall_1
    move-exception v1

    move v2, v8

    move-object v12, v0

    move-object v0, v1

    move-object v1, v12

    goto :goto_3

    :catchall_2
    move-exception v2

    move-object v12, v2

    move v2, v1

    move-object v1, v0

    move-object v0, v12

    goto :goto_3

    :catchall_3
    move-exception v1

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    goto :goto_3

    :cond_8
    move v1, v2

    goto :goto_1
.end method
