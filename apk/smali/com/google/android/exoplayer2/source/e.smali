.class final Lcom/google/android/exoplayer2/source/e;
.super Ljava/lang/Object;
.source "ExtractorMediaPeriod.java"

# interfaces
.implements Lcom/google/android/exoplayer2/extractor/g;
.implements Lcom/google/android/exoplayer2/source/h;
.implements Lcom/google/android/exoplayer2/source/l$b;
.implements Lcom/google/android/exoplayer2/upstream/Loader$a;
.implements Lcom/google/android/exoplayer2/upstream/Loader$d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/source/e$b;,
        Lcom/google/android/exoplayer2/source/e$a;,
        Lcom/google/android/exoplayer2/source/e$d;,
        Lcom/google/android/exoplayer2/source/e$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/exoplayer2/extractor/g;",
        "Lcom/google/android/exoplayer2/source/h;",
        "Lcom/google/android/exoplayer2/source/l$b;",
        "Lcom/google/android/exoplayer2/upstream/Loader$a",
        "<",
        "Lcom/google/android/exoplayer2/source/e$a;",
        ">;",
        "Lcom/google/android/exoplayer2/upstream/Loader$d;"
    }
.end annotation


# instance fields
.field private A:[Z

.field private B:[Z

.field private C:Z

.field private D:J

.field private E:J

.field private F:J

.field private G:I

.field private H:Z

.field private I:Z

.field private final a:Landroid/net/Uri;

.field private final b:Lcom/google/android/exoplayer2/upstream/c;

.field private final c:I

.field private final d:Landroid/os/Handler;

.field private final e:Lcom/google/android/exoplayer2/source/f$a;

.field private final f:Lcom/google/android/exoplayer2/source/e$c;

.field private final g:Lcom/google/android/exoplayer2/upstream/b;

.field private final h:Ljava/lang/String;

.field private final i:J

.field private final j:Lcom/google/android/exoplayer2/upstream/Loader;

.field private final k:Lcom/google/android/exoplayer2/source/e$b;

.field private final l:Lcom/google/android/exoplayer2/util/d;

.field private final m:Ljava/lang/Runnable;

.field private final n:Ljava/lang/Runnable;

.field private final o:Landroid/os/Handler;

.field private p:Lcom/google/android/exoplayer2/source/h$a;

.field private q:Lcom/google/android/exoplayer2/extractor/l;

.field private r:[Lcom/google/android/exoplayer2/source/l;

.field private s:[I

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:I

.field private y:Lcom/google/android/exoplayer2/source/q;

.field private z:J


# direct methods
.method public constructor <init>(Landroid/net/Uri;Lcom/google/android/exoplayer2/upstream/c;[Lcom/google/android/exoplayer2/extractor/e;ILandroid/os/Handler;Lcom/google/android/exoplayer2/source/f$a;Lcom/google/android/exoplayer2/source/e$c;Lcom/google/android/exoplayer2/upstream/b;Ljava/lang/String;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    iput-object p1, p0, Lcom/google/android/exoplayer2/source/e;->a:Landroid/net/Uri;

    .line 133
    iput-object p2, p0, Lcom/google/android/exoplayer2/source/e;->b:Lcom/google/android/exoplayer2/upstream/c;

    .line 134
    iput p4, p0, Lcom/google/android/exoplayer2/source/e;->c:I

    .line 135
    iput-object p5, p0, Lcom/google/android/exoplayer2/source/e;->d:Landroid/os/Handler;

    .line 136
    iput-object p6, p0, Lcom/google/android/exoplayer2/source/e;->e:Lcom/google/android/exoplayer2/source/f$a;

    .line 137
    iput-object p7, p0, Lcom/google/android/exoplayer2/source/e;->f:Lcom/google/android/exoplayer2/source/e$c;

    .line 138
    iput-object p8, p0, Lcom/google/android/exoplayer2/source/e;->g:Lcom/google/android/exoplayer2/upstream/b;

    .line 139
    iput-object p9, p0, Lcom/google/android/exoplayer2/source/e;->h:Ljava/lang/String;

    .line 140
    int-to-long v0, p10

    iput-wide v0, p0, Lcom/google/android/exoplayer2/source/e;->i:J

    .line 141
    new-instance v0, Lcom/google/android/exoplayer2/upstream/Loader;

    const-string v1, "Loader:ExtractorMediaPeriod"

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/upstream/Loader;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/e;->j:Lcom/google/android/exoplayer2/upstream/Loader;

    .line 142
    new-instance v0, Lcom/google/android/exoplayer2/source/e$b;

    invoke-direct {v0, p3, p0}, Lcom/google/android/exoplayer2/source/e$b;-><init>([Lcom/google/android/exoplayer2/extractor/e;Lcom/google/android/exoplayer2/extractor/g;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/e;->k:Lcom/google/android/exoplayer2/source/e$b;

    .line 143
    new-instance v0, Lcom/google/android/exoplayer2/util/d;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/util/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/e;->l:Lcom/google/android/exoplayer2/util/d;

    .line 144
    new-instance v0, Lcom/google/android/exoplayer2/source/e$1;

    invoke-direct {v0, p0}, Lcom/google/android/exoplayer2/source/e$1;-><init>(Lcom/google/android/exoplayer2/source/e;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/e;->m:Ljava/lang/Runnable;

    .line 150
    new-instance v0, Lcom/google/android/exoplayer2/source/e$2;

    invoke-direct {v0, p0}, Lcom/google/android/exoplayer2/source/e$2;-><init>(Lcom/google/android/exoplayer2/source/e;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/e;->n:Ljava/lang/Runnable;

    .line 158
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/e;->o:Landroid/os/Handler;

    .line 159
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/e;->s:[I

    .line 160
    new-array v0, v2, [Lcom/google/android/exoplayer2/source/l;

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/e;->r:[Lcom/google/android/exoplayer2/source/l;

    .line 161
    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v0, p0, Lcom/google/android/exoplayer2/source/e;->F:J

    .line 162
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/exoplayer2/source/e;->D:J

    .line 163
    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/source/e$a;)V
    .locals 4

    .prologue
    .line 499
    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/e;->D:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 500
    invoke-static {p1}, Lcom/google/android/exoplayer2/source/e$a;->a(Lcom/google/android/exoplayer2/source/e$a;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/source/e;->D:J

    .line 502
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/source/e;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/e;->i()V

    return-void
.end method

.method private a(Ljava/io/IOException;)Z
    .locals 1

    .prologue
    .line 596
    instance-of v0, p1, Lcom/google/android/exoplayer2/source/UnrecognizedInputFormatException;

    return v0
.end method

.method private b(Lcom/google/android/exoplayer2/source/e$a;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 531
    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/e;->D:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->q:Lcom/google/android/exoplayer2/extractor/l;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->q:Lcom/google/android/exoplayer2/extractor/l;

    .line 532
    invoke-interface {v0}, Lcom/google/android/exoplayer2/extractor/l;->b()J

    move-result-wide v0

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 548
    :cond_0
    :goto_0
    return-void

    .line 541
    :cond_1
    iput-wide v4, p0, Lcom/google/android/exoplayer2/source/e;->E:J

    .line 542
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/e;->u:Z

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/e;->w:Z

    .line 543
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/e;->r:[Lcom/google/android/exoplayer2/source/l;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 544
    invoke-virtual {v3}, Lcom/google/android/exoplayer2/source/l;->a()V

    .line 543
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 546
    :cond_2
    invoke-virtual {p1, v4, v5, v4, v5}, Lcom/google/android/exoplayer2/source/e$a;->a(JJ)V

    goto :goto_0
.end method

.method private b(Ljava/io/IOException;)V
    .locals 2

    .prologue
    .line 600
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->d:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->e:Lcom/google/android/exoplayer2/source/f$a;

    if-eqz v0, :cond_0

    .line 601
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->d:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/exoplayer2/source/e$3;

    invoke-direct {v1, p0, p1}, Lcom/google/android/exoplayer2/source/e$3;-><init>(Lcom/google/android/exoplayer2/source/e;Ljava/io/IOException;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 608
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/exoplayer2/source/e;)Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/e;->I:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/exoplayer2/source/e;)Lcom/google/android/exoplayer2/source/h$a;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->p:Lcom/google/android/exoplayer2/source/h$a;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/exoplayer2/source/e;)Lcom/google/android/exoplayer2/source/f$a;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->e:Lcom/google/android/exoplayer2/source/f$a;

    return-object v0
.end method

.method private d(J)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 557
    iget-object v2, p0, Lcom/google/android/exoplayer2/source/e;->r:[Lcom/google/android/exoplayer2/source/l;

    array-length v3, v2

    move v2, v0

    .line 558
    :goto_0
    if-ge v2, v3, :cond_2

    .line 559
    iget-object v4, p0, Lcom/google/android/exoplayer2/source/e;->r:[Lcom/google/android/exoplayer2/source/l;

    aget-object v4, v4, v2

    .line 560
    invoke-virtual {v4}, Lcom/google/android/exoplayer2/source/l;->g()V

    .line 561
    invoke-virtual {v4, p1, p2, v1, v0}, Lcom/google/android/exoplayer2/source/l;->b(JZZ)Z

    move-result v5

    .line 566
    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/google/android/exoplayer2/source/e;->B:[Z

    aget-boolean v5, v5, v2

    if-nez v5, :cond_0

    iget-boolean v5, p0, Lcom/google/android/exoplayer2/source/e;->C:Z

    if-nez v5, :cond_1

    .line 571
    :cond_0
    :goto_1
    return v0

    .line 569
    :cond_1
    invoke-virtual {v4}, Lcom/google/android/exoplayer2/source/l;->h()V

    .line 558
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 571
    goto :goto_1
.end method

.method static synthetic e(Lcom/google/android/exoplayer2/source/e;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/exoplayer2/source/e;)J
    .locals 2

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/e;->i:J

    return-wide v0
.end method

.method static synthetic g(Lcom/google/android/exoplayer2/source/e;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->n:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/exoplayer2/source/e;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->o:Landroid/os/Handler;

    return-object v0
.end method

.method private i()V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 470
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/e;->I:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/e;->u:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->q:Lcom/google/android/exoplayer2/extractor/l;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/e;->t:Z

    if-nez v0, :cond_1

    .line 496
    :cond_0
    :goto_0
    return-void

    .line 473
    :cond_1
    iget-object v3, p0, Lcom/google/android/exoplayer2/source/e;->r:[Lcom/google/android/exoplayer2/source/l;

    array-length v4, v3

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_2

    aget-object v5, v3, v0

    .line 474
    invoke-virtual {v5}, Lcom/google/android/exoplayer2/source/l;->e()Lcom/google/android/exoplayer2/j;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 473
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 478
    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->l:Lcom/google/android/exoplayer2/util/d;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/d;->b()Z

    .line 479
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->r:[Lcom/google/android/exoplayer2/source/l;

    array-length v4, v0

    .line 480
    new-array v5, v4, [Lcom/google/android/exoplayer2/source/p;

    .line 481
    new-array v0, v4, [Z

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/e;->B:[Z

    .line 482
    new-array v0, v4, [Z

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/e;->A:[Z

    .line 483
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->q:Lcom/google/android/exoplayer2/extractor/l;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/extractor/l;->b()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/exoplayer2/source/e;->z:J

    move v3, v1

    .line 484
    :goto_2
    if-ge v3, v4, :cond_5

    .line 485
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->r:[Lcom/google/android/exoplayer2/source/l;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/l;->e()Lcom/google/android/exoplayer2/j;

    move-result-object v0

    .line 486
    new-instance v6, Lcom/google/android/exoplayer2/source/p;

    new-array v7, v2, [Lcom/google/android/exoplayer2/j;

    aput-object v0, v7, v1

    invoke-direct {v6, v7}, Lcom/google/android/exoplayer2/source/p;-><init>([Lcom/google/android/exoplayer2/j;)V

    aput-object v6, v5, v3

    .line 487
    iget-object v0, v0, Lcom/google/android/exoplayer2/j;->f:Ljava/lang/String;

    .line 488
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/h;->b(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/h;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    move v0, v2

    .line 489
    :goto_3
    iget-object v6, p0, Lcom/google/android/exoplayer2/source/e;->B:[Z

    aput-boolean v0, v6, v3

    .line 490
    iget-boolean v6, p0, Lcom/google/android/exoplayer2/source/e;->C:Z

    or-int/2addr v0, v6

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/e;->C:Z

    .line 484
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_4
    move v0, v1

    .line 488
    goto :goto_3

    .line 492
    :cond_5
    new-instance v0, Lcom/google/android/exoplayer2/source/q;

    invoke-direct {v0, v5}, Lcom/google/android/exoplayer2/source/q;-><init>([Lcom/google/android/exoplayer2/source/p;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/e;->y:Lcom/google/android/exoplayer2/source/q;

    .line 493
    iput-boolean v2, p0, Lcom/google/android/exoplayer2/source/e;->u:Z

    .line 494
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->f:Lcom/google/android/exoplayer2/source/e$c;

    iget-wide v2, p0, Lcom/google/android/exoplayer2/source/e;->z:J

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/e;->q:Lcom/google/android/exoplayer2/extractor/l;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/extractor/l;->a()Z

    move-result v1

    invoke-interface {v0, v2, v3, v1}, Lcom/google/android/exoplayer2/source/e$c;->a(JZ)V

    .line 495
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->p:Lcom/google/android/exoplayer2/source/h$a;

    invoke-interface {v0, p0}, Lcom/google/android/exoplayer2/source/h$a;->a(Lcom/google/android/exoplayer2/source/h;)V

    goto :goto_0
.end method

.method private j()V
    .locals 8

    .prologue
    const-wide v6, -0x7fffffffffffffffL    # -4.9E-324

    .line 505
    new-instance v0, Lcom/google/android/exoplayer2/source/e$a;

    iget-object v2, p0, Lcom/google/android/exoplayer2/source/e;->a:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/e;->b:Lcom/google/android/exoplayer2/upstream/c;

    iget-object v4, p0, Lcom/google/android/exoplayer2/source/e;->k:Lcom/google/android/exoplayer2/source/e$b;

    iget-object v5, p0, Lcom/google/android/exoplayer2/source/e;->l:Lcom/google/android/exoplayer2/util/d;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/source/e$a;-><init>(Lcom/google/android/exoplayer2/source/e;Landroid/net/Uri;Lcom/google/android/exoplayer2/upstream/c;Lcom/google/android/exoplayer2/source/e$b;Lcom/google/android/exoplayer2/util/d;)V

    .line 507
    iget-boolean v1, p0, Lcom/google/android/exoplayer2/source/e;->u:Z

    if-eqz v1, :cond_1

    .line 508
    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/e;->m()Z

    move-result v1

    invoke-static {v1}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 509
    iget-wide v2, p0, Lcom/google/android/exoplayer2/source/e;->z:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/exoplayer2/source/e;->F:J

    iget-wide v4, p0, Lcom/google/android/exoplayer2/source/e;->z:J

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 510
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/e;->H:Z

    .line 511
    iput-wide v6, p0, Lcom/google/android/exoplayer2/source/e;->F:J

    .line 528
    :goto_0
    return-void

    .line 514
    :cond_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/e;->q:Lcom/google/android/exoplayer2/extractor/l;

    iget-wide v2, p0, Lcom/google/android/exoplayer2/source/e;->F:J

    invoke-interface {v1, v2, v3}, Lcom/google/android/exoplayer2/extractor/l;->b(J)J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/exoplayer2/source/e;->F:J

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/google/android/exoplayer2/source/e$a;->a(JJ)V

    .line 515
    iput-wide v6, p0, Lcom/google/android/exoplayer2/source/e;->F:J

    .line 517
    :cond_1
    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/e;->k()I

    move-result v1

    iput v1, p0, Lcom/google/android/exoplayer2/source/e;->G:I

    .line 519
    iget v1, p0, Lcom/google/android/exoplayer2/source/e;->c:I

    .line 520
    const/4 v2, -0x1

    if-ne v1, v2, :cond_3

    .line 522
    iget-boolean v1, p0, Lcom/google/android/exoplayer2/source/e;->u:Z

    if-eqz v1, :cond_2

    iget-wide v2, p0, Lcom/google/android/exoplayer2/source/e;->D:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/e;->q:Lcom/google/android/exoplayer2/extractor/l;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/e;->q:Lcom/google/android/exoplayer2/extractor/l;

    .line 523
    invoke-interface {v1}, Lcom/google/android/exoplayer2/extractor/l;->b()J

    move-result-wide v2

    cmp-long v1, v2, v6

    if-eqz v1, :cond_4

    :cond_2
    const/4 v1, 0x3

    .line 527
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/google/android/exoplayer2/source/e;->j:Lcom/google/android/exoplayer2/upstream/Loader;

    invoke-virtual {v2, v0, p0, v1}, Lcom/google/android/exoplayer2/upstream/Loader;->a(Lcom/google/android/exoplayer2/upstream/Loader$c;Lcom/google/android/exoplayer2/upstream/Loader$a;I)J

    goto :goto_0

    .line 523
    :cond_4
    const/4 v1, 0x6

    goto :goto_1
.end method

.method private k()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 575
    .line 576
    iget-object v2, p0, Lcom/google/android/exoplayer2/source/e;->r:[Lcom/google/android/exoplayer2/source/l;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 577
    invoke-virtual {v4}, Lcom/google/android/exoplayer2/source/l;->b()I

    move-result v4

    add-int/2addr v1, v4

    .line 576
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 579
    :cond_0
    return v1
.end method

.method private l()J
    .locals 8

    .prologue
    .line 583
    const-wide/high16 v2, -0x8000000000000000L

    .line 584
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/e;->r:[Lcom/google/android/exoplayer2/source/l;

    array-length v4, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v1, v0

    .line 586
    invoke-virtual {v5}, Lcom/google/android/exoplayer2/source/l;->f()J

    move-result-wide v6

    .line 585
    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 584
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 588
    :cond_0
    return-wide v2
.end method

.method private m()Z
    .locals 4

    .prologue
    .line 592
    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/e;->F:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a(ILcom/google/android/exoplayer2/k;Lcom/google/android/exoplayer2/a/e;Z)I
    .locals 8

    .prologue
    .line 367
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/e;->w:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/e;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 368
    :cond_0
    const/4 v0, -0x3

    .line 370
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->r:[Lcom/google/android/exoplayer2/source/l;

    aget-object v1, v0, p1

    iget-boolean v5, p0, Lcom/google/android/exoplayer2/source/e;->H:Z

    iget-wide v6, p0, Lcom/google/android/exoplayer2/source/e;->E:J

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/exoplayer2/source/l;->a(Lcom/google/android/exoplayer2/k;Lcom/google/android/exoplayer2/a/e;ZZJ)I

    move-result v0

    goto :goto_0
.end method

.method public a(Lcom/google/android/exoplayer2/source/e$a;JJLjava/io/IOException;)I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 417
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/source/e;->a(Lcom/google/android/exoplayer2/source/e$a;)V

    .line 418
    invoke-direct {p0, p6}, Lcom/google/android/exoplayer2/source/e;->b(Ljava/io/IOException;)V

    .line 419
    invoke-direct {p0, p6}, Lcom/google/android/exoplayer2/source/e;->a(Ljava/io/IOException;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 420
    const/4 v1, 0x3

    .line 426
    :cond_0
    :goto_0
    return v1

    .line 422
    :cond_1
    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/e;->k()I

    move-result v0

    .line 423
    iget v3, p0, Lcom/google/android/exoplayer2/source/e;->G:I

    if-le v0, v3, :cond_2

    move v0, v1

    .line 424
    :goto_1
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/source/e;->b(Lcom/google/android/exoplayer2/source/e$a;)V

    .line 425
    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/e;->k()I

    move-result v3

    iput v3, p0, Lcom/google/android/exoplayer2/source/e;->G:I

    .line 426
    if-nez v0, :cond_0

    move v1, v2

    goto :goto_0

    :cond_2
    move v0, v2

    .line 423
    goto :goto_1
.end method

.method public bridge synthetic a(Lcom/google/android/exoplayer2/upstream/Loader$c;JJLjava/io/IOException;)I
    .locals 8

    .prologue
    .line 49
    move-object v1, p1

    check-cast v1, Lcom/google/android/exoplayer2/source/e$a;

    move-object v0, p0

    move-wide v2, p2

    move-wide v4, p4

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/exoplayer2/source/e;->a(Lcom/google/android/exoplayer2/source/e$a;JJLjava/io/IOException;)I

    move-result v0

    return v0
.end method

.method public a([Lcom/google/android/exoplayer2/b/e;[Z[Lcom/google/android/exoplayer2/source/m;[ZJ)J
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 206
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/e;->u:Z

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 207
    iget v4, p0, Lcom/google/android/exoplayer2/source/e;->x:I

    move v1, v2

    .line 209
    :goto_0
    array-length v0, p1

    if-ge v1, v0, :cond_2

    .line 210
    aget-object v0, p3, v1

    if-eqz v0, :cond_1

    aget-object v0, p1, v1

    if-eqz v0, :cond_0

    aget-boolean v0, p2, v1

    if-nez v0, :cond_1

    .line 211
    :cond_0
    aget-object v0, p3, v1

    check-cast v0, Lcom/google/android/exoplayer2/source/e$d;

    invoke-static {v0}, Lcom/google/android/exoplayer2/source/e$d;->a(Lcom/google/android/exoplayer2/source/e$d;)I

    move-result v0

    .line 212
    iget-object v5, p0, Lcom/google/android/exoplayer2/source/e;->A:[Z

    aget-boolean v5, v5, v0

    invoke-static {v5}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 213
    iget v5, p0, Lcom/google/android/exoplayer2/source/e;->x:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lcom/google/android/exoplayer2/source/e;->x:I

    .line 214
    iget-object v5, p0, Lcom/google/android/exoplayer2/source/e;->A:[Z

    aput-boolean v2, v5, v0

    .line 215
    const/4 v0, 0x0

    aput-object v0, p3, v1

    .line 209
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 220
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/e;->v:Z

    if-eqz v0, :cond_5

    if-nez v4, :cond_4

    move v0, v3

    :goto_1
    move v1, v0

    move v0, v2

    .line 222
    :goto_2
    array-length v4, p1

    if-ge v0, v4, :cond_b

    .line 223
    aget-object v4, p3, v0

    if-nez v4, :cond_3

    aget-object v4, p1, v0

    if-eqz v4, :cond_3

    .line 224
    aget-object v5, p1, v0

    .line 225
    invoke-interface {v5}, Lcom/google/android/exoplayer2/b/e;->b()I

    move-result v4

    if-ne v4, v3, :cond_7

    move v4, v3

    :goto_3
    invoke-static {v4}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 226
    invoke-interface {v5, v2}, Lcom/google/android/exoplayer2/b/e;->b(I)I

    move-result v4

    if-nez v4, :cond_8

    move v4, v3

    :goto_4
    invoke-static {v4}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 227
    iget-object v4, p0, Lcom/google/android/exoplayer2/source/e;->y:Lcom/google/android/exoplayer2/source/q;

    invoke-interface {v5}, Lcom/google/android/exoplayer2/b/e;->a()Lcom/google/android/exoplayer2/source/p;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/exoplayer2/source/q;->a(Lcom/google/android/exoplayer2/source/p;)I

    move-result v5

    .line 228
    iget-object v4, p0, Lcom/google/android/exoplayer2/source/e;->A:[Z

    aget-boolean v4, v4, v5

    if-nez v4, :cond_9

    move v4, v3

    :goto_5
    invoke-static {v4}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 229
    iget v4, p0, Lcom/google/android/exoplayer2/source/e;->x:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/android/exoplayer2/source/e;->x:I

    .line 230
    iget-object v4, p0, Lcom/google/android/exoplayer2/source/e;->A:[Z

    aput-boolean v3, v4, v5

    .line 231
    new-instance v4, Lcom/google/android/exoplayer2/source/e$d;

    invoke-direct {v4, p0, v5}, Lcom/google/android/exoplayer2/source/e$d;-><init>(Lcom/google/android/exoplayer2/source/e;I)V

    aput-object v4, p3, v0

    .line 232
    aput-boolean v3, p4, v0

    .line 234
    if-nez v1, :cond_3

    .line 235
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/e;->r:[Lcom/google/android/exoplayer2/source/l;

    aget-object v1, v1, v5

    .line 236
    invoke-virtual {v1}, Lcom/google/android/exoplayer2/source/l;->g()V

    .line 241
    invoke-virtual {v1, p5, p6, v3, v3}, Lcom/google/android/exoplayer2/source/l;->b(JZZ)Z

    move-result v4

    if-nez v4, :cond_a

    .line 242
    invoke-virtual {v1}, Lcom/google/android/exoplayer2/source/l;->d()I

    move-result v1

    if-eqz v1, :cond_a

    move v1, v3

    .line 222
    :cond_3
    :goto_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    move v0, v2

    .line 220
    goto :goto_1

    :cond_5
    const-wide/16 v0, 0x0

    cmp-long v0, p5, v0

    if-eqz v0, :cond_6

    move v0, v3

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_1

    :cond_7
    move v4, v2

    .line 225
    goto :goto_3

    :cond_8
    move v4, v2

    .line 226
    goto :goto_4

    :cond_9
    move v4, v2

    .line 228
    goto :goto_5

    :cond_a
    move v1, v2

    .line 242
    goto :goto_6

    .line 246
    :cond_b
    iget v0, p0, Lcom/google/android/exoplayer2/source/e;->x:I

    if-nez v0, :cond_f

    .line 247
    iput-boolean v2, p0, Lcom/google/android/exoplayer2/source/e;->w:Z

    .line 248
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->j:Lcom/google/android/exoplayer2/upstream/Loader;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/upstream/Loader;->a()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 250
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->r:[Lcom/google/android/exoplayer2/source/l;

    array-length v1, v0

    :goto_7
    if-ge v2, v1, :cond_c

    aget-object v4, v0, v2

    .line 251
    invoke-virtual {v4}, Lcom/google/android/exoplayer2/source/l;->i()V

    .line 250
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 253
    :cond_c
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->j:Lcom/google/android/exoplayer2/upstream/Loader;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/upstream/Loader;->b()V

    .line 268
    :cond_d
    iput-boolean v3, p0, Lcom/google/android/exoplayer2/source/e;->v:Z

    .line 269
    return-wide p5

    .line 255
    :cond_e
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->r:[Lcom/google/android/exoplayer2/source/l;

    array-length v1, v0

    :goto_8
    if-ge v2, v1, :cond_d

    aget-object v4, v0, v2

    .line 256
    invoke-virtual {v4}, Lcom/google/android/exoplayer2/source/l;->a()V

    .line 255
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 259
    :cond_f
    if-eqz v1, :cond_d

    .line 260
    invoke-virtual {p0, p5, p6}, Lcom/google/android/exoplayer2/source/e;->b(J)J

    move-result-wide p5

    .line 262
    :goto_9
    array-length v0, p3

    if-ge v2, v0, :cond_d

    .line 263
    aget-object v0, p3, v2

    if-eqz v0, :cond_10

    .line 264
    aput-boolean v3, p4, v2

    .line 262
    :cond_10
    add-int/lit8 v2, v2, 0x1

    goto :goto_9
.end method

.method public a(II)Lcom/google/android/exoplayer2/extractor/m;
    .locals 4

    .prologue
    .line 433
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->r:[Lcom/google/android/exoplayer2/source/l;

    array-length v2, v0

    .line 434
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    .line 435
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/e;->s:[I

    aget v1, v1, v0

    if-ne v1, p1, :cond_0

    .line 436
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/e;->r:[Lcom/google/android/exoplayer2/source/l;

    aget-object v0, v1, v0

    .line 445
    :goto_1
    return-object v0

    .line 434
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 439
    :cond_1
    new-instance v1, Lcom/google/android/exoplayer2/source/l;

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->g:Lcom/google/android/exoplayer2/upstream/b;

    invoke-direct {v1, v0}, Lcom/google/android/exoplayer2/source/l;-><init>(Lcom/google/android/exoplayer2/upstream/b;)V

    .line 440
    invoke-virtual {v1, p0}, Lcom/google/android/exoplayer2/source/l;->a(Lcom/google/android/exoplayer2/source/l$b;)V

    .line 441
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->s:[I

    add-int/lit8 v3, v2, 0x1

    invoke-static {v0, v3}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/e;->s:[I

    .line 442
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->s:[I

    aput p1, v0, v2

    .line 443
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->r:[Lcom/google/android/exoplayer2/source/l;

    add-int/lit8 v3, v2, 0x1

    invoke-static {v0, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/exoplayer2/source/l;

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/e;->r:[Lcom/google/android/exoplayer2/source/l;

    .line 444
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->r:[Lcom/google/android/exoplayer2/source/l;

    aput-object v1, v0, v2

    move-object v0, v1

    .line 445
    goto :goto_1
.end method

.method public a()V
    .locals 2

    .prologue
    .line 450
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/e;->t:Z

    .line 451
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->o:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/e;->m:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 452
    return-void
.end method

.method a(IJ)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 375
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->r:[Lcom/google/android/exoplayer2/source/l;

    aget-object v0, v0, p1

    .line 376
    iget-boolean v1, p0, Lcom/google/android/exoplayer2/source/e;->H:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/l;->f()J

    move-result-wide v2

    cmp-long v1, p2, v2

    if-lez v1, :cond_0

    .line 377
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/l;->j()V

    .line 381
    :goto_0
    return-void

    .line 379
    :cond_0
    invoke-virtual {v0, p2, p3, v4, v4}, Lcom/google/android/exoplayer2/source/l;->b(JZZ)Z

    goto :goto_0
.end method

.method public a(J)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 274
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->r:[Lcom/google/android/exoplayer2/source/l;

    array-length v2, v0

    move v0, v1

    .line 275
    :goto_0
    if-ge v0, v2, :cond_0

    .line 276
    iget-object v3, p0, Lcom/google/android/exoplayer2/source/e;->r:[Lcom/google/android/exoplayer2/source/l;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/google/android/exoplayer2/source/e;->A:[Z

    aget-boolean v4, v4, v0

    invoke-virtual {v3, p1, p2, v1, v4}, Lcom/google/android/exoplayer2/source/l;->a(JZZ)V

    .line 275
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 278
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/l;)V
    .locals 2

    .prologue
    .line 456
    iput-object p1, p0, Lcom/google/android/exoplayer2/source/e;->q:Lcom/google/android/exoplayer2/extractor/l;

    .line 457
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->o:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/e;->m:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 458
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/j;)V
    .locals 2

    .prologue
    .line 464
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->o:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/e;->m:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 465
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/source/e$a;JJ)V
    .locals 4

    .prologue
    .line 388
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/source/e;->a(Lcom/google/android/exoplayer2/source/e$a;)V

    .line 389
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/e;->H:Z

    .line 390
    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/e;->z:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 391
    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/e;->l()J

    move-result-wide v0

    .line 392
    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    const-wide/16 v0, 0x0

    :goto_0
    iput-wide v0, p0, Lcom/google/android/exoplayer2/source/e;->z:J

    .line 394
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->f:Lcom/google/android/exoplayer2/source/e$c;

    iget-wide v2, p0, Lcom/google/android/exoplayer2/source/e;->z:J

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/e;->q:Lcom/google/android/exoplayer2/extractor/l;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/extractor/l;->a()Z

    move-result v1

    invoke-interface {v0, v2, v3, v1}, Lcom/google/android/exoplayer2/source/e$c;->a(JZ)V

    .line 396
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->p:Lcom/google/android/exoplayer2/source/h$a;

    invoke-interface {v0, p0}, Lcom/google/android/exoplayer2/source/h$a;->a(Lcom/google/android/exoplayer2/source/n;)V

    .line 397
    return-void

    .line 392
    :cond_1
    const-wide/16 v2, 0x2710

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method public a(Lcom/google/android/exoplayer2/source/e$a;JJZ)V
    .locals 4

    .prologue
    .line 402
    if-eqz p6, :cond_1

    .line 412
    :cond_0
    :goto_0
    return-void

    .line 405
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/source/e;->a(Lcom/google/android/exoplayer2/source/e$a;)V

    .line 406
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/e;->r:[Lcom/google/android/exoplayer2/source/l;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 407
    invoke-virtual {v3}, Lcom/google/android/exoplayer2/source/l;->a()V

    .line 406
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 409
    :cond_2
    iget v0, p0, Lcom/google/android/exoplayer2/source/e;->x:I

    if-lez v0, :cond_0

    .line 410
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->p:Lcom/google/android/exoplayer2/source/h$a;

    invoke-interface {v0, p0}, Lcom/google/android/exoplayer2/source/h$a;->a(Lcom/google/android/exoplayer2/source/n;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/exoplayer2/source/h$a;J)V
    .locals 1

    .prologue
    .line 188
    iput-object p1, p0, Lcom/google/android/exoplayer2/source/e;->p:Lcom/google/android/exoplayer2/source/h$a;

    .line 189
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->l:Lcom/google/android/exoplayer2/util/d;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/d;->a()Z

    .line 190
    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/e;->j()V

    .line 191
    return-void
.end method

.method public bridge synthetic a(Lcom/google/android/exoplayer2/upstream/Loader$c;JJ)V
    .locals 6

    .prologue
    .line 49
    move-object v1, p1

    check-cast v1, Lcom/google/android/exoplayer2/source/e$a;

    move-object v0, p0

    move-wide v2, p2

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/exoplayer2/source/e;->a(Lcom/google/android/exoplayer2/source/e$a;JJ)V

    return-void
.end method

.method public bridge synthetic a(Lcom/google/android/exoplayer2/upstream/Loader$c;JJZ)V
    .locals 8

    .prologue
    .line 49
    move-object v1, p1

    check-cast v1, Lcom/google/android/exoplayer2/source/e$a;

    move-object v0, p0

    move-wide v2, p2

    move-wide v4, p4

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/exoplayer2/source/e;->a(Lcom/google/android/exoplayer2/source/e$a;JJZ)V

    return-void
.end method

.method a(I)Z
    .locals 1

    .prologue
    .line 358
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/e;->H:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/e;->m()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->r:[Lcom/google/android/exoplayer2/source/l;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/l;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(J)J
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 335
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/e;->q:Lcom/google/android/exoplayer2/extractor/l;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/extractor/l;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 336
    :goto_0
    iput-wide p1, p0, Lcom/google/android/exoplayer2/source/e;->E:J

    .line 337
    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/e;->w:Z

    .line 339
    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/e;->m()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/source/e;->d(J)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 352
    :cond_0
    :goto_1
    return-wide p1

    .line 335
    :cond_1
    const-wide/16 p1, 0x0

    goto :goto_0

    .line 343
    :cond_2
    iput-wide p1, p0, Lcom/google/android/exoplayer2/source/e;->F:J

    .line 344
    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/e;->H:Z

    .line 345
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/e;->j:Lcom/google/android/exoplayer2/upstream/Loader;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/upstream/Loader;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 346
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->j:Lcom/google/android/exoplayer2/upstream/Loader;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/upstream/Loader;->b()V

    goto :goto_1

    .line 348
    :cond_3
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/e;->r:[Lcom/google/android/exoplayer2/source/l;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 349
    invoke-virtual {v3}, Lcom/google/android/exoplayer2/source/l;->a()V

    .line 348
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public b()Lcom/google/android/exoplayer2/source/q;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->y:Lcom/google/android/exoplayer2/source/q;

    return-object v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 300
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/e;->w:Z

    if-eqz v0, :cond_0

    .line 301
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/e;->w:Z

    .line 302
    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/e;->E:J

    .line 304
    :goto_0
    return-wide v0

    :cond_0
    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    goto :goto_0
.end method

.method public c(J)Z
    .locals 2

    .prologue
    .line 282
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/e;->H:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/e;->u:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/exoplayer2/source/e;->x:I

    if-nez v0, :cond_2

    .line 283
    :cond_0
    const/4 v0, 0x0

    .line 290
    :cond_1
    :goto_0
    return v0

    .line 285
    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->l:Lcom/google/android/exoplayer2/util/d;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/d;->a()Z

    move-result v0

    .line 286
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/e;->j:Lcom/google/android/exoplayer2/upstream/Loader;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/upstream/Loader;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 287
    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/e;->j()V

    .line 288
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public d()J
    .locals 11

    .prologue
    const-wide/high16 v2, -0x8000000000000000L

    .line 309
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/e;->H:Z

    if-eqz v0, :cond_1

    move-wide v0, v2

    .line 328
    :cond_0
    :goto_0
    return-wide v0

    .line 311
    :cond_1
    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/e;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 312
    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/e;->F:J

    goto :goto_0

    .line 315
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/e;->C:Z

    if-eqz v0, :cond_4

    .line 317
    const-wide v4, 0x7fffffffffffffffL

    .line 318
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->r:[Lcom/google/android/exoplayer2/source/l;

    array-length v6, v0

    .line 319
    const/4 v0, 0x0

    move v10, v0

    move-wide v0, v4

    move v4, v10

    :goto_1
    if-ge v4, v6, :cond_5

    .line 320
    iget-object v5, p0, Lcom/google/android/exoplayer2/source/e;->B:[Z

    aget-boolean v5, v5, v4

    if-eqz v5, :cond_3

    .line 321
    iget-object v5, p0, Lcom/google/android/exoplayer2/source/e;->r:[Lcom/google/android/exoplayer2/source/l;

    aget-object v5, v5, v4

    .line 322
    invoke-virtual {v5}, Lcom/google/android/exoplayer2/source/l;->f()J

    move-result-wide v8

    .line 321
    invoke-static {v0, v1, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 319
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 326
    :cond_4
    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/e;->l()J

    move-result-wide v0

    .line 328
    :cond_5
    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/e;->E:J

    goto :goto_0
.end method

.method public e()J
    .locals 2

    .prologue
    .line 295
    iget v0, p0, Lcom/google/android/exoplayer2/source/e;->x:I

    if-nez v0, :cond_0

    const-wide/high16 v0, -0x8000000000000000L

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/source/e;->d()J

    move-result-wide v0

    goto :goto_0
.end method

.method public f()V
    .locals 4

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->j:Lcom/google/android/exoplayer2/upstream/Loader;

    invoke-virtual {v0, p0}, Lcom/google/android/exoplayer2/upstream/Loader;->a(Lcom/google/android/exoplayer2/upstream/Loader$d;)Z

    move-result v0

    .line 167
    iget-boolean v1, p0, Lcom/google/android/exoplayer2/source/e;->u:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 170
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/e;->r:[Lcom/google/android/exoplayer2/source/l;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 171
    invoke-virtual {v3}, Lcom/google/android/exoplayer2/source/l;->i()V

    .line 170
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->o:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 175
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/e;->I:Z

    .line 176
    return-void
.end method

.method public g()V
    .locals 4

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->k:Lcom/google/android/exoplayer2/source/e$b;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/e$b;->a()V

    .line 181
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/e;->r:[Lcom/google/android/exoplayer2/source/l;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 182
    invoke-virtual {v3}, Lcom/google/android/exoplayer2/source/l;->a()V

    .line 181
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 184
    :cond_0
    return-void
.end method

.method public g_()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 195
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/source/e;->h()V

    .line 196
    return-void
.end method

.method h()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 362
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/e;->j:Lcom/google/android/exoplayer2/upstream/Loader;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/upstream/Loader;->c()V

    .line 363
    return-void
.end method
