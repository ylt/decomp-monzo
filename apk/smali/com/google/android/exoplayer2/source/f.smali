.class public final Lcom/google/android/exoplayer2/source/f;
.super Ljava/lang/Object;
.source "ExtractorMediaSource.java"

# interfaces
.implements Lcom/google/android/exoplayer2/source/e$c;
.implements Lcom/google/android/exoplayer2/source/i;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/source/f$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/net/Uri;

.field private final b:Lcom/google/android/exoplayer2/upstream/c$a;

.field private final c:Lcom/google/android/exoplayer2/extractor/h;

.field private final d:I

.field private final e:Landroid/os/Handler;

.field private final f:Lcom/google/android/exoplayer2/source/f$a;

.field private final g:Lcom/google/android/exoplayer2/u$a;

.field private final h:Ljava/lang/String;

.field private final i:I

.field private j:Lcom/google/android/exoplayer2/source/i$a;

.field private k:J

.field private l:Z


# direct methods
.method public constructor <init>(Landroid/net/Uri;Lcom/google/android/exoplayer2/upstream/c$a;Lcom/google/android/exoplayer2/extractor/h;ILandroid/os/Handler;Lcom/google/android/exoplayer2/source/f$a;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    iput-object p1, p0, Lcom/google/android/exoplayer2/source/f;->a:Landroid/net/Uri;

    .line 145
    iput-object p2, p0, Lcom/google/android/exoplayer2/source/f;->b:Lcom/google/android/exoplayer2/upstream/c$a;

    .line 146
    iput-object p3, p0, Lcom/google/android/exoplayer2/source/f;->c:Lcom/google/android/exoplayer2/extractor/h;

    .line 147
    iput p4, p0, Lcom/google/android/exoplayer2/source/f;->d:I

    .line 148
    iput-object p5, p0, Lcom/google/android/exoplayer2/source/f;->e:Landroid/os/Handler;

    .line 149
    iput-object p6, p0, Lcom/google/android/exoplayer2/source/f;->f:Lcom/google/android/exoplayer2/source/f$a;

    .line 150
    iput-object p7, p0, Lcom/google/android/exoplayer2/source/f;->h:Ljava/lang/String;

    .line 151
    iput p8, p0, Lcom/google/android/exoplayer2/source/f;->i:I

    .line 152
    new-instance v0, Lcom/google/android/exoplayer2/u$a;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/u$a;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/f;->g:Lcom/google/android/exoplayer2/u$a;

    .line 153
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Lcom/google/android/exoplayer2/upstream/c$a;Lcom/google/android/exoplayer2/extractor/h;Landroid/os/Handler;Lcom/google/android/exoplayer2/source/f$a;)V
    .locals 7

    .prologue
    .line 106
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer2/source/f;-><init>(Landroid/net/Uri;Lcom/google/android/exoplayer2/upstream/c$a;Lcom/google/android/exoplayer2/extractor/h;Landroid/os/Handler;Lcom/google/android/exoplayer2/source/f$a;Ljava/lang/String;)V

    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Lcom/google/android/exoplayer2/upstream/c$a;Lcom/google/android/exoplayer2/extractor/h;Landroid/os/Handler;Lcom/google/android/exoplayer2/source/f$a;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 123
    const/4 v4, -0x1

    const/high16 v8, 0x100000

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v8}, Lcom/google/android/exoplayer2/source/f;-><init>(Landroid/net/Uri;Lcom/google/android/exoplayer2/upstream/c$a;Lcom/google/android/exoplayer2/extractor/h;ILandroid/os/Handler;Lcom/google/android/exoplayer2/source/f$a;Ljava/lang/String;I)V

    .line 125
    return-void
.end method

.method private b(JZ)V
    .locals 5

    .prologue
    .line 201
    iput-wide p1, p0, Lcom/google/android/exoplayer2/source/f;->k:J

    .line 202
    iput-boolean p3, p0, Lcom/google/android/exoplayer2/source/f;->l:Z

    .line 203
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/f;->j:Lcom/google/android/exoplayer2/source/i$a;

    new-instance v1, Lcom/google/android/exoplayer2/source/o;

    iget-wide v2, p0, Lcom/google/android/exoplayer2/source/f;->k:J

    iget-boolean v4, p0, Lcom/google/android/exoplayer2/source/f;->l:Z

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/exoplayer2/source/o;-><init>(JZ)V

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/exoplayer2/source/i$a;->a(Lcom/google/android/exoplayer2/u;Ljava/lang/Object;)V

    .line 205
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/exoplayer2/source/i$b;Lcom/google/android/exoplayer2/upstream/b;)Lcom/google/android/exoplayer2/source/h;
    .locals 11

    .prologue
    .line 168
    iget v0, p1, Lcom/google/android/exoplayer2/source/i$b;->b:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/a;->a(Z)V

    .line 169
    new-instance v0, Lcom/google/android/exoplayer2/source/e;

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/f;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/exoplayer2/source/f;->b:Lcom/google/android/exoplayer2/upstream/c$a;

    invoke-interface {v2}, Lcom/google/android/exoplayer2/upstream/c$a;->a()Lcom/google/android/exoplayer2/upstream/c;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/f;->c:Lcom/google/android/exoplayer2/extractor/h;

    .line 170
    invoke-interface {v3}, Lcom/google/android/exoplayer2/extractor/h;->a()[Lcom/google/android/exoplayer2/extractor/e;

    move-result-object v3

    iget v4, p0, Lcom/google/android/exoplayer2/source/f;->d:I

    iget-object v5, p0, Lcom/google/android/exoplayer2/source/f;->e:Landroid/os/Handler;

    iget-object v6, p0, Lcom/google/android/exoplayer2/source/f;->f:Lcom/google/android/exoplayer2/source/f$a;

    iget-object v9, p0, Lcom/google/android/exoplayer2/source/f;->h:Ljava/lang/String;

    iget v10, p0, Lcom/google/android/exoplayer2/source/f;->i:I

    move-object v7, p0

    move-object v8, p2

    invoke-direct/range {v0 .. v10}, Lcom/google/android/exoplayer2/source/e;-><init>(Landroid/net/Uri;Lcom/google/android/exoplayer2/upstream/c;[Lcom/google/android/exoplayer2/extractor/e;ILandroid/os/Handler;Lcom/google/android/exoplayer2/source/f$a;Lcom/google/android/exoplayer2/source/e$c;Lcom/google/android/exoplayer2/upstream/b;Ljava/lang/String;I)V

    return-object v0

    .line 168
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 164
    return-void
.end method

.method public a(JZ)V
    .locals 5

    .prologue
    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    .line 189
    cmp-long v0, p1, v2

    if-nez v0, :cond_0

    iget-wide p1, p0, Lcom/google/android/exoplayer2/source/f;->k:J

    .line 190
    :cond_0
    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/f;->k:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/f;->l:Z

    if-eq v0, p3, :cond_2

    :cond_1
    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/f;->k:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    cmp-long v0, p1, v2

    if-nez v0, :cond_3

    .line 196
    :cond_2
    :goto_0
    return-void

    .line 195
    :cond_3
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/exoplayer2/source/f;->b(JZ)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/exoplayer2/e;ZLcom/google/android/exoplayer2/source/i$a;)V
    .locals 3

    .prologue
    .line 157
    iput-object p3, p0, Lcom/google/android/exoplayer2/source/f;->j:Lcom/google/android/exoplayer2/source/i$a;

    .line 158
    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/exoplayer2/source/f;->b(JZ)V

    .line 159
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/source/h;)V
    .locals 0

    .prologue
    .line 176
    check-cast p1, Lcom/google/android/exoplayer2/source/e;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/source/e;->f()V

    .line 177
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 181
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/f;->j:Lcom/google/android/exoplayer2/source/i$a;

    .line 182
    return-void
.end method
