.class public final Lcom/google/android/exoplayer2/source/o;
.super Lcom/google/android/exoplayer2/u;
.source "SinglePeriodTimeline.java"


# static fields
.field private static final b:Ljava/lang/Object;


# instance fields
.field private final c:J

.field private final d:J

.field private final e:J

.field private final f:J

.field private final g:J

.field private final h:J

.field private final i:Z

.field private final j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/exoplayer2/source/o;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(JJJJJJZZ)V
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/google/android/exoplayer2/u;-><init>()V

    .line 88
    iput-wide p1, p0, Lcom/google/android/exoplayer2/source/o;->c:J

    .line 89
    iput-wide p3, p0, Lcom/google/android/exoplayer2/source/o;->d:J

    .line 90
    iput-wide p5, p0, Lcom/google/android/exoplayer2/source/o;->e:J

    .line 91
    iput-wide p7, p0, Lcom/google/android/exoplayer2/source/o;->f:J

    .line 92
    iput-wide p9, p0, Lcom/google/android/exoplayer2/source/o;->g:J

    .line 93
    iput-wide p11, p0, Lcom/google/android/exoplayer2/source/o;->h:J

    .line 94
    iput-boolean p13, p0, Lcom/google/android/exoplayer2/source/o;->i:Z

    .line 95
    iput-boolean p14, p0, Lcom/google/android/exoplayer2/source/o;->j:Z

    .line 96
    return-void
.end method

.method public constructor <init>(JJJJZZ)V
    .locals 17

    .prologue
    .line 65
    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    move-object/from16 v1, p0

    move-wide/from16 v6, p1

    move-wide/from16 v8, p3

    move-wide/from16 v10, p5

    move-wide/from16 v12, p7

    move/from16 v14, p9

    move/from16 v15, p10

    invoke-direct/range {v1 .. v15}, Lcom/google/android/exoplayer2/source/o;-><init>(JJJJJJZZ)V

    .line 67
    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 13

    .prologue
    const-wide/16 v6, 0x0

    .line 46
    const/4 v11, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p1

    move-wide v8, v6

    move/from16 v10, p3

    invoke-direct/range {v1 .. v11}, Lcom/google/android/exoplayer2/source/o;-><init>(JJJJZZ)V

    .line 47
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 134
    sget-object v0, Lcom/google/android/exoplayer2/source/o;->b:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public a(ILcom/google/android/exoplayer2/u$a;Z)Lcom/google/android/exoplayer2/u$a;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 127
    const/4 v0, 0x1

    invoke-static {p1, v3, v0}, Lcom/google/android/exoplayer2/util/a;->a(III)I

    .line 128
    if-eqz p3, :cond_0

    sget-object v1, Lcom/google/android/exoplayer2/source/o;->b:Ljava/lang/Object;

    .line 129
    :goto_0
    iget-wide v4, p0, Lcom/google/android/exoplayer2/source/o;->e:J

    iget-wide v6, p0, Lcom/google/android/exoplayer2/source/o;->g:J

    neg-long v6, v6

    move-object v0, p2

    move-object v2, v1

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/exoplayer2/u$a;->a(Ljava/lang/Object;Ljava/lang/Object;IJJ)Lcom/google/android/exoplayer2/u$a;

    move-result-object v0

    return-object v0

    .line 128
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public a(ILcom/google/android/exoplayer2/u$b;ZJ)Lcom/google/android/exoplayer2/u$b;
    .locals 18

    .prologue
    .line 106
    const/4 v2, 0x0

    const/4 v3, 0x1

    move/from16 v0, p1

    invoke-static {v0, v2, v3}, Lcom/google/android/exoplayer2/util/a;->a(III)I

    .line 107
    if-eqz p3, :cond_1

    sget-object v3, Lcom/google/android/exoplayer2/source/o;->b:Ljava/lang/Object;

    .line 108
    :goto_0
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/exoplayer2/source/o;->h:J

    .line 109
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/exoplayer2/source/o;->j:Z

    if-eqz v2, :cond_0

    .line 110
    add-long v10, v10, p4

    .line 111
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/exoplayer2/source/o;->f:J

    cmp-long v2, v10, v4

    if-lez v2, :cond_0

    .line 113
    const-wide v10, -0x7fffffffffffffffL    # -4.9E-324

    .line 116
    :cond_0
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/exoplayer2/source/o;->c:J

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/exoplayer2/source/o;->d:J

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/google/android/exoplayer2/source/o;->i:Z

    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/google/android/exoplayer2/source/o;->j:Z

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/exoplayer2/source/o;->f:J

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/exoplayer2/source/o;->g:J

    move-wide/from16 v16, v0

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v17}, Lcom/google/android/exoplayer2/u$b;->a(Ljava/lang/Object;JJZZJJIIJ)Lcom/google/android/exoplayer2/u$b;

    move-result-object v2

    return-object v2

    .line 107
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x1

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x1

    return v0
.end method
