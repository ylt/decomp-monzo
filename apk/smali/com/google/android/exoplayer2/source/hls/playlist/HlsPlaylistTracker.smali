.class public final Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;
.super Ljava/lang/Object;
.source "HlsPlaylistTracker.java"

# interfaces
.implements Lcom/google/android/exoplayer2/upstream/Loader$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;,
        Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$b;,
        Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$c;,
        Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$PlaylistResetException;,
        Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$PlaylistStuckException;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/exoplayer2/upstream/Loader$a",
        "<",
        "Lcom/google/android/exoplayer2/upstream/k",
        "<",
        "Lcom/google/android/exoplayer2/source/hls/playlist/c;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/exoplayer2/source/hls/a;

.field private final b:Lcom/google/android/exoplayer2/source/hls/playlist/d;

.field private final c:I

.field private final d:Ljava/util/IdentityHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/IdentityHashMap",
            "<",
            "Lcom/google/android/exoplayer2/source/hls/playlist/a$a;",
            "Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/os/Handler;

.field private final f:Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$c;

.field private final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$b;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/google/android/exoplayer2/source/b$a;

.field private i:Lcom/google/android/exoplayer2/source/hls/playlist/a;

.field private j:Lcom/google/android/exoplayer2/source/hls/playlist/a$a;

.field private k:Lcom/google/android/exoplayer2/source/hls/playlist/b;

.field private l:Z


# direct methods
.method static synthetic a(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;)Lcom/google/android/exoplayer2/source/hls/a;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->a:Lcom/google/android/exoplayer2/source/hls/a;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;Lcom/google/android/exoplayer2/source/hls/playlist/b;Lcom/google/android/exoplayer2/source/hls/playlist/b;)Lcom/google/android/exoplayer2/source/hls/playlist/b;
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->a(Lcom/google/android/exoplayer2/source/hls/playlist/b;Lcom/google/android/exoplayer2/source/hls/playlist/b;)Lcom/google/android/exoplayer2/source/hls/playlist/b;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/exoplayer2/source/hls/playlist/b;Lcom/google/android/exoplayer2/source/hls/playlist/b;)Lcom/google/android/exoplayer2/source/hls/playlist/b;
    .locals 3

    .prologue
    .line 404
    invoke-virtual {p2, p1}, Lcom/google/android/exoplayer2/source/hls/playlist/b;->a(Lcom/google/android/exoplayer2/source/hls/playlist/b;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 405
    iget-boolean v0, p2, Lcom/google/android/exoplayer2/source/hls/playlist/b;->j:Z

    if-eqz v0, :cond_0

    .line 410
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/source/hls/playlist/b;->b()Lcom/google/android/exoplayer2/source/hls/playlist/b;

    move-result-object p1

    .line 417
    :cond_0
    :goto_0
    return-object p1

    .line 415
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->b(Lcom/google/android/exoplayer2/source/hls/playlist/b;Lcom/google/android/exoplayer2/source/hls/playlist/b;)J

    move-result-wide v0

    .line 416
    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->c(Lcom/google/android/exoplayer2/source/hls/playlist/b;Lcom/google/android/exoplayer2/source/hls/playlist/b;)I

    move-result v2

    .line 417
    invoke-virtual {p2, v0, v1, v2}, Lcom/google/android/exoplayer2/source/hls/playlist/b;->a(JI)Lcom/google/android/exoplayer2/source/hls/playlist/b;

    move-result-object p1

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;Lcom/google/android/exoplayer2/source/hls/playlist/a$a;J)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->a(Lcom/google/android/exoplayer2/source/hls/playlist/a$a;J)V

    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/source/hls/playlist/a$a;J)V
    .locals 4

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 397
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 398
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$b;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$b;->a(Lcom/google/android/exoplayer2/source/hls/playlist/a$a;J)V

    .line 397
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 400
    :cond_0
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer2/source/hls/playlist/a$a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 362
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 363
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 364
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 365
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/source/hls/playlist/a$a;

    .line 366
    new-instance v3, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;

    invoke-direct {v3, p0, v0, v4, v5}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;-><init>(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;Lcom/google/android/exoplayer2/source/hls/playlist/a$a;J)V

    .line 367
    iget-object v6, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->d:Ljava/util/IdentityHashMap;

    invoke-virtual {v6, v0, v3}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 364
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 369
    :cond_0
    return-void
.end method

.method private a()Z
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 331
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->i:Lcom/google/android/exoplayer2/source/hls/playlist/a;

    iget-object v3, v0, Lcom/google/android/exoplayer2/source/hls/playlist/a;->a:Ljava/util/List;

    .line 332
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    .line 333
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    move v2, v1

    .line 334
    :goto_0
    if-ge v2, v4, :cond_1

    .line 335
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->d:Ljava/util/IdentityHashMap;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/IdentityHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;

    .line 336
    invoke-static {v0}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->a(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;)J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-lez v5, :cond_0

    .line 337
    invoke-static {v0}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->b(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;)Lcom/google/android/exoplayer2/source/hls/playlist/a$a;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->j:Lcom/google/android/exoplayer2/source/hls/playlist/a$a;

    .line 338
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->a()V

    .line 339
    const/4 v0, 0x1

    .line 342
    :goto_1
    return v0

    .line 334
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 342
    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;Lcom/google/android/exoplayer2/source/hls/playlist/a$a;Lcom/google/android/exoplayer2/source/hls/playlist/b;)Z
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->a(Lcom/google/android/exoplayer2/source/hls/playlist/a$a;Lcom/google/android/exoplayer2/source/hls/playlist/b;)Z

    move-result v0

    return v0
.end method

.method private a(Lcom/google/android/exoplayer2/source/hls/playlist/a$a;Lcom/google/android/exoplayer2/source/hls/playlist/b;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 379
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->j:Lcom/google/android/exoplayer2/source/hls/playlist/a$a;

    if-ne p1, v0, :cond_1

    .line 380
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->k:Lcom/google/android/exoplayer2/source/hls/playlist/b;

    if-nez v0, :cond_0

    .line 382
    iget-boolean v0, p2, Lcom/google/android/exoplayer2/source/hls/playlist/b;->j:Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->l:Z

    .line 384
    :cond_0
    iput-object p2, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->k:Lcom/google/android/exoplayer2/source/hls/playlist/b;

    .line 385
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->f:Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$c;

    invoke-interface {v0, p2}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$c;->a(Lcom/google/android/exoplayer2/source/hls/playlist/b;)V

    .line 387
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v3, v2

    .line 388
    :goto_1
    if-ge v3, v4, :cond_3

    .line 389
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->g:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$b;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$b;->a()V

    .line 388
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 382
    goto :goto_0

    .line 392
    :cond_3
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->j:Lcom/google/android/exoplayer2/source/hls/playlist/a$a;

    if-ne p1, v0, :cond_4

    iget-boolean v0, p2, Lcom/google/android/exoplayer2/source/hls/playlist/b;->j:Z

    if-nez v0, :cond_4

    :goto_2
    return v1

    :cond_4
    move v1, v2

    goto :goto_2
.end method

.method private b(Lcom/google/android/exoplayer2/source/hls/playlist/b;Lcom/google/android/exoplayer2/source/hls/playlist/b;)J
    .locals 5

    .prologue
    .line 422
    iget-boolean v0, p2, Lcom/google/android/exoplayer2/source/hls/playlist/b;->k:Z

    if-eqz v0, :cond_1

    .line 423
    iget-wide v0, p2, Lcom/google/android/exoplayer2/source/hls/playlist/b;->c:J

    .line 438
    :cond_0
    :goto_0
    return-wide v0

    .line 425
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->k:Lcom/google/android/exoplayer2/source/hls/playlist/b;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->k:Lcom/google/android/exoplayer2/source/hls/playlist/b;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->c:J

    .line 427
    :goto_1
    if-eqz p1, :cond_0

    .line 430
    iget-object v2, p1, Lcom/google/android/exoplayer2/source/hls/playlist/b;->m:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 431
    invoke-static {p1, p2}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->d(Lcom/google/android/exoplayer2/source/hls/playlist/b;Lcom/google/android/exoplayer2/source/hls/playlist/b;)Lcom/google/android/exoplayer2/source/hls/playlist/b$a;

    move-result-object v3

    .line 432
    if-eqz v3, :cond_3

    .line 433
    iget-wide v0, p1, Lcom/google/android/exoplayer2/source/hls/playlist/b;->c:J

    iget-wide v2, v3, Lcom/google/android/exoplayer2/source/hls/playlist/b$a;->d:J

    add-long/2addr v0, v2

    goto :goto_0

    .line 425
    :cond_2
    const-wide/16 v0, 0x0

    goto :goto_1

    .line 434
    :cond_3
    iget v3, p2, Lcom/google/android/exoplayer2/source/hls/playlist/b;->f:I

    iget v4, p1, Lcom/google/android/exoplayer2/source/hls/playlist/b;->f:I

    sub-int/2addr v3, v4

    if-ne v2, v3, :cond_0

    .line 435
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/source/hls/playlist/b;->a()J

    move-result-wide v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;)Lcom/google/android/exoplayer2/source/hls/playlist/a;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->i:Lcom/google/android/exoplayer2/source/hls/playlist/a;

    return-object v0
.end method

.method private c(Lcom/google/android/exoplayer2/source/hls/playlist/b;Lcom/google/android/exoplayer2/source/hls/playlist/b;)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 444
    iget-boolean v0, p2, Lcom/google/android/exoplayer2/source/hls/playlist/b;->d:Z

    if-eqz v0, :cond_1

    .line 445
    iget v0, p2, Lcom/google/android/exoplayer2/source/hls/playlist/b;->e:I

    .line 459
    :cond_0
    :goto_0
    return v0

    .line 448
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->k:Lcom/google/android/exoplayer2/source/hls/playlist/b;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->k:Lcom/google/android/exoplayer2/source/hls/playlist/b;

    iget v0, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->e:I

    .line 450
    :goto_1
    if-eqz p1, :cond_0

    .line 453
    invoke-static {p1, p2}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->d(Lcom/google/android/exoplayer2/source/hls/playlist/b;Lcom/google/android/exoplayer2/source/hls/playlist/b;)Lcom/google/android/exoplayer2/source/hls/playlist/b$a;

    move-result-object v2

    .line 454
    if-eqz v2, :cond_0

    .line 455
    iget v0, p1, Lcom/google/android/exoplayer2/source/hls/playlist/b;->e:I

    iget v2, v2, Lcom/google/android/exoplayer2/source/hls/playlist/b$a;->c:I

    add-int/2addr v2, v0

    iget-object v0, p2, Lcom/google/android/exoplayer2/source/hls/playlist/b;->m:Ljava/util/List;

    .line 457
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/source/hls/playlist/b$a;

    iget v0, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b$a;->c:I

    sub-int v0, v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 448
    goto :goto_1
.end method

.method static synthetic c(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;)Lcom/google/android/exoplayer2/source/hls/playlist/d;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->b:Lcom/google/android/exoplayer2/source/hls/playlist/d;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;)I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->c:I

    return v0
.end method

.method private static d(Lcom/google/android/exoplayer2/source/hls/playlist/b;Lcom/google/android/exoplayer2/source/hls/playlist/b;)Lcom/google/android/exoplayer2/source/hls/playlist/b$a;
    .locals 3

    .prologue
    .line 464
    iget v0, p1, Lcom/google/android/exoplayer2/source/hls/playlist/b;->f:I

    iget v1, p0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->f:I

    sub-int/2addr v0, v1

    .line 465
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->m:Ljava/util/List;

    .line 466
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/source/hls/playlist/b$a;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;)Lcom/google/android/exoplayer2/source/b$a;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->h:Lcom/google/android/exoplayer2/source/b$a;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;)Lcom/google/android/exoplayer2/source/hls/playlist/a$a;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->j:Lcom/google/android/exoplayer2/source/hls/playlist/a$a;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;)Z
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->a()Z

    move-result v0

    return v0
.end method

.method static synthetic h(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->e:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Lcom/google/android/exoplayer2/upstream/Loader$c;JJLjava/io/IOException;)I
    .locals 8

    .prologue
    .line 41
    move-object v1, p1

    check-cast v1, Lcom/google/android/exoplayer2/upstream/k;

    move-object v0, p0

    move-wide v2, p2

    move-wide v4, p4

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->a(Lcom/google/android/exoplayer2/upstream/k;JJLjava/io/IOException;)I

    move-result v0

    return v0
.end method

.method public a(Lcom/google/android/exoplayer2/upstream/k;JJLjava/io/IOException;)I
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/upstream/k",
            "<",
            "Lcom/google/android/exoplayer2/source/hls/playlist/c;",
            ">;JJ",
            "Ljava/io/IOException;",
            ")I"
        }
    .end annotation

    .prologue
    .line 322
    move-object/from16 v0, p6

    instance-of v13, v0, Lcom/google/android/exoplayer2/ParserException;

    .line 323
    iget-object v3, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->h:Lcom/google/android/exoplayer2/source/b$a;

    iget-object v4, p1, Lcom/google/android/exoplayer2/upstream/k;->a:Lcom/google/android/exoplayer2/upstream/e;

    const/4 v5, 0x4

    .line 324
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/upstream/k;->e()J

    move-result-wide v10

    move-wide/from16 v6, p2

    move-wide/from16 v8, p4

    move-object/from16 v12, p6

    .line 323
    invoke-virtual/range {v3 .. v13}, Lcom/google/android/exoplayer2/source/b$a;->a(Lcom/google/android/exoplayer2/upstream/e;IJJJLjava/io/IOException;Z)V

    .line 325
    if-eqz v13, :cond_0

    const/4 v2, 0x3

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public bridge synthetic a(Lcom/google/android/exoplayer2/upstream/Loader$c;JJ)V
    .locals 6

    .prologue
    .line 41
    move-object v1, p1

    check-cast v1, Lcom/google/android/exoplayer2/upstream/k;

    move-object v0, p0

    move-wide v2, p2

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->a(Lcom/google/android/exoplayer2/upstream/k;JJ)V

    return-void
.end method

.method public bridge synthetic a(Lcom/google/android/exoplayer2/upstream/Loader$c;JJZ)V
    .locals 8

    .prologue
    .line 41
    move-object v1, p1

    check-cast v1, Lcom/google/android/exoplayer2/upstream/k;

    move-object v0, p0

    move-wide v2, p2

    move-wide v4, p4

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->a(Lcom/google/android/exoplayer2/upstream/k;JJZ)V

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/upstream/k;JJ)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/upstream/k",
            "<",
            "Lcom/google/android/exoplayer2/source/hls/playlist/c;",
            ">;JJ)V"
        }
    .end annotation

    .prologue
    .line 286
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/upstream/k;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/source/hls/playlist/c;

    .line 288
    instance-of v3, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;

    .line 289
    if-eqz v3, :cond_0

    .line 290
    iget-object v1, v0, Lcom/google/android/exoplayer2/source/hls/playlist/c;->o:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/exoplayer2/source/hls/playlist/a;->a(Ljava/lang/String;)Lcom/google/android/exoplayer2/source/hls/playlist/a;

    move-result-object v1

    move-object v2, v1

    .line 294
    :goto_0
    iput-object v2, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->i:Lcom/google/android/exoplayer2/source/hls/playlist/a;

    .line 295
    iget-object v1, v2, Lcom/google/android/exoplayer2/source/hls/playlist/a;->a:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/source/hls/playlist/a$a;

    iput-object v1, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->j:Lcom/google/android/exoplayer2/source/hls/playlist/a$a;

    .line 296
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 297
    iget-object v4, v2, Lcom/google/android/exoplayer2/source/hls/playlist/a;->a:Ljava/util/List;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 298
    iget-object v4, v2, Lcom/google/android/exoplayer2/source/hls/playlist/a;->b:Ljava/util/List;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 299
    iget-object v2, v2, Lcom/google/android/exoplayer2/source/hls/playlist/a;->c:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 300
    invoke-direct {p0, v1}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->a(Ljava/util/List;)V

    .line 301
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->d:Ljava/util/IdentityHashMap;

    iget-object v2, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->j:Lcom/google/android/exoplayer2/source/hls/playlist/a$a;

    invoke-virtual {v1, v2}, Ljava/util/IdentityHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;

    .line 302
    if-eqz v3, :cond_1

    .line 304
    check-cast v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;

    invoke-static {v1, v0}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->a(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;Lcom/google/android/exoplayer2/source/hls/playlist/b;)V

    .line 308
    :goto_1
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->h:Lcom/google/android/exoplayer2/source/b$a;

    iget-object v2, p1, Lcom/google/android/exoplayer2/upstream/k;->a:Lcom/google/android/exoplayer2/upstream/e;

    const/4 v3, 0x4

    .line 309
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/upstream/k;->e()J

    move-result-wide v8

    move-wide v4, p2

    move-wide v6, p4

    .line 308
    invoke-virtual/range {v1 .. v9}, Lcom/google/android/exoplayer2/source/b$a;->a(Lcom/google/android/exoplayer2/upstream/e;IJJJ)V

    .line 310
    return-void

    :cond_0
    move-object v1, v0

    .line 292
    check-cast v1, Lcom/google/android/exoplayer2/source/hls/playlist/a;

    move-object v2, v1

    goto :goto_0

    .line 306
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->a()V

    goto :goto_1
.end method

.method public a(Lcom/google/android/exoplayer2/upstream/k;JJZ)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/upstream/k",
            "<",
            "Lcom/google/android/exoplayer2/source/hls/playlist/c;",
            ">;JJZ)V"
        }
    .end annotation

    .prologue
    .line 315
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->h:Lcom/google/android/exoplayer2/source/b$a;

    iget-object v2, p1, Lcom/google/android/exoplayer2/upstream/k;->a:Lcom/google/android/exoplayer2/upstream/e;

    const/4 v3, 0x4

    .line 316
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/upstream/k;->e()J

    move-result-wide v8

    move-wide v4, p2

    move-wide v6, p4

    .line 315
    invoke-virtual/range {v1 .. v9}, Lcom/google/android/exoplayer2/source/b$a;->b(Lcom/google/android/exoplayer2/upstream/e;IJJJ)V

    .line 317
    return-void
.end method
