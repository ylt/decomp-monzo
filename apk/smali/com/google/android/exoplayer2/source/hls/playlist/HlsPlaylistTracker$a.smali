.class final Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;
.super Ljava/lang/Object;
.source "HlsPlaylistTracker.java"

# interfaces
.implements Lcom/google/android/exoplayer2/upstream/Loader$a;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/exoplayer2/upstream/Loader$a",
        "<",
        "Lcom/google/android/exoplayer2/upstream/k",
        "<",
        "Lcom/google/android/exoplayer2/source/hls/playlist/c;",
        ">;>;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;

.field private final b:Lcom/google/android/exoplayer2/source/hls/playlist/a$a;

.field private final c:Lcom/google/android/exoplayer2/upstream/Loader;

.field private final d:Lcom/google/android/exoplayer2/upstream/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/exoplayer2/upstream/k",
            "<",
            "Lcom/google/android/exoplayer2/source/hls/playlist/c;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/google/android/exoplayer2/source/hls/playlist/b;

.field private f:J

.field private g:J

.field private h:J

.field private i:J

.field private j:Z

.field private k:Ljava/io/IOException;


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;Lcom/google/android/exoplayer2/source/hls/playlist/a$a;J)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    .line 487
    iput-object p1, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->a:Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 488
    iput-object p2, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->b:Lcom/google/android/exoplayer2/source/hls/playlist/a$a;

    .line 489
    iput-wide p3, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->h:J

    .line 490
    new-instance v0, Lcom/google/android/exoplayer2/upstream/Loader;

    const-string v1, "HlsPlaylistTracker:MediaPlaylist"

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/upstream/Loader;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->c:Lcom/google/android/exoplayer2/upstream/Loader;

    .line 491
    new-instance v0, Lcom/google/android/exoplayer2/upstream/k;

    .line 492
    invoke-static {p1}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->a(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;)Lcom/google/android/exoplayer2/source/hls/a;

    move-result-object v1

    invoke-interface {v1, v4}, Lcom/google/android/exoplayer2/source/hls/a;->a(I)Lcom/google/android/exoplayer2/upstream/c;

    move-result-object v1

    .line 493
    invoke-static {p1}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->b(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;)Lcom/google/android/exoplayer2/source/hls/playlist/a;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/exoplayer2/source/hls/playlist/a;->o:Ljava/lang/String;

    iget-object v3, p2, Lcom/google/android/exoplayer2/source/hls/playlist/a$a;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/exoplayer2/util/r;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 494
    invoke-static {p1}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->c(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;)Lcom/google/android/exoplayer2/source/hls/playlist/d;

    move-result-object v3

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/google/android/exoplayer2/upstream/k;-><init>(Lcom/google/android/exoplayer2/upstream/c;Landroid/net/Uri;ILcom/google/android/exoplayer2/upstream/k$a;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->d:Lcom/google/android/exoplayer2/upstream/k;

    .line 495
    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;)J
    .locals 2

    .prologue
    .line 472
    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->i:J

    return-wide v0
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;Lcom/google/android/exoplayer2/source/hls/playlist/b;)V
    .locals 0

    .prologue
    .line 472
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->a(Lcom/google/android/exoplayer2/source/hls/playlist/b;)V

    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/source/hls/playlist/b;)V
    .locals 9

    .prologue
    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v8, 0x0

    .line 582
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->e:Lcom/google/android/exoplayer2/source/hls/playlist/b;

    .line 583
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 584
    iput-wide v4, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->f:J

    .line 585
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->a:Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;

    invoke-static {v1, v0, p1}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->a(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;Lcom/google/android/exoplayer2/source/hls/playlist/b;Lcom/google/android/exoplayer2/source/hls/playlist/b;)Lcom/google/android/exoplayer2/source/hls/playlist/b;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->e:Lcom/google/android/exoplayer2/source/hls/playlist/b;

    .line 587
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->e:Lcom/google/android/exoplayer2/source/hls/playlist/b;

    if-eq v1, v0, :cond_1

    .line 588
    iput-object v8, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->k:Ljava/io/IOException;

    .line 589
    iput-wide v4, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->g:J

    .line 590
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->a:Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->b:Lcom/google/android/exoplayer2/source/hls/playlist/a$a;

    iget-object v4, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->e:Lcom/google/android/exoplayer2/source/hls/playlist/b;

    invoke-static {v0, v1, v4}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->a(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;Lcom/google/android/exoplayer2/source/hls/playlist/a$a;Lcom/google/android/exoplayer2/source/hls/playlist/b;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 591
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->e:Lcom/google/android/exoplayer2/source/hls/playlist/b;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->h:J

    .line 607
    :goto_0
    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 609
    iget-object v2, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->a:Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;

    invoke-static {v2}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->h(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;)Landroid/os/Handler;

    move-result-object v2

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/b;->a(J)J

    move-result-wide v0

    invoke-virtual {v2, p0, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->j:Z

    .line 611
    :cond_0
    return-void

    .line 593
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->e:Lcom/google/android/exoplayer2/source/hls/playlist/b;

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->j:Z

    if-nez v0, :cond_4

    .line 594
    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->g:J

    sub-long v0, v4, v0

    long-to-double v0, v0

    iget-object v4, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->e:Lcom/google/android/exoplayer2/source/hls/playlist/b;

    iget-wide v4, v4, Lcom/google/android/exoplayer2/source/hls/playlist/b;->h:J

    .line 595
    invoke-static {v4, v5}, Lcom/google/android/exoplayer2/b;->a(J)J

    move-result-wide v4

    long-to-double v4, v4

    const-wide/high16 v6, 0x400c000000000000L    # 3.5

    mul-double/2addr v4, v6

    cmpl-double v0, v0, v4

    if-lez v0, :cond_3

    .line 598
    new-instance v0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$PlaylistStuckException;

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->b:Lcom/google/android/exoplayer2/source/hls/playlist/a$a;

    iget-object v1, v1, Lcom/google/android/exoplayer2/source/hls/playlist/a$a;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v8}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$PlaylistStuckException;-><init>(Ljava/lang/String;Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$1;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->k:Ljava/io/IOException;

    .line 599
    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->b()V

    .line 605
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->e:Lcom/google/android/exoplayer2/source/hls/playlist/b;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->h:J

    const-wide/16 v4, 0x2

    div-long/2addr v0, v4

    goto :goto_0

    .line 600
    :cond_3
    iget v0, p1, Lcom/google/android/exoplayer2/source/hls/playlist/b;->f:I

    iget-object v1, p1, Lcom/google/android/exoplayer2/source/hls/playlist/b;->m:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->e:Lcom/google/android/exoplayer2/source/hls/playlist/b;

    iget v1, v1, Lcom/google/android/exoplayer2/source/hls/playlist/b;->f:I

    if-ge v0, v1, :cond_2

    .line 603
    new-instance v0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$PlaylistResetException;

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->b:Lcom/google/android/exoplayer2/source/hls/playlist/a$a;

    iget-object v1, v1, Lcom/google/android/exoplayer2/source/hls/playlist/a$a;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v8}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$PlaylistResetException;-><init>(Ljava/lang/String;Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$1;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->k:Ljava/io/IOException;

    goto :goto_1

    :cond_4
    move-wide v0, v2

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;)Lcom/google/android/exoplayer2/source/hls/playlist/a$a;
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->b:Lcom/google/android/exoplayer2/source/hls/playlist/a$a;

    return-object v0
.end method

.method private b()V
    .locals 4

    .prologue
    const-wide/32 v2, 0xea60

    .line 614
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->i:J

    .line 616
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->a:Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->b:Lcom/google/android/exoplayer2/source/hls/playlist/a$a;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->a(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;Lcom/google/android/exoplayer2/source/hls/playlist/a$a;J)V

    .line 617
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/google/android/exoplayer2/upstream/Loader$c;JJLjava/io/IOException;)I
    .locals 8

    .prologue
    .line 472
    move-object v1, p1

    check-cast v1, Lcom/google/android/exoplayer2/upstream/k;

    move-object v0, p0

    move-wide v2, p2

    move-wide v4, p4

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->a(Lcom/google/android/exoplayer2/upstream/k;JJLjava/io/IOException;)I

    move-result v0

    return v0
.end method

.method public a(Lcom/google/android/exoplayer2/upstream/k;JJLjava/io/IOException;)I
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/upstream/k",
            "<",
            "Lcom/google/android/exoplayer2/source/hls/playlist/c;",
            ">;JJ",
            "Ljava/io/IOException;",
            ")I"
        }
    .end annotation

    .prologue
    .line 557
    move-object/from16 v0, p6

    instance-of v13, v0, Lcom/google/android/exoplayer2/ParserException;

    .line 558
    iget-object v2, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->a:Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;

    invoke-static {v2}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->e(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;)Lcom/google/android/exoplayer2/source/b$a;

    move-result-object v3

    iget-object v4, p1, Lcom/google/android/exoplayer2/upstream/k;->a:Lcom/google/android/exoplayer2/upstream/e;

    const/4 v5, 0x4

    .line 559
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/upstream/k;->e()J

    move-result-wide v10

    move-wide/from16 v6, p2

    move-wide/from16 v8, p4

    move-object/from16 v12, p6

    .line 558
    invoke-virtual/range {v3 .. v13}, Lcom/google/android/exoplayer2/source/b$a;->a(Lcom/google/android/exoplayer2/upstream/e;IJJJLjava/io/IOException;Z)V

    .line 560
    if-eqz v13, :cond_0

    .line 561
    const/4 v2, 0x3

    .line 568
    :goto_0
    return v2

    .line 563
    :cond_0
    const/4 v2, 0x1

    .line 564
    invoke-static/range {p6 .. p6}, Lcom/google/android/exoplayer2/source/a/a;->a(Ljava/lang/Exception;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 565
    invoke-direct {p0}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->b()V

    .line 566
    iget-object v2, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->a:Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;

    invoke-static {v2}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->f(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;)Lcom/google/android/exoplayer2/source/hls/playlist/a$a;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->b:Lcom/google/android/exoplayer2/source/hls/playlist/a$a;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->a:Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;

    invoke-static {v2}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->g(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x1

    .line 568
    :cond_1
    :goto_1
    if-eqz v2, :cond_3

    const/4 v2, 0x0

    goto :goto_0

    .line 566
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 568
    :cond_3
    const/4 v2, 0x2

    goto :goto_0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 519
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->i:J

    .line 520
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->j:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->c:Lcom/google/android/exoplayer2/upstream/Loader;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/upstream/Loader;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 521
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->c:Lcom/google/android/exoplayer2/upstream/Loader;

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->d:Lcom/google/android/exoplayer2/upstream/k;

    iget-object v2, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->a:Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;

    invoke-static {v2}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->d(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;)I

    move-result v2

    invoke-virtual {v0, v1, p0, v2}, Lcom/google/android/exoplayer2/upstream/Loader;->a(Lcom/google/android/exoplayer2/upstream/Loader$c;Lcom/google/android/exoplayer2/upstream/Loader$a;I)J

    .line 523
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Lcom/google/android/exoplayer2/upstream/Loader$c;JJ)V
    .locals 6

    .prologue
    .line 472
    move-object v1, p1

    check-cast v1, Lcom/google/android/exoplayer2/upstream/k;

    move-object v0, p0

    move-wide v2, p2

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->a(Lcom/google/android/exoplayer2/upstream/k;JJ)V

    return-void
.end method

.method public bridge synthetic a(Lcom/google/android/exoplayer2/upstream/Loader$c;JJZ)V
    .locals 8

    .prologue
    .line 472
    move-object v1, p1

    check-cast v1, Lcom/google/android/exoplayer2/upstream/k;

    move-object v0, p0

    move-wide v2, p2

    move-wide v4, p4

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->a(Lcom/google/android/exoplayer2/upstream/k;JJZ)V

    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/upstream/k;JJ)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/upstream/k",
            "<",
            "Lcom/google/android/exoplayer2/source/hls/playlist/c;",
            ">;JJ)V"
        }
    .end annotation

    .prologue
    .line 537
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/upstream/k;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/source/hls/playlist/c;

    .line 538
    instance-of v1, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;

    if-eqz v1, :cond_0

    .line 539
    check-cast v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->a(Lcom/google/android/exoplayer2/source/hls/playlist/b;)V

    .line 540
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->a:Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;

    invoke-static {v0}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->e(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;)Lcom/google/android/exoplayer2/source/b$a;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/exoplayer2/upstream/k;->a:Lcom/google/android/exoplayer2/upstream/e;

    const/4 v3, 0x4

    .line 541
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/upstream/k;->e()J

    move-result-wide v8

    move-wide v4, p2

    move-wide v6, p4

    .line 540
    invoke-virtual/range {v1 .. v9}, Lcom/google/android/exoplayer2/source/b$a;->a(Lcom/google/android/exoplayer2/upstream/e;IJJJ)V

    .line 545
    :goto_0
    return-void

    .line 543
    :cond_0
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Loaded playlist has unexpected type."

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->k:Ljava/io/IOException;

    goto :goto_0
.end method

.method public a(Lcom/google/android/exoplayer2/upstream/k;JJZ)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/upstream/k",
            "<",
            "Lcom/google/android/exoplayer2/source/hls/playlist/c;",
            ">;JJZ)V"
        }
    .end annotation

    .prologue
    .line 550
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->a:Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;

    invoke-static {v0}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;->e(Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker;)Lcom/google/android/exoplayer2/source/b$a;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/exoplayer2/upstream/k;->a:Lcom/google/android/exoplayer2/upstream/e;

    const/4 v3, 0x4

    .line 551
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/upstream/k;->e()J

    move-result-wide v8

    move-wide v4, p2

    move-wide v6, p4

    .line 550
    invoke-virtual/range {v1 .. v9}, Lcom/google/android/exoplayer2/source/b$a;->b(Lcom/google/android/exoplayer2/upstream/e;IJJJ)V

    .line 552
    return-void
.end method

.method public run()V
    .locals 1

    .prologue
    .line 575
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->j:Z

    .line 576
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$a;->a()V

    .line 577
    return-void
.end method
