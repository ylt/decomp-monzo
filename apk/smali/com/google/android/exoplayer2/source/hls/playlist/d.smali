.class public final Lcom/google/android/exoplayer2/source/hls/playlist/d;
.super Ljava/lang/Object;
.source "HlsPlaylistParser.java"

# interfaces
.implements Lcom/google/android/exoplayer2/upstream/k$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/source/hls/playlist/d$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/exoplayer2/upstream/k$a",
        "<",
        "Lcom/google/android/exoplayer2/source/hls/playlist/c;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/util/regex/Pattern;

.field private static final b:Ljava/util/regex/Pattern;

.field private static final c:Ljava/util/regex/Pattern;

.field private static final d:Ljava/util/regex/Pattern;

.field private static final e:Ljava/util/regex/Pattern;

.field private static final f:Ljava/util/regex/Pattern;

.field private static final g:Ljava/util/regex/Pattern;

.field private static final h:Ljava/util/regex/Pattern;

.field private static final i:Ljava/util/regex/Pattern;

.field private static final j:Ljava/util/regex/Pattern;

.field private static final k:Ljava/util/regex/Pattern;

.field private static final l:Ljava/util/regex/Pattern;

.field private static final m:Ljava/util/regex/Pattern;

.field private static final n:Ljava/util/regex/Pattern;

.field private static final o:Ljava/util/regex/Pattern;

.field private static final p:Ljava/util/regex/Pattern;

.field private static final q:Ljava/util/regex/Pattern;

.field private static final r:Ljava/util/regex/Pattern;

.field private static final s:Ljava/util/regex/Pattern;

.field private static final t:Ljava/util/regex/Pattern;

.field private static final u:Ljava/util/regex/Pattern;

.field private static final v:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 79
    const-string v0, "AVERAGE-BANDWIDTH=(\\d+)\\b"

    .line 80
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/source/hls/playlist/d;->a:Ljava/util/regex/Pattern;

    .line 81
    const-string v0, "[^-]BANDWIDTH=(\\d+)\\b"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/source/hls/playlist/d;->b:Ljava/util/regex/Pattern;

    .line 82
    const-string v0, "CODECS=\"(.+?)\""

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/source/hls/playlist/d;->c:Ljava/util/regex/Pattern;

    .line 83
    const-string v0, "RESOLUTION=(\\d+x\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/source/hls/playlist/d;->d:Ljava/util/regex/Pattern;

    .line 84
    const-string v0, "#EXT-X-TARGETDURATION:(\\d+)\\b"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/source/hls/playlist/d;->e:Ljava/util/regex/Pattern;

    .line 86
    const-string v0, "#EXT-X-VERSION:(\\d+)\\b"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/source/hls/playlist/d;->f:Ljava/util/regex/Pattern;

    .line 87
    const-string v0, "#EXT-X-PLAYLIST-TYPE:(.+)\\b"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/source/hls/playlist/d;->g:Ljava/util/regex/Pattern;

    .line 89
    const-string v0, "#EXT-X-MEDIA-SEQUENCE:(\\d+)\\b"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/source/hls/playlist/d;->h:Ljava/util/regex/Pattern;

    .line 91
    const-string v0, "#EXTINF:([\\d\\.]+)\\b"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/source/hls/playlist/d;->i:Ljava/util/regex/Pattern;

    .line 93
    const-string v0, "TIME-OFFSET=(-?[\\d\\.]+)\\b"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/source/hls/playlist/d;->j:Ljava/util/regex/Pattern;

    .line 94
    const-string v0, "#EXT-X-BYTERANGE:(\\d+(?:@\\d+)?)\\b"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/source/hls/playlist/d;->k:Ljava/util/regex/Pattern;

    .line 96
    const-string v0, "BYTERANGE=\"(\\d+(?:@\\d+)?)\\b\""

    .line 97
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/source/hls/playlist/d;->l:Ljava/util/regex/Pattern;

    .line 98
    const-string v0, "METHOD=(NONE|AES-128)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/source/hls/playlist/d;->m:Ljava/util/regex/Pattern;

    .line 100
    const-string v0, "URI=\"(.+?)\""

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/source/hls/playlist/d;->n:Ljava/util/regex/Pattern;

    .line 101
    const-string v0, "IV=([^,.*]+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/source/hls/playlist/d;->o:Ljava/util/regex/Pattern;

    .line 102
    const-string v0, "TYPE=(AUDIO|VIDEO|SUBTITLES|CLOSED-CAPTIONS)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/source/hls/playlist/d;->p:Ljava/util/regex/Pattern;

    .line 104
    const-string v0, "LANGUAGE=\"(.+?)\""

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/source/hls/playlist/d;->q:Ljava/util/regex/Pattern;

    .line 105
    const-string v0, "NAME=\"(.+?)\""

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/source/hls/playlist/d;->r:Ljava/util/regex/Pattern;

    .line 106
    const-string v0, "INSTREAM-ID=\"((?:CC|SERVICE)\\d+)\""

    .line 107
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/source/hls/playlist/d;->s:Ljava/util/regex/Pattern;

    .line 108
    const-string v0, "AUTOSELECT"

    invoke-static {v0}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->b(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/source/hls/playlist/d;->t:Ljava/util/regex/Pattern;

    .line 109
    const-string v0, "DEFAULT"

    invoke-static {v0}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->b(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/source/hls/playlist/d;->u:Ljava/util/regex/Pattern;

    .line 110
    const-string v0, "FORCED"

    invoke-static {v0}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->b(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/source/hls/playlist/d;->v:Ljava/util/regex/Pattern;

    return-void
.end method

.method private static a(Ljava/io/BufferedReader;ZI)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 172
    :goto_0
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    invoke-static {p2}, Ljava/lang/Character;->isWhitespace(I)Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    invoke-static {p2}, Lcom/google/android/exoplayer2/util/s;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 173
    :cond_0
    invoke-virtual {p0}, Ljava/io/BufferedReader;->read()I

    move-result p2

    goto :goto_0

    .line 175
    :cond_1
    return p2
.end method

.method private static a(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 283
    sget-object v0, Lcom/google/android/exoplayer2/source/hls/playlist/d;->u:Ljava/util/regex/Pattern;

    invoke-static {p0, v0, v1}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->a(Ljava/lang/String;Ljava/util/regex/Pattern;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    sget-object v2, Lcom/google/android/exoplayer2/source/hls/playlist/d;->v:Ljava/util/regex/Pattern;

    .line 284
    invoke-static {p0, v2, v1}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->a(Ljava/lang/String;Ljava/util/regex/Pattern;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    :goto_1
    or-int/2addr v0, v2

    sget-object v2, Lcom/google/android/exoplayer2/source/hls/playlist/d;->t:Ljava/util/regex/Pattern;

    .line 285
    invoke-static {p0, v2, v1}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->a(Ljava/lang/String;Ljava/util/regex/Pattern;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    :cond_0
    or-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    .line 283
    goto :goto_0

    :cond_2
    move v2, v1

    .line 284
    goto :goto_1
.end method

.method private static a(Ljava/lang/String;Ljava/util/regex/Pattern;)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    .line 419
    invoke-static {p0, p1}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->d(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private static a(Lcom/google/android/exoplayer2/source/hls/playlist/d$a;Ljava/lang/String;)Lcom/google/android/exoplayer2/source/hls/playlist/a;
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 180
    new-instance v15, Ljava/util/HashSet;

    invoke-direct {v15}, Ljava/util/HashSet;-><init>()V

    .line 181
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 182
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 183
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 184
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 185
    const/4 v13, 0x0

    .line 186
    const/4 v12, 0x0

    .line 187
    const/4 v1, 0x0

    move v14, v1

    .line 190
    :cond_0
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/source/hls/playlist/d$a;->a()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 191
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/source/hls/playlist/d$a;->b()Ljava/lang/String;

    move-result-object v3

    .line 193
    const-string v1, "#EXT"

    invoke-virtual {v3, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 195
    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 198
    :cond_1
    const-string v1, "#EXT-X-MEDIA"

    invoke-virtual {v3, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 199
    invoke-static {v3}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->a(Ljava/lang/String;)I

    move-result v9

    .line 200
    sget-object v1, Lcom/google/android/exoplayer2/source/hls/playlist/d;->n:Ljava/util/regex/Pattern;

    invoke-static {v3, v1}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->c(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v11

    .line 201
    sget-object v1, Lcom/google/android/exoplayer2/source/hls/playlist/d;->r:Ljava/util/regex/Pattern;

    invoke-static {v3, v1}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->d(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v1

    .line 202
    sget-object v2, Lcom/google/android/exoplayer2/source/hls/playlist/d;->q:Ljava/util/regex/Pattern;

    invoke-static {v3, v2}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->c(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v10

    .line 204
    sget-object v2, Lcom/google/android/exoplayer2/source/hls/playlist/d;->p:Ljava/util/regex/Pattern;

    invoke-static {v3, v2}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->d(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v4

    const/4 v2, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_2
    :goto_1
    packed-switch v2, :pswitch_data_0

    move-object v1, v12

    move-object v2, v13

    :goto_2
    move-object v12, v1

    move-object v13, v2

    .line 240
    goto :goto_0

    .line 204
    :sswitch_0
    const-string v5, "AUDIO"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v2, 0x0

    goto :goto_1

    :sswitch_1
    const-string v5, "SUBTITLES"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v2, 0x1

    goto :goto_1

    :sswitch_2
    const-string v5, "CLOSED-CAPTIONS"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v2, 0x2

    goto :goto_1

    .line 206
    :pswitch_0
    const-string v2, "application/x-mpegURL"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, -0x1

    const/4 v6, -0x1

    const/4 v7, -0x1

    const/4 v8, 0x0

    invoke-static/range {v1 .. v10}, Lcom/google/android/exoplayer2/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILjava/util/List;ILjava/lang/String;)Lcom/google/android/exoplayer2/j;

    move-result-object v1

    .line 208
    if-nez v11, :cond_3

    move-object v2, v1

    move-object v1, v12

    .line 209
    goto :goto_2

    .line 211
    :cond_3
    new-instance v2, Lcom/google/android/exoplayer2/source/hls/playlist/a$a;

    invoke-direct {v2, v11, v1}, Lcom/google/android/exoplayer2/source/hls/playlist/a$a;-><init>(Ljava/lang/String;Lcom/google/android/exoplayer2/j;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v1, v12

    move-object v2, v13

    .line 213
    goto :goto_2

    .line 215
    :pswitch_1
    const-string v5, "application/x-mpegURL"

    const-string v6, "text/vtt"

    const/4 v7, 0x0

    const/4 v8, -0x1

    move-object v4, v1

    invoke-static/range {v4 .. v10}, Lcom/google/android/exoplayer2/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)Lcom/google/android/exoplayer2/j;

    move-result-object v1

    .line 217
    new-instance v2, Lcom/google/android/exoplayer2/source/hls/playlist/a$a;

    invoke-direct {v2, v11, v1}, Lcom/google/android/exoplayer2/source/hls/playlist/a$a;-><init>(Ljava/lang/String;Lcom/google/android/exoplayer2/j;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v1, v12

    move-object v2, v13

    .line 218
    goto :goto_2

    .line 220
    :pswitch_2
    sget-object v2, Lcom/google/android/exoplayer2/source/hls/playlist/d;->s:Ljava/util/regex/Pattern;

    invoke-static {v3, v2}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->d(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v2

    .line 223
    const-string v3, "CC"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 224
    const-string v6, "application/cea-608"

    .line 225
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    .line 230
    :goto_3
    if-nez v12, :cond_d

    .line 231
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 233
    :goto_4
    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, -0x1

    move-object v4, v1

    invoke-static/range {v4 .. v11}, Lcom/google/android/exoplayer2/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;I)Lcom/google/android/exoplayer2/j;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v1, v2

    move-object v2, v13

    .line 235
    goto/16 :goto_2

    .line 227
    :cond_4
    const-string v6, "application/cea-708"

    .line 228
    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    goto :goto_3

    .line 240
    :cond_5
    const-string v1, "#EXT-X-STREAM-INF"

    invoke-virtual {v3, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 241
    sget-object v1, Lcom/google/android/exoplayer2/source/hls/playlist/d;->b:Ljava/util/regex/Pattern;

    invoke-static {v3, v1}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->a(Ljava/lang/String;Ljava/util/regex/Pattern;)I

    move-result v5

    .line 242
    sget-object v1, Lcom/google/android/exoplayer2/source/hls/playlist/d;->a:Ljava/util/regex/Pattern;

    invoke-static {v3, v1}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->c(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v1

    .line 243
    if-eqz v1, :cond_6

    .line 245
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 247
    :cond_6
    sget-object v1, Lcom/google/android/exoplayer2/source/hls/playlist/d;->c:Ljava/util/regex/Pattern;

    invoke-static {v3, v1}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->c(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v4

    .line 248
    sget-object v1, Lcom/google/android/exoplayer2/source/hls/playlist/d;->d:Ljava/util/regex/Pattern;

    invoke-static {v3, v1}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->c(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v1

    .line 249
    const-string v2, "CLOSED-CAPTIONS=NONE"

    invoke-virtual {v3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    or-int v11, v14, v2

    .line 252
    if-eqz v1, :cond_a

    .line 253
    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 254
    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 255
    const/4 v3, 0x1

    aget-object v1, v1, v3

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 256
    if-lez v2, :cond_7

    if-gtz v1, :cond_8

    .line 258
    :cond_7
    const/4 v2, -0x1

    .line 259
    const/4 v1, -0x1

    :cond_8
    move v7, v1

    move v6, v2

    .line 265
    :goto_5
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/source/hls/playlist/d$a;->b()Ljava/lang/String;

    move-result-object v14

    .line 266
    invoke-virtual {v15, v14}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 267
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "application/x-mpegURL"

    const/4 v3, 0x0

    const/high16 v8, -0x40800000    # -1.0f

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static/range {v1 .. v10}, Lcom/google/android/exoplayer2/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIFLjava/util/List;I)Lcom/google/android/exoplayer2/j;

    move-result-object v1

    .line 270
    new-instance v2, Lcom/google/android/exoplayer2/source/hls/playlist/a$a;

    invoke-direct {v2, v14, v1}, Lcom/google/android/exoplayer2/source/hls/playlist/a$a;-><init>(Ljava/lang/String;Lcom/google/android/exoplayer2/j;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_9
    move v14, v11

    .line 272
    goto/16 :goto_0

    .line 262
    :cond_a
    const/4 v6, -0x1

    .line 263
    const/4 v7, -0x1

    goto :goto_5

    .line 274
    :cond_b
    if-eqz v14, :cond_c

    .line 275
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v8

    .line 277
    :goto_6
    new-instance v1, Lcom/google/android/exoplayer2/source/hls/playlist/a;

    move-object/from16 v2, p1

    move-object/from16 v3, v19

    move-object/from16 v4, v16

    move-object/from16 v5, v17

    move-object/from16 v6, v18

    move-object v7, v13

    invoke-direct/range {v1 .. v8}, Lcom/google/android/exoplayer2/source/hls/playlist/a;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/android/exoplayer2/j;Ljava/util/List;)V

    return-object v1

    :cond_c
    move-object v8, v12

    goto :goto_6

    :cond_d
    move-object v2, v12

    goto/16 :goto_4

    .line 204
    :sswitch_data_0
    .sparse-switch
        -0x392db8c5 -> :sswitch_1
        -0x13dc6572 -> :sswitch_2
        0x3bba3b6 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Ljava/io/BufferedReader;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 150
    invoke-virtual {p0}, Ljava/io/BufferedReader;->read()I

    move-result v0

    .line 151
    const/16 v2, 0xef

    if-ne v0, v2, :cond_2

    .line 152
    invoke-virtual {p0}, Ljava/io/BufferedReader;->read()I

    move-result v0

    const/16 v2, 0xbb

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Ljava/io/BufferedReader;->read()I

    move-result v0

    const/16 v2, 0xbf

    if-eq v0, v2, :cond_1

    :cond_0
    move v0, v1

    .line 167
    :goto_0
    return v0

    .line 156
    :cond_1
    invoke-virtual {p0}, Ljava/io/BufferedReader;->read()I

    move-result v0

    .line 158
    :cond_2
    const/4 v2, 0x1

    invoke-static {p0, v2, v0}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->a(Ljava/io/BufferedReader;ZI)I

    move-result v0

    .line 159
    const-string v2, "#EXTM3U"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    move v2, v0

    move v0, v1

    .line 160
    :goto_1
    if-ge v0, v3, :cond_4

    .line 161
    const-string v4, "#EXTM3U"

    invoke-virtual {v4, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-eq v2, v4, :cond_3

    move v0, v1

    .line 162
    goto :goto_0

    .line 164
    :cond_3
    invoke-virtual {p0}, Ljava/io/BufferedReader;->read()I

    move-result v2

    .line 160
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 166
    :cond_4
    invoke-static {p0, v1, v2}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->a(Ljava/io/BufferedReader;ZI)I

    move-result v0

    .line 167
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/s;->a(I)Z

    move-result v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/util/regex/Pattern;Z)Z
    .locals 2

    .prologue
    .line 440
    invoke-virtual {p1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 441
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 442
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "YES"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    .line 444
    :cond_0
    return p2
.end method

.method private static b(Ljava/lang/String;Ljava/util/regex/Pattern;)D
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    .line 423
    invoke-static {p0, p1}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->d(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    return-wide v0
.end method

.method private static b(Lcom/google/android/exoplayer2/source/hls/playlist/d$a;Ljava/lang/String;)Lcom/google/android/exoplayer2/source/hls/playlist/b;
    .locals 39
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 290
    const/16 v27, 0x0

    .line 291
    const-wide v28, -0x7fffffffffffffffL    # -4.9E-324

    .line 292
    const/16 v26, 0x0

    .line 293
    const/16 v23, 0x1

    .line 294
    const-wide v24, -0x7fffffffffffffffL    # -4.9E-324

    .line 295
    const/16 v22, 0x0

    .line 296
    const/16 v21, 0x0

    .line 297
    const/16 v17, 0x0

    .line 298
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 299
    new-instance v38, Ljava/util/ArrayList;

    invoke-direct/range {v38 .. v38}, Ljava/util/ArrayList;-><init>()V

    .line 301
    const-wide/16 v18, 0x0

    .line 302
    const/16 v16, 0x0

    .line 303
    const/4 v11, 0x0

    .line 304
    const/4 v8, 0x0

    .line 305
    const-wide/16 v12, 0x0

    .line 306
    const-wide/16 v6, 0x0

    .line 307
    const-wide/16 v4, 0x0

    .line 308
    const-wide/16 v14, -0x1

    .line 309
    const/4 v3, 0x0

    .line 311
    const/4 v9, 0x0

    .line 312
    const/4 v10, 0x0

    .line 313
    const/4 v2, 0x0

    move-wide/from16 v30, v18

    move-wide/from16 v32, v24

    move/from16 v34, v26

    move-wide/from16 v36, v28

    move/from16 v35, v27

    move/from16 v27, v11

    move/from16 v28, v16

    move-object/from16 v18, v2

    move-object/from16 v19, v17

    move/from16 v29, v23

    move-wide/from16 v24, v12

    move/from16 v26, v8

    move/from16 v17, v21

    move/from16 v16, v22

    move v8, v3

    move-wide/from16 v22, v6

    .line 316
    :cond_0
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/source/hls/playlist/d$a;->a()Z

    move-result v2

    if-eqz v2, :cond_15

    .line 317
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/source/hls/playlist/d$a;->b()Ljava/lang/String;

    move-result-object v6

    .line 319
    const-string v2, "#EXT"

    invoke-virtual {v6, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 321
    move-object/from16 v0, v38

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 324
    :cond_1
    const-string v2, "#EXT-X-PLAYLIST-TYPE"

    invoke-virtual {v6, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 325
    sget-object v2, Lcom/google/android/exoplayer2/source/hls/playlist/d;->g:Ljava/util/regex/Pattern;

    invoke-static {v6, v2}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->d(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v2

    .line 326
    const-string v3, "VOD"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 327
    const/4 v2, 0x1

    :goto_1
    move/from16 v35, v2

    .line 331
    goto :goto_0

    .line 328
    :cond_2
    const-string v3, "EVENT"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 329
    const/4 v2, 0x2

    goto :goto_1

    .line 331
    :cond_3
    const-string v2, "#EXT-X-START"

    invoke-virtual {v6, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 332
    sget-object v2, Lcom/google/android/exoplayer2/source/hls/playlist/d;->j:Ljava/util/regex/Pattern;

    invoke-static {v6, v2}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->b(Ljava/lang/String;Ljava/util/regex/Pattern;)D

    move-result-wide v2

    const-wide v6, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v2, v6

    double-to-long v2, v2

    move-wide/from16 v36, v2

    goto :goto_0

    .line 333
    :cond_4
    const-string v2, "#EXT-X-MAP"

    invoke-virtual {v6, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 334
    sget-object v2, Lcom/google/android/exoplayer2/source/hls/playlist/d;->n:Ljava/util/regex/Pattern;

    invoke-static {v6, v2}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->d(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v3

    .line 335
    sget-object v2, Lcom/google/android/exoplayer2/source/hls/playlist/d;->l:Ljava/util/regex/Pattern;

    invoke-static {v6, v2}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->c(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v2

    .line 336
    if-eqz v2, :cond_1a

    .line 337
    const-string v6, "@"

    invoke-virtual {v2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 338
    const/4 v6, 0x0

    aget-object v6, v2, v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 339
    array-length v11, v2

    const/4 v12, 0x1

    if-le v11, v12, :cond_5

    .line 340
    const/4 v4, 0x1

    aget-object v2, v2, v4

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 343
    :cond_5
    :goto_2
    new-instance v2, Lcom/google/android/exoplayer2/source/hls/playlist/b$a;

    invoke-direct/range {v2 .. v7}, Lcom/google/android/exoplayer2/source/hls/playlist/b$a;-><init>(Ljava/lang/String;JJ)V

    .line 344
    const-wide/16 v4, 0x0

    .line 345
    const-wide/16 v14, -0x1

    move-object/from16 v19, v2

    .line 346
    goto/16 :goto_0

    :cond_6
    const-string v2, "#EXT-X-TARGETDURATION"

    invoke-virtual {v6, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 347
    sget-object v2, Lcom/google/android/exoplayer2/source/hls/playlist/d;->e:Ljava/util/regex/Pattern;

    invoke-static {v6, v2}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->a(Ljava/lang/String;Ljava/util/regex/Pattern;)I

    move-result v2

    int-to-long v2, v2

    const-wide/32 v6, 0xf4240

    mul-long/2addr v2, v6

    move-wide/from16 v32, v2

    goto/16 :goto_0

    .line 348
    :cond_7
    const-string v2, "#EXT-X-MEDIA-SEQUENCE"

    invoke-virtual {v6, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 349
    sget-object v2, Lcom/google/android/exoplayer2/source/hls/playlist/d;->h:Ljava/util/regex/Pattern;

    invoke-static {v6, v2}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->a(Ljava/lang/String;Ljava/util/regex/Pattern;)I

    move-result v2

    move v8, v2

    move/from16 v34, v2

    .line 350
    goto/16 :goto_0

    .line 351
    :cond_8
    const-string v2, "#EXT-X-VERSION"

    invoke-virtual {v6, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 352
    sget-object v2, Lcom/google/android/exoplayer2/source/hls/playlist/d;->f:Ljava/util/regex/Pattern;

    invoke-static {v6, v2}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->a(Ljava/lang/String;Ljava/util/regex/Pattern;)I

    move-result v2

    move/from16 v29, v2

    goto/16 :goto_0

    .line 353
    :cond_9
    const-string v2, "#EXTINF"

    invoke-virtual {v6, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 354
    sget-object v2, Lcom/google/android/exoplayer2/source/hls/playlist/d;->i:Ljava/util/regex/Pattern;

    .line 355
    invoke-static {v6, v2}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->b(Ljava/lang/String;Ljava/util/regex/Pattern;)D

    move-result-wide v2

    const-wide v6, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v2, v6

    double-to-long v2, v2

    move-wide/from16 v30, v2

    goto/16 :goto_0

    .line 356
    :cond_a
    const-string v2, "#EXT-X-KEY"

    invoke-virtual {v6, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 357
    sget-object v2, Lcom/google/android/exoplayer2/source/hls/playlist/d;->m:Ljava/util/regex/Pattern;

    invoke-static {v6, v2}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->d(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v2

    .line 358
    const-string v3, "AES-128"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    .line 359
    if-eqz v9, :cond_b

    .line 360
    sget-object v2, Lcom/google/android/exoplayer2/source/hls/playlist/d;->n:Ljava/util/regex/Pattern;

    invoke-static {v6, v2}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->d(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v3

    .line 361
    sget-object v2, Lcom/google/android/exoplayer2/source/hls/playlist/d;->o:Ljava/util/regex/Pattern;

    invoke-static {v6, v2}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->c(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v2

    :goto_3
    move-object/from16 v18, v2

    move-object v10, v3

    .line 366
    goto/16 :goto_0

    .line 363
    :cond_b
    const/4 v3, 0x0

    .line 364
    const/4 v2, 0x0

    goto :goto_3

    .line 366
    :cond_c
    const-string v2, "#EXT-X-BYTERANGE"

    invoke-virtual {v6, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 367
    sget-object v2, Lcom/google/android/exoplayer2/source/hls/playlist/d;->k:Ljava/util/regex/Pattern;

    invoke-static {v6, v2}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->d(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v2

    .line 368
    const-string v3, "@"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 369
    const/4 v3, 0x0

    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v14

    .line 370
    array-length v3, v2

    const/4 v6, 0x1

    if-le v3, v6, :cond_19

    .line 371
    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    :goto_4
    move-wide v4, v2

    .line 373
    goto/16 :goto_0

    :cond_d
    const-string v2, "#EXT-X-DISCONTINUITY-SEQUENCE"

    invoke-virtual {v6, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 374
    const/4 v3, 0x1

    .line 375
    const/16 v2, 0x3a

    invoke-virtual {v6, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v6, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    move/from16 v27, v2

    move/from16 v28, v3

    goto/16 :goto_0

    .line 376
    :cond_e
    const-string v2, "#EXT-X-DISCONTINUITY"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 377
    add-int/lit8 v2, v26, 0x1

    move/from16 v26, v2

    goto/16 :goto_0

    .line 378
    :cond_f
    const-string v2, "#EXT-X-PROGRAM-DATE-TIME"

    invoke-virtual {v6, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 379
    const-wide/16 v2, 0x0

    cmp-long v2, v24, v2

    if-nez v2, :cond_0

    .line 380
    const/16 v2, 0x3a

    .line 381
    invoke-virtual {v6, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v6, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/exoplayer2/util/s;->e(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/exoplayer2/b;->b(J)J

    move-result-wide v2

    .line 382
    sub-long v2, v2, v22

    move-wide/from16 v24, v2

    .line 383
    goto/16 :goto_0

    .line 384
    :cond_10
    const-string v2, "#"

    invoke-virtual {v6, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_13

    .line 386
    if-nez v9, :cond_11

    .line 387
    const/4 v11, 0x0

    .line 393
    :goto_5
    add-int/lit8 v21, v8, 0x1

    .line 394
    const-wide/16 v2, -0x1

    cmp-long v2, v14, v2

    if-nez v2, :cond_18

    .line 395
    const-wide/16 v12, 0x0

    .line 397
    :goto_6
    new-instance v2, Lcom/google/android/exoplayer2/source/hls/playlist/b$a;

    move-object v3, v6

    move-wide/from16 v4, v30

    move/from16 v6, v26

    move-wide/from16 v7, v22

    invoke-direct/range {v2 .. v15}, Lcom/google/android/exoplayer2/source/hls/playlist/b$a;-><init>(Ljava/lang/String;JIJZLjava/lang/String;Ljava/lang/String;JJ)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 400
    add-long v4, v22, v30

    .line 401
    const-wide/16 v6, 0x0

    .line 402
    const-wide/16 v2, -0x1

    cmp-long v2, v14, v2

    if-eqz v2, :cond_17

    .line 403
    add-long v2, v12, v14

    .line 405
    :goto_7
    const-wide/16 v14, -0x1

    move/from16 v8, v21

    move-wide/from16 v22, v4

    move-wide/from16 v30, v6

    move-wide v4, v2

    .line 406
    goto/16 :goto_0

    .line 388
    :cond_11
    if-eqz v18, :cond_12

    move-object/from16 v11, v18

    .line 389
    goto :goto_5

    .line 391
    :cond_12
    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v11

    goto :goto_5

    .line 406
    :cond_13
    const-string v2, "#EXT-X-INDEPENDENT-SEGMENTS"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 407
    const/16 v16, 0x1

    goto/16 :goto_0

    .line 408
    :cond_14
    const-string v2, "#EXT-X-ENDLIST"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 409
    const/16 v17, 0x1

    goto/16 :goto_0

    .line 412
    :cond_15
    new-instance v2, Lcom/google/android/exoplayer2/source/hls/playlist/b;

    const-wide/16 v4, 0x0

    cmp-long v3, v24, v4

    if-eqz v3, :cond_16

    const/16 v18, 0x1

    :goto_8
    move/from16 v3, v35

    move-object/from16 v4, p1

    move-object/from16 v5, v38

    move-wide/from16 v6, v36

    move-wide/from16 v8, v24

    move/from16 v10, v28

    move/from16 v11, v27

    move/from16 v12, v34

    move/from16 v13, v29

    move-wide/from16 v14, v32

    invoke-direct/range {v2 .. v20}, Lcom/google/android/exoplayer2/source/hls/playlist/b;-><init>(ILjava/lang/String;Ljava/util/List;JJZIIIJZZZLcom/google/android/exoplayer2/source/hls/playlist/b$a;Ljava/util/List;)V

    return-object v2

    :cond_16
    const/16 v18, 0x0

    goto :goto_8

    :cond_17
    move-wide v2, v12

    goto :goto_7

    :cond_18
    move-wide v12, v4

    goto :goto_6

    :cond_19
    move-wide v2, v4

    goto/16 :goto_4

    :cond_1a
    move-wide v6, v14

    goto/16 :goto_2

    :cond_1b
    move/from16 v2, v35

    goto/16 :goto_1
.end method

.method private static b(Ljava/lang/String;)Ljava/util/regex/Pattern;
    .locals 2

    .prologue
    .line 448
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "NO"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "YES"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    return-object v0
.end method

.method private static c(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 427
    invoke-virtual {p1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 428
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 432
    invoke-virtual {p1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 433
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 434
    invoke-virtual {v0, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 436
    :cond_0
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Couldn\'t match "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/regex/Pattern;->pattern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a(Landroid/net/Uri;Ljava/io/InputStream;)Lcom/google/android/exoplayer2/source/hls/playlist/c;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, p2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 115
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 118
    :try_start_0
    invoke-static {v1}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->a(Ljava/io/BufferedReader;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 119
    new-instance v0, Lcom/google/android/exoplayer2/source/UnrecognizedInputFormatException;

    const-string v2, "Input does not start with the #EXTM3U header."

    invoke-direct {v0, v2, p1}, Lcom/google/android/exoplayer2/source/UnrecognizedInputFormatException;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    :catchall_0
    move-exception v0

    invoke-static {v1}, Lcom/google/android/exoplayer2/util/s;->a(Ljava/io/Closeable;)V

    throw v0

    .line 122
    :cond_0
    :goto_0
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 123
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 124
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 126
    const-string v3, "#EXT-X-STREAM-INF"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 127
    invoke-interface {v0, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 128
    new-instance v2, Lcom/google/android/exoplayer2/source/hls/playlist/d$a;

    invoke-direct {v2, v0, v1}, Lcom/google/android/exoplayer2/source/hls/playlist/d$a;-><init>(Ljava/util/Queue;Ljava/io/BufferedReader;)V

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->a(Lcom/google/android/exoplayer2/source/hls/playlist/d$a;Ljava/lang/String;)Lcom/google/android/exoplayer2/source/hls/playlist/a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 144
    invoke-static {v1}, Lcom/google/android/exoplayer2/util/s;->a(Ljava/io/Closeable;)V

    :goto_1
    return-object v0

    .line 129
    :cond_1
    :try_start_2
    const-string v3, "#EXT-X-TARGETDURATION"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "#EXT-X-MEDIA-SEQUENCE"

    .line 130
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "#EXTINF"

    .line 131
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "#EXT-X-KEY"

    .line 132
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "#EXT-X-BYTERANGE"

    .line 133
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "#EXT-X-DISCONTINUITY"

    .line 134
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "#EXT-X-DISCONTINUITY-SEQUENCE"

    .line 135
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "#EXT-X-ENDLIST"

    .line 136
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 137
    :cond_2
    invoke-interface {v0, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 138
    new-instance v2, Lcom/google/android/exoplayer2/source/hls/playlist/d$a;

    invoke-direct {v2, v0, v1}, Lcom/google/android/exoplayer2/source/hls/playlist/d$a;-><init>(Ljava/util/Queue;Ljava/io/BufferedReader;)V

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->b(Lcom/google/android/exoplayer2/source/hls/playlist/d$a;Ljava/lang/String;)Lcom/google/android/exoplayer2/source/hls/playlist/b;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 144
    invoke-static {v1}, Lcom/google/android/exoplayer2/util/s;->a(Ljava/io/Closeable;)V

    goto :goto_1

    .line 140
    :cond_3
    :try_start_3
    invoke-interface {v0, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 144
    :cond_4
    invoke-static {v1}, Lcom/google/android/exoplayer2/util/s;->a(Ljava/io/Closeable;)V

    .line 146
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Failed to parse the playlist, could not identify any tags."

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public synthetic b(Landroid/net/Uri;Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    invoke-virtual {p0, p1, p2}, Lcom/google/android/exoplayer2/source/hls/playlist/d;->a(Landroid/net/Uri;Ljava/io/InputStream;)Lcom/google/android/exoplayer2/source/hls/playlist/c;

    move-result-object v0

    return-object v0
.end method
