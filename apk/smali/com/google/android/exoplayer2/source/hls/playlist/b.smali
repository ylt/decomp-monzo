.class public final Lcom/google/android/exoplayer2/source/hls/playlist/b;
.super Lcom/google/android/exoplayer2/source/hls/playlist/c;
.source "HlsMediaPlaylist.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/source/hls/playlist/b$a;
    }
.end annotation


# instance fields
.field public final a:I

.field public final b:J

.field public final c:J

.field public final d:Z

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:J

.field public final i:Z

.field public final j:Z

.field public final k:Z

.field public final l:Lcom/google/android/exoplayer2/source/hls/playlist/b$a;

.field public final m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer2/source/hls/playlist/b$a;",
            ">;"
        }
    .end annotation
.end field

.field public final n:J


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/util/List;JJZIIIJZZZLcom/google/android/exoplayer2/source/hls/playlist/b$a;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;JJZIIIJZZZ",
            "Lcom/google/android/exoplayer2/source/hls/playlist/b$a;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer2/source/hls/playlist/b$a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 203
    invoke-direct {p0, p2, p3}, Lcom/google/android/exoplayer2/source/hls/playlist/c;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 204
    iput p1, p0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->a:I

    .line 205
    iput-wide p6, p0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->c:J

    .line 206
    iput-boolean p8, p0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->d:Z

    .line 207
    iput p9, p0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->e:I

    .line 208
    move/from16 v0, p10

    iput v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->f:I

    .line 209
    move/from16 v0, p11

    iput v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->g:I

    .line 210
    move-wide/from16 v0, p12

    iput-wide v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->h:J

    .line 211
    move/from16 v0, p14

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->i:Z

    .line 212
    move/from16 v0, p15

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->j:Z

    .line 213
    move/from16 v0, p16

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->k:Z

    .line 214
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->l:Lcom/google/android/exoplayer2/source/hls/playlist/b$a;

    .line 215
    invoke-static/range {p18 .. p18}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->m:Ljava/util/List;

    .line 216
    invoke-interface/range {p18 .. p18}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 217
    invoke-interface/range {p18 .. p18}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p18

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/source/hls/playlist/b$a;

    .line 218
    iget-wide v4, v2, Lcom/google/android/exoplayer2/source/hls/playlist/b$a;->d:J

    iget-wide v2, v2, Lcom/google/android/exoplayer2/source/hls/playlist/b$a;->b:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->n:J

    .line 222
    :goto_0
    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v2, p4, v2

    if-nez v2, :cond_2

    const-wide p4, -0x7fffffffffffffffL    # -4.9E-324

    :cond_0
    :goto_1
    iput-wide p4, p0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->b:J

    .line 224
    return-void

    .line 220
    :cond_1
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->n:J

    goto :goto_0

    .line 222
    :cond_2
    const-wide/16 v2, 0x0

    cmp-long v2, p4, v2

    if-gez v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->n:J

    add-long/2addr p4, v2

    goto :goto_1
.end method


# virtual methods
.method public a()J
    .locals 4

    .prologue
    .line 250
    iget-wide v0, p0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->c:J

    iget-wide v2, p0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->n:J

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public a(JI)Lcom/google/android/exoplayer2/source/hls/playlist/b;
    .locals 21

    .prologue
    .line 263
    new-instance v2, Lcom/google/android/exoplayer2/source/hls/playlist/b;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->a:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->o:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->p:Ljava/util/List;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->b:J

    const/4 v10, 0x1

    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->f:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->g:I

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->h:J

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->i:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->j:Z

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->k:Z

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->l:Lcom/google/android/exoplayer2/source/hls/playlist/b$a;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->m:Ljava/util/List;

    move-object/from16 v20, v0

    move-wide/from16 v8, p1

    move/from16 v11, p3

    invoke-direct/range {v2 .. v20}, Lcom/google/android/exoplayer2/source/hls/playlist/b;-><init>(ILjava/lang/String;Ljava/util/List;JJZIIIJZZZLcom/google/android/exoplayer2/source/hls/playlist/b$a;Ljava/util/List;)V

    return-object v2
.end method

.method public a(Lcom/google/android/exoplayer2/source/hls/playlist/b;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 233
    if-eqz p1, :cond_0

    iget v2, p0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->f:I

    iget v3, p1, Lcom/google/android/exoplayer2/source/hls/playlist/b;->f:I

    if-le v2, v3, :cond_2

    :cond_0
    move v0, v1

    .line 242
    :cond_1
    :goto_0
    return v0

    .line 236
    :cond_2
    iget v2, p0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->f:I

    iget v3, p1, Lcom/google/android/exoplayer2/source/hls/playlist/b;->f:I

    if-lt v2, v3, :cond_1

    .line 240
    iget-object v2, p0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->m:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 241
    iget-object v3, p1, Lcom/google/android/exoplayer2/source/hls/playlist/b;->m:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    .line 242
    if-gt v2, v3, :cond_3

    if-ne v2, v3, :cond_1

    iget-boolean v2, p0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->j:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p1, Lcom/google/android/exoplayer2/source/hls/playlist/b;->j:Z

    if-nez v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public b()Lcom/google/android/exoplayer2/source/hls/playlist/b;
    .locals 21

    .prologue
    .line 275
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->j:Z

    if-eqz v2, :cond_0

    .line 278
    :goto_0
    return-object p0

    :cond_0
    new-instance v2, Lcom/google/android/exoplayer2/source/hls/playlist/b;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->a:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->o:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->p:Ljava/util/List;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->b:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->c:J

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->d:Z

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->e:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->f:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->g:I

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->h:J

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->i:Z

    move/from16 v16, v0

    const/16 v17, 0x1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->k:Z

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->l:Lcom/google/android/exoplayer2/source/hls/playlist/b$a;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/hls/playlist/b;->m:Ljava/util/List;

    move-object/from16 v20, v0

    invoke-direct/range {v2 .. v20}, Lcom/google/android/exoplayer2/source/hls/playlist/b;-><init>(ILjava/lang/String;Ljava/util/List;JJZIIIJZZZLcom/google/android/exoplayer2/source/hls/playlist/b$a;Ljava/util/List;)V

    move-object/from16 p0, v2

    goto :goto_0
.end method
