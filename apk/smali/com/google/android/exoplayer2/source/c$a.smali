.class final Lcom/google/android/exoplayer2/source/c$a;
.super Ljava/lang/Object;
.source "ClippingMediaPeriod.java"

# interfaces
.implements Lcom/google/android/exoplayer2/source/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/source/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/google/android/exoplayer2/source/h;

.field private final b:Lcom/google/android/exoplayer2/source/m;

.field private final c:J

.field private final d:J

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/source/h;Lcom/google/android/exoplayer2/source/m;JJZ)V
    .locals 1

    .prologue
    .line 236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 237
    iput-object p1, p0, Lcom/google/android/exoplayer2/source/c$a;->a:Lcom/google/android/exoplayer2/source/h;

    .line 238
    iput-object p2, p0, Lcom/google/android/exoplayer2/source/c$a;->b:Lcom/google/android/exoplayer2/source/m;

    .line 239
    iput-wide p3, p0, Lcom/google/android/exoplayer2/source/c$a;->c:J

    .line 240
    iput-wide p5, p0, Lcom/google/android/exoplayer2/source/c$a;->d:J

    .line 241
    iput-boolean p7, p0, Lcom/google/android/exoplayer2/source/c$a;->e:Z

    .line 242
    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/source/c$a;)Lcom/google/android/exoplayer2/source/m;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/c$a;->b:Lcom/google/android/exoplayer2/source/m;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/android/exoplayer2/k;Lcom/google/android/exoplayer2/a/e;Z)I
    .locals 12

    .prologue
    const-wide/high16 v10, -0x8000000000000000L

    const/4 v8, 0x4

    const/4 v0, -0x3

    const/4 v1, -0x4

    .line 265
    iget-boolean v2, p0, Lcom/google/android/exoplayer2/source/c$a;->e:Z

    if-eqz v2, :cond_0

    .line 285
    :goto_0
    return v0

    .line 268
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/exoplayer2/source/c$a;->f:Z

    if-eqz v2, :cond_1

    .line 269
    invoke-virtual {p2, v8}, Lcom/google/android/exoplayer2/a/e;->b_(I)V

    move v0, v1

    .line 270
    goto :goto_0

    .line 272
    :cond_1
    iget-object v2, p0, Lcom/google/android/exoplayer2/source/c$a;->b:Lcom/google/android/exoplayer2/source/m;

    invoke-interface {v2, p1, p2, p3}, Lcom/google/android/exoplayer2/source/m;->a(Lcom/google/android/exoplayer2/k;Lcom/google/android/exoplayer2/a/e;Z)I

    move-result v2

    .line 274
    iget-wide v4, p0, Lcom/google/android/exoplayer2/source/c$a;->d:J

    cmp-long v3, v4, v10

    if-eqz v3, :cond_4

    if-ne v2, v1, :cond_2

    iget-wide v4, p2, Lcom/google/android/exoplayer2/a/e;->c:J

    iget-wide v6, p0, Lcom/google/android/exoplayer2/source/c$a;->d:J

    cmp-long v3, v4, v6

    if-gez v3, :cond_3

    :cond_2
    if-ne v2, v0, :cond_4

    iget-object v0, p0, Lcom/google/android/exoplayer2/source/c$a;->a:Lcom/google/android/exoplayer2/source/h;

    .line 276
    invoke-interface {v0}, Lcom/google/android/exoplayer2/source/h;->d()J

    move-result-wide v4

    cmp-long v0, v4, v10

    if-nez v0, :cond_4

    .line 277
    :cond_3
    invoke-virtual {p2}, Lcom/google/android/exoplayer2/a/e;->a()V

    .line 278
    invoke-virtual {p2, v8}, Lcom/google/android/exoplayer2/a/e;->b_(I)V

    .line 279
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/c$a;->f:Z

    move v0, v1

    .line 280
    goto :goto_0

    .line 282
    :cond_4
    if-ne v2, v1, :cond_5

    invoke-virtual {p2}, Lcom/google/android/exoplayer2/a/e;->c()Z

    move-result v0

    if-nez v0, :cond_5

    .line 283
    iget-wide v0, p2, Lcom/google/android/exoplayer2/a/e;->c:J

    iget-wide v4, p0, Lcom/google/android/exoplayer2/source/c$a;->c:J

    sub-long/2addr v0, v4

    iput-wide v0, p2, Lcom/google/android/exoplayer2/a/e;->c:J

    :cond_5
    move v0, v2

    .line 285
    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 245
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/c$a;->e:Z

    .line 246
    return-void
.end method

.method public a(J)V
    .locals 5

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/c$a;->b:Lcom/google/android/exoplayer2/source/m;

    iget-wide v2, p0, Lcom/google/android/exoplayer2/source/c$a;->c:J

    add-long/2addr v2, p1

    invoke-interface {v0, v2, v3}, Lcom/google/android/exoplayer2/source/m;->a(J)V

    .line 291
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 249
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/source/c$a;->f:Z

    .line 250
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/c$a;->b:Lcom/google/android/exoplayer2/source/m;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/source/m;->c()Z

    move-result v0

    return v0
.end method

.method public d()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/c$a;->b:Lcom/google/android/exoplayer2/source/m;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/source/m;->d()V

    .line 260
    return-void
.end method
