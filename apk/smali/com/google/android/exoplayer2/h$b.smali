.class public final Lcom/google/android/exoplayer2/h$b;
.super Ljava/lang/Object;
.source "ExoPlayerImplInternal.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field public final a:Lcom/google/android/exoplayer2/source/i$b;

.field public final b:J

.field public final c:J

.field public volatile d:J

.field public volatile e:J


# direct methods
.method public constructor <init>(IJ)V
    .locals 2

    .prologue
    .line 63
    new-instance v0, Lcom/google/android/exoplayer2/source/i$b;

    invoke-direct {v0, p1}, Lcom/google/android/exoplayer2/source/i$b;-><init>(I)V

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/exoplayer2/h$b;-><init>(Lcom/google/android/exoplayer2/source/i$b;J)V

    .line 64
    return-void
.end method

.method public constructor <init>(Lcom/google/android/exoplayer2/source/i$b;J)V
    .locals 6

    .prologue
    .line 67
    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/h$b;-><init>(Lcom/google/android/exoplayer2/source/i$b;JJ)V

    .line 68
    return-void
.end method

.method public constructor <init>(Lcom/google/android/exoplayer2/source/i$b;JJ)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, Lcom/google/android/exoplayer2/h$b;->a:Lcom/google/android/exoplayer2/source/i$b;

    .line 72
    iput-wide p2, p0, Lcom/google/android/exoplayer2/h$b;->b:J

    .line 73
    iput-wide p4, p0, Lcom/google/android/exoplayer2/h$b;->c:J

    .line 74
    iput-wide p2, p0, Lcom/google/android/exoplayer2/h$b;->d:J

    .line 75
    iput-wide p2, p0, Lcom/google/android/exoplayer2/h$b;->e:J

    .line 76
    return-void
.end method


# virtual methods
.method public a(I)Lcom/google/android/exoplayer2/h$b;
    .locals 6

    .prologue
    .line 79
    new-instance v0, Lcom/google/android/exoplayer2/h$b;

    iget-object v1, p0, Lcom/google/android/exoplayer2/h$b;->a:Lcom/google/android/exoplayer2/source/i$b;

    invoke-virtual {v1, p1}, Lcom/google/android/exoplayer2/source/i$b;->a(I)Lcom/google/android/exoplayer2/source/i$b;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/exoplayer2/h$b;->b:J

    iget-wide v4, p0, Lcom/google/android/exoplayer2/h$b;->c:J

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/h$b;-><init>(Lcom/google/android/exoplayer2/source/i$b;JJ)V

    .line 81
    iget-wide v2, p0, Lcom/google/android/exoplayer2/h$b;->d:J

    iput-wide v2, v0, Lcom/google/android/exoplayer2/h$b;->d:J

    .line 82
    iget-wide v2, p0, Lcom/google/android/exoplayer2/h$b;->e:J

    iput-wide v2, v0, Lcom/google/android/exoplayer2/h$b;->e:J

    .line 83
    return-object v0
.end method
