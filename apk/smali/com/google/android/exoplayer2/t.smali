.class public Lcom/google/android/exoplayer2/t;
.super Ljava/lang/Object;
.source "SimpleExoPlayer.java"

# interfaces
.implements Lcom/google/android/exoplayer2/e;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/t$a;,
        Lcom/google/android/exoplayer2/t$b;
    }
.end annotation


# instance fields
.field protected final a:[Lcom/google/android/exoplayer2/p;

.field private final b:Lcom/google/android/exoplayer2/e;

.field private final c:Lcom/google/android/exoplayer2/t$a;

.field private final d:I

.field private final e:I

.field private f:Lcom/google/android/exoplayer2/j;

.field private g:Lcom/google/android/exoplayer2/j;

.field private h:Landroid/view/Surface;

.field private i:Z

.field private j:I

.field private k:Landroid/view/SurfaceHolder;

.field private l:Landroid/view/TextureView;

.field private m:Lcom/google/android/exoplayer2/text/j$a;

.field private n:Lcom/google/android/exoplayer2/metadata/e$a;

.field private o:Lcom/google/android/exoplayer2/t$b;

.field private p:Lcom/google/android/exoplayer2/audio/d;

.field private q:Lcom/google/android/exoplayer2/c/g;

.field private r:Lcom/google/android/exoplayer2/a/d;

.field private s:Lcom/google/android/exoplayer2/a/d;

.field private t:I

.field private u:Lcom/google/android/exoplayer2/audio/b;

.field private v:F


# direct methods
.method protected constructor <init>(Lcom/google/android/exoplayer2/s;Lcom/google/android/exoplayer2/b/g;Lcom/google/android/exoplayer2/l;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    new-instance v0, Lcom/google/android/exoplayer2/t$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/exoplayer2/t$a;-><init>(Lcom/google/android/exoplayer2/t;Lcom/google/android/exoplayer2/t$1;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/t;->c:Lcom/google/android/exoplayer2/t$a;

    .line 116
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    .line 117
    :goto_0
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 118
    iget-object v2, p0, Lcom/google/android/exoplayer2/t;->c:Lcom/google/android/exoplayer2/t$a;

    iget-object v3, p0, Lcom/google/android/exoplayer2/t;->c:Lcom/google/android/exoplayer2/t$a;

    iget-object v4, p0, Lcom/google/android/exoplayer2/t;->c:Lcom/google/android/exoplayer2/t$a;

    iget-object v5, p0, Lcom/google/android/exoplayer2/t;->c:Lcom/google/android/exoplayer2/t$a;

    move-object v0, p1

    invoke-interface/range {v0 .. v5}, Lcom/google/android/exoplayer2/s;->a(Landroid/os/Handler;Lcom/google/android/exoplayer2/c/g;Lcom/google/android/exoplayer2/audio/d;Lcom/google/android/exoplayer2/text/j$a;Lcom/google/android/exoplayer2/metadata/e$a;)[Lcom/google/android/exoplayer2/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/t;->a:[Lcom/google/android/exoplayer2/p;

    .line 124
    iget-object v3, p0, Lcom/google/android/exoplayer2/t;->a:[Lcom/google/android/exoplayer2/p;

    array-length v4, v3

    move v2, v6

    move v0, v6

    move v1, v6

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 125
    invoke-interface {v5}, Lcom/google/android/exoplayer2/p;->a()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 124
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 116
    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    goto :goto_0

    .line 127
    :pswitch_0
    add-int/lit8 v1, v1, 0x1

    .line 128
    goto :goto_2

    .line 130
    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 134
    :cond_1
    iput v1, p0, Lcom/google/android/exoplayer2/t;->d:I

    .line 135
    iput v0, p0, Lcom/google/android/exoplayer2/t;->e:I

    .line 138
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/exoplayer2/t;->v:F

    .line 139
    iput v6, p0, Lcom/google/android/exoplayer2/t;->t:I

    .line 140
    sget-object v0, Lcom/google/android/exoplayer2/audio/b;->a:Lcom/google/android/exoplayer2/audio/b;

    iput-object v0, p0, Lcom/google/android/exoplayer2/t;->u:Lcom/google/android/exoplayer2/audio/b;

    .line 141
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/exoplayer2/t;->j:I

    .line 144
    new-instance v0, Lcom/google/android/exoplayer2/g;

    iget-object v1, p0, Lcom/google/android/exoplayer2/t;->a:[Lcom/google/android/exoplayer2/p;

    invoke-direct {v0, v1, p2, p3}, Lcom/google/android/exoplayer2/g;-><init>([Lcom/google/android/exoplayer2/p;Lcom/google/android/exoplayer2/b/g;Lcom/google/android/exoplayer2/l;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/t;->b:Lcom/google/android/exoplayer2/e;

    .line 145
    return-void

    .line 125
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/t;I)I
    .locals 0

    .prologue
    .line 50
    iput p1, p0, Lcom/google/android/exoplayer2/t;->t:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/t;Lcom/google/android/exoplayer2/a/d;)Lcom/google/android/exoplayer2/a/d;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/google/android/exoplayer2/t;->r:Lcom/google/android/exoplayer2/a/d;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/c/g;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->q:Lcom/google/android/exoplayer2/c/g;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/t;Lcom/google/android/exoplayer2/j;)Lcom/google/android/exoplayer2/j;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/google/android/exoplayer2/t;->f:Lcom/google/android/exoplayer2/j;

    return-object p1
.end method

.method private a(Landroid/view/Surface;Z)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 744
    iget v1, p0, Lcom/google/android/exoplayer2/t;->d:I

    new-array v3, v1, [Lcom/google/android/exoplayer2/e$c;

    .line 746
    iget-object v4, p0, Lcom/google/android/exoplayer2/t;->a:[Lcom/google/android/exoplayer2/p;

    array-length v5, v4

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v6, v4, v2

    .line 747
    invoke-interface {v6}, Lcom/google/android/exoplayer2/p;->a()I

    move-result v0

    const/4 v7, 0x2

    if-ne v0, v7, :cond_3

    .line 748
    add-int/lit8 v0, v1, 0x1

    new-instance v7, Lcom/google/android/exoplayer2/e$c;

    const/4 v8, 0x1

    invoke-direct {v7, v6, v8, p1}, Lcom/google/android/exoplayer2/e$c;-><init>(Lcom/google/android/exoplayer2/e$b;ILjava/lang/Object;)V

    aput-object v7, v3, v1

    .line 746
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 751
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->h:Landroid/view/Surface;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->h:Landroid/view/Surface;

    if-eq v0, p1, :cond_2

    .line 753
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->b:Lcom/google/android/exoplayer2/e;

    invoke-interface {v0, v3}, Lcom/google/android/exoplayer2/e;->b([Lcom/google/android/exoplayer2/e$c;)V

    .line 755
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/t;->i:Z

    if-eqz v0, :cond_1

    .line 756
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->h:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 761
    :cond_1
    :goto_2
    iput-object p1, p0, Lcom/google/android/exoplayer2/t;->h:Landroid/view/Surface;

    .line 762
    iput-boolean p2, p0, Lcom/google/android/exoplayer2/t;->i:Z

    .line 763
    return-void

    .line 759
    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->b:Lcom/google/android/exoplayer2/e;

    invoke-interface {v0, v3}, Lcom/google/android/exoplayer2/e;->a([Lcom/google/android/exoplayer2/e$c;)V

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/t;Landroid/view/Surface;Z)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/t;->a(Landroid/view/Surface;Z)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/exoplayer2/t;Lcom/google/android/exoplayer2/a/d;)Lcom/google/android/exoplayer2/a/d;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/google/android/exoplayer2/t;->s:Lcom/google/android/exoplayer2/a/d;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/exoplayer2/t;Lcom/google/android/exoplayer2/j;)Lcom/google/android/exoplayer2/j;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/google/android/exoplayer2/t;->g:Lcom/google/android/exoplayer2/j;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/t$b;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->o:Lcom/google/android/exoplayer2/t$b;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/exoplayer2/t;)Landroid/view/Surface;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->h:Landroid/view/Surface;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/audio/d;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->p:Lcom/google/android/exoplayer2/audio/d;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/text/j$a;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->m:Lcom/google/android/exoplayer2/text/j$a;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/metadata/e$a;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->n:Lcom/google/android/exoplayer2/metadata/e$a;

    return-object v0
.end method

.method private m()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 727
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->l:Landroid/view/TextureView;

    if-eqz v0, :cond_0

    .line 728
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->l:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->getSurfaceTextureListener()Landroid/view/TextureView$SurfaceTextureListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer2/t;->c:Lcom/google/android/exoplayer2/t$a;

    if-eq v0, v1, :cond_2

    .line 729
    const-string v0, "SimpleExoPlayer"

    const-string v1, "SurfaceTextureListener already unset or replaced."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 733
    :goto_0
    iput-object v2, p0, Lcom/google/android/exoplayer2/t;->l:Landroid/view/TextureView;

    .line 735
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->k:Landroid/view/SurfaceHolder;

    if-eqz v0, :cond_1

    .line 736
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->k:Landroid/view/SurfaceHolder;

    iget-object v1, p0, Lcom/google/android/exoplayer2/t;->c:Lcom/google/android/exoplayer2/t$a;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->removeCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 737
    iput-object v2, p0, Lcom/google/android/exoplayer2/t;->k:Landroid/view/SurfaceHolder;

    .line 739
    :cond_1
    return-void

    .line 731
    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->l:Landroid/view/TextureView;

    invoke-virtual {v0, v2}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 539
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->b:Lcom/google/android/exoplayer2/e;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/e;->a()I

    move-result v0

    return v0
.end method

.method public a(F)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 370
    iput p1, p0, Lcom/google/android/exoplayer2/t;->v:F

    .line 371
    iget v1, p0, Lcom/google/android/exoplayer2/t;->e:I

    new-array v3, v1, [Lcom/google/android/exoplayer2/e$c;

    .line 373
    iget-object v4, p0, Lcom/google/android/exoplayer2/t;->a:[Lcom/google/android/exoplayer2/p;

    array-length v5, v4

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v6, v4, v2

    .line 374
    invoke-interface {v6}, Lcom/google/android/exoplayer2/p;->a()I

    move-result v0

    const/4 v7, 0x1

    if-ne v0, v7, :cond_1

    .line 375
    add-int/lit8 v0, v1, 0x1

    new-instance v7, Lcom/google/android/exoplayer2/e$c;

    const/4 v8, 0x2

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-direct {v7, v6, v8, v9}, Lcom/google/android/exoplayer2/e$c;-><init>(Lcom/google/android/exoplayer2/e$b;ILjava/lang/Object;)V

    aput-object v7, v3, v1

    .line 373
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 378
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->b:Lcom/google/android/exoplayer2/e;

    invoke-interface {v0, v3}, Lcom/google/android/exoplayer2/e;->a([Lcom/google/android/exoplayer2/e$c;)V

    .line 379
    return-void

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 569
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->b:Lcom/google/android/exoplayer2/e;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/e;->a(I)V

    .line 570
    return-void
.end method

.method public a(IJ)V
    .locals 2

    .prologue
    .line 594
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->b:Lcom/google/android/exoplayer2/e;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/exoplayer2/e;->a(IJ)V

    .line 595
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 589
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->b:Lcom/google/android/exoplayer2/e;

    invoke-interface {v0, p1, p2}, Lcom/google/android/exoplayer2/e;->a(J)V

    .line 590
    return-void
.end method

.method public a(Landroid/view/SurfaceHolder;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 220
    invoke-direct {p0}, Lcom/google/android/exoplayer2/t;->m()V

    .line 221
    iput-object p1, p0, Lcom/google/android/exoplayer2/t;->k:Landroid/view/SurfaceHolder;

    .line 222
    if-nez p1, :cond_0

    .line 223
    invoke-direct {p0, v1, v3}, Lcom/google/android/exoplayer2/t;->a(Landroid/view/Surface;Z)V

    .line 229
    :goto_0
    return-void

    .line 225
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->c:Lcom/google/android/exoplayer2/t$a;

    invoke-interface {p1, v0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 226
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    .line 227
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/Surface;->isValid()Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    invoke-direct {p0, v0, v3}, Lcom/google/android/exoplayer2/t;->a(Landroid/view/Surface;Z)V

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public a(Landroid/view/SurfaceView;)V
    .locals 1

    .prologue
    .line 250
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/t;->a(Landroid/view/SurfaceHolder;)V

    .line 251
    return-void

    .line 250
    :cond_0
    invoke-virtual {p1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/view/TextureView;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 270
    invoke-direct {p0}, Lcom/google/android/exoplayer2/t;->m()V

    .line 271
    iput-object p1, p0, Lcom/google/android/exoplayer2/t;->l:Landroid/view/TextureView;

    .line 272
    if-nez p1, :cond_0

    .line 273
    invoke-direct {p0, v0, v3}, Lcom/google/android/exoplayer2/t;->a(Landroid/view/Surface;Z)V

    .line 283
    :goto_0
    return-void

    .line 275
    :cond_0
    invoke-virtual {p1}, Landroid/view/TextureView;->getSurfaceTextureListener()Landroid/view/TextureView$SurfaceTextureListener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 276
    const-string v1, "SimpleExoPlayer"

    const-string v2, "Replacing existing SurfaceTextureListener."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    :cond_1
    iget-object v1, p0, Lcom/google/android/exoplayer2/t;->c:Lcom/google/android/exoplayer2/t$a;

    invoke-virtual {p1, v1}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 279
    invoke-virtual {p1}, Landroid/view/TextureView;->isAvailable()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Landroid/view/TextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v1

    .line 281
    :goto_1
    if-nez v1, :cond_3

    :goto_2
    invoke-direct {p0, v0, v3}, Lcom/google/android/exoplayer2/t;->a(Landroid/view/Surface;Z)V

    goto :goto_0

    :cond_2
    move-object v1, v0

    .line 279
    goto :goto_1

    .line 281
    :cond_3
    new-instance v0, Landroid/view/Surface;

    invoke-direct {v0, v1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    goto :goto_2
.end method

.method public a(Lcom/google/android/exoplayer2/o$a;)V
    .locals 1

    .prologue
    .line 529
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->b:Lcom/google/android/exoplayer2/e;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/e;->a(Lcom/google/android/exoplayer2/o$a;)V

    .line 530
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/source/i;)V
    .locals 1

    .prologue
    .line 544
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->b:Lcom/google/android/exoplayer2/e;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/e;->a(Lcom/google/android/exoplayer2/source/i;)V

    .line 545
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/t$b;)V
    .locals 0

    .prologue
    .line 448
    iput-object p1, p0, Lcom/google/android/exoplayer2/t;->o:Lcom/google/android/exoplayer2/t$b;

    .line 449
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/text/j$a;)V
    .locals 0

    .prologue
    .line 468
    iput-object p1, p0, Lcom/google/android/exoplayer2/t;->m:Lcom/google/android/exoplayer2/text/j$a;

    .line 469
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 554
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->b:Lcom/google/android/exoplayer2/e;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/e;->a(Z)V

    .line 555
    return-void
.end method

.method public varargs a([Lcom/google/android/exoplayer2/e$c;)V
    .locals 1

    .prologue
    .line 626
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->b:Lcom/google/android/exoplayer2/e;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/e;->a([Lcom/google/android/exoplayer2/e$c;)V

    .line 627
    return-void
.end method

.method public b(I)I
    .locals 1

    .prologue
    .line 641
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->b:Lcom/google/android/exoplayer2/e;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/e;->b(I)I

    move-result v0

    return v0
.end method

.method public b(Landroid/view/SurfaceHolder;)V
    .locals 1

    .prologue
    .line 238
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->k:Landroid/view/SurfaceHolder;

    if-ne p1, v0, :cond_0

    .line 239
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/t;->a(Landroid/view/SurfaceHolder;)V

    .line 241
    :cond_0
    return-void
.end method

.method public b(Landroid/view/SurfaceView;)V
    .locals 1

    .prologue
    .line 260
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/t;->b(Landroid/view/SurfaceHolder;)V

    .line 261
    return-void

    .line 260
    :cond_0
    invoke-virtual {p1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Landroid/view/TextureView;)V
    .locals 1

    .prologue
    .line 292
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->l:Landroid/view/TextureView;

    if-ne p1, v0, :cond_0

    .line 293
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/t;->a(Landroid/view/TextureView;)V

    .line 295
    :cond_0
    return-void
.end method

.method public b(Lcom/google/android/exoplayer2/o$a;)V
    .locals 1

    .prologue
    .line 534
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->b:Lcom/google/android/exoplayer2/e;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/e;->b(Lcom/google/android/exoplayer2/o$a;)V

    .line 535
    return-void
.end method

.method public b(Lcom/google/android/exoplayer2/t$b;)V
    .locals 1

    .prologue
    .line 457
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->o:Lcom/google/android/exoplayer2/t$b;

    if-ne v0, p1, :cond_0

    .line 458
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer2/t;->o:Lcom/google/android/exoplayer2/t$b;

    .line 460
    :cond_0
    return-void
.end method

.method public b(Lcom/google/android/exoplayer2/text/j$a;)V
    .locals 1

    .prologue
    .line 477
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->m:Lcom/google/android/exoplayer2/text/j$a;

    if-ne v0, p1, :cond_0

    .line 478
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer2/t;->m:Lcom/google/android/exoplayer2/text/j$a;

    .line 480
    :cond_0
    return-void
.end method

.method public varargs b([Lcom/google/android/exoplayer2/e$c;)V
    .locals 1

    .prologue
    .line 631
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->b:Lcom/google/android/exoplayer2/e;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/e;->b([Lcom/google/android/exoplayer2/e$c;)V

    .line 632
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 559
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->b:Lcom/google/android/exoplayer2/e;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/e;->b()Z

    move-result v0

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 564
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->b:Lcom/google/android/exoplayer2/e;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/e;->c()I

    move-result v0

    return v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 614
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->b:Lcom/google/android/exoplayer2/e;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/e;->d()V

    .line 615
    invoke-direct {p0}, Lcom/google/android/exoplayer2/t;->m()V

    .line 616
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->h:Landroid/view/Surface;

    if-eqz v0, :cond_1

    .line 617
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/t;->i:Z

    if-eqz v0, :cond_0

    .line 618
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->h:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 620
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer2/t;->h:Landroid/view/Surface;

    .line 622
    :cond_1
    return-void
.end method

.method public e()I
    .locals 1

    .prologue
    .line 671
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->b:Lcom/google/android/exoplayer2/e;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/e;->e()I

    move-result v0

    return v0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 676
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->b:Lcom/google/android/exoplayer2/e;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/e;->f()J

    move-result-wide v0

    return-wide v0
.end method

.method public g()J
    .locals 2

    .prologue
    .line 681
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->b:Lcom/google/android/exoplayer2/e;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/e;->g()J

    move-result-wide v0

    return-wide v0
.end method

.method public h()J
    .locals 2

    .prologue
    .line 686
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->b:Lcom/google/android/exoplayer2/e;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/e;->h()J

    move-result-wide v0

    return-wide v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 706
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->b:Lcom/google/android/exoplayer2/e;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/e;->i()Z

    move-result v0

    return v0
.end method

.method public j()J
    .locals 2

    .prologue
    .line 721
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->b:Lcom/google/android/exoplayer2/e;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/e;->j()J

    move-result-wide v0

    return-wide v0
.end method

.method public k()Lcom/google/android/exoplayer2/b/f;
    .locals 1

    .prologue
    .line 651
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->b:Lcom/google/android/exoplayer2/e;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/e;->k()Lcom/google/android/exoplayer2/b/f;

    move-result-object v0

    return-object v0
.end method

.method public l()Lcom/google/android/exoplayer2/u;
    .locals 1

    .prologue
    .line 656
    iget-object v0, p0, Lcom/google/android/exoplayer2/t;->b:Lcom/google/android/exoplayer2/e;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/e;->l()Lcom/google/android/exoplayer2/u;

    move-result-object v0

    return-object v0
.end method
