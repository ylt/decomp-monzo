.class public final Lcom/google/android/exoplayer2/m$a;
.super Ljava/lang/Object;
.source "MediaPeriodInfoSequence.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field public final a:Lcom/google/android/exoplayer2/source/i$b;

.field public final b:J

.field public final c:J

.field public final d:J

.field public final e:J

.field public final f:Z

.field public final g:Z


# direct methods
.method private constructor <init>(Lcom/google/android/exoplayer2/source/i$b;JJJJZZ)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/google/android/exoplayer2/m$a;->a:Lcom/google/android/exoplayer2/source/i$b;

    .line 74
    iput-wide p2, p0, Lcom/google/android/exoplayer2/m$a;->b:J

    .line 75
    iput-wide p4, p0, Lcom/google/android/exoplayer2/m$a;->c:J

    .line 76
    iput-wide p6, p0, Lcom/google/android/exoplayer2/m$a;->d:J

    .line 77
    iput-wide p8, p0, Lcom/google/android/exoplayer2/m$a;->e:J

    .line 78
    iput-boolean p10, p0, Lcom/google/android/exoplayer2/m$a;->f:Z

    .line 79
    iput-boolean p11, p0, Lcom/google/android/exoplayer2/m$a;->g:Z

    .line 80
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/exoplayer2/source/i$b;JJJJZZLcom/google/android/exoplayer2/m$1;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct/range {p0 .. p11}, Lcom/google/android/exoplayer2/m$a;-><init>(Lcom/google/android/exoplayer2/source/i$b;JJJJZZ)V

    return-void
.end method


# virtual methods
.method public a(I)Lcom/google/android/exoplayer2/m$a;
    .locals 12

    .prologue
    .line 87
    new-instance v0, Lcom/google/android/exoplayer2/m$a;

    iget-object v1, p0, Lcom/google/android/exoplayer2/m$a;->a:Lcom/google/android/exoplayer2/source/i$b;

    invoke-virtual {v1, p1}, Lcom/google/android/exoplayer2/source/i$b;->a(I)Lcom/google/android/exoplayer2/source/i$b;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/exoplayer2/m$a;->b:J

    iget-wide v4, p0, Lcom/google/android/exoplayer2/m$a;->c:J

    iget-wide v6, p0, Lcom/google/android/exoplayer2/m$a;->d:J

    iget-wide v8, p0, Lcom/google/android/exoplayer2/m$a;->e:J

    iget-boolean v10, p0, Lcom/google/android/exoplayer2/m$a;->f:Z

    iget-boolean v11, p0, Lcom/google/android/exoplayer2/m$a;->g:Z

    invoke-direct/range {v0 .. v11}, Lcom/google/android/exoplayer2/m$a;-><init>(Lcom/google/android/exoplayer2/source/i$b;JJJJZZ)V

    return-object v0
.end method

.method public a(J)Lcom/google/android/exoplayer2/m$a;
    .locals 13

    .prologue
    .line 95
    new-instance v0, Lcom/google/android/exoplayer2/m$a;

    iget-object v1, p0, Lcom/google/android/exoplayer2/m$a;->a:Lcom/google/android/exoplayer2/source/i$b;

    iget-wide v4, p0, Lcom/google/android/exoplayer2/m$a;->c:J

    iget-wide v6, p0, Lcom/google/android/exoplayer2/m$a;->d:J

    iget-wide v8, p0, Lcom/google/android/exoplayer2/m$a;->e:J

    iget-boolean v10, p0, Lcom/google/android/exoplayer2/m$a;->f:Z

    iget-boolean v11, p0, Lcom/google/android/exoplayer2/m$a;->g:Z

    move-wide v2, p1

    invoke-direct/range {v0 .. v11}, Lcom/google/android/exoplayer2/m$a;-><init>(Lcom/google/android/exoplayer2/source/i$b;JJJJZZ)V

    return-object v0
.end method
