.class public interface abstract Lcom/google/android/exoplayer2/p;
.super Ljava/lang/Object;
.source "Renderer.java"

# interfaces
.implements Lcom/google/android/exoplayer2/e$b;


# virtual methods
.method public abstract a()I
.end method

.method public abstract a(I)V
.end method

.method public abstract a(J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation
.end method

.method public abstract a(JJ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation
.end method

.method public abstract a(Lcom/google/android/exoplayer2/r;[Lcom/google/android/exoplayer2/j;Lcom/google/android/exoplayer2/source/m;JZJ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation
.end method

.method public abstract a([Lcom/google/android/exoplayer2/j;Lcom/google/android/exoplayer2/source/m;J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation
.end method

.method public abstract b()Lcom/google/android/exoplayer2/q;
.end method

.method public abstract c()Lcom/google/android/exoplayer2/util/g;
.end method

.method public abstract d()I
.end method

.method public abstract e()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation
.end method

.method public abstract f()Lcom/google/android/exoplayer2/source/m;
.end method

.method public abstract g()Z
.end method

.method public abstract h()V
.end method

.method public abstract i()Z
.end method

.method public abstract j()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract k()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation
.end method

.method public abstract l()V
.end method

.method public abstract t()Z
.end method

.method public abstract u()Z
.end method
