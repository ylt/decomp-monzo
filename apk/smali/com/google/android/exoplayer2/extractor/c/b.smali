.class final Lcom/google/android/exoplayer2/extractor/c/b;
.super Ljava/lang/Object;
.source "AtomParsers.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/extractor/c/b$e;,
        Lcom/google/android/exoplayer2/extractor/c/b$d;,
        Lcom/google/android/exoplayer2/extractor/c/b$b;,
        Lcom/google/android/exoplayer2/extractor/c/b$c;,
        Lcom/google/android/exoplayer2/extractor/c/b$f;,
        Lcom/google/android/exoplayer2/extractor/c/b$a;
    }
.end annotation


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:I

.field private static final d:I

.field private static final e:I

.field private static final f:I

.field private static final g:I

.field private static final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-string v0, "vide"

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/s;->f(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/exoplayer2/extractor/c/b;->a:I

    .line 47
    const-string v0, "soun"

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/s;->f(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/exoplayer2/extractor/c/b;->b:I

    .line 48
    const-string v0, "text"

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/s;->f(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/exoplayer2/extractor/c/b;->c:I

    .line 49
    const-string v0, "sbtl"

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/s;->f(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/exoplayer2/extractor/c/b;->d:I

    .line 50
    const-string v0, "subt"

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/s;->f(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/exoplayer2/extractor/c/b;->e:I

    .line 51
    const-string v0, "clcp"

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/s;->f(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/exoplayer2/extractor/c/b;->f:I

    .line 52
    const-string v0, "cenc"

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/s;->f(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/exoplayer2/extractor/c/b;->g:I

    .line 53
    const-string v0, "meta"

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/s;->f(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/exoplayer2/extractor/c/b;->h:I

    return-void
.end method

.method private static a(Lcom/google/android/exoplayer2/util/k;II)I
    .locals 4

    .prologue
    .line 955
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->d()I

    move-result v1

    .line 956
    :goto_0
    sub-int v0, v1, p1

    if-ge v0, p2, :cond_2

    .line 957
    invoke-virtual {p0, v1}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 958
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v2

    .line 959
    if-lez v2, :cond_0

    const/4 v0, 0x1

    :goto_1
    const-string v3, "childAtomSize should be positive"

    invoke-static {v0, v3}, Lcom/google/android/exoplayer2/util/a;->a(ZLjava/lang/Object;)V

    .line 960
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v0

    .line 961
    sget v3, Lcom/google/android/exoplayer2/extractor/c/a;->J:I

    if-ne v0, v3, :cond_1

    move v0, v1

    .line 966
    :goto_2
    return v0

    .line 959
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 964
    :cond_1
    add-int/2addr v1, v2

    .line 965
    goto :goto_0

    .line 966
    :cond_2
    const/4 v0, -0x1

    goto :goto_2
.end method

.method private static a(Lcom/google/android/exoplayer2/util/k;)J
    .locals 2

    .prologue
    const/16 v0, 0x8

    .line 467
    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 468
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v1

    .line 469
    invoke-static {v1}, Lcom/google/android/exoplayer2/extractor/c/a;->a(I)I

    move-result v1

    .line 470
    if-nez v1, :cond_0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 471
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->l()J

    move-result-wide v0

    return-wide v0

    .line 470
    :cond_0
    const/16 v0, 0x10

    goto :goto_0
.end method

.method private static a(Lcom/google/android/exoplayer2/extractor/c/a$a;)Landroid/util/Pair;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/extractor/c/a$a;",
            ")",
            "Landroid/util/Pair",
            "<[J[J>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x1

    .line 786
    if-eqz p0, :cond_0

    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->Q:I

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/extractor/c/a$a;->d(I)Lcom/google/android/exoplayer2/extractor/c/a$b;

    move-result-object v0

    if-nez v0, :cond_1

    .line 787
    :cond_0
    invoke-static {v1, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 807
    :goto_0
    return-object v0

    .line 789
    :cond_1
    iget-object v3, v0, Lcom/google/android/exoplayer2/extractor/c/a$b;->aQ:Lcom/google/android/exoplayer2/util/k;

    .line 790
    const/16 v0, 0x8

    invoke-virtual {v3, v0}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 791
    invoke-virtual {v3}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v0

    .line 792
    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/c/a;->a(I)I

    move-result v4

    .line 793
    invoke-virtual {v3}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v5

    .line 794
    new-array v6, v5, [J

    .line 795
    new-array v7, v5, [J

    .line 796
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_5

    .line 797
    if-ne v4, v8, :cond_2

    .line 798
    invoke-virtual {v3}, Lcom/google/android/exoplayer2/util/k;->v()J

    move-result-wide v0

    :goto_2
    aput-wide v0, v6, v2

    .line 799
    if-ne v4, v8, :cond_3

    invoke-virtual {v3}, Lcom/google/android/exoplayer2/util/k;->p()J

    move-result-wide v0

    :goto_3
    aput-wide v0, v7, v2

    .line 800
    invoke-virtual {v3}, Lcom/google/android/exoplayer2/util/k;->j()S

    move-result v0

    .line 801
    if-eq v0, v8, :cond_4

    .line 803
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported media rate."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 798
    :cond_2
    invoke-virtual {v3}, Lcom/google/android/exoplayer2/util/k;->l()J

    move-result-wide v0

    goto :goto_2

    .line 799
    :cond_3
    invoke-virtual {v3}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v0

    int-to-long v0, v0

    goto :goto_3

    .line 805
    :cond_4
    const/4 v0, 0x2

    invoke-virtual {v3, v0}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 796
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 807
    :cond_5
    invoke-static {v6, v7}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/google/android/exoplayer2/util/k;IILjava/lang/String;Lcom/google/android/exoplayer2/drm/a;Z)Lcom/google/android/exoplayer2/extractor/c/b$c;
    .locals 21
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    .line 590
    const/16 v1, 0xc

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 591
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v20

    .line 592
    new-instance v8, Lcom/google/android/exoplayer2/extractor/c/b$c;

    move/from16 v0, v20

    invoke-direct {v8, v0}, Lcom/google/android/exoplayer2/extractor/c/b$c;-><init>(I)V

    .line 593
    const/4 v9, 0x0

    :goto_0
    move/from16 v0, v20

    if-ge v9, v0, :cond_8

    .line 594
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->d()I

    move-result v3

    .line 595
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v4

    .line 596
    if-lez v4, :cond_2

    const/4 v1, 0x1

    :goto_1
    const-string v2, "childAtomSize should be positive"

    invoke-static {v1, v2}, Lcom/google/android/exoplayer2/util/a;->a(ZLjava/lang/Object;)V

    .line 597
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v2

    .line 598
    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->b:I

    if-eq v2, v1, :cond_0

    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->c:I

    if-eq v2, v1, :cond_0

    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->Z:I

    if-eq v2, v1, :cond_0

    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->al:I

    if-eq v2, v1, :cond_0

    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->d:I

    if-eq v2, v1, :cond_0

    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->e:I

    if-eq v2, v1, :cond_0

    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->f:I

    if-eq v2, v1, :cond_0

    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->aK:I

    if-eq v2, v1, :cond_0

    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->aL:I

    if-ne v2, v1, :cond_3

    :cond_0
    move-object/from16 v1, p0

    move/from16 v5, p1

    move/from16 v6, p2

    move-object/from16 v7, p4

    .line 603
    invoke-static/range {v1 .. v9}, Lcom/google/android/exoplayer2/extractor/c/b;->a(Lcom/google/android/exoplayer2/util/k;IIIIILcom/google/android/exoplayer2/drm/a;Lcom/google/android/exoplayer2/extractor/c/b$c;I)V

    .line 623
    :cond_1
    :goto_2
    add-int v1, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 593
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 596
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 605
    :cond_3
    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->i:I

    if-eq v2, v1, :cond_4

    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->aa:I

    if-eq v2, v1, :cond_4

    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->n:I

    if-eq v2, v1, :cond_4

    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->p:I

    if-eq v2, v1, :cond_4

    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->r:I

    if-eq v2, v1, :cond_4

    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->u:I

    if-eq v2, v1, :cond_4

    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->s:I

    if-eq v2, v1, :cond_4

    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->t:I

    if-eq v2, v1, :cond_4

    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->ay:I

    if-eq v2, v1, :cond_4

    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->az:I

    if-eq v2, v1, :cond_4

    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->l:I

    if-eq v2, v1, :cond_4

    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->m:I

    if-eq v2, v1, :cond_4

    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->j:I

    if-eq v2, v1, :cond_4

    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->aO:I

    if-ne v2, v1, :cond_5

    :cond_4
    move-object/from16 v10, p0

    move v11, v2

    move v12, v3

    move v13, v4

    move/from16 v14, p1

    move-object/from16 v15, p3

    move/from16 v16, p5

    move-object/from16 v17, p4

    move-object/from16 v18, v8

    move/from16 v19, v9

    .line 612
    invoke-static/range {v10 .. v19}, Lcom/google/android/exoplayer2/extractor/c/b;->a(Lcom/google/android/exoplayer2/util/k;IIIILjava/lang/String;ZLcom/google/android/exoplayer2/drm/a;Lcom/google/android/exoplayer2/extractor/c/b$c;I)V

    goto :goto_2

    .line 614
    :cond_5
    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->aj:I

    if-eq v2, v1, :cond_6

    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->au:I

    if-eq v2, v1, :cond_6

    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->av:I

    if-eq v2, v1, :cond_6

    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->aw:I

    if-eq v2, v1, :cond_6

    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->ax:I

    if-ne v2, v1, :cond_7

    :cond_6
    move-object/from16 v1, p0

    move/from16 v5, p1

    move-object/from16 v6, p3

    move-object v7, v8

    .line 617
    invoke-static/range {v1 .. v7}, Lcom/google/android/exoplayer2/extractor/c/b;->a(Lcom/google/android/exoplayer2/util/k;IIIILjava/lang/String;Lcom/google/android/exoplayer2/extractor/c/b$c;)V

    goto :goto_2

    .line 619
    :cond_7
    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->aN:I

    if-ne v2, v1, :cond_1

    .line 620
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "application/x-camera-motion"

    const/4 v5, 0x0

    const/4 v6, -0x1

    const/4 v7, 0x0

    invoke-static {v1, v2, v5, v6, v7}, Lcom/google/android/exoplayer2/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/exoplayer2/drm/a;)Lcom/google/android/exoplayer2/j;

    move-result-object v1

    iput-object v1, v8, Lcom/google/android/exoplayer2/extractor/c/b$c;->b:Lcom/google/android/exoplayer2/j;

    goto/16 :goto_2

    .line 625
    :cond_8
    return-object v8
.end method

.method public static a(Lcom/google/android/exoplayer2/extractor/c/a$a;Lcom/google/android/exoplayer2/extractor/c/a$b;JLcom/google/android/exoplayer2/drm/a;Z)Lcom/google/android/exoplayer2/extractor/c/j;
    .locals 24
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    .line 68
    sget v2, Lcom/google/android/exoplayer2/extractor/c/a;->E:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/extractor/c/a$a;->e(I)Lcom/google/android/exoplayer2/extractor/c/a$a;

    move-result-object v8

    .line 69
    sget v2, Lcom/google/android/exoplayer2/extractor/c/a;->S:I

    invoke-virtual {v8, v2}, Lcom/google/android/exoplayer2/extractor/c/a$a;->d(I)Lcom/google/android/exoplayer2/extractor/c/a$b;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/exoplayer2/extractor/c/a$b;->aQ:Lcom/google/android/exoplayer2/util/k;

    invoke-static {v2}, Lcom/google/android/exoplayer2/extractor/c/b;->c(Lcom/google/android/exoplayer2/util/k;)I

    move-result v14

    .line 70
    const/4 v2, -0x1

    if-ne v14, v2, :cond_0

    .line 71
    const/4 v9, 0x0

    .line 93
    :goto_0
    return-object v9

    .line 74
    :cond_0
    sget v2, Lcom/google/android/exoplayer2/extractor/c/a;->O:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/extractor/c/a$a;->d(I)Lcom/google/android/exoplayer2/extractor/c/a$b;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/exoplayer2/extractor/c/a$b;->aQ:Lcom/google/android/exoplayer2/util/k;

    invoke-static {v2}, Lcom/google/android/exoplayer2/extractor/c/b;->b(Lcom/google/android/exoplayer2/util/k;)Lcom/google/android/exoplayer2/extractor/c/b$f;

    move-result-object v15

    .line 75
    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v2, p2, v2

    if-nez v2, :cond_3

    .line 76
    invoke-static {v15}, Lcom/google/android/exoplayer2/extractor/c/b$f;->a(Lcom/google/android/exoplayer2/extractor/c/b$f;)J

    move-result-wide v2

    .line 78
    :goto_1
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/exoplayer2/extractor/c/a$b;->aQ:Lcom/google/android/exoplayer2/util/k;

    invoke-static {v4}, Lcom/google/android/exoplayer2/extractor/c/b;->a(Lcom/google/android/exoplayer2/util/k;)J

    move-result-wide v6

    .line 80
    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v4, v2, v4

    if-nez v4, :cond_1

    .line 81
    const-wide v16, -0x7fffffffffffffffL    # -4.9E-324

    .line 85
    :goto_2
    sget v2, Lcom/google/android/exoplayer2/extractor/c/a;->F:I

    invoke-virtual {v8, v2}, Lcom/google/android/exoplayer2/extractor/c/a$a;->e(I)Lcom/google/android/exoplayer2/extractor/c/a$a;

    move-result-object v2

    sget v3, Lcom/google/android/exoplayer2/extractor/c/a;->G:I

    .line 86
    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer2/extractor/c/a$a;->e(I)Lcom/google/android/exoplayer2/extractor/c/a$a;

    move-result-object v2

    .line 88
    sget v3, Lcom/google/android/exoplayer2/extractor/c/a;->R:I

    invoke-virtual {v8, v3}, Lcom/google/android/exoplayer2/extractor/c/a$a;->d(I)Lcom/google/android/exoplayer2/extractor/c/a$b;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/exoplayer2/extractor/c/a$b;->aQ:Lcom/google/android/exoplayer2/util/k;

    invoke-static {v3}, Lcom/google/android/exoplayer2/extractor/c/b;->d(Lcom/google/android/exoplayer2/util/k;)Landroid/util/Pair;

    move-result-object v3

    .line 89
    sget v4, Lcom/google/android/exoplayer2/extractor/c/a;->T:I

    invoke-virtual {v2, v4}, Lcom/google/android/exoplayer2/extractor/c/a$a;->d(I)Lcom/google/android/exoplayer2/extractor/c/a$b;

    move-result-object v2

    iget-object v8, v2, Lcom/google/android/exoplayer2/extractor/c/a$b;->aQ:Lcom/google/android/exoplayer2/util/k;

    invoke-static {v15}, Lcom/google/android/exoplayer2/extractor/c/b$f;->b(Lcom/google/android/exoplayer2/extractor/c/b$f;)I

    move-result v9

    .line 90
    invoke-static {v15}, Lcom/google/android/exoplayer2/extractor/c/b$f;->c(Lcom/google/android/exoplayer2/extractor/c/b$f;)I

    move-result v10

    iget-object v11, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v11, Ljava/lang/String;

    move-object/from16 v12, p4

    move/from16 v13, p5

    .line 89
    invoke-static/range {v8 .. v13}, Lcom/google/android/exoplayer2/extractor/c/b;->a(Lcom/google/android/exoplayer2/util/k;IILjava/lang/String;Lcom/google/android/exoplayer2/drm/a;Z)Lcom/google/android/exoplayer2/extractor/c/b$c;

    move-result-object v4

    .line 91
    sget v2, Lcom/google/android/exoplayer2/extractor/c/a;->P:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/extractor/c/a$a;->e(I)Lcom/google/android/exoplayer2/extractor/c/a$a;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/exoplayer2/extractor/c/b;->a(Lcom/google/android/exoplayer2/extractor/c/a$a;)Landroid/util/Pair;

    move-result-object v5

    .line 92
    iget-object v2, v4, Lcom/google/android/exoplayer2/extractor/c/b$c;->b:Lcom/google/android/exoplayer2/j;

    if-nez v2, :cond_2

    const/4 v9, 0x0

    goto :goto_0

    .line 83
    :cond_1
    const-wide/32 v4, 0xf4240

    invoke-static/range {v2 .. v7}, Lcom/google/android/exoplayer2/util/s;->b(JJJ)J

    move-result-wide v16

    goto :goto_2

    .line 92
    :cond_2
    new-instance v9, Lcom/google/android/exoplayer2/extractor/c/j;

    .line 93
    invoke-static {v15}, Lcom/google/android/exoplayer2/extractor/c/b$f;->b(Lcom/google/android/exoplayer2/extractor/c/b$f;)I

    move-result v10

    iget-object v2, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    iget-object v0, v4, Lcom/google/android/exoplayer2/extractor/c/b$c;->b:Lcom/google/android/exoplayer2/j;

    move-object/from16 v18, v0

    iget v0, v4, Lcom/google/android/exoplayer2/extractor/c/b$c;->d:I

    move/from16 v19, v0

    iget-object v0, v4, Lcom/google/android/exoplayer2/extractor/c/b$c;->a:[Lcom/google/android/exoplayer2/extractor/c/k;

    move-object/from16 v20, v0

    iget v0, v4, Lcom/google/android/exoplayer2/extractor/c/b$c;->c:I

    move/from16 v21, v0

    iget-object v0, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, [J

    iget-object v0, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v23, v0

    check-cast v23, [J

    move v11, v14

    move-wide v14, v6

    invoke-direct/range {v9 .. v23}, Lcom/google/android/exoplayer2/extractor/c/j;-><init>(IIJJJLcom/google/android/exoplayer2/j;I[Lcom/google/android/exoplayer2/extractor/c/k;I[J[J)V

    goto/16 :goto_0

    :cond_3
    move-wide/from16 v2, p2

    goto/16 :goto_1
.end method

.method private static a(Lcom/google/android/exoplayer2/util/k;IILjava/lang/String;)Lcom/google/android/exoplayer2/extractor/c/k;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1109
    add-int/lit8 v2, p1, 0x8

    .line 1110
    :goto_0
    sub-int v3, v2, p1

    if-ge v3, p2, :cond_4

    .line 1111
    invoke-virtual {p0, v2}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 1112
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v3

    .line 1113
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v4

    .line 1114
    sget v5, Lcom/google/android/exoplayer2/extractor/c/a;->Y:I

    if-ne v4, v5, :cond_3

    .line 1115
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v2

    .line 1116
    invoke-static {v2}, Lcom/google/android/exoplayer2/extractor/c/a;->a(I)I

    move-result v2

    .line 1117
    invoke-virtual {p0, v1}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 1120
    if-nez v2, :cond_1

    .line 1121
    invoke-virtual {p0, v1}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    move v6, v0

    move v5, v0

    .line 1127
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->g()I

    move-result v2

    if-ne v2, v1, :cond_2

    .line 1128
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->g()I

    move-result v3

    .line 1129
    const/16 v2, 0x10

    new-array v4, v2, [B

    .line 1130
    array-length v2, v4

    invoke-virtual {p0, v4, v0, v2}, Lcom/google/android/exoplayer2/util/k;->a([BII)V

    .line 1132
    if-eqz v1, :cond_0

    if-nez v3, :cond_0

    .line 1133
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->g()I

    move-result v2

    .line 1134
    new-array v7, v2, [B

    .line 1135
    invoke-virtual {p0, v7, v0, v2}, Lcom/google/android/exoplayer2/util/k;->a([BII)V

    .line 1137
    :cond_0
    new-instance v0, Lcom/google/android/exoplayer2/extractor/c/k;

    move-object v2, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/exoplayer2/extractor/c/k;-><init>(ZLjava/lang/String;I[BII[B)V

    .line 1142
    :goto_3
    return-object v0

    .line 1123
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->g()I

    move-result v2

    .line 1124
    and-int/lit16 v3, v2, 0xf0

    shr-int/lit8 v5, v3, 0x4

    .line 1125
    and-int/lit8 v6, v2, 0xf

    goto :goto_1

    :cond_2
    move v1, v0

    .line 1127
    goto :goto_2

    .line 1140
    :cond_3
    add-int/2addr v2, v3

    .line 1141
    goto :goto_0

    :cond_4
    move-object v0, v7

    .line 1142
    goto :goto_3
.end method

.method public static a(Lcom/google/android/exoplayer2/extractor/c/j;Lcom/google/android/exoplayer2/extractor/c/a$a;Lcom/google/android/exoplayer2/extractor/i;)Lcom/google/android/exoplayer2/extractor/c/m;
    .locals 33
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    .line 110
    sget v2, Lcom/google/android/exoplayer2/extractor/c/a;->aq:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/extractor/c/a$a;->d(I)Lcom/google/android/exoplayer2/extractor/c/a$b;

    move-result-object v3

    .line 111
    if-eqz v3, :cond_0

    .line 112
    new-instance v2, Lcom/google/android/exoplayer2/extractor/c/b$d;

    invoke-direct {v2, v3}, Lcom/google/android/exoplayer2/extractor/c/b$d;-><init>(Lcom/google/android/exoplayer2/extractor/c/a$b;)V

    .line 121
    :goto_0
    invoke-interface {v2}, Lcom/google/android/exoplayer2/extractor/c/b$b;->a()I

    move-result v28

    .line 122
    if-nez v28, :cond_2

    .line 123
    new-instance v2, Lcom/google/android/exoplayer2/extractor/c/m;

    const/4 v3, 0x0

    new-array v3, v3, [J

    const/4 v4, 0x0

    new-array v4, v4, [I

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-array v6, v6, [J

    const/4 v7, 0x0

    new-array v7, v7, [I

    invoke-direct/range {v2 .. v7}, Lcom/google/android/exoplayer2/extractor/c/m;-><init>([J[II[J[I)V

    .line 401
    :goto_1
    return-object v2

    .line 114
    :cond_0
    sget v2, Lcom/google/android/exoplayer2/extractor/c/a;->ar:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/extractor/c/a$a;->d(I)Lcom/google/android/exoplayer2/extractor/c/a$b;

    move-result-object v3

    .line 115
    if-nez v3, :cond_1

    .line 116
    new-instance v2, Lcom/google/android/exoplayer2/ParserException;

    const-string v3, "Track has no sample table size information"

    invoke-direct {v2, v3}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 118
    :cond_1
    new-instance v2, Lcom/google/android/exoplayer2/extractor/c/b$e;

    invoke-direct {v2, v3}, Lcom/google/android/exoplayer2/extractor/c/b$e;-><init>(Lcom/google/android/exoplayer2/extractor/c/a$b;)V

    goto :goto_0

    .line 127
    :cond_2
    const/4 v4, 0x0

    .line 128
    sget v3, Lcom/google/android/exoplayer2/extractor/c/a;->as:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/android/exoplayer2/extractor/c/a$a;->d(I)Lcom/google/android/exoplayer2/extractor/c/a$b;

    move-result-object v3

    .line 129
    if-nez v3, :cond_3

    .line 130
    const/4 v4, 0x1

    .line 131
    sget v3, Lcom/google/android/exoplayer2/extractor/c/a;->at:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/android/exoplayer2/extractor/c/a$a;->d(I)Lcom/google/android/exoplayer2/extractor/c/a$b;

    move-result-object v3

    .line 133
    :cond_3
    iget-object v6, v3, Lcom/google/android/exoplayer2/extractor/c/a$b;->aQ:Lcom/google/android/exoplayer2/util/k;

    .line 135
    sget v3, Lcom/google/android/exoplayer2/extractor/c/a;->ap:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/android/exoplayer2/extractor/c/a$a;->d(I)Lcom/google/android/exoplayer2/extractor/c/a$b;

    move-result-object v3

    iget-object v7, v3, Lcom/google/android/exoplayer2/extractor/c/a$b;->aQ:Lcom/google/android/exoplayer2/util/k;

    .line 137
    sget v3, Lcom/google/android/exoplayer2/extractor/c/a;->am:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/android/exoplayer2/extractor/c/a$a;->d(I)Lcom/google/android/exoplayer2/extractor/c/a$b;

    move-result-object v3

    iget-object v0, v3, Lcom/google/android/exoplayer2/extractor/c/a$b;->aQ:Lcom/google/android/exoplayer2/util/k;

    move-object/from16 v29, v0

    .line 139
    sget v3, Lcom/google/android/exoplayer2/extractor/c/a;->an:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/android/exoplayer2/extractor/c/a$a;->d(I)Lcom/google/android/exoplayer2/extractor/c/a$b;

    move-result-object v3

    .line 140
    if-eqz v3, :cond_5

    iget-object v3, v3, Lcom/google/android/exoplayer2/extractor/c/a$b;->aQ:Lcom/google/android/exoplayer2/util/k;

    .line 142
    :goto_2
    sget v5, Lcom/google/android/exoplayer2/extractor/c/a;->ao:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/exoplayer2/extractor/c/a$a;->d(I)Lcom/google/android/exoplayer2/extractor/c/a$b;

    move-result-object v5

    .line 143
    if-eqz v5, :cond_6

    iget-object v5, v5, Lcom/google/android/exoplayer2/extractor/c/a$b;->aQ:Lcom/google/android/exoplayer2/util/k;

    .line 146
    :goto_3
    new-instance v30, Lcom/google/android/exoplayer2/extractor/c/b$a;

    move-object/from16 v0, v30

    invoke-direct {v0, v7, v6, v4}, Lcom/google/android/exoplayer2/extractor/c/b$a;-><init>(Lcom/google/android/exoplayer2/util/k;Lcom/google/android/exoplayer2/util/k;Z)V

    .line 149
    const/16 v4, 0xc

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 150
    invoke-virtual/range {v29 .. v29}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v4

    add-int/lit8 v23, v4, -0x1

    .line 151
    invoke-virtual/range {v29 .. v29}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v22

    .line 152
    invoke-virtual/range {v29 .. v29}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v19

    .line 155
    const/16 v18, 0x0

    .line 156
    const/4 v13, 0x0

    .line 157
    const/4 v12, 0x0

    .line 158
    if-eqz v5, :cond_4

    .line 159
    const/16 v4, 0xc

    invoke-virtual {v5, v4}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 160
    invoke-virtual {v5}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v13

    .line 163
    :cond_4
    const/4 v6, -0x1

    .line 164
    const/4 v4, 0x0

    .line 165
    if-eqz v3, :cond_30

    .line 166
    const/16 v4, 0xc

    invoke-virtual {v3, v4}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 167
    invoke-virtual {v3}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v4

    .line 168
    if-lez v4, :cond_7

    .line 169
    invoke-virtual {v3}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    move/from16 v32, v4

    move v4, v6

    move-object v6, v3

    move/from16 v3, v32

    .line 177
    :goto_4
    invoke-interface {v2}, Lcom/google/android/exoplayer2/extractor/c/b$b;->c()Z

    move-result v7

    if-eqz v7, :cond_8

    const-string v7, "audio/raw"

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/exoplayer2/extractor/c/j;->f:Lcom/google/android/exoplayer2/j;

    iget-object v8, v8, Lcom/google/android/exoplayer2/j;->f:Ljava/lang/String;

    .line 178
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    if-nez v23, :cond_8

    if-nez v13, :cond_8

    if-nez v3, :cond_8

    const/4 v7, 0x1

    .line 184
    :goto_5
    const/4 v11, 0x0

    .line 187
    const-wide/16 v8, 0x0

    .line 189
    if-nez v7, :cond_16

    .line 190
    move/from16 v0, v28

    new-array v0, v0, [J

    move-object/from16 v17, v0

    .line 191
    move/from16 v0, v28

    new-array v0, v0, [I

    move-object/from16 v16, v0

    .line 192
    move/from16 v0, v28

    new-array v15, v0, [J

    .line 193
    move/from16 v0, v28

    new-array v14, v0, [I

    .line 194
    const-wide/16 v20, 0x0

    .line 195
    const/4 v10, 0x0

    .line 197
    const/4 v7, 0x0

    move/from16 v24, v7

    move/from16 v25, v22

    move/from16 v7, v18

    move/from16 v18, v3

    move v3, v12

    move/from16 v32, v4

    move v4, v13

    move-wide v12, v8

    move v8, v11

    move v9, v10

    move/from16 v10, v19

    move/from16 v11, v23

    move/from16 v19, v32

    :goto_6
    move/from16 v0, v24

    move/from16 v1, v28

    if-ge v0, v1, :cond_f

    move-wide/from16 v22, v20

    move/from16 v20, v9

    .line 199
    :goto_7
    if-nez v20, :cond_9

    .line 200
    invoke-virtual/range {v30 .. v30}, Lcom/google/android/exoplayer2/extractor/c/b$a;->a()Z

    move-result v9

    invoke-static {v9}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 201
    move-object/from16 v0, v30

    iget-wide v0, v0, Lcom/google/android/exoplayer2/extractor/c/b$a;->d:J

    move-wide/from16 v20, v0

    .line 202
    move-object/from16 v0, v30

    iget v9, v0, Lcom/google/android/exoplayer2/extractor/c/b$a;->c:I

    move-wide/from16 v22, v20

    move/from16 v20, v9

    goto :goto_7

    .line 140
    :cond_5
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 143
    :cond_6
    const/4 v5, 0x0

    goto/16 :goto_3

    .line 172
    :cond_7
    const/4 v3, 0x0

    move/from16 v32, v4

    move v4, v6

    move-object v6, v3

    move/from16 v3, v32

    goto :goto_4

    .line 178
    :cond_8
    const/4 v7, 0x0

    goto :goto_5

    .line 206
    :cond_9
    if-eqz v5, :cond_b

    .line 207
    :goto_8
    if-nez v7, :cond_a

    if-lez v4, :cond_a

    .line 208
    invoke-virtual {v5}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v7

    .line 214
    invoke-virtual {v5}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v3

    .line 215
    add-int/lit8 v4, v4, -0x1

    goto :goto_8

    .line 217
    :cond_a
    add-int/lit8 v7, v7, -0x1

    .line 220
    :cond_b
    aput-wide v22, v17, v24

    .line 221
    invoke-interface {v2}, Lcom/google/android/exoplayer2/extractor/c/b$b;->b()I

    move-result v9

    aput v9, v16, v24

    .line 222
    aget v9, v16, v24

    if-le v9, v8, :cond_c

    .line 223
    aget v8, v16, v24

    .line 225
    :cond_c
    int-to-long v0, v3

    move-wide/from16 v26, v0

    add-long v26, v26, v12

    aput-wide v26, v15, v24

    .line 228
    if-nez v6, :cond_e

    const/4 v9, 0x1

    :goto_9
    aput v9, v14, v24

    .line 229
    move/from16 v0, v24

    move/from16 v1, v19

    if-ne v0, v1, :cond_d

    .line 230
    const/4 v9, 0x1

    aput v9, v14, v24

    .line 231
    add-int/lit8 v9, v18, -0x1

    .line 232
    if-lez v9, :cond_2f

    .line 233
    invoke-virtual {v6}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v18

    add-int/lit8 v18, v18, -0x1

    move/from16 v19, v18

    move/from16 v18, v9

    .line 238
    :cond_d
    :goto_a
    int-to-long v0, v10

    move-wide/from16 v26, v0

    add-long v26, v26, v12

    .line 239
    add-int/lit8 v9, v25, -0x1

    .line 240
    if-nez v9, :cond_2e

    if-lez v11, :cond_2e

    .line 241
    invoke-virtual/range {v29 .. v29}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v10

    .line 242
    invoke-virtual/range {v29 .. v29}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v9

    .line 243
    add-int/lit8 v11, v11, -0x1

    .line 246
    :goto_b
    aget v12, v16, v24

    int-to-long v12, v12

    add-long v22, v22, v12

    .line 247
    add-int/lit8 v13, v20, -0x1

    .line 197
    add-int/lit8 v12, v24, 0x1

    move/from16 v24, v12

    move-wide/from16 v20, v22

    move/from16 v25, v10

    move v10, v9

    move v9, v13

    move-wide/from16 v12, v26

    goto/16 :goto_6

    .line 228
    :cond_e
    const/4 v9, 0x0

    goto :goto_9

    .line 250
    :cond_f
    if-nez v7, :cond_10

    const/4 v2, 0x1

    :goto_c
    invoke-static {v2}, Lcom/google/android/exoplayer2/util/a;->a(Z)V

    .line 252
    :goto_d
    if-lez v4, :cond_12

    .line 253
    invoke-virtual {v5}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v2

    if-nez v2, :cond_11

    const/4 v2, 0x1

    :goto_e
    invoke-static {v2}, Lcom/google/android/exoplayer2/util/a;->a(Z)V

    .line 254
    invoke-virtual {v5}, Lcom/google/android/exoplayer2/util/k;->n()I

    .line 255
    add-int/lit8 v4, v4, -0x1

    goto :goto_d

    .line 250
    :cond_10
    const/4 v2, 0x0

    goto :goto_c

    .line 253
    :cond_11
    const/4 v2, 0x0

    goto :goto_e

    .line 260
    :cond_12
    if-nez v18, :cond_13

    if-nez v25, :cond_13

    if-nez v9, :cond_13

    if-eqz v11, :cond_14

    .line 262
    :cond_13
    const-string v2, "AtomParsers"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Inconsistent stbl box for track "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/exoplayer2/extractor/c/j;->a:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": remainingSynchronizationSamples "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", remainingSamplesAtTimestampDelta "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v25

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", remainingSamplesInChunk "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", remainingTimestampDeltaChanges "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_14
    move-object v7, v14

    move-object v6, v15

    move v5, v8

    move-object/from16 v4, v16

    move-object/from16 v3, v17

    move-wide v14, v12

    .line 285
    :goto_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/c/j;->h:[J

    if-eqz v2, :cond_15

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/exoplayer2/extractor/i;->a()Z

    move-result v2

    if-eqz v2, :cond_18

    .line 288
    :cond_15
    const-wide/32 v8, 0xf4240

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/exoplayer2/extractor/c/j;->c:J

    invoke-static {v6, v8, v9, v10, v11}, Lcom/google/android/exoplayer2/util/s;->a([JJJ)V

    .line 289
    new-instance v2, Lcom/google/android/exoplayer2/extractor/c/m;

    invoke-direct/range {v2 .. v7}, Lcom/google/android/exoplayer2/extractor/c/m;-><init>([J[II[J[I)V

    goto/16 :goto_1

    .line 269
    :cond_16
    move-object/from16 v0, v30

    iget v3, v0, Lcom/google/android/exoplayer2/extractor/c/b$a;->a:I

    new-array v3, v3, [J

    .line 270
    move-object/from16 v0, v30

    iget v4, v0, Lcom/google/android/exoplayer2/extractor/c/b$a;->a:I

    new-array v4, v4, [I

    .line 271
    :goto_10
    invoke-virtual/range {v30 .. v30}, Lcom/google/android/exoplayer2/extractor/c/b$a;->a()Z

    move-result v5

    if-eqz v5, :cond_17

    .line 272
    move-object/from16 v0, v30

    iget v5, v0, Lcom/google/android/exoplayer2/extractor/c/b$a;->b:I

    move-object/from16 v0, v30

    iget-wide v6, v0, Lcom/google/android/exoplayer2/extractor/c/b$a;->d:J

    aput-wide v6, v3, v5

    .line 273
    move-object/from16 v0, v30

    iget v5, v0, Lcom/google/android/exoplayer2/extractor/c/b$a;->b:I

    move-object/from16 v0, v30

    iget v6, v0, Lcom/google/android/exoplayer2/extractor/c/b$a;->c:I

    aput v6, v4, v5

    goto :goto_10

    .line 275
    :cond_17
    invoke-interface {v2}, Lcom/google/android/exoplayer2/extractor/c/b$b;->b()I

    move-result v2

    .line 276
    move/from16 v0, v19

    int-to-long v6, v0

    invoke-static {v2, v3, v4, v6, v7}, Lcom/google/android/exoplayer2/extractor/c/d;->a(I[J[IJ)Lcom/google/android/exoplayer2/extractor/c/d$a;

    move-result-object v2

    .line 278
    iget-object v3, v2, Lcom/google/android/exoplayer2/extractor/c/d$a;->a:[J

    .line 279
    iget-object v4, v2, Lcom/google/android/exoplayer2/extractor/c/d$a;->b:[I

    .line 280
    iget v5, v2, Lcom/google/android/exoplayer2/extractor/c/d$a;->c:I

    .line 281
    iget-object v6, v2, Lcom/google/android/exoplayer2/extractor/c/d$a;->d:[J

    .line 282
    iget-object v7, v2, Lcom/google/android/exoplayer2/extractor/c/d$a;->e:[I

    move-wide v14, v8

    goto :goto_f

    .line 298
    :cond_18
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/c/j;->h:[J

    array-length v2, v2

    const/4 v8, 0x1

    if-ne v2, v8, :cond_1a

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer2/extractor/c/j;->b:I

    const/4 v8, 0x1

    if-ne v2, v8, :cond_1a

    array-length v2, v6

    const/4 v8, 0x2

    if-lt v2, v8, :cond_1a

    .line 303
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/c/j;->i:[J

    const/4 v8, 0x0

    aget-wide v16, v2, v8

    .line 304
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/c/j;->h:[J

    const/4 v8, 0x0

    aget-wide v8, v2, v8

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/exoplayer2/extractor/c/j;->c:J

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/exoplayer2/extractor/c/j;->d:J

    invoke-static/range {v8 .. v13}, Lcom/google/android/exoplayer2/util/s;->b(JJJ)J

    move-result-wide v8

    add-long v8, v8, v16

    .line 307
    const/4 v2, 0x0

    aget-wide v10, v6, v2

    cmp-long v2, v10, v16

    if-gtz v2, :cond_1a

    const/4 v2, 0x1

    aget-wide v10, v6, v2

    cmp-long v2, v16, v10

    if-gez v2, :cond_1a

    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v10, v6, v2

    cmp-long v2, v10, v8

    if-gez v2, :cond_1a

    cmp-long v2, v8, v14

    if-gtz v2, :cond_1a

    .line 309
    sub-long/2addr v14, v8

    .line 310
    const/4 v2, 0x0

    aget-wide v8, v6, v2

    sub-long v8, v16, v8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/c/j;->f:Lcom/google/android/exoplayer2/j;

    iget v2, v2, Lcom/google/android/exoplayer2/j;->s:I

    int-to-long v10, v2

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/exoplayer2/extractor/c/j;->c:J

    invoke-static/range {v8 .. v13}, Lcom/google/android/exoplayer2/util/s;->b(JJJ)J

    move-result-wide v16

    .line 312
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/c/j;->f:Lcom/google/android/exoplayer2/j;

    iget v2, v2, Lcom/google/android/exoplayer2/j;->s:I

    int-to-long v10, v2

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/exoplayer2/extractor/c/j;->c:J

    move-wide v8, v14

    invoke-static/range {v8 .. v13}, Lcom/google/android/exoplayer2/util/s;->b(JJJ)J

    move-result-wide v8

    .line 314
    const-wide/16 v10, 0x0

    cmp-long v2, v16, v10

    if-nez v2, :cond_19

    const-wide/16 v10, 0x0

    cmp-long v2, v8, v10

    if-eqz v2, :cond_1a

    :cond_19
    const-wide/32 v10, 0x7fffffff

    cmp-long v2, v16, v10

    if-gtz v2, :cond_1a

    const-wide/32 v10, 0x7fffffff

    cmp-long v2, v8, v10

    if-gtz v2, :cond_1a

    .line 316
    move-wide/from16 v0, v16

    long-to-int v2, v0

    move-object/from16 v0, p2

    iput v2, v0, Lcom/google/android/exoplayer2/extractor/i;->b:I

    .line 317
    long-to-int v2, v8

    move-object/from16 v0, p2

    iput v2, v0, Lcom/google/android/exoplayer2/extractor/i;->c:I

    .line 318
    const-wide/32 v8, 0xf4240

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/exoplayer2/extractor/c/j;->c:J

    invoke-static {v6, v8, v9, v10, v11}, Lcom/google/android/exoplayer2/util/s;->a([JJJ)V

    .line 319
    new-instance v2, Lcom/google/android/exoplayer2/extractor/c/m;

    invoke-direct/range {v2 .. v7}, Lcom/google/android/exoplayer2/extractor/c/m;-><init>([J[II[J[I)V

    goto/16 :goto_1

    .line 324
    :cond_1a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/c/j;->h:[J

    array-length v2, v2

    const/4 v8, 0x1

    if-ne v2, v8, :cond_1c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/c/j;->h:[J

    const/4 v8, 0x0

    aget-wide v8, v2, v8

    const-wide/16 v10, 0x0

    cmp-long v2, v8, v10

    if-nez v2, :cond_1c

    .line 328
    const/4 v2, 0x0

    :goto_11
    array-length v8, v6

    if-ge v2, v8, :cond_1b

    .line 329
    aget-wide v8, v6, v2

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/exoplayer2/extractor/c/j;->i:[J

    const/4 v11, 0x0

    aget-wide v10, v10, v11

    sub-long/2addr v8, v10

    const-wide/32 v10, 0xf4240

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/exoplayer2/extractor/c/j;->c:J

    invoke-static/range {v8 .. v13}, Lcom/google/android/exoplayer2/util/s;->b(JJJ)J

    move-result-wide v8

    aput-wide v8, v6, v2

    .line 328
    add-int/lit8 v2, v2, 0x1

    goto :goto_11

    .line 332
    :cond_1b
    new-instance v2, Lcom/google/android/exoplayer2/extractor/c/m;

    invoke-direct/range {v2 .. v7}, Lcom/google/android/exoplayer2/extractor/c/m;-><init>([J[II[J[I)V

    goto/16 :goto_1

    .line 336
    :cond_1c
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer2/extractor/c/j;->b:I

    const/4 v8, 0x1

    if-ne v2, v8, :cond_1d

    const/4 v2, 0x1

    .line 339
    :goto_12
    const/4 v11, 0x0

    .line 340
    const/4 v10, 0x0

    .line 341
    const/4 v9, 0x0

    .line 342
    const/4 v8, 0x0

    move v14, v8

    move v15, v9

    move/from16 v16, v10

    move/from16 v17, v11

    :goto_13
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/exoplayer2/extractor/c/j;->h:[J

    array-length v8, v8

    if-ge v14, v8, :cond_1f

    .line 343
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/exoplayer2/extractor/c/j;->i:[J

    aget-wide v18, v8, v14

    .line 344
    const-wide/16 v8, -0x1

    cmp-long v8, v18, v8

    if-eqz v8, :cond_2d

    .line 345
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/exoplayer2/extractor/c/j;->h:[J

    aget-wide v8, v8, v14

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/exoplayer2/extractor/c/j;->c:J

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/exoplayer2/extractor/c/j;->d:J

    invoke-static/range {v8 .. v13}, Lcom/google/android/exoplayer2/util/s;->b(JJJ)J

    move-result-wide v8

    .line 347
    const/4 v10, 0x1

    const/4 v11, 0x1

    move-wide/from16 v0, v18

    invoke-static {v6, v0, v1, v10, v11}, Lcom/google/android/exoplayer2/util/s;->b([JJZZ)I

    move-result v12

    .line 348
    add-long v8, v8, v18

    const/4 v10, 0x0

    invoke-static {v6, v8, v9, v2, v10}, Lcom/google/android/exoplayer2/util/s;->b([JJZZ)I

    move-result v10

    .line 350
    sub-int v8, v10, v12

    add-int v11, v17, v8

    .line 351
    move/from16 v0, v16

    if-eq v0, v12, :cond_1e

    const/4 v8, 0x1

    :goto_14
    or-int v9, v15, v8

    .line 342
    :goto_15
    add-int/lit8 v8, v14, 0x1

    move v14, v8

    move v15, v9

    move/from16 v16, v10

    move/from16 v17, v11

    goto :goto_13

    .line 336
    :cond_1d
    const/4 v2, 0x0

    goto :goto_12

    .line 351
    :cond_1e
    const/4 v8, 0x0

    goto :goto_14

    .line 355
    :cond_1f
    move/from16 v0, v17

    move/from16 v1, v28

    if-eq v0, v1, :cond_22

    const/4 v8, 0x1

    :goto_16
    or-int v24, v15, v8

    .line 358
    if-eqz v24, :cond_23

    move/from16 v0, v17

    new-array v8, v0, [J

    move-object/from16 v23, v8

    .line 359
    :goto_17
    if-eqz v24, :cond_24

    move/from16 v0, v17

    new-array v8, v0, [I

    move-object/from16 v22, v8

    .line 360
    :goto_18
    if-eqz v24, :cond_25

    const/4 v9, 0x0

    .line 361
    :goto_19
    if-eqz v24, :cond_26

    move/from16 v0, v17

    new-array v5, v0, [I

    move-object/from16 v16, v5

    .line 362
    :goto_1a
    move/from16 v0, v17

    new-array v0, v0, [J

    move-object/from16 v25, v0

    .line 363
    const-wide/16 v10, 0x0

    .line 364
    const/4 v8, 0x0

    .line 365
    const/4 v5, 0x0

    move/from16 v17, v5

    move v14, v8

    move-wide/from16 v18, v10

    move v5, v9

    :goto_1b
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/exoplayer2/extractor/c/j;->h:[J

    array-length v8, v8

    move/from16 v0, v17

    if-ge v0, v8, :cond_28

    .line 366
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/exoplayer2/extractor/c/j;->i:[J

    aget-wide v26, v8, v17

    .line 367
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/exoplayer2/extractor/c/j;->h:[J

    aget-wide v8, v8, v17

    .line 368
    const-wide/16 v10, -0x1

    cmp-long v10, v26, v10

    if-eqz v10, :cond_2c

    .line 369
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/exoplayer2/extractor/c/j;->c:J

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/exoplayer2/extractor/c/j;->d:J

    invoke-static/range {v8 .. v13}, Lcom/google/android/exoplayer2/util/s;->b(JJJ)J

    move-result-wide v10

    add-long v12, v26, v10

    .line 371
    const/4 v10, 0x1

    const/4 v11, 0x1

    move-wide/from16 v0, v26

    invoke-static {v6, v0, v1, v10, v11}, Lcom/google/android/exoplayer2/util/s;->b([JJZZ)I

    move-result v10

    .line 372
    const/4 v11, 0x0

    invoke-static {v6, v12, v13, v2, v11}, Lcom/google/android/exoplayer2/util/s;->b([JJZZ)I

    move-result v28

    .line 373
    if-eqz v24, :cond_20

    .line 374
    sub-int v11, v28, v10

    .line 375
    move-object/from16 v0, v23

    invoke-static {v3, v10, v0, v14, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 376
    move-object/from16 v0, v22

    invoke-static {v4, v10, v0, v14, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 377
    move-object/from16 v0, v16

    invoke-static {v7, v10, v0, v14, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_20
    move/from16 v21, v10

    move/from16 v20, v14

    .line 379
    :goto_1c
    move/from16 v0, v21

    move/from16 v1, v28

    if-ge v0, v1, :cond_27

    .line 380
    const-wide/32 v12, 0xf4240

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/exoplayer2/extractor/c/j;->d:J

    move-wide/from16 v10, v18

    invoke-static/range {v10 .. v15}, Lcom/google/android/exoplayer2/util/s;->b(JJJ)J

    move-result-wide v30

    .line 381
    aget-wide v10, v6, v21

    sub-long v10, v10, v26

    const-wide/32 v12, 0xf4240

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/exoplayer2/extractor/c/j;->c:J

    invoke-static/range {v10 .. v15}, Lcom/google/android/exoplayer2/util/s;->b(JJJ)J

    move-result-wide v10

    .line 383
    add-long v10, v10, v30

    aput-wide v10, v25, v20

    .line 384
    if-eqz v24, :cond_21

    aget v10, v22, v20

    if-le v10, v5, :cond_21

    .line 385
    aget v5, v4, v21

    .line 387
    :cond_21
    add-int/lit8 v20, v20, 0x1

    .line 379
    add-int/lit8 v10, v21, 0x1

    move/from16 v21, v10

    goto :goto_1c

    .line 355
    :cond_22
    const/4 v8, 0x0

    goto/16 :goto_16

    :cond_23
    move-object/from16 v23, v3

    .line 358
    goto/16 :goto_17

    :cond_24
    move-object/from16 v22, v4

    .line 359
    goto/16 :goto_18

    :cond_25
    move v9, v5

    .line 360
    goto/16 :goto_19

    :cond_26
    move-object/from16 v16, v7

    .line 361
    goto/16 :goto_1a

    :cond_27
    move/from16 v10, v20

    move v11, v5

    .line 390
    :goto_1d
    add-long v8, v8, v18

    .line 365
    add-int/lit8 v5, v17, 0x1

    move/from16 v17, v5

    move v14, v10

    move-wide/from16 v18, v8

    move v5, v11

    goto/16 :goto_1b

    .line 393
    :cond_28
    const/4 v3, 0x0

    .line 394
    const/4 v2, 0x0

    :goto_1e
    move-object/from16 v0, v16

    array-length v4, v0

    if-ge v2, v4, :cond_2a

    if-nez v3, :cond_2a

    .line 395
    aget v4, v16, v2

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_29

    const/4 v4, 0x1

    :goto_1f
    or-int/2addr v3, v4

    .line 394
    add-int/lit8 v2, v2, 0x1

    goto :goto_1e

    .line 395
    :cond_29
    const/4 v4, 0x0

    goto :goto_1f

    .line 397
    :cond_2a
    if-nez v3, :cond_2b

    .line 398
    new-instance v2, Lcom/google/android/exoplayer2/ParserException;

    const-string v3, "The edited sample sequence does not contain a sync sample."

    invoke-direct {v2, v3}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 401
    :cond_2b
    new-instance v2, Lcom/google/android/exoplayer2/extractor/c/m;

    move-object/from16 v3, v23

    move-object/from16 v4, v22

    move-object/from16 v6, v25

    move-object/from16 v7, v16

    invoke-direct/range {v2 .. v7}, Lcom/google/android/exoplayer2/extractor/c/m;-><init>([J[II[J[I)V

    goto/16 :goto_1

    :cond_2c
    move v10, v14

    move v11, v5

    goto :goto_1d

    :cond_2d
    move v9, v15

    move/from16 v10, v16

    move/from16 v11, v17

    goto/16 :goto_15

    :cond_2e
    move/from16 v32, v10

    move v10, v9

    move/from16 v9, v32

    goto/16 :goto_b

    :cond_2f
    move/from16 v18, v9

    goto/16 :goto_a

    :cond_30
    move/from16 v32, v4

    move v4, v6

    move-object v6, v3

    move/from16 v3, v32

    goto/16 :goto_4
.end method

.method public static a(Lcom/google/android/exoplayer2/extractor/c/a$b;Z)Lcom/google/android/exoplayer2/metadata/a;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/16 v6, 0x8

    .line 413
    if-eqz p1, :cond_1

    .line 430
    :cond_0
    :goto_0
    return-object v0

    .line 418
    :cond_1
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/c/a$b;->aQ:Lcom/google/android/exoplayer2/util/k;

    .line 419
    invoke-virtual {v1, v6}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 420
    :goto_1
    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/k;->b()I

    move-result v2

    if-lt v2, v6, :cond_0

    .line 421
    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/k;->d()I

    move-result v2

    .line 422
    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v3

    .line 423
    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v4

    .line 424
    sget v5, Lcom/google/android/exoplayer2/extractor/c/a;->aB:I

    if-ne v4, v5, :cond_2

    .line 425
    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 426
    add-int v0, v2, v3

    invoke-static {v1, v0}, Lcom/google/android/exoplayer2/extractor/c/b;->a(Lcom/google/android/exoplayer2/util/k;I)Lcom/google/android/exoplayer2/metadata/a;

    move-result-object v0

    goto :goto_0

    .line 428
    :cond_2
    add-int/lit8 v2, v3, -0x8

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    goto :goto_1
.end method

.method private static a(Lcom/google/android/exoplayer2/util/k;I)Lcom/google/android/exoplayer2/metadata/a;
    .locals 4

    .prologue
    .line 434
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 435
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->d()I

    move-result v0

    if-ge v0, p1, :cond_1

    .line 436
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->d()I

    move-result v0

    .line 437
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v1

    .line 438
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v2

    .line 439
    sget v3, Lcom/google/android/exoplayer2/extractor/c/a;->aC:I

    if-ne v2, v3, :cond_0

    .line 440
    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 441
    add-int/2addr v0, v1

    invoke-static {p0, v0}, Lcom/google/android/exoplayer2/extractor/c/b;->b(Lcom/google/android/exoplayer2/util/k;I)Lcom/google/android/exoplayer2/metadata/a;

    move-result-object v0

    .line 445
    :goto_1
    return-object v0

    .line 443
    :cond_0
    add-int/lit8 v0, v1, -0x8

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    goto :goto_0

    .line 445
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static a(Lcom/google/android/exoplayer2/util/k;IIIIILcom/google/android/exoplayer2/drm/a;Lcom/google/android/exoplayer2/extractor/c/b$c;I)V
    .locals 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    .line 666
    add-int/lit8 v3, p2, 0x8

    add-int/lit8 v3, v3, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 668
    const/16 v3, 0x10

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 669
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->h()I

    move-result v8

    .line 670
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->h()I

    move-result v9

    .line 671
    const/4 v6, 0x0

    .line 672
    const/high16 v13, 0x3f800000    # 1.0f

    .line 673
    const/16 v3, 0x32

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 675
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->d()I

    move-result v5

    .line 676
    sget v3, Lcom/google/android/exoplayer2/extractor/c/a;->Z:I

    move/from16 v0, p1

    if-ne v0, v3, :cond_15

    .line 677
    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/google/android/exoplayer2/extractor/c/b;->b(Lcom/google/android/exoplayer2/util/k;II)Landroid/util/Pair;

    move-result-object v7

    .line 679
    if-eqz v7, :cond_14

    .line 680
    iget-object v3, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 681
    if-nez p6, :cond_1

    const/4 v3, 0x0

    move-object v4, v3

    .line 683
    :goto_0
    move-object/from16 v0, p7

    iget-object v10, v0, Lcom/google/android/exoplayer2/extractor/c/b$c;->a:[Lcom/google/android/exoplayer2/extractor/c/k;

    iget-object v3, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Lcom/google/android/exoplayer2/extractor/c/k;

    aput-object v3, v10, p8

    .line 685
    :goto_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    move-object/from16 v17, v4

    .line 692
    :goto_2
    const/4 v11, 0x0

    .line 693
    const/4 v4, 0x0

    .line 694
    const/4 v14, 0x0

    .line 696
    const/4 v15, -0x1

    move/from16 v18, v5

    move v5, v6

    move/from16 v6, v18

    .line 697
    :goto_3
    sub-int v3, v6, p2

    move/from16 v0, p3

    if-ge v3, v0, :cond_0

    .line 698
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 699
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->d()I

    move-result v7

    .line 700
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v10

    .line 701
    if-nez v10, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->d()I

    move-result v3

    sub-int v3, v3, p2

    move/from16 v0, p3

    if-ne v3, v0, :cond_2

    .line 768
    :cond_0
    if-nez v4, :cond_13

    .line 775
    :goto_4
    return-void

    .line 681
    :cond_1
    iget-object v3, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Lcom/google/android/exoplayer2/extractor/c/k;

    iget-object v3, v3, Lcom/google/android/exoplayer2/extractor/c/k;->b:Ljava/lang/String;

    .line 682
    move-object/from16 v0, p6

    invoke-virtual {v0, v3}, Lcom/google/android/exoplayer2/drm/a;->a(Ljava/lang/String;)Lcom/google/android/exoplayer2/drm/a;

    move-result-object v3

    move-object v4, v3

    goto :goto_0

    .line 705
    :cond_2
    if-lez v10, :cond_4

    const/4 v3, 0x1

    :goto_5
    const-string v12, "childAtomSize should be positive"

    invoke-static {v3, v12}, Lcom/google/android/exoplayer2/util/a;->a(ZLjava/lang/Object;)V

    .line 706
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v3

    .line 707
    sget v12, Lcom/google/android/exoplayer2/extractor/c/a;->H:I

    if-ne v3, v12, :cond_6

    .line 708
    if-nez v4, :cond_5

    const/4 v3, 0x1

    :goto_6
    invoke-static {v3}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 709
    const-string v4, "video/avc"

    .line 710
    add-int/lit8 v3, v7, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 711
    invoke-static/range {p0 .. p0}, Lcom/google/android/exoplayer2/c/a;->a(Lcom/google/android/exoplayer2/util/k;)Lcom/google/android/exoplayer2/c/a;

    move-result-object v3

    .line 712
    iget-object v11, v3, Lcom/google/android/exoplayer2/c/a;->a:Ljava/util/List;

    .line 713
    iget v7, v3, Lcom/google/android/exoplayer2/c/a;->b:I

    move-object/from16 v0, p7

    iput v7, v0, Lcom/google/android/exoplayer2/extractor/c/b$c;->c:I

    .line 714
    if-nez v5, :cond_3

    .line 715
    iget v13, v3, Lcom/google/android/exoplayer2/c/a;->e:F

    :cond_3
    move v3, v5

    .line 764
    :goto_7
    add-int v5, v6, v10

    move v6, v5

    move v5, v3

    .line 765
    goto :goto_3

    .line 705
    :cond_4
    const/4 v3, 0x0

    goto :goto_5

    .line 708
    :cond_5
    const/4 v3, 0x0

    goto :goto_6

    .line 717
    :cond_6
    sget v12, Lcom/google/android/exoplayer2/extractor/c/a;->I:I

    if-ne v3, v12, :cond_8

    .line 718
    if-nez v4, :cond_7

    const/4 v3, 0x1

    :goto_8
    invoke-static {v3}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 719
    const-string v4, "video/hevc"

    .line 720
    add-int/lit8 v3, v7, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 721
    invoke-static/range {p0 .. p0}, Lcom/google/android/exoplayer2/c/d;->a(Lcom/google/android/exoplayer2/util/k;)Lcom/google/android/exoplayer2/c/d;

    move-result-object v3

    .line 722
    iget-object v11, v3, Lcom/google/android/exoplayer2/c/d;->a:Ljava/util/List;

    .line 723
    iget v3, v3, Lcom/google/android/exoplayer2/c/d;->b:I

    move-object/from16 v0, p7

    iput v3, v0, Lcom/google/android/exoplayer2/extractor/c/b$c;->c:I

    move v3, v5

    .line 724
    goto :goto_7

    .line 718
    :cond_7
    const/4 v3, 0x0

    goto :goto_8

    .line 724
    :cond_8
    sget v12, Lcom/google/android/exoplayer2/extractor/c/a;->aM:I

    if-ne v3, v12, :cond_b

    .line 725
    if-nez v4, :cond_9

    const/4 v3, 0x1

    :goto_9
    invoke-static {v3}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 726
    sget v3, Lcom/google/android/exoplayer2/extractor/c/a;->aK:I

    move/from16 v0, p1

    if-ne v0, v3, :cond_a

    const-string v4, "video/x-vnd.on2.vp8"

    :goto_a
    move v3, v5

    goto :goto_7

    .line 725
    :cond_9
    const/4 v3, 0x0

    goto :goto_9

    .line 726
    :cond_a
    const-string v4, "video/x-vnd.on2.vp9"

    goto :goto_a

    .line 727
    :cond_b
    sget v12, Lcom/google/android/exoplayer2/extractor/c/a;->g:I

    if-ne v3, v12, :cond_d

    .line 728
    if-nez v4, :cond_c

    const/4 v3, 0x1

    :goto_b
    invoke-static {v3}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 729
    const-string v4, "video/3gpp"

    move v3, v5

    goto :goto_7

    .line 728
    :cond_c
    const/4 v3, 0x0

    goto :goto_b

    .line 730
    :cond_d
    sget v12, Lcom/google/android/exoplayer2/extractor/c/a;->J:I

    if-ne v3, v12, :cond_f

    .line 731
    if-nez v4, :cond_e

    const/4 v3, 0x1

    :goto_c
    invoke-static {v3}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 733
    move-object/from16 v0, p0

    invoke-static {v0, v7}, Lcom/google/android/exoplayer2/extractor/c/b;->d(Lcom/google/android/exoplayer2/util/k;I)Landroid/util/Pair;

    move-result-object v4

    .line 734
    iget-object v3, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    .line 735
    iget-object v4, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-static {v4}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v11

    move-object v4, v3

    move v3, v5

    .line 736
    goto :goto_7

    .line 731
    :cond_e
    const/4 v3, 0x0

    goto :goto_c

    .line 736
    :cond_f
    sget v12, Lcom/google/android/exoplayer2/extractor/c/a;->ai:I

    if-ne v3, v12, :cond_10

    .line 737
    move-object/from16 v0, p0

    invoke-static {v0, v7}, Lcom/google/android/exoplayer2/extractor/c/b;->c(Lcom/google/android/exoplayer2/util/k;I)F

    move-result v13

    .line 738
    const/4 v3, 0x1

    goto :goto_7

    .line 739
    :cond_10
    sget v12, Lcom/google/android/exoplayer2/extractor/c/a;->aI:I

    if-ne v3, v12, :cond_11

    .line 740
    move-object/from16 v0, p0

    invoke-static {v0, v7, v10}, Lcom/google/android/exoplayer2/extractor/c/b;->d(Lcom/google/android/exoplayer2/util/k;II)[B

    move-result-object v14

    move v3, v5

    goto/16 :goto_7

    .line 741
    :cond_11
    sget v7, Lcom/google/android/exoplayer2/extractor/c/a;->aH:I

    if-ne v3, v7, :cond_12

    .line 742
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->g()I

    move-result v3

    .line 743
    const/4 v7, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 744
    if-nez v3, :cond_12

    .line 745
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->g()I

    move-result v3

    .line 746
    packed-switch v3, :pswitch_data_0

    :cond_12
    move v3, v5

    goto/16 :goto_7

    .line 748
    :pswitch_0
    const/4 v15, 0x0

    move v3, v5

    .line 749
    goto/16 :goto_7

    .line 751
    :pswitch_1
    const/4 v15, 0x1

    move v3, v5

    .line 752
    goto/16 :goto_7

    .line 754
    :pswitch_2
    const/4 v15, 0x2

    move v3, v5

    .line 755
    goto/16 :goto_7

    .line 757
    :pswitch_3
    const/4 v15, 0x3

    move v3, v5

    .line 758
    goto/16 :goto_7

    .line 772
    :cond_13
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v6, -0x1

    const/4 v7, -0x1

    const/high16 v10, -0x40800000    # -1.0f

    const/16 v16, 0x0

    move/from16 v12, p5

    invoke-static/range {v3 .. v17}, Lcom/google/android/exoplayer2/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIIFLjava/util/List;IF[BILcom/google/android/exoplayer2/c/b;Lcom/google/android/exoplayer2/drm/a;)Lcom/google/android/exoplayer2/j;

    move-result-object v3

    move-object/from16 v0, p7

    iput-object v3, v0, Lcom/google/android/exoplayer2/extractor/c/b$c;->b:Lcom/google/android/exoplayer2/j;

    goto/16 :goto_4

    :cond_14
    move-object/from16 v4, p6

    goto/16 :goto_1

    :cond_15
    move-object/from16 v17, p6

    goto/16 :goto_2

    .line 746
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static a(Lcom/google/android/exoplayer2/util/k;IIIILjava/lang/String;Lcom/google/android/exoplayer2/extractor/c/b$c;)V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    .line 630
    add-int/lit8 v2, p2, 0x8

    add-int/lit8 v2, v2, 0x8

    invoke-virtual {p0, v2}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 633
    const/4 v12, 0x0

    .line 634
    const-wide v10, 0x7fffffffffffffffL

    .line 637
    sget v2, Lcom/google/android/exoplayer2/extractor/c/a;->aj:I

    if-ne p1, v2, :cond_0

    .line 638
    const-string v3, "application/ttml+xml"

    .line 659
    :goto_0
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, -0x1

    const/4 v6, 0x0

    const/4 v8, -0x1

    const/4 v9, 0x0

    move-object/from16 v7, p5

    invoke-static/range {v2 .. v12}, Lcom/google/android/exoplayer2/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;ILcom/google/android/exoplayer2/drm/a;JLjava/util/List;)Lcom/google/android/exoplayer2/j;

    move-result-object v2

    move-object/from16 v0, p6

    iput-object v2, v0, Lcom/google/android/exoplayer2/extractor/c/b$c;->b:Lcom/google/android/exoplayer2/j;

    .line 661
    return-void

    .line 639
    :cond_0
    sget v2, Lcom/google/android/exoplayer2/extractor/c/a;->au:I

    if-ne p1, v2, :cond_1

    .line 640
    const-string v3, "application/x-quicktime-tx3g"

    .line 641
    add-int/lit8 v2, p3, -0x8

    add-int/lit8 v2, v2, -0x8

    .line 642
    new-array v4, v2, [B

    .line 643
    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5, v2}, Lcom/google/android/exoplayer2/util/k;->a([BII)V

    .line 644
    invoke-static {v4}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v12

    goto :goto_0

    .line 645
    :cond_1
    sget v2, Lcom/google/android/exoplayer2/extractor/c/a;->av:I

    if-ne p1, v2, :cond_2

    .line 646
    const-string v3, "application/x-mp4-vtt"

    goto :goto_0

    .line 647
    :cond_2
    sget v2, Lcom/google/android/exoplayer2/extractor/c/a;->aw:I

    if-ne p1, v2, :cond_3

    .line 648
    const-string v3, "application/ttml+xml"

    .line 649
    const-wide/16 v10, 0x0

    goto :goto_0

    .line 650
    :cond_3
    sget v2, Lcom/google/android/exoplayer2/extractor/c/a;->ax:I

    if-ne p1, v2, :cond_4

    .line 652
    const-string v3, "application/x-mp4-cea-608"

    .line 653
    const/4 v2, 0x1

    move-object/from16 v0, p6

    iput v2, v0, Lcom/google/android/exoplayer2/extractor/c/b$c;->d:I

    goto :goto_0

    .line 656
    :cond_4
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2}, Ljava/lang/IllegalStateException;-><init>()V

    throw v2
.end method

.method private static a(Lcom/google/android/exoplayer2/util/k;IIIILjava/lang/String;ZLcom/google/android/exoplayer2/drm/a;Lcom/google/android/exoplayer2/extractor/c/b$c;I)V
    .locals 25

    .prologue
    .line 820
    add-int/lit8 v4, p2, 0x8

    add-int/lit8 v4, v4, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 822
    const/4 v4, 0x0

    .line 823
    if-eqz p6, :cond_5

    .line 824
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->h()I

    move-result v4

    .line 825
    const/4 v5, 0x6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    move v6, v4

    .line 833
    :goto_0
    if-eqz v6, :cond_0

    const/4 v4, 0x1

    if-ne v6, v4, :cond_6

    .line 834
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->h()I

    move-result v5

    .line 835
    const/4 v4, 0x6

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 836
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->r()I

    move-result v4

    .line 838
    const/4 v7, 0x1

    if-ne v6, v7, :cond_20

    .line 839
    const/16 v6, 0x10

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    move v6, v5

    move v5, v4

    .line 855
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->d()I

    move-result v15

    .line 856
    sget v4, Lcom/google/android/exoplayer2/extractor/c/a;->aa:I

    move/from16 v0, p1

    if-ne v0, v4, :cond_1f

    .line 857
    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/google/android/exoplayer2/extractor/c/b;->b(Lcom/google/android/exoplayer2/util/k;II)Landroid/util/Pair;

    move-result-object v8

    .line 859
    if-eqz v8, :cond_1e

    .line 860
    iget-object v4, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 861
    if-nez p7, :cond_7

    const/4 v4, 0x0

    move-object v7, v4

    .line 863
    :goto_2
    move-object/from16 v0, p8

    iget-object v9, v0, Lcom/google/android/exoplayer2/extractor/c/b$c;->a:[Lcom/google/android/exoplayer2/extractor/c/k;

    iget-object v4, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Lcom/google/android/exoplayer2/extractor/c/k;

    aput-object v4, v9, p9

    .line 865
    :goto_3
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    move-object v12, v7

    .line 873
    :goto_4
    const/4 v4, 0x0

    .line 874
    sget v7, Lcom/google/android/exoplayer2/extractor/c/a;->n:I

    move/from16 v0, p1

    if-ne v0, v7, :cond_8

    .line 875
    const-string v4, "audio/ac3"

    .line 896
    :cond_1
    :goto_5
    const/16 v18, 0x0

    move v10, v5

    move v9, v6

    move-object v5, v4

    .line 897
    :goto_6
    sub-int v4, v15, p2

    move/from16 v0, p3

    if-ge v4, v0, :cond_19

    .line 898
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 899
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v16

    .line 900
    if-lez v16, :cond_13

    const/4 v4, 0x1

    :goto_7
    const-string v6, "childAtomSize should be positive"

    invoke-static {v4, v6}, Lcom/google/android/exoplayer2/util/a;->a(ZLjava/lang/Object;)V

    .line 901
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v4

    .line 902
    sget v6, Lcom/google/android/exoplayer2/extractor/c/a;->J:I

    if-eq v4, v6, :cond_2

    if-eqz p6, :cond_15

    sget v6, Lcom/google/android/exoplayer2/extractor/c/a;->k:I

    if-ne v4, v6, :cond_15

    .line 903
    :cond_2
    sget v6, Lcom/google/android/exoplayer2/extractor/c/a;->J:I

    if-ne v4, v6, :cond_14

    move v4, v15

    .line 905
    :goto_8
    const/4 v6, -0x1

    if-eq v4, v6, :cond_1d

    .line 907
    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/google/android/exoplayer2/extractor/c/b;->d(Lcom/google/android/exoplayer2/util/k;I)Landroid/util/Pair;

    move-result-object v6

    .line 908
    iget-object v4, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object v5, v4

    check-cast v5, Ljava/lang/String;

    .line 909
    iget-object v4, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object v6, v4

    check-cast v6, [B

    .line 910
    const-string v4, "audio/mp4a-latm"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 914
    invoke-static {v6}, Lcom/google/android/exoplayer2/util/b;->a([B)Landroid/util/Pair;

    move-result-object v7

    .line 915
    iget-object v4, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 916
    iget-object v4, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v9

    :cond_3
    :goto_9
    move-object/from16 v18, v6

    .line 936
    :cond_4
    :goto_a
    add-int v15, v15, v16

    .line 937
    goto :goto_6

    .line 827
    :cond_5
    const/16 v5, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    move v6, v4

    goto/16 :goto_0

    .line 841
    :cond_6
    const/4 v4, 0x2

    if-ne v6, v4, :cond_1a

    .line 842
    const/16 v4, 0x10

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 844
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->w()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v4, v4

    .line 845
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v5

    .line 849
    const/16 v6, 0x14

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    move v6, v5

    move v5, v4

    goto/16 :goto_1

    .line 861
    :cond_7
    iget-object v4, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Lcom/google/android/exoplayer2/extractor/c/k;

    iget-object v4, v4, Lcom/google/android/exoplayer2/extractor/c/k;->b:Ljava/lang/String;

    .line 862
    move-object/from16 v0, p7

    invoke-virtual {v0, v4}, Lcom/google/android/exoplayer2/drm/a;->a(Ljava/lang/String;)Lcom/google/android/exoplayer2/drm/a;

    move-result-object v4

    move-object v7, v4

    goto/16 :goto_2

    .line 876
    :cond_8
    sget v7, Lcom/google/android/exoplayer2/extractor/c/a;->p:I

    move/from16 v0, p1

    if-ne v0, v7, :cond_9

    .line 877
    const-string v4, "audio/eac3"

    goto/16 :goto_5

    .line 878
    :cond_9
    sget v7, Lcom/google/android/exoplayer2/extractor/c/a;->r:I

    move/from16 v0, p1

    if-ne v0, v7, :cond_a

    .line 879
    const-string v4, "audio/vnd.dts"

    goto/16 :goto_5

    .line 880
    :cond_a
    sget v7, Lcom/google/android/exoplayer2/extractor/c/a;->s:I

    move/from16 v0, p1

    if-eq v0, v7, :cond_b

    sget v7, Lcom/google/android/exoplayer2/extractor/c/a;->t:I

    move/from16 v0, p1

    if-ne v0, v7, :cond_c

    .line 881
    :cond_b
    const-string v4, "audio/vnd.dts.hd"

    goto/16 :goto_5

    .line 882
    :cond_c
    sget v7, Lcom/google/android/exoplayer2/extractor/c/a;->u:I

    move/from16 v0, p1

    if-ne v0, v7, :cond_d

    .line 883
    const-string v4, "audio/vnd.dts.hd;profile=lbr"

    goto/16 :goto_5

    .line 884
    :cond_d
    sget v7, Lcom/google/android/exoplayer2/extractor/c/a;->ay:I

    move/from16 v0, p1

    if-ne v0, v7, :cond_e

    .line 885
    const-string v4, "audio/3gpp"

    goto/16 :goto_5

    .line 886
    :cond_e
    sget v7, Lcom/google/android/exoplayer2/extractor/c/a;->az:I

    move/from16 v0, p1

    if-ne v0, v7, :cond_f

    .line 887
    const-string v4, "audio/amr-wb"

    goto/16 :goto_5

    .line 888
    :cond_f
    sget v7, Lcom/google/android/exoplayer2/extractor/c/a;->l:I

    move/from16 v0, p1

    if-eq v0, v7, :cond_10

    sget v7, Lcom/google/android/exoplayer2/extractor/c/a;->m:I

    move/from16 v0, p1

    if-ne v0, v7, :cond_11

    .line 889
    :cond_10
    const-string v4, "audio/raw"

    goto/16 :goto_5

    .line 890
    :cond_11
    sget v7, Lcom/google/android/exoplayer2/extractor/c/a;->j:I

    move/from16 v0, p1

    if-ne v0, v7, :cond_12

    .line 891
    const-string v4, "audio/mpeg"

    goto/16 :goto_5

    .line 892
    :cond_12
    sget v7, Lcom/google/android/exoplayer2/extractor/c/a;->aO:I

    move/from16 v0, p1

    if-ne v0, v7, :cond_1

    .line 893
    const-string v4, "audio/alac"

    goto/16 :goto_5

    .line 900
    :cond_13
    const/4 v4, 0x0

    goto/16 :goto_7

    .line 904
    :cond_14
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v15, v1}, Lcom/google/android/exoplayer2/extractor/c/b;->a(Lcom/google/android/exoplayer2/util/k;II)I

    move-result v4

    goto/16 :goto_8

    .line 919
    :cond_15
    sget v6, Lcom/google/android/exoplayer2/extractor/c/a;->o:I

    if-ne v4, v6, :cond_16

    .line 920
    add-int/lit8 v4, v15, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 921
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-static {v0, v4, v1, v12}, Lcom/google/android/exoplayer2/audio/a;->a(Lcom/google/android/exoplayer2/util/k;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/exoplayer2/drm/a;)Lcom/google/android/exoplayer2/j;

    move-result-object v4

    move-object/from16 v0, p8

    iput-object v4, v0, Lcom/google/android/exoplayer2/extractor/c/b$c;->b:Lcom/google/android/exoplayer2/j;

    goto/16 :goto_a

    .line 923
    :cond_16
    sget v6, Lcom/google/android/exoplayer2/extractor/c/a;->q:I

    if-ne v4, v6, :cond_17

    .line 924
    add-int/lit8 v4, v15, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 925
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-static {v0, v4, v1, v12}, Lcom/google/android/exoplayer2/audio/a;->b(Lcom/google/android/exoplayer2/util/k;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/exoplayer2/drm/a;)Lcom/google/android/exoplayer2/j;

    move-result-object v4

    move-object/from16 v0, p8

    iput-object v4, v0, Lcom/google/android/exoplayer2/extractor/c/b$c;->b:Lcom/google/android/exoplayer2/j;

    goto/16 :goto_a

    .line 927
    :cond_17
    sget v6, Lcom/google/android/exoplayer2/extractor/c/a;->v:I

    if-ne v4, v6, :cond_18

    .line 928
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    const/4 v7, -0x1

    const/4 v8, -0x1

    const/4 v11, 0x0

    const/4 v13, 0x0

    move-object/from16 v14, p5

    invoke-static/range {v4 .. v14}, Lcom/google/android/exoplayer2/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIILjava/util/List;Lcom/google/android/exoplayer2/drm/a;ILjava/lang/String;)Lcom/google/android/exoplayer2/j;

    move-result-object v4

    move-object/from16 v0, p8

    iput-object v4, v0, Lcom/google/android/exoplayer2/extractor/c/b$c;->b:Lcom/google/android/exoplayer2/j;

    goto/16 :goto_a

    .line 931
    :cond_18
    sget v6, Lcom/google/android/exoplayer2/extractor/c/a;->aO:I

    if-ne v4, v6, :cond_4

    .line 932
    move/from16 v0, v16

    new-array v0, v0, [B

    move-object/from16 v18, v0

    .line 933
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 934
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v16

    invoke-virtual {v0, v1, v4, v2}, Lcom/google/android/exoplayer2/util/k;->a([BII)V

    goto/16 :goto_a

    .line 939
    :cond_19
    move-object/from16 v0, p8

    iget-object v4, v0, Lcom/google/android/exoplayer2/extractor/c/b$c;->b:Lcom/google/android/exoplayer2/j;

    if-nez v4, :cond_1a

    if-eqz v5, :cond_1a

    .line 941
    const-string v4, "audio/raw"

    .line 942
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1b

    const/16 v20, 0x2

    .line 943
    :goto_b
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v15, 0x0

    const/16 v16, -0x1

    const/16 v17, -0x1

    if-nez v18, :cond_1c

    const/16 v21, 0x0

    .line 945
    :goto_c
    const/16 v23, 0x0

    move-object v14, v5

    move/from16 v18, v9

    move/from16 v19, v10

    move-object/from16 v22, v12

    move-object/from16 v24, p5

    .line 943
    invoke-static/range {v13 .. v24}, Lcom/google/android/exoplayer2/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIIILjava/util/List;Lcom/google/android/exoplayer2/drm/a;ILjava/lang/String;)Lcom/google/android/exoplayer2/j;

    move-result-object v4

    move-object/from16 v0, p8

    iput-object v4, v0, Lcom/google/android/exoplayer2/extractor/c/b$c;->b:Lcom/google/android/exoplayer2/j;

    .line 948
    :cond_1a
    return-void

    .line 942
    :cond_1b
    const/16 v20, -0x1

    goto :goto_b

    .line 945
    :cond_1c
    invoke-static/range {v18 .. v18}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v21

    goto :goto_c

    :cond_1d
    move-object/from16 v6, v18

    goto/16 :goto_9

    :cond_1e
    move-object/from16 v7, p7

    goto/16 :goto_3

    :cond_1f
    move-object/from16 v12, p7

    goto/16 :goto_4

    :cond_20
    move v6, v5

    move v5, v4

    goto/16 :goto_1
.end method

.method private static b(Lcom/google/android/exoplayer2/util/k;II)Landroid/util/Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/util/k;",
            "II)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/exoplayer2/extractor/c/k;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1052
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->d()I

    move-result v0

    move v1, v0

    .line 1053
    :goto_0
    sub-int v0, v1, p1

    if-ge v0, p2, :cond_2

    .line 1054
    invoke-virtual {p0, v1}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 1055
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v2

    .line 1056
    if-lez v2, :cond_0

    const/4 v0, 0x1

    :goto_1
    const-string v3, "childAtomSize should be positive"

    invoke-static {v0, v3}, Lcom/google/android/exoplayer2/util/a;->a(ZLjava/lang/Object;)V

    .line 1057
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v0

    .line 1058
    sget v3, Lcom/google/android/exoplayer2/extractor/c/a;->V:I

    if-ne v0, v3, :cond_1

    .line 1059
    invoke-static {p0, v1, v2}, Lcom/google/android/exoplayer2/extractor/c/b;->c(Lcom/google/android/exoplayer2/util/k;II)Landroid/util/Pair;

    move-result-object v0

    .line 1061
    if-eqz v0, :cond_1

    .line 1067
    :goto_2
    return-object v0

    .line 1056
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 1065
    :cond_1
    add-int v0, v1, v2

    move v1, v0

    .line 1066
    goto :goto_0

    .line 1067
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private static b(Lcom/google/android/exoplayer2/util/k;)Lcom/google/android/exoplayer2/extractor/c/b$f;
    .locals 13

    .prologue
    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    const/16 v2, 0x10

    const/16 v1, 0x8

    const/4 v3, 0x4

    const/4 v6, 0x0

    .line 480
    invoke-virtual {p0, v1}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 481
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v0

    .line 482
    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/c/a;->a(I)I

    move-result v8

    .line 484
    if-nez v8, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 485
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v9

    .line 487
    invoke-virtual {p0, v3}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 488
    const/4 v0, 0x1

    .line 489
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->d()I

    move-result v10

    .line 490
    if-nez v8, :cond_0

    move v1, v3

    :cond_0
    move v7, v6

    .line 491
    :goto_1
    if-ge v7, v1, :cond_1

    .line 492
    iget-object v11, p0, Lcom/google/android/exoplayer2/util/k;->a:[B

    add-int v12, v10, v7

    aget-byte v11, v11, v12

    const/4 v12, -0x1

    if-eq v11, v12, :cond_4

    move v0, v6

    .line 498
    :cond_1
    if-eqz v0, :cond_5

    .line 499
    invoke-virtual {p0, v1}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    move-wide v0, v4

    .line 510
    :cond_2
    :goto_2
    invoke-virtual {p0, v2}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 511
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v2

    .line 512
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v4

    .line 513
    invoke-virtual {p0, v3}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 514
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v3

    .line 515
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v5

    .line 518
    const/high16 v7, 0x10000

    .line 519
    if-nez v2, :cond_7

    if-ne v4, v7, :cond_7

    neg-int v8, v7

    if-ne v3, v8, :cond_7

    if-nez v5, :cond_7

    .line 520
    const/16 v2, 0x5a

    .line 530
    :goto_3
    new-instance v3, Lcom/google/android/exoplayer2/extractor/c/b$f;

    invoke-direct {v3, v9, v0, v1, v2}, Lcom/google/android/exoplayer2/extractor/c/b$f;-><init>(IJI)V

    return-object v3

    :cond_3
    move v0, v2

    .line 484
    goto :goto_0

    .line 491
    :cond_4
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 502
    :cond_5
    if-nez v8, :cond_6

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->l()J

    move-result-wide v0

    .line 503
    :goto_4
    const-wide/16 v10, 0x0

    cmp-long v7, v0, v10

    if-nez v7, :cond_2

    move-wide v0, v4

    .line 506
    goto :goto_2

    .line 502
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->v()J

    move-result-wide v0

    goto :goto_4

    .line 521
    :cond_7
    if-nez v2, :cond_8

    neg-int v8, v7

    if-ne v4, v8, :cond_8

    if-ne v3, v7, :cond_8

    if-nez v5, :cond_8

    .line 522
    const/16 v2, 0x10e

    goto :goto_3

    .line 523
    :cond_8
    neg-int v8, v7

    if-ne v2, v8, :cond_9

    if-nez v4, :cond_9

    if-nez v3, :cond_9

    neg-int v2, v7

    if-ne v5, v2, :cond_9

    .line 524
    const/16 v2, 0xb4

    goto :goto_3

    :cond_9
    move v2, v6

    .line 527
    goto :goto_3
.end method

.method private static b(Lcom/google/android/exoplayer2/util/k;I)Lcom/google/android/exoplayer2/metadata/a;
    .locals 2

    .prologue
    .line 449
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 450
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 451
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->d()I

    move-result v0

    if-ge v0, p1, :cond_1

    .line 452
    invoke-static {p0}, Lcom/google/android/exoplayer2/extractor/c/f;->a(Lcom/google/android/exoplayer2/util/k;)Lcom/google/android/exoplayer2/metadata/a$a;

    move-result-object v0

    .line 453
    if-eqz v0, :cond_0

    .line 454
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 457
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_2
    new-instance v0, Lcom/google/android/exoplayer2/metadata/a;

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/metadata/a;-><init>(Ljava/util/List;)V

    goto :goto_1
.end method

.method private static c(Lcom/google/android/exoplayer2/util/k;I)F
    .locals 2

    .prologue
    .line 811
    add-int/lit8 v0, p1, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 812
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v0

    .line 813
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v1

    .line 814
    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method private static c(Lcom/google/android/exoplayer2/util/k;)I
    .locals 2

    .prologue
    .line 540
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 541
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v0

    .line 542
    sget v1, Lcom/google/android/exoplayer2/extractor/c/b;->b:I

    if-ne v0, v1, :cond_0

    .line 543
    const/4 v0, 0x1

    .line 552
    :goto_0
    return v0

    .line 544
    :cond_0
    sget v1, Lcom/google/android/exoplayer2/extractor/c/b;->a:I

    if-ne v0, v1, :cond_1

    .line 545
    const/4 v0, 0x2

    goto :goto_0

    .line 546
    :cond_1
    sget v1, Lcom/google/android/exoplayer2/extractor/c/b;->c:I

    if-eq v0, v1, :cond_2

    sget v1, Lcom/google/android/exoplayer2/extractor/c/b;->d:I

    if-eq v0, v1, :cond_2

    sget v1, Lcom/google/android/exoplayer2/extractor/c/b;->e:I

    if-eq v0, v1, :cond_2

    sget v1, Lcom/google/android/exoplayer2/extractor/c/b;->f:I

    if-ne v0, v1, :cond_3

    .line 548
    :cond_2
    const/4 v0, 0x3

    goto :goto_0

    .line 549
    :cond_3
    sget v1, Lcom/google/android/exoplayer2/extractor/c/b;->h:I

    if-ne v0, v1, :cond_4

    .line 550
    const/4 v0, 0x4

    goto :goto_0

    .line 552
    :cond_4
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private static c(Lcom/google/android/exoplayer2/util/k;II)Landroid/util/Pair;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/util/k;",
            "II)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/exoplayer2/extractor/c/k;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v12, 0x4

    const/4 v9, -0x1

    const/4 v8, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1072
    add-int/lit8 v5, p1, 0x8

    move-object v0, v8

    move-object v1, v8

    move v2, v7

    move v4, v9

    .line 1077
    :goto_0
    sub-int v3, v5, p1

    if-ge v3, p2, :cond_3

    .line 1078
    invoke-virtual {p0, v5}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 1079
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v3

    .line 1080
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v10

    .line 1081
    sget v11, Lcom/google/android/exoplayer2/extractor/c/a;->ab:I

    if-ne v10, v11, :cond_1

    .line 1082
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1091
    :cond_0
    :goto_1
    add-int/2addr v5, v3

    .line 1092
    goto :goto_0

    .line 1083
    :cond_1
    sget v11, Lcom/google/android/exoplayer2/extractor/c/a;->W:I

    if-ne v10, v11, :cond_2

    .line 1084
    invoke-virtual {p0, v12}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 1086
    invoke-virtual {p0, v12}, Lcom/google/android/exoplayer2/util/k;->e(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 1087
    :cond_2
    sget v11, Lcom/google/android/exoplayer2/extractor/c/a;->X:I

    if-ne v10, v11, :cond_0

    move v2, v3

    move v4, v5

    .line 1089
    goto :goto_1

    .line 1094
    :cond_3
    if-eqz v1, :cond_7

    .line 1095
    if-eqz v0, :cond_4

    move v3, v6

    :goto_2
    const-string v5, "frma atom is mandatory"

    invoke-static {v3, v5}, Lcom/google/android/exoplayer2/util/a;->a(ZLjava/lang/Object;)V

    .line 1096
    if-eq v4, v9, :cond_5

    move v3, v6

    :goto_3
    const-string v5, "schi atom is mandatory"

    invoke-static {v3, v5}, Lcom/google/android/exoplayer2/util/a;->a(ZLjava/lang/Object;)V

    .line 1098
    invoke-static {p0, v4, v2, v1}, Lcom/google/android/exoplayer2/extractor/c/b;->a(Lcom/google/android/exoplayer2/util/k;IILjava/lang/String;)Lcom/google/android/exoplayer2/extractor/c/k;

    move-result-object v1

    .line 1100
    if-eqz v1, :cond_6

    :goto_4
    const-string v2, "tenc atom is mandatory"

    invoke-static {v6, v2}, Lcom/google/android/exoplayer2/util/a;->a(ZLjava/lang/Object;)V

    .line 1101
    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 1103
    :goto_5
    return-object v0

    :cond_4
    move v3, v7

    .line 1095
    goto :goto_2

    :cond_5
    move v3, v7

    .line 1096
    goto :goto_3

    :cond_6
    move v6, v7

    .line 1100
    goto :goto_4

    :cond_7
    move-object v0, v8

    .line 1103
    goto :goto_5
.end method

.method private static d(Lcom/google/android/exoplayer2/util/k;)Landroid/util/Pair;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/util/k;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v1, 0x8

    .line 564
    invoke-virtual {p0, v1}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 565
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v0

    .line 566
    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/c/a;->a(I)I

    move-result v2

    .line 567
    if-nez v2, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 568
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->l()J

    move-result-wide v4

    .line 569
    if-nez v2, :cond_0

    const/4 v1, 0x4

    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 570
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->h()I

    move-result v0

    .line 571
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    shr-int/lit8 v2, v0, 0xa

    and-int/lit8 v2, v2, 0x1f

    add-int/lit8 v2, v2, 0x60

    int-to-char v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    shr-int/lit8 v2, v0, 0x5

    and-int/lit8 v2, v2, 0x1f

    add-int/lit8 v2, v2, 0x60

    int-to-char v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    and-int/lit8 v0, v0, 0x1f

    add-int/lit8 v0, v0, 0x60

    int-to-char v0, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 574
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    .line 567
    :cond_1
    const/16 v0, 0x10

    goto :goto_0
.end method

.method private static d(Lcom/google/android/exoplayer2/util/k;I)Landroid/util/Pair;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/util/k;",
            "I)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "[B>;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 973
    add-int/lit8 v1, p1, 0x8

    add-int/lit8 v1, v1, 0x4

    invoke-virtual {p0, v1}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 975
    invoke-virtual {p0, v3}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 976
    invoke-static {p0}, Lcom/google/android/exoplayer2/extractor/c/b;->e(Lcom/google/android/exoplayer2/util/k;)I

    .line 977
    invoke-virtual {p0, v4}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 979
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->g()I

    move-result v1

    .line 980
    and-int/lit16 v2, v1, 0x80

    if-eqz v2, :cond_0

    .line 981
    invoke-virtual {p0, v4}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 983
    :cond_0
    and-int/lit8 v2, v1, 0x40

    if-eqz v2, :cond_1

    .line 984
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->h()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 986
    :cond_1
    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_2

    .line 987
    invoke-virtual {p0, v4}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 991
    :cond_2
    invoke-virtual {p0, v3}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 992
    invoke-static {p0}, Lcom/google/android/exoplayer2/extractor/c/b;->e(Lcom/google/android/exoplayer2/util/k;)I

    .line 995
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->g()I

    move-result v1

    .line 997
    sparse-switch v1, :sswitch_data_0

    .line 1035
    :goto_0
    const/16 v1, 0xc

    invoke-virtual {p0, v1}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 1038
    invoke-virtual {p0, v3}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 1039
    invoke-static {p0}, Lcom/google/android/exoplayer2/extractor/c/b;->e(Lcom/google/android/exoplayer2/util/k;)I

    move-result v1

    .line 1040
    new-array v2, v1, [B

    .line 1041
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3, v1}, Lcom/google/android/exoplayer2/util/k;->a([BII)V

    .line 1042
    invoke-static {v0, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    :goto_1
    return-object v0

    .line 999
    :sswitch_0
    const-string v1, "audio/mpeg"

    .line 1000
    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_1

    .line 1002
    :sswitch_1
    const-string v0, "video/mp4v-es"

    goto :goto_0

    .line 1005
    :sswitch_2
    const-string v0, "video/avc"

    goto :goto_0

    .line 1008
    :sswitch_3
    const-string v0, "video/hevc"

    goto :goto_0

    .line 1014
    :sswitch_4
    const-string v0, "audio/mp4a-latm"

    goto :goto_0

    .line 1017
    :sswitch_5
    const-string v0, "audio/ac3"

    goto :goto_0

    .line 1020
    :sswitch_6
    const-string v0, "audio/eac3"

    goto :goto_0

    .line 1024
    :sswitch_7
    const-string v1, "audio/vnd.dts"

    .line 1025
    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_1

    .line 1028
    :sswitch_8
    const-string v1, "audio/vnd.dts.hd"

    .line 1029
    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_1

    .line 997
    nop

    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_1
        0x21 -> :sswitch_2
        0x23 -> :sswitch_3
        0x40 -> :sswitch_4
        0x66 -> :sswitch_4
        0x67 -> :sswitch_4
        0x68 -> :sswitch_4
        0x6b -> :sswitch_0
        0xa5 -> :sswitch_5
        0xa6 -> :sswitch_6
        0xa9 -> :sswitch_7
        0xaa -> :sswitch_8
        0xab -> :sswitch_8
        0xac -> :sswitch_7
    .end sparse-switch
.end method

.method private static d(Lcom/google/android/exoplayer2/util/k;II)[B
    .locals 4

    .prologue
    .line 1149
    add-int/lit8 v0, p1, 0x8

    .line 1150
    :goto_0
    sub-int v1, v0, p1

    if-ge v1, p2, :cond_1

    .line 1151
    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 1152
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v1

    .line 1153
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v2

    .line 1154
    sget v3, Lcom/google/android/exoplayer2/extractor/c/a;->aJ:I

    if-ne v2, v3, :cond_0

    .line 1155
    iget-object v2, p0, Lcom/google/android/exoplayer2/util/k;->a:[B

    add-int/2addr v1, v0

    invoke-static {v2, v0, v1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    .line 1159
    :goto_1
    return-object v0

    .line 1157
    :cond_0
    add-int/2addr v0, v1

    .line 1158
    goto :goto_0

    .line 1159
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static e(Lcom/google/android/exoplayer2/util/k;)I
    .locals 3

    .prologue
    .line 1166
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->g()I

    move-result v1

    .line 1167
    and-int/lit8 v0, v1, 0x7f

    .line 1168
    :goto_0
    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_0

    .line 1169
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->g()I

    move-result v1

    .line 1170
    shl-int/lit8 v0, v0, 0x7

    and-int/lit8 v2, v1, 0x7f

    or-int/2addr v0, v2

    goto :goto_0

    .line 1172
    :cond_0
    return v0
.end method
