.class public final Lcom/google/android/exoplayer2/extractor/c/e;
.super Ljava/lang/Object;
.source "FragmentedMp4Extractor.java"

# interfaces
.implements Lcom/google/android/exoplayer2/extractor/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/extractor/c/e$b;,
        Lcom/google/android/exoplayer2/extractor/c/e$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/google/android/exoplayer2/extractor/h;

.field private static final b:I

.field private static final c:[B


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:Z

.field private E:Lcom/google/android/exoplayer2/extractor/g;

.field private F:Lcom/google/android/exoplayer2/extractor/m;

.field private G:[Lcom/google/android/exoplayer2/extractor/m;

.field private H:Z

.field private final d:I

.field private final e:Lcom/google/android/exoplayer2/extractor/c/j;

.field private final f:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/exoplayer2/extractor/c/e$b;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/google/android/exoplayer2/util/k;

.field private final h:Lcom/google/android/exoplayer2/util/k;

.field private final i:Lcom/google/android/exoplayer2/util/k;

.field private final j:Lcom/google/android/exoplayer2/util/k;

.field private final k:Lcom/google/android/exoplayer2/util/k;

.field private final l:Lcom/google/android/exoplayer2/util/p;

.field private final m:Lcom/google/android/exoplayer2/util/k;

.field private final n:[B

.field private final o:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/google/android/exoplayer2/extractor/c/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/exoplayer2/extractor/c/e$a;",
            ">;"
        }
    .end annotation
.end field

.field private q:I

.field private r:I

.field private s:J

.field private t:I

.field private u:Lcom/google/android/exoplayer2/util/k;

.field private v:J

.field private w:I

.field private x:J

.field private y:J

.field private z:Lcom/google/android/exoplayer2/extractor/c/e$b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lcom/google/android/exoplayer2/extractor/c/e$1;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/extractor/c/e$1;-><init>()V

    sput-object v0, Lcom/google/android/exoplayer2/extractor/c/e;->a:Lcom/google/android/exoplayer2/extractor/h;

    .line 108
    const-string v0, "seig"

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/s;->f(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/exoplayer2/extractor/c/e;->b:I

    .line 109
    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/exoplayer2/extractor/c/e;->c:[B

    return-void

    nop

    :array_0
    .array-data 1
        -0x5et
        0x39t
        0x4ft
        0x52t
        0x5at
        -0x65t
        0x4ft
        0x14t
        -0x5et
        0x44t
        0x6ct
        0x42t
        0x7ct
        0x64t
        -0x73t
        -0xct
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/extractor/c/e;-><init>(I)V

    .line 168
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 174
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/exoplayer2/extractor/c/e;-><init>(ILcom/google/android/exoplayer2/util/p;)V

    .line 175
    return-void
.end method

.method public constructor <init>(ILcom/google/android/exoplayer2/util/p;)V
    .locals 1

    .prologue
    .line 182
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/exoplayer2/extractor/c/e;-><init>(ILcom/google/android/exoplayer2/util/p;Lcom/google/android/exoplayer2/extractor/c/j;)V

    .line 183
    return-void
.end method

.method public constructor <init>(ILcom/google/android/exoplayer2/util/p;Lcom/google/android/exoplayer2/extractor/c/j;)V
    .locals 6

    .prologue
    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    const/16 v1, 0x10

    .line 192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 193
    if-eqz p3, :cond_0

    move v0, v1

    :goto_0
    or-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->d:I

    .line 194
    iput-object p2, p0, Lcom/google/android/exoplayer2/extractor/c/e;->l:Lcom/google/android/exoplayer2/util/p;

    .line 195
    iput-object p3, p0, Lcom/google/android/exoplayer2/extractor/c/e;->e:Lcom/google/android/exoplayer2/extractor/c/j;

    .line 196
    new-instance v0, Lcom/google/android/exoplayer2/util/k;

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/util/k;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->m:Lcom/google/android/exoplayer2/util/k;

    .line 197
    new-instance v0, Lcom/google/android/exoplayer2/util/k;

    sget-object v2, Lcom/google/android/exoplayer2/util/i;->a:[B

    invoke-direct {v0, v2}, Lcom/google/android/exoplayer2/util/k;-><init>([B)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->g:Lcom/google/android/exoplayer2/util/k;

    .line 198
    new-instance v0, Lcom/google/android/exoplayer2/util/k;

    const/4 v2, 0x5

    invoke-direct {v0, v2}, Lcom/google/android/exoplayer2/util/k;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->h:Lcom/google/android/exoplayer2/util/k;

    .line 199
    new-instance v0, Lcom/google/android/exoplayer2/util/k;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/util/k;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->i:Lcom/google/android/exoplayer2/util/k;

    .line 200
    new-instance v0, Lcom/google/android/exoplayer2/util/k;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Lcom/google/android/exoplayer2/util/k;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->j:Lcom/google/android/exoplayer2/util/k;

    .line 201
    new-instance v0, Lcom/google/android/exoplayer2/util/k;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/util/k;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->k:Lcom/google/android/exoplayer2/util/k;

    .line 202
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->n:[B

    .line 203
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->o:Ljava/util/Stack;

    .line 204
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->p:Ljava/util/LinkedList;

    .line 205
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->f:Landroid/util/SparseArray;

    .line 206
    iput-wide v4, p0, Lcom/google/android/exoplayer2/extractor/c/e;->x:J

    .line 207
    iput-wide v4, p0, Lcom/google/android/exoplayer2/extractor/c/e;->y:J

    .line 208
    invoke-direct {p0}, Lcom/google/android/exoplayer2/extractor/c/e;->a()V

    .line 209
    return-void

    .line 193
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/android/exoplayer2/extractor/c/e$b;)I
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 1211
    iget-object v4, p1, Lcom/google/android/exoplayer2/extractor/c/e$b;->a:Lcom/google/android/exoplayer2/extractor/c/l;

    .line 1212
    iget-object v0, v4, Lcom/google/android/exoplayer2/extractor/c/l;->a:Lcom/google/android/exoplayer2/extractor/c/c;

    iget v0, v0, Lcom/google/android/exoplayer2/extractor/c/c;->a:I

    .line 1213
    iget-object v1, v4, Lcom/google/android/exoplayer2/extractor/c/l;->o:Lcom/google/android/exoplayer2/extractor/c/k;

    if-eqz v1, :cond_0

    iget-object v0, v4, Lcom/google/android/exoplayer2/extractor/c/l;->o:Lcom/google/android/exoplayer2/extractor/c/k;

    .line 1219
    :goto_0
    iget v1, v0, Lcom/google/android/exoplayer2/extractor/c/k;->d:I

    if-eqz v1, :cond_1

    .line 1220
    iget-object v1, v4, Lcom/google/android/exoplayer2/extractor/c/l;->q:Lcom/google/android/exoplayer2/util/k;

    .line 1221
    iget v0, v0, Lcom/google/android/exoplayer2/extractor/c/k;->d:I

    .line 1230
    :goto_1
    iget-object v2, v4, Lcom/google/android/exoplayer2/extractor/c/l;->n:[Z

    iget v5, p1, Lcom/google/android/exoplayer2/extractor/c/e$b;->e:I

    aget-boolean v5, v2, v5

    .line 1234
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/c/e;->j:Lcom/google/android/exoplayer2/util/k;

    iget-object v6, v2, Lcom/google/android/exoplayer2/util/k;->a:[B

    if-eqz v5, :cond_2

    const/16 v2, 0x80

    :goto_2
    or-int/2addr v2, v0

    int-to-byte v2, v2

    aput-byte v2, v6, v3

    .line 1235
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/c/e;->j:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 1236
    iget-object v2, p1, Lcom/google/android/exoplayer2/extractor/c/e$b;->b:Lcom/google/android/exoplayer2/extractor/m;

    .line 1237
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/c/e;->j:Lcom/google/android/exoplayer2/util/k;

    const/4 v6, 0x1

    invoke-interface {v2, v3, v6}, Lcom/google/android/exoplayer2/extractor/m;->a(Lcom/google/android/exoplayer2/util/k;I)V

    .line 1239
    invoke-interface {v2, v1, v0}, Lcom/google/android/exoplayer2/extractor/m;->a(Lcom/google/android/exoplayer2/util/k;I)V

    .line 1241
    if-nez v5, :cond_3

    .line 1242
    add-int/lit8 v0, v0, 0x1

    .line 1250
    :goto_3
    return v0

    .line 1213
    :cond_0
    iget-object v1, p1, Lcom/google/android/exoplayer2/extractor/c/e$b;->c:Lcom/google/android/exoplayer2/extractor/c/j;

    .line 1215
    invoke-virtual {v1, v0}, Lcom/google/android/exoplayer2/extractor/c/j;->a(I)Lcom/google/android/exoplayer2/extractor/c/k;

    move-result-object v0

    goto :goto_0

    .line 1224
    :cond_1
    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/c/k;->e:[B

    .line 1225
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/c/e;->k:Lcom/google/android/exoplayer2/util/k;

    array-length v2, v0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/exoplayer2/util/k;->a([BI)V

    .line 1226
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/c/e;->k:Lcom/google/android/exoplayer2/util/k;

    .line 1227
    array-length v0, v0

    goto :goto_1

    :cond_2
    move v2, v3

    .line 1234
    goto :goto_2

    .line 1245
    :cond_3
    iget-object v1, v4, Lcom/google/android/exoplayer2/extractor/c/l;->q:Lcom/google/android/exoplayer2/util/k;

    .line 1246
    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/k;->h()I

    move-result v3

    .line 1247
    const/4 v4, -0x2

    invoke-virtual {v1, v4}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 1248
    mul-int/lit8 v3, v3, 0x6

    add-int/lit8 v3, v3, 0x2

    .line 1249
    invoke-interface {v2, v1, v3}, Lcom/google/android/exoplayer2/extractor/m;->a(Lcom/google/android/exoplayer2/util/k;I)V

    .line 1250
    add-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v3

    goto :goto_3
.end method

.method private static a(Lcom/google/android/exoplayer2/extractor/c/e$b;IJILcom/google/android/exoplayer2/util/k;I)I
    .locals 30

    .prologue
    .line 756
    const/16 v2, 0x8

    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 757
    invoke-virtual/range {p5 .. p5}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v2

    .line 758
    invoke-static {v2}, Lcom/google/android/exoplayer2/extractor/c/a;->b(I)I

    move-result v3

    .line 760
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/exoplayer2/extractor/c/e$b;->c:Lcom/google/android/exoplayer2/extractor/c/j;

    .line 761
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/c/e$b;->a:Lcom/google/android/exoplayer2/extractor/c/l;

    move-object/from16 v20, v0

    .line 762
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/c/l;->a:Lcom/google/android/exoplayer2/extractor/c/c;

    move-object/from16 v21, v0

    .line 764
    move-object/from16 v0, v20

    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/c/l;->h:[I

    invoke-virtual/range {p5 .. p5}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v4

    aput v4, v2, p1

    .line 765
    move-object/from16 v0, v20

    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/c/l;->g:[J

    move-object/from16 v0, v20

    iget-wide v4, v0, Lcom/google/android/exoplayer2/extractor/c/l;->c:J

    aput-wide v4, v2, p1

    .line 766
    and-int/lit8 v2, v3, 0x1

    if-eqz v2, :cond_0

    .line 767
    move-object/from16 v0, v20

    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/c/l;->g:[J

    aget-wide v4, v2, p1

    invoke-virtual/range {p5 .. p5}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v6

    int-to-long v6, v6

    add-long/2addr v4, v6

    aput-wide v4, v2, p1

    .line 770
    :cond_0
    and-int/lit8 v2, v3, 0x4

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    move v8, v2

    .line 771
    :goto_0
    move-object/from16 v0, v21

    iget v14, v0, Lcom/google/android/exoplayer2/extractor/c/c;->d:I

    .line 772
    if-eqz v8, :cond_1

    .line 773
    invoke-virtual/range {p5 .. p5}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v14

    .line 776
    :cond_1
    and-int/lit16 v2, v3, 0x100

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    move/from16 v19, v2

    .line 777
    :goto_1
    and-int/lit16 v2, v3, 0x200

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    move/from16 v18, v2

    .line 778
    :goto_2
    and-int/lit16 v2, v3, 0x400

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    move/from16 v17, v2

    .line 779
    :goto_3
    and-int/lit16 v2, v3, 0x800

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    move v9, v2

    .line 784
    :goto_4
    const-wide/16 v2, 0x0

    .line 788
    iget-object v4, v13, Lcom/google/android/exoplayer2/extractor/c/j;->h:[J

    if-eqz v4, :cond_11

    iget-object v4, v13, Lcom/google/android/exoplayer2/extractor/c/j;->h:[J

    array-length v4, v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_11

    iget-object v4, v13, Lcom/google/android/exoplayer2/extractor/c/j;->h:[J

    const/4 v5, 0x0

    aget-wide v4, v4, v5

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_11

    .line 790
    iget-object v2, v13, Lcom/google/android/exoplayer2/extractor/c/j;->i:[J

    const/4 v3, 0x0

    aget-wide v2, v2, v3

    const-wide/16 v4, 0x3e8

    iget-wide v6, v13, Lcom/google/android/exoplayer2/extractor/c/j;->c:J

    invoke-static/range {v2 .. v7}, Lcom/google/android/exoplayer2/util/s;->b(JJJ)J

    move-result-wide v2

    move-wide v10, v2

    .line 793
    :goto_5
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/c/l;->i:[I

    move-object/from16 v22, v0

    .line 794
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/c/l;->j:[I

    move-object/from16 v23, v0

    .line 795
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/c/l;->k:[J

    move-object/from16 v24, v0

    .line 796
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/c/l;->l:[Z

    move-object/from16 v25, v0

    .line 798
    iget v2, v13, Lcom/google/android/exoplayer2/extractor/c/j;->b:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_9

    and-int/lit8 v2, p4, 0x1

    if-eqz v2, :cond_9

    const/4 v2, 0x1

    move v12, v2

    .line 801
    :goto_6
    move-object/from16 v0, v20

    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/c/l;->h:[I

    aget v2, v2, p1

    add-int v26, p6, v2

    .line 802
    iget-wide v6, v13, Lcom/google/android/exoplayer2/extractor/c/j;->c:J

    .line 803
    if-lez p1, :cond_2

    move-object/from16 v0, v20

    iget-wide v0, v0, Lcom/google/android/exoplayer2/extractor/c/l;->s:J

    move-wide/from16 p2, v0

    :cond_2
    move-wide/from16 v2, p2

    .line 804
    :goto_7
    move/from16 v0, p6

    move/from16 v1, v26

    if-ge v0, v1, :cond_10

    .line 806
    if-eqz v19, :cond_a

    invoke-virtual/range {p5 .. p5}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v4

    move/from16 v16, v4

    .line 808
    :goto_8
    if-eqz v18, :cond_b

    invoke-virtual/range {p5 .. p5}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v4

    move v15, v4

    .line 809
    :goto_9
    if-nez p6, :cond_c

    if-eqz v8, :cond_c

    move v13, v14

    .line 811
    :goto_a
    if-eqz v9, :cond_e

    .line 817
    invoke-virtual/range {p5 .. p5}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v4

    .line 818
    int-to-long v4, v4

    const-wide/16 v28, 0x3e8

    mul-long v4, v4, v28

    div-long/2addr v4, v6

    long-to-int v4, v4

    aput v4, v23, p6

    .line 822
    :goto_b
    const-wide/16 v4, 0x3e8

    .line 823
    invoke-static/range {v2 .. v7}, Lcom/google/android/exoplayer2/util/s;->b(JJJ)J

    move-result-wide v4

    sub-long/2addr v4, v10

    aput-wide v4, v24, p6

    .line 824
    aput v15, v22, p6

    .line 825
    shr-int/lit8 v4, v13, 0x10

    and-int/lit8 v4, v4, 0x1

    if-nez v4, :cond_f

    if-eqz v12, :cond_3

    if-nez p6, :cond_f

    :cond_3
    const/4 v4, 0x1

    :goto_c
    aput-boolean v4, v25, p6

    .line 827
    move/from16 v0, v16

    int-to-long v4, v0

    add-long/2addr v2, v4

    .line 804
    add-int/lit8 p6, p6, 0x1

    goto :goto_7

    .line 770
    :cond_4
    const/4 v2, 0x0

    move v8, v2

    goto/16 :goto_0

    .line 776
    :cond_5
    const/4 v2, 0x0

    move/from16 v19, v2

    goto/16 :goto_1

    .line 777
    :cond_6
    const/4 v2, 0x0

    move/from16 v18, v2

    goto/16 :goto_2

    .line 778
    :cond_7
    const/4 v2, 0x0

    move/from16 v17, v2

    goto/16 :goto_3

    .line 779
    :cond_8
    const/4 v2, 0x0

    move v9, v2

    goto/16 :goto_4

    .line 798
    :cond_9
    const/4 v2, 0x0

    move v12, v2

    goto :goto_6

    .line 806
    :cond_a
    move-object/from16 v0, v21

    iget v4, v0, Lcom/google/android/exoplayer2/extractor/c/c;->b:I

    move/from16 v16, v4

    goto :goto_8

    .line 808
    :cond_b
    move-object/from16 v0, v21

    iget v4, v0, Lcom/google/android/exoplayer2/extractor/c/c;->c:I

    move v15, v4

    goto :goto_9

    .line 809
    :cond_c
    if-eqz v17, :cond_d

    .line 810
    invoke-virtual/range {p5 .. p5}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v4

    move v13, v4

    goto :goto_a

    :cond_d
    move-object/from16 v0, v21

    iget v4, v0, Lcom/google/android/exoplayer2/extractor/c/c;->d:I

    move v13, v4

    goto :goto_a

    .line 820
    :cond_e
    const/4 v4, 0x0

    aput v4, v23, p6

    goto :goto_b

    .line 825
    :cond_f
    const/4 v4, 0x0

    goto :goto_c

    .line 829
    :cond_10
    move-object/from16 v0, v20

    iput-wide v2, v0, Lcom/google/android/exoplayer2/extractor/c/l;->s:J

    .line 830
    return v26

    :cond_11
    move-wide v10, v2

    goto/16 :goto_5
.end method

.method private static a(Lcom/google/android/exoplayer2/util/k;J)Landroid/util/Pair;
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/util/k;",
            "J)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/exoplayer2/extractor/a;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    .line 940
    const/16 v4, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 941
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v4

    .line 942
    invoke-static {v4}, Lcom/google/android/exoplayer2/extractor/c/a;->a(I)I

    move-result v4

    .line 944
    const/4 v5, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 945
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->l()J

    move-result-wide v8

    .line 948
    if-nez v4, :cond_0

    .line 949
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->l()J

    move-result-wide v6

    .line 950
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->l()J

    move-result-wide v4

    add-long v4, v4, p1

    move-wide v10, v4

    move-wide v4, v6

    .line 955
    :goto_0
    const-wide/32 v6, 0xf4240

    invoke-static/range {v4 .. v9}, Lcom/google/android/exoplayer2/util/s;->b(JJJ)J

    move-result-wide v12

    .line 958
    const/4 v6, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 960
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->h()I

    move-result v18

    .line 961
    move/from16 v0, v18

    new-array v0, v0, [I

    move-object/from16 v19, v0

    .line 962
    move/from16 v0, v18

    new-array v0, v0, [J

    move-object/from16 v20, v0

    .line 963
    move/from16 v0, v18

    new-array v0, v0, [J

    move-object/from16 v21, v0

    .line 964
    move/from16 v0, v18

    new-array v0, v0, [J

    move-object/from16 v22, v0

    .line 968
    const/4 v6, 0x0

    move-wide/from16 v16, v10

    move v10, v6

    move-wide v6, v4

    move-wide v4, v12

    :goto_1
    move/from16 v0, v18

    if-ge v10, v0, :cond_2

    .line 969
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v11

    .line 971
    const/high16 v14, -0x80000000

    and-int/2addr v14, v11

    .line 972
    if-eqz v14, :cond_1

    .line 973
    new-instance v4, Lcom/google/android/exoplayer2/ParserException;

    const-string v5, "Unhandled indirect reference"

    invoke-direct {v4, v5}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 952
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->v()J

    move-result-wide v6

    .line 953
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->v()J

    move-result-wide v4

    add-long v4, v4, p1

    move-wide v10, v4

    move-wide v4, v6

    goto :goto_0

    .line 975
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/util/k;->l()J

    move-result-wide v14

    .line 977
    const v23, 0x7fffffff

    and-int v11, v11, v23

    aput v11, v19, v10

    .line 978
    aput-wide v16, v20, v10

    .line 982
    aput-wide v4, v22, v10

    .line 983
    add-long v4, v6, v14

    .line 984
    const-wide/32 v6, 0xf4240

    invoke-static/range {v4 .. v9}, Lcom/google/android/exoplayer2/util/s;->b(JJJ)J

    move-result-wide v14

    .line 985
    aget-wide v6, v22, v10

    sub-long v6, v14, v6

    aput-wide v6, v21, v10

    .line 987
    const/4 v6, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 988
    aget v6, v19, v10

    int-to-long v6, v6

    add-long v16, v16, v6

    .line 968
    add-int/lit8 v6, v10, 0x1

    move v10, v6

    move-wide v6, v4

    move-wide v4, v14

    goto :goto_1

    .line 991
    :cond_2
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    new-instance v5, Lcom/google/android/exoplayer2/extractor/a;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    invoke-direct {v5, v0, v1, v2, v3}, Lcom/google/android/exoplayer2/extractor/a;-><init>([I[J[J[J)V

    invoke-static {v4, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    return-object v4
.end method

.method private static a(Ljava/util/List;)Lcom/google/android/exoplayer2/drm/a;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer2/extractor/c/a$b;",
            ">;)",
            "Lcom/google/android/exoplayer2/drm/a;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1255
    .line 1256
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    .line 1257
    const/4 v0, 0x0

    move v3, v0

    move-object v1, v2

    :goto_0
    if-ge v3, v4, :cond_3

    .line 1258
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/extractor/c/a$b;

    .line 1259
    iget v5, v0, Lcom/google/android/exoplayer2/extractor/c/a$b;->aP:I

    sget v6, Lcom/google/android/exoplayer2/extractor/c/a;->U:I

    if-ne v5, v6, :cond_1

    .line 1260
    if-nez v1, :cond_0

    .line 1261
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1263
    :cond_0
    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/c/a$b;->aQ:Lcom/google/android/exoplayer2/util/k;

    iget-object v0, v0, Lcom/google/android/exoplayer2/util/k;->a:[B

    .line 1264
    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/c/h;->a([B)Ljava/util/UUID;

    move-result-object v5

    .line 1265
    if-nez v5, :cond_2

    .line 1266
    const-string v0, "FragmentedMp4Extractor"

    const-string v5, "Skipped pssh atom (failed to extract uuid)"

    invoke-static {v0, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1257
    :cond_1
    :goto_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 1268
    :cond_2
    new-instance v6, Lcom/google/android/exoplayer2/drm/a$a;

    const-string v7, "video/mp4"

    invoke-direct {v6, v5, v2, v7, v0}, Lcom/google/android/exoplayer2/drm/a$a;-><init>(Ljava/util/UUID;Ljava/lang/String;Ljava/lang/String;[B)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1272
    :cond_3
    if-nez v1, :cond_4

    move-object v0, v2

    :goto_2
    return-object v0

    :cond_4
    new-instance v0, Lcom/google/android/exoplayer2/drm/a;

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/drm/a;-><init>(Ljava/util/List;)V

    goto :goto_2
.end method

.method private static a(Landroid/util/SparseArray;)Lcom/google/android/exoplayer2/extractor/c/e$b;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/exoplayer2/extractor/c/e$b;",
            ">;)",
            "Lcom/google/android/exoplayer2/extractor/c/e$b;"
        }
    .end annotation

    .prologue
    .line 1183
    const/4 v1, 0x0

    .line 1184
    const-wide v2, 0x7fffffffffffffffL

    .line 1186
    invoke-virtual {p0}, Landroid/util/SparseArray;->size()I

    move-result v7

    .line 1187
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v7, :cond_1

    .line 1188
    invoke-virtual {p0, v6}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/extractor/c/e$b;

    .line 1189
    iget v4, v0, Lcom/google/android/exoplayer2/extractor/c/e$b;->g:I

    iget-object v5, v0, Lcom/google/android/exoplayer2/extractor/c/e$b;->a:Lcom/google/android/exoplayer2/extractor/c/l;

    iget v5, v5, Lcom/google/android/exoplayer2/extractor/c/l;->e:I

    if-ne v4, v5, :cond_0

    move-wide v9, v2

    move-object v2, v1

    move-wide v0, v9

    .line 1187
    :goto_1
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    move-wide v9, v0

    move-object v1, v2

    move-wide v2, v9

    goto :goto_0

    .line 1192
    :cond_0
    iget-object v4, v0, Lcom/google/android/exoplayer2/extractor/c/e$b;->a:Lcom/google/android/exoplayer2/extractor/c/l;

    iget-object v4, v4, Lcom/google/android/exoplayer2/extractor/c/l;->g:[J

    iget v5, v0, Lcom/google/android/exoplayer2/extractor/c/e$b;->g:I

    aget-wide v4, v4, v5

    .line 1193
    cmp-long v8, v4, v2

    if-gez v8, :cond_2

    move-object v2, v0

    move-wide v0, v4

    .line 1195
    goto :goto_1

    .line 1199
    :cond_1
    return-object v1

    :cond_2
    move-wide v9, v2

    move-object v2, v1

    move-wide v0, v9

    goto :goto_1
.end method

.method private static a(Lcom/google/android/exoplayer2/util/k;Landroid/util/SparseArray;I)Lcom/google/android/exoplayer2/extractor/c/e$b;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/util/k;",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/exoplayer2/extractor/c/e$b;",
            ">;I)",
            "Lcom/google/android/exoplayer2/extractor/c/e$b;"
        }
    .end annotation

    .prologue
    .line 701
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 702
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v0

    .line 703
    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/c/a;->b(I)I

    move-result v5

    .line 704
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v0

    .line 705
    and-int/lit8 v1, p2, 0x10

    if-nez v1, :cond_0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/extractor/c/e$b;

    .line 706
    if-nez v0, :cond_1

    .line 707
    const/4 v0, 0x0

    .line 727
    :goto_1
    return-object v0

    .line 705
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 709
    :cond_1
    and-int/lit8 v1, v5, 0x1

    if-eqz v1, :cond_2

    .line 710
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->v()J

    move-result-wide v2

    .line 711
    iget-object v1, v0, Lcom/google/android/exoplayer2/extractor/c/e$b;->a:Lcom/google/android/exoplayer2/extractor/c/l;

    iput-wide v2, v1, Lcom/google/android/exoplayer2/extractor/c/l;->c:J

    .line 712
    iget-object v1, v0, Lcom/google/android/exoplayer2/extractor/c/e$b;->a:Lcom/google/android/exoplayer2/extractor/c/l;

    iput-wide v2, v1, Lcom/google/android/exoplayer2/extractor/c/l;->d:J

    .line 715
    :cond_2
    iget-object v6, v0, Lcom/google/android/exoplayer2/extractor/c/e$b;->d:Lcom/google/android/exoplayer2/extractor/c/c;

    .line 716
    and-int/lit8 v1, v5, 0x2

    if-eqz v1, :cond_3

    .line 718
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v4, v1

    .line 719
    :goto_2
    and-int/lit8 v1, v5, 0x8

    if-eqz v1, :cond_4

    .line 720
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v1

    move v3, v1

    .line 721
    :goto_3
    and-int/lit8 v1, v5, 0x10

    if-eqz v1, :cond_5

    .line 722
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v1

    move v2, v1

    .line 723
    :goto_4
    and-int/lit8 v1, v5, 0x20

    if-eqz v1, :cond_6

    .line 724
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v1

    .line 725
    :goto_5
    iget-object v5, v0, Lcom/google/android/exoplayer2/extractor/c/e$b;->a:Lcom/google/android/exoplayer2/extractor/c/l;

    new-instance v6, Lcom/google/android/exoplayer2/extractor/c/c;

    invoke-direct {v6, v4, v3, v2, v1}, Lcom/google/android/exoplayer2/extractor/c/c;-><init>(IIII)V

    iput-object v6, v5, Lcom/google/android/exoplayer2/extractor/c/l;->a:Lcom/google/android/exoplayer2/extractor/c/c;

    goto :goto_1

    .line 718
    :cond_3
    iget v1, v6, Lcom/google/android/exoplayer2/extractor/c/c;->a:I

    move v4, v1

    goto :goto_2

    .line 720
    :cond_4
    iget v1, v6, Lcom/google/android/exoplayer2/extractor/c/c;->b:I

    move v3, v1

    goto :goto_3

    .line 722
    :cond_5
    iget v1, v6, Lcom/google/android/exoplayer2/extractor/c/c;->c:I

    move v2, v1

    goto :goto_4

    .line 724
    :cond_6
    iget v1, v6, Lcom/google/android/exoplayer2/extractor/c/c;->d:I

    goto :goto_5
.end method

.method private a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 270
    iput v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->q:I

    .line 271
    iput v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->t:I

    .line 272
    return-void
.end method

.method private a(J)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    .line 363
    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->o:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->o:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/extractor/c/a$a;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/extractor/c/a$a;->aQ:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    .line 364
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->o:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/extractor/c/a$a;

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/extractor/c/e;->a(Lcom/google/android/exoplayer2/extractor/c/a$a;)V

    goto :goto_0

    .line 366
    :cond_0
    invoke-direct {p0}, Lcom/google/android/exoplayer2/extractor/c/e;->a()V

    .line 367
    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/extractor/c/a$a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    .line 383
    iget v0, p1, Lcom/google/android/exoplayer2/extractor/c/a$a;->aP:I

    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->B:I

    if-ne v0, v1, :cond_1

    .line 384
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/c/e;->b(Lcom/google/android/exoplayer2/extractor/c/a$a;)V

    .line 390
    :cond_0
    :goto_0
    return-void

    .line 385
    :cond_1
    iget v0, p1, Lcom/google/android/exoplayer2/extractor/c/a$a;->aP:I

    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->K:I

    if-ne v0, v1, :cond_2

    .line 386
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/c/e;->c(Lcom/google/android/exoplayer2/extractor/c/a$a;)V

    goto :goto_0

    .line 387
    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->o:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 388
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->o:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/extractor/c/a$a;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/extractor/c/a$a;->a(Lcom/google/android/exoplayer2/extractor/c/a$a;)V

    goto :goto_0
.end method

.method private static a(Lcom/google/android/exoplayer2/extractor/c/a$a;Landroid/util/SparseArray;I[B)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/extractor/c/a$a;",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/exoplayer2/extractor/c/e$b;",
            ">;I[B)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    .line 532
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/a$a;->aS:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 533
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 534
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/a$a;->aS:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/extractor/c/a$a;

    .line 536
    iget v3, v0, Lcom/google/android/exoplayer2/extractor/c/a$a;->aP:I

    sget v4, Lcom/google/android/exoplayer2/extractor/c/a;->L:I

    if-ne v3, v4, :cond_0

    .line 537
    invoke-static {v0, p1, p2, p3}, Lcom/google/android/exoplayer2/extractor/c/e;->b(Lcom/google/android/exoplayer2/extractor/c/a$a;Landroid/util/SparseArray;I[B)V

    .line 533
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 540
    :cond_1
    return-void
.end method

.method private static a(Lcom/google/android/exoplayer2/extractor/c/a$a;Lcom/google/android/exoplayer2/extractor/c/e$b;JI)V
    .locals 12

    .prologue
    const/4 v3, 0x0

    .line 600
    .line 602
    iget-object v9, p0, Lcom/google/android/exoplayer2/extractor/c/a$a;->aR:Ljava/util/List;

    .line 603
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v10

    move v4, v3

    move v1, v3

    move v2, v3

    .line 604
    :goto_0
    if-ge v4, v10, :cond_0

    .line 605
    invoke-interface {v9, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/extractor/c/a$b;

    .line 606
    iget v5, v0, Lcom/google/android/exoplayer2/extractor/c/a$b;->aP:I

    sget v6, Lcom/google/android/exoplayer2/extractor/c/a;->z:I

    if-ne v5, v6, :cond_3

    .line 607
    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/c/a$b;->aQ:Lcom/google/android/exoplayer2/util/k;

    .line 608
    const/16 v5, 0xc

    invoke-virtual {v0, v5}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 609
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v0

    .line 610
    if-lez v0, :cond_3

    .line 611
    add-int/2addr v0, v1

    .line 612
    add-int/lit8 v1, v2, 0x1

    .line 604
    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 616
    :cond_0
    iput v3, p1, Lcom/google/android/exoplayer2/extractor/c/e$b;->g:I

    .line 617
    iput v3, p1, Lcom/google/android/exoplayer2/extractor/c/e$b;->f:I

    .line 618
    iput v3, p1, Lcom/google/android/exoplayer2/extractor/c/e$b;->e:I

    .line 619
    iget-object v0, p1, Lcom/google/android/exoplayer2/extractor/c/e$b;->a:Lcom/google/android/exoplayer2/extractor/c/l;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/exoplayer2/extractor/c/l;->a(II)V

    move v8, v3

    move v6, v3

    move v1, v3

    .line 623
    :goto_2
    if-ge v8, v10, :cond_2

    .line 624
    invoke-interface {v9, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/extractor/c/a$b;

    .line 625
    iget v2, v0, Lcom/google/android/exoplayer2/extractor/c/a$b;->aP:I

    sget v3, Lcom/google/android/exoplayer2/extractor/c/a;->z:I

    if-ne v2, v3, :cond_1

    .line 626
    add-int/lit8 v7, v1, 0x1

    iget-object v5, v0, Lcom/google/android/exoplayer2/extractor/c/a$b;->aQ:Lcom/google/android/exoplayer2/util/k;

    move-object v0, p1

    move-wide v2, p2

    move/from16 v4, p4

    invoke-static/range {v0 .. v6}, Lcom/google/android/exoplayer2/extractor/c/e;->a(Lcom/google/android/exoplayer2/extractor/c/e$b;IJILcom/google/android/exoplayer2/util/k;I)I

    move-result v6

    move v1, v7

    .line 623
    :cond_1
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_2

    .line 630
    :cond_2
    return-void

    :cond_3
    move v0, v1

    move v1, v2

    goto :goto_1
.end method

.method private a(Lcom/google/android/exoplayer2/extractor/c/a$b;J)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    .line 370
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->o:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 371
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->o:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/extractor/c/a$a;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/extractor/c/a$a;->a(Lcom/google/android/exoplayer2/extractor/c/a$b;)V

    .line 380
    :cond_0
    :goto_0
    return-void

    .line 372
    :cond_1
    iget v0, p1, Lcom/google/android/exoplayer2/extractor/c/a$b;->aP:I

    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->A:I

    if-ne v0, v1, :cond_2

    .line 373
    iget-object v0, p1, Lcom/google/android/exoplayer2/extractor/c/a$b;->aQ:Lcom/google/android/exoplayer2/util/k;

    invoke-static {v0, p2, p3}, Lcom/google/android/exoplayer2/extractor/c/e;->a(Lcom/google/android/exoplayer2/util/k;J)Landroid/util/Pair;

    move-result-object v1

    .line 374
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/exoplayer2/extractor/c/e;->y:J

    .line 375
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/c/e;->E:Lcom/google/android/exoplayer2/extractor/g;

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/extractor/l;

    invoke-interface {v2, v0}, Lcom/google/android/exoplayer2/extractor/g;->a(Lcom/google/android/exoplayer2/extractor/l;)V

    .line 376
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->H:Z

    goto :goto_0

    .line 377
    :cond_2
    iget v0, p1, Lcom/google/android/exoplayer2/extractor/c/a$b;->aP:I

    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->aG:I

    if-ne v0, v1, :cond_0

    .line 378
    iget-object v0, p1, Lcom/google/android/exoplayer2/extractor/c/a$b;->aQ:Lcom/google/android/exoplayer2/util/k;

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/extractor/c/e;->a(Lcom/google/android/exoplayer2/util/k;)V

    goto :goto_0
.end method

.method private static a(Lcom/google/android/exoplayer2/extractor/c/k;Lcom/google/android/exoplayer2/util/k;Lcom/google/android/exoplayer2/extractor/c/l;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 634
    iget v5, p0, Lcom/google/android/exoplayer2/extractor/c/k;->d:I

    .line 635
    invoke-virtual {p1, v3}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 636
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v0

    .line 637
    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/c/a;->b(I)I

    move-result v0

    .line 638
    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 639
    invoke-virtual {p1, v3}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 641
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->g()I

    move-result v0

    .line 643
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v6

    .line 644
    iget v3, p2, Lcom/google/android/exoplayer2/extractor/c/l;->f:I

    if-eq v6, v3, :cond_1

    .line 645
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Length mismatch: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Lcom/google/android/exoplayer2/extractor/c/l;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 649
    :cond_1
    if-nez v0, :cond_3

    .line 650
    iget-object v7, p2, Lcom/google/android/exoplayer2/extractor/c/l;->n:[Z

    move v3, v2

    move v0, v2

    .line 651
    :goto_0
    if-ge v3, v6, :cond_4

    .line 652
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->g()I

    move-result v8

    .line 653
    add-int v4, v0, v8

    .line 654
    if-le v8, v5, :cond_2

    move v0, v1

    :goto_1
    aput-boolean v0, v7, v3

    .line 651
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v4

    goto :goto_0

    :cond_2
    move v0, v2

    .line 654
    goto :goto_1

    .line 657
    :cond_3
    if-le v0, v5, :cond_5

    .line 658
    :goto_2
    mul-int/2addr v0, v6

    add-int/2addr v0, v2

    .line 659
    iget-object v3, p2, Lcom/google/android/exoplayer2/extractor/c/l;->n:[Z

    invoke-static {v3, v2, v6, v1}, Ljava/util/Arrays;->fill([ZIIZ)V

    .line 661
    :cond_4
    invoke-virtual {p2, v0}, Lcom/google/android/exoplayer2/extractor/c/l;->a(I)V

    .line 662
    return-void

    :cond_5
    move v1, v2

    .line 657
    goto :goto_2
.end method

.method private a(Lcom/google/android/exoplayer2/util/k;)V
    .locals 8

    .prologue
    const/16 v6, 0xc

    .line 477
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->F:Lcom/google/android/exoplayer2/extractor/m;

    if-nez v0, :cond_0

    .line 503
    :goto_0
    return-void

    .line 481
    :cond_0
    invoke-virtual {p1, v6}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 482
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->x()Ljava/lang/String;

    .line 483
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->x()Ljava/lang/String;

    .line 484
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->l()J

    move-result-wide v4

    .line 486
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->l()J

    move-result-wide v0

    const-wide/32 v2, 0xf4240

    invoke-static/range {v0 .. v5}, Lcom/google/android/exoplayer2/util/s;->b(JJJ)J

    move-result-wide v2

    .line 488
    invoke-virtual {p1, v6}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 489
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->b()I

    move-result v5

    .line 490
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->F:Lcom/google/android/exoplayer2/extractor/m;

    invoke-interface {v0, p1, v5}, Lcom/google/android/exoplayer2/extractor/m;->a(Lcom/google/android/exoplayer2/util/k;I)V

    .line 492
    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->y:J

    const-wide v6, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, v0, v6

    if-eqz v0, :cond_1

    .line 494
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/c/e;->F:Lcom/google/android/exoplayer2/extractor/m;

    iget-wide v6, p0, Lcom/google/android/exoplayer2/extractor/c/e;->y:J

    add-long/2addr v2, v6

    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-interface/range {v1 .. v7}, Lcom/google/android/exoplayer2/extractor/m;->a(JIIILcom/google/android/exoplayer2/extractor/m$a;)V

    goto :goto_0

    .line 499
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->p:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/exoplayer2/extractor/c/e$a;

    invoke-direct {v1, v2, v3, v5}, Lcom/google/android/exoplayer2/extractor/c/e$a;-><init>(JI)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 501
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->w:I

    add-int/2addr v0, v5

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->w:I

    goto :goto_0
.end method

.method private static a(Lcom/google/android/exoplayer2/util/k;ILcom/google/android/exoplayer2/extractor/c/l;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 855
    add-int/lit8 v0, p1, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 856
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v0

    .line 857
    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/c/a;->b(I)I

    move-result v0

    .line 859
    and-int/lit8 v2, v0, 0x1

    if-eqz v2, :cond_0

    .line 861
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Overriding TrackEncryptionBox parameters is unsupported."

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 864
    :cond_0
    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 865
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v2

    .line 866
    iget v3, p2, Lcom/google/android/exoplayer2/extractor/c/l;->f:I

    if-eq v2, v3, :cond_2

    .line 867
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Length mismatch: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Lcom/google/android/exoplayer2/extractor/c/l;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v1

    .line 864
    goto :goto_0

    .line 870
    :cond_2
    iget-object v3, p2, Lcom/google/android/exoplayer2/extractor/c/l;->n:[Z

    invoke-static {v3, v1, v2, v0}, Ljava/util/Arrays;->fill([ZIIZ)V

    .line 871
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->b()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/android/exoplayer2/extractor/c/l;->a(I)V

    .line 872
    invoke-virtual {p2, p0}, Lcom/google/android/exoplayer2/extractor/c/l;->a(Lcom/google/android/exoplayer2/util/k;)V

    .line 873
    return-void
.end method

.method private static a(Lcom/google/android/exoplayer2/util/k;Lcom/google/android/exoplayer2/extractor/c/l;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x1

    .line 671
    invoke-virtual {p0, v3}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 672
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v0

    .line 673
    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/c/a;->b(I)I

    move-result v1

    .line 674
    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_0

    .line 675
    invoke-virtual {p0, v3}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 678
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v1

    .line 679
    if-eq v1, v2, :cond_1

    .line 681
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected saio entry count: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 684
    :cond_1
    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/c/a;->a(I)I

    move-result v0

    .line 685
    iget-wide v2, p1, Lcom/google/android/exoplayer2/extractor/c/l;->d:J

    if-nez v0, :cond_2

    .line 686
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->l()J

    move-result-wide v0

    :goto_0
    add-long/2addr v0, v2

    iput-wide v0, p1, Lcom/google/android/exoplayer2/extractor/c/l;->d:J

    .line 687
    return-void

    .line 686
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->v()J

    move-result-wide v0

    goto :goto_0
.end method

.method private static a(Lcom/google/android/exoplayer2/util/k;Lcom/google/android/exoplayer2/extractor/c/l;[B)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    const/16 v1, 0x10

    .line 835
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 836
    const/4 v0, 0x0

    invoke-virtual {p0, p2, v0, v1}, Lcom/google/android/exoplayer2/util/k;->a([BII)V

    .line 839
    sget-object v0, Lcom/google/android/exoplayer2/extractor/c/e;->c:[B

    invoke-static {p2, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_0

    .line 847
    :goto_0
    return-void

    .line 846
    :cond_0
    invoke-static {p0, v1, p1}, Lcom/google/android/exoplayer2/extractor/c/e;->a(Lcom/google/android/exoplayer2/util/k;ILcom/google/android/exoplayer2/extractor/c/l;)V

    goto :goto_0
.end method

.method private static a(Lcom/google/android/exoplayer2/util/k;Lcom/google/android/exoplayer2/util/k;Ljava/lang/String;Lcom/google/android/exoplayer2/extractor/c/l;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 877
    invoke-virtual {p0, v6}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 878
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v1

    .line 879
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v3

    sget v4, Lcom/google/android/exoplayer2/extractor/c/e;->b:I

    if-eq v3, v4, :cond_1

    .line 928
    :cond_0
    :goto_0
    return-void

    .line 883
    :cond_1
    invoke-static {v1}, Lcom/google/android/exoplayer2/extractor/c/a;->a(I)I

    move-result v1

    if-ne v1, v0, :cond_2

    .line 884
    invoke-virtual {p0, v5}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 886
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v1

    if-eq v1, v0, :cond_3

    .line 887
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Entry count in sbgp != 1 (unsupported)."

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 890
    :cond_3
    invoke-virtual {p1, v6}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 891
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v1

    .line 892
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v3

    sget v4, Lcom/google/android/exoplayer2/extractor/c/e;->b:I

    if-ne v3, v4, :cond_0

    .line 896
    invoke-static {v1}, Lcom/google/android/exoplayer2/extractor/c/a;->a(I)I

    move-result v1

    .line 897
    if-ne v1, v0, :cond_4

    .line 898
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->l()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-nez v1, :cond_5

    .line 899
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Variable length description in sgpd found (unsupported)"

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 901
    :cond_4
    const/4 v3, 0x2

    if-lt v1, v3, :cond_5

    .line 902
    invoke-virtual {p1, v5}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 904
    :cond_5
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->l()J

    move-result-wide v4

    const-wide/16 v6, 0x1

    cmp-long v1, v4, v6

    if-eqz v1, :cond_6

    .line 905
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Entry count in sgpd != 1 (unsupported)."

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 908
    :cond_6
    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 909
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->g()I

    move-result v1

    .line 910
    and-int/lit16 v3, v1, 0xf0

    shr-int/lit8 v5, v3, 0x4

    .line 911
    and-int/lit8 v6, v1, 0xf

    .line 912
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->g()I

    move-result v1

    if-ne v1, v0, :cond_8

    move v1, v0

    .line 913
    :goto_1
    if-eqz v1, :cond_0

    .line 916
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->g()I

    move-result v3

    .line 917
    const/16 v4, 0x10

    new-array v4, v4, [B

    .line 918
    array-length v7, v4

    invoke-virtual {p1, v4, v2, v7}, Lcom/google/android/exoplayer2/util/k;->a([BII)V

    .line 919
    const/4 v7, 0x0

    .line 920
    if-eqz v1, :cond_7

    if-nez v3, :cond_7

    .line 921
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->g()I

    move-result v8

    .line 922
    new-array v7, v8, [B

    .line 923
    invoke-virtual {p1, v7, v2, v8}, Lcom/google/android/exoplayer2/util/k;->a([BII)V

    .line 925
    :cond_7
    iput-boolean v0, p3, Lcom/google/android/exoplayer2/extractor/c/l;->m:Z

    .line 926
    new-instance v0, Lcom/google/android/exoplayer2/extractor/c/k;

    move-object v2, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/exoplayer2/extractor/c/k;-><init>(ZLjava/lang/String;I[BII[B)V

    iput-object v0, p3, Lcom/google/android/exoplayer2/extractor/c/l;->o:Lcom/google/android/exoplayer2/extractor/c/k;

    goto/16 :goto_0

    :cond_8
    move v1, v2

    .line 912
    goto :goto_1
.end method

.method private static a(I)Z
    .locals 1

    .prologue
    .line 1277
    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->S:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->R:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->C:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->A:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->T:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->w:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->x:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->O:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->y:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->z:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->U:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->ac:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->ad:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->ah:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->ag:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->ae:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->af:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->Q:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->N:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->aG:I

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lcom/google/android/exoplayer2/util/k;)Landroid/util/Pair;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/util/k;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/exoplayer2/extractor/c/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 509
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 510
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v0

    .line 511
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 512
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v2

    .line 513
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v3

    .line 514
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v4

    .line 516
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v5, Lcom/google/android/exoplayer2/extractor/c/c;

    invoke-direct {v5, v1, v2, v3, v4}, Lcom/google/android/exoplayer2/extractor/c/c;-><init>(IIII)V

    invoke-static {v0, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 459
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->d:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->F:Lcom/google/android/exoplayer2/extractor/m;

    if-nez v0, :cond_0

    .line 460
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->E:Lcom/google/android/exoplayer2/extractor/g;

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/c/e;->f:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    const/4 v2, 0x4

    invoke-interface {v0, v1, v2}, Lcom/google/android/exoplayer2/extractor/g;->a(II)Lcom/google/android/exoplayer2/extractor/m;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->F:Lcom/google/android/exoplayer2/extractor/m;

    .line 461
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->F:Lcom/google/android/exoplayer2/extractor/m;

    const-string v1, "application/x-emsg"

    const-wide v2, 0x7fffffffffffffffL

    invoke-static {v4, v1, v2, v3}, Lcom/google/android/exoplayer2/j;->a(Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/exoplayer2/j;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/extractor/m;->a(Lcom/google/android/exoplayer2/j;)V

    .line 464
    :cond_0
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->d:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->G:[Lcom/google/android/exoplayer2/extractor/m;

    if-nez v0, :cond_1

    .line 465
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->E:Lcom/google/android/exoplayer2/extractor/g;

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/c/e;->f:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lcom/google/android/exoplayer2/extractor/g;->a(II)Lcom/google/android/exoplayer2/extractor/m;

    move-result-object v0

    .line 467
    const-string v1, "application/cea-608"

    invoke-static {v4, v1, v5, v4}, Lcom/google/android/exoplayer2/j;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/google/android/exoplayer2/j;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/extractor/m;->a(Lcom/google/android/exoplayer2/j;)V

    .line 469
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/exoplayer2/extractor/m;

    aput-object v0, v1, v5

    iput-object v1, p0, Lcom/google/android/exoplayer2/extractor/c/e;->G:[Lcom/google/android/exoplayer2/extractor/m;

    .line 471
    :cond_1
    return-void
.end method

.method private b(Lcom/google/android/exoplayer2/extractor/c/a$a;)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 393
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->e:Lcom/google/android/exoplayer2/extractor/c/j;

    if-nez v0, :cond_1

    move v0, v6

    :goto_0
    const-string v1, "Unexpected moov box."

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/util/a;->b(ZLjava/lang/Object;)V

    .line 395
    iget-object v0, p1, Lcom/google/android/exoplayer2/extractor/c/a$a;->aR:Ljava/util/List;

    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/c/e;->a(Ljava/util/List;)Lcom/google/android/exoplayer2/drm/a;

    move-result-object v4

    .line 398
    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->M:I

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/extractor/c/a$a;->e(I)Lcom/google/android/exoplayer2/extractor/c/a$a;

    move-result-object v7

    .line 399
    new-instance v8, Landroid/util/SparseArray;

    invoke-direct {v8}, Landroid/util/SparseArray;-><init>()V

    .line 400
    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    .line 401
    iget-object v0, v7, Lcom/google/android/exoplayer2/extractor/c/a$a;->aR:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v9

    move v1, v5

    .line 402
    :goto_1
    if-ge v1, v9, :cond_3

    .line 403
    iget-object v0, v7, Lcom/google/android/exoplayer2/extractor/c/a$a;->aR:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/extractor/c/a$b;

    .line 404
    iget v10, v0, Lcom/google/android/exoplayer2/extractor/c/a$b;->aP:I

    sget v11, Lcom/google/android/exoplayer2/extractor/c/a;->y:I

    if-ne v10, v11, :cond_2

    .line 405
    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/c/a$b;->aQ:Lcom/google/android/exoplayer2/util/k;

    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/c/e;->b(Lcom/google/android/exoplayer2/util/k;)Landroid/util/Pair;

    move-result-object v10

    .line 406
    iget-object v0, v10, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v10, v10, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v8, v0, v10}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 402
    :cond_0
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v0, v5

    .line 393
    goto :goto_0

    .line 407
    :cond_2
    iget v10, v0, Lcom/google/android/exoplayer2/extractor/c/a$b;->aP:I

    sget v11, Lcom/google/android/exoplayer2/extractor/c/a;->N:I

    if-ne v10, v11, :cond_0

    .line 408
    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/c/a$b;->aQ:Lcom/google/android/exoplayer2/util/k;

    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/c/e;->c(Lcom/google/android/exoplayer2/util/k;)J

    move-result-wide v2

    goto :goto_2

    .line 413
    :cond_3
    new-instance v9, Landroid/util/SparseArray;

    invoke-direct {v9}, Landroid/util/SparseArray;-><init>()V

    .line 414
    iget-object v0, p1, Lcom/google/android/exoplayer2/extractor/c/a$a;->aS:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v10

    move v7, v5

    .line 415
    :goto_3
    if-ge v7, v10, :cond_5

    .line 416
    iget-object v0, p1, Lcom/google/android/exoplayer2/extractor/c/a$a;->aS:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/extractor/c/a$a;

    .line 417
    iget v1, v0, Lcom/google/android/exoplayer2/extractor/c/a$a;->aP:I

    sget v11, Lcom/google/android/exoplayer2/extractor/c/a;->D:I

    if-ne v1, v11, :cond_4

    .line 418
    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->C:I

    invoke-virtual {p1, v1}, Lcom/google/android/exoplayer2/extractor/c/a$a;->d(I)Lcom/google/android/exoplayer2/extractor/c/a$b;

    move-result-object v1

    invoke-static/range {v0 .. v5}, Lcom/google/android/exoplayer2/extractor/c/b;->a(Lcom/google/android/exoplayer2/extractor/c/a$a;Lcom/google/android/exoplayer2/extractor/c/a$b;JLcom/google/android/exoplayer2/drm/a;Z)Lcom/google/android/exoplayer2/extractor/c/j;

    move-result-object v0

    .line 420
    if-eqz v0, :cond_4

    .line 421
    iget v1, v0, Lcom/google/android/exoplayer2/extractor/c/j;->a:I

    invoke-virtual {v9, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 415
    :cond_4
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_3

    .line 426
    :cond_5
    invoke-virtual {v9}, Landroid/util/SparseArray;->size()I

    move-result v3

    .line 427
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_8

    .line 429
    :goto_4
    if-ge v5, v3, :cond_6

    .line 430
    invoke-virtual {v9, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/extractor/c/j;

    .line 431
    new-instance v2, Lcom/google/android/exoplayer2/extractor/c/e$b;

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/c/e;->E:Lcom/google/android/exoplayer2/extractor/g;

    iget v4, v0, Lcom/google/android/exoplayer2/extractor/c/j;->b:I

    invoke-interface {v1, v5, v4}, Lcom/google/android/exoplayer2/extractor/g;->a(II)Lcom/google/android/exoplayer2/extractor/m;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/google/android/exoplayer2/extractor/c/e$b;-><init>(Lcom/google/android/exoplayer2/extractor/m;)V

    .line 432
    iget v1, v0, Lcom/google/android/exoplayer2/extractor/c/j;->a:I

    invoke-virtual {v8, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/extractor/c/c;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/exoplayer2/extractor/c/e$b;->a(Lcom/google/android/exoplayer2/extractor/c/j;Lcom/google/android/exoplayer2/extractor/c/c;)V

    .line 433
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/c/e;->f:Landroid/util/SparseArray;

    iget v4, v0, Lcom/google/android/exoplayer2/extractor/c/j;->a:I

    invoke-virtual {v1, v4, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 434
    iget-wide v6, p0, Lcom/google/android/exoplayer2/extractor/c/e;->x:J

    iget-wide v0, v0, Lcom/google/android/exoplayer2/extractor/c/j;->e:J

    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->x:J

    .line 429
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 436
    :cond_6
    invoke-direct {p0}, Lcom/google/android/exoplayer2/extractor/c/e;->b()V

    .line 437
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->E:Lcom/google/android/exoplayer2/extractor/g;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/extractor/g;->a()V

    .line 445
    :cond_7
    return-void

    .line 439
    :cond_8
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ne v0, v3, :cond_9

    :goto_5
    invoke-static {v6}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 440
    :goto_6
    if-ge v5, v3, :cond_7

    .line 441
    invoke-virtual {v9, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/extractor/c/j;

    .line 442
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/c/e;->f:Landroid/util/SparseArray;

    iget v2, v0, Lcom/google/android/exoplayer2/extractor/c/j;->a:I

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/extractor/c/e$b;

    iget v2, v0, Lcom/google/android/exoplayer2/extractor/c/j;->a:I

    invoke-virtual {v8, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/extractor/c/c;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/exoplayer2/extractor/c/e$b;->a(Lcom/google/android/exoplayer2/extractor/c/j;Lcom/google/android/exoplayer2/extractor/c/c;)V

    .line 440
    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    :cond_9
    move v6, v5

    .line 439
    goto :goto_5
.end method

.method private static b(Lcom/google/android/exoplayer2/extractor/c/a$a;Landroid/util/SparseArray;I[B)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/extractor/c/a$a;",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/exoplayer2/extractor/c/e$b;",
            ">;I[B)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    .line 547
    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->x:I

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/extractor/c/a$a;->d(I)Lcom/google/android/exoplayer2/extractor/c/a$b;

    move-result-object v0

    .line 548
    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/c/a$b;->aQ:Lcom/google/android/exoplayer2/util/k;

    invoke-static {v0, p1, p2}, Lcom/google/android/exoplayer2/extractor/c/e;->a(Lcom/google/android/exoplayer2/util/k;Landroid/util/SparseArray;I)Lcom/google/android/exoplayer2/extractor/c/e$b;

    move-result-object v2

    .line 549
    if-nez v2, :cond_1

    .line 596
    :cond_0
    return-void

    .line 553
    :cond_1
    iget-object v3, v2, Lcom/google/android/exoplayer2/extractor/c/e$b;->a:Lcom/google/android/exoplayer2/extractor/c/l;

    .line 554
    iget-wide v0, v3, Lcom/google/android/exoplayer2/extractor/c/l;->s:J

    .line 555
    invoke-virtual {v2}, Lcom/google/android/exoplayer2/extractor/c/e$b;->a()V

    .line 557
    sget v4, Lcom/google/android/exoplayer2/extractor/c/a;->w:I

    invoke-virtual {p0, v4}, Lcom/google/android/exoplayer2/extractor/c/a$a;->d(I)Lcom/google/android/exoplayer2/extractor/c/a$b;

    move-result-object v4

    .line 558
    if-eqz v4, :cond_2

    and-int/lit8 v4, p2, 0x2

    if-nez v4, :cond_2

    .line 559
    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->w:I

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/extractor/c/a$a;->d(I)Lcom/google/android/exoplayer2/extractor/c/a$b;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/c/a$b;->aQ:Lcom/google/android/exoplayer2/util/k;

    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/c/e;->d(Lcom/google/android/exoplayer2/util/k;)J

    move-result-wide v0

    .line 562
    :cond_2
    invoke-static {p0, v2, v0, v1, p2}, Lcom/google/android/exoplayer2/extractor/c/e;->a(Lcom/google/android/exoplayer2/extractor/c/a$a;Lcom/google/android/exoplayer2/extractor/c/e$b;JI)V

    .line 564
    iget-object v0, v2, Lcom/google/android/exoplayer2/extractor/c/e$b;->c:Lcom/google/android/exoplayer2/extractor/c/j;

    iget-object v1, v3, Lcom/google/android/exoplayer2/extractor/c/l;->a:Lcom/google/android/exoplayer2/extractor/c/c;

    iget v1, v1, Lcom/google/android/exoplayer2/extractor/c/c;->a:I

    .line 565
    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/extractor/c/j;->a(I)Lcom/google/android/exoplayer2/extractor/c/k;

    move-result-object v0

    .line 567
    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->ac:I

    invoke-virtual {p0, v1}, Lcom/google/android/exoplayer2/extractor/c/a$a;->d(I)Lcom/google/android/exoplayer2/extractor/c/a$b;

    move-result-object v1

    .line 568
    if-eqz v1, :cond_3

    .line 569
    iget-object v1, v1, Lcom/google/android/exoplayer2/extractor/c/a$b;->aQ:Lcom/google/android/exoplayer2/util/k;

    invoke-static {v0, v1, v3}, Lcom/google/android/exoplayer2/extractor/c/e;->a(Lcom/google/android/exoplayer2/extractor/c/k;Lcom/google/android/exoplayer2/util/k;Lcom/google/android/exoplayer2/extractor/c/l;)V

    .line 572
    :cond_3
    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->ad:I

    invoke-virtual {p0, v1}, Lcom/google/android/exoplayer2/extractor/c/a$a;->d(I)Lcom/google/android/exoplayer2/extractor/c/a$b;

    move-result-object v1

    .line 573
    if-eqz v1, :cond_4

    .line 574
    iget-object v1, v1, Lcom/google/android/exoplayer2/extractor/c/a$b;->aQ:Lcom/google/android/exoplayer2/util/k;

    invoke-static {v1, v3}, Lcom/google/android/exoplayer2/extractor/c/e;->a(Lcom/google/android/exoplayer2/util/k;Lcom/google/android/exoplayer2/extractor/c/l;)V

    .line 577
    :cond_4
    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->ah:I

    invoke-virtual {p0, v1}, Lcom/google/android/exoplayer2/extractor/c/a$a;->d(I)Lcom/google/android/exoplayer2/extractor/c/a$b;

    move-result-object v1

    .line 578
    if-eqz v1, :cond_5

    .line 579
    iget-object v1, v1, Lcom/google/android/exoplayer2/extractor/c/a$b;->aQ:Lcom/google/android/exoplayer2/util/k;

    invoke-static {v1, v3}, Lcom/google/android/exoplayer2/extractor/c/e;->b(Lcom/google/android/exoplayer2/util/k;Lcom/google/android/exoplayer2/extractor/c/l;)V

    .line 582
    :cond_5
    sget v1, Lcom/google/android/exoplayer2/extractor/c/a;->ae:I

    invoke-virtual {p0, v1}, Lcom/google/android/exoplayer2/extractor/c/a$a;->d(I)Lcom/google/android/exoplayer2/extractor/c/a$b;

    move-result-object v1

    .line 583
    sget v2, Lcom/google/android/exoplayer2/extractor/c/a;->af:I

    invoke-virtual {p0, v2}, Lcom/google/android/exoplayer2/extractor/c/a$a;->d(I)Lcom/google/android/exoplayer2/extractor/c/a$b;

    move-result-object v2

    .line 584
    if-eqz v1, :cond_6

    if-eqz v2, :cond_6

    .line 585
    iget-object v1, v1, Lcom/google/android/exoplayer2/extractor/c/a$b;->aQ:Lcom/google/android/exoplayer2/util/k;

    iget-object v2, v2, Lcom/google/android/exoplayer2/extractor/c/a$b;->aQ:Lcom/google/android/exoplayer2/util/k;

    if-eqz v0, :cond_8

    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/c/k;->b:Ljava/lang/String;

    :goto_0
    invoke-static {v1, v2, v0, v3}, Lcom/google/android/exoplayer2/extractor/c/e;->a(Lcom/google/android/exoplayer2/util/k;Lcom/google/android/exoplayer2/util/k;Ljava/lang/String;Lcom/google/android/exoplayer2/extractor/c/l;)V

    .line 589
    :cond_6
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/a$a;->aR:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 590
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_0

    .line 591
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/a$a;->aR:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/extractor/c/a$b;

    .line 592
    iget v4, v0, Lcom/google/android/exoplayer2/extractor/c/a$b;->aP:I

    sget v5, Lcom/google/android/exoplayer2/extractor/c/a;->ag:I

    if-ne v4, v5, :cond_7

    .line 593
    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/c/a$b;->aQ:Lcom/google/android/exoplayer2/util/k;

    invoke-static {v0, v3, p3}, Lcom/google/android/exoplayer2/extractor/c/e;->a(Lcom/google/android/exoplayer2/util/k;Lcom/google/android/exoplayer2/extractor/c/l;[B)V

    .line 590
    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 585
    :cond_8
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lcom/google/android/exoplayer2/util/k;Lcom/google/android/exoplayer2/extractor/c/l;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    .line 850
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/google/android/exoplayer2/extractor/c/e;->a(Lcom/google/android/exoplayer2/util/k;ILcom/google/android/exoplayer2/extractor/c/l;)V

    .line 851
    return-void
.end method

.method private static b(I)Z
    .locals 1

    .prologue
    .line 1288
    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->B:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->D:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->E:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->F:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->G:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->K:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->L:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->M:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/exoplayer2/extractor/c/a;->P:I

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/google/android/exoplayer2/extractor/f;)Z
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const-wide/32 v10, 0x7fffffff

    const/4 v9, 0x0

    const/16 v8, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 275
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->t:I

    if-nez v0, :cond_1

    .line 277
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->m:Lcom/google/android/exoplayer2/util/k;

    iget-object v0, v0, Lcom/google/android/exoplayer2/util/k;->a:[B

    invoke-interface {p1, v0, v1, v8, v2}, Lcom/google/android/exoplayer2/extractor/f;->a([BIIZ)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 348
    :goto_0
    return v0

    .line 280
    :cond_0
    iput v8, p0, Lcom/google/android/exoplayer2/extractor/c/e;->t:I

    .line 281
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->m:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 282
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->m:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/k;->l()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/exoplayer2/extractor/c/e;->s:J

    .line 283
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->m:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v0

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->r:I

    .line 286
    :cond_1
    iget-wide v4, p0, Lcom/google/android/exoplayer2/extractor/c/e;->s:J

    const-wide/16 v6, 0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_2

    .line 289
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->m:Lcom/google/android/exoplayer2/util/k;

    iget-object v0, v0, Lcom/google/android/exoplayer2/util/k;->a:[B

    invoke-interface {p1, v0, v8, v8}, Lcom/google/android/exoplayer2/extractor/f;->b([BII)V

    .line 290
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->t:I

    add-int/2addr v0, v8

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->t:I

    .line 291
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->m:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/k;->v()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/exoplayer2/extractor/c/e;->s:J

    .line 294
    :cond_2
    iget-wide v4, p0, Lcom/google/android/exoplayer2/extractor/c/e;->s:J

    iget v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->t:I

    int-to-long v6, v0

    cmp-long v0, v4, v6

    if-gez v0, :cond_3

    .line 295
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Atom size less than header length (unsupported)."

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 298
    :cond_3
    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/f;->c()J

    move-result-wide v4

    iget v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->t:I

    int-to-long v6, v0

    sub-long/2addr v4, v6

    .line 299
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->r:I

    sget v3, Lcom/google/android/exoplayer2/extractor/c/a;->K:I

    if-ne v0, v3, :cond_4

    .line 301
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v6

    move v3, v1

    .line 302
    :goto_1
    if-ge v3, v6, :cond_4

    .line 303
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->f:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/extractor/c/e$b;

    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/c/e$b;->a:Lcom/google/android/exoplayer2/extractor/c/l;

    .line 304
    iput-wide v4, v0, Lcom/google/android/exoplayer2/extractor/c/l;->b:J

    .line 305
    iput-wide v4, v0, Lcom/google/android/exoplayer2/extractor/c/l;->d:J

    .line 306
    iput-wide v4, v0, Lcom/google/android/exoplayer2/extractor/c/l;->c:J

    .line 302
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 310
    :cond_4
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->r:I

    sget v3, Lcom/google/android/exoplayer2/extractor/c/a;->h:I

    if-ne v0, v3, :cond_6

    .line 311
    iput-object v9, p0, Lcom/google/android/exoplayer2/extractor/c/e;->z:Lcom/google/android/exoplayer2/extractor/c/e$b;

    .line 312
    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->s:J

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->v:J

    .line 313
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->H:Z

    if-nez v0, :cond_5

    .line 314
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->E:Lcom/google/android/exoplayer2/extractor/g;

    new-instance v1, Lcom/google/android/exoplayer2/extractor/l$a;

    iget-wide v4, p0, Lcom/google/android/exoplayer2/extractor/c/e;->x:J

    invoke-direct {v1, v4, v5}, Lcom/google/android/exoplayer2/extractor/l$a;-><init>(J)V

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/extractor/g;->a(Lcom/google/android/exoplayer2/extractor/l;)V

    .line 315
    iput-boolean v2, p0, Lcom/google/android/exoplayer2/extractor/c/e;->H:Z

    .line 317
    :cond_5
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->q:I

    move v0, v2

    .line 318
    goto/16 :goto_0

    .line 321
    :cond_6
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->r:I

    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/c/e;->b(I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 322
    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/f;->c()J

    move-result-wide v0

    iget-wide v4, p0, Lcom/google/android/exoplayer2/extractor/c/e;->s:J

    add-long/2addr v0, v4

    const-wide/16 v4, 0x8

    sub-long/2addr v0, v4

    .line 323
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/c/e;->o:Ljava/util/Stack;

    new-instance v4, Lcom/google/android/exoplayer2/extractor/c/a$a;

    iget v5, p0, Lcom/google/android/exoplayer2/extractor/c/e;->r:I

    invoke-direct {v4, v5, v0, v1}, Lcom/google/android/exoplayer2/extractor/c/a$a;-><init>(IJ)V

    invoke-virtual {v3, v4}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 324
    iget-wide v4, p0, Lcom/google/android/exoplayer2/extractor/c/e;->s:J

    iget v3, p0, Lcom/google/android/exoplayer2/extractor/c/e;->t:I

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-nez v3, :cond_7

    .line 325
    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer2/extractor/c/e;->a(J)V

    :goto_2
    move v0, v2

    .line 348
    goto/16 :goto_0

    .line 328
    :cond_7
    invoke-direct {p0}, Lcom/google/android/exoplayer2/extractor/c/e;->a()V

    goto :goto_2

    .line 330
    :cond_8
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->r:I

    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/c/e;->a(I)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 331
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->t:I

    if-eq v0, v8, :cond_9

    .line 332
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Leaf atom defines extended atom size (unsupported)."

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 334
    :cond_9
    iget-wide v4, p0, Lcom/google/android/exoplayer2/extractor/c/e;->s:J

    cmp-long v0, v4, v10

    if-lez v0, :cond_a

    .line 335
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Leaf atom with length > 2147483647 (unsupported)."

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 337
    :cond_a
    new-instance v0, Lcom/google/android/exoplayer2/util/k;

    iget-wide v4, p0, Lcom/google/android/exoplayer2/extractor/c/e;->s:J

    long-to-int v3, v4

    invoke-direct {v0, v3}, Lcom/google/android/exoplayer2/util/k;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->u:Lcom/google/android/exoplayer2/util/k;

    .line 338
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->m:Lcom/google/android/exoplayer2/util/k;

    iget-object v0, v0, Lcom/google/android/exoplayer2/util/k;->a:[B

    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/c/e;->u:Lcom/google/android/exoplayer2/util/k;

    iget-object v3, v3, Lcom/google/android/exoplayer2/util/k;->a:[B

    invoke-static {v0, v1, v3, v1, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 339
    iput v2, p0, Lcom/google/android/exoplayer2/extractor/c/e;->q:I

    goto :goto_2

    .line 341
    :cond_b
    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->s:J

    cmp-long v0, v0, v10

    if-lez v0, :cond_c

    .line 342
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Skipping atom with length > 2147483647 (unsupported)."

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 344
    :cond_c
    iput-object v9, p0, Lcom/google/android/exoplayer2/extractor/c/e;->u:Lcom/google/android/exoplayer2/util/k;

    .line 345
    iput v2, p0, Lcom/google/android/exoplayer2/extractor/c/e;->q:I

    goto :goto_2
.end method

.method private static c(Lcom/google/android/exoplayer2/util/k;)J
    .locals 2

    .prologue
    .line 524
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 525
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v0

    .line 526
    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/c/a;->a(I)I

    move-result v0

    .line 527
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->l()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->v()J

    move-result-wide v0

    goto :goto_0
.end method

.method private c(Lcom/google/android/exoplayer2/extractor/c/a$a;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    .line 448
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->f:Landroid/util/SparseArray;

    iget v1, p0, Lcom/google/android/exoplayer2/extractor/c/e;->d:I

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/c/e;->n:[B

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/exoplayer2/extractor/c/e;->a(Lcom/google/android/exoplayer2/extractor/c/a$a;Landroid/util/SparseArray;I[B)V

    .line 449
    iget-object v0, p1, Lcom/google/android/exoplayer2/extractor/c/a$a;->aR:Ljava/util/List;

    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/c/e;->a(Ljava/util/List;)Lcom/google/android/exoplayer2/drm/a;

    move-result-object v2

    .line 450
    if-eqz v2, :cond_0

    .line 451
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v3

    .line 452
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 453
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->f:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/extractor/c/e$b;

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/extractor/c/e$b;->a(Lcom/google/android/exoplayer2/drm/a;)V

    .line 452
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 456
    :cond_0
    return-void
.end method

.method private c(Lcom/google/android/exoplayer2/extractor/f;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 352
    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->s:J

    long-to-int v0, v0

    iget v1, p0, Lcom/google/android/exoplayer2/extractor/c/e;->t:I

    sub-int/2addr v0, v1

    .line 353
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/c/e;->u:Lcom/google/android/exoplayer2/util/k;

    if-eqz v1, :cond_0

    .line 354
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/c/e;->u:Lcom/google/android/exoplayer2/util/k;

    iget-object v1, v1, Lcom/google/android/exoplayer2/util/k;->a:[B

    const/16 v2, 0x8

    invoke-interface {p1, v1, v2, v0}, Lcom/google/android/exoplayer2/extractor/f;->b([BII)V

    .line 355
    new-instance v0, Lcom/google/android/exoplayer2/extractor/c/a$b;

    iget v1, p0, Lcom/google/android/exoplayer2/extractor/c/e;->r:I

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/c/e;->u:Lcom/google/android/exoplayer2/util/k;

    invoke-direct {v0, v1, v2}, Lcom/google/android/exoplayer2/extractor/c/a$b;-><init>(ILcom/google/android/exoplayer2/util/k;)V

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/f;->c()J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/exoplayer2/extractor/c/e;->a(Lcom/google/android/exoplayer2/extractor/c/a$b;J)V

    .line 359
    :goto_0
    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/f;->c()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer2/extractor/c/e;->a(J)V

    .line 360
    return-void

    .line 357
    :cond_0
    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/extractor/f;->b(I)V

    goto :goto_0
.end method

.method private static d(Lcom/google/android/exoplayer2/util/k;)J
    .locals 2

    .prologue
    .line 737
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 738
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v0

    .line 739
    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/c/a;->a(I)I

    move-result v0

    .line 740
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->v()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->l()J

    move-result-wide v0

    goto :goto_0
.end method

.method private d(Lcom/google/android/exoplayer2/extractor/f;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 996
    const/4 v1, 0x0

    .line 997
    const-wide v2, 0x7fffffffffffffffL

    .line 998
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v5

    .line 999
    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v5, :cond_0

    .line 1000
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->f:Landroid/util/SparseArray;

    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/extractor/c/e$b;

    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/c/e$b;->a:Lcom/google/android/exoplayer2/extractor/c/l;

    .line 1001
    iget-boolean v6, v0, Lcom/google/android/exoplayer2/extractor/c/l;->r:Z

    if-eqz v6, :cond_3

    iget-wide v6, v0, Lcom/google/android/exoplayer2/extractor/c/l;->d:J

    cmp-long v6, v6, v2

    if-gez v6, :cond_3

    .line 1003
    iget-wide v2, v0, Lcom/google/android/exoplayer2/extractor/c/l;->d:J

    .line 1004
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->f:Landroid/util/SparseArray;

    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/extractor/c/e$b;

    move-wide v8, v2

    move-object v2, v0

    move-wide v0, v8

    .line 999
    :goto_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move-wide v8, v0

    move-object v1, v2

    move-wide v2, v8

    goto :goto_0

    .line 1007
    :cond_0
    if-nez v1, :cond_1

    .line 1008
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->q:I

    .line 1017
    :goto_2
    return-void

    .line 1011
    :cond_1
    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/f;->c()J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-int v0, v2

    .line 1012
    if-gez v0, :cond_2

    .line 1013
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Offset to encryption data was negative."

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1015
    :cond_2
    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/extractor/f;->b(I)V

    .line 1016
    iget-object v0, v1, Lcom/google/android/exoplayer2/extractor/c/e$b;->a:Lcom/google/android/exoplayer2/extractor/c/l;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/extractor/c/l;->a(Lcom/google/android/exoplayer2/extractor/f;)V

    goto :goto_2

    :cond_3
    move-wide v8, v2

    move-object v2, v1

    move-wide v0, v8

    goto :goto_1
.end method

.method private e(Lcom/google/android/exoplayer2/extractor/f;)Z
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 1034
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->q:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_5

    .line 1035
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->z:Lcom/google/android/exoplayer2/extractor/c/e$b;

    if-nez v0, :cond_3

    .line 1036
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->f:Landroid/util/SparseArray;

    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/c/e;->a(Landroid/util/SparseArray;)Lcom/google/android/exoplayer2/extractor/c/e$b;

    move-result-object v1

    .line 1037
    if-nez v1, :cond_1

    .line 1040
    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->v:J

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/f;->c()J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    .line 1041
    if-gez v0, :cond_0

    .line 1042
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Offset to end of mdat was negative."

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1044
    :cond_0
    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/extractor/f;->b(I)V

    .line 1045
    invoke-direct {p0}, Lcom/google/android/exoplayer2/extractor/c/e;->a()V

    .line 1046
    const/4 v0, 0x0

    .line 1175
    :goto_0
    return v0

    .line 1049
    :cond_1
    iget-object v0, v1, Lcom/google/android/exoplayer2/extractor/c/e$b;->a:Lcom/google/android/exoplayer2/extractor/c/l;

    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/c/l;->g:[J

    iget v2, v1, Lcom/google/android/exoplayer2/extractor/c/e$b;->g:I

    aget-wide v2, v0, v2

    .line 1052
    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/f;->c()J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-int v0, v2

    .line 1053
    if-gez v0, :cond_2

    .line 1055
    const-string v0, "FragmentedMp4Extractor"

    const-string v2, "Ignoring negative offset to sample data."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1056
    const/4 v0, 0x0

    .line 1058
    :cond_2
    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/extractor/f;->b(I)V

    .line 1059
    iput-object v1, p0, Lcom/google/android/exoplayer2/extractor/c/e;->z:Lcom/google/android/exoplayer2/extractor/c/e$b;

    .line 1061
    :cond_3
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->z:Lcom/google/android/exoplayer2/extractor/c/e$b;

    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/c/e$b;->a:Lcom/google/android/exoplayer2/extractor/c/l;

    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/c/l;->i:[I

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/c/e;->z:Lcom/google/android/exoplayer2/extractor/c/e$b;

    iget v1, v1, Lcom/google/android/exoplayer2/extractor/c/e$b;->e:I

    aget v0, v0, v1

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->A:I

    .line 1063
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->z:Lcom/google/android/exoplayer2/extractor/c/e$b;

    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/c/e$b;->a:Lcom/google/android/exoplayer2/extractor/c/l;

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/extractor/c/l;->m:Z

    if-eqz v0, :cond_6

    .line 1064
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->z:Lcom/google/android/exoplayer2/extractor/c/e$b;

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/extractor/c/e;->a(Lcom/google/android/exoplayer2/extractor/c/e$b;)I

    move-result v0

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->B:I

    .line 1065
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->A:I

    iget v1, p0, Lcom/google/android/exoplayer2/extractor/c/e;->B:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->A:I

    .line 1069
    :goto_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->z:Lcom/google/android/exoplayer2/extractor/c/e$b;

    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/c/e$b;->c:Lcom/google/android/exoplayer2/extractor/c/j;

    iget v0, v0, Lcom/google/android/exoplayer2/extractor/c/j;->g:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 1070
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->A:I

    add-int/lit8 v0, v0, -0x8

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->A:I

    .line 1071
    const/16 v0, 0x8

    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/extractor/f;->b(I)V

    .line 1073
    :cond_4
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->q:I

    .line 1074
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->C:I

    .line 1077
    :cond_5
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->z:Lcom/google/android/exoplayer2/extractor/c/e$b;

    iget-object v12, v0, Lcom/google/android/exoplayer2/extractor/c/e$b;->a:Lcom/google/android/exoplayer2/extractor/c/l;

    .line 1078
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->z:Lcom/google/android/exoplayer2/extractor/c/e$b;

    iget-object v5, v0, Lcom/google/android/exoplayer2/extractor/c/e$b;->c:Lcom/google/android/exoplayer2/extractor/c/j;

    .line 1079
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->z:Lcom/google/android/exoplayer2/extractor/c/e$b;

    iget-object v1, v0, Lcom/google/android/exoplayer2/extractor/c/e$b;->b:Lcom/google/android/exoplayer2/extractor/m;

    .line 1080
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->z:Lcom/google/android/exoplayer2/extractor/c/e$b;

    iget v4, v0, Lcom/google/android/exoplayer2/extractor/c/e$b;->e:I

    .line 1081
    iget v0, v5, Lcom/google/android/exoplayer2/extractor/c/j;->j:I

    if-eqz v0, :cond_b

    .line 1084
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->h:Lcom/google/android/exoplayer2/util/k;

    iget-object v3, v0, Lcom/google/android/exoplayer2/util/k;->a:[B

    .line 1085
    const/4 v0, 0x0

    const/4 v2, 0x0

    aput-byte v2, v3, v0

    .line 1086
    const/4 v0, 0x1

    const/4 v2, 0x0

    aput-byte v2, v3, v0

    .line 1087
    const/4 v0, 0x2

    const/4 v2, 0x0

    aput-byte v2, v3, v0

    .line 1088
    iget v0, v5, Lcom/google/android/exoplayer2/extractor/c/j;->j:I

    add-int/lit8 v6, v0, 0x1

    .line 1089
    iget v0, v5, Lcom/google/android/exoplayer2/extractor/c/j;->j:I

    rsub-int/lit8 v7, v0, 0x4

    .line 1093
    :goto_2
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->B:I

    iget v2, p0, Lcom/google/android/exoplayer2/extractor/c/e;->A:I

    if-ge v0, v2, :cond_c

    .line 1094
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->C:I

    if-nez v0, :cond_8

    .line 1096
    invoke-interface {p1, v3, v7, v6}, Lcom/google/android/exoplayer2/extractor/f;->b([BII)V

    .line 1097
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->h:Lcom/google/android/exoplayer2/util/k;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 1098
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->h:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->C:I

    .line 1100
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->g:Lcom/google/android/exoplayer2/util/k;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 1101
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->g:Lcom/google/android/exoplayer2/util/k;

    const/4 v2, 0x4

    invoke-interface {v1, v0, v2}, Lcom/google/android/exoplayer2/extractor/m;->a(Lcom/google/android/exoplayer2/util/k;I)V

    .line 1103
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->h:Lcom/google/android/exoplayer2/util/k;

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, Lcom/google/android/exoplayer2/extractor/m;->a(Lcom/google/android/exoplayer2/util/k;I)V

    .line 1104
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->G:[Lcom/google/android/exoplayer2/extractor/m;

    if-eqz v0, :cond_7

    iget-object v0, v5, Lcom/google/android/exoplayer2/extractor/c/j;->f:Lcom/google/android/exoplayer2/j;

    iget-object v0, v0, Lcom/google/android/exoplayer2/j;->f:Ljava/lang/String;

    const/4 v2, 0x4

    aget-byte v2, v3, v2

    .line 1105
    invoke-static {v0, v2}, Lcom/google/android/exoplayer2/util/i;->a(Ljava/lang/String;B)Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->D:Z

    .line 1106
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->B:I

    add-int/lit8 v0, v0, 0x5

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->B:I

    .line 1107
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->A:I

    add-int/2addr v0, v7

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->A:I

    goto :goto_2

    .line 1067
    :cond_6
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->B:I

    goto/16 :goto_1

    .line 1105
    :cond_7
    const/4 v0, 0x0

    goto :goto_3

    .line 1110
    :cond_8
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->D:Z

    if-eqz v0, :cond_a

    .line 1112
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->i:Lcom/google/android/exoplayer2/util/k;

    iget v2, p0, Lcom/google/android/exoplayer2/extractor/c/e;->C:I

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/util/k;->a(I)V

    .line 1113
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->i:Lcom/google/android/exoplayer2/util/k;

    iget-object v0, v0, Lcom/google/android/exoplayer2/util/k;->a:[B

    const/4 v2, 0x0

    iget v8, p0, Lcom/google/android/exoplayer2/extractor/c/e;->C:I

    invoke-interface {p1, v0, v2, v8}, Lcom/google/android/exoplayer2/extractor/f;->b([BII)V

    .line 1114
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->i:Lcom/google/android/exoplayer2/util/k;

    iget v2, p0, Lcom/google/android/exoplayer2/extractor/c/e;->C:I

    invoke-interface {v1, v0, v2}, Lcom/google/android/exoplayer2/extractor/m;->a(Lcom/google/android/exoplayer2/util/k;I)V

    .line 1115
    iget v2, p0, Lcom/google/android/exoplayer2/extractor/c/e;->C:I

    .line 1117
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->i:Lcom/google/android/exoplayer2/util/k;

    iget-object v0, v0, Lcom/google/android/exoplayer2/util/k;->a:[B

    iget-object v8, p0, Lcom/google/android/exoplayer2/extractor/c/e;->i:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v8}, Lcom/google/android/exoplayer2/util/k;->c()I

    move-result v8

    invoke-static {v0, v8}, Lcom/google/android/exoplayer2/util/i;->a([BI)I

    move-result v8

    .line 1119
    iget-object v9, p0, Lcom/google/android/exoplayer2/extractor/c/e;->i:Lcom/google/android/exoplayer2/util/k;

    const-string v0, "video/hevc"

    iget-object v10, v5, Lcom/google/android/exoplayer2/extractor/c/j;->f:Lcom/google/android/exoplayer2/j;

    iget-object v10, v10, Lcom/google/android/exoplayer2/j;->f:Ljava/lang/String;

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    :goto_4
    invoke-virtual {v9, v0}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 1120
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->i:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v0, v8}, Lcom/google/android/exoplayer2/util/k;->b(I)V

    .line 1121
    invoke-virtual {v12, v4}, Lcom/google/android/exoplayer2/extractor/c/l;->b(I)J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->i:Lcom/google/android/exoplayer2/util/k;

    iget-object v10, p0, Lcom/google/android/exoplayer2/extractor/c/e;->G:[Lcom/google/android/exoplayer2/extractor/m;

    invoke-static {v8, v9, v0, v10}, Lcom/google/android/exoplayer2/text/a/g;->a(JLcom/google/android/exoplayer2/util/k;[Lcom/google/android/exoplayer2/extractor/m;)V

    move v0, v2

    .line 1127
    :goto_5
    iget v2, p0, Lcom/google/android/exoplayer2/extractor/c/e;->B:I

    add-int/2addr v2, v0

    iput v2, p0, Lcom/google/android/exoplayer2/extractor/c/e;->B:I

    .line 1128
    iget v2, p0, Lcom/google/android/exoplayer2/extractor/c/e;->C:I

    sub-int v0, v2, v0

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->C:I

    goto/16 :goto_2

    .line 1119
    :cond_9
    const/4 v0, 0x0

    goto :goto_4

    .line 1125
    :cond_a
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->C:I

    const/4 v2, 0x0

    invoke-interface {v1, p1, v0, v2}, Lcom/google/android/exoplayer2/extractor/m;->a(Lcom/google/android/exoplayer2/extractor/f;IZ)I

    move-result v0

    goto :goto_5

    .line 1132
    :cond_b
    :goto_6
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->B:I

    iget v2, p0, Lcom/google/android/exoplayer2/extractor/c/e;->A:I

    if-ge v0, v2, :cond_c

    .line 1133
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->A:I

    iget v2, p0, Lcom/google/android/exoplayer2/extractor/c/e;->B:I

    sub-int/2addr v0, v2

    const/4 v2, 0x0

    invoke-interface {v1, p1, v0, v2}, Lcom/google/android/exoplayer2/extractor/m;->a(Lcom/google/android/exoplayer2/extractor/f;IZ)I

    move-result v0

    .line 1134
    iget v2, p0, Lcom/google/android/exoplayer2/extractor/c/e;->B:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->B:I

    goto :goto_6

    .line 1138
    :cond_c
    invoke-virtual {v12, v4}, Lcom/google/android/exoplayer2/extractor/c/l;->b(I)J

    move-result-wide v2

    const-wide/16 v6, 0x3e8

    mul-long/2addr v2, v6

    .line 1139
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->l:Lcom/google/android/exoplayer2/util/p;

    if-eqz v0, :cond_d

    .line 1140
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->l:Lcom/google/android/exoplayer2/util/p;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/exoplayer2/util/p;->c(J)J

    move-result-wide v2

    .line 1143
    :cond_d
    iget-object v0, v12, Lcom/google/android/exoplayer2/extractor/c/l;->l:[Z

    aget-boolean v0, v0, v4

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    .line 1147
    :goto_7
    const/4 v7, 0x0

    .line 1148
    iget-boolean v4, v12, Lcom/google/android/exoplayer2/extractor/c/l;->m:Z

    if-eqz v4, :cond_12

    .line 1149
    const/high16 v4, 0x40000000    # 2.0f

    or-int/2addr v4, v0

    .line 1150
    iget-object v0, v12, Lcom/google/android/exoplayer2/extractor/c/l;->o:Lcom/google/android/exoplayer2/extractor/c/k;

    if-eqz v0, :cond_f

    iget-object v0, v12, Lcom/google/android/exoplayer2/extractor/c/l;->o:Lcom/google/android/exoplayer2/extractor/c/k;

    .line 1153
    :goto_8
    iget-object v7, v0, Lcom/google/android/exoplayer2/extractor/c/k;->c:Lcom/google/android/exoplayer2/extractor/m$a;

    .line 1156
    :goto_9
    iget v5, p0, Lcom/google/android/exoplayer2/extractor/c/e;->A:I

    const/4 v6, 0x0

    invoke-interface/range {v1 .. v7}, Lcom/google/android/exoplayer2/extractor/m;->a(JIIILcom/google/android/exoplayer2/extractor/m$a;)V

    .line 1158
    :goto_a
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->p:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_10

    .line 1159
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->p:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/extractor/c/e$a;

    .line 1160
    iget v1, p0, Lcom/google/android/exoplayer2/extractor/c/e;->w:I

    iget v4, v0, Lcom/google/android/exoplayer2/extractor/c/e$a;->b:I

    sub-int/2addr v1, v4

    iput v1, p0, Lcom/google/android/exoplayer2/extractor/c/e;->w:I

    .line 1161
    iget-object v5, p0, Lcom/google/android/exoplayer2/extractor/c/e;->F:Lcom/google/android/exoplayer2/extractor/m;

    iget-wide v6, v0, Lcom/google/android/exoplayer2/extractor/c/e$a;->a:J

    add-long/2addr v6, v2

    const/4 v8, 0x1

    iget v9, v0, Lcom/google/android/exoplayer2/extractor/c/e$a;->b:I

    iget v10, p0, Lcom/google/android/exoplayer2/extractor/c/e;->w:I

    const/4 v11, 0x0

    invoke-interface/range {v5 .. v11}, Lcom/google/android/exoplayer2/extractor/m;->a(JIIILcom/google/android/exoplayer2/extractor/m$a;)V

    goto :goto_a

    .line 1143
    :cond_e
    const/4 v0, 0x0

    goto :goto_7

    .line 1150
    :cond_f
    iget-object v0, v12, Lcom/google/android/exoplayer2/extractor/c/l;->a:Lcom/google/android/exoplayer2/extractor/c/c;

    iget v0, v0, Lcom/google/android/exoplayer2/extractor/c/c;->a:I

    .line 1152
    invoke-virtual {v5, v0}, Lcom/google/android/exoplayer2/extractor/c/j;->a(I)Lcom/google/android/exoplayer2/extractor/c/k;

    move-result-object v0

    goto :goto_8

    .line 1166
    :cond_10
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->z:Lcom/google/android/exoplayer2/extractor/c/e$b;

    iget v1, v0, Lcom/google/android/exoplayer2/extractor/c/e$b;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/c/e$b;->e:I

    .line 1167
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->z:Lcom/google/android/exoplayer2/extractor/c/e$b;

    iget v1, v0, Lcom/google/android/exoplayer2/extractor/c/e$b;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/c/e$b;->f:I

    .line 1168
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->z:Lcom/google/android/exoplayer2/extractor/c/e$b;

    iget v0, v0, Lcom/google/android/exoplayer2/extractor/c/e$b;->f:I

    iget-object v1, v12, Lcom/google/android/exoplayer2/extractor/c/l;->h:[I

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/c/e;->z:Lcom/google/android/exoplayer2/extractor/c/e$b;

    iget v2, v2, Lcom/google/android/exoplayer2/extractor/c/e$b;->g:I

    aget v1, v1, v2

    if-ne v0, v1, :cond_11

    .line 1170
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->z:Lcom/google/android/exoplayer2/extractor/c/e$b;

    iget v1, v0, Lcom/google/android/exoplayer2/extractor/c/e$b;->g:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/c/e$b;->g:I

    .line 1171
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->z:Lcom/google/android/exoplayer2/extractor/c/e$b;

    const/4 v1, 0x0

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/c/e$b;->f:I

    .line 1172
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->z:Lcom/google/android/exoplayer2/extractor/c/e$b;

    .line 1174
    :cond_11
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->q:I

    .line 1175
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_12
    move v4, v0

    goto :goto_9
.end method


# virtual methods
.method public a(Lcom/google/android/exoplayer2/extractor/f;Lcom/google/android/exoplayer2/extractor/k;)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 249
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->q:I

    packed-switch v0, :pswitch_data_0

    .line 262
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/c/e;->e(Lcom/google/android/exoplayer2/extractor/f;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 263
    const/4 v0, 0x0

    :goto_1
    return v0

    .line 251
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/c/e;->b(Lcom/google/android/exoplayer2/extractor/f;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 252
    const/4 v0, -0x1

    goto :goto_1

    .line 256
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/c/e;->c(Lcom/google/android/exoplayer2/extractor/f;)V

    goto :goto_0

    .line 259
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/c/e;->d(Lcom/google/android/exoplayer2/extractor/f;)V

    goto :goto_0

    .line 249
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(JJ)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 230
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v3

    move v1, v2

    .line 231
    :goto_0
    if-ge v1, v3, :cond_0

    .line 232
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->f:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/extractor/c/e$b;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/c/e$b;->a()V

    .line 231
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 234
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->p:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 235
    iput v2, p0, Lcom/google/android/exoplayer2/extractor/c/e;->w:I

    .line 236
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->o:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 237
    invoke-direct {p0}, Lcom/google/android/exoplayer2/extractor/c/e;->a()V

    .line 238
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/g;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 218
    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/c/e;->E:Lcom/google/android/exoplayer2/extractor/g;

    .line 219
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->e:Lcom/google/android/exoplayer2/extractor/c/j;

    if-eqz v0, :cond_0

    .line 220
    new-instance v0, Lcom/google/android/exoplayer2/extractor/c/e$b;

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/c/e;->e:Lcom/google/android/exoplayer2/extractor/c/j;

    iget v1, v1, Lcom/google/android/exoplayer2/extractor/c/j;->b:I

    invoke-interface {p1, v3, v1}, Lcom/google/android/exoplayer2/extractor/g;->a(II)Lcom/google/android/exoplayer2/extractor/m;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/extractor/c/e$b;-><init>(Lcom/google/android/exoplayer2/extractor/m;)V

    .line 221
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/c/e;->e:Lcom/google/android/exoplayer2/extractor/c/j;

    new-instance v2, Lcom/google/android/exoplayer2/extractor/c/c;

    invoke-direct {v2, v3, v3, v3, v3}, Lcom/google/android/exoplayer2/extractor/c/c;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/extractor/c/e$b;->a(Lcom/google/android/exoplayer2/extractor/c/j;Lcom/google/android/exoplayer2/extractor/c/c;)V

    .line 222
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/c/e;->f:Landroid/util/SparseArray;

    invoke-virtual {v1, v3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 223
    invoke-direct {p0}, Lcom/google/android/exoplayer2/extractor/c/e;->b()V

    .line 224
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e;->E:Lcom/google/android/exoplayer2/extractor/g;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/extractor/g;->a()V

    .line 226
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/f;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 213
    invoke-static {p1}, Lcom/google/android/exoplayer2/extractor/c/i;->a(Lcom/google/android/exoplayer2/extractor/f;)Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 243
    return-void
.end method
