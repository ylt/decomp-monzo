.class final Lcom/google/android/exoplayer2/extractor/c/e$b;
.super Ljava/lang/Object;
.source "FragmentedMp4Extractor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/extractor/c/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation


# instance fields
.field public final a:Lcom/google/android/exoplayer2/extractor/c/l;

.field public final b:Lcom/google/android/exoplayer2/extractor/m;

.field public c:Lcom/google/android/exoplayer2/extractor/c/j;

.field public d:Lcom/google/android/exoplayer2/extractor/c/c;

.field public e:I

.field public f:I

.field public g:I


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/extractor/m;)V
    .locals 1

    .prologue
    .line 1322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1323
    new-instance v0, Lcom/google/android/exoplayer2/extractor/c/l;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/extractor/c/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e$b;->a:Lcom/google/android/exoplayer2/extractor/c/l;

    .line 1324
    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/c/e$b;->b:Lcom/google/android/exoplayer2/extractor/m;

    .line 1325
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1335
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e$b;->a:Lcom/google/android/exoplayer2/extractor/c/l;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/c/l;->a()V

    .line 1336
    iput v1, p0, Lcom/google/android/exoplayer2/extractor/c/e$b;->e:I

    .line 1337
    iput v1, p0, Lcom/google/android/exoplayer2/extractor/c/e$b;->g:I

    .line 1338
    iput v1, p0, Lcom/google/android/exoplayer2/extractor/c/e$b;->f:I

    .line 1339
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/drm/a;)V
    .locals 3

    .prologue
    .line 1342
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e$b;->c:Lcom/google/android/exoplayer2/extractor/c/j;

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/c/e$b;->a:Lcom/google/android/exoplayer2/extractor/c/l;

    iget-object v1, v1, Lcom/google/android/exoplayer2/extractor/c/l;->a:Lcom/google/android/exoplayer2/extractor/c/c;

    iget v1, v1, Lcom/google/android/exoplayer2/extractor/c/c;->a:I

    .line 1343
    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/extractor/c/j;->a(I)Lcom/google/android/exoplayer2/extractor/c/k;

    move-result-object v0

    .line 1344
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/c/k;->b:Ljava/lang/String;

    .line 1345
    :goto_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/c/e$b;->b:Lcom/google/android/exoplayer2/extractor/m;

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/c/e$b;->c:Lcom/google/android/exoplayer2/extractor/c/j;

    iget-object v2, v2, Lcom/google/android/exoplayer2/extractor/c/j;->f:Lcom/google/android/exoplayer2/j;

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/drm/a;->a(Ljava/lang/String;)Lcom/google/android/exoplayer2/drm/a;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/exoplayer2/j;->a(Lcom/google/android/exoplayer2/drm/a;)Lcom/google/android/exoplayer2/j;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/exoplayer2/extractor/m;->a(Lcom/google/android/exoplayer2/j;)V

    .line 1346
    return-void

    .line 1344
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/c/j;Lcom/google/android/exoplayer2/extractor/c/c;)V
    .locals 2

    .prologue
    .line 1328
    invoke-static {p1}, Lcom/google/android/exoplayer2/util/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/extractor/c/j;

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e$b;->c:Lcom/google/android/exoplayer2/extractor/c/j;

    .line 1329
    invoke-static {p2}, Lcom/google/android/exoplayer2/util/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/extractor/c/c;

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e$b;->d:Lcom/google/android/exoplayer2/extractor/c/c;

    .line 1330
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/c/e$b;->b:Lcom/google/android/exoplayer2/extractor/m;

    iget-object v1, p1, Lcom/google/android/exoplayer2/extractor/c/j;->f:Lcom/google/android/exoplayer2/j;

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/extractor/m;->a(Lcom/google/android/exoplayer2/j;)V

    .line 1331
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/extractor/c/e$b;->a()V

    .line 1332
    return-void
.end method
