.class final Lcom/google/android/exoplayer2/extractor/a/d$b;
.super Ljava/lang/Object;
.source "MatroskaExtractor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/extractor/a/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation


# instance fields
.field public A:F

.field public B:F

.field public C:F

.field public D:F

.field public E:F

.field public F:F

.field public G:I

.field public H:I

.field public I:I

.field public J:J

.field public K:J

.field public L:Z

.field public M:Z

.field public N:Lcom/google/android/exoplayer2/extractor/m;

.field public O:I

.field private P:Ljava/lang/String;

.field public a:Ljava/lang/String;

.field public b:I

.field public c:I

.field public d:I

.field public e:Z

.field public f:[B

.field public g:Lcom/google/android/exoplayer2/extractor/m$a;

.field public h:[B

.field public i:Lcom/google/android/exoplayer2/drm/a;

.field public j:I

.field public k:I

.field public l:I

.field public m:I

.field public n:I

.field public o:[B

.field public p:I

.field public q:Z

.field public r:I

.field public s:I

.field public t:I

.field public u:I

.field public v:I

.field public w:F

.field public x:F

.field public y:F

.field public z:F


# direct methods
.method private constructor <init>()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    const/high16 v1, -0x40800000    # -1.0f

    .line 1516
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1542
    iput v2, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->j:I

    .line 1543
    iput v2, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->k:I

    .line 1544
    iput v2, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->l:I

    .line 1545
    iput v2, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->m:I

    .line 1546
    iput v3, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->n:I

    .line 1547
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->o:[B

    .line 1548
    iput v2, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->p:I

    .line 1550
    iput-boolean v3, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->q:Z

    .line 1551
    iput v2, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->r:I

    .line 1553
    iput v2, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->s:I

    .line 1555
    iput v2, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->t:I

    .line 1557
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->u:I

    .line 1558
    const/16 v0, 0xc8

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->v:I

    .line 1559
    iput v1, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->w:F

    .line 1560
    iput v1, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->x:F

    .line 1561
    iput v1, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->y:F

    .line 1562
    iput v1, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->z:F

    .line 1563
    iput v1, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->A:F

    .line 1564
    iput v1, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->B:F

    .line 1565
    iput v1, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->C:F

    .line 1566
    iput v1, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->D:F

    .line 1567
    iput v1, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->E:F

    .line 1568
    iput v1, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->F:F

    .line 1571
    iput v4, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->G:I

    .line 1572
    iput v2, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->H:I

    .line 1573
    const/16 v0, 0x1f40

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->I:I

    .line 1574
    iput-wide v6, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->J:J

    .line 1575
    iput-wide v6, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->K:J

    .line 1579
    iput-boolean v4, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->M:Z

    .line 1580
    const-string v0, "eng"

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->P:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/exoplayer2/extractor/a/d$1;)V
    .locals 0

    .prologue
    .line 1516
    invoke-direct {p0}, Lcom/google/android/exoplayer2/extractor/a/d$b;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer2/extractor/a/d$b;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 1516
    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->P:Ljava/lang/String;

    return-object p1
.end method

.method private static a(Lcom/google/android/exoplayer2/util/k;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/util/k;",
            ")",
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    .line 1833
    const/16 v0, 0x10

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 1834
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->m()J

    move-result-wide v0

    .line 1835
    const-wide/32 v2, 0x31435657

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1836
    const/4 v0, 0x0

    .line 1848
    :goto_0
    return-object v0

    .line 1841
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->d()I

    move-result v0

    add-int/lit8 v0, v0, 0x14

    .line 1842
    iget-object v1, p0, Lcom/google/android/exoplayer2/util/k;->a:[B

    .line 1843
    :goto_1
    array-length v2, v1

    add-int/lit8 v2, v2, -0x4

    if-ge v0, v2, :cond_2

    .line 1844
    aget-byte v2, v1, v0

    if-nez v2, :cond_1

    add-int/lit8 v2, v0, 0x1

    aget-byte v2, v1, v2

    if-nez v2, :cond_1

    add-int/lit8 v2, v0, 0x2

    aget-byte v2, v1, v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    add-int/lit8 v2, v0, 0x3

    aget-byte v2, v1, v2

    const/16 v3, 0xf

    if-ne v2, v3, :cond_1

    .line 1847
    array-length v2, v1

    invoke-static {v1, v0, v2}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    .line 1848
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 1843
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1852
    :cond_2
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Failed to find FourCC VC1 initialization data"

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1853
    :catch_0
    move-exception v0

    .line 1854
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Error parsing FourCC VC1 codec private"

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a([B)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v5, -0x1

    const/4 v0, 0x0

    .line 1867
    const/4 v1, 0x0

    :try_start_0
    aget-byte v1, p0, v1

    if-eq v1, v2, :cond_0

    .line 1868
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Error parsing vorbis codec private"

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1904
    :catch_0
    move-exception v0

    .line 1905
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Error parsing vorbis codec private"

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v2, v0

    move v3, v4

    .line 1872
    :goto_0
    :try_start_1
    aget-byte v1, p0, v3

    if-ne v1, v5, :cond_1

    .line 1873
    add-int/lit16 v1, v2, 0xff

    .line 1874
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    goto :goto_0

    .line 1876
    :cond_1
    add-int/lit8 v1, v3, 0x1

    aget-byte v3, p0, v3

    add-int/2addr v2, v3

    .line 1879
    :goto_1
    aget-byte v3, p0, v1

    if-ne v3, v5, :cond_2

    .line 1880
    add-int/lit16 v0, v0, 0xff

    .line 1881
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1883
    :cond_2
    add-int/lit8 v3, v1, 0x1

    aget-byte v1, p0, v1

    add-int/2addr v0, v1

    .line 1885
    aget-byte v1, p0, v3

    if-eq v1, v4, :cond_3

    .line 1886
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Error parsing vorbis codec private"

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1888
    :cond_3
    new-array v1, v2, [B

    .line 1889
    const/4 v4, 0x0

    invoke-static {p0, v3, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1890
    add-int/2addr v2, v3

    .line 1891
    aget-byte v3, p0, v2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_4

    .line 1892
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Error parsing vorbis codec private"

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1894
    :cond_4
    add-int/2addr v0, v2

    .line 1895
    aget-byte v2, p0, v0

    const/4 v3, 0x5

    if-eq v2, v3, :cond_5

    .line 1896
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Error parsing vorbis codec private"

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1898
    :cond_5
    array-length v2, p0

    sub-int/2addr v2, v0

    new-array v2, v2, [B

    .line 1899
    const/4 v3, 0x0

    array-length v4, p0

    sub-int/2addr v4, v0

    invoke-static {p0, v0, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1900
    new-instance v0, Ljava/util/ArrayList;

    const/4 v3, 0x2

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 1901
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1902
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1903
    return-object v0
.end method

.method private a()[B
    .locals 5

    .prologue
    const v4, 0x47435000    # 50000.0f

    const/high16 v3, 0x3f000000    # 0.5f

    const/high16 v1, -0x40800000    # -1.0f

    .line 1794
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->w:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->x:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->y:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->z:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->A:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->B:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->C:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->D:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->E:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->F:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 1800
    :cond_0
    const/4 v0, 0x0

    .line 1818
    :goto_0
    return-object v0

    .line 1803
    :cond_1
    const/16 v0, 0x19

    new-array v0, v0, [B

    .line 1804
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1805
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1806
    iget v2, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->w:F

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    float-to-int v2, v2

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1807
    iget v2, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->x:F

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    float-to-int v2, v2

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1808
    iget v2, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->y:F

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    float-to-int v2, v2

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1809
    iget v2, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->z:F

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    float-to-int v2, v2

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1810
    iget v2, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->A:F

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    float-to-int v2, v2

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1811
    iget v2, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->B:F

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    float-to-int v2, v2

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1812
    iget v2, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->C:F

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    float-to-int v2, v2

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1813
    iget v2, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->D:F

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    float-to-int v2, v2

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1814
    iget v2, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->E:F

    add-float/2addr v2, v3

    float-to-int v2, v2

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1815
    iget v2, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->F:F

    add-float/2addr v2, v3

    float-to-int v2, v2

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1816
    iget v2, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->u:I

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1817
    iget v2, p0, Lcom/google/android/exoplayer2/extractor/a/d$b;->v:I

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    goto :goto_0
.end method

.method private static b(Lcom/google/android/exoplayer2/util/k;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1917
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->i()I

    move-result v2

    .line 1918
    if-ne v2, v0, :cond_1

    .line 1925
    :cond_0
    :goto_0
    return v0

    .line 1920
    :cond_1
    const v3, 0xfffe

    if-ne v2, v3, :cond_3

    .line 1921
    const/16 v2, 0x18

    invoke-virtual {p0, v2}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 1922
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->p()J

    move-result-wide v2

    invoke-static {}, Lcom/google/android/exoplayer2/extractor/a/d;->b()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->getMostSignificantBits()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    .line 1923
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/k;->p()J

    move-result-wide v2

    invoke-static {}, Lcom/google/android/exoplayer2/extractor/a/d;->b()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->getLeastSignificantBits()J
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1925
    goto :goto_0

    .line 1927
    :catch_0
    move-exception v0

    .line 1928
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Error parsing MS/ACM codec private"

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a(Lcom/google/android/exoplayer2/extractor/g;I)V
    .locals 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    .line 1591
    const/4 v6, -0x1

    .line 1592
    const/4 v9, -0x1

    .line 1593
    const/4 v10, 0x0

    .line 1594
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->a:Ljava/lang/String;

    const/4 v2, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v2, :pswitch_data_0

    .line 1729
    new-instance v2, Lcom/google/android/exoplayer2/ParserException;

    const-string v3, "Unrecognized codec identifier."

    invoke-direct {v2, v3}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1594
    :sswitch_0
    const-string v4, "V_VP8"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :sswitch_1
    const-string v4, "V_VP9"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :sswitch_2
    const-string v4, "V_MPEG2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x2

    goto :goto_0

    :sswitch_3
    const-string v4, "V_MPEG4/ISO/SP"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x3

    goto :goto_0

    :sswitch_4
    const-string v4, "V_MPEG4/ISO/ASP"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x4

    goto :goto_0

    :sswitch_5
    const-string v4, "V_MPEG4/ISO/AP"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x5

    goto :goto_0

    :sswitch_6
    const-string v4, "V_MPEG4/ISO/AVC"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x6

    goto :goto_0

    :sswitch_7
    const-string v4, "V_MPEGH/ISO/HEVC"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x7

    goto :goto_0

    :sswitch_8
    const-string v4, "V_MS/VFW/FOURCC"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0x8

    goto :goto_0

    :sswitch_9
    const-string v4, "V_THEORA"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0x9

    goto :goto_0

    :sswitch_a
    const-string v4, "A_VORBIS"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0xa

    goto :goto_0

    :sswitch_b
    const-string v4, "A_OPUS"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0xb

    goto/16 :goto_0

    :sswitch_c
    const-string v4, "A_AAC"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0xc

    goto/16 :goto_0

    :sswitch_d
    const-string v4, "A_MPEG/L2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0xd

    goto/16 :goto_0

    :sswitch_e
    const-string v4, "A_MPEG/L3"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0xe

    goto/16 :goto_0

    :sswitch_f
    const-string v4, "A_AC3"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0xf

    goto/16 :goto_0

    :sswitch_10
    const-string v4, "A_EAC3"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0x10

    goto/16 :goto_0

    :sswitch_11
    const-string v4, "A_TRUEHD"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0x11

    goto/16 :goto_0

    :sswitch_12
    const-string v4, "A_DTS"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0x12

    goto/16 :goto_0

    :sswitch_13
    const-string v4, "A_DTS/EXPRESS"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0x13

    goto/16 :goto_0

    :sswitch_14
    const-string v4, "A_DTS/LOSSLESS"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0x14

    goto/16 :goto_0

    :sswitch_15
    const-string v4, "A_FLAC"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0x15

    goto/16 :goto_0

    :sswitch_16
    const-string v4, "A_MS/ACM"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0x16

    goto/16 :goto_0

    :sswitch_17
    const-string v4, "A_PCM/INT/LIT"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0x17

    goto/16 :goto_0

    :sswitch_18
    const-string v4, "S_TEXT/UTF8"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0x18

    goto/16 :goto_0

    :sswitch_19
    const-string v4, "S_TEXT/ASS"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0x19

    goto/16 :goto_0

    :sswitch_1a
    const-string v4, "S_VOBSUB"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0x1a

    goto/16 :goto_0

    :sswitch_1b
    const-string v4, "S_HDMV/PGS"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0x1b

    goto/16 :goto_0

    :sswitch_1c
    const-string v4, "S_DVBSUB"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0x1c

    goto/16 :goto_0

    .line 1596
    :pswitch_0
    const-string v3, "video/x-vnd.on2.vp8"

    .line 1734
    :cond_1
    :goto_1
    const/4 v4, 0x0

    .line 1735
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->M:Z

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :goto_2
    or-int/2addr v4, v2

    .line 1736
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->L:Z

    if-eqz v2, :cond_6

    const/4 v2, 0x2

    :goto_3
    or-int v12, v4, v2

    .line 1739
    invoke-static {v3}, Lcom/google/android/exoplayer2/util/h;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1740
    const/4 v14, 0x1

    .line 1741
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, -0x1

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->G:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->I:I

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->i:Lcom/google/android/exoplayer2/drm/a;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->P:Ljava/lang/String;

    invoke-static/range {v2 .. v13}, Lcom/google/android/exoplayer2/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIIILjava/util/List;Lcom/google/android/exoplayer2/drm/a;ILjava/lang/String;)Lcom/google/android/exoplayer2/j;

    move-result-object v2

    move v3, v14

    .line 1785
    :goto_4
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->b:I

    move-object/from16 v0, p1

    invoke-interface {v0, v4, v3}, Lcom/google/android/exoplayer2/extractor/g;->a(II)Lcom/google/android/exoplayer2/extractor/m;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->N:Lcom/google/android/exoplayer2/extractor/m;

    .line 1786
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->N:Lcom/google/android/exoplayer2/extractor/m;

    invoke-interface {v3, v2}, Lcom/google/android/exoplayer2/extractor/m;->a(Lcom/google/android/exoplayer2/j;)V

    .line 1787
    return-void

    .line 1599
    :pswitch_1
    const-string v3, "video/x-vnd.on2.vp9"

    goto :goto_1

    .line 1602
    :pswitch_2
    const-string v3, "video/mpeg2"

    goto :goto_1

    .line 1607
    :pswitch_3
    const-string v3, "video/mp4v-es"

    .line 1608
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->h:[B

    if-nez v2, :cond_2

    const/4 v2, 0x0

    :goto_5
    move-object v10, v2

    .line 1610
    goto :goto_1

    .line 1608
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->h:[B

    .line 1609
    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    goto :goto_5

    .line 1612
    :pswitch_4
    const-string v3, "video/avc"

    .line 1613
    new-instance v2, Lcom/google/android/exoplayer2/util/k;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->h:[B

    invoke-direct {v2, v4}, Lcom/google/android/exoplayer2/util/k;-><init>([B)V

    invoke-static {v2}, Lcom/google/android/exoplayer2/c/a;->a(Lcom/google/android/exoplayer2/util/k;)Lcom/google/android/exoplayer2/c/a;

    move-result-object v2

    .line 1614
    iget-object v10, v2, Lcom/google/android/exoplayer2/c/a;->a:Ljava/util/List;

    .line 1615
    iget v2, v2, Lcom/google/android/exoplayer2/c/a;->b:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->O:I

    goto :goto_1

    .line 1618
    :pswitch_5
    const-string v3, "video/hevc"

    .line 1619
    new-instance v2, Lcom/google/android/exoplayer2/util/k;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->h:[B

    invoke-direct {v2, v4}, Lcom/google/android/exoplayer2/util/k;-><init>([B)V

    invoke-static {v2}, Lcom/google/android/exoplayer2/c/d;->a(Lcom/google/android/exoplayer2/util/k;)Lcom/google/android/exoplayer2/c/d;

    move-result-object v2

    .line 1620
    iget-object v10, v2, Lcom/google/android/exoplayer2/c/d;->a:Ljava/util/List;

    .line 1621
    iget v2, v2, Lcom/google/android/exoplayer2/c/d;->b:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->O:I

    goto/16 :goto_1

    .line 1624
    :pswitch_6
    new-instance v2, Lcom/google/android/exoplayer2/util/k;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->h:[B

    invoke-direct {v2, v3}, Lcom/google/android/exoplayer2/util/k;-><init>([B)V

    invoke-static {v2}, Lcom/google/android/exoplayer2/extractor/a/d$b;->a(Lcom/google/android/exoplayer2/util/k;)Ljava/util/List;

    move-result-object v10

    .line 1625
    if-eqz v10, :cond_3

    .line 1626
    const-string v3, "video/wvc1"

    goto/16 :goto_1

    .line 1628
    :cond_3
    const-string v2, "MatroskaExtractor"

    const-string v3, "Unsupported FourCC. Setting mimeType to video/x-unknown"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1629
    const-string v3, "video/x-unknown"

    goto/16 :goto_1

    .line 1635
    :pswitch_7
    const-string v3, "video/x-unknown"

    goto/16 :goto_1

    .line 1638
    :pswitch_8
    const-string v3, "audio/vorbis"

    .line 1639
    const/16 v6, 0x2000

    .line 1640
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->h:[B

    invoke-static {v2}, Lcom/google/android/exoplayer2/extractor/a/d$b;->a([B)Ljava/util/List;

    move-result-object v10

    goto/16 :goto_1

    .line 1643
    :pswitch_9
    const-string v3, "audio/opus"

    .line 1644
    const/16 v6, 0x1680

    .line 1645
    new-instance v10, Ljava/util/ArrayList;

    const/4 v2, 0x3

    invoke-direct {v10, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 1646
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->h:[B

    invoke-interface {v10, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1647
    const/16 v2, 0x8

    .line 1648
    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->J:J

    invoke-virtual {v2, v4, v5}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    .line 1647
    invoke-interface {v10, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1649
    const/16 v2, 0x8

    .line 1650
    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->K:J

    invoke-virtual {v2, v4, v5}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    .line 1649
    invoke-interface {v10, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1653
    :pswitch_a
    const-string v3, "audio/mp4a-latm"

    .line 1654
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->h:[B

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v10

    goto/16 :goto_1

    .line 1657
    :pswitch_b
    const-string v3, "audio/mpeg-L2"

    .line 1658
    const/16 v6, 0x1000

    .line 1659
    goto/16 :goto_1

    .line 1661
    :pswitch_c
    const-string v3, "audio/mpeg"

    .line 1662
    const/16 v6, 0x1000

    .line 1663
    goto/16 :goto_1

    .line 1665
    :pswitch_d
    const-string v3, "audio/ac3"

    goto/16 :goto_1

    .line 1668
    :pswitch_e
    const-string v3, "audio/eac3"

    goto/16 :goto_1

    .line 1671
    :pswitch_f
    const-string v3, "audio/true-hd"

    goto/16 :goto_1

    .line 1675
    :pswitch_10
    const-string v3, "audio/vnd.dts"

    goto/16 :goto_1

    .line 1678
    :pswitch_11
    const-string v3, "audio/vnd.dts.hd"

    goto/16 :goto_1

    .line 1681
    :pswitch_12
    const-string v3, "audio/flac"

    .line 1682
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->h:[B

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v10

    goto/16 :goto_1

    .line 1685
    :pswitch_13
    const-string v3, "audio/raw"

    .line 1686
    new-instance v2, Lcom/google/android/exoplayer2/util/k;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->h:[B

    invoke-direct {v2, v4}, Lcom/google/android/exoplayer2/util/k;-><init>([B)V

    invoke-static {v2}, Lcom/google/android/exoplayer2/extractor/a/d$b;->b(Lcom/google/android/exoplayer2/util/k;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1687
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->H:I

    invoke-static {v2}, Lcom/google/android/exoplayer2/util/s;->b(I)I

    move-result v9

    .line 1688
    if-nez v9, :cond_1

    .line 1689
    const/4 v9, -0x1

    .line 1690
    const-string v3, "audio/x-unknown"

    .line 1691
    const-string v2, "MatroskaExtractor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unsupported PCM bit depth: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->H:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ". Setting mimeType to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1695
    :cond_4
    const-string v3, "audio/x-unknown"

    .line 1696
    const-string v2, "MatroskaExtractor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Non-PCM MS/ACM is unsupported. Setting mimeType to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1700
    :pswitch_14
    const-string v3, "audio/raw"

    .line 1701
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->H:I

    invoke-static {v2}, Lcom/google/android/exoplayer2/util/s;->b(I)I

    move-result v9

    .line 1702
    if-nez v9, :cond_1

    .line 1703
    const/4 v9, -0x1

    .line 1704
    const-string v3, "audio/x-unknown"

    .line 1705
    const-string v2, "MatroskaExtractor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unsupported PCM bit depth: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->H:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ". Setting mimeType to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1710
    :pswitch_15
    const-string v3, "application/x-subrip"

    goto/16 :goto_1

    .line 1713
    :pswitch_16
    const-string v3, "text/x-ssa"

    goto/16 :goto_1

    .line 1716
    :pswitch_17
    const-string v3, "application/vobsub"

    .line 1717
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->h:[B

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v10

    goto/16 :goto_1

    .line 1720
    :pswitch_18
    const-string v3, "application/pgs"

    goto/16 :goto_1

    .line 1723
    :pswitch_19
    const-string v3, "application/dvbsubs"

    .line 1725
    const/4 v2, 0x4

    new-array v2, v2, [B

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->h:[B

    const/4 v7, 0x0

    aget-byte v5, v5, v7

    aput-byte v5, v2, v4

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->h:[B

    const/4 v7, 0x1

    aget-byte v5, v5, v7

    aput-byte v5, v2, v4

    const/4 v4, 0x2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->h:[B

    const/4 v7, 0x2

    aget-byte v5, v5, v7

    aput-byte v5, v2, v4

    const/4 v4, 0x3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->h:[B

    const/4 v7, 0x3

    aget-byte v5, v5, v7

    aput-byte v5, v2, v4

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v10

    goto/16 :goto_1

    .line 1735
    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 1736
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 1744
    :cond_7
    invoke-static {v3}, Lcom/google/android/exoplayer2/util/h;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1745
    const/16 v17, 0x2

    .line 1746
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->n:I

    if-nez v2, :cond_8

    .line 1747
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->l:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_b

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->j:I

    :goto_6
    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->l:I

    .line 1748
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->m:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_c

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->k:I

    :goto_7
    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->m:I

    .line 1750
    :cond_8
    const/high16 v12, -0x40800000    # -1.0f

    .line 1751
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->l:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_9

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->m:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_9

    .line 1752
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->k:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->l:I

    mul-int/2addr v2, v4

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->j:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->m:I

    mul-int/2addr v4, v5

    int-to-float v4, v4

    div-float v12, v2, v4

    .line 1754
    :cond_9
    const/4 v15, 0x0

    .line 1755
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->q:Z

    if-eqz v2, :cond_a

    .line 1756
    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer2/extractor/a/d$b;->a()[B

    move-result-object v2

    .line 1757
    new-instance v15, Lcom/google/android/exoplayer2/c/b;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->r:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->t:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->s:I

    invoke-direct {v15, v4, v5, v7, v2}, Lcom/google/android/exoplayer2/c/b;-><init>(III[B)V

    .line 1759
    :cond_a
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, -0x1

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->j:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->k:I

    const/high16 v9, -0x40800000    # -1.0f

    const/4 v11, -0x1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->o:[B

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->p:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->i:Lcom/google/android/exoplayer2/drm/a;

    move-object/from16 v16, v0

    invoke-static/range {v2 .. v16}, Lcom/google/android/exoplayer2/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIIFLjava/util/List;IF[BILcom/google/android/exoplayer2/c/b;Lcom/google/android/exoplayer2/drm/a;)Lcom/google/android/exoplayer2/j;

    move-result-object v2

    move/from16 v3, v17

    .line 1763
    goto/16 :goto_4

    .line 1747
    :cond_b
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->l:I

    goto :goto_6

    .line 1748
    :cond_c
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->m:I

    goto :goto_7

    .line 1763
    :cond_d
    const-string v2, "application/x-subrip"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 1764
    const/4 v4, 0x3

    .line 1765
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->P:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->i:Lcom/google/android/exoplayer2/drm/a;

    invoke-static {v2, v3, v12, v5, v6}, Lcom/google/android/exoplayer2/j;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/exoplayer2/drm/a;)Lcom/google/android/exoplayer2/j;

    move-result-object v2

    move v3, v4

    goto/16 :goto_4

    .line 1767
    :cond_e
    const-string v2, "text/x-ssa"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1768
    const/4 v4, 0x3

    .line 1769
    new-instance v18, Ljava/util/ArrayList;

    const/4 v2, 0x2

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 1770
    invoke-static {}, Lcom/google/android/exoplayer2/extractor/a/d;->a()[B

    move-result-object v2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1771
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->h:[B

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1772
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v10, 0x0

    const/4 v11, -0x1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->P:Ljava/lang/String;

    const/4 v14, -0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->i:Lcom/google/android/exoplayer2/drm/a;

    const-wide v16, 0x7fffffffffffffffL

    move-object v9, v3

    invoke-static/range {v8 .. v18}, Lcom/google/android/exoplayer2/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;ILcom/google/android/exoplayer2/drm/a;JLjava/util/List;)Lcom/google/android/exoplayer2/j;

    move-result-object v2

    move v3, v4

    goto/16 :goto_4

    .line 1775
    :cond_f
    const-string v2, "application/vobsub"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    const-string v2, "application/pgs"

    .line 1776
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    const-string v2, "application/dvbsubs"

    .line 1777
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 1778
    :cond_10
    const/4 v9, 0x3

    .line 1779
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, -0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->P:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->i:Lcom/google/android/exoplayer2/drm/a;

    move-object v6, v10

    invoke-static/range {v2 .. v8}, Lcom/google/android/exoplayer2/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/util/List;Ljava/lang/String;Lcom/google/android/exoplayer2/drm/a;)Lcom/google/android/exoplayer2/j;

    move-result-object v2

    move v3, v9

    goto/16 :goto_4

    .line 1782
    :cond_11
    new-instance v2, Lcom/google/android/exoplayer2/ParserException;

    const-string v3, "Unexpected MIME type."

    invoke-direct {v2, v3}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1594
    :sswitch_data_0
    .sparse-switch
        -0x7ce7f5de -> :sswitch_5
        -0x7ce7f3b0 -> :sswitch_3
        -0x76567dc0 -> :sswitch_16
        -0x6a615338 -> :sswitch_11
        -0x672350af -> :sswitch_a
        -0x585f4fce -> :sswitch_d
        -0x585f4fcd -> :sswitch_e
        -0x51dc40b2 -> :sswitch_8
        -0x37a9c464 -> :sswitch_1c
        -0x2016c535 -> :sswitch_4
        -0x2016c4e5 -> :sswitch_6
        -0x19552dbd -> :sswitch_1a
        -0x1538b2ba -> :sswitch_14
        0x3c02325 -> :sswitch_c
        0x3c02353 -> :sswitch_f
        0x3c030c5 -> :sswitch_12
        0x4e86155 -> :sswitch_0
        0x4e86156 -> :sswitch_1
        0x5e8da3e -> :sswitch_1b
        0x1a8350d6 -> :sswitch_9
        0x2056f406 -> :sswitch_13
        0x2b453ce4 -> :sswitch_17
        0x2c0618eb -> :sswitch_19
        0x32fdf009 -> :sswitch_7
        0x54c61e47 -> :sswitch_18
        0x6bd6c624 -> :sswitch_2
        0x7446132a -> :sswitch_10
        0x7446b0a6 -> :sswitch_15
        0x744ad97d -> :sswitch_b
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
    .end packed-switch
.end method
