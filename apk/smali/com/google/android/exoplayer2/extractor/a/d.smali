.class public final Lcom/google/android/exoplayer2/extractor/a/d;
.super Ljava/lang/Object;
.source "MatroskaExtractor.java"

# interfaces
.implements Lcom/google/android/exoplayer2/extractor/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/extractor/a/d$b;,
        Lcom/google/android/exoplayer2/extractor/a/d$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/google/android/exoplayer2/extractor/h;

.field private static final b:[B

.field private static final c:[B

.field private static d:J

.field private static final e:[B

.field private static final f:[B

.field private static g:J

.field private static final h:[B

.field private static final i:Ljava/util/UUID;


# instance fields
.field private A:J

.field private B:J

.field private C:Lcom/google/android/exoplayer2/extractor/a/d$b;

.field private D:Z

.field private E:I

.field private F:J

.field private G:Z

.field private H:J

.field private I:J

.field private J:J

.field private K:Lcom/google/android/exoplayer2/util/f;

.field private L:Lcom/google/android/exoplayer2/util/f;

.field private M:Z

.field private N:I

.field private O:J

.field private P:J

.field private Q:I

.field private R:I

.field private S:[I

.field private T:I

.field private U:I

.field private V:I

.field private W:I

.field private X:Z

.field private Y:Z

.field private Z:Z

.field private aa:Z

.field private ab:B

.field private ac:I

.field private ad:I

.field private ae:I

.field private af:Z

.field private ag:Z

.field private ah:Lcom/google/android/exoplayer2/extractor/g;

.field private final j:Lcom/google/android/exoplayer2/extractor/a/b;

.field private final k:Lcom/google/android/exoplayer2/extractor/a/f;

.field private final l:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/exoplayer2/extractor/a/d$b;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Z

.field private final n:Lcom/google/android/exoplayer2/util/k;

.field private final o:Lcom/google/android/exoplayer2/util/k;

.field private final p:Lcom/google/android/exoplayer2/util/k;

.field private final q:Lcom/google/android/exoplayer2/util/k;

.field private final r:Lcom/google/android/exoplayer2/util/k;

.field private final s:Lcom/google/android/exoplayer2/util/k;

.field private final t:Lcom/google/android/exoplayer2/util/k;

.field private final u:Lcom/google/android/exoplayer2/util/k;

.field private final v:Lcom/google/android/exoplayer2/util/k;

.field private w:Ljava/nio/ByteBuffer;

.field private x:J

.field private y:J

.field private z:J


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v2, 0x20

    .line 63
    new-instance v0, Lcom/google/android/exoplayer2/extractor/a/d$1;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/extractor/a/d$1;-><init>()V

    sput-object v0, Lcom/google/android/exoplayer2/extractor/a/d;->a:Lcom/google/android/exoplayer2/extractor/h;

    .line 227
    new-array v0, v2, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/exoplayer2/extractor/a/d;->b:[B

    .line 239
    const/16 v0, 0xc

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/exoplayer2/extractor/a/d;->c:[B

    .line 245
    const-wide/16 v0, 0x3e8

    sput-wide v0, Lcom/google/android/exoplayer2/extractor/a/d;->d:J

    .line 254
    const-string v0, "Format: Start, End, ReadOrder, Layer, Style, Name, MarginL, MarginR, MarginV, Effect, Text"

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/s;->c(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/extractor/a/d;->e:[B

    .line 263
    new-array v0, v2, [B

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/exoplayer2/extractor/a/d;->f:[B

    .line 273
    const-wide/16 v0, 0x2710

    sput-wide v0, Lcom/google/android/exoplayer2/extractor/a/d;->g:J

    .line 280
    const/16 v0, 0xa

    new-array v0, v0, [B

    fill-array-data v0, :array_3

    sput-object v0, Lcom/google/android/exoplayer2/extractor/a/d;->h:[B

    .line 302
    new-instance v0, Ljava/util/UUID;

    const-wide v2, 0x100000000001000L

    const-wide v4, -0x7fffff55ffc7648fL    # -3.607411173533E-312

    invoke-direct {v0, v2, v3, v4, v5}, Ljava/util/UUID;-><init>(JJ)V

    sput-object v0, Lcom/google/android/exoplayer2/extractor/a/d;->i:Ljava/util/UUID;

    return-void

    .line 227
    nop

    :array_0
    .array-data 1
        0x31t
        0xat
        0x30t
        0x30t
        0x3at
        0x30t
        0x30t
        0x3at
        0x30t
        0x30t
        0x2ct
        0x30t
        0x30t
        0x30t
        0x20t
        0x2dt
        0x2dt
        0x3et
        0x20t
        0x30t
        0x30t
        0x3at
        0x30t
        0x30t
        0x3at
        0x30t
        0x30t
        0x2ct
        0x30t
        0x30t
        0x30t
        0xat
    .end array-data

    .line 239
    :array_1
    .array-data 1
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
    .end array-data

    .line 263
    :array_2
    .array-data 1
        0x44t
        0x69t
        0x61t
        0x6ct
        0x6ft
        0x67t
        0x75t
        0x65t
        0x3at
        0x20t
        0x30t
        0x3at
        0x30t
        0x30t
        0x3at
        0x30t
        0x30t
        0x3at
        0x30t
        0x30t
        0x2ct
        0x30t
        0x3at
        0x30t
        0x30t
        0x3at
        0x30t
        0x30t
        0x3at
        0x30t
        0x30t
        0x2ct
    .end array-data

    .line 280
    :array_3
    .array-data 1
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 375
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/extractor/a/d;-><init>(I)V

    .line 376
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 379
    new-instance v0, Lcom/google/android/exoplayer2/extractor/a/a;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/extractor/a/a;-><init>()V

    invoke-direct {p0, v0, p1}, Lcom/google/android/exoplayer2/extractor/a/d;-><init>(Lcom/google/android/exoplayer2/extractor/a/b;I)V

    .line 380
    return-void
.end method

.method constructor <init>(Lcom/google/android/exoplayer2/extractor/a/b;I)V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v3, 0x4

    .line 382
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 322
    iput-wide v4, p0, Lcom/google/android/exoplayer2/extractor/a/d;->y:J

    .line 323
    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->z:J

    .line 324
    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->A:J

    .line 325
    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->B:J

    .line 339
    iput-wide v4, p0, Lcom/google/android/exoplayer2/extractor/a/d;->H:J

    .line 340
    iput-wide v4, p0, Lcom/google/android/exoplayer2/extractor/a/d;->I:J

    .line 341
    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->J:J

    .line 383
    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->j:Lcom/google/android/exoplayer2/extractor/a/b;

    .line 384
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->j:Lcom/google/android/exoplayer2/extractor/a/b;

    new-instance v1, Lcom/google/android/exoplayer2/extractor/a/d$a;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/exoplayer2/extractor/a/d$a;-><init>(Lcom/google/android/exoplayer2/extractor/a/d;Lcom/google/android/exoplayer2/extractor/a/d$1;)V

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/extractor/a/b;->a(Lcom/google/android/exoplayer2/extractor/a/c;)V

    .line 385
    and-int/lit8 v0, p2, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->m:Z

    .line 386
    new-instance v0, Lcom/google/android/exoplayer2/extractor/a/f;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/extractor/a/f;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->k:Lcom/google/android/exoplayer2/extractor/a/f;

    .line 387
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->l:Landroid/util/SparseArray;

    .line 388
    new-instance v0, Lcom/google/android/exoplayer2/util/k;

    invoke-direct {v0, v3}, Lcom/google/android/exoplayer2/util/k;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    .line 389
    new-instance v0, Lcom/google/android/exoplayer2/util/k;

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/util/k;-><init>([B)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->q:Lcom/google/android/exoplayer2/util/k;

    .line 390
    new-instance v0, Lcom/google/android/exoplayer2/util/k;

    invoke-direct {v0, v3}, Lcom/google/android/exoplayer2/util/k;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->r:Lcom/google/android/exoplayer2/util/k;

    .line 391
    new-instance v0, Lcom/google/android/exoplayer2/util/k;

    sget-object v1, Lcom/google/android/exoplayer2/util/i;->a:[B

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/util/k;-><init>([B)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->n:Lcom/google/android/exoplayer2/util/k;

    .line 392
    new-instance v0, Lcom/google/android/exoplayer2/util/k;

    invoke-direct {v0, v3}, Lcom/google/android/exoplayer2/util/k;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->o:Lcom/google/android/exoplayer2/util/k;

    .line 393
    new-instance v0, Lcom/google/android/exoplayer2/util/k;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/util/k;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->s:Lcom/google/android/exoplayer2/util/k;

    .line 394
    new-instance v0, Lcom/google/android/exoplayer2/util/k;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/util/k;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->t:Lcom/google/android/exoplayer2/util/k;

    .line 395
    new-instance v0, Lcom/google/android/exoplayer2/util/k;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/util/k;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->u:Lcom/google/android/exoplayer2/util/k;

    .line 396
    new-instance v0, Lcom/google/android/exoplayer2/util/k;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/util/k;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->v:Lcom/google/android/exoplayer2/util/k;

    .line 397
    return-void

    .line 385
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/android/exoplayer2/extractor/f;Lcom/google/android/exoplayer2/extractor/m;I)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 1339
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->s:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/k;->b()I

    move-result v0

    .line 1340
    if-lez v0, :cond_0

    .line 1341
    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1342
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->s:Lcom/google/android/exoplayer2/util/k;

    invoke-interface {p2, v1, v0}, Lcom/google/android/exoplayer2/extractor/m;->a(Lcom/google/android/exoplayer2/util/k;I)V

    .line 1346
    :goto_0
    iget v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->W:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->W:I

    .line 1347
    iget v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ae:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ae:I

    .line 1348
    return v0

    .line 1344
    :cond_0
    const/4 v0, 0x0

    invoke-interface {p2, p1, p3, v0}, Lcom/google/android/exoplayer2/extractor/m;->a(Lcom/google/android/exoplayer2/extractor/f;IZ)I

    move-result v0

    goto :goto_0
.end method

.method private a(J)J
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    .line 1414
    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->z:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1415
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Can\'t scale timecode prior to timecodeScale being set."

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1417
    :cond_0
    iget-wide v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->z:J

    const-wide/16 v4, 0x3e8

    move-wide v0, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/exoplayer2/util/s;->b(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method private a(Lcom/google/android/exoplayer2/extractor/a/d$b;J)V
    .locals 8

    .prologue
    .line 1080
    const-string v0, "S_TEXT/UTF8"

    iget-object v1, p1, Lcom/google/android/exoplayer2/extractor/a/d$b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1081
    const-string v2, "%02d:%02d:%02d,%03d"

    const/16 v3, 0x13

    sget-wide v4, Lcom/google/android/exoplayer2/extractor/a/d;->d:J

    sget-object v6, Lcom/google/android/exoplayer2/extractor/a/d;->c:[B

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer2/extractor/a/d;->a(Lcom/google/android/exoplayer2/extractor/a/d$b;Ljava/lang/String;IJ[B)V

    .line 1087
    :cond_0
    :goto_0
    iget-object v1, p1, Lcom/google/android/exoplayer2/extractor/a/d$b;->N:Lcom/google/android/exoplayer2/extractor/m;

    iget v4, p0, Lcom/google/android/exoplayer2/extractor/a/d;->V:I

    iget v5, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ae:I

    const/4 v6, 0x0

    iget-object v7, p1, Lcom/google/android/exoplayer2/extractor/a/d$b;->g:Lcom/google/android/exoplayer2/extractor/m$a;

    move-wide v2, p2

    invoke-interface/range {v1 .. v7}, Lcom/google/android/exoplayer2/extractor/m;->a(JIIILcom/google/android/exoplayer2/extractor/m$a;)V

    .line 1088
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->af:Z

    .line 1089
    invoke-direct {p0}, Lcom/google/android/exoplayer2/extractor/a/d;->d()V

    .line 1090
    return-void

    .line 1083
    :cond_1
    const-string v0, "S_TEXT/ASS"

    iget-object v1, p1, Lcom/google/android/exoplayer2/extractor/a/d$b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1084
    const-string v2, "%01d:%02d:%02d:%02d"

    const/16 v3, 0x15

    sget-wide v4, Lcom/google/android/exoplayer2/extractor/a/d;->g:J

    sget-object v6, Lcom/google/android/exoplayer2/extractor/a/d;->h:[B

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer2/extractor/a/d;->a(Lcom/google/android/exoplayer2/extractor/a/d$b;Ljava/lang/String;IJ[B)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/exoplayer2/extractor/a/d$b;Ljava/lang/String;IJ[B)V
    .locals 10

    .prologue
    .line 1290
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->t:Lcom/google/android/exoplayer2/util/k;

    iget-object v1, v0, Lcom/google/android/exoplayer2/util/k;->a:[B

    iget-wide v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->P:J

    move-object v4, p2

    move v5, p3

    move-wide v6, p4

    move-object/from16 v8, p6

    invoke-static/range {v1 .. v8}, Lcom/google/android/exoplayer2/extractor/a/d;->a([BJLjava/lang/String;IJ[B)V

    .line 1294
    iget-object v0, p1, Lcom/google/android/exoplayer2/extractor/a/d$b;->N:Lcom/google/android/exoplayer2/extractor/m;

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->t:Lcom/google/android/exoplayer2/util/k;

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->t:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/util/k;->c()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/exoplayer2/extractor/m;->a(Lcom/google/android/exoplayer2/util/k;I)V

    .line 1295
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ae:I

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->t:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/k;->c()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ae:I

    .line 1296
    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/extractor/f;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 1111
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/k;->c()I

    move-result v0

    if-lt v0, p2, :cond_0

    .line 1120
    :goto_0
    return-void

    .line 1114
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/k;->e()I

    move-result v0

    if-ge v0, p2, :cond_1

    .line 1115
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    iget-object v1, v1, Lcom/google/android/exoplayer2/util/k;->a:[B

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    iget-object v2, v2, Lcom/google/android/exoplayer2/util/k;->a:[B

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v2, p2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    .line 1116
    invoke-virtual {v2}, Lcom/google/android/exoplayer2/util/k;->c()I

    move-result v2

    .line 1115
    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/util/k;->a([BI)V

    .line 1118
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    iget-object v0, v0, Lcom/google/android/exoplayer2/util/k;->a:[B

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/k;->c()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/util/k;->c()I

    move-result v2

    sub-int v2, p2, v2

    invoke-interface {p1, v0, v1, v2}, Lcom/google/android/exoplayer2/extractor/f;->b([BII)V

    .line 1119
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v0, p2}, Lcom/google/android/exoplayer2/util/k;->b(I)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/exoplayer2/extractor/f;Lcom/google/android/exoplayer2/extractor/a/d$b;I)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x8

    const/4 v9, 0x4

    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1124
    const-string v0, "S_TEXT/UTF8"

    iget-object v3, p2, Lcom/google/android/exoplayer2/extractor/a/d$b;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1125
    sget-object v0, Lcom/google/android/exoplayer2/extractor/a/d;->b:[B

    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/exoplayer2/extractor/a/d;->a(Lcom/google/android/exoplayer2/extractor/f;[BI)V

    .line 1270
    :cond_0
    :goto_0
    return-void

    .line 1127
    :cond_1
    const-string v0, "S_TEXT/ASS"

    iget-object v3, p2, Lcom/google/android/exoplayer2/extractor/a/d$b;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1128
    sget-object v0, Lcom/google/android/exoplayer2/extractor/a/d;->f:[B

    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/exoplayer2/extractor/a/d;->a(Lcom/google/android/exoplayer2/extractor/f;[BI)V

    goto :goto_0

    .line 1132
    :cond_2
    iget-object v5, p2, Lcom/google/android/exoplayer2/extractor/a/d$b;->N:Lcom/google/android/exoplayer2/extractor/m;

    .line 1133
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->X:Z

    if-nez v0, :cond_f

    .line 1134
    iget-boolean v0, p2, Lcom/google/android/exoplayer2/extractor/a/d$b;->e:Z

    if-eqz v0, :cond_12

    .line 1137
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->V:I

    const v3, -0x40000001    # -1.9999999f

    and-int/2addr v0, v3

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->V:I

    .line 1138
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->Y:Z

    if-nez v0, :cond_4

    .line 1139
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    iget-object v0, v0, Lcom/google/android/exoplayer2/util/k;->a:[B

    invoke-interface {p1, v0, v2, v1}, Lcom/google/android/exoplayer2/extractor/f;->b([BII)V

    .line 1140
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->W:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->W:I

    .line 1141
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    iget-object v0, v0, Lcom/google/android/exoplayer2/util/k;->a:[B

    aget-byte v0, v0, v2

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_3

    .line 1142
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Extension bit is set in signal byte"

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1144
    :cond_3
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    iget-object v0, v0, Lcom/google/android/exoplayer2/util/k;->a:[B

    aget-byte v0, v0, v2

    iput-byte v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ab:B

    .line 1145
    iput-boolean v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->Y:Z

    .line 1147
    :cond_4
    iget-byte v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ab:B

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_9

    move v0, v1

    .line 1148
    :goto_1
    if-eqz v0, :cond_e

    .line 1149
    iget-byte v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ab:B

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v8, :cond_a

    move v0, v1

    .line 1150
    :goto_2
    iget v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->V:I

    const/high16 v4, 0x40000000    # 2.0f

    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->V:I

    .line 1151
    iget-boolean v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->Z:Z

    if-nez v3, :cond_5

    .line 1152
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->u:Lcom/google/android/exoplayer2/util/k;

    iget-object v3, v3, Lcom/google/android/exoplayer2/util/k;->a:[B

    invoke-interface {p1, v3, v2, v6}, Lcom/google/android/exoplayer2/extractor/f;->b([BII)V

    .line 1153
    iget v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->W:I

    add-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->W:I

    .line 1154
    iput-boolean v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->Z:Z

    .line 1156
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    iget-object v4, v3, Lcom/google/android/exoplayer2/util/k;->a:[B

    if-eqz v0, :cond_b

    const/16 v3, 0x80

    :goto_3
    or-int/lit8 v3, v3, 0x8

    int-to-byte v3, v3

    aput-byte v3, v4, v2

    .line 1157
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v3, v2}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 1158
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    invoke-interface {v5, v3, v1}, Lcom/google/android/exoplayer2/extractor/m;->a(Lcom/google/android/exoplayer2/util/k;I)V

    .line 1159
    iget v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ae:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ae:I

    .line 1161
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->u:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v3, v2}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 1162
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->u:Lcom/google/android/exoplayer2/util/k;

    invoke-interface {v5, v3, v6}, Lcom/google/android/exoplayer2/extractor/m;->a(Lcom/google/android/exoplayer2/util/k;I)V

    .line 1163
    iget v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ae:I

    add-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ae:I

    .line 1165
    :cond_5
    if-eqz v0, :cond_e

    .line 1166
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->aa:Z

    if-nez v0, :cond_6

    .line 1167
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    iget-object v0, v0, Lcom/google/android/exoplayer2/util/k;->a:[B

    invoke-interface {p1, v0, v2, v1}, Lcom/google/android/exoplayer2/extractor/f;->b([BII)V

    .line 1168
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->W:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->W:I

    .line 1169
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 1170
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/k;->g()I

    move-result v0

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ac:I

    .line 1171
    iput-boolean v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->aa:Z

    .line 1173
    :cond_6
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ac:I

    mul-int/lit8 v0, v0, 0x4

    .line 1174
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v3, v0}, Lcom/google/android/exoplayer2/util/k;->a(I)V

    .line 1175
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    iget-object v3, v3, Lcom/google/android/exoplayer2/util/k;->a:[B

    invoke-interface {p1, v3, v2, v0}, Lcom/google/android/exoplayer2/extractor/f;->b([BII)V

    .line 1176
    iget v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->W:I

    add-int/2addr v0, v3

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->W:I

    .line 1177
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ac:I

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1

    int-to-short v0, v0

    .line 1178
    mul-int/lit8 v3, v0, 0x6

    add-int/lit8 v6, v3, 0x2

    .line 1179
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->w:Ljava/nio/ByteBuffer;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->w:Ljava/nio/ByteBuffer;

    .line 1180
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v3

    if-ge v3, v6, :cond_8

    .line 1181
    :cond_7
    invoke-static {v6}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->w:Ljava/nio/ByteBuffer;

    .line 1183
    :cond_8
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->w:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1184
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->w:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    move v0, v2

    move v3, v2

    .line 1192
    :goto_4
    iget v4, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ac:I

    if-ge v0, v4, :cond_d

    .line 1194
    iget-object v4, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v4

    .line 1195
    rem-int/lit8 v7, v0, 0x2

    if-nez v7, :cond_c

    .line 1196
    iget-object v7, p0, Lcom/google/android/exoplayer2/extractor/a/d;->w:Ljava/nio/ByteBuffer;

    sub-int v3, v4, v3

    int-to-short v3, v3

    invoke-virtual {v7, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1192
    :goto_5
    add-int/lit8 v0, v0, 0x1

    move v3, v4

    goto :goto_4

    :cond_9
    move v0, v2

    .line 1147
    goto/16 :goto_1

    :cond_a
    move v0, v2

    .line 1149
    goto/16 :goto_2

    :cond_b
    move v3, v2

    .line 1156
    goto/16 :goto_3

    .line 1199
    :cond_c
    iget-object v7, p0, Lcom/google/android/exoplayer2/extractor/a/d;->w:Ljava/nio/ByteBuffer;

    sub-int v3, v4, v3

    invoke-virtual {v7, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_5

    .line 1202
    :cond_d
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->W:I

    sub-int v0, p3, v0

    sub-int/2addr v0, v3

    .line 1203
    iget v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ac:I

    rem-int/lit8 v3, v3, 0x2

    if-ne v3, v1, :cond_11

    .line 1204
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->w:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1209
    :goto_6
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->v:Lcom/google/android/exoplayer2/util/k;

    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->w:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    invoke-virtual {v0, v3, v6}, Lcom/google/android/exoplayer2/util/k;->a([BI)V

    .line 1210
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->v:Lcom/google/android/exoplayer2/util/k;

    invoke-interface {v5, v0, v6}, Lcom/google/android/exoplayer2/extractor/m;->a(Lcom/google/android/exoplayer2/util/k;I)V

    .line 1211
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ae:I

    add-int/2addr v0, v6

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ae:I

    .line 1218
    :cond_e
    :goto_7
    iput-boolean v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->X:Z

    .line 1220
    :cond_f
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->s:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/k;->c()I

    move-result v0

    add-int/2addr v0, p3

    .line 1222
    const-string v3, "V_MPEG4/ISO/AVC"

    iget-object v4, p2, Lcom/google/android/exoplayer2/extractor/a/d$b;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_10

    const-string v3, "V_MPEGH/ISO/HEVC"

    iget-object v4, p2, Lcom/google/android/exoplayer2/extractor/a/d$b;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 1227
    :cond_10
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->o:Lcom/google/android/exoplayer2/util/k;

    iget-object v3, v3, Lcom/google/android/exoplayer2/util/k;->a:[B

    .line 1228
    aput-byte v2, v3, v2

    .line 1229
    aput-byte v2, v3, v1

    .line 1230
    aput-byte v2, v3, v8

    .line 1231
    iget v1, p2, Lcom/google/android/exoplayer2/extractor/a/d$b;->O:I

    .line 1232
    iget v4, p2, Lcom/google/android/exoplayer2/extractor/a/d$b;->O:I

    rsub-int/lit8 v4, v4, 0x4

    .line 1236
    :goto_8
    iget v6, p0, Lcom/google/android/exoplayer2/extractor/a/d;->W:I

    if-ge v6, v0, :cond_15

    .line 1237
    iget v6, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ad:I

    if-nez v6, :cond_13

    .line 1239
    invoke-direct {p0, p1, v3, v4, v1}, Lcom/google/android/exoplayer2/extractor/a/d;->a(Lcom/google/android/exoplayer2/extractor/f;[BII)V

    .line 1241
    iget-object v6, p0, Lcom/google/android/exoplayer2/extractor/a/d;->o:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v6, v2}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 1242
    iget-object v6, p0, Lcom/google/android/exoplayer2/extractor/a/d;->o:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v6}, Lcom/google/android/exoplayer2/util/k;->t()I

    move-result v6

    iput v6, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ad:I

    .line 1244
    iget-object v6, p0, Lcom/google/android/exoplayer2/extractor/a/d;->n:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v6, v2}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 1245
    iget-object v6, p0, Lcom/google/android/exoplayer2/extractor/a/d;->n:Lcom/google/android/exoplayer2/util/k;

    invoke-interface {v5, v6, v9}, Lcom/google/android/exoplayer2/extractor/m;->a(Lcom/google/android/exoplayer2/util/k;I)V

    .line 1246
    iget v6, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ae:I

    add-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ae:I

    goto :goto_8

    .line 1206
    :cond_11
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->w:Ljava/nio/ByteBuffer;

    int-to-short v0, v0

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1207
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->w:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_6

    .line 1214
    :cond_12
    iget-object v0, p2, Lcom/google/android/exoplayer2/extractor/a/d$b;->f:[B

    if-eqz v0, :cond_e

    .line 1216
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->s:Lcom/google/android/exoplayer2/util/k;

    iget-object v3, p2, Lcom/google/android/exoplayer2/extractor/a/d$b;->f:[B

    iget-object v4, p2, Lcom/google/android/exoplayer2/extractor/a/d$b;->f:[B

    array-length v4, v4

    invoke-virtual {v0, v3, v4}, Lcom/google/android/exoplayer2/util/k;->a([BI)V

    goto :goto_7

    .line 1249
    :cond_13
    iget v6, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ad:I

    iget v7, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ad:I

    .line 1250
    invoke-direct {p0, p1, v5, v7}, Lcom/google/android/exoplayer2/extractor/a/d;->a(Lcom/google/android/exoplayer2/extractor/f;Lcom/google/android/exoplayer2/extractor/m;I)I

    move-result v7

    sub-int/2addr v6, v7

    iput v6, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ad:I

    goto :goto_8

    .line 1254
    :cond_14
    :goto_9
    iget v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->W:I

    if-ge v1, v0, :cond_15

    .line 1255
    iget v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->W:I

    sub-int v1, v0, v1

    invoke-direct {p0, p1, v5, v1}, Lcom/google/android/exoplayer2/extractor/a/d;->a(Lcom/google/android/exoplayer2/extractor/f;Lcom/google/android/exoplayer2/extractor/m;I)I

    goto :goto_9

    .line 1259
    :cond_15
    const-string v0, "A_VORBIS"

    iget-object v1, p2, Lcom/google/android/exoplayer2/extractor/a/d$b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1266
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->q:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 1267
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->q:Lcom/google/android/exoplayer2/util/k;

    invoke-interface {v5, v0, v9}, Lcom/google/android/exoplayer2/extractor/m;->a(Lcom/google/android/exoplayer2/util/k;I)V

    .line 1268
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ae:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ae:I

    goto/16 :goto_0
.end method

.method private a(Lcom/google/android/exoplayer2/extractor/f;[BI)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1274
    array-length v0, p2

    add-int/2addr v0, p3

    .line 1275
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->t:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/k;->e()I

    move-result v1

    if-ge v1, v0, :cond_0

    .line 1278
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->t:Lcom/google/android/exoplayer2/util/k;

    add-int v2, v0, p3

    invoke-static {p2, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/exoplayer2/util/k;->a:[B

    .line 1282
    :goto_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->t:Lcom/google/android/exoplayer2/util/k;

    iget-object v1, v1, Lcom/google/android/exoplayer2/util/k;->a:[B

    array-length v2, p2

    invoke-interface {p1, v1, v2, p3}, Lcom/google/android/exoplayer2/extractor/f;->b([BII)V

    .line 1283
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->t:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v1, v0}, Lcom/google/android/exoplayer2/util/k;->a(I)V

    .line 1286
    return-void

    .line 1280
    :cond_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->t:Lcom/google/android/exoplayer2/util/k;

    iget-object v1, v1, Lcom/google/android/exoplayer2/util/k;->a:[B

    array-length v2, p2

    invoke-static {p2, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/exoplayer2/extractor/f;[BII)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 1324
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->s:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/k;->b()I

    move-result v0

    invoke-static {p4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1325
    add-int v1, p3, v0

    sub-int v2, p4, v0

    invoke-interface {p1, p2, v1, v2}, Lcom/google/android/exoplayer2/extractor/f;->b([BII)V

    .line 1326
    if-lez v0, :cond_0

    .line 1327
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->s:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v1, p2, p3, v0}, Lcom/google/android/exoplayer2/util/k;->a([BII)V

    .line 1329
    :cond_0
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->W:I

    add-int/2addr v0, p4

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->W:I

    .line 1330
    return-void
.end method

.method private static a([BJLjava/lang/String;IJ[B)V
    .locals 13

    .prologue
    .line 1302
    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v2, p1, v2

    if-nez v2, :cond_0

    move-object/from16 v2, p7

    .line 1315
    :goto_0
    const/4 v3, 0x0

    move-object/from16 v0, p7

    array-length v4, v0

    move/from16 v0, p4

    invoke-static {v2, v3, p0, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1316
    return-void

    .line 1305
    :cond_0
    const-wide v2, 0xd693a400L

    div-long v2, p1, v2

    long-to-int v2, v2

    .line 1306
    mul-int/lit16 v3, v2, 0xe10

    int-to-long v4, v3

    const-wide/32 v6, 0xf4240

    mul-long/2addr v4, v6

    sub-long v4, p1, v4

    .line 1307
    const-wide/32 v6, 0x3938700

    div-long v6, v4, v6

    long-to-int v3, v6

    .line 1308
    mul-int/lit8 v6, v3, 0x3c

    int-to-long v6, v6

    const-wide/32 v8, 0xf4240

    mul-long/2addr v6, v8

    sub-long/2addr v4, v6

    .line 1309
    const-wide/32 v6, 0xf4240

    div-long v6, v4, v6

    long-to-int v6, v6

    .line 1310
    int-to-long v8, v6

    const-wide/32 v10, 0xf4240

    mul-long/2addr v8, v10

    sub-long/2addr v4, v8

    .line 1311
    div-long v4, v4, p5

    long-to-int v4, v4

    .line 1312
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v7, v8

    const/4 v2, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v7, v2

    const/4 v2, 0x2

    .line 1313
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v7, v2

    const/4 v2, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v7, v2

    .line 1312
    move-object/from16 v0, p3

    invoke-static {v5, v0, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/exoplayer2/util/s;->c(Ljava/lang/String;)[B

    move-result-object v2

    goto :goto_0
.end method

.method private a(Lcom/google/android/exoplayer2/extractor/k;J)Z
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1397
    iget-boolean v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->G:Z

    if-eqz v2, :cond_0

    .line 1398
    iput-wide p2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->I:J

    .line 1399
    iget-wide v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->H:J

    iput-wide v2, p1, Lcom/google/android/exoplayer2/extractor/k;->a:J

    .line 1400
    iput-boolean v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->G:Z

    .line 1410
    :goto_0
    return v0

    .line 1405
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->D:Z

    if-eqz v2, :cond_1

    iget-wide v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->I:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 1406
    iget-wide v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->I:J

    iput-wide v2, p1, Lcom/google/android/exoplayer2/extractor/k;->a:J

    .line 1407
    iput-wide v4, p0, Lcom/google/android/exoplayer2/extractor/a/d;->I:J

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1410
    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1421
    const-string v0, "V_VP8"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_VP9"

    .line 1422
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_MPEG2"

    .line 1423
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_MPEG4/ISO/SP"

    .line 1424
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_MPEG4/ISO/ASP"

    .line 1425
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_MPEG4/ISO/AP"

    .line 1426
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_MPEG4/ISO/AVC"

    .line 1427
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_MPEGH/ISO/HEVC"

    .line 1428
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_MS/VFW/FOURCC"

    .line 1429
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_THEORA"

    .line 1430
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_OPUS"

    .line 1431
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_VORBIS"

    .line 1432
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_AAC"

    .line 1433
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_MPEG/L2"

    .line 1434
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_MPEG/L3"

    .line 1435
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_AC3"

    .line 1436
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_EAC3"

    .line 1437
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_TRUEHD"

    .line 1438
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_DTS"

    .line 1439
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_DTS/EXPRESS"

    .line 1440
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_DTS/LOSSLESS"

    .line 1441
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_FLAC"

    .line 1442
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_MS/ACM"

    .line 1443
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_PCM/INT/LIT"

    .line 1444
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "S_TEXT/UTF8"

    .line 1445
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "S_TEXT/ASS"

    .line 1446
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "S_VOBSUB"

    .line 1447
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "S_HDMV/PGS"

    .line 1448
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "S_DVBSUB"

    .line 1449
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a()[B
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/google/android/exoplayer2/extractor/a/d;->e:[B

    return-object v0
.end method

.method private static a([II)[I
    .locals 1

    .prologue
    .line 1457
    if-nez p0, :cond_1

    .line 1458
    new-array p0, p1, [I

    .line 1463
    :cond_0
    :goto_0
    return-object p0

    .line 1459
    :cond_1
    array-length v0, p0

    if-ge v0, p1, :cond_0

    .line 1463
    array-length v0, p0

    mul-int/lit8 v0, v0, 0x2

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    new-array p0, v0, [I

    goto :goto_0
.end method

.method static synthetic b()Ljava/util/UUID;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/google/android/exoplayer2/extractor/a/d;->i:Ljava/util/UUID;

    return-object v0
.end method

.method private d()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1093
    iput v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->W:I

    .line 1094
    iput v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ae:I

    .line 1095
    iput v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ad:I

    .line 1096
    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->X:Z

    .line 1097
    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->Y:Z

    .line 1098
    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->aa:Z

    .line 1099
    iput v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ac:I

    .line 1100
    iput-byte v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ab:B

    .line 1101
    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->Z:Z

    .line 1102
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->s:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/k;->a()V

    .line 1103
    return-void
.end method

.method private e()Lcom/google/android/exoplayer2/extractor/l;
    .locals 13

    .prologue
    const/4 v0, 0x0

    const/4 v12, 0x0

    .line 1358
    iget-wide v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->y:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->B:J

    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->K:Lcom/google/android/exoplayer2/util/f;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->K:Lcom/google/android/exoplayer2/util/f;

    .line 1359
    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/f;->a()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->L:Lcom/google/android/exoplayer2/util/f;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->L:Lcom/google/android/exoplayer2/util/f;

    .line 1360
    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/f;->a()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->K:Lcom/google/android/exoplayer2/util/f;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/util/f;->a()I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 1362
    :cond_0
    iput-object v12, p0, Lcom/google/android/exoplayer2/extractor/a/d;->K:Lcom/google/android/exoplayer2/util/f;

    .line 1363
    iput-object v12, p0, Lcom/google/android/exoplayer2/extractor/a/d;->L:Lcom/google/android/exoplayer2/util/f;

    .line 1364
    new-instance v0, Lcom/google/android/exoplayer2/extractor/l$a;

    iget-wide v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->B:J

    invoke-direct {v0, v2, v3}, Lcom/google/android/exoplayer2/extractor/l$a;-><init>(J)V

    .line 1384
    :goto_0
    return-object v0

    .line 1366
    :cond_1
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->K:Lcom/google/android/exoplayer2/util/f;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/util/f;->a()I

    move-result v2

    .line 1367
    new-array v3, v2, [I

    .line 1368
    new-array v4, v2, [J

    .line 1369
    new-array v5, v2, [J

    .line 1370
    new-array v6, v2, [J

    move v1, v0

    .line 1371
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1372
    iget-object v7, p0, Lcom/google/android/exoplayer2/extractor/a/d;->K:Lcom/google/android/exoplayer2/util/f;

    invoke-virtual {v7, v1}, Lcom/google/android/exoplayer2/util/f;->a(I)J

    move-result-wide v8

    aput-wide v8, v6, v1

    .line 1373
    iget-wide v8, p0, Lcom/google/android/exoplayer2/extractor/a/d;->y:J

    iget-object v7, p0, Lcom/google/android/exoplayer2/extractor/a/d;->L:Lcom/google/android/exoplayer2/util/f;

    invoke-virtual {v7, v1}, Lcom/google/android/exoplayer2/util/f;->a(I)J

    move-result-wide v10

    add-long/2addr v8, v10

    aput-wide v8, v4, v1

    .line 1371
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1375
    :cond_2
    :goto_2
    add-int/lit8 v1, v2, -0x1

    if-ge v0, v1, :cond_3

    .line 1376
    add-int/lit8 v1, v0, 0x1

    aget-wide v8, v4, v1

    aget-wide v10, v4, v0

    sub-long/2addr v8, v10

    long-to-int v1, v8

    aput v1, v3, v0

    .line 1377
    add-int/lit8 v1, v0, 0x1

    aget-wide v8, v6, v1

    aget-wide v10, v6, v0

    sub-long/2addr v8, v10

    aput-wide v8, v5, v0

    .line 1375
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1379
    :cond_3
    add-int/lit8 v0, v2, -0x1

    iget-wide v8, p0, Lcom/google/android/exoplayer2/extractor/a/d;->y:J

    iget-wide v10, p0, Lcom/google/android/exoplayer2/extractor/a/d;->x:J

    add-long/2addr v8, v10

    add-int/lit8 v1, v2, -0x1

    aget-wide v10, v4, v1

    sub-long/2addr v8, v10

    long-to-int v1, v8

    aput v1, v3, v0

    .line 1381
    add-int/lit8 v0, v2, -0x1

    iget-wide v8, p0, Lcom/google/android/exoplayer2/extractor/a/d;->B:J

    add-int/lit8 v1, v2, -0x1

    aget-wide v10, v6, v1

    sub-long/2addr v8, v10

    aput-wide v8, v5, v0

    .line 1382
    iput-object v12, p0, Lcom/google/android/exoplayer2/extractor/a/d;->K:Lcom/google/android/exoplayer2/util/f;

    .line 1383
    iput-object v12, p0, Lcom/google/android/exoplayer2/extractor/a/d;->L:Lcom/google/android/exoplayer2/util/f;

    .line 1384
    new-instance v0, Lcom/google/android/exoplayer2/extractor/a;

    invoke-direct {v0, v3, v4, v5, v6}, Lcom/google/android/exoplayer2/extractor/a;-><init>([I[J[J[J)V

    goto :goto_0
.end method


# virtual methods
.method a(I)I
    .locals 1

    .prologue
    .line 438
    sparse-switch p1, :sswitch_data_0

    .line 523
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 461
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 496
    :sswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 500
    :sswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 508
    :sswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 521
    :sswitch_4
    const/4 v0, 0x5

    goto :goto_0

    .line 438
    nop

    :sswitch_data_0
    .sparse-switch
        0x83 -> :sswitch_1
        0x86 -> :sswitch_2
        0x88 -> :sswitch_1
        0x9b -> :sswitch_1
        0x9f -> :sswitch_1
        0xa0 -> :sswitch_0
        0xa1 -> :sswitch_3
        0xa3 -> :sswitch_3
        0xae -> :sswitch_0
        0xb0 -> :sswitch_1
        0xb3 -> :sswitch_1
        0xb5 -> :sswitch_4
        0xb7 -> :sswitch_0
        0xba -> :sswitch_1
        0xbb -> :sswitch_0
        0xd7 -> :sswitch_1
        0xe0 -> :sswitch_0
        0xe1 -> :sswitch_0
        0xe7 -> :sswitch_1
        0xf1 -> :sswitch_1
        0xfb -> :sswitch_1
        0x4254 -> :sswitch_1
        0x4255 -> :sswitch_3
        0x4282 -> :sswitch_2
        0x4285 -> :sswitch_1
        0x42f7 -> :sswitch_1
        0x4489 -> :sswitch_4
        0x47e1 -> :sswitch_1
        0x47e2 -> :sswitch_3
        0x47e7 -> :sswitch_0
        0x47e8 -> :sswitch_1
        0x4dbb -> :sswitch_0
        0x5031 -> :sswitch_1
        0x5032 -> :sswitch_1
        0x5034 -> :sswitch_0
        0x5035 -> :sswitch_0
        0x53ab -> :sswitch_3
        0x53ac -> :sswitch_1
        0x53b8 -> :sswitch_1
        0x54b0 -> :sswitch_1
        0x54b2 -> :sswitch_1
        0x54ba -> :sswitch_1
        0x55aa -> :sswitch_1
        0x55b0 -> :sswitch_0
        0x55b9 -> :sswitch_1
        0x55ba -> :sswitch_1
        0x55bb -> :sswitch_1
        0x55bc -> :sswitch_1
        0x55bd -> :sswitch_1
        0x55d0 -> :sswitch_0
        0x55d1 -> :sswitch_4
        0x55d2 -> :sswitch_4
        0x55d3 -> :sswitch_4
        0x55d4 -> :sswitch_4
        0x55d5 -> :sswitch_4
        0x55d6 -> :sswitch_4
        0x55d7 -> :sswitch_4
        0x55d8 -> :sswitch_4
        0x55d9 -> :sswitch_4
        0x55da -> :sswitch_4
        0x56aa -> :sswitch_1
        0x56bb -> :sswitch_1
        0x6240 -> :sswitch_0
        0x6264 -> :sswitch_1
        0x63a2 -> :sswitch_3
        0x6d80 -> :sswitch_0
        0x7670 -> :sswitch_0
        0x7672 -> :sswitch_3
        0x22b59c -> :sswitch_2
        0x23e383 -> :sswitch_1
        0x2ad7b1 -> :sswitch_1
        0x114d9b74 -> :sswitch_0
        0x1549a966 -> :sswitch_0
        0x1654ae6b -> :sswitch_0
        0x18538067 -> :sswitch_0
        0x1a45dfa3 -> :sswitch_0
        0x1c53bb6b -> :sswitch_0
        0x1f43b675 -> :sswitch_0
    .end sparse-switch
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/f;Lcom/google/android/exoplayer2/extractor/k;)I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 426
    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->af:Z

    move v2, v1

    .line 428
    :cond_0
    if-eqz v2, :cond_2

    iget-boolean v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->af:Z

    if-nez v3, :cond_2

    .line 429
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->j:Lcom/google/android/exoplayer2/extractor/a/b;

    invoke-interface {v2, p1}, Lcom/google/android/exoplayer2/extractor/a/b;->a(Lcom/google/android/exoplayer2/extractor/f;)Z

    move-result v2

    .line 430
    if-eqz v2, :cond_0

    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/f;->c()J

    move-result-wide v4

    invoke-direct {p0, p2, v4, v5}, Lcom/google/android/exoplayer2/extractor/a/d;->a(Lcom/google/android/exoplayer2/extractor/k;J)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    .line 434
    :cond_1
    :goto_0
    return v0

    :cond_2
    if-nez v2, :cond_1

    const/4 v0, -0x1

    goto :goto_0
.end method

.method a(ID)V
    .locals 2

    .prologue
    .line 852
    sparse-switch p1, :sswitch_data_0

    .line 892
    :goto_0
    return-void

    .line 854
    :sswitch_0
    double-to-long v0, p2

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->A:J

    goto :goto_0

    .line 857
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    double-to-int v1, p2

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->I:I

    goto :goto_0

    .line 860
    :sswitch_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    double-to-float v1, p2

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->w:F

    goto :goto_0

    .line 863
    :sswitch_3
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    double-to-float v1, p2

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->x:F

    goto :goto_0

    .line 866
    :sswitch_4
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    double-to-float v1, p2

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->y:F

    goto :goto_0

    .line 869
    :sswitch_5
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    double-to-float v1, p2

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->z:F

    goto :goto_0

    .line 872
    :sswitch_6
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    double-to-float v1, p2

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->A:F

    goto :goto_0

    .line 875
    :sswitch_7
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    double-to-float v1, p2

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->B:F

    goto :goto_0

    .line 878
    :sswitch_8
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    double-to-float v1, p2

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->C:F

    goto :goto_0

    .line 881
    :sswitch_9
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    double-to-float v1, p2

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->D:F

    goto :goto_0

    .line 884
    :sswitch_a
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    double-to-float v1, p2

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->E:F

    goto :goto_0

    .line 887
    :sswitch_b
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    double-to-float v1, p2

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->F:F

    goto :goto_0

    .line 852
    :sswitch_data_0
    .sparse-switch
        0xb5 -> :sswitch_1
        0x4489 -> :sswitch_0
        0x55d1 -> :sswitch_2
        0x55d2 -> :sswitch_3
        0x55d3 -> :sswitch_4
        0x55d4 -> :sswitch_5
        0x55d5 -> :sswitch_6
        0x55d6 -> :sswitch_7
        0x55d7 -> :sswitch_8
        0x55d8 -> :sswitch_9
        0x55d9 -> :sswitch_a
        0x55da -> :sswitch_b
    .end sparse-switch
.end method

.method a(IILcom/google/android/exoplayer2/extractor/f;)V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 915
    sparse-switch p1, :sswitch_data_0

    .line 1075
    new-instance v2, Lcom/google/android/exoplayer2/ParserException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unexpected id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 917
    :sswitch_0
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->r:Lcom/google/android/exoplayer2/util/k;

    iget-object v2, v2, Lcom/google/android/exoplayer2/util/k;->a:[B

    const/4 v3, 0x0

    invoke-static {v2, v3}, Ljava/util/Arrays;->fill([BB)V

    .line 918
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->r:Lcom/google/android/exoplayer2/util/k;

    iget-object v2, v2, Lcom/google/android/exoplayer2/util/k;->a:[B

    rsub-int/lit8 v3, p2, 0x4

    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v2, v3, v1}, Lcom/google/android/exoplayer2/extractor/f;->b([BII)V

    .line 919
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->r:Lcom/google/android/exoplayer2/util/k;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 920
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->r:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/util/k;->l()J

    move-result-wide v2

    long-to-int v2, v2

    iput v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->E:I

    .line 1077
    :goto_0
    return-void

    .line 923
    :sswitch_1
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    move/from16 v0, p2

    new-array v3, v0, [B

    iput-object v3, v2, Lcom/google/android/exoplayer2/extractor/a/d$b;->h:[B

    .line 924
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    iget-object v2, v2, Lcom/google/android/exoplayer2/extractor/a/d$b;->h:[B

    const/4 v3, 0x0

    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v2, v3, v1}, Lcom/google/android/exoplayer2/extractor/f;->b([BII)V

    goto :goto_0

    .line 927
    :sswitch_2
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    move/from16 v0, p2

    new-array v3, v0, [B

    iput-object v3, v2, Lcom/google/android/exoplayer2/extractor/a/d$b;->o:[B

    .line 928
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    iget-object v2, v2, Lcom/google/android/exoplayer2/extractor/a/d$b;->o:[B

    const/4 v3, 0x0

    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v2, v3, v1}, Lcom/google/android/exoplayer2/extractor/f;->b([BII)V

    goto :goto_0

    .line 932
    :sswitch_3
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    move/from16 v0, p2

    new-array v3, v0, [B

    iput-object v3, v2, Lcom/google/android/exoplayer2/extractor/a/d$b;->f:[B

    .line 933
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    iget-object v2, v2, Lcom/google/android/exoplayer2/extractor/a/d$b;->f:[B

    const/4 v3, 0x0

    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v2, v3, v1}, Lcom/google/android/exoplayer2/extractor/f;->b([BII)V

    goto :goto_0

    .line 936
    :sswitch_4
    move/from16 v0, p2

    new-array v2, v0, [B

    .line 937
    const/4 v3, 0x0

    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v2, v3, v1}, Lcom/google/android/exoplayer2/extractor/f;->b([BII)V

    .line 938
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    new-instance v4, Lcom/google/android/exoplayer2/extractor/m$a;

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {v4, v5, v2, v6, v7}, Lcom/google/android/exoplayer2/extractor/m$a;-><init>(I[BII)V

    iput-object v4, v3, Lcom/google/android/exoplayer2/extractor/a/d$b;->g:Lcom/google/android/exoplayer2/extractor/m$a;

    goto :goto_0

    .line 948
    :sswitch_5
    iget v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->N:I

    if-nez v2, :cond_0

    .line 949
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->k:Lcom/google/android/exoplayer2/extractor/a/f;

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/16 v5, 0x8

    move-object/from16 v0, p3

    invoke-virtual {v2, v0, v3, v4, v5}, Lcom/google/android/exoplayer2/extractor/a/f;->a(Lcom/google/android/exoplayer2/extractor/f;ZZI)J

    move-result-wide v2

    long-to-int v2, v2

    iput v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->T:I

    .line 950
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->k:Lcom/google/android/exoplayer2/extractor/a/f;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/extractor/a/f;->b()I

    move-result v2

    iput v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->U:I

    .line 951
    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->P:J

    .line 952
    const/4 v2, 0x1

    iput v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->N:I

    .line 953
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/util/k;->a()V

    .line 956
    :cond_0
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->l:Landroid/util/SparseArray;

    iget v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->T:I

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/extractor/a/d$b;

    .line 959
    if-nez v2, :cond_1

    .line 960
    iget v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->U:I

    sub-int v2, p2, v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Lcom/google/android/exoplayer2/extractor/f;->b(I)V

    .line 961
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->N:I

    goto/16 :goto_0

    .line 965
    :cond_1
    iget v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->N:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    .line 967
    const/4 v3, 0x3

    move-object/from16 v0, p3

    invoke-direct {p0, v0, v3}, Lcom/google/android/exoplayer2/extractor/a/d;->a(Lcom/google/android/exoplayer2/extractor/f;I)V

    .line 968
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    iget-object v3, v3, Lcom/google/android/exoplayer2/util/k;->a:[B

    const/4 v4, 0x2

    aget-byte v3, v3, v4

    and-int/lit8 v3, v3, 0x6

    shr-int/lit8 v3, v3, 0x1

    .line 969
    if-nez v3, :cond_4

    .line 970
    const/4 v3, 0x1

    iput v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->R:I

    .line 971
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->S:[I

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/google/android/exoplayer2/extractor/a/d;->a([II)[I

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->S:[I

    .line 972
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->S:[I

    const/4 v4, 0x0

    iget v5, p0, Lcom/google/android/exoplayer2/extractor/a/d;->U:I

    sub-int v5, p2, v5

    add-int/lit8 v5, v5, -0x3

    aput v5, v3, v4

    .line 1046
    :goto_1
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    iget-object v3, v3, Lcom/google/android/exoplayer2/util/k;->a:[B

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    shl-int/lit8 v3, v3, 0x8

    iget-object v4, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    iget-object v4, v4, Lcom/google/android/exoplayer2/util/k;->a:[B

    const/4 v5, 0x1

    aget-byte v4, v4, v5

    and-int/lit16 v4, v4, 0xff

    or-int/2addr v3, v4

    .line 1047
    iget-wide v4, p0, Lcom/google/android/exoplayer2/extractor/a/d;->J:J

    int-to-long v6, v3

    invoke-direct {p0, v6, v7}, Lcom/google/android/exoplayer2/extractor/a/d;->a(J)J

    move-result-wide v6

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/android/exoplayer2/extractor/a/d;->O:J

    .line 1048
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    iget-object v3, v3, Lcom/google/android/exoplayer2/util/k;->a:[B

    const/4 v4, 0x2

    aget-byte v3, v3, v4

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_13

    const/4 v3, 0x1

    .line 1049
    :goto_2
    iget v4, v2, Lcom/google/android/exoplayer2/extractor/a/d$b;->c:I

    const/4 v5, 0x2

    if-eq v4, v5, :cond_2

    const/16 v4, 0xa3

    move/from16 v0, p1

    if-ne v0, v4, :cond_14

    iget-object v4, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    iget-object v4, v4, Lcom/google/android/exoplayer2/util/k;->a:[B

    const/4 v5, 0x2

    aget-byte v4, v4, v5

    and-int/lit16 v4, v4, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_14

    :cond_2
    const/4 v4, 0x1

    .line 1051
    :goto_3
    if-eqz v4, :cond_15

    const/4 v4, 0x1

    :goto_4
    if-eqz v3, :cond_16

    const/high16 v3, -0x80000000

    :goto_5
    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->V:I

    .line 1053
    const/4 v3, 0x2

    iput v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->N:I

    .line 1054
    const/4 v3, 0x0

    iput v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->Q:I

    .line 1057
    :cond_3
    const/16 v3, 0xa3

    move/from16 v0, p1

    if-ne v0, v3, :cond_18

    .line 1059
    :goto_6
    iget v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->Q:I

    iget v4, p0, Lcom/google/android/exoplayer2/extractor/a/d;->R:I

    if-ge v3, v4, :cond_17

    .line 1060
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->S:[I

    iget v4, p0, Lcom/google/android/exoplayer2/extractor/a/d;->Q:I

    aget v3, v3, v4

    move-object/from16 v0, p3

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/exoplayer2/extractor/a/d;->a(Lcom/google/android/exoplayer2/extractor/f;Lcom/google/android/exoplayer2/extractor/a/d$b;I)V

    .line 1061
    iget-wide v4, p0, Lcom/google/android/exoplayer2/extractor/a/d;->O:J

    iget v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->Q:I

    iget v6, v2, Lcom/google/android/exoplayer2/extractor/a/d$b;->d:I

    mul-int/2addr v3, v6

    div-int/lit16 v3, v3, 0x3e8

    int-to-long v6, v3

    add-long/2addr v4, v6

    .line 1063
    invoke-direct {p0, v2, v4, v5}, Lcom/google/android/exoplayer2/extractor/a/d;->a(Lcom/google/android/exoplayer2/extractor/a/d$b;J)V

    .line 1064
    iget v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->Q:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->Q:I

    goto :goto_6

    .line 974
    :cond_4
    const/16 v4, 0xa3

    move/from16 v0, p1

    if-eq v0, v4, :cond_5

    .line 975
    new-instance v2, Lcom/google/android/exoplayer2/ParserException;

    const-string v3, "Lacing only supported in SimpleBlocks."

    invoke-direct {v2, v3}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 979
    :cond_5
    const/4 v4, 0x4

    move-object/from16 v0, p3

    invoke-direct {p0, v0, v4}, Lcom/google/android/exoplayer2/extractor/a/d;->a(Lcom/google/android/exoplayer2/extractor/f;I)V

    .line 980
    iget-object v4, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    iget-object v4, v4, Lcom/google/android/exoplayer2/util/k;->a:[B

    const/4 v5, 0x3

    aget-byte v4, v4, v5

    and-int/lit16 v4, v4, 0xff

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/android/exoplayer2/extractor/a/d;->R:I

    .line 981
    iget-object v4, p0, Lcom/google/android/exoplayer2/extractor/a/d;->S:[I

    iget v5, p0, Lcom/google/android/exoplayer2/extractor/a/d;->R:I

    .line 982
    invoke-static {v4, v5}, Lcom/google/android/exoplayer2/extractor/a/d;->a([II)[I

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/exoplayer2/extractor/a/d;->S:[I

    .line 983
    const/4 v4, 0x2

    if-ne v3, v4, :cond_6

    .line 984
    iget v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->U:I

    sub-int v3, p2, v3

    add-int/lit8 v3, v3, -0x4

    iget v4, p0, Lcom/google/android/exoplayer2/extractor/a/d;->R:I

    div-int/2addr v3, v4

    .line 986
    iget-object v4, p0, Lcom/google/android/exoplayer2/extractor/a/d;->S:[I

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/exoplayer2/extractor/a/d;->R:I

    invoke-static {v4, v5, v6, v3}, Ljava/util/Arrays;->fill([IIII)V

    goto/16 :goto_1

    .line 987
    :cond_6
    const/4 v4, 0x1

    if-ne v3, v4, :cond_9

    .line 988
    const/4 v5, 0x0

    .line 989
    const/4 v4, 0x4

    .line 990
    const/4 v3, 0x0

    :goto_7
    iget v6, p0, Lcom/google/android/exoplayer2/extractor/a/d;->R:I

    add-int/lit8 v6, v6, -0x1

    if-ge v3, v6, :cond_8

    .line 991
    iget-object v6, p0, Lcom/google/android/exoplayer2/extractor/a/d;->S:[I

    const/4 v7, 0x0

    aput v7, v6, v3

    .line 994
    :cond_7
    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p3

    invoke-direct {p0, v0, v4}, Lcom/google/android/exoplayer2/extractor/a/d;->a(Lcom/google/android/exoplayer2/extractor/f;I)V

    .line 995
    iget-object v6, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    iget-object v6, v6, Lcom/google/android/exoplayer2/util/k;->a:[B

    add-int/lit8 v7, v4, -0x1

    aget-byte v6, v6, v7

    and-int/lit16 v6, v6, 0xff

    .line 996
    iget-object v7, p0, Lcom/google/android/exoplayer2/extractor/a/d;->S:[I

    aget v8, v7, v3

    add-int/2addr v8, v6

    aput v8, v7, v3

    .line 997
    const/16 v7, 0xff

    if-eq v6, v7, :cond_7

    .line 998
    iget-object v6, p0, Lcom/google/android/exoplayer2/extractor/a/d;->S:[I

    aget v6, v6, v3

    add-int/2addr v5, v6

    .line 990
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 1000
    :cond_8
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->S:[I

    iget v6, p0, Lcom/google/android/exoplayer2/extractor/a/d;->R:I

    add-int/lit8 v6, v6, -0x1

    iget v7, p0, Lcom/google/android/exoplayer2/extractor/a/d;->U:I

    sub-int v7, p2, v7

    sub-int v4, v7, v4

    sub-int/2addr v4, v5

    aput v4, v3, v6

    goto/16 :goto_1

    .line 1002
    :cond_9
    const/4 v4, 0x3

    if-ne v3, v4, :cond_12

    .line 1003
    const/4 v5, 0x0

    .line 1004
    const/4 v4, 0x4

    .line 1005
    const/4 v3, 0x0

    :goto_8
    iget v6, p0, Lcom/google/android/exoplayer2/extractor/a/d;->R:I

    add-int/lit8 v6, v6, -0x1

    if-ge v3, v6, :cond_11

    .line 1006
    iget-object v6, p0, Lcom/google/android/exoplayer2/extractor/a/d;->S:[I

    const/4 v7, 0x0

    aput v7, v6, v3

    .line 1007
    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p3

    invoke-direct {p0, v0, v4}, Lcom/google/android/exoplayer2/extractor/a/d;->a(Lcom/google/android/exoplayer2/extractor/f;I)V

    .line 1008
    iget-object v6, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    iget-object v6, v6, Lcom/google/android/exoplayer2/util/k;->a:[B

    add-int/lit8 v7, v4, -0x1

    aget-byte v6, v6, v7

    if-nez v6, :cond_a

    .line 1009
    new-instance v2, Lcom/google/android/exoplayer2/ParserException;

    const-string v3, "No valid varint length mask found"

    invoke-direct {v2, v3}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1011
    :cond_a
    const-wide/16 v6, 0x0

    .line 1012
    const/4 v8, 0x0

    move v10, v8

    :goto_9
    const/16 v8, 0x8

    if-ge v10, v8, :cond_c

    .line 1013
    const/4 v8, 0x1

    rsub-int/lit8 v9, v10, 0x7

    shl-int/2addr v8, v9

    .line 1014
    iget-object v9, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    iget-object v9, v9, Lcom/google/android/exoplayer2/util/k;->a:[B

    add-int/lit8 v11, v4, -0x1

    aget-byte v9, v9, v11

    and-int/2addr v9, v8

    if-eqz v9, :cond_e

    .line 1015
    add-int/lit8 v7, v4, -0x1

    .line 1016
    add-int/2addr v4, v10

    .line 1017
    move-object/from16 v0, p3

    invoke-direct {p0, v0, v4}, Lcom/google/android/exoplayer2/extractor/a/d;->a(Lcom/google/android/exoplayer2/extractor/f;I)V

    .line 1018
    iget-object v6, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    iget-object v9, v6, Lcom/google/android/exoplayer2/util/k;->a:[B

    add-int/lit8 v6, v7, 0x1

    aget-byte v7, v9, v7

    and-int/lit16 v7, v7, 0xff

    xor-int/lit8 v8, v8, -0x1

    and-int/2addr v7, v8

    int-to-long v8, v7

    move v14, v6

    move-wide v6, v8

    move v8, v14

    .line 1019
    :goto_a
    if-ge v8, v4, :cond_b

    .line 1020
    const/16 v9, 0x8

    shl-long v12, v6, v9

    .line 1021
    iget-object v6, p0, Lcom/google/android/exoplayer2/extractor/a/d;->p:Lcom/google/android/exoplayer2/util/k;

    iget-object v7, v6, Lcom/google/android/exoplayer2/util/k;->a:[B

    add-int/lit8 v6, v8, 0x1

    aget-byte v7, v7, v8

    and-int/lit16 v7, v7, 0xff

    int-to-long v8, v7

    or-long/2addr v8, v12

    move v14, v6

    move-wide v6, v8

    move v8, v14

    goto :goto_a

    .line 1024
    :cond_b
    if-lez v3, :cond_c

    .line 1025
    const-wide/16 v8, 0x1

    mul-int/lit8 v10, v10, 0x7

    add-int/lit8 v10, v10, 0x6

    shl-long/2addr v8, v10

    const-wide/16 v10, 0x1

    sub-long/2addr v8, v10

    sub-long/2addr v6, v8

    .line 1030
    :cond_c
    const-wide/32 v8, -0x80000000

    cmp-long v8, v6, v8

    if-ltz v8, :cond_d

    const-wide/32 v8, 0x7fffffff

    cmp-long v8, v6, v8

    if-lez v8, :cond_f

    .line 1031
    :cond_d
    new-instance v2, Lcom/google/android/exoplayer2/ParserException;

    const-string v3, "EBML lacing sample size out of range."

    invoke-direct {v2, v3}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1012
    :cond_e
    add-int/lit8 v8, v10, 0x1

    move v10, v8

    goto :goto_9

    .line 1033
    :cond_f
    long-to-int v6, v6

    .line 1034
    iget-object v7, p0, Lcom/google/android/exoplayer2/extractor/a/d;->S:[I

    if-nez v3, :cond_10

    :goto_b
    aput v6, v7, v3

    .line 1036
    iget-object v6, p0, Lcom/google/android/exoplayer2/extractor/a/d;->S:[I

    aget v6, v6, v3

    add-int/2addr v5, v6

    .line 1005
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_8

    .line 1034
    :cond_10
    iget-object v8, p0, Lcom/google/android/exoplayer2/extractor/a/d;->S:[I

    add-int/lit8 v9, v3, -0x1

    aget v8, v8, v9

    add-int/2addr v6, v8

    goto :goto_b

    .line 1038
    :cond_11
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->S:[I

    iget v6, p0, Lcom/google/android/exoplayer2/extractor/a/d;->R:I

    add-int/lit8 v6, v6, -0x1

    iget v7, p0, Lcom/google/android/exoplayer2/extractor/a/d;->U:I

    sub-int v7, p2, v7

    sub-int v4, v7, v4

    sub-int/2addr v4, v5

    aput v4, v3, v6

    goto/16 :goto_1

    .line 1042
    :cond_12
    new-instance v2, Lcom/google/android/exoplayer2/ParserException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unexpected lacing value: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1048
    :cond_13
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 1049
    :cond_14
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 1051
    :cond_15
    const/4 v4, 0x0

    goto/16 :goto_4

    :cond_16
    const/4 v3, 0x0

    goto/16 :goto_5

    .line 1066
    :cond_17
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->N:I

    goto/16 :goto_0

    .line 1070
    :cond_18
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/a/d;->S:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p3

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/exoplayer2/extractor/a/d;->a(Lcom/google/android/exoplayer2/extractor/f;Lcom/google/android/exoplayer2/extractor/a/d$b;I)V

    goto/16 :goto_0

    .line 915
    nop

    :sswitch_data_0
    .sparse-switch
        0xa1 -> :sswitch_5
        0xa3 -> :sswitch_5
        0x4255 -> :sswitch_3
        0x47e2 -> :sswitch_4
        0x53ab -> :sswitch_0
        0x63a2 -> :sswitch_1
        0x7672 -> :sswitch_2
    .end sparse-switch
.end method

.method a(IJ)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x3

    const/4 v3, 0x2

    const/4 v1, 0x0

    const-wide/16 v4, 0x1

    const/4 v0, 0x1

    .line 659
    sparse-switch p1, :sswitch_data_0

    .line 849
    :cond_0
    :goto_0
    return-void

    .line 662
    :sswitch_0
    cmp-long v0, p2, v4

    if-eqz v0, :cond_0

    .line 663
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EBMLReadVersion "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 668
    :sswitch_1
    cmp-long v0, p2, v4

    if-ltz v0, :cond_1

    const-wide/16 v0, 0x2

    cmp-long v0, p2, v0

    if-lez v0, :cond_0

    .line 669
    :cond_1
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DocTypeReadVersion "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 675
    :sswitch_2
    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->y:J

    add-long/2addr v0, p2

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->F:J

    goto :goto_0

    .line 678
    :sswitch_3
    iput-wide p2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->z:J

    goto :goto_0

    .line 681
    :sswitch_4
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->j:I

    goto :goto_0

    .line 684
    :sswitch_5
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->k:I

    goto :goto_0

    .line 687
    :sswitch_6
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->l:I

    goto :goto_0

    .line 690
    :sswitch_7
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->m:I

    goto :goto_0

    .line 693
    :sswitch_8
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->n:I

    goto :goto_0

    .line 696
    :sswitch_9
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->b:I

    goto :goto_0

    .line 699
    :sswitch_a
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    cmp-long v3, p2, v4

    if-nez v3, :cond_2

    :goto_1
    iput-boolean v0, v2, Lcom/google/android/exoplayer2/extractor/a/d$b;->L:Z

    goto/16 :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 702
    :sswitch_b
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    cmp-long v3, p2, v4

    if-nez v3, :cond_3

    :goto_2
    iput-boolean v0, v2, Lcom/google/android/exoplayer2/extractor/a/d$b;->M:Z

    goto/16 :goto_0

    :cond_3
    move v0, v1

    goto :goto_2

    .line 705
    :sswitch_c
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->c:I

    goto/16 :goto_0

    .line 708
    :sswitch_d
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->d:I

    goto/16 :goto_0

    .line 711
    :sswitch_e
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    iput-wide p2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->J:J

    goto/16 :goto_0

    .line 714
    :sswitch_f
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    iput-wide p2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->K:J

    goto/16 :goto_0

    .line 717
    :sswitch_10
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->G:I

    goto/16 :goto_0

    .line 720
    :sswitch_11
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->H:I

    goto/16 :goto_0

    .line 723
    :sswitch_12
    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ag:Z

    goto/16 :goto_0

    .line 727
    :sswitch_13
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    .line 728
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ContentEncodingOrder "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 733
    :sswitch_14
    cmp-long v0, p2, v4

    if-eqz v0, :cond_0

    .line 734
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ContentEncodingScope "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 739
    :sswitch_15
    const-wide/16 v0, 0x3

    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    .line 740
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ContentCompAlgo "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 745
    :sswitch_16
    const-wide/16 v0, 0x5

    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    .line 746
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ContentEncAlgo "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 751
    :sswitch_17
    cmp-long v0, p2, v4

    if-eqz v0, :cond_0

    .line 752
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AESSettingsCipherMode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 756
    :sswitch_18
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->K:Lcom/google/android/exoplayer2/util/f;

    invoke-direct {p0, p2, p3}, Lcom/google/android/exoplayer2/extractor/a/d;->a(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/exoplayer2/util/f;->a(J)V

    goto/16 :goto_0

    .line 759
    :sswitch_19
    iget-boolean v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->M:Z

    if-nez v1, :cond_0

    .line 763
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->L:Lcom/google/android/exoplayer2/util/f;

    invoke-virtual {v1, p2, p3}, Lcom/google/android/exoplayer2/util/f;->a(J)V

    .line 764
    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->M:Z

    goto/16 :goto_0

    .line 768
    :sswitch_1a
    invoke-direct {p0, p2, p3}, Lcom/google/android/exoplayer2/extractor/a/d;->a(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->J:J

    goto/16 :goto_0

    .line 771
    :sswitch_1b
    invoke-direct {p0, p2, p3}, Lcom/google/android/exoplayer2/extractor/a/d;->a(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->P:J

    goto/16 :goto_0

    .line 774
    :sswitch_1c
    long-to-int v2, p2

    .line 775
    sparse-switch v2, :sswitch_data_1

    goto/16 :goto_0

    .line 777
    :sswitch_1d
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->p:I

    goto/16 :goto_0

    .line 780
    :sswitch_1e
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    iput v3, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->p:I

    goto/16 :goto_0

    .line 783
    :sswitch_1f
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    iput v0, v1, Lcom/google/android/exoplayer2/extractor/a/d$b;->p:I

    goto/16 :goto_0

    .line 786
    :sswitch_20
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    iput v6, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->p:I

    goto/16 :goto_0

    .line 793
    :sswitch_21
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    iput-boolean v0, v1, Lcom/google/android/exoplayer2/extractor/a/d$b;->q:Z

    .line 794
    long-to-int v1, p2

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_0

    .line 796
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    iput v0, v1, Lcom/google/android/exoplayer2/extractor/a/d$b;->r:I

    goto/16 :goto_0

    .line 802
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    iput v3, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->r:I

    goto/16 :goto_0

    .line 805
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    const/4 v1, 0x6

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->r:I

    goto/16 :goto_0

    .line 812
    :sswitch_22
    long-to-int v0, p2

    sparse-switch v0, :sswitch_data_2

    goto/16 :goto_0

    .line 816
    :sswitch_23
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    iput v6, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->s:I

    goto/16 :goto_0

    .line 819
    :sswitch_24
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    const/4 v1, 0x6

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->s:I

    goto/16 :goto_0

    .line 822
    :sswitch_25
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    const/4 v1, 0x7

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->s:I

    goto/16 :goto_0

    .line 829
    :sswitch_26
    long-to-int v1, p2

    packed-switch v1, :pswitch_data_1

    goto/16 :goto_0

    .line 831
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    iput v3, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->t:I

    goto/16 :goto_0

    .line 834
    :pswitch_5
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    iput v0, v1, Lcom/google/android/exoplayer2/extractor/a/d$b;->t:I

    goto/16 :goto_0

    .line 841
    :sswitch_27
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->u:I

    goto/16 :goto_0

    .line 844
    :sswitch_28
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->v:I

    goto/16 :goto_0

    .line 659
    nop

    :sswitch_data_0
    .sparse-switch
        0x83 -> :sswitch_c
        0x88 -> :sswitch_a
        0x9b -> :sswitch_1b
        0x9f -> :sswitch_10
        0xb0 -> :sswitch_4
        0xb3 -> :sswitch_18
        0xba -> :sswitch_5
        0xd7 -> :sswitch_9
        0xe7 -> :sswitch_1a
        0xf1 -> :sswitch_19
        0xfb -> :sswitch_12
        0x4254 -> :sswitch_15
        0x4285 -> :sswitch_1
        0x42f7 -> :sswitch_0
        0x47e1 -> :sswitch_16
        0x47e8 -> :sswitch_17
        0x5031 -> :sswitch_13
        0x5032 -> :sswitch_14
        0x53ac -> :sswitch_2
        0x53b8 -> :sswitch_1c
        0x54b0 -> :sswitch_6
        0x54b2 -> :sswitch_8
        0x54ba -> :sswitch_7
        0x55aa -> :sswitch_b
        0x55b9 -> :sswitch_26
        0x55ba -> :sswitch_22
        0x55bb -> :sswitch_21
        0x55bc -> :sswitch_27
        0x55bd -> :sswitch_28
        0x56aa -> :sswitch_e
        0x56bb -> :sswitch_f
        0x6264 -> :sswitch_11
        0x23e383 -> :sswitch_d
        0x2ad7b1 -> :sswitch_3
    .end sparse-switch

    .line 775
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_1d
        0x1 -> :sswitch_1e
        0x3 -> :sswitch_1f
        0xf -> :sswitch_20
    .end sparse-switch

    .line 794
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch

    .line 812
    :sswitch_data_2
    .sparse-switch
        0x1 -> :sswitch_23
        0x6 -> :sswitch_23
        0x7 -> :sswitch_23
        0x10 -> :sswitch_24
        0x12 -> :sswitch_25
    .end sparse-switch

    .line 829
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method a(IJJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const-wide/16 v2, -0x1

    const/4 v4, 0x1

    .line 533
    sparse-switch p1, :sswitch_data_0

    .line 585
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 535
    :sswitch_1
    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->y:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->y:J

    cmp-long v0, v0, p2

    if-eqz v0, :cond_1

    .line 537
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Multiple Segment elements not supported"

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 539
    :cond_1
    iput-wide p2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->y:J

    .line 540
    iput-wide p4, p0, Lcom/google/android/exoplayer2/extractor/a/d;->x:J

    goto :goto_0

    .line 543
    :sswitch_2
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->E:I

    .line 544
    iput-wide v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->F:J

    goto :goto_0

    .line 547
    :sswitch_3
    new-instance v0, Lcom/google/android/exoplayer2/util/f;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/util/f;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->K:Lcom/google/android/exoplayer2/util/f;

    .line 548
    new-instance v0, Lcom/google/android/exoplayer2/util/f;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/util/f;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->L:Lcom/google/android/exoplayer2/util/f;

    goto :goto_0

    .line 551
    :sswitch_4
    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->M:Z

    goto :goto_0

    .line 554
    :sswitch_5
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->D:Z

    if-nez v0, :cond_0

    .line 556
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->m:Z

    if-eqz v0, :cond_2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->H:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 558
    iput-boolean v4, p0, Lcom/google/android/exoplayer2/extractor/a/d;->G:Z

    goto :goto_0

    .line 562
    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ah:Lcom/google/android/exoplayer2/extractor/g;

    new-instance v1, Lcom/google/android/exoplayer2/extractor/l$a;

    iget-wide v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->B:J

    invoke-direct {v1, v2, v3}, Lcom/google/android/exoplayer2/extractor/l$a;-><init>(J)V

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/extractor/g;->a(Lcom/google/android/exoplayer2/extractor/l;)V

    .line 563
    iput-boolean v4, p0, Lcom/google/android/exoplayer2/extractor/a/d;->D:Z

    goto :goto_0

    .line 568
    :sswitch_6
    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ag:Z

    goto :goto_0

    .line 574
    :sswitch_7
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    iput-boolean v4, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->e:Z

    goto :goto_0

    .line 577
    :sswitch_8
    new-instance v0, Lcom/google/android/exoplayer2/extractor/a/d$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/extractor/a/d$b;-><init>(Lcom/google/android/exoplayer2/extractor/a/d$1;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    goto :goto_0

    .line 580
    :sswitch_9
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    iput-boolean v4, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->q:Z

    goto :goto_0

    .line 533
    nop

    :sswitch_data_0
    .sparse-switch
        0xa0 -> :sswitch_6
        0xae -> :sswitch_8
        0xbb -> :sswitch_4
        0x4dbb -> :sswitch_2
        0x5035 -> :sswitch_7
        0x55d0 -> :sswitch_9
        0x6240 -> :sswitch_0
        0x18538067 -> :sswitch_1
        0x1c53bb6b -> :sswitch_3
        0x1f43b675 -> :sswitch_5
    .end sparse-switch
.end method

.method a(ILjava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    .line 895
    sparse-switch p1, :sswitch_data_0

    .line 911
    :cond_0
    :goto_0
    return-void

    .line 898
    :sswitch_0
    const-string v0, "webm"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "matroska"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 899
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DocType "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 903
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    iput-object p2, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->a:Ljava/lang/String;

    goto :goto_0

    .line 906
    :sswitch_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    invoke-static {v0, p2}, Lcom/google/android/exoplayer2/extractor/a/d$b;->a(Lcom/google/android/exoplayer2/extractor/a/d$b;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 895
    :sswitch_data_0
    .sparse-switch
        0x86 -> :sswitch_1
        0x4282 -> :sswitch_0
        0x22b59c -> :sswitch_2
    .end sparse-switch
.end method

.method public a(JJ)V
    .locals 2

    .prologue
    .line 411
    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->J:J

    .line 412
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->N:I

    .line 413
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->j:Lcom/google/android/exoplayer2/extractor/a/b;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/extractor/a/b;->a()V

    .line 414
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->k:Lcom/google/android/exoplayer2/extractor/a/f;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/a/f;->a()V

    .line 415
    invoke-direct {p0}, Lcom/google/android/exoplayer2/extractor/a/d;->d()V

    .line 416
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/g;)V
    .locals 0

    .prologue
    .line 406
    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ah:Lcom/google/android/exoplayer2/extractor/g;

    .line 407
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/f;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 401
    new-instance v0, Lcom/google/android/exoplayer2/extractor/a/e;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/extractor/a/e;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/extractor/a/e;->a(Lcom/google/android/exoplayer2/extractor/f;)Z

    move-result v0

    return v0
.end method

.method b(I)Z
    .locals 1

    .prologue
    .line 528
    const v0, 0x1549a966

    if-eq p1, v0, :cond_0

    const v0, 0x1f43b675

    if-eq p1, v0, :cond_0

    const v0, 0x1c53bb6b

    if-eq p1, v0, :cond_0

    const v0, 0x1654ae6b

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 421
    return-void
.end method

.method c(I)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ParserException;
        }
    .end annotation

    .prologue
    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v8, 0x0

    const/4 v2, 0x1

    const/4 v7, 0x0

    .line 588
    sparse-switch p1, :sswitch_data_0

    .line 656
    :cond_0
    :goto_0
    return-void

    .line 590
    :sswitch_0
    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->z:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    .line 592
    const-wide/32 v0, 0xf4240

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->z:J

    .line 594
    :cond_1
    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->A:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 595
    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->A:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer2/extractor/a/d;->a(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->B:J

    goto :goto_0

    .line 599
    :sswitch_1
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->E:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->F:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    .line 600
    :cond_2
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Mandatory element SeekID or SeekPosition not found"

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 602
    :cond_3
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->E:I

    const v1, 0x1c53bb6b

    if-ne v0, v1, :cond_0

    .line 603
    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->F:J

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->H:J

    goto :goto_0

    .line 607
    :sswitch_2
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->D:Z

    if-nez v0, :cond_0

    .line 608
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ah:Lcom/google/android/exoplayer2/extractor/g;

    invoke-direct {p0}, Lcom/google/android/exoplayer2/extractor/a/d;->e()Lcom/google/android/exoplayer2/extractor/l;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/extractor/g;->a(Lcom/google/android/exoplayer2/extractor/l;)V

    .line 609
    iput-boolean v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->D:Z

    goto :goto_0

    .line 615
    :sswitch_3
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->N:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 620
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ag:Z

    if-nez v0, :cond_4

    .line 621
    iget v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->V:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->V:I

    .line 623
    :cond_4
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->l:Landroid/util/SparseArray;

    iget v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->T:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/extractor/a/d$b;

    iget-wide v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->O:J

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/exoplayer2/extractor/a/d;->a(Lcom/google/android/exoplayer2/extractor/a/d$b;J)V

    .line 624
    iput v7, p0, Lcom/google/android/exoplayer2/extractor/a/d;->N:I

    goto :goto_0

    .line 627
    :sswitch_4
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->e:Z

    if-eqz v0, :cond_0

    .line 628
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->g:Lcom/google/android/exoplayer2/extractor/m$a;

    if-nez v0, :cond_5

    .line 629
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Encrypted Track found but ContentEncKeyID was not found"

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 631
    :cond_5
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    new-instance v1, Lcom/google/android/exoplayer2/drm/a;

    new-array v2, v2, [Lcom/google/android/exoplayer2/drm/a$a;

    new-instance v3, Lcom/google/android/exoplayer2/drm/a$a;

    sget-object v4, Lcom/google/android/exoplayer2/b;->b:Ljava/util/UUID;

    const-string v5, "video/webm"

    iget-object v6, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    iget-object v6, v6, Lcom/google/android/exoplayer2/extractor/a/d$b;->g:Lcom/google/android/exoplayer2/extractor/m$a;

    iget-object v6, v6, Lcom/google/android/exoplayer2/extractor/m$a;->b:[B

    invoke-direct {v3, v4, v8, v5, v6}, Lcom/google/android/exoplayer2/drm/a$a;-><init>(Ljava/util/UUID;Ljava/lang/String;Ljava/lang/String;[B)V

    aput-object v3, v2, v7

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer2/drm/a;-><init>([Lcom/google/android/exoplayer2/drm/a$a;)V

    iput-object v1, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->i:Lcom/google/android/exoplayer2/drm/a;

    goto/16 :goto_0

    .line 636
    :sswitch_5
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->f:[B

    if-eqz v0, :cond_0

    .line 637
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Combining encryption and compression is not supported"

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 641
    :sswitch_6
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    iget-object v0, v0, Lcom/google/android/exoplayer2/extractor/a/d$b;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/a/d;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 642
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ah:Lcom/google/android/exoplayer2/extractor/g;

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    iget v2, v2, Lcom/google/android/exoplayer2/extractor/a/d$b;->b:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/extractor/a/d$b;->a(Lcom/google/android/exoplayer2/extractor/g;I)V

    .line 643
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->l:Landroid/util/SparseArray;

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    iget v1, v1, Lcom/google/android/exoplayer2/extractor/a/d$b;->b:I

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 645
    :cond_6
    iput-object v8, p0, Lcom/google/android/exoplayer2/extractor/a/d;->C:Lcom/google/android/exoplayer2/extractor/a/d$b;

    goto/16 :goto_0

    .line 648
    :sswitch_7
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->l:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_7

    .line 649
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "No valid tracks were found"

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 651
    :cond_7
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/a/d;->ah:Lcom/google/android/exoplayer2/extractor/g;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/extractor/g;->a()V

    goto/16 :goto_0

    .line 588
    nop

    :sswitch_data_0
    .sparse-switch
        0xa0 -> :sswitch_3
        0xae -> :sswitch_6
        0x4dbb -> :sswitch_1
        0x6240 -> :sswitch_4
        0x6d80 -> :sswitch_5
        0x1549a966 -> :sswitch_0
        0x1654ae6b -> :sswitch_7
        0x1c53bb6b -> :sswitch_2
    .end sparse-switch
.end method
