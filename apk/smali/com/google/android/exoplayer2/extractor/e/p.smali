.class public final Lcom/google/android/exoplayer2/extractor/e/p;
.super Ljava/lang/Object;
.source "PsExtractor.java"

# interfaces
.implements Lcom/google/android/exoplayer2/extractor/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/extractor/e/p$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/google/android/exoplayer2/extractor/h;


# instance fields
.field private final b:Lcom/google/android/exoplayer2/util/p;

.field private final c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/exoplayer2/extractor/e/p$a;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/google/android/exoplayer2/util/k;

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Lcom/google/android/exoplayer2/extractor/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lcom/google/android/exoplayer2/extractor/e/p$1;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/extractor/e/p$1;-><init>()V

    sput-object v0, Lcom/google/android/exoplayer2/extractor/e/p;->a:Lcom/google/android/exoplayer2/extractor/h;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 73
    new-instance v0, Lcom/google/android/exoplayer2/util/p;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Lcom/google/android/exoplayer2/util/p;-><init>(J)V

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer2/extractor/e/p;-><init>(Lcom/google/android/exoplayer2/util/p;)V

    .line 74
    return-void
.end method

.method public constructor <init>(Lcom/google/android/exoplayer2/util/p;)V
    .locals 2

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/e/p;->b:Lcom/google/android/exoplayer2/util/p;

    .line 78
    new-instance v0, Lcom/google/android/exoplayer2/util/k;

    const/16 v1, 0x1000

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/util/k;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/p;->d:Lcom/google/android/exoplayer2/util/k;

    .line 79
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/p;->c:Landroid/util/SparseArray;

    .line 80
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/exoplayer2/extractor/f;Lcom/google/android/exoplayer2/extractor/k;)I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v0, -0x1

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 146
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/e/p;->d:Lcom/google/android/exoplayer2/util/k;

    iget-object v2, v2, Lcom/google/android/exoplayer2/util/k;->a:[B

    const/4 v3, 0x4

    invoke-interface {p1, v2, v1, v3, v6}, Lcom/google/android/exoplayer2/extractor/f;->b([BIIZ)Z

    move-result v2

    if-nez v2, :cond_1

    .line 234
    :cond_0
    :goto_0
    return v0

    .line 150
    :cond_1
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/e/p;->d:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v2, v1}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 151
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/e/p;->d:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/util/k;->n()I

    move-result v2

    .line 152
    const/16 v3, 0x1b9

    if-eq v2, v3, :cond_0

    .line 154
    const/16 v0, 0x1ba

    if-ne v2, v0, :cond_2

    .line 156
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/p;->d:Lcom/google/android/exoplayer2/util/k;

    iget-object v0, v0, Lcom/google/android/exoplayer2/util/k;->a:[B

    const/16 v2, 0xa

    invoke-interface {p1, v0, v1, v2}, Lcom/google/android/exoplayer2/extractor/f;->c([BII)V

    .line 159
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/p;->d:Lcom/google/android/exoplayer2/util/k;

    const/16 v2, 0x9

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 162
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/p;->d:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/k;->g()I

    move-result v0

    and-int/lit8 v0, v0, 0x7

    .line 165
    add-int/lit8 v0, v0, 0xe

    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/extractor/f;->b(I)V

    move v0, v1

    .line 166
    goto :goto_0

    .line 167
    :cond_2
    const/16 v0, 0x1bb

    if-ne v2, v0, :cond_3

    .line 169
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/p;->d:Lcom/google/android/exoplayer2/util/k;

    iget-object v0, v0, Lcom/google/android/exoplayer2/util/k;->a:[B

    invoke-interface {p1, v0, v1, v7}, Lcom/google/android/exoplayer2/extractor/f;->c([BII)V

    .line 172
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/p;->d:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 173
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/p;->d:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/k;->h()I

    move-result v0

    .line 174
    add-int/lit8 v0, v0, 0x6

    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/extractor/f;->b(I)V

    move v0, v1

    .line 175
    goto :goto_0

    .line 176
    :cond_3
    and-int/lit16 v0, v2, -0x100

    shr-int/lit8 v0, v0, 0x8

    if-eq v0, v6, :cond_4

    .line 177
    invoke-interface {p1, v6}, Lcom/google/android/exoplayer2/extractor/f;->b(I)V

    move v0, v1

    .line 178
    goto :goto_0

    .line 183
    :cond_4
    and-int/lit16 v3, v2, 0xff

    .line 186
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/p;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/extractor/e/p$a;

    .line 187
    iget-boolean v2, p0, Lcom/google/android/exoplayer2/extractor/e/p;->e:Z

    if-nez v2, :cond_9

    .line 188
    if-nez v0, :cond_6

    .line 189
    const/4 v2, 0x0

    .line 190
    iget-boolean v4, p0, Lcom/google/android/exoplayer2/extractor/e/p;->f:Z

    if-nez v4, :cond_a

    const/16 v4, 0xbd

    if-ne v3, v4, :cond_a

    .line 194
    new-instance v2, Lcom/google/android/exoplayer2/extractor/e/b;

    invoke-direct {v2}, Lcom/google/android/exoplayer2/extractor/e/b;-><init>()V

    .line 195
    iput-boolean v6, p0, Lcom/google/android/exoplayer2/extractor/e/p;->f:Z

    .line 203
    :cond_5
    :goto_1
    if-eqz v2, :cond_6

    .line 204
    new-instance v0, Lcom/google/android/exoplayer2/extractor/e/v$d;

    const/16 v4, 0x100

    invoke-direct {v0, v3, v4}, Lcom/google/android/exoplayer2/extractor/e/v$d;-><init>(II)V

    .line 205
    iget-object v4, p0, Lcom/google/android/exoplayer2/extractor/e/p;->h:Lcom/google/android/exoplayer2/extractor/g;

    invoke-interface {v2, v4, v0}, Lcom/google/android/exoplayer2/extractor/e/h;->a(Lcom/google/android/exoplayer2/extractor/g;Lcom/google/android/exoplayer2/extractor/e/v$d;)V

    .line 206
    new-instance v0, Lcom/google/android/exoplayer2/extractor/e/p$a;

    iget-object v4, p0, Lcom/google/android/exoplayer2/extractor/e/p;->b:Lcom/google/android/exoplayer2/util/p;

    invoke-direct {v0, v2, v4}, Lcom/google/android/exoplayer2/extractor/e/p$a;-><init>(Lcom/google/android/exoplayer2/extractor/e/h;Lcom/google/android/exoplayer2/util/p;)V

    .line 207
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/e/p;->c:Landroid/util/SparseArray;

    invoke-virtual {v2, v3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 210
    :cond_6
    iget-boolean v2, p0, Lcom/google/android/exoplayer2/extractor/e/p;->f:Z

    if-eqz v2, :cond_7

    iget-boolean v2, p0, Lcom/google/android/exoplayer2/extractor/e/p;->g:Z

    if-nez v2, :cond_8

    :cond_7
    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/f;->c()J

    move-result-wide v2

    const-wide/32 v4, 0x100000

    cmp-long v2, v2, v4

    if-lez v2, :cond_9

    .line 211
    :cond_8
    iput-boolean v6, p0, Lcom/google/android/exoplayer2/extractor/e/p;->e:Z

    .line 212
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/e/p;->h:Lcom/google/android/exoplayer2/extractor/g;

    invoke-interface {v2}, Lcom/google/android/exoplayer2/extractor/g;->a()V

    .line 217
    :cond_9
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/e/p;->d:Lcom/google/android/exoplayer2/util/k;

    iget-object v2, v2, Lcom/google/android/exoplayer2/util/k;->a:[B

    invoke-interface {p1, v2, v1, v7}, Lcom/google/android/exoplayer2/extractor/f;->c([BII)V

    .line 218
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/e/p;->d:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v2, v1}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 219
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/e/p;->d:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/util/k;->h()I

    move-result v2

    .line 220
    add-int/lit8 v2, v2, 0x6

    .line 222
    if-nez v0, :cond_c

    .line 224
    invoke-interface {p1, v2}, Lcom/google/android/exoplayer2/extractor/f;->b(I)V

    :goto_2
    move v0, v1

    .line 234
    goto/16 :goto_0

    .line 196
    :cond_a
    iget-boolean v4, p0, Lcom/google/android/exoplayer2/extractor/e/p;->f:Z

    if-nez v4, :cond_b

    and-int/lit16 v4, v3, 0xe0

    const/16 v5, 0xc0

    if-ne v4, v5, :cond_b

    .line 197
    new-instance v2, Lcom/google/android/exoplayer2/extractor/e/m;

    invoke-direct {v2}, Lcom/google/android/exoplayer2/extractor/e/m;-><init>()V

    .line 198
    iput-boolean v6, p0, Lcom/google/android/exoplayer2/extractor/e/p;->f:Z

    goto :goto_1

    .line 199
    :cond_b
    iget-boolean v4, p0, Lcom/google/android/exoplayer2/extractor/e/p;->g:Z

    if-nez v4, :cond_5

    and-int/lit16 v4, v3, 0xf0

    const/16 v5, 0xe0

    if-ne v4, v5, :cond_5

    .line 200
    new-instance v2, Lcom/google/android/exoplayer2/extractor/e/i;

    invoke-direct {v2}, Lcom/google/android/exoplayer2/extractor/e/i;-><init>()V

    .line 201
    iput-boolean v6, p0, Lcom/google/android/exoplayer2/extractor/e/p;->g:Z

    goto :goto_1

    .line 226
    :cond_c
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/e/p;->d:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v3, v2}, Lcom/google/android/exoplayer2/util/k;->a(I)V

    .line 228
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/e/p;->d:Lcom/google/android/exoplayer2/util/k;

    iget-object v3, v3, Lcom/google/android/exoplayer2/util/k;->a:[B

    invoke-interface {p1, v3, v1, v2}, Lcom/google/android/exoplayer2/extractor/f;->b([BII)V

    .line 229
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/e/p;->d:Lcom/google/android/exoplayer2/util/k;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 230
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/e/p;->d:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/extractor/e/p$a;->a(Lcom/google/android/exoplayer2/util/k;)V

    .line 231
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/p;->d:Lcom/google/android/exoplayer2/util/k;

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/e/p;->d:Lcom/google/android/exoplayer2/util/k;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/util/k;->e()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/util/k;->b(I)V

    goto :goto_2
.end method

.method public a(JJ)V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/p;->b:Lcom/google/android/exoplayer2/util/p;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/p;->d()V

    .line 132
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/p;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/p;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/extractor/e/p$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/e/p$a;->a()V

    .line 132
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 135
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/g;)V
    .locals 4

    .prologue
    .line 125
    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/e/p;->h:Lcom/google/android/exoplayer2/extractor/g;

    .line 126
    new-instance v0, Lcom/google/android/exoplayer2/extractor/l$a;

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    invoke-direct {v0, v2, v3}, Lcom/google/android/exoplayer2/extractor/l$a;-><init>(J)V

    invoke-interface {p1, v0}, Lcom/google/android/exoplayer2/extractor/g;->a(Lcom/google/android/exoplayer2/extractor/l;)V

    .line 127
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/f;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 86
    const/16 v2, 0xe

    new-array v2, v2, [B

    .line 87
    const/16 v3, 0xe

    invoke-interface {p1, v2, v1, v3}, Lcom/google/android/exoplayer2/extractor/f;->c([BII)V

    .line 90
    const/16 v3, 0x1ba

    aget-byte v4, v2, v1

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x18

    aget-byte v5, v2, v0

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x10

    or-int/2addr v4, v5

    aget-byte v5, v2, v8

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v4, v5

    aget-byte v5, v2, v6

    and-int/lit16 v5, v5, 0xff

    or-int/2addr v4, v5

    if-eq v3, v4, :cond_1

    .line 119
    :cond_0
    :goto_0
    return v1

    .line 95
    :cond_1
    aget-byte v3, v2, v7

    and-int/lit16 v3, v3, 0xc4

    const/16 v4, 0x44

    if-ne v3, v4, :cond_0

    .line 99
    const/4 v3, 0x6

    aget-byte v3, v2, v3

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v7, :cond_0

    .line 103
    const/16 v3, 0x8

    aget-byte v3, v2, v3

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v7, :cond_0

    .line 107
    const/16 v3, 0x9

    aget-byte v3, v2, v3

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    .line 111
    const/16 v3, 0xc

    aget-byte v3, v2, v3

    and-int/lit8 v3, v3, 0x3

    if-ne v3, v6, :cond_0

    .line 115
    const/16 v3, 0xd

    aget-byte v3, v2, v3

    and-int/lit8 v3, v3, 0x7

    .line 116
    invoke-interface {p1, v3}, Lcom/google/android/exoplayer2/extractor/f;->c(I)V

    .line 118
    invoke-interface {p1, v2, v1, v6}, Lcom/google/android/exoplayer2/extractor/f;->c([BII)V

    .line 119
    aget-byte v3, v2, v1

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    aget-byte v4, v2, v0

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x8

    or-int/2addr v3, v4

    aget-byte v2, v2, v8

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v2, v3

    if-ne v0, v2, :cond_2

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public c()V
    .locals 0

    .prologue
    .line 140
    return-void
.end method
