.class Lcom/google/android/exoplayer2/extractor/e/u$b;
.super Ljava/lang/Object;
.source "TsExtractor.java"

# interfaces
.implements Lcom/google/android/exoplayer2/extractor/e/q;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/extractor/e/u;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/exoplayer2/extractor/e/u;

.field private final b:Lcom/google/android/exoplayer2/util/j;

.field private final c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/exoplayer2/extractor/e/v;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/util/SparseIntArray;

.field private final e:I


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/extractor/e/u;I)V
    .locals 2

    .prologue
    .line 386
    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387
    new-instance v0, Lcom/google/android/exoplayer2/util/j;

    const/4 v1, 0x5

    new-array v1, v1, [B

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/util/j;-><init>([B)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->b:Lcom/google/android/exoplayer2/util/j;

    .line 388
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->c:Landroid/util/SparseArray;

    .line 389
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->d:Landroid/util/SparseIntArray;

    .line 390
    iput p2, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->e:I

    .line 391
    return-void
.end method

.method private a(Lcom/google/android/exoplayer2/util/k;I)Lcom/google/android/exoplayer2/extractor/e/v$b;
    .locals 12

    .prologue
    .line 509
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->d()I

    move-result v3

    .line 510
    add-int v4, v3, p2

    .line 511
    const/4 v2, -0x1

    .line 512
    const/4 v1, 0x0

    .line 513
    const/4 v0, 0x0

    .line 514
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->d()I

    move-result v5

    if-ge v5, v4, :cond_8

    .line 515
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->g()I

    move-result v5

    .line 516
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->g()I

    move-result v6

    .line 517
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->d()I

    move-result v7

    add-int/2addr v6, v7

    .line 518
    const/4 v7, 0x5

    if-ne v5, v7, :cond_3

    .line 519
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->l()J

    move-result-wide v8

    .line 520
    invoke-static {}, Lcom/google/android/exoplayer2/extractor/e/u;->a()J

    move-result-wide v10

    cmp-long v5, v8, v10

    if-nez v5, :cond_1

    .line 521
    const/16 v2, 0x81

    .line 549
    :cond_0
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->d()I

    move-result v5

    sub-int v5, v6, v5

    invoke-virtual {p1, v5}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    goto :goto_0

    .line 522
    :cond_1
    invoke-static {}, Lcom/google/android/exoplayer2/extractor/e/u;->b()J

    move-result-wide v10

    cmp-long v5, v8, v10

    if-nez v5, :cond_2

    .line 523
    const/16 v2, 0x87

    goto :goto_1

    .line 524
    :cond_2
    invoke-static {}, Lcom/google/android/exoplayer2/extractor/e/u;->d()J

    move-result-wide v10

    cmp-long v5, v8, v10

    if-nez v5, :cond_0

    .line 525
    const/16 v2, 0x24

    goto :goto_1

    .line 527
    :cond_3
    const/16 v7, 0x6a

    if-ne v5, v7, :cond_4

    .line 528
    const/16 v2, 0x81

    goto :goto_1

    .line 529
    :cond_4
    const/16 v7, 0x7a

    if-ne v5, v7, :cond_5

    .line 530
    const/16 v2, 0x87

    goto :goto_1

    .line 531
    :cond_5
    const/16 v7, 0x7b

    if-ne v5, v7, :cond_6

    .line 532
    const/16 v2, 0x8a

    goto :goto_1

    .line 533
    :cond_6
    const/16 v7, 0xa

    if-ne v5, v7, :cond_7

    .line 534
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, Lcom/google/android/exoplayer2/util/k;->e(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 536
    :cond_7
    const/16 v7, 0x59

    if-ne v5, v7, :cond_0

    .line 537
    const/16 v2, 0x59

    .line 538
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 539
    :goto_2
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->d()I

    move-result v5

    if-ge v5, v6, :cond_0

    .line 540
    const/4 v5, 0x3

    invoke-virtual {p1, v5}, Lcom/google/android/exoplayer2/util/k;->e(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 541
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->g()I

    move-result v7

    .line 542
    const/4 v8, 0x4

    new-array v8, v8, [B

    .line 543
    const/4 v9, 0x0

    const/4 v10, 0x4

    invoke-virtual {p1, v8, v9, v10}, Lcom/google/android/exoplayer2/util/k;->a([BII)V

    .line 544
    new-instance v9, Lcom/google/android/exoplayer2/extractor/e/v$a;

    invoke-direct {v9, v5, v7, v8}, Lcom/google/android/exoplayer2/extractor/e/v$a;-><init>(Ljava/lang/String;I[B)V

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 551
    :cond_8
    invoke-virtual {p1, v4}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 552
    new-instance v5, Lcom/google/android/exoplayer2/extractor/e/v$b;

    iget-object v6, p1, Lcom/google/android/exoplayer2/util/k;->a:[B

    .line 553
    invoke-static {v6, v3, v4}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v3

    invoke-direct {v5, v2, v1, v0, v3}, Lcom/google/android/exoplayer2/extractor/e/v$b;-><init>(ILjava/lang/String;Ljava/util/List;[B)V

    return-object v5
.end method


# virtual methods
.method public a(Lcom/google/android/exoplayer2/util/k;)V
    .locals 13

    .prologue
    const/16 v12, 0x2000

    const/16 v11, 0x15

    const/4 v10, 0x1

    const/4 v4, 0x0

    const/4 v9, 0x2

    .line 401
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->g()I

    move-result v0

    .line 402
    if-eq v0, v9, :cond_1

    .line 498
    :cond_0
    :goto_0
    return-void

    .line 408
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/e/u;->c(Lcom/google/android/exoplayer2/extractor/e/u;)I

    move-result v0

    if-eq v0, v10, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/e/u;->c(Lcom/google/android/exoplayer2/extractor/e/u;)I

    move-result v0

    if-eq v0, v9, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/e/u;->d(Lcom/google/android/exoplayer2/extractor/e/u;)I

    move-result v0

    if-ne v0, v10, :cond_5

    .line 409
    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/e/u;->e(Lcom/google/android/exoplayer2/extractor/e/u;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/util/p;

    move-object v1, v0

    .line 417
    :goto_1
    invoke-virtual {p1, v9}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 418
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->h()I

    move-result v6

    .line 421
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 424
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->b:Lcom/google/android/exoplayer2/util/j;

    invoke-virtual {p1, v0, v9}, Lcom/google/android/exoplayer2/util/k;->a(Lcom/google/android/exoplayer2/util/j;I)V

    .line 425
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->b:Lcom/google/android/exoplayer2/util/j;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/util/j;->b(I)V

    .line 426
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->b:Lcom/google/android/exoplayer2/util/j;

    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer2/util/j;->c(I)I

    move-result v0

    .line 429
    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/util/k;->d(I)V

    .line 431
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/e/u;->c(Lcom/google/android/exoplayer2/extractor/e/u;)I

    move-result v0

    if-ne v0, v9, :cond_3

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/e/u;->f(Lcom/google/android/exoplayer2/extractor/e/u;)Lcom/google/android/exoplayer2/extractor/e/v;

    move-result-object v0

    if-nez v0, :cond_3

    .line 434
    new-instance v0, Lcom/google/android/exoplayer2/extractor/e/v$b;

    const/4 v2, 0x0

    const/4 v3, 0x0

    new-array v5, v4, [B

    invoke-direct {v0, v11, v2, v3, v5}, Lcom/google/android/exoplayer2/extractor/e/v$b;-><init>(ILjava/lang/String;Ljava/util/List;[B)V

    .line 435
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v3}, Lcom/google/android/exoplayer2/extractor/e/u;->g(Lcom/google/android/exoplayer2/extractor/e/u;)Lcom/google/android/exoplayer2/extractor/e/v$c;

    move-result-object v3

    invoke-interface {v3, v11, v0}, Lcom/google/android/exoplayer2/extractor/e/v$c;->a(ILcom/google/android/exoplayer2/extractor/e/v$b;)Lcom/google/android/exoplayer2/extractor/e/v;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/exoplayer2/extractor/e/u;->a(Lcom/google/android/exoplayer2/extractor/e/u;Lcom/google/android/exoplayer2/extractor/e/v;)Lcom/google/android/exoplayer2/extractor/e/v;

    .line 436
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/e/u;->f(Lcom/google/android/exoplayer2/extractor/e/u;)Lcom/google/android/exoplayer2/extractor/e/v;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v2}, Lcom/google/android/exoplayer2/extractor/e/u;->h(Lcom/google/android/exoplayer2/extractor/e/u;)Lcom/google/android/exoplayer2/extractor/g;

    move-result-object v2

    new-instance v3, Lcom/google/android/exoplayer2/extractor/e/v$d;

    invoke-direct {v3, v6, v11, v12}, Lcom/google/android/exoplayer2/extractor/e/v$d;-><init>(III)V

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/exoplayer2/extractor/e/v;->a(Lcom/google/android/exoplayer2/util/p;Lcom/google/android/exoplayer2/extractor/g;Lcom/google/android/exoplayer2/extractor/e/v$d;)V

    .line 440
    :cond_3
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 441
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->d:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 442
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->b()I

    move-result v0

    move v2, v0

    .line 443
    :goto_2
    if-lez v2, :cond_b

    .line 444
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->b:Lcom/google/android/exoplayer2/util/j;

    const/4 v3, 0x5

    invoke-virtual {p1, v0, v3}, Lcom/google/android/exoplayer2/util/k;->a(Lcom/google/android/exoplayer2/util/j;I)V

    .line 445
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->b:Lcom/google/android/exoplayer2/util/j;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Lcom/google/android/exoplayer2/util/j;->c(I)I

    move-result v0

    .line 446
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->b:Lcom/google/android/exoplayer2/util/j;

    const/4 v5, 0x3

    invoke-virtual {v3, v5}, Lcom/google/android/exoplayer2/util/j;->b(I)V

    .line 447
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->b:Lcom/google/android/exoplayer2/util/j;

    const/16 v5, 0xd

    invoke-virtual {v3, v5}, Lcom/google/android/exoplayer2/util/j;->c(I)I

    move-result v3

    .line 448
    iget-object v5, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->b:Lcom/google/android/exoplayer2/util/j;

    const/4 v7, 0x4

    invoke-virtual {v5, v7}, Lcom/google/android/exoplayer2/util/j;->b(I)V

    .line 449
    iget-object v5, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->b:Lcom/google/android/exoplayer2/util/j;

    const/16 v7, 0xc

    invoke-virtual {v5, v7}, Lcom/google/android/exoplayer2/util/j;->c(I)I

    move-result v5

    .line 450
    invoke-direct {p0, p1, v5}, Lcom/google/android/exoplayer2/extractor/e/u$b;->a(Lcom/google/android/exoplayer2/util/k;I)Lcom/google/android/exoplayer2/extractor/e/v$b;

    move-result-object v7

    .line 451
    const/4 v8, 0x6

    if-ne v0, v8, :cond_4

    .line 452
    iget v0, v7, Lcom/google/android/exoplayer2/extractor/e/v$b;->a:I

    .line 454
    :cond_4
    add-int/lit8 v5, v5, 0x5

    sub-int v5, v2, v5

    .line 456
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v2}, Lcom/google/android/exoplayer2/extractor/e/u;->c(Lcom/google/android/exoplayer2/extractor/e/u;)I

    move-result v2

    if-ne v2, v9, :cond_6

    move v2, v0

    .line 457
    :goto_3
    iget-object v8, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v8}, Lcom/google/android/exoplayer2/extractor/e/u;->i(Lcom/google/android/exoplayer2/extractor/e/u;)Landroid/util/SparseBooleanArray;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v8

    if-eqz v8, :cond_7

    move v2, v5

    .line 458
    goto :goto_2

    .line 411
    :cond_5
    new-instance v1, Lcom/google/android/exoplayer2/util/p;

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    .line 412
    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/e/u;->e(Lcom/google/android/exoplayer2/extractor/e/u;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/util/p;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/util/p;->a()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/google/android/exoplayer2/util/p;-><init>(J)V

    .line 413
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/e/u;->e(Lcom/google/android/exoplayer2/extractor/e/u;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_6
    move v2, v3

    .line 456
    goto :goto_3

    .line 461
    :cond_7
    iget-object v8, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v8}, Lcom/google/android/exoplayer2/extractor/e/u;->c(Lcom/google/android/exoplayer2/extractor/e/u;)I

    move-result v8

    if-ne v8, v9, :cond_a

    if-ne v0, v11, :cond_a

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/e/u;->f(Lcom/google/android/exoplayer2/extractor/e/u;)Lcom/google/android/exoplayer2/extractor/e/v;

    move-result-object v0

    .line 463
    :goto_4
    iget-object v7, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v7}, Lcom/google/android/exoplayer2/extractor/e/u;->c(Lcom/google/android/exoplayer2/extractor/e/u;)I

    move-result v7

    if-ne v7, v9, :cond_8

    iget-object v7, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->d:Landroid/util/SparseIntArray;

    .line 464
    invoke-virtual {v7, v2, v12}, Landroid/util/SparseIntArray;->get(II)I

    move-result v7

    if-ge v3, v7, :cond_9

    .line 465
    :cond_8
    iget-object v7, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->d:Landroid/util/SparseIntArray;

    invoke-virtual {v7, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 466
    iget-object v3, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->c:Landroid/util/SparseArray;

    invoke-virtual {v3, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_9
    move v2, v5

    .line 468
    goto/16 :goto_2

    .line 461
    :cond_a
    iget-object v8, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    .line 462
    invoke-static {v8}, Lcom/google/android/exoplayer2/extractor/e/u;->g(Lcom/google/android/exoplayer2/extractor/e/u;)Lcom/google/android/exoplayer2/extractor/e/v$c;

    move-result-object v8

    invoke-interface {v8, v0, v7}, Lcom/google/android/exoplayer2/extractor/e/v$c;->a(ILcom/google/android/exoplayer2/extractor/e/v$b;)Lcom/google/android/exoplayer2/extractor/e/v;

    move-result-object v0

    goto :goto_4

    .line 470
    :cond_b
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->d:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v3

    move v2, v4

    .line 471
    :goto_5
    if-ge v2, v3, :cond_e

    .line 472
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->d:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v5

    .line 473
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/e/u;->i(Lcom/google/android/exoplayer2/extractor/e/u;)Landroid/util/SparseBooleanArray;

    move-result-object v0

    invoke-virtual {v0, v5, v10}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 474
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/extractor/e/v;

    .line 475
    if-eqz v0, :cond_d

    .line 476
    iget-object v7, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v7}, Lcom/google/android/exoplayer2/extractor/e/u;->f(Lcom/google/android/exoplayer2/extractor/e/u;)Lcom/google/android/exoplayer2/extractor/e/v;

    move-result-object v7

    if-eq v0, v7, :cond_c

    .line 477
    iget-object v7, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v7}, Lcom/google/android/exoplayer2/extractor/e/u;->h(Lcom/google/android/exoplayer2/extractor/e/u;)Lcom/google/android/exoplayer2/extractor/g;

    move-result-object v7

    new-instance v8, Lcom/google/android/exoplayer2/extractor/e/v$d;

    invoke-direct {v8, v6, v5, v12}, Lcom/google/android/exoplayer2/extractor/e/v$d;-><init>(III)V

    invoke-interface {v0, v1, v7, v8}, Lcom/google/android/exoplayer2/extractor/e/v;->a(Lcom/google/android/exoplayer2/util/p;Lcom/google/android/exoplayer2/extractor/g;Lcom/google/android/exoplayer2/extractor/e/v$d;)V

    .line 480
    :cond_c
    iget-object v5, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v5}, Lcom/google/android/exoplayer2/extractor/e/u;->a(Lcom/google/android/exoplayer2/extractor/e/u;)Landroid/util/SparseArray;

    move-result-object v5

    iget-object v7, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->d:Landroid/util/SparseIntArray;

    invoke-virtual {v7, v2}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v7

    invoke-virtual {v5, v7, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 471
    :cond_d
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 484
    :cond_e
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/e/u;->c(Lcom/google/android/exoplayer2/extractor/e/u;)I

    move-result v0

    if-ne v0, v9, :cond_f

    .line 485
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/e/u;->j(Lcom/google/android/exoplayer2/extractor/e/u;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 486
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/e/u;->h(Lcom/google/android/exoplayer2/extractor/e/u;)Lcom/google/android/exoplayer2/extractor/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/exoplayer2/extractor/g;->a()V

    .line 487
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v0, v4}, Lcom/google/android/exoplayer2/extractor/e/u;->a(Lcom/google/android/exoplayer2/extractor/e/u;I)I

    .line 488
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v0, v10}, Lcom/google/android/exoplayer2/extractor/e/u;->a(Lcom/google/android/exoplayer2/extractor/e/u;Z)Z

    goto/16 :goto_0

    .line 491
    :cond_f
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/e/u;->a(Lcom/google/android/exoplayer2/extractor/e/u;)Landroid/util/SparseArray;

    move-result-object v0

    iget v1, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->e:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->remove(I)V

    .line 492
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/e/u;->c(Lcom/google/android/exoplayer2/extractor/e/u;)I

    move-result v0

    if-ne v0, v10, :cond_10

    move v0, v4

    :goto_6
    invoke-static {v1, v0}, Lcom/google/android/exoplayer2/extractor/e/u;->a(Lcom/google/android/exoplayer2/extractor/e/u;I)I

    .line 493
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/e/u;->d(Lcom/google/android/exoplayer2/extractor/e/u;)I

    move-result v0

    if-nez v0, :cond_0

    .line 494
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/e/u;->h(Lcom/google/android/exoplayer2/extractor/e/u;)Lcom/google/android/exoplayer2/extractor/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/exoplayer2/extractor/g;->a()V

    .line 495
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v0, v10}, Lcom/google/android/exoplayer2/extractor/e/u;->a(Lcom/google/android/exoplayer2/extractor/e/u;Z)Z

    goto/16 :goto_0

    .line 492
    :cond_10
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/u$b;->a:Lcom/google/android/exoplayer2/extractor/e/u;

    invoke-static {v0}, Lcom/google/android/exoplayer2/extractor/e/u;->d(Lcom/google/android/exoplayer2/extractor/e/u;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_6
.end method

.method public a(Lcom/google/android/exoplayer2/util/p;Lcom/google/android/exoplayer2/extractor/g;Lcom/google/android/exoplayer2/extractor/e/v$d;)V
    .locals 0

    .prologue
    .line 397
    return-void
.end method
