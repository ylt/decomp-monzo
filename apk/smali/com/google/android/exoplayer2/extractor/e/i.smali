.class public final Lcom/google/android/exoplayer2/extractor/e/i;
.super Ljava/lang/Object;
.source "H262Reader.java"

# interfaces
.implements Lcom/google/android/exoplayer2/extractor/e/h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/extractor/e/i$a;
    }
.end annotation


# static fields
.field private static final c:[D


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/google/android/exoplayer2/extractor/m;

.field private d:Z

.field private e:J

.field private final f:[Z

.field private final g:Lcom/google/android/exoplayer2/extractor/e/i$a;

.field private h:J

.field private i:Z

.field private j:J

.field private k:J

.field private l:J

.field private m:Z

.field private n:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/16 v0, 0x8

    new-array v0, v0, [D

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/exoplayer2/extractor/e/i;->c:[D

    return-void

    :array_0
    .array-data 8
        0x4037f9dcb5112287L    # 23.976023976023978
        0x4038000000000000L    # 24.0
        0x4039000000000000L    # 25.0
        0x403df853e2556b28L    # 29.97002997002997
        0x403e000000000000L    # 30.0
        0x4049000000000000L    # 50.0
        0x404df853e2556b28L    # 59.94005994005994
        0x404e000000000000L    # 60.0
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const/4 v0, 0x4

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->f:[Z

    .line 68
    new-instance v0, Lcom/google/android/exoplayer2/extractor/e/i$a;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/extractor/e/i$a;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->g:Lcom/google/android/exoplayer2/extractor/e/i$a;

    .line 69
    return-void
.end method

.method private static a(Lcom/google/android/exoplayer2/extractor/e/i$a;Ljava/lang/String;)Landroid/util/Pair;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/extractor/e/i$a;",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/exoplayer2/j;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v13, 0x7

    const/4 v3, -0x1

    .line 174
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/i$a;->c:[B

    iget v1, p0, Lcom/google/android/exoplayer2/extractor/e/i$a;->a:I

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v12

    .line 176
    const/4 v0, 0x4

    aget-byte v0, v12, v0

    and-int/lit16 v0, v0, 0xff

    .line 177
    const/4 v1, 0x5

    aget-byte v1, v12, v1

    and-int/lit16 v1, v1, 0xff

    .line 178
    const/4 v4, 0x6

    aget-byte v4, v12, v4

    and-int/lit16 v4, v4, 0xff

    .line 179
    shl-int/lit8 v0, v0, 0x4

    shr-int/lit8 v5, v1, 0x4

    or-int/2addr v5, v0

    .line 180
    and-int/lit8 v0, v1, 0xf

    shl-int/lit8 v0, v0, 0x8

    or-int v6, v0, v4

    .line 182
    const/high16 v10, 0x3f800000    # 1.0f

    .line 183
    aget-byte v0, v12, v13

    and-int/lit16 v0, v0, 0xf0

    shr-int/lit8 v0, v0, 0x4

    .line 184
    packed-switch v0, :pswitch_data_0

    .line 199
    :goto_0
    const-string v1, "video/mpeg2"

    const/high16 v7, -0x40800000    # -1.0f

    .line 201
    invoke-static {v12}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    move-object v0, p1

    move v4, v3

    move v9, v3

    move-object v11, v2

    .line 199
    invoke-static/range {v0 .. v11}, Lcom/google/android/exoplayer2/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIIFLjava/util/List;IFLcom/google/android/exoplayer2/drm/a;)Lcom/google/android/exoplayer2/j;

    move-result-object v2

    .line 203
    const-wide/16 v0, 0x0

    .line 204
    aget-byte v3, v12, v13

    and-int/lit8 v3, v3, 0xf

    add-int/lit8 v3, v3, -0x1

    .line 205
    if-ltz v3, :cond_1

    sget-object v4, Lcom/google/android/exoplayer2/extractor/e/i;->c:[D

    array-length v4, v4

    if-ge v3, v4, :cond_1

    .line 206
    sget-object v0, Lcom/google/android/exoplayer2/extractor/e/i;->c:[D

    aget-wide v0, v0, v3

    .line 207
    iget v3, p0, Lcom/google/android/exoplayer2/extractor/e/i$a;->b:I

    .line 208
    add-int/lit8 v4, v3, 0x9

    aget-byte v4, v12, v4

    and-int/lit8 v4, v4, 0x60

    shr-int/lit8 v4, v4, 0x5

    .line 209
    add-int/lit8 v3, v3, 0x9

    aget-byte v3, v12, v3

    and-int/lit8 v3, v3, 0x1f

    .line 210
    if-eq v4, v3, :cond_0

    .line 211
    int-to-double v4, v4

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    add-double/2addr v4, v6

    add-int/lit8 v3, v3, 0x1

    int-to-double v6, v3

    div-double/2addr v4, v6

    mul-double/2addr v0, v4

    .line 213
    :cond_0
    const-wide v4, 0x412e848000000000L    # 1000000.0

    div-double v0, v4, v0

    double-to-long v0, v0

    .line 216
    :cond_1
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    .line 186
    :pswitch_0
    mul-int/lit8 v0, v6, 0x4

    int-to-float v0, v0

    mul-int/lit8 v1, v5, 0x3

    int-to-float v1, v1

    div-float v10, v0, v1

    .line 187
    goto :goto_0

    .line 189
    :pswitch_1
    mul-int/lit8 v0, v6, 0x10

    int-to-float v0, v0

    mul-int/lit8 v1, v5, 0x9

    int-to-float v1, v1

    div-float v10, v0, v1

    .line 190
    goto :goto_0

    .line 192
    :pswitch_2
    mul-int/lit8 v0, v6, 0x79

    int-to-float v0, v0

    mul-int/lit8 v1, v5, 0x64

    int-to-float v1, v1

    div-float v10, v0, v1

    .line 193
    goto :goto_0

    .line 184
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->f:[Z

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/i;->a([Z)V

    .line 74
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->g:Lcom/google/android/exoplayer2/extractor/e/i$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/extractor/e/i$a;->a()V

    .line 75
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->h:J

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->i:Z

    .line 77
    return-void
.end method

.method public a(JZ)V
    .locals 1

    .prologue
    .line 88
    iput-wide p1, p0, Lcom/google/android/exoplayer2/extractor/e/i;->j:J

    .line 89
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/g;Lcom/google/android/exoplayer2/extractor/e/v$d;)V
    .locals 2

    .prologue
    .line 81
    invoke-virtual {p2}, Lcom/google/android/exoplayer2/extractor/e/v$d;->a()V

    .line 82
    invoke-virtual {p2}, Lcom/google/android/exoplayer2/extractor/e/v$d;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->a:Ljava/lang/String;

    .line 83
    invoke-virtual {p2}, Lcom/google/android/exoplayer2/extractor/e/v$d;->b()I

    move-result v0

    const/4 v1, 0x2

    invoke-interface {p1, v0, v1}, Lcom/google/android/exoplayer2/extractor/g;->a(II)Lcom/google/android/exoplayer2/extractor/m;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->b:Lcom/google/android/exoplayer2/extractor/m;

    .line 84
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/util/k;)V
    .locals 12

    .prologue
    .line 93
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->d()I

    move-result v0

    .line 94
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->c()I

    move-result v8

    .line 95
    iget-object v9, p1, Lcom/google/android/exoplayer2/util/k;->a:[B

    .line 98
    iget-wide v2, p0, Lcom/google/android/exoplayer2/extractor/e/i;->h:J

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->b()I

    move-result v1

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/exoplayer2/extractor/e/i;->h:J

    .line 99
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/e/i;->b:Lcom/google/android/exoplayer2/extractor/m;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/util/k;->b()I

    move-result v2

    invoke-interface {v1, p1, v2}, Lcom/google/android/exoplayer2/extractor/m;->a(Lcom/google/android/exoplayer2/util/k;I)V

    .line 102
    :goto_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/e/i;->f:[Z

    invoke-static {v9, v0, v8, v1}, Lcom/google/android/exoplayer2/util/i;->a([BII[Z)I

    move-result v10

    .line 104
    if-ne v10, v8, :cond_1

    .line 106
    iget-boolean v1, p0, Lcom/google/android/exoplayer2/extractor/e/i;->d:Z

    if-nez v1, :cond_0

    .line 107
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/e/i;->g:Lcom/google/android/exoplayer2/extractor/e/i$a;

    invoke-virtual {v1, v9, v0, v8}, Lcom/google/android/exoplayer2/extractor/e/i$a;->a([BII)V

    .line 109
    :cond_0
    return-void

    .line 113
    :cond_1
    iget-object v1, p1, Lcom/google/android/exoplayer2/util/k;->a:[B

    add-int/lit8 v2, v10, 0x3

    aget-byte v1, v1, v2

    and-int/lit16 v11, v1, 0xff

    .line 115
    iget-boolean v1, p0, Lcom/google/android/exoplayer2/extractor/e/i;->d:Z

    if-nez v1, :cond_3

    .line 118
    sub-int v1, v10, v0

    .line 119
    if-lez v1, :cond_2

    .line 120
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/e/i;->g:Lcom/google/android/exoplayer2/extractor/e/i$a;

    invoke-virtual {v2, v9, v0, v10}, Lcom/google/android/exoplayer2/extractor/e/i$a;->a([BII)V

    .line 124
    :cond_2
    if-gez v1, :cond_9

    neg-int v0, v1

    .line 125
    :goto_1
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/e/i;->g:Lcom/google/android/exoplayer2/extractor/e/i$a;

    invoke-virtual {v1, v11, v0}, Lcom/google/android/exoplayer2/extractor/e/i$a;->a(II)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 127
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->g:Lcom/google/android/exoplayer2/extractor/e/i$a;

    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/e/i;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/extractor/e/i;->a(Lcom/google/android/exoplayer2/extractor/e/i$a;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v1

    .line 128
    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/e/i;->b:Lcom/google/android/exoplayer2/extractor/m;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/j;

    invoke-interface {v2, v0}, Lcom/google/android/exoplayer2/extractor/m;->a(Lcom/google/android/exoplayer2/j;)V

    .line 129
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->e:J

    .line 130
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->d:Z

    .line 134
    :cond_3
    if-eqz v11, :cond_4

    const/16 v0, 0xb3

    if-ne v11, v0, :cond_e

    .line 135
    :cond_4
    sub-int v6, v8, v10

    .line 136
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->i:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->n:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->d:Z

    if-eqz v0, :cond_5

    .line 138
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->m:Z

    if-eqz v0, :cond_a

    const/4 v4, 0x1

    .line 139
    :goto_2
    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->h:J

    iget-wide v2, p0, Lcom/google/android/exoplayer2/extractor/e/i;->k:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    sub-int v5, v0, v6

    .line 140
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/e/i;->b:Lcom/google/android/exoplayer2/extractor/m;

    iget-wide v2, p0, Lcom/google/android/exoplayer2/extractor/e/i;->l:J

    const/4 v7, 0x0

    invoke-interface/range {v1 .. v7}, Lcom/google/android/exoplayer2/extractor/m;->a(JIIILcom/google/android/exoplayer2/extractor/m$a;)V

    .line 142
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->i:Z

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->n:Z

    if-eqz v0, :cond_7

    .line 144
    :cond_6
    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->h:J

    int-to-long v2, v6

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->k:J

    .line 145
    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->j:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, v0, v2

    if-eqz v0, :cond_b

    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->j:J

    :goto_3
    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->l:J

    .line 147
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->m:Z

    .line 148
    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->j:J

    .line 149
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->i:Z

    .line 151
    :cond_7
    if-nez v11, :cond_d

    const/4 v0, 0x1

    :goto_4
    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->n:Z

    .line 156
    :cond_8
    :goto_5
    add-int/lit8 v0, v10, 0x3

    .line 157
    goto/16 :goto_0

    .line 124
    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 138
    :cond_a
    const/4 v4, 0x0

    goto :goto_2

    .line 145
    :cond_b
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->i:Z

    if-eqz v0, :cond_c

    iget-wide v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->l:J

    iget-wide v2, p0, Lcom/google/android/exoplayer2/extractor/e/i;->e:J

    add-long/2addr v0, v2

    goto :goto_3

    :cond_c
    const-wide/16 v0, 0x0

    goto :goto_3

    .line 151
    :cond_d
    const/4 v0, 0x0

    goto :goto_4

    .line 152
    :cond_e
    const/16 v0, 0xb8

    if-ne v11, v0, :cond_8

    .line 153
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/e/i;->m:Z

    goto :goto_5
.end method

.method public b()V
    .locals 0

    .prologue
    .line 163
    return-void
.end method
