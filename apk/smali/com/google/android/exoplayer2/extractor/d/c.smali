.class public Lcom/google/android/exoplayer2/extractor/d/c;
.super Ljava/lang/Object;
.source "OggExtractor.java"

# interfaces
.implements Lcom/google/android/exoplayer2/extractor/e;


# static fields
.field public static final a:Lcom/google/android/exoplayer2/extractor/h;


# instance fields
.field private b:Lcom/google/android/exoplayer2/extractor/g;

.field private c:Lcom/google/android/exoplayer2/extractor/d/h;

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lcom/google/android/exoplayer2/extractor/d/c$1;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/extractor/d/c$1;-><init>()V

    sput-object v0, Lcom/google/android/exoplayer2/extractor/d/c;->a:Lcom/google/android/exoplayer2/extractor/h;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/google/android/exoplayer2/util/k;)Lcom/google/android/exoplayer2/util/k;
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/util/k;->c(I)V

    .line 120
    return-object p0
.end method

.method private b(Lcom/google/android/exoplayer2/extractor/f;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 97
    new-instance v2, Lcom/google/android/exoplayer2/extractor/d/e;

    invoke-direct {v2}, Lcom/google/android/exoplayer2/extractor/d/e;-><init>()V

    .line 98
    invoke-virtual {v2, p1, v1}, Lcom/google/android/exoplayer2/extractor/d/e;->a(Lcom/google/android/exoplayer2/extractor/f;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    iget v3, v2, Lcom/google/android/exoplayer2/extractor/d/e;->b:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    .line 115
    :cond_0
    :goto_0
    return v0

    .line 102
    :cond_1
    iget v2, v2, Lcom/google/android/exoplayer2/extractor/d/e;->i:I

    const/16 v3, 0x8

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 103
    new-instance v3, Lcom/google/android/exoplayer2/util/k;

    invoke-direct {v3, v2}, Lcom/google/android/exoplayer2/util/k;-><init>(I)V

    .line 104
    iget-object v4, v3, Lcom/google/android/exoplayer2/util/k;->a:[B

    invoke-interface {p1, v4, v0, v2}, Lcom/google/android/exoplayer2/extractor/f;->c([BII)V

    .line 106
    invoke-static {v3}, Lcom/google/android/exoplayer2/extractor/d/c;->a(Lcom/google/android/exoplayer2/util/k;)Lcom/google/android/exoplayer2/util/k;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/exoplayer2/extractor/d/b;->a(Lcom/google/android/exoplayer2/util/k;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 107
    new-instance v0, Lcom/google/android/exoplayer2/extractor/d/b;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/extractor/d/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/d/c;->c:Lcom/google/android/exoplayer2/extractor/d/h;

    :goto_1
    move v0, v1

    .line 115
    goto :goto_0

    .line 108
    :cond_2
    invoke-static {v3}, Lcom/google/android/exoplayer2/extractor/d/c;->a(Lcom/google/android/exoplayer2/util/k;)Lcom/google/android/exoplayer2/util/k;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/exoplayer2/extractor/d/j;->a(Lcom/google/android/exoplayer2/util/k;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 109
    new-instance v0, Lcom/google/android/exoplayer2/extractor/d/j;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/extractor/d/j;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/d/c;->c:Lcom/google/android/exoplayer2/extractor/d/h;

    goto :goto_1

    .line 110
    :cond_3
    invoke-static {v3}, Lcom/google/android/exoplayer2/extractor/d/c;->a(Lcom/google/android/exoplayer2/util/k;)Lcom/google/android/exoplayer2/util/k;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/exoplayer2/extractor/d/g;->a(Lcom/google/android/exoplayer2/util/k;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 111
    new-instance v0, Lcom/google/android/exoplayer2/extractor/d/g;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/extractor/d/g;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/extractor/d/c;->c:Lcom/google/android/exoplayer2/extractor/d/h;

    goto :goto_1
.end method


# virtual methods
.method public a(Lcom/google/android/exoplayer2/extractor/f;Lcom/google/android/exoplayer2/extractor/k;)I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 81
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/d/c;->c:Lcom/google/android/exoplayer2/extractor/d/h;

    if-nez v0, :cond_1

    .line 82
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/d/c;->b(Lcom/google/android/exoplayer2/extractor/f;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    new-instance v0, Lcom/google/android/exoplayer2/ParserException;

    const-string v1, "Failed to determine bitstream type"

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_0
    invoke-interface {p1}, Lcom/google/android/exoplayer2/extractor/f;->a()V

    .line 87
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/extractor/d/c;->d:Z

    if-nez v0, :cond_2

    .line 88
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/d/c;->b:Lcom/google/android/exoplayer2/extractor/g;

    const/4 v1, 0x0

    invoke-interface {v0, v1, v3}, Lcom/google/android/exoplayer2/extractor/g;->a(II)Lcom/google/android/exoplayer2/extractor/m;

    move-result-object v0

    .line 89
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/d/c;->b:Lcom/google/android/exoplayer2/extractor/g;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/extractor/g;->a()V

    .line 90
    iget-object v1, p0, Lcom/google/android/exoplayer2/extractor/d/c;->c:Lcom/google/android/exoplayer2/extractor/d/h;

    iget-object v2, p0, Lcom/google/android/exoplayer2/extractor/d/c;->b:Lcom/google/android/exoplayer2/extractor/g;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/exoplayer2/extractor/d/h;->a(Lcom/google/android/exoplayer2/extractor/g;Lcom/google/android/exoplayer2/extractor/m;)V

    .line 91
    iput-boolean v3, p0, Lcom/google/android/exoplayer2/extractor/d/c;->d:Z

    .line 93
    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/d/c;->c:Lcom/google/android/exoplayer2/extractor/d/h;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/extractor/d/h;->a(Lcom/google/android/exoplayer2/extractor/f;Lcom/google/android/exoplayer2/extractor/k;)I

    move-result v0

    return v0
.end method

.method public a(JJ)V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/d/c;->c:Lcom/google/android/exoplayer2/extractor/d/h;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/google/android/exoplayer2/extractor/d/c;->c:Lcom/google/android/exoplayer2/extractor/d/h;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/exoplayer2/extractor/d/h;->a(JJ)V

    .line 71
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/g;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/android/exoplayer2/extractor/d/c;->b:Lcom/google/android/exoplayer2/extractor/g;

    .line 64
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/extractor/f;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 55
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/extractor/d/c;->b(Lcom/google/android/exoplayer2/extractor/f;)Z
    :try_end_0
    .catch Lcom/google/android/exoplayer2/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 57
    :goto_0
    return v0

    .line 56
    :catch_0
    move-exception v0

    .line 57
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 76
    return-void
.end method
