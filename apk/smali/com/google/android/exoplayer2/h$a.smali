.class final Lcom/google/android/exoplayer2/h$a;
.super Ljava/lang/Object;
.source "ExoPlayerImplInternal.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field public final a:Lcom/google/android/exoplayer2/source/h;

.field public final b:Ljava/lang/Object;

.field public final c:I

.field public final d:[Lcom/google/android/exoplayer2/source/m;

.field public final e:[Z

.field public final f:J

.field public g:Lcom/google/android/exoplayer2/m$a;

.field public h:Z

.field public i:Z

.field public j:Lcom/google/android/exoplayer2/h$a;

.field public k:Lcom/google/android/exoplayer2/b/h;

.field private final l:[Lcom/google/android/exoplayer2/p;

.field private final m:[Lcom/google/android/exoplayer2/q;

.field private final n:Lcom/google/android/exoplayer2/b/g;

.field private final o:Lcom/google/android/exoplayer2/l;

.field private final p:Lcom/google/android/exoplayer2/source/i;

.field private q:Lcom/google/android/exoplayer2/b/h;


# direct methods
.method public constructor <init>([Lcom/google/android/exoplayer2/p;[Lcom/google/android/exoplayer2/q;JLcom/google/android/exoplayer2/b/g;Lcom/google/android/exoplayer2/l;Lcom/google/android/exoplayer2/source/i;Ljava/lang/Object;ILcom/google/android/exoplayer2/m$a;)V
    .locals 9

    .prologue
    .line 1521
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1522
    iput-object p1, p0, Lcom/google/android/exoplayer2/h$a;->l:[Lcom/google/android/exoplayer2/p;

    .line 1523
    iput-object p2, p0, Lcom/google/android/exoplayer2/h$a;->m:[Lcom/google/android/exoplayer2/q;

    .line 1524
    iput-wide p3, p0, Lcom/google/android/exoplayer2/h$a;->f:J

    .line 1525
    iput-object p5, p0, Lcom/google/android/exoplayer2/h$a;->n:Lcom/google/android/exoplayer2/b/g;

    .line 1526
    iput-object p6, p0, Lcom/google/android/exoplayer2/h$a;->o:Lcom/google/android/exoplayer2/l;

    .line 1527
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/exoplayer2/h$a;->p:Lcom/google/android/exoplayer2/source/i;

    .line 1528
    invoke-static/range {p8 .. p8}, Lcom/google/android/exoplayer2/util/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/exoplayer2/h$a;->b:Ljava/lang/Object;

    .line 1529
    move/from16 v0, p9

    iput v0, p0, Lcom/google/android/exoplayer2/h$a;->c:I

    .line 1530
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    .line 1531
    array-length v2, p1

    new-array v2, v2, [Lcom/google/android/exoplayer2/source/m;

    iput-object v2, p0, Lcom/google/android/exoplayer2/h$a;->d:[Lcom/google/android/exoplayer2/source/m;

    .line 1532
    array-length v2, p1

    new-array v2, v2, [Z

    iput-object v2, p0, Lcom/google/android/exoplayer2/h$a;->e:[Z

    .line 1533
    move-object/from16 v0, p10

    iget-object v2, v0, Lcom/google/android/exoplayer2/m$a;->a:Lcom/google/android/exoplayer2/source/i$b;

    invoke-interface {p6}, Lcom/google/android/exoplayer2/l;->d()Lcom/google/android/exoplayer2/upstream/b;

    move-result-object v3

    move-object/from16 v0, p7

    invoke-interface {v0, v2, v3}, Lcom/google/android/exoplayer2/source/i;->a(Lcom/google/android/exoplayer2/source/i$b;Lcom/google/android/exoplayer2/upstream/b;)Lcom/google/android/exoplayer2/source/h;

    move-result-object v3

    .line 1534
    move-object/from16 v0, p10

    iget-wide v4, v0, Lcom/google/android/exoplayer2/m$a;->c:J

    const-wide/high16 v6, -0x8000000000000000L

    cmp-long v2, v4, v6

    if-eqz v2, :cond_0

    .line 1535
    new-instance v2, Lcom/google/android/exoplayer2/source/c;

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Lcom/google/android/exoplayer2/source/c;-><init>(Lcom/google/android/exoplayer2/source/h;Z)V

    .line 1536
    const-wide/16 v4, 0x0

    move-object/from16 v0, p10

    iget-wide v6, v0, Lcom/google/android/exoplayer2/m$a;->c:J

    invoke-virtual {v2, v4, v5, v6, v7}, Lcom/google/android/exoplayer2/source/c;->a(JJ)V

    .line 1539
    :goto_0
    iput-object v2, p0, Lcom/google/android/exoplayer2/h$a;->a:Lcom/google/android/exoplayer2/source/h;

    .line 1540
    return-void

    :cond_0
    move-object v2, v3

    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 4

    .prologue
    .line 1551
    iget v0, p0, Lcom/google/android/exoplayer2/h$a;->c:I

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/exoplayer2/h$a;->f:J

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/google/android/exoplayer2/h$a;->f:J

    iget-object v2, p0, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-wide v2, v2, Lcom/google/android/exoplayer2/m$a;->b:J

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method public a(J)J
    .locals 3

    .prologue
    .line 1543
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/h$a;->a()J

    move-result-wide v0

    add-long/2addr v0, p1

    return-wide v0
.end method

.method public a(JZ)J
    .locals 3

    .prologue
    .line 1607
    iget-object v0, p0, Lcom/google/android/exoplayer2/h$a;->l:[Lcom/google/android/exoplayer2/p;

    array-length v0, v0

    new-array v0, v0, [Z

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/exoplayer2/h$a;->a(JZ[Z)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(JZ[Z)J
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1613
    iget-object v0, p0, Lcom/google/android/exoplayer2/h$a;->k:Lcom/google/android/exoplayer2/b/h;

    iget-object v10, v0, Lcom/google/android/exoplayer2/b/h;->b:Lcom/google/android/exoplayer2/b/f;

    move v0, v8

    .line 1614
    :goto_0
    iget v1, v10, Lcom/google/android/exoplayer2/b/f;->a:I

    if-ge v0, v1, :cond_1

    .line 1615
    iget-object v2, p0, Lcom/google/android/exoplayer2/h$a;->e:[Z

    if-nez p3, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer2/h$a;->k:Lcom/google/android/exoplayer2/b/h;

    iget-object v3, p0, Lcom/google/android/exoplayer2/h$a;->q:Lcom/google/android/exoplayer2/b/h;

    .line 1616
    invoke-virtual {v1, v3, v0}, Lcom/google/android/exoplayer2/b/h;->a(Lcom/google/android/exoplayer2/b/h;I)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v9

    :goto_1
    aput-boolean v1, v2, v0

    .line 1614
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v1, v8

    .line 1616
    goto :goto_1

    .line 1620
    :cond_1
    iget-object v1, p0, Lcom/google/android/exoplayer2/h$a;->a:Lcom/google/android/exoplayer2/source/h;

    invoke-virtual {v10}, Lcom/google/android/exoplayer2/b/f;->a()[Lcom/google/android/exoplayer2/b/e;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/exoplayer2/h$a;->e:[Z

    iget-object v4, p0, Lcom/google/android/exoplayer2/h$a;->d:[Lcom/google/android/exoplayer2/source/m;

    move-object v5, p4

    move-wide v6, p1

    invoke-interface/range {v1 .. v7}, Lcom/google/android/exoplayer2/source/h;->a([Lcom/google/android/exoplayer2/b/e;[Z[Lcom/google/android/exoplayer2/source/m;[ZJ)J

    move-result-wide v2

    .line 1622
    iget-object v0, p0, Lcom/google/android/exoplayer2/h$a;->k:Lcom/google/android/exoplayer2/b/h;

    iput-object v0, p0, Lcom/google/android/exoplayer2/h$a;->q:Lcom/google/android/exoplayer2/b/h;

    .line 1625
    iput-boolean v8, p0, Lcom/google/android/exoplayer2/h$a;->i:Z

    move v0, v8

    .line 1626
    :goto_2
    iget-object v1, p0, Lcom/google/android/exoplayer2/h$a;->d:[Lcom/google/android/exoplayer2/source/m;

    array-length v1, v1

    if-ge v0, v1, :cond_5

    .line 1627
    iget-object v1, p0, Lcom/google/android/exoplayer2/h$a;->d:[Lcom/google/android/exoplayer2/source/m;

    aget-object v1, v1, v0

    if-eqz v1, :cond_3

    .line 1628
    invoke-virtual {v10, v0}, Lcom/google/android/exoplayer2/b/f;->a(I)Lcom/google/android/exoplayer2/b/e;

    move-result-object v1

    if-eqz v1, :cond_2

    move v1, v9

    :goto_3
    invoke-static {v1}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 1629
    iput-boolean v9, p0, Lcom/google/android/exoplayer2/h$a;->i:Z

    .line 1626
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    move v1, v8

    .line 1628
    goto :goto_3

    .line 1631
    :cond_3
    invoke-virtual {v10, v0}, Lcom/google/android/exoplayer2/b/f;->a(I)Lcom/google/android/exoplayer2/b/e;

    move-result-object v1

    if-nez v1, :cond_4

    move v1, v9

    :goto_5
    invoke-static {v1}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    goto :goto_4

    :cond_4
    move v1, v8

    goto :goto_5

    .line 1636
    :cond_5
    iget-object v0, p0, Lcom/google/android/exoplayer2/h$a;->o:Lcom/google/android/exoplayer2/l;

    iget-object v1, p0, Lcom/google/android/exoplayer2/h$a;->l:[Lcom/google/android/exoplayer2/p;

    iget-object v4, p0, Lcom/google/android/exoplayer2/h$a;->k:Lcom/google/android/exoplayer2/b/h;

    iget-object v4, v4, Lcom/google/android/exoplayer2/b/h;->a:Lcom/google/android/exoplayer2/source/q;

    invoke-interface {v0, v1, v4, v10}, Lcom/google/android/exoplayer2/l;->a([Lcom/google/android/exoplayer2/p;Lcom/google/android/exoplayer2/source/q;Lcom/google/android/exoplayer2/b/f;)V

    .line 1637
    return-wide v2
.end method

.method public a(ZJ)Z
    .locals 6

    .prologue
    .line 1561
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/h$a;->h:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/m$a;->b:J

    .line 1563
    :goto_0
    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v2, v0, v2

    if-nez v2, :cond_2

    .line 1564
    iget-object v0, p0, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/m$a;->g:Z

    if-eqz v0, :cond_1

    .line 1565
    const/4 v0, 0x1

    .line 1569
    :goto_1
    return v0

    .line 1561
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/h$a;->a:Lcom/google/android/exoplayer2/source/h;

    .line 1562
    invoke-interface {v0}, Lcom/google/android/exoplayer2/source/h;->d()J

    move-result-wide v0

    goto :goto_0

    .line 1567
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/m$a;->e:J

    .line 1569
    :cond_2
    iget-object v2, p0, Lcom/google/android/exoplayer2/h$a;->o:Lcom/google/android/exoplayer2/l;

    invoke-virtual {p0, p2, p3}, Lcom/google/android/exoplayer2/h$a;->b(J)J

    move-result-wide v4

    sub-long/2addr v0, v4

    invoke-interface {v2, v0, v1, p1}, Lcom/google/android/exoplayer2/l;->a(JZ)Z

    move-result v0

    goto :goto_1
.end method

.method public b(J)J
    .locals 3

    .prologue
    .line 1547
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/h$a;->a()J

    move-result-wide v0

    sub-long v0, p1, v0

    return-wide v0
.end method

.method public b()Z
    .locals 4

    .prologue
    .line 1556
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/h$a;->h:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/h$a;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/h$a;->a:Lcom/google/android/exoplayer2/source/h;

    .line 1557
    invoke-interface {v0}, Lcom/google/android/exoplayer2/source/h;->d()J

    move-result-wide v0

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    .line 1574
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/h$a;->h:Z

    .line 1575
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/h$a;->d()Z

    .line 1576
    iget-object v0, p0, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/m$a;->b:J

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/exoplayer2/h$a;->a(JZ)J

    move-result-wide v0

    .line 1577
    iget-object v2, p0, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/exoplayer2/m$a;->a(J)Lcom/google/android/exoplayer2/m$a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    .line 1578
    return-void
.end method

.method public c(J)Z
    .locals 5

    .prologue
    .line 1581
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/h$a;->h:Z

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    .line 1582
    :goto_0
    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 1583
    const/4 v0, 0x0

    .line 1587
    :goto_1
    return v0

    .line 1581
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/h$a;->a:Lcom/google/android/exoplayer2/source/h;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/source/h;->e()J

    move-result-wide v0

    goto :goto_0

    .line 1585
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/google/android/exoplayer2/h$a;->b(J)J

    move-result-wide v2

    .line 1586
    sub-long/2addr v0, v2

    .line 1587
    iget-object v2, p0, Lcom/google/android/exoplayer2/h$a;->o:Lcom/google/android/exoplayer2/l;

    invoke-interface {v2, v0, v1}, Lcom/google/android/exoplayer2/l;->a(J)Z

    move-result v0

    goto :goto_1
.end method

.method public d(J)V
    .locals 3

    .prologue
    .line 1592
    invoke-virtual {p0, p1, p2}, Lcom/google/android/exoplayer2/h$a;->b(J)J

    move-result-wide v0

    .line 1593
    iget-object v2, p0, Lcom/google/android/exoplayer2/h$a;->a:Lcom/google/android/exoplayer2/source/h;

    invoke-interface {v2, v0, v1}, Lcom/google/android/exoplayer2/source/h;->c(J)Z

    .line 1594
    return-void
.end method

.method public d()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    .line 1597
    iget-object v0, p0, Lcom/google/android/exoplayer2/h$a;->n:Lcom/google/android/exoplayer2/b/g;

    iget-object v1, p0, Lcom/google/android/exoplayer2/h$a;->m:[Lcom/google/android/exoplayer2/q;

    iget-object v2, p0, Lcom/google/android/exoplayer2/h$a;->a:Lcom/google/android/exoplayer2/source/h;

    .line 1598
    invoke-interface {v2}, Lcom/google/android/exoplayer2/source/h;->b()Lcom/google/android/exoplayer2/source/q;

    move-result-object v2

    .line 1597
    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/b/g;->a([Lcom/google/android/exoplayer2/q;Lcom/google/android/exoplayer2/source/q;)Lcom/google/android/exoplayer2/b/h;

    move-result-object v0

    .line 1599
    iget-object v1, p0, Lcom/google/android/exoplayer2/h$a;->q:Lcom/google/android/exoplayer2/b/h;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/b/h;->a(Lcom/google/android/exoplayer2/b/h;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1600
    const/4 v0, 0x0

    .line 1603
    :goto_0
    return v0

    .line 1602
    :cond_0
    iput-object v0, p0, Lcom/google/android/exoplayer2/h$a;->k:Lcom/google/android/exoplayer2/b/h;

    .line 1603
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public e()V
    .locals 4

    .prologue
    .line 1642
    :try_start_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/h$a;->g:Lcom/google/android/exoplayer2/m$a;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/m$a;->c:J

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1643
    iget-object v1, p0, Lcom/google/android/exoplayer2/h$a;->p:Lcom/google/android/exoplayer2/source/i;

    iget-object v0, p0, Lcom/google/android/exoplayer2/h$a;->a:Lcom/google/android/exoplayer2/source/h;

    check-cast v0, Lcom/google/android/exoplayer2/source/c;

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/c;->a:Lcom/google/android/exoplayer2/source/h;

    invoke-interface {v1, v0}, Lcom/google/android/exoplayer2/source/i;->a(Lcom/google/android/exoplayer2/source/h;)V

    .line 1651
    :goto_0
    return-void

    .line 1645
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/h$a;->p:Lcom/google/android/exoplayer2/source/i;

    iget-object v1, p0, Lcom/google/android/exoplayer2/h$a;->a:Lcom/google/android/exoplayer2/source/h;

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/source/i;->a(Lcom/google/android/exoplayer2/source/h;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1647
    :catch_0
    move-exception v0

    .line 1649
    const-string v1, "ExoPlayerImplInternal"

    const-string v2, "Period release failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
