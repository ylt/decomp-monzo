.class final Lcom/google/android/exoplayer2/t$a;
.super Ljava/lang/Object;
.source "SimpleExoPlayer.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;
.implements Landroid/view/TextureView$SurfaceTextureListener;
.implements Lcom/google/android/exoplayer2/audio/d;
.implements Lcom/google/android/exoplayer2/c/g;
.implements Lcom/google/android/exoplayer2/metadata/e$a;
.implements Lcom/google/android/exoplayer2/text/j$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/t;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/exoplayer2/t;


# direct methods
.method private constructor <init>(Lcom/google/android/exoplayer2/t;)V
    .locals 0

    .prologue
    .line 765
    iput-object p1, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/exoplayer2/t;Lcom/google/android/exoplayer2/t$1;)V
    .locals 0

    .prologue
    .line 765
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/t$a;-><init>(Lcom/google/android/exoplayer2/t;)V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .prologue
    .line 847
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0, p1}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/t;I)I

    .line 848
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->d(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/audio/d;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 849
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->d(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/audio/d;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/audio/d;->a(I)V

    .line 851
    :cond_0
    return-void
.end method

.method public a(IIIF)V
    .locals 1

    .prologue
    .line 806
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->b(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/t$b;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 807
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->b(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/t$b;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/exoplayer2/t$b;->a(IIIF)V

    .line 810
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/c/g;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 811
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/c/g;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/exoplayer2/c/g;->a(IIIF)V

    .line 814
    :cond_1
    return-void
.end method

.method public a(IJ)V
    .locals 2

    .prologue
    .line 798
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/c/g;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 799
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/c/g;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/exoplayer2/c/g;->a(IJ)V

    .line 801
    :cond_0
    return-void
.end method

.method public a(IJJ)V
    .locals 6

    .prologue
    .line 873
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->d(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/audio/d;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 874
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->d(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/audio/d;

    move-result-object v0

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/google/android/exoplayer2/audio/d;->a(IJJ)V

    .line 876
    :cond_0
    return-void
.end method

.method public a(Landroid/view/Surface;)V
    .locals 1

    .prologue
    .line 818
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->b(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/t$b;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->c(Lcom/google/android/exoplayer2/t;)Landroid/view/Surface;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 819
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->b(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/t$b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/exoplayer2/t$b;->b()V

    .line 821
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/c/g;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 822
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/c/g;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/c/g;->a(Landroid/view/Surface;)V

    .line 824
    :cond_1
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/a/d;)V
    .locals 1

    .prologue
    .line 773
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0, p1}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/t;Lcom/google/android/exoplayer2/a/d;)Lcom/google/android/exoplayer2/a/d;

    .line 774
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/c/g;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 775
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/c/g;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/c/g;->a(Lcom/google/android/exoplayer2/a/d;)V

    .line 777
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/j;)V
    .locals 1

    .prologue
    .line 790
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0, p1}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/t;Lcom/google/android/exoplayer2/j;)Lcom/google/android/exoplayer2/j;

    .line 791
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/c/g;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 792
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/c/g;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/c/g;->a(Lcom/google/android/exoplayer2/j;)V

    .line 794
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/metadata/a;)V
    .locals 1

    .prologue
    .line 901
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->f(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/metadata/e$a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 902
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->f(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/metadata/e$a;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/metadata/e$a;->a(Lcom/google/android/exoplayer2/metadata/a;)V

    .line 904
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;JJ)V
    .locals 6

    .prologue
    .line 782
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/c/g;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 783
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/c/g;

    move-result-object v0

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/google/android/exoplayer2/c/g;->a(Ljava/lang/String;JJ)V

    .line 786
    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer2/text/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 892
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->e(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/text/j$a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 893
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->e(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/text/j$a;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/text/j$a;->a(Ljava/util/List;)V

    .line 895
    :cond_0
    return-void
.end method

.method public b(Lcom/google/android/exoplayer2/a/d;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 828
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/c/g;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 829
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/c/g;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/c/g;->b(Lcom/google/android/exoplayer2/a/d;)V

    .line 831
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/t;Lcom/google/android/exoplayer2/j;)Lcom/google/android/exoplayer2/j;

    .line 832
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/t;Lcom/google/android/exoplayer2/a/d;)Lcom/google/android/exoplayer2/a/d;

    .line 833
    return-void
.end method

.method public b(Lcom/google/android/exoplayer2/j;)V
    .locals 1

    .prologue
    .line 864
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0, p1}, Lcom/google/android/exoplayer2/t;->b(Lcom/google/android/exoplayer2/t;Lcom/google/android/exoplayer2/j;)Lcom/google/android/exoplayer2/j;

    .line 865
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->d(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/audio/d;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 866
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->d(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/audio/d;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/audio/d;->b(Lcom/google/android/exoplayer2/j;)V

    .line 868
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;JJ)V
    .locals 6

    .prologue
    .line 856
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->d(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/audio/d;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 857
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->d(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/audio/d;

    move-result-object v0

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/google/android/exoplayer2/audio/d;->b(Ljava/lang/String;JJ)V

    .line 860
    :cond_0
    return-void
.end method

.method public c(Lcom/google/android/exoplayer2/a/d;)V
    .locals 1

    .prologue
    .line 839
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0, p1}, Lcom/google/android/exoplayer2/t;->b(Lcom/google/android/exoplayer2/t;Lcom/google/android/exoplayer2/a/d;)Lcom/google/android/exoplayer2/a/d;

    .line 840
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->d(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/audio/d;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 841
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->d(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/audio/d;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/audio/d;->c(Lcom/google/android/exoplayer2/a/d;)V

    .line 843
    :cond_0
    return-void
.end method

.method public d(Lcom/google/android/exoplayer2/a/d;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 880
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->d(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/audio/d;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 881
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0}, Lcom/google/android/exoplayer2/t;->d(Lcom/google/android/exoplayer2/t;)Lcom/google/android/exoplayer2/audio/d;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/audio/d;->d(Lcom/google/android/exoplayer2/a/d;)V

    .line 883
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/t;->b(Lcom/google/android/exoplayer2/t;Lcom/google/android/exoplayer2/j;)Lcom/google/android/exoplayer2/j;

    .line 884
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/t;->b(Lcom/google/android/exoplayer2/t;Lcom/google/android/exoplayer2/a/d;)Lcom/google/android/exoplayer2/a/d;

    .line 885
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/t;I)I

    .line 886
    return-void
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 3

    .prologue
    .line 927
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    new-instance v1, Landroid/view/Surface;

    invoke-direct {v1, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/t;Landroid/view/Surface;Z)V

    .line 928
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 937
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    const/4 v1, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/t;Landroid/view/Surface;Z)V

    .line 938
    return v2
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    .prologue
    .line 933
    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    .prologue
    .line 944
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0

    .prologue
    .line 916
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 3

    .prologue
    .line 910
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/t;Landroid/view/Surface;Z)V

    .line 911
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 3

    .prologue
    .line 920
    iget-object v0, p0, Lcom/google/android/exoplayer2/t$a;->a:Lcom/google/android/exoplayer2/t;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/exoplayer2/t;->a(Lcom/google/android/exoplayer2/t;Landroid/view/Surface;Z)V

    .line 921
    return-void
.end method
