.class final Lcom/google/android/exoplayer2/g;
.super Ljava/lang/Object;
.source "ExoPlayerImpl.java"

# interfaces
.implements Lcom/google/android/exoplayer2/e;


# instance fields
.field private final a:[Lcom/google/android/exoplayer2/p;

.field private final b:Lcom/google/android/exoplayer2/b/g;

.field private final c:Lcom/google/android/exoplayer2/b/f;

.field private final d:Landroid/os/Handler;

.field private final e:Lcom/google/android/exoplayer2/h;

.field private final f:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet",
            "<",
            "Lcom/google/android/exoplayer2/o$a;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/google/android/exoplayer2/u$b;

.field private final h:Lcom/google/android/exoplayer2/u$a;

.field private i:Z

.field private j:Z

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:Z

.field private p:Lcom/google/android/exoplayer2/u;

.field private q:Ljava/lang/Object;

.field private r:Lcom/google/android/exoplayer2/source/q;

.field private s:Lcom/google/android/exoplayer2/b/f;

.field private t:Lcom/google/android/exoplayer2/n;

.field private u:Lcom/google/android/exoplayer2/h$b;

.field private v:I

.field private w:I

.field private x:J


# direct methods
.method public constructor <init>([Lcom/google/android/exoplayer2/p;Lcom/google/android/exoplayer2/b/g;Lcom/google/android/exoplayer2/l;)V
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    const-string v0, "ExoPlayerImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Init "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ExoPlayerLib/2.5.1"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/google/android/exoplayer2/util/s;->e:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    array-length v0, p1

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer2/util/a;->b(Z)V

    .line 86
    invoke-static {p1}, Lcom/google/android/exoplayer2/util/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/exoplayer2/p;

    iput-object v0, p0, Lcom/google/android/exoplayer2/g;->a:[Lcom/google/android/exoplayer2/p;

    .line 87
    invoke-static {p2}, Lcom/google/android/exoplayer2/util/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/b/g;

    iput-object v0, p0, Lcom/google/android/exoplayer2/g;->b:Lcom/google/android/exoplayer2/b/g;

    .line 88
    iput-boolean v2, p0, Lcom/google/android/exoplayer2/g;->j:Z

    .line 89
    iput v2, p0, Lcom/google/android/exoplayer2/g;->k:I

    .line 90
    iput v1, p0, Lcom/google/android/exoplayer2/g;->l:I

    .line 91
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/g;->f:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 92
    new-instance v0, Lcom/google/android/exoplayer2/b/f;

    array-length v1, p1

    new-array v1, v1, [Lcom/google/android/exoplayer2/b/e;

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer2/b/f;-><init>([Lcom/google/android/exoplayer2/b/e;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/g;->c:Lcom/google/android/exoplayer2/b/f;

    .line 93
    sget-object v0, Lcom/google/android/exoplayer2/u;->a:Lcom/google/android/exoplayer2/u;

    iput-object v0, p0, Lcom/google/android/exoplayer2/g;->p:Lcom/google/android/exoplayer2/u;

    .line 94
    new-instance v0, Lcom/google/android/exoplayer2/u$b;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/u$b;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/g;->g:Lcom/google/android/exoplayer2/u$b;

    .line 95
    new-instance v0, Lcom/google/android/exoplayer2/u$a;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/u$a;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/g;->h:Lcom/google/android/exoplayer2/u$a;

    .line 96
    sget-object v0, Lcom/google/android/exoplayer2/source/q;->a:Lcom/google/android/exoplayer2/source/q;

    iput-object v0, p0, Lcom/google/android/exoplayer2/g;->r:Lcom/google/android/exoplayer2/source/q;

    .line 97
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->c:Lcom/google/android/exoplayer2/b/f;

    iput-object v0, p0, Lcom/google/android/exoplayer2/g;->s:Lcom/google/android/exoplayer2/b/f;

    .line 98
    sget-object v0, Lcom/google/android/exoplayer2/n;->a:Lcom/google/android/exoplayer2/n;

    iput-object v0, p0, Lcom/google/android/exoplayer2/g;->t:Lcom/google/android/exoplayer2/n;

    .line 99
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    .line 100
    :goto_1
    new-instance v1, Lcom/google/android/exoplayer2/g$1;

    invoke-direct {v1, p0, v0}, Lcom/google/android/exoplayer2/g$1;-><init>(Lcom/google/android/exoplayer2/g;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/exoplayer2/g;->d:Landroid/os/Handler;

    .line 106
    new-instance v0, Lcom/google/android/exoplayer2/h$b;

    const-wide/16 v4, 0x0

    invoke-direct {v0, v2, v4, v5}, Lcom/google/android/exoplayer2/h$b;-><init>(IJ)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/g;->u:Lcom/google/android/exoplayer2/h$b;

    .line 107
    new-instance v0, Lcom/google/android/exoplayer2/h;

    iget-boolean v4, p0, Lcom/google/android/exoplayer2/g;->j:Z

    iget v5, p0, Lcom/google/android/exoplayer2/g;->k:I

    iget-object v6, p0, Lcom/google/android/exoplayer2/g;->d:Landroid/os/Handler;

    iget-object v7, p0, Lcom/google/android/exoplayer2/g;->u:Lcom/google/android/exoplayer2/h$b;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v8, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/exoplayer2/h;-><init>([Lcom/google/android/exoplayer2/p;Lcom/google/android/exoplayer2/b/g;Lcom/google/android/exoplayer2/l;ZILandroid/os/Handler;Lcom/google/android/exoplayer2/h$b;Lcom/google/android/exoplayer2/e;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/g;->e:Lcom/google/android/exoplayer2/h;

    .line 109
    return-void

    :cond_0
    move v0, v2

    .line 85
    goto :goto_0

    .line 99
    :cond_1
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/google/android/exoplayer2/g;->l:I

    return v0
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 178
    iget v0, p0, Lcom/google/android/exoplayer2/g;->k:I

    if-eq v0, p1, :cond_0

    .line 179
    iput p1, p0, Lcom/google/android/exoplayer2/g;->k:I

    .line 180
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->e:Lcom/google/android/exoplayer2/h;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/h;->a(I)V

    .line 181
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->f:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/o$a;

    .line 182
    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/o$a;->a(I)V

    goto :goto_0

    .line 185
    :cond_0
    return-void
.end method

.method public a(IJ)V
    .locals 8

    .prologue
    const-wide v6, -0x7fffffffffffffffL    # -4.9E-324

    .line 214
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->p:Lcom/google/android/exoplayer2/u;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/u;->a()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->p:Lcom/google/android/exoplayer2/u;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/u;->b()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 215
    :cond_0
    new-instance v0, Lcom/google/android/exoplayer2/IllegalSeekPositionException;

    iget-object v1, p0, Lcom/google/android/exoplayer2/g;->p:Lcom/google/android/exoplayer2/u;

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/google/android/exoplayer2/IllegalSeekPositionException;-><init>(Lcom/google/android/exoplayer2/u;IJ)V

    throw v0

    .line 217
    :cond_1
    iget v0, p0, Lcom/google/android/exoplayer2/g;->m:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/exoplayer2/g;->m:I

    .line 218
    iput p1, p0, Lcom/google/android/exoplayer2/g;->v:I

    .line 219
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->p:Lcom/google/android/exoplayer2/u;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/u;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 220
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer2/g;->w:I

    .line 235
    :goto_0
    cmp-long v0, p2, v6

    if-nez v0, :cond_6

    .line 236
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/g;->x:J

    .line 237
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->e:Lcom/google/android/exoplayer2/h;

    iget-object v1, p0, Lcom/google/android/exoplayer2/g;->p:Lcom/google/android/exoplayer2/u;

    invoke-virtual {v0, v1, p1, v6, v7}, Lcom/google/android/exoplayer2/h;->a(Lcom/google/android/exoplayer2/u;IJ)V

    .line 245
    :cond_2
    return-void

    .line 222
    :cond_3
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->p:Lcom/google/android/exoplayer2/u;

    iget-object v1, p0, Lcom/google/android/exoplayer2/g;->g:Lcom/google/android/exoplayer2/u$b;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$b;)Lcom/google/android/exoplayer2/u$b;

    .line 223
    cmp-long v0, p2, v6

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->g:Lcom/google/android/exoplayer2/u$b;

    .line 224
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/u$b;->a()J

    move-result-wide v0

    .line 225
    :goto_1
    iget-object v2, p0, Lcom/google/android/exoplayer2/g;->g:Lcom/google/android/exoplayer2/u$b;

    iget v4, v2, Lcom/google/android/exoplayer2/u$b;->f:I

    .line 226
    iget-object v2, p0, Lcom/google/android/exoplayer2/g;->g:Lcom/google/android/exoplayer2/u$b;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/u$b;->c()J

    move-result-wide v2

    add-long/2addr v2, v0

    .line 227
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->p:Lcom/google/android/exoplayer2/u;

    iget-object v1, p0, Lcom/google/android/exoplayer2/g;->h:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v0, v4, v1}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;)Lcom/google/android/exoplayer2/u$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/u$a;->a()J

    move-result-wide v0

    .line 228
    :goto_2
    cmp-long v5, v0, v6

    if-eqz v5, :cond_5

    cmp-long v5, v2, v0

    if-ltz v5, :cond_5

    iget-object v5, p0, Lcom/google/android/exoplayer2/g;->g:Lcom/google/android/exoplayer2/u$b;

    iget v5, v5, Lcom/google/android/exoplayer2/u$b;->g:I

    if-ge v4, v5, :cond_5

    .line 230
    sub-long/2addr v2, v0

    .line 231
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->p:Lcom/google/android/exoplayer2/u;

    add-int/lit8 v4, v4, 0x1

    iget-object v1, p0, Lcom/google/android/exoplayer2/g;->h:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v0, v4, v1}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;)Lcom/google/android/exoplayer2/u$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/u$a;->a()J

    move-result-wide v0

    goto :goto_2

    .line 224
    :cond_4
    invoke-static {p2, p3}, Lcom/google/android/exoplayer2/b;->b(J)J

    move-result-wide v0

    goto :goto_1

    .line 233
    :cond_5
    iput v4, p0, Lcom/google/android/exoplayer2/g;->w:I

    goto :goto_0

    .line 239
    :cond_6
    iput-wide p2, p0, Lcom/google/android/exoplayer2/g;->x:J

    .line 240
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->e:Lcom/google/android/exoplayer2/h;

    iget-object v1, p0, Lcom/google/android/exoplayer2/g;->p:Lcom/google/android/exoplayer2/u;

    invoke-static {p2, p3}, Lcom/google/android/exoplayer2/b;->b(J)J

    move-result-wide v2

    invoke-virtual {v0, v1, p1, v2, v3}, Lcom/google/android/exoplayer2/h;->a(Lcom/google/android/exoplayer2/u;IJ)V

    .line 241
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->f:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/o$a;

    .line 242
    invoke-interface {v0}, Lcom/google/android/exoplayer2/o$a;->a()V

    goto :goto_3
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/g;->e()I

    move-result v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/exoplayer2/g;->a(IJ)V

    .line 210
    return-void
.end method

.method a(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 416
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 499
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 418
    :pswitch_0
    iget v0, p0, Lcom/google/android/exoplayer2/g;->n:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/exoplayer2/g;->n:I

    .line 501
    :cond_0
    return-void

    .line 422
    :pswitch_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    iput v0, p0, Lcom/google/android/exoplayer2/g;->l:I

    .line 423
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->f:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/o$a;

    .line 424
    iget-boolean v2, p0, Lcom/google/android/exoplayer2/g;->j:Z

    iget v3, p0, Lcom/google/android/exoplayer2/g;->l:I

    invoke-interface {v0, v2, v3}, Lcom/google/android/exoplayer2/o$a;->a(ZI)V

    goto :goto_0

    .line 429
    :pswitch_2
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/exoplayer2/g;->o:Z

    .line 430
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->f:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/o$a;

    .line 431
    iget-boolean v2, p0, Lcom/google/android/exoplayer2/g;->o:Z

    invoke-interface {v0, v2}, Lcom/google/android/exoplayer2/o$a;->a(Z)V

    goto :goto_2

    .line 429
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 436
    :pswitch_3
    iget v0, p0, Lcom/google/android/exoplayer2/g;->n:I

    if-nez v0, :cond_0

    .line 437
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/b/h;

    .line 438
    iput-boolean v1, p0, Lcom/google/android/exoplayer2/g;->i:Z

    .line 439
    iget-object v1, v0, Lcom/google/android/exoplayer2/b/h;->a:Lcom/google/android/exoplayer2/source/q;

    iput-object v1, p0, Lcom/google/android/exoplayer2/g;->r:Lcom/google/android/exoplayer2/source/q;

    .line 440
    iget-object v1, v0, Lcom/google/android/exoplayer2/b/h;->b:Lcom/google/android/exoplayer2/b/f;

    iput-object v1, p0, Lcom/google/android/exoplayer2/g;->s:Lcom/google/android/exoplayer2/b/f;

    .line 441
    iget-object v1, p0, Lcom/google/android/exoplayer2/g;->b:Lcom/google/android/exoplayer2/b/g;

    iget-object v0, v0, Lcom/google/android/exoplayer2/b/h;->c:Ljava/lang/Object;

    invoke-virtual {v1, v0}, Lcom/google/android/exoplayer2/b/g;->a(Ljava/lang/Object;)V

    .line 442
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->f:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/o$a;

    .line 443
    iget-object v2, p0, Lcom/google/android/exoplayer2/g;->r:Lcom/google/android/exoplayer2/source/q;

    iget-object v3, p0, Lcom/google/android/exoplayer2/g;->s:Lcom/google/android/exoplayer2/b/f;

    invoke-interface {v0, v2, v3}, Lcom/google/android/exoplayer2/o$a;->a(Lcom/google/android/exoplayer2/source/q;Lcom/google/android/exoplayer2/b/f;)V

    goto :goto_3

    .line 449
    :pswitch_4
    iget v0, p0, Lcom/google/android/exoplayer2/g;->m:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/exoplayer2/g;->m:I

    if-nez v0, :cond_0

    .line 450
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/h$b;

    iput-object v0, p0, Lcom/google/android/exoplayer2/g;->u:Lcom/google/android/exoplayer2/h$b;

    .line 451
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_0

    .line 452
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->f:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/o$a;

    .line 453
    invoke-interface {v0}, Lcom/google/android/exoplayer2/o$a;->a()V

    goto :goto_4

    .line 460
    :pswitch_5
    iget v0, p0, Lcom/google/android/exoplayer2/g;->m:I

    if-nez v0, :cond_0

    .line 461
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/h$b;

    iput-object v0, p0, Lcom/google/android/exoplayer2/g;->u:Lcom/google/android/exoplayer2/h$b;

    .line 462
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->f:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/o$a;

    .line 463
    invoke-interface {v0}, Lcom/google/android/exoplayer2/o$a;->a()V

    goto :goto_5

    .line 469
    :pswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/h$d;

    .line 470
    iget v1, p0, Lcom/google/android/exoplayer2/g;->m:I

    iget v2, v0, Lcom/google/android/exoplayer2/h$d;->d:I

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/exoplayer2/g;->m:I

    .line 471
    iget v1, p0, Lcom/google/android/exoplayer2/g;->n:I

    if-nez v1, :cond_0

    .line 472
    iget-object v1, v0, Lcom/google/android/exoplayer2/h$d;->a:Lcom/google/android/exoplayer2/u;

    iput-object v1, p0, Lcom/google/android/exoplayer2/g;->p:Lcom/google/android/exoplayer2/u;

    .line 473
    iget-object v1, v0, Lcom/google/android/exoplayer2/h$d;->b:Ljava/lang/Object;

    iput-object v1, p0, Lcom/google/android/exoplayer2/g;->q:Ljava/lang/Object;

    .line 474
    iget-object v0, v0, Lcom/google/android/exoplayer2/h$d;->c:Lcom/google/android/exoplayer2/h$b;

    iput-object v0, p0, Lcom/google/android/exoplayer2/g;->u:Lcom/google/android/exoplayer2/h$b;

    .line 475
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->f:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/o$a;

    .line 476
    iget-object v2, p0, Lcom/google/android/exoplayer2/g;->p:Lcom/google/android/exoplayer2/u;

    iget-object v3, p0, Lcom/google/android/exoplayer2/g;->q:Ljava/lang/Object;

    invoke-interface {v0, v2, v3}, Lcom/google/android/exoplayer2/o$a;->a(Lcom/google/android/exoplayer2/u;Ljava/lang/Object;)V

    goto :goto_6

    .line 482
    :pswitch_7
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/n;

    .line 483
    iget-object v1, p0, Lcom/google/android/exoplayer2/g;->t:Lcom/google/android/exoplayer2/n;

    invoke-virtual {v1, v0}, Lcom/google/android/exoplayer2/n;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 484
    iput-object v0, p0, Lcom/google/android/exoplayer2/g;->t:Lcom/google/android/exoplayer2/n;

    .line 485
    iget-object v1, p0, Lcom/google/android/exoplayer2/g;->f:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/o$a;

    .line 486
    invoke-interface {v1, v0}, Lcom/google/android/exoplayer2/o$a;->a(Lcom/google/android/exoplayer2/n;)V

    goto :goto_7

    .line 492
    :pswitch_8
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer2/ExoPlaybackException;

    .line 493
    iget-object v1, p0, Lcom/google/android/exoplayer2/g;->f:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/o$a;

    .line 494
    invoke-interface {v1, v0}, Lcom/google/android/exoplayer2/o$a;->a(Lcom/google/android/exoplayer2/ExoPlaybackException;)V

    goto :goto_8

    .line 416
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public a(Lcom/google/android/exoplayer2/o$a;)V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->f:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    .line 119
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/source/i;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 133
    invoke-virtual {p0, p1, v0, v0}, Lcom/google/android/exoplayer2/g;->a(Lcom/google/android/exoplayer2/source/i;ZZ)V

    .line 134
    return-void
.end method

.method public a(Lcom/google/android/exoplayer2/source/i;ZZ)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 138
    if-eqz p3, :cond_2

    .line 139
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->p:Lcom/google/android/exoplayer2/u;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/u;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->q:Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 140
    :cond_0
    sget-object v0, Lcom/google/android/exoplayer2/u;->a:Lcom/google/android/exoplayer2/u;

    iput-object v0, p0, Lcom/google/android/exoplayer2/g;->p:Lcom/google/android/exoplayer2/u;

    .line 141
    iput-object v4, p0, Lcom/google/android/exoplayer2/g;->q:Ljava/lang/Object;

    .line 142
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->f:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/o$a;

    .line 143
    iget-object v2, p0, Lcom/google/android/exoplayer2/g;->p:Lcom/google/android/exoplayer2/u;

    iget-object v3, p0, Lcom/google/android/exoplayer2/g;->q:Ljava/lang/Object;

    invoke-interface {v0, v2, v3}, Lcom/google/android/exoplayer2/o$a;->a(Lcom/google/android/exoplayer2/u;Ljava/lang/Object;)V

    goto :goto_0

    .line 146
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/g;->i:Z

    if-eqz v0, :cond_2

    .line 147
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/g;->i:Z

    .line 148
    sget-object v0, Lcom/google/android/exoplayer2/source/q;->a:Lcom/google/android/exoplayer2/source/q;

    iput-object v0, p0, Lcom/google/android/exoplayer2/g;->r:Lcom/google/android/exoplayer2/source/q;

    .line 149
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->c:Lcom/google/android/exoplayer2/b/f;

    iput-object v0, p0, Lcom/google/android/exoplayer2/g;->s:Lcom/google/android/exoplayer2/b/f;

    .line 150
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->b:Lcom/google/android/exoplayer2/b/g;

    invoke-virtual {v0, v4}, Lcom/google/android/exoplayer2/b/g;->a(Ljava/lang/Object;)V

    .line 151
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->f:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/o$a;

    .line 152
    iget-object v2, p0, Lcom/google/android/exoplayer2/g;->r:Lcom/google/android/exoplayer2/source/q;

    iget-object v3, p0, Lcom/google/android/exoplayer2/g;->s:Lcom/google/android/exoplayer2/b/f;

    invoke-interface {v0, v2, v3}, Lcom/google/android/exoplayer2/o$a;->a(Lcom/google/android/exoplayer2/source/q;Lcom/google/android/exoplayer2/b/f;)V

    goto :goto_1

    .line 156
    :cond_2
    iget v0, p0, Lcom/google/android/exoplayer2/g;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/exoplayer2/g;->n:I

    .line 157
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->e:Lcom/google/android/exoplayer2/h;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/h;->a(Lcom/google/android/exoplayer2/source/i;Z)V

    .line 158
    return-void
.end method

.method public a(Z)V
    .locals 3

    .prologue
    .line 162
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/g;->j:Z

    if-eq v0, p1, :cond_0

    .line 163
    iput-boolean p1, p0, Lcom/google/android/exoplayer2/g;->j:Z

    .line 164
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->e:Lcom/google/android/exoplayer2/h;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/h;->a(Z)V

    .line 165
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->f:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/o$a;

    .line 166
    iget v2, p0, Lcom/google/android/exoplayer2/g;->l:I

    invoke-interface {v0, p1, v2}, Lcom/google/android/exoplayer2/o$a;->a(ZI)V

    goto :goto_0

    .line 169
    :cond_0
    return-void
.end method

.method public varargs a([Lcom/google/android/exoplayer2/e$c;)V
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->e:Lcom/google/android/exoplayer2/h;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/h;->a([Lcom/google/android/exoplayer2/e$c;)V

    .line 277
    return-void
.end method

.method public b(I)I
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->a:[Lcom/google/android/exoplayer2/p;

    aget-object v0, v0, p1

    invoke-interface {v0}, Lcom/google/android/exoplayer2/p;->a()I

    move-result v0

    return v0
.end method

.method public b(Lcom/google/android/exoplayer2/o$a;)V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->f:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->remove(Ljava/lang/Object;)Z

    .line 124
    return-void
.end method

.method public varargs b([Lcom/google/android/exoplayer2/e$c;)V
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->e:Lcom/google/android/exoplayer2/h;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/h;->b([Lcom/google/android/exoplayer2/e$c;)V

    .line 282
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 173
    iget-boolean v0, p0, Lcom/google/android/exoplayer2/g;->j:Z

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 189
    iget v0, p0, Lcom/google/android/exoplayer2/g;->k:I

    return v0
.end method

.method public d()V
    .locals 3

    .prologue
    .line 267
    const-string v0, "ExoPlayerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Release "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ExoPlayerLib/2.5.1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/google/android/exoplayer2/util/s;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 269
    invoke-static {}, Lcom/google/android/exoplayer2/i;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 267
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->e:Lcom/google/android/exoplayer2/h;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/h;->a()V

    .line 271
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->d:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 272
    return-void
.end method

.method public e()I
    .locals 3

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->p:Lcom/google/android/exoplayer2/u;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/u;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer2/g;->m:I

    if-lez v0, :cond_1

    .line 296
    :cond_0
    iget v0, p0, Lcom/google/android/exoplayer2/g;->v:I

    .line 298
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->p:Lcom/google/android/exoplayer2/u;

    iget-object v1, p0, Lcom/google/android/exoplayer2/g;->u:Lcom/google/android/exoplayer2/h$b;

    iget-object v1, v1, Lcom/google/android/exoplayer2/h$b;->a:Lcom/google/android/exoplayer2/source/i$b;

    iget v1, v1, Lcom/google/android/exoplayer2/source/i$b;->b:I

    iget-object v2, p0, Lcom/google/android/exoplayer2/g;->h:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;)Lcom/google/android/exoplayer2/u$a;

    move-result-object v0

    iget v0, v0, Lcom/google/android/exoplayer2/u$a;->c:I

    goto :goto_0
.end method

.method public f()J
    .locals 4

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->p:Lcom/google/android/exoplayer2/u;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/u;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 305
    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    .line 313
    :goto_0
    return-wide v0

    .line 307
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/g;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 308
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->u:Lcom/google/android/exoplayer2/h$b;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$b;->a:Lcom/google/android/exoplayer2/source/i$b;

    .line 309
    iget-object v1, p0, Lcom/google/android/exoplayer2/g;->p:Lcom/google/android/exoplayer2/u;

    iget v2, v0, Lcom/google/android/exoplayer2/source/i$b;->b:I

    iget-object v3, p0, Lcom/google/android/exoplayer2/g;->h:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;)Lcom/google/android/exoplayer2/u$a;

    .line 310
    iget-object v1, p0, Lcom/google/android/exoplayer2/g;->h:Lcom/google/android/exoplayer2/u$a;

    iget v2, v0, Lcom/google/android/exoplayer2/source/i$b;->c:I

    iget v0, v0, Lcom/google/android/exoplayer2/source/i$b;->d:I

    invoke-virtual {v1, v2, v0}, Lcom/google/android/exoplayer2/u$a;->b(II)J

    move-result-wide v0

    .line 311
    invoke-static {v0, v1}, Lcom/google/android/exoplayer2/b;->a(J)J

    move-result-wide v0

    goto :goto_0

    .line 313
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->p:Lcom/google/android/exoplayer2/u;

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/g;->e()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/exoplayer2/g;->g:Lcom/google/android/exoplayer2/u$b;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$b;)Lcom/google/android/exoplayer2/u$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/u$b;->b()J

    move-result-wide v0

    goto :goto_0
.end method

.method public g()J
    .locals 4

    .prologue
    .line 319
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->p:Lcom/google/android/exoplayer2/u;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/u;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer2/g;->m:I

    if-lez v0, :cond_1

    .line 320
    :cond_0
    iget-wide v0, p0, Lcom/google/android/exoplayer2/g;->x:J

    .line 323
    :goto_0
    return-wide v0

    .line 322
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->p:Lcom/google/android/exoplayer2/u;

    iget-object v1, p0, Lcom/google/android/exoplayer2/g;->u:Lcom/google/android/exoplayer2/h$b;

    iget-object v1, v1, Lcom/google/android/exoplayer2/h$b;->a:Lcom/google/android/exoplayer2/source/i$b;

    iget v1, v1, Lcom/google/android/exoplayer2/source/i$b;->b:I

    iget-object v2, p0, Lcom/google/android/exoplayer2/g;->h:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;)Lcom/google/android/exoplayer2/u$a;

    .line 323
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->h:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/u$a;->b()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/exoplayer2/g;->u:Lcom/google/android/exoplayer2/h$b;

    iget-wide v2, v2, Lcom/google/android/exoplayer2/h$b;->d:J

    invoke-static {v2, v3}, Lcom/google/android/exoplayer2/b;->a(J)J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method public h()J
    .locals 4

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->p:Lcom/google/android/exoplayer2/u;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/u;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer2/g;->m:I

    if-lez v0, :cond_1

    .line 331
    :cond_0
    iget-wide v0, p0, Lcom/google/android/exoplayer2/g;->x:J

    .line 334
    :goto_0
    return-wide v0

    .line 333
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->p:Lcom/google/android/exoplayer2/u;

    iget-object v1, p0, Lcom/google/android/exoplayer2/g;->u:Lcom/google/android/exoplayer2/h$b;

    iget-object v1, v1, Lcom/google/android/exoplayer2/h$b;->a:Lcom/google/android/exoplayer2/source/i$b;

    iget v1, v1, Lcom/google/android/exoplayer2/source/i$b;->b:I

    iget-object v2, p0, Lcom/google/android/exoplayer2/g;->h:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;)Lcom/google/android/exoplayer2/u$a;

    .line 334
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->h:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/u$a;->b()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/exoplayer2/g;->u:Lcom/google/android/exoplayer2/h$b;

    iget-wide v2, v2, Lcom/google/android/exoplayer2/h$b;->e:J

    invoke-static {v2, v3}, Lcom/google/android/exoplayer2/b;->a(J)J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 361
    iget v0, p0, Lcom/google/android/exoplayer2/g;->m:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->u:Lcom/google/android/exoplayer2/h$b;

    iget-object v0, v0, Lcom/google/android/exoplayer2/h$b;->a:Lcom/google/android/exoplayer2/source/i$b;

    iget v0, v0, Lcom/google/android/exoplayer2/source/i$b;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()J
    .locals 4

    .prologue
    .line 376
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/g;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 377
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->p:Lcom/google/android/exoplayer2/u;

    iget-object v1, p0, Lcom/google/android/exoplayer2/g;->u:Lcom/google/android/exoplayer2/h$b;

    iget-object v1, v1, Lcom/google/android/exoplayer2/h$b;->a:Lcom/google/android/exoplayer2/source/i$b;

    iget v1, v1, Lcom/google/android/exoplayer2/source/i$b;->b:I

    iget-object v2, p0, Lcom/google/android/exoplayer2/g;->h:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/exoplayer2/u;->a(ILcom/google/android/exoplayer2/u$a;)Lcom/google/android/exoplayer2/u$a;

    .line 378
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->h:Lcom/google/android/exoplayer2/u$a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/u$a;->b()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/exoplayer2/g;->u:Lcom/google/android/exoplayer2/h$b;

    iget-wide v2, v2, Lcom/google/android/exoplayer2/h$b;->c:J

    invoke-static {v2, v3}, Lcom/google/android/exoplayer2/b;->a(J)J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 380
    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/g;->g()J

    move-result-wide v0

    goto :goto_0
.end method

.method public k()Lcom/google/android/exoplayer2/b/f;
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->s:Lcom/google/android/exoplayer2/b/f;

    return-object v0
.end method

.method public l()Lcom/google/android/exoplayer2/u;
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Lcom/google/android/exoplayer2/g;->p:Lcom/google/android/exoplayer2/u;

    return-object v0
.end method
