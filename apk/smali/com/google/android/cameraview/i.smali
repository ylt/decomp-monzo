.class Lcom/google/android/cameraview/i;
.super Ljava/lang/Object;
.source "SizeMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/cameraview/i$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/support/v4/g/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/g/a",
            "<",
            "Lcom/google/android/cameraview/a;",
            "Ljava/util/SortedSet",
            "<",
            "Lcom/google/android/cameraview/h;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Landroid/support/v4/g/a;

    invoke-direct {v0}, Landroid/support/v4/g/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/cameraview/i;->a:Landroid/support/v4/g/a;

    return-void
.end method


# virtual methods
.method a(I)Lcom/google/android/cameraview/h;
    .locals 5

    .prologue
    .line 83
    new-instance v1, Landroid/support/v4/g/b;

    invoke-direct {v1}, Landroid/support/v4/g/b;-><init>()V

    .line 84
    new-instance v2, Landroid/support/v4/g/b;

    invoke-direct {v2}, Landroid/support/v4/g/b;-><init>()V

    .line 85
    iget-object v0, p0, Lcom/google/android/cameraview/i;->a:Landroid/support/v4/g/a;

    invoke-virtual {v0}, Landroid/support/v4/g/a;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/cameraview/a;

    .line 86
    invoke-virtual {v0, p1}, Lcom/google/android/cameraview/a;->a(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 87
    iget-object v4, p0, Lcom/google/android/cameraview/i;->a:Landroid/support/v4/g/a;

    invoke-virtual {v4, v0}, Landroid/support/v4/g/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 89
    :cond_0
    iget-object v4, p0, Lcom/google/android/cameraview/i;->a:Landroid/support/v4/g/a;

    invoke-virtual {v4, v0}, Landroid/support/v4/g/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v2, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 93
    :cond_1
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    move-object v0, v1

    :goto_1
    new-instance v1, Lcom/google/android/cameraview/i$a;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/cameraview/i$a;-><init>(Lcom/google/android/cameraview/i$1;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->max(Ljava/util/Collection;Ljava/util/Comparator;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/cameraview/h;

    return-object v0

    :cond_2
    move-object v0, v2

    goto :goto_1
.end method

.method a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/cameraview/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/cameraview/i;->a:Landroid/support/v4/g/a;

    invoke-virtual {v0}, Landroid/support/v4/g/a;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/cameraview/a;)V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/cameraview/i;->a:Landroid/support/v4/g/a;

    invoke-virtual {v0, p1}, Landroid/support/v4/g/a;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    return-void
.end method

.method public a(Lcom/google/android/cameraview/h;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 44
    iget-object v0, p0, Lcom/google/android/cameraview/i;->a:Landroid/support/v4/g/a;

    invoke-virtual {v0}, Landroid/support/v4/g/a;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/cameraview/a;

    .line 45
    invoke-virtual {v0, p1}, Lcom/google/android/cameraview/a;->a(Lcom/google/android/cameraview/h;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 46
    iget-object v2, p0, Lcom/google/android/cameraview/i;->a:Landroid/support/v4/g/a;

    invoke-virtual {v2, v0}, Landroid/support/v4/g/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    .line 47
    invoke-interface {v0, p1}, Ljava/util/SortedSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 48
    const/4 v0, 0x0

    .line 59
    :goto_0
    return v0

    .line 50
    :cond_1
    invoke-interface {v0, p1}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 51
    goto :goto_0

    .line 56
    :cond_2
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    .line 57
    invoke-interface {v0, p1}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    .line 58
    iget-object v2, p0, Lcom/google/android/cameraview/i;->a:Landroid/support/v4/g/a;

    invoke-virtual {p1}, Lcom/google/android/cameraview/h;->a()I

    move-result v3

    invoke-virtual {p1}, Lcom/google/android/cameraview/h;->b()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/android/cameraview/a;->a(II)Lcom/google/android/cameraview/a;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Landroid/support/v4/g/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    .line 59
    goto :goto_0
.end method

.method b(Lcom/google/android/cameraview/a;)Ljava/util/SortedSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/cameraview/a;",
            ")",
            "Ljava/util/SortedSet",
            "<",
            "Lcom/google/android/cameraview/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/cameraview/i;->a:Landroid/support/v4/g/a;

    invoke-virtual {v0, p1}, Landroid/support/v4/g/a;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/google/android/cameraview/i;->a:Landroid/support/v4/g/a;

    invoke-virtual {v0, p1}, Landroid/support/v4/g/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    .line 79
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    goto :goto_0
.end method

.method b()V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/cameraview/i;->a:Landroid/support/v4/g/a;

    invoke-virtual {v0}, Landroid/support/v4/g/a;->clear()V

    .line 98
    return-void
.end method
