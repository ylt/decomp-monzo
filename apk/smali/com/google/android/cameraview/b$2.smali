.class Lcom/google/android/cameraview/b$2;
.super Landroid/hardware/camera2/CameraDevice$StateCallback;
.source "Camera2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/cameraview/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/cameraview/b;


# direct methods
.method constructor <init>(Lcom/google/android/cameraview/b;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/google/android/cameraview/b$2;->a:Lcom/google/android/cameraview/b;

    invoke-direct {p0}, Landroid/hardware/camera2/CameraDevice$StateCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onClosed(Landroid/hardware/camera2/CameraDevice;)V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/cameraview/b$2;->a:Lcom/google/android/cameraview/b;

    iget-object v0, v0, Lcom/google/android/cameraview/b;->a:Lcom/google/android/cameraview/c$a;

    invoke-interface {v0}, Lcom/google/android/cameraview/c$a;->b()V

    .line 128
    return-void
.end method

.method public onDisconnected(Landroid/hardware/camera2/CameraDevice;)V
    .locals 2

    .prologue
    .line 132
    invoke-virtual {p1}, Landroid/hardware/camera2/CameraDevice;->close()V

    .line 133
    iget-object v0, p0, Lcom/google/android/cameraview/b$2;->a:Lcom/google/android/cameraview/b;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/cameraview/b;->a(Lcom/google/android/cameraview/b;Landroid/hardware/camera2/CameraDevice;)Landroid/hardware/camera2/CameraDevice;

    .line 134
    return-void
.end method

.method public onError(Landroid/hardware/camera2/CameraDevice;I)V
    .locals 3

    .prologue
    .line 138
    const-string v0, "Camera2"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onError: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/hardware/camera2/CameraDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    invoke-virtual {p1}, Landroid/hardware/camera2/CameraDevice;->close()V

    .line 140
    iget-object v0, p0, Lcom/google/android/cameraview/b$2;->a:Lcom/google/android/cameraview/b;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/cameraview/b;->a(Lcom/google/android/cameraview/b;Landroid/hardware/camera2/CameraDevice;)Landroid/hardware/camera2/CameraDevice;

    .line 141
    return-void
.end method

.method public onOpened(Landroid/hardware/camera2/CameraDevice;)V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/cameraview/b$2;->a:Lcom/google/android/cameraview/b;

    invoke-static {v0, p1}, Lcom/google/android/cameraview/b;->a(Lcom/google/android/cameraview/b;Landroid/hardware/camera2/CameraDevice;)Landroid/hardware/camera2/CameraDevice;

    .line 121
    iget-object v0, p0, Lcom/google/android/cameraview/b$2;->a:Lcom/google/android/cameraview/b;

    iget-object v0, v0, Lcom/google/android/cameraview/b;->a:Lcom/google/android/cameraview/c$a;

    invoke-interface {v0}, Lcom/google/android/cameraview/c$a;->a()V

    .line 122
    iget-object v0, p0, Lcom/google/android/cameraview/b$2;->a:Lcom/google/android/cameraview/b;

    invoke-static {v0}, Lcom/google/android/cameraview/b;->c(Lcom/google/android/cameraview/b;)V

    .line 123
    return-void
.end method
