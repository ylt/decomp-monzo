.class Lcom/google/android/cameraview/b$3$1;
.super Ljava/lang/Object;
.source "Camera2.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/cameraview/b$3;->onConfigured(Landroid/hardware/camera2/CameraCaptureSession;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/cameraview/b$3;


# direct methods
.method constructor <init>(Lcom/google/android/cameraview/b$3;)V
    .locals 0

    .prologue
    .line 157
    iput-object p1, p0, Lcom/google/android/cameraview/b$3$1;->a:Lcom/google/android/cameraview/b$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/cameraview/b$3$1;->a:Lcom/google/android/cameraview/b$3;

    iget-object v0, v0, Lcom/google/android/cameraview/b$3;->a:Lcom/google/android/cameraview/b;

    invoke-static {v0}, Lcom/google/android/cameraview/b;->g(Lcom/google/android/cameraview/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/google/android/cameraview/b$3$1;->a:Lcom/google/android/cameraview/b$3;

    iget-object v0, v0, Lcom/google/android/cameraview/b$3;->a:Lcom/google/android/cameraview/b;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/cameraview/b;->a(Lcom/google/android/cameraview/b;Z)Z

    .line 162
    iget-object v0, p0, Lcom/google/android/cameraview/b$3$1;->a:Lcom/google/android/cameraview/b$3;

    iget-object v0, v0, Lcom/google/android/cameraview/b$3;->a:Lcom/google/android/cameraview/b;

    invoke-static {v0}, Lcom/google/android/cameraview/b;->h(Lcom/google/android/cameraview/b;)Landroid/media/MediaRecorder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->start()V

    .line 163
    iget-object v0, p0, Lcom/google/android/cameraview/b$3$1;->a:Lcom/google/android/cameraview/b$3;

    iget-object v0, v0, Lcom/google/android/cameraview/b$3;->a:Lcom/google/android/cameraview/b;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/cameraview/b;->b(Lcom/google/android/cameraview/b;Z)Z

    .line 166
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/cameraview/b$3$1;->a:Lcom/google/android/cameraview/b$3;

    iget-object v0, v0, Lcom/google/android/cameraview/b$3;->a:Lcom/google/android/cameraview/b;

    invoke-static {v0}, Lcom/google/android/cameraview/b;->k(Lcom/google/android/cameraview/b;)Landroid/hardware/camera2/CameraCaptureSession;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/cameraview/b$3$1;->a:Lcom/google/android/cameraview/b$3;

    iget-object v1, v1, Lcom/google/android/cameraview/b$3;->a:Lcom/google/android/cameraview/b;

    invoke-static {v1}, Lcom/google/android/cameraview/b;->i(Lcom/google/android/cameraview/b;)Landroid/hardware/camera2/CaptureRequest$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->build()Landroid/hardware/camera2/CaptureRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/cameraview/b$3$1;->a:Lcom/google/android/cameraview/b$3;

    iget-object v2, v2, Lcom/google/android/cameraview/b$3;->a:Lcom/google/android/cameraview/b;

    invoke-static {v2}, Lcom/google/android/cameraview/b;->j(Lcom/google/android/cameraview/b;)Lcom/google/android/cameraview/f;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/camera2/CameraCaptureSession;->setRepeatingRequest(Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;Landroid/os/Handler;)I
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    :goto_0
    return-void

    .line 167
    :catch_0
    move-exception v0

    .line 168
    :goto_1
    const-string v1, "Camera2"

    const-string v2, "Failed to start camera preview."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 167
    :catch_1
    move-exception v0

    goto :goto_1
.end method
