.class Lcom/google/android/cameraview/b$4;
.super Lcom/google/android/cameraview/f;
.source "Camera2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/cameraview/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/cameraview/b;


# direct methods
.method constructor <init>(Lcom/google/android/cameraview/b;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/google/android/cameraview/b$4;->a:Lcom/google/android/cameraview/b;

    invoke-direct {p0}, Lcom/google/android/cameraview/f;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/cameraview/b$4;->a:Lcom/google/android/cameraview/b;

    invoke-static {v0}, Lcom/google/android/cameraview/b;->i(Lcom/google/android/cameraview/b;)Landroid/hardware/camera2/CaptureRequest$Builder;

    move-result-object v0

    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AE_PRECAPTURE_TRIGGER:Landroid/hardware/camera2/CaptureRequest$Key;

    const/4 v2, 0x1

    .line 193
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 192
    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 194
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/cameraview/b$4;->a(I)V

    .line 196
    :try_start_0
    iget-object v0, p0, Lcom/google/android/cameraview/b$4;->a:Lcom/google/android/cameraview/b;

    invoke-static {v0}, Lcom/google/android/cameraview/b;->k(Lcom/google/android/cameraview/b;)Landroid/hardware/camera2/CameraCaptureSession;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/cameraview/b$4;->a:Lcom/google/android/cameraview/b;

    invoke-static {v1}, Lcom/google/android/cameraview/b;->i(Lcom/google/android/cameraview/b;)Landroid/hardware/camera2/CaptureRequest$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->build()Landroid/hardware/camera2/CaptureRequest;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/hardware/camera2/CameraCaptureSession;->capture(Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;Landroid/os/Handler;)I

    .line 197
    iget-object v0, p0, Lcom/google/android/cameraview/b$4;->a:Lcom/google/android/cameraview/b;

    invoke-static {v0}, Lcom/google/android/cameraview/b;->i(Lcom/google/android/cameraview/b;)Landroid/hardware/camera2/CaptureRequest$Builder;

    move-result-object v0

    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AE_PRECAPTURE_TRIGGER:Landroid/hardware/camera2/CaptureRequest$Key;

    const/4 v2, 0x0

    .line 198
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 197
    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 202
    :goto_0
    return-void

    .line 199
    :catch_0
    move-exception v0

    .line 200
    const-string v1, "Camera2"

    const-string v2, "Failed to run precapture sequence."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/cameraview/b$4;->a:Lcom/google/android/cameraview/b;

    invoke-static {v0}, Lcom/google/android/cameraview/b;->l(Lcom/google/android/cameraview/b;)V

    .line 207
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/cameraview/b$4;->a:Lcom/google/android/cameraview/b;

    invoke-static {v0}, Lcom/google/android/cameraview/b;->m(Lcom/google/android/cameraview/b;)V

    .line 213
    return-void
.end method
