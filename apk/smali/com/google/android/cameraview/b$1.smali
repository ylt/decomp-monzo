.class Lcom/google/android/cameraview/b$1;
.super Ljava/lang/Object;
.source "Camera2.java"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/cameraview/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/cameraview/b;


# direct methods
.method constructor <init>(Lcom/google/android/cameraview/b;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/google/android/cameraview/b$1;->a:Lcom/google/android/cameraview/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/cameraview/b$1;->a:Lcom/google/android/cameraview/b;

    invoke-static {v0}, Lcom/google/android/cameraview/b;->a(Lcom/google/android/cameraview/b;)Lcom/google/android/cameraview/j;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/cameraview/j;->a(Landroid/graphics/SurfaceTexture;II)V

    .line 92
    iget-object v0, p0, Lcom/google/android/cameraview/b$1;->a:Lcom/google/android/cameraview/b;

    invoke-static {v0}, Lcom/google/android/cameraview/b;->b(Lcom/google/android/cameraview/b;)V

    .line 93
    iget-object v0, p0, Lcom/google/android/cameraview/b$1;->a:Lcom/google/android/cameraview/b;

    invoke-static {v0}, Lcom/google/android/cameraview/b;->c(Lcom/google/android/cameraview/b;)V

    .line 94
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 105
    iget-object v0, p0, Lcom/google/android/cameraview/b$1;->a:Lcom/google/android/cameraview/b;

    invoke-static {v0}, Lcom/google/android/cameraview/b;->a(Lcom/google/android/cameraview/b;)Lcom/google/android/cameraview/j;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, v2}, Lcom/google/android/cameraview/j;->a(Landroid/graphics/SurfaceTexture;II)V

    .line 106
    const/4 v0, 0x1

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/cameraview/b$1;->a:Lcom/google/android/cameraview/b;

    invoke-static {v0}, Lcom/google/android/cameraview/b;->a(Lcom/google/android/cameraview/b;)Lcom/google/android/cameraview/j;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/cameraview/j;->a(Landroid/graphics/SurfaceTexture;II)V

    .line 99
    iget-object v0, p0, Lcom/google/android/cameraview/b$1;->a:Lcom/google/android/cameraview/b;

    invoke-static {v0}, Lcom/google/android/cameraview/b;->b(Lcom/google/android/cameraview/b;)V

    .line 100
    iget-object v0, p0, Lcom/google/android/cameraview/b$1;->a:Lcom/google/android/cameraview/b;

    invoke-static {v0}, Lcom/google/android/cameraview/b;->c(Lcom/google/android/cameraview/b;)V

    .line 101
    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    .prologue
    .line 111
    return-void
.end method
