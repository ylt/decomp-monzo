.class Lcom/google/android/cameraview/b$3;
.super Landroid/hardware/camera2/CameraCaptureSession$StateCallback;
.source "Camera2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/cameraview/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/cameraview/b;


# direct methods
.method constructor <init>(Lcom/google/android/cameraview/b;)V
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lcom/google/android/cameraview/b$3;->a:Lcom/google/android/cameraview/b;

    invoke-direct {p0}, Landroid/hardware/camera2/CameraCaptureSession$StateCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onClosed(Landroid/hardware/camera2/CameraCaptureSession;)V
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/cameraview/b$3;->a:Lcom/google/android/cameraview/b;

    invoke-static {v0}, Lcom/google/android/cameraview/b;->k(Lcom/google/android/cameraview/b;)Landroid/hardware/camera2/CameraCaptureSession;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/cameraview/b$3;->a:Lcom/google/android/cameraview/b;

    invoke-static {v0}, Lcom/google/android/cameraview/b;->k(Lcom/google/android/cameraview/b;)Landroid/hardware/camera2/CameraCaptureSession;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/google/android/cameraview/b$3;->a:Lcom/google/android/cameraview/b;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/cameraview/b;->a(Lcom/google/android/cameraview/b;Landroid/hardware/camera2/CameraCaptureSession;)Landroid/hardware/camera2/CameraCaptureSession;

    .line 184
    :cond_0
    return-void
.end method

.method public onConfigureFailed(Landroid/hardware/camera2/CameraCaptureSession;)V
    .locals 2

    .prologue
    .line 176
    const-string v0, "Camera2"

    const-string v1, "Failed to configure capture session."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    return-void
.end method

.method public onConfigured(Landroid/hardware/camera2/CameraCaptureSession;)V
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/cameraview/b$3;->a:Lcom/google/android/cameraview/b;

    invoke-static {v0}, Lcom/google/android/cameraview/b;->d(Lcom/google/android/cameraview/b;)Landroid/hardware/camera2/CameraDevice;

    move-result-object v0

    if-nez v0, :cond_0

    .line 172
    :goto_0
    return-void

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/google/android/cameraview/b$3;->a:Lcom/google/android/cameraview/b;

    invoke-static {v0, p1}, Lcom/google/android/cameraview/b;->a(Lcom/google/android/cameraview/b;Landroid/hardware/camera2/CameraCaptureSession;)Landroid/hardware/camera2/CameraCaptureSession;

    .line 154
    iget-object v0, p0, Lcom/google/android/cameraview/b$3;->a:Lcom/google/android/cameraview/b;

    invoke-static {v0}, Lcom/google/android/cameraview/b;->e(Lcom/google/android/cameraview/b;)V

    .line 155
    iget-object v0, p0, Lcom/google/android/cameraview/b$3;->a:Lcom/google/android/cameraview/b;

    invoke-static {v0}, Lcom/google/android/cameraview/b;->f(Lcom/google/android/cameraview/b;)V

    .line 157
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/google/android/cameraview/b$3$1;

    invoke-direct {v1, p0}, Lcom/google/android/cameraview/b$3$1;-><init>(Lcom/google/android/cameraview/b$3;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
