.class abstract Lcom/google/android/cameraview/c;
.super Ljava/lang/Object;
.source "CameraViewImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/cameraview/c$a;
    }
.end annotation


# instance fields
.field protected final a:Lcom/google/android/cameraview/c$a;


# direct methods
.method public constructor <init>(Lcom/google/android/cameraview/c$a;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/cameraview/c;->a:Lcom/google/android/cameraview/c$a;

    .line 30
    return-void
.end method


# virtual methods
.method abstract a()Landroid/view/TextureView$SurfaceTextureListener;
.end method

.method abstract a(I)V
.end method

.method abstract a(Ljava/lang/String;)V
.end method

.method abstract a(Z)V
.end method

.method abstract a([Lcom/google/android/cameraview/a;)Z
.end method

.method abstract b()V
.end method

.method abstract b(I)V
.end method

.method abstract c()V
.end method

.method abstract c(I)V
.end method

.method abstract d()V
.end method

.method abstract d(I)V
.end method

.method abstract e(I)V
.end method

.method abstract e()Z
.end method

.method abstract f()I
.end method

.method abstract f(I)V
.end method

.method abstract g()I
.end method

.method abstract g(I)V
.end method

.method abstract h()I
.end method

.method abstract h(I)V
.end method

.method abstract i()I
.end method

.method abstract j()I
.end method

.method abstract k()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/cameraview/a;",
            ">;"
        }
    .end annotation
.end method

.method abstract l()[Lcom/google/android/cameraview/a;
.end method

.method abstract m()Lcom/google/android/cameraview/a;
.end method

.method abstract n()Z
.end method

.method abstract o()I
.end method

.method abstract p()V
.end method

.method abstract q()V
.end method

.method abstract r()Z
.end method
