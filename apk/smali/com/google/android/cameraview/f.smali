.class abstract Lcom/google/android/cameraview/f;
.super Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;
.source "PictureCaptureCallback.java"


# instance fields
.field private a:I

.field private b:I


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;-><init>()V

    return-void
.end method

.method private a(Landroid/hardware/camera2/CaptureResult;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x5

    .line 49
    iget v0, p0, Lcom/google/android/cameraview/f;->a:I

    packed-switch v0, :pswitch_data_0

    .line 90
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 51
    :pswitch_1
    sget-object v0, Landroid/hardware/camera2/CaptureResult;->CONTROL_AF_STATE:Landroid/hardware/camera2/CaptureResult$Key;

    invoke-virtual {p1, v0}, Landroid/hardware/camera2/CaptureResult;->get(Landroid/hardware/camera2/CaptureResult$Key;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 52
    if-eqz v0, :cond_0

    .line 55
    sget-object v1, Landroid/hardware/camera2/CaptureResult;->CONTROL_AE_STATE:Landroid/hardware/camera2/CaptureResult$Key;

    invoke-virtual {p1, v1}, Landroid/hardware/camera2/CaptureResult;->get(Landroid/hardware/camera2/CaptureResult$Key;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 56
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v7, :cond_3

    move v2, v3

    .line 57
    :goto_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v5, :cond_4

    .line 58
    :goto_2
    if-nez v2, :cond_1

    if-eqz v3, :cond_6

    iget v0, p0, Lcom/google/android/cameraview/f;->b:I

    const/4 v2, 0x3

    if-lt v0, v2, :cond_6

    .line 59
    :cond_1
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v6, :cond_5

    .line 60
    :cond_2
    invoke-virtual {p0, v5}, Lcom/google/android/cameraview/f;->a(I)V

    .line 61
    invoke-virtual {p0}, Lcom/google/android/cameraview/f;->b()V

    goto :goto_0

    :cond_3
    move v2, v4

    .line 56
    goto :goto_1

    :cond_4
    move v3, v4

    .line 57
    goto :goto_2

    .line 63
    :cond_5
    invoke-virtual {p0, v6}, Lcom/google/android/cameraview/f;->a(I)V

    .line 64
    invoke-virtual {p0}, Lcom/google/android/cameraview/f;->a()V

    goto :goto_0

    .line 66
    :cond_6
    if-eqz v3, :cond_0

    .line 67
    invoke-virtual {p0}, Lcom/google/android/cameraview/f;->c()V

    .line 68
    iget v0, p0, Lcom/google/android/cameraview/f;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/cameraview/f;->b:I

    goto :goto_0

    .line 73
    :pswitch_2
    sget-object v0, Landroid/hardware/camera2/CaptureResult;->CONTROL_AE_STATE:Landroid/hardware/camera2/CaptureResult$Key;

    invoke-virtual {p1, v0}, Landroid/hardware/camera2/CaptureResult;->get(Landroid/hardware/camera2/CaptureResult$Key;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 74
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, v5, :cond_7

    .line 75
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, v7, :cond_7

    .line 76
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v6, :cond_0

    .line 77
    :cond_7
    invoke-virtual {p0, v7}, Lcom/google/android/cameraview/f;->a(I)V

    goto :goto_0

    .line 82
    :pswitch_3
    sget-object v0, Landroid/hardware/camera2/CaptureResult;->CONTROL_AE_STATE:Landroid/hardware/camera2/CaptureResult$Key;

    invoke-virtual {p1, v0}, Landroid/hardware/camera2/CaptureResult;->get(Landroid/hardware/camera2/CaptureResult$Key;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 83
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, v5, :cond_0

    .line 84
    :cond_8
    invoke-virtual {p0, v5}, Lcom/google/android/cameraview/f;->a(I)V

    .line 85
    invoke-virtual {p0}, Lcom/google/android/cameraview/f;->b()V

    goto/16 :goto_0

    .line 49
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public abstract a()V
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 27
    iput p1, p0, Lcom/google/android/cameraview/f;->a:I

    .line 28
    return-void
.end method

.method public abstract b()V
.end method

.method public abstract c()V
.end method

.method public d()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/cameraview/f;->b:I

    .line 32
    return-void
.end method

.method public onCaptureCompleted(Landroid/hardware/camera2/CameraCaptureSession;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/TotalCaptureResult;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p3}, Lcom/google/android/cameraview/f;->a(Landroid/hardware/camera2/CaptureResult;)V

    .line 46
    return-void
.end method

.method public onCaptureProgressed(Landroid/hardware/camera2/CameraCaptureSession;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureResult;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p3}, Lcom/google/android/cameraview/f;->a(Landroid/hardware/camera2/CaptureResult;)V

    .line 39
    return-void
.end method
