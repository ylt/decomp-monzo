.class Lcom/google/android/cameraview/b;
.super Lcom/google/android/cameraview/c;
.source "Camera2.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# static fields
.field private static final b:Landroid/util/SparseIntArray;


# instance fields
.field private A:Ljava/lang/String;

.field private B:I

.field private C:I

.field private D:I

.field private E:I

.field private F:Landroid/os/HandlerThread;

.field private G:Landroid/os/Handler;

.field private H:Z

.field private I:I

.field private final c:Landroid/hardware/camera2/CameraManager;

.field private d:Landroid/media/MediaRecorder;

.field private final e:Landroid/view/TextureView$SurfaceTextureListener;

.field private final f:Landroid/hardware/camera2/CameraDevice$StateCallback;

.field private final g:Landroid/hardware/camera2/CameraCaptureSession$StateCallback;

.field private h:Lcom/google/android/cameraview/f;

.field private final i:Landroid/media/ImageReader$OnImageAvailableListener;

.field private j:Ljava/lang/String;

.field private k:Landroid/hardware/camera2/CameraCharacteristics;

.field private l:Landroid/hardware/camera2/CameraDevice;

.field private m:Landroid/hardware/camera2/CameraCaptureSession;

.field private n:Landroid/hardware/camera2/CaptureRequest$Builder;

.field private o:Landroid/media/ImageReader;

.field private final p:Lcom/google/android/cameraview/j;

.field private final q:Lcom/google/android/cameraview/i;

.field private final r:Lcom/google/android/cameraview/i;

.field private s:I

.field private t:Lcom/google/android/cameraview/a;

.field private u:[Lcom/google/android/cameraview/a;

.field private v:Z

.field private w:I

.field private x:I

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 65
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    sput-object v0, Lcom/google/android/cameraview/b;->b:Landroid/util/SparseIntArray;

    .line 68
    sget-object v0, Lcom/google/android/cameraview/b;->b:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 69
    sget-object v0, Lcom/google/android/cameraview/b;->b:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 70
    return-void
.end method

.method public constructor <init>(Lcom/google/android/cameraview/c$a;Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 289
    invoke-direct {p0, p1}, Lcom/google/android/cameraview/c;-><init>(Lcom/google/android/cameraview/c$a;)V

    .line 86
    new-instance v0, Lcom/google/android/cameraview/b$1;

    invoke-direct {v0, p0}, Lcom/google/android/cameraview/b$1;-><init>(Lcom/google/android/cameraview/b;)V

    iput-object v0, p0, Lcom/google/android/cameraview/b;->e:Landroid/view/TextureView$SurfaceTextureListener;

    .line 115
    new-instance v0, Lcom/google/android/cameraview/b$2;

    invoke-direct {v0, p0}, Lcom/google/android/cameraview/b$2;-><init>(Lcom/google/android/cameraview/b;)V

    iput-object v0, p0, Lcom/google/android/cameraview/b;->f:Landroid/hardware/camera2/CameraDevice$StateCallback;

    .line 145
    new-instance v0, Lcom/google/android/cameraview/b$3;

    invoke-direct {v0, p0}, Lcom/google/android/cameraview/b$3;-><init>(Lcom/google/android/cameraview/b;)V

    iput-object v0, p0, Lcom/google/android/cameraview/b;->g:Landroid/hardware/camera2/CameraCaptureSession$StateCallback;

    .line 188
    new-instance v0, Lcom/google/android/cameraview/b$4;

    invoke-direct {v0, p0}, Lcom/google/android/cameraview/b$4;-><init>(Lcom/google/android/cameraview/b;)V

    iput-object v0, p0, Lcom/google/android/cameraview/b;->h:Lcom/google/android/cameraview/f;

    .line 217
    new-instance v0, Lcom/google/android/cameraview/b$5;

    invoke-direct {v0, p0}, Lcom/google/android/cameraview/b$5;-><init>(Lcom/google/android/cameraview/b;)V

    iput-object v0, p0, Lcom/google/android/cameraview/b;->i:Landroid/media/ImageReader$OnImageAvailableListener;

    .line 248
    new-instance v0, Lcom/google/android/cameraview/j;

    invoke-direct {v0}, Lcom/google/android/cameraview/j;-><init>()V

    iput-object v0, p0, Lcom/google/android/cameraview/b;->p:Lcom/google/android/cameraview/j;

    .line 250
    new-instance v0, Lcom/google/android/cameraview/i;

    invoke-direct {v0}, Lcom/google/android/cameraview/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/cameraview/b;->q:Lcom/google/android/cameraview/i;

    .line 252
    new-instance v0, Lcom/google/android/cameraview/i;

    invoke-direct {v0}, Lcom/google/android/cameraview/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/cameraview/b;->r:Lcom/google/android/cameraview/i;

    .line 258
    new-array v0, v1, [Lcom/google/android/cameraview/a;

    iput-object v0, p0, Lcom/google/android/cameraview/b;->u:[Lcom/google/android/cameraview/a;

    .line 266
    iput-boolean v1, p0, Lcom/google/android/cameraview/b;->y:Z

    .line 268
    iput-boolean v1, p0, Lcom/google/android/cameraview/b;->z:Z

    .line 284
    iput-boolean v2, p0, Lcom/google/android/cameraview/b;->H:Z

    .line 286
    iput v2, p0, Lcom/google/android/cameraview/b;->I:I

    .line 290
    const-string v0, "camera"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/camera2/CameraManager;

    iput-object v0, p0, Lcom/google/android/cameraview/b;->c:Landroid/hardware/camera2/CameraManager;

    .line 291
    return-void
.end method

.method private A()V
    .locals 4

    .prologue
    .line 716
    :try_start_0
    iget-object v0, p0, Lcom/google/android/cameraview/b;->c:Landroid/hardware/camera2/CameraManager;

    iget-object v1, p0, Lcom/google/android/cameraview/b;->j:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/cameraview/b;->f:Landroid/hardware/camera2/CameraDevice$StateCallback;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/camera2/CameraManager;->openCamera(Ljava/lang/String;Landroid/hardware/camera2/CameraDevice$StateCallback;Landroid/os/Handler;)V
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 720
    return-void

    .line 717
    :catch_0
    move-exception v0

    .line 718
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to open camera: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/cameraview/b;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private B()V
    .locals 4

    .prologue
    .line 730
    invoke-virtual {p0}, Lcom/google/android/cameraview/b;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/cameraview/b;->p:Lcom/google/android/cameraview/j;

    iget-object v0, v0, Lcom/google/android/cameraview/j;->a:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/cameraview/b;->H:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/cameraview/b;->o:Landroid/media/ImageReader;

    if-nez v0, :cond_1

    .line 756
    :cond_0
    :goto_0
    return-void

    .line 734
    :cond_1
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->C()Lcom/google/android/cameraview/h;

    move-result-object v0

    .line 735
    iget-object v1, p0, Lcom/google/android/cameraview/b;->p:Lcom/google/android/cameraview/j;

    iget-object v1, v1, Lcom/google/android/cameraview/j;->a:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Lcom/google/android/cameraview/h;->a()I

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/cameraview/h;->b()I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/graphics/SurfaceTexture;->setDefaultBufferSize(II)V

    .line 736
    new-instance v0, Landroid/view/Surface;

    iget-object v1, p0, Lcom/google/android/cameraview/b;->p:Lcom/google/android/cameraview/j;

    iget-object v1, v1, Lcom/google/android/cameraview/j;->a:Landroid/graphics/SurfaceTexture;

    invoke-direct {v0, v1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    .line 737
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 738
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 740
    iget-boolean v2, p0, Lcom/google/android/cameraview/b;->y:Z

    if-eqz v2, :cond_3

    .line 741
    iget-object v2, p0, Lcom/google/android/cameraview/b;->l:Landroid/hardware/camera2/CameraDevice;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/hardware/camera2/CameraDevice;->createCaptureRequest(I)Landroid/hardware/camera2/CaptureRequest$Builder;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/cameraview/b;->n:Landroid/hardware/camera2/CaptureRequest$Builder;

    .line 742
    iget-object v2, p0, Lcom/google/android/cameraview/b;->n:Landroid/hardware/camera2/CaptureRequest$Builder;

    iget-object v3, p0, Lcom/google/android/cameraview/b;->d:Landroid/media/MediaRecorder;

    invoke-virtual {v3}, Landroid/media/MediaRecorder;->getSurface()Landroid/view/Surface;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/hardware/camera2/CaptureRequest$Builder;->addTarget(Landroid/view/Surface;)V

    .line 743
    iget-object v2, p0, Lcom/google/android/cameraview/b;->n:Landroid/hardware/camera2/CaptureRequest$Builder;

    invoke-virtual {v2, v0}, Landroid/hardware/camera2/CaptureRequest$Builder;->addTarget(Landroid/view/Surface;)V

    .line 744
    iget-object v0, p0, Lcom/google/android/cameraview/b;->d:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 752
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/android/cameraview/b;->l:Landroid/hardware/camera2/CameraDevice;

    iget-object v2, p0, Lcom/google/android/cameraview/b;->g:Landroid/hardware/camera2/CameraCaptureSession$StateCallback;

    iget-object v3, p0, Lcom/google/android/cameraview/b;->G:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/camera2/CameraDevice;->createCaptureSession(Ljava/util/List;Landroid/hardware/camera2/CameraCaptureSession$StateCallback;Landroid/os/Handler;)V
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 753
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 754
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to start capture session for mode "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v0, p0, Lcom/google/android/cameraview/b;->H:Z

    if-eqz v0, :cond_4

    const-string v0, "video"

    :goto_2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 746
    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/google/android/cameraview/b;->l:Landroid/hardware/camera2/CameraDevice;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/hardware/camera2/CameraDevice;->createCaptureRequest(I)Landroid/hardware/camera2/CaptureRequest$Builder;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/cameraview/b;->n:Landroid/hardware/camera2/CaptureRequest$Builder;

    .line 747
    iget-object v2, p0, Lcom/google/android/cameraview/b;->n:Landroid/hardware/camera2/CaptureRequest$Builder;

    invoke-virtual {v2, v0}, Landroid/hardware/camera2/CaptureRequest$Builder;->addTarget(Landroid/view/Surface;)V

    .line 748
    iget-object v0, p0, Lcom/google/android/cameraview/b;->o:Landroid/media/ImageReader;

    if-eqz v0, :cond_2

    .line 749
    iget-object v0, p0, Lcom/google/android/cameraview/b;->o:Landroid/media/ImageReader;

    invoke-virtual {v0}, Landroid/media/ImageReader;->getSurface()Landroid/view/Surface;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 754
    :cond_4
    const-string v0, "picture"

    goto :goto_2
.end method

.method private C()Lcom/google/android/cameraview/h;
    .locals 6

    .prologue
    .line 787
    iget-object v0, p0, Lcom/google/android/cameraview/b;->p:Lcom/google/android/cameraview/j;

    iget v0, v0, Lcom/google/android/cameraview/j;->b:I

    iget-object v1, p0, Lcom/google/android/cameraview/b;->p:Lcom/google/android/cameraview/j;

    iget v1, v1, Lcom/google/android/cameraview/j;->c:I

    if-ge v0, v1, :cond_1

    .line 788
    iget-object v0, p0, Lcom/google/android/cameraview/b;->p:Lcom/google/android/cameraview/j;

    iget v1, v0, Lcom/google/android/cameraview/j;->c:I

    .line 789
    iget-object v0, p0, Lcom/google/android/cameraview/b;->p:Lcom/google/android/cameraview/j;

    iget v0, v0, Lcom/google/android/cameraview/j;->b:I

    move v2, v1

    move v1, v0

    .line 794
    :goto_0
    iget-object v0, p0, Lcom/google/android/cameraview/b;->q:Lcom/google/android/cameraview/i;

    iget-object v3, p0, Lcom/google/android/cameraview/b;->t:Lcom/google/android/cameraview/a;

    invoke-virtual {v0, v3}, Lcom/google/android/cameraview/i;->b(Lcom/google/android/cameraview/a;)Ljava/util/SortedSet;

    move-result-object v3

    .line 796
    invoke-interface {v3}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/cameraview/h;

    .line 797
    invoke-virtual {v0}, Lcom/google/android/cameraview/h;->a()I

    move-result v5

    if-lt v5, v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/cameraview/h;->b()I

    move-result v5

    if-lt v5, v1, :cond_0

    .line 802
    :goto_1
    return-object v0

    .line 791
    :cond_1
    iget-object v0, p0, Lcom/google/android/cameraview/b;->p:Lcom/google/android/cameraview/j;

    iget v1, v0, Lcom/google/android/cameraview/j;->b:I

    .line 792
    iget-object v0, p0, Lcom/google/android/cameraview/b;->p:Lcom/google/android/cameraview/j;

    iget v0, v0, Lcom/google/android/cameraview/j;->c:I

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 802
    :cond_2
    invoke-interface {v3}, Ljava/util/SortedSet;->last()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/cameraview/h;

    goto :goto_1
.end method

.method private D()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x4

    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 810
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 811
    iget v1, p0, Lcom/google/android/cameraview/b;->x:I

    rem-int/lit16 v1, v1, 0xb4

    const/16 v3, 0x5a

    if-ne v1, v3, :cond_0

    .line 813
    const/16 v1, 0x8

    new-array v1, v1, [F

    aput v7, v1, v2

    aput v7, v1, v6

    iget-object v3, p0, Lcom/google/android/cameraview/b;->p:Lcom/google/android/cameraview/j;

    iget v3, v3, Lcom/google/android/cameraview/j;->b:I

    int-to-float v3, v3

    aput v3, v1, v8

    const/4 v3, 0x3

    aput v7, v1, v3

    aput v7, v1, v5

    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/android/cameraview/b;->p:Lcom/google/android/cameraview/j;

    iget v4, v4, Lcom/google/android/cameraview/j;->c:I

    int-to-float v4, v4

    aput v4, v1, v3

    const/4 v3, 0x6

    iget-object v4, p0, Lcom/google/android/cameraview/b;->p:Lcom/google/android/cameraview/j;

    iget v4, v4, Lcom/google/android/cameraview/j;->b:I

    int-to-float v4, v4

    aput v4, v1, v3

    const/4 v3, 0x7

    iget-object v4, p0, Lcom/google/android/cameraview/b;->p:Lcom/google/android/cameraview/j;

    iget v4, v4, Lcom/google/android/cameraview/j;->c:I

    int-to-float v4, v4

    aput v4, v1, v3

    iget v3, p0, Lcom/google/android/cameraview/b;->x:I

    const/16 v4, 0x5a

    if-ne v3, v4, :cond_1

    const/16 v3, 0x8

    new-array v3, v3, [F

    aput v7, v3, v2

    iget-object v4, p0, Lcom/google/android/cameraview/b;->p:Lcom/google/android/cameraview/j;

    iget v4, v4, Lcom/google/android/cameraview/j;->c:I

    int-to-float v4, v4

    aput v4, v3, v6

    aput v7, v3, v8

    const/4 v4, 0x3

    aput v7, v3, v4

    iget-object v4, p0, Lcom/google/android/cameraview/b;->p:Lcom/google/android/cameraview/j;

    iget v4, v4, Lcom/google/android/cameraview/j;->b:I

    int-to-float v4, v4

    aput v4, v3, v5

    const/4 v4, 0x5

    iget-object v6, p0, Lcom/google/android/cameraview/b;->p:Lcom/google/android/cameraview/j;

    iget v6, v6, Lcom/google/android/cameraview/j;->c:I

    int-to-float v6, v6

    aput v6, v3, v4

    const/4 v4, 0x6

    iget-object v6, p0, Lcom/google/android/cameraview/b;->p:Lcom/google/android/cameraview/j;

    iget v6, v6, Lcom/google/android/cameraview/j;->b:I

    int-to-float v6, v6

    aput v6, v3, v4

    const/4 v4, 0x7

    aput v7, v3, v4

    :goto_0
    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Matrix;->setPolyToPoly([FI[FII)Z

    .line 838
    :cond_0
    iget-object v1, p0, Lcom/google/android/cameraview/b;->a:Lcom/google/android/cameraview/c$a;

    invoke-interface {v1, v0}, Lcom/google/android/cameraview/c$a;->a(Landroid/graphics/Matrix;)V

    .line 839
    return-void

    .line 813
    :cond_1
    const/16 v3, 0x8

    new-array v3, v3, [F

    iget-object v4, p0, Lcom/google/android/cameraview/b;->p:Lcom/google/android/cameraview/j;

    iget v4, v4, Lcom/google/android/cameraview/j;->b:I

    int-to-float v4, v4

    aput v4, v3, v2

    aput v7, v3, v6

    iget-object v4, p0, Lcom/google/android/cameraview/b;->p:Lcom/google/android/cameraview/j;

    iget v4, v4, Lcom/google/android/cameraview/j;->b:I

    int-to-float v4, v4

    aput v4, v3, v8

    const/4 v4, 0x3

    iget-object v6, p0, Lcom/google/android/cameraview/b;->p:Lcom/google/android/cameraview/j;

    iget v6, v6, Lcom/google/android/cameraview/j;->c:I

    int-to-float v6, v6

    aput v6, v3, v4

    aput v7, v3, v5

    const/4 v4, 0x5

    aput v7, v3, v4

    const/4 v4, 0x6

    aput v7, v3, v4

    const/4 v4, 0x7

    iget-object v6, p0, Lcom/google/android/cameraview/b;->p:Lcom/google/android/cameraview/j;

    iget v6, v6, Lcom/google/android/cameraview/j;->c:I

    int-to-float v6, v6

    aput v6, v3, v4

    goto :goto_0
.end method

.method private E()V
    .locals 3

    .prologue
    .line 845
    const/4 v0, 0x0

    .line 846
    iget-boolean v1, p0, Lcom/google/android/cameraview/b;->v:Z

    if-eqz v1, :cond_0

    .line 847
    iget-object v0, p0, Lcom/google/android/cameraview/b;->k:Landroid/hardware/camera2/CameraCharacteristics;

    invoke-static {v0}, Lcom/google/android/cameraview/e;->a(Landroid/hardware/camera2/CameraCharacteristics;)I

    move-result v0

    .line 849
    :cond_0
    iget-object v1, p0, Lcom/google/android/cameraview/b;->n:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v2, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AF_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 850
    return-void
.end method

.method private F()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 856
    iget v0, p0, Lcom/google/android/cameraview/b;->w:I

    packed-switch v0, :pswitch_data_0

    .line 888
    :goto_0
    return-void

    .line 858
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/cameraview/b;->n:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AE_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    .line 859
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 858
    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 860
    iget-object v0, p0, Lcom/google/android/cameraview/b;->n:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->FLASH_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    .line 861
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 860
    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    goto :goto_0

    .line 864
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/cameraview/b;->n:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AE_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    const/4 v2, 0x3

    .line 865
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 864
    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 866
    iget-object v0, p0, Lcom/google/android/cameraview/b;->n:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->FLASH_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    .line 867
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 866
    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    goto :goto_0

    .line 870
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/cameraview/b;->n:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AE_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    .line 871
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 870
    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 872
    iget-object v0, p0, Lcom/google/android/cameraview/b;->n:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->FLASH_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    .line 873
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 872
    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    goto :goto_0

    .line 876
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/cameraview/b;->n:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AE_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    .line 877
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 876
    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 878
    iget-object v0, p0, Lcom/google/android/cameraview/b;->n:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->FLASH_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    .line 879
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 878
    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    goto :goto_0

    .line 882
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/cameraview/b;->n:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AE_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    const/4 v2, 0x4

    .line 883
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 882
    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 884
    iget-object v0, p0, Lcom/google/android/cameraview/b;->n:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->FLASH_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    .line 885
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 884
    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    goto :goto_0

    .line 856
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private G()V
    .locals 4

    .prologue
    .line 895
    :try_start_0
    iget-object v0, p0, Lcom/google/android/cameraview/b;->n:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AF_TRIGGER:Landroid/hardware/camera2/CaptureRequest$Key;

    const/4 v2, 0x1

    .line 896
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 895
    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 897
    iget-object v0, p0, Lcom/google/android/cameraview/b;->h:Lcom/google/android/cameraview/f;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/cameraview/f;->a(I)V

    .line 898
    iget-object v0, p0, Lcom/google/android/cameraview/b;->m:Landroid/hardware/camera2/CameraCaptureSession;

    iget-object v1, p0, Lcom/google/android/cameraview/b;->n:Landroid/hardware/camera2/CaptureRequest$Builder;

    invoke-virtual {v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->build()Landroid/hardware/camera2/CaptureRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/cameraview/b;->h:Lcom/google/android/cameraview/f;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/camera2/CameraCaptureSession;->capture(Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;Landroid/os/Handler;)I
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 902
    :goto_0
    return-void

    .line 899
    :catch_0
    move-exception v0

    .line 900
    const-string v1, "Camera2"

    const-string v2, "Failed to lock focus."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private H()V
    .locals 4

    .prologue
    .line 909
    :try_start_0
    iget-object v0, p0, Lcom/google/android/cameraview/b;->l:Landroid/hardware/camera2/CameraDevice;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/hardware/camera2/CameraDevice;->createCaptureRequest(I)Landroid/hardware/camera2/CaptureRequest$Builder;

    move-result-object v0

    .line 911
    iget-object v1, p0, Lcom/google/android/cameraview/b;->o:Landroid/media/ImageReader;

    invoke-virtual {v1}, Landroid/media/ImageReader;->getSurface()Landroid/view/Surface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->addTarget(Landroid/view/Surface;)V

    .line 912
    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AF_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    iget-object v2, p0, Lcom/google/android/cameraview/b;->n:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v3, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AF_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    .line 913
    invoke-virtual {v2, v3}, Landroid/hardware/camera2/CaptureRequest$Builder;->get(Landroid/hardware/camera2/CaptureRequest$Key;)Ljava/lang/Object;

    move-result-object v2

    .line 912
    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 914
    iget v1, p0, Lcom/google/android/cameraview/b;->w:I

    packed-switch v1, :pswitch_data_0

    .line 941
    :goto_0
    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->JPEG_ORIENTATION:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-direct {p0}, Lcom/google/android/cameraview/b;->I()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 943
    iget-object v1, p0, Lcom/google/android/cameraview/b;->m:Landroid/hardware/camera2/CameraCaptureSession;

    invoke-virtual {v1}, Landroid/hardware/camera2/CameraCaptureSession;->stopRepeating()V

    .line 944
    iget-object v1, p0, Lcom/google/android/cameraview/b;->m:Landroid/hardware/camera2/CameraCaptureSession;

    invoke-virtual {v0}, Landroid/hardware/camera2/CaptureRequest$Builder;->build()Landroid/hardware/camera2/CaptureRequest;

    move-result-object v0

    new-instance v2, Lcom/google/android/cameraview/b$7;

    invoke-direct {v2, p0}, Lcom/google/android/cameraview/b$7;-><init>(Lcom/google/android/cameraview/b;)V

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/hardware/camera2/CameraCaptureSession;->capture(Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;Landroid/os/Handler;)I

    .line 956
    :goto_1
    return-void

    .line 916
    :pswitch_0
    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AE_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    const/4 v2, 0x1

    .line 917
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 916
    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 918
    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->FLASH_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    const/4 v2, 0x0

    .line 919
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 918
    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 953
    :catch_0
    move-exception v0

    .line 954
    const-string v1, "Camera2"

    const-string v2, "Cannot capture a still picture."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 922
    :pswitch_1
    :try_start_1
    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AE_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    const/4 v2, 0x3

    .line 923
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 922
    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    goto :goto_0

    .line 926
    :pswitch_2
    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AE_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    const/4 v2, 0x1

    .line 927
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 926
    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 928
    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->FLASH_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    const/4 v2, 0x2

    .line 929
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 928
    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    goto :goto_0

    .line 932
    :pswitch_3
    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AE_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    const/4 v2, 0x2

    .line 933
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 932
    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    goto :goto_0

    .line 936
    :pswitch_4
    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AE_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    const/4 v2, 0x2

    .line 937
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 936
    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V
    :try_end_1
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 914
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private I()I
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 960
    iget-object v0, p0, Lcom/google/android/cameraview/b;->k:Landroid/hardware/camera2/CameraCharacteristics;

    sget-object v2, Landroid/hardware/camera2/CameraCharacteristics;->SENSOR_ORIENTATION:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {v0, v2}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 962
    iget v3, p0, Lcom/google/android/cameraview/b;->x:I

    iget v0, p0, Lcom/google/android/cameraview/b;->s:I

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    mul-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit16 v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private J()V
    .locals 4

    .prologue
    .line 970
    iget-object v0, p0, Lcom/google/android/cameraview/b;->n:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AF_TRIGGER:Landroid/hardware/camera2/CaptureRequest$Key;

    const/4 v2, 0x2

    .line 971
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 970
    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 973
    :try_start_0
    iget-object v0, p0, Lcom/google/android/cameraview/b;->m:Landroid/hardware/camera2/CameraCaptureSession;

    iget-object v1, p0, Lcom/google/android/cameraview/b;->n:Landroid/hardware/camera2/CaptureRequest$Builder;

    invoke-virtual {v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->build()Landroid/hardware/camera2/CaptureRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/cameraview/b;->h:Lcom/google/android/cameraview/f;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/camera2/CameraCaptureSession;->capture(Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;Landroid/os/Handler;)I

    .line 974
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->E()V

    .line 975
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->F()V

    .line 976
    iget-object v0, p0, Lcom/google/android/cameraview/b;->n:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AF_TRIGGER:Landroid/hardware/camera2/CaptureRequest$Key;

    const/4 v2, 0x0

    .line 977
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 976
    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 978
    iget-object v0, p0, Lcom/google/android/cameraview/b;->m:Landroid/hardware/camera2/CameraCaptureSession;

    iget-object v1, p0, Lcom/google/android/cameraview/b;->n:Landroid/hardware/camera2/CaptureRequest$Builder;

    invoke-virtual {v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->build()Landroid/hardware/camera2/CaptureRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/cameraview/b;->h:Lcom/google/android/cameraview/f;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/camera2/CameraCaptureSession;->setRepeatingRequest(Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;Landroid/os/Handler;)I

    .line 980
    iget-object v0, p0, Lcom/google/android/cameraview/b;->h:Lcom/google/android/cameraview/f;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/cameraview/f;->a(I)V
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 984
    :goto_0
    return-void

    .line 981
    :catch_0
    move-exception v0

    .line 982
    const-string v1, "Camera2"

    const-string v2, "Failed to restart camera preview."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/cameraview/b;Landroid/hardware/camera2/CameraCaptureSession;)Landroid/hardware/camera2/CameraCaptureSession;
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/cameraview/b;->m:Landroid/hardware/camera2/CameraCaptureSession;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/cameraview/b;Landroid/hardware/camera2/CameraDevice;)Landroid/hardware/camera2/CameraDevice;
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/cameraview/b;->l:Landroid/hardware/camera2/CameraDevice;

    return-object p1
.end method

.method private a(II)Lcom/google/android/cameraview/h;
    .locals 5

    .prologue
    .line 759
    iget-object v0, p0, Lcom/google/android/cameraview/b;->r:Lcom/google/android/cameraview/i;

    iget-object v1, p0, Lcom/google/android/cameraview/b;->t:Lcom/google/android/cameraview/a;

    invoke-virtual {v0, v1}, Lcom/google/android/cameraview/i;->b(Lcom/google/android/cameraview/a;)Ljava/util/SortedSet;

    move-result-object v1

    .line 760
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 761
    invoke-interface {v1}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/cameraview/h;

    .line 762
    invoke-virtual {v0}, Lcom/google/android/cameraview/h;->a()I

    move-result v4

    if-lt v4, p1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/cameraview/h;->b()I

    move-result v4

    if-lt v4, p2, :cond_0

    .line 763
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 767
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 768
    new-instance v0, Lcom/google/android/cameraview/b$6;

    invoke-direct {v0, p0}, Lcom/google/android/cameraview/b$6;-><init>(Lcom/google/android/cameraview/b;)V

    invoke-static {v2, v0}, Ljava/util/Collections;->min(Ljava/util/Collection;Ljava/util/Comparator;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/cameraview/h;

    .line 776
    :goto_1
    return-object v0

    .line 775
    :cond_2
    const-string v0, "Camera2"

    const-string v2, "Couldn\'t find any suitable video recording size"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 776
    invoke-interface {v1}, Ljava/util/SortedSet;->last()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/cameraview/h;

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/cameraview/b;)Lcom/google/android/cameraview/j;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/cameraview/b;->p:Lcom/google/android/cameraview/j;

    return-object v0
.end method

.method private a(Landroid/hardware/camera2/params/StreamConfigurationMap;)V
    .locals 7

    .prologue
    .line 670
    iget-boolean v0, p0, Lcom/google/android/cameraview/b;->H:Z

    if-eqz v0, :cond_0

    const-class v0, Landroid/media/MediaRecorder;

    .line 671
    invoke-virtual {p1, v0}, Landroid/hardware/camera2/params/StreamConfigurationMap;->getOutputSizes(Ljava/lang/Class;)[Landroid/util/Size;

    move-result-object v0

    .line 672
    :goto_0
    array-length v2, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 673
    iget-object v4, p0, Lcom/google/android/cameraview/b;->r:Lcom/google/android/cameraview/i;

    new-instance v5, Lcom/google/android/cameraview/h;

    invoke-virtual {v3}, Landroid/util/Size;->getWidth()I

    move-result v6

    invoke-virtual {v3}, Landroid/util/Size;->getHeight()I

    move-result v3

    invoke-direct {v5, v6, v3}, Lcom/google/android/cameraview/h;-><init>(II)V

    invoke-virtual {v4, v5}, Lcom/google/android/cameraview/i;->a(Lcom/google/android/cameraview/h;)Z

    .line 672
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 671
    :cond_0
    const/16 v0, 0x100

    invoke-virtual {p1, v0}, Landroid/hardware/camera2/params/StreamConfigurationMap;->getOutputSizes(I)[Landroid/util/Size;

    move-result-object v0

    goto :goto_0

    .line 675
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/google/android/cameraview/b;Z)Z
    .locals 0

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/google/android/cameraview/b;->y:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/cameraview/b;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->D()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/cameraview/b;Z)Z
    .locals 0

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/google/android/cameraview/b;->z:Z

    return p1
.end method

.method static synthetic c(Lcom/google/android/cameraview/b;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->B()V

    return-void
.end method

.method static synthetic d(Lcom/google/android/cameraview/b;)Landroid/hardware/camera2/CameraDevice;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/cameraview/b;->l:Landroid/hardware/camera2/CameraDevice;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/cameraview/b;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->E()V

    return-void
.end method

.method static synthetic f(Lcom/google/android/cameraview/b;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->F()V

    return-void
.end method

.method static synthetic g(Lcom/google/android/cameraview/b;)Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/android/cameraview/b;->y:Z

    return v0
.end method

.method static synthetic h(Lcom/google/android/cameraview/b;)Landroid/media/MediaRecorder;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/cameraview/b;->d:Landroid/media/MediaRecorder;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/cameraview/b;)Landroid/hardware/camera2/CaptureRequest$Builder;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/cameraview/b;->n:Landroid/hardware/camera2/CaptureRequest$Builder;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/cameraview/b;)Lcom/google/android/cameraview/f;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/cameraview/b;->h:Lcom/google/android/cameraview/f;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/cameraview/b;)Landroid/hardware/camera2/CameraCaptureSession;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/cameraview/b;->m:Landroid/hardware/camera2/CameraCaptureSession;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/cameraview/b;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->H()V

    return-void
.end method

.method static synthetic m(Lcom/google/android/cameraview/b;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->G()V

    return-void
.end method

.method static synthetic n(Lcom/google/android/cameraview/b;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->J()V

    return-void
.end method

.method private s()V
    .locals 2

    .prologue
    .line 339
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "CameraBackground"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/cameraview/b;->F:Landroid/os/HandlerThread;

    .line 340
    iget-object v0, p0, Lcom/google/android/cameraview/b;->F:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 341
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/cameraview/b;->F:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/cameraview/b;->G:Landroid/os/Handler;

    .line 342
    return-void
.end method

.method private t()V
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/android/cameraview/b;->F:Landroid/os/HandlerThread;

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/google/android/cameraview/b;->F:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quitSafely()Z

    .line 348
    :try_start_0
    iget-object v0, p0, Lcom/google/android/cameraview/b;->F:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->join()V

    .line 349
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/cameraview/b;->F:Landroid/os/HandlerThread;

    .line 350
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/cameraview/b;->G:Landroid/os/Handler;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 355
    :cond_0
    :goto_0
    return-void

    .line 351
    :catch_0
    move-exception v0

    .line 352
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method private u()V
    .locals 1

    .prologue
    .line 568
    iget-object v0, p0, Lcom/google/android/cameraview/b;->m:Landroid/hardware/camera2/CameraCaptureSession;

    if-eqz v0, :cond_0

    .line 569
    iget-object v0, p0, Lcom/google/android/cameraview/b;->m:Landroid/hardware/camera2/CameraCaptureSession;

    invoke-virtual {v0}, Landroid/hardware/camera2/CameraCaptureSession;->close()V

    .line 570
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/cameraview/b;->m:Landroid/hardware/camera2/CameraCaptureSession;

    .line 572
    :cond_0
    return-void
.end method

.method private v()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 582
    :try_start_0
    sget-object v0, Lcom/google/android/cameraview/b;->b:Landroid/util/SparseIntArray;

    iget v2, p0, Lcom/google/android/cameraview/b;->s:I

    invoke-virtual {v0, v2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v3

    .line 583
    iget-object v0, p0, Lcom/google/android/cameraview/b;->c:Landroid/hardware/camera2/CameraManager;

    invoke-virtual {v0}, Landroid/hardware/camera2/CameraManager;->getCameraIdList()[Ljava/lang/String;

    move-result-object v4

    .line 584
    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_2

    aget-object v6, v4, v2

    .line 585
    iget-object v0, p0, Lcom/google/android/cameraview/b;->c:Landroid/hardware/camera2/CameraManager;

    invoke-virtual {v0, v6}, Landroid/hardware/camera2/CameraManager;->getCameraCharacteristics(Ljava/lang/String;)Landroid/hardware/camera2/CameraCharacteristics;

    move-result-object v7

    .line 586
    sget-object v0, Landroid/hardware/camera2/CameraCharacteristics;->LENS_FACING:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {v7, v0}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 587
    if-nez v0, :cond_0

    .line 588
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Unexpected state: LENS_FACING null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 612
    :catch_0
    move-exception v0

    .line 613
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Failed to get a list of camera devices"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 590
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 591
    iput-object v6, p0, Lcom/google/android/cameraview/b;->j:Ljava/lang/String;

    .line 592
    iput-object v7, p0, Lcom/google/android/cameraview/b;->k:Landroid/hardware/camera2/CameraCharacteristics;

    .line 615
    :goto_1
    return-void

    .line 584
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 597
    :cond_2
    const/4 v0, 0x0

    aget-object v0, v4, v0

    iput-object v0, p0, Lcom/google/android/cameraview/b;->j:Ljava/lang/String;

    .line 598
    iget-object v0, p0, Lcom/google/android/cameraview/b;->c:Landroid/hardware/camera2/CameraManager;

    iget-object v2, p0, Lcom/google/android/cameraview/b;->j:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/hardware/camera2/CameraManager;->getCameraCharacteristics(Ljava/lang/String;)Landroid/hardware/camera2/CameraCharacteristics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/cameraview/b;->k:Landroid/hardware/camera2/CameraCharacteristics;

    .line 599
    iget-object v0, p0, Lcom/google/android/cameraview/b;->k:Landroid/hardware/camera2/CameraCharacteristics;

    sget-object v2, Landroid/hardware/camera2/CameraCharacteristics;->LENS_FACING:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {v0, v2}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 600
    if-nez v0, :cond_3

    .line 601
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Unexpected state: LENS_FACING null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 603
    :cond_3
    sget-object v2, Lcom/google/android/cameraview/b;->b:Landroid/util/SparseIntArray;

    invoke-virtual {v2}, Landroid/util/SparseIntArray;->size()I

    move-result v2

    :goto_2
    if-ge v1, v2, :cond_5

    .line 604
    sget-object v3, Lcom/google/android/cameraview/b;->b:Landroid/util/SparseIntArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v3, v4, :cond_4

    .line 605
    sget-object v0, Lcom/google/android/cameraview/b;->b:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/cameraview/b;->s:I

    goto :goto_1

    .line 603
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 611
    :cond_5
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/cameraview/b;->s:I
    :try_end_1
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method private w()V
    .locals 8

    .prologue
    .line 624
    iget-object v0, p0, Lcom/google/android/cameraview/b;->k:Landroid/hardware/camera2/CameraCharacteristics;

    sget-object v1, Landroid/hardware/camera2/CameraCharacteristics;->SCALER_STREAM_CONFIGURATION_MAP:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {v0, v1}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/camera2/params/StreamConfigurationMap;

    .line 626
    if-nez v0, :cond_0

    .line 627
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to get configuration map: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/cameraview/b;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 629
    :cond_0
    iget-object v1, p0, Lcom/google/android/cameraview/b;->q:Lcom/google/android/cameraview/i;

    invoke-virtual {v1}, Lcom/google/android/cameraview/i;->b()V

    .line 630
    const-class v1, Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0, v1}, Landroid/hardware/camera2/params/StreamConfigurationMap;->getOutputSizes(Ljava/lang/Class;)[Landroid/util/Size;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 631
    invoke-virtual {v4}, Landroid/util/Size;->getWidth()I

    move-result v5

    .line 632
    invoke-virtual {v4}, Landroid/util/Size;->getHeight()I

    move-result v4

    .line 633
    const/16 v6, 0x780

    if-gt v5, v6, :cond_1

    const/16 v6, 0x438

    if-gt v4, v6, :cond_1

    .line 634
    iget-object v6, p0, Lcom/google/android/cameraview/b;->q:Lcom/google/android/cameraview/i;

    new-instance v7, Lcom/google/android/cameraview/h;

    invoke-direct {v7, v5, v4}, Lcom/google/android/cameraview/h;-><init>(II)V

    invoke-virtual {v6, v7}, Lcom/google/android/cameraview/i;->a(Lcom/google/android/cameraview/h;)Z

    .line 630
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 637
    :cond_2
    iget-object v1, p0, Lcom/google/android/cameraview/b;->r:Lcom/google/android/cameraview/i;

    invoke-virtual {v1}, Lcom/google/android/cameraview/i;->b()V

    .line 638
    invoke-direct {p0, v0}, Lcom/google/android/cameraview/b;->a(Landroid/hardware/camera2/params/StreamConfigurationMap;)V

    .line 639
    iget-object v0, p0, Lcom/google/android/cameraview/b;->q:Lcom/google/android/cameraview/i;

    invoke-virtual {v0}, Lcom/google/android/cameraview/i;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/cameraview/a;

    .line 640
    iget-object v2, p0, Lcom/google/android/cameraview/b;->r:Lcom/google/android/cameraview/i;

    invoke-virtual {v2}, Lcom/google/android/cameraview/i;->a()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 641
    iget-object v2, p0, Lcom/google/android/cameraview/b;->q:Lcom/google/android/cameraview/i;

    invoke-virtual {v2, v0}, Lcom/google/android/cameraview/i;->a(Lcom/google/android/cameraview/a;)V

    goto :goto_1

    .line 645
    :cond_4
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->x()Lcom/google/android/cameraview/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/cameraview/b;->t:Lcom/google/android/cameraview/a;

    .line 646
    return-void
.end method

.method private x()Lcom/google/android/cameraview/a;
    .locals 5

    .prologue
    .line 656
    iget-object v0, p0, Lcom/google/android/cameraview/b;->q:Lcom/google/android/cameraview/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/cameraview/b;->q:Lcom/google/android/cameraview/i;

    invoke-virtual {v0}, Lcom/google/android/cameraview/i;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 657
    :cond_0
    const/4 v0, 0x0

    .line 666
    :cond_1
    :goto_0
    return-object v0

    .line 659
    :cond_2
    iget-object v2, p0, Lcom/google/android/cameraview/b;->u:[Lcom/google/android/cameraview/a;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v0, v2, v1

    .line 660
    iget-object v4, p0, Lcom/google/android/cameraview/b;->q:Lcom/google/android/cameraview/i;

    invoke-virtual {v4}, Lcom/google/android/cameraview/i;->a()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 659
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 665
    :cond_3
    iget-object v0, p0, Lcom/google/android/cameraview/b;->q:Lcom/google/android/cameraview/i;

    iget v1, p0, Lcom/google/android/cameraview/b;->I:I

    invoke-virtual {v0, v1}, Lcom/google/android/cameraview/i;->a(I)Lcom/google/android/cameraview/h;

    move-result-object v0

    .line 666
    invoke-virtual {v0}, Lcom/google/android/cameraview/h;->a()I

    move-result v1

    invoke-virtual {v0}, Lcom/google/android/cameraview/h;->b()I

    move-result v0

    invoke-static {v1, v0}, Lcom/google/android/cameraview/a;->a(II)Lcom/google/android/cameraview/a;

    move-result-object v0

    goto :goto_0
.end method

.method private y()V
    .locals 4

    .prologue
    .line 678
    iget-object v0, p0, Lcom/google/android/cameraview/b;->o:Landroid/media/ImageReader;

    if-eqz v0, :cond_0

    .line 679
    iget-object v0, p0, Lcom/google/android/cameraview/b;->o:Landroid/media/ImageReader;

    invoke-virtual {v0}, Landroid/media/ImageReader;->close()V

    .line 681
    :cond_0
    iget-object v0, p0, Lcom/google/android/cameraview/b;->r:Lcom/google/android/cameraview/i;

    iget-object v1, p0, Lcom/google/android/cameraview/b;->t:Lcom/google/android/cameraview/a;

    invoke-virtual {v0, v1}, Lcom/google/android/cameraview/i;->b(Lcom/google/android/cameraview/a;)Ljava/util/SortedSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedSet;->last()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/cameraview/h;

    .line 682
    invoke-virtual {v0}, Lcom/google/android/cameraview/h;->a()I

    move-result v1

    invoke-virtual {v0}, Lcom/google/android/cameraview/h;->b()I

    move-result v0

    const/16 v2, 0x100

    const/4 v3, 0x2

    invoke-static {v1, v0, v2, v3}, Landroid/media/ImageReader;->newInstance(IIII)Landroid/media/ImageReader;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/cameraview/b;->o:Landroid/media/ImageReader;

    .line 684
    iget-object v0, p0, Lcom/google/android/cameraview/b;->o:Landroid/media/ImageReader;

    iget-object v1, p0, Lcom/google/android/cameraview/b;->i:Landroid/media/ImageReader$OnImageAvailableListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/media/ImageReader;->setOnImageAvailableListener(Landroid/media/ImageReader$OnImageAvailableListener;Landroid/os/Handler;)V

    .line 685
    return-void
.end method

.method private z()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    .line 688
    iget-object v0, p0, Lcom/google/android/cameraview/b;->d:Landroid/media/MediaRecorder;

    if-nez v0, :cond_0

    .line 689
    new-instance v0, Landroid/media/MediaRecorder;

    invoke-direct {v0}, Landroid/media/MediaRecorder;-><init>()V

    iput-object v0, p0, Lcom/google/android/cameraview/b;->d:Landroid/media/MediaRecorder;

    .line 694
    :goto_0
    iget v0, p0, Lcom/google/android/cameraview/b;->D:I

    iget v1, p0, Lcom/google/android/cameraview/b;->E:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/cameraview/b;->a(II)Lcom/google/android/cameraview/h;

    move-result-object v0

    .line 696
    iget-object v1, p0, Lcom/google/android/cameraview/b;->d:Landroid/media/MediaRecorder;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/media/MediaRecorder;->setAudioSource(I)V

    .line 697
    iget-object v1, p0, Lcom/google/android/cameraview/b;->d:Landroid/media/MediaRecorder;

    invoke-virtual {v1, v3}, Landroid/media/MediaRecorder;->setVideoSource(I)V

    .line 698
    iget-object v1, p0, Lcom/google/android/cameraview/b;->d:Landroid/media/MediaRecorder;

    invoke-virtual {v1, v3}, Landroid/media/MediaRecorder;->setOutputFormat(I)V

    .line 699
    iget-object v1, p0, Lcom/google/android/cameraview/b;->d:Landroid/media/MediaRecorder;

    iget-object v2, p0, Lcom/google/android/cameraview/b;->A:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/media/MediaRecorder;->setOutputFile(Ljava/lang/String;)V

    .line 700
    iget-object v1, p0, Lcom/google/android/cameraview/b;->d:Landroid/media/MediaRecorder;

    iget v2, p0, Lcom/google/android/cameraview/b;->B:I

    invoke-virtual {v1, v2}, Landroid/media/MediaRecorder;->setVideoEncodingBitRate(I)V

    .line 701
    iget-object v1, p0, Lcom/google/android/cameraview/b;->d:Landroid/media/MediaRecorder;

    iget v2, p0, Lcom/google/android/cameraview/b;->C:I

    invoke-virtual {v1, v2}, Landroid/media/MediaRecorder;->setVideoFrameRate(I)V

    .line 702
    iget-object v1, p0, Lcom/google/android/cameraview/b;->d:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Lcom/google/android/cameraview/h;->a()I

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/cameraview/h;->b()I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/media/MediaRecorder;->setVideoSize(II)V

    .line 703
    iget-object v0, p0, Lcom/google/android/cameraview/b;->d:Landroid/media/MediaRecorder;

    invoke-virtual {v0, v3}, Landroid/media/MediaRecorder;->setVideoEncoder(I)V

    .line 704
    iget-object v0, p0, Lcom/google/android/cameraview/b;->d:Landroid/media/MediaRecorder;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setAudioEncoder(I)V

    .line 705
    iget-object v0, p0, Lcom/google/android/cameraview/b;->d:Landroid/media/MediaRecorder;

    invoke-direct {p0}, Lcom/google/android/cameraview/b;->I()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setOrientationHint(I)V

    .line 706
    iget-object v0, p0, Lcom/google/android/cameraview/b;->d:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->prepare()V

    .line 707
    return-void

    .line 691
    :cond_0
    iget-object v0, p0, Lcom/google/android/cameraview/b;->d:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->reset()V

    goto :goto_0
.end method


# virtual methods
.method a()Landroid/view/TextureView$SurfaceTextureListener;
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/cameraview/b;->e:Landroid/view/TextureView$SurfaceTextureListener;

    return-object v0
.end method

.method a(I)V
    .locals 0

    .prologue
    .line 364
    iput p1, p0, Lcom/google/android/cameraview/b;->B:I

    .line 365
    return-void
.end method

.method a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 534
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/cameraview/b;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 545
    :goto_0
    return-void

    .line 537
    :cond_0
    iput-object p1, p0, Lcom/google/android/cameraview/b;->A:Ljava/lang/String;

    .line 538
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/cameraview/b;->y:Z

    .line 539
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->z()V

    .line 540
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->u()V

    .line 541
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->B()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 542
    :catch_0
    move-exception v0

    .line 543
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method a(Z)V
    .locals 4

    .prologue
    .line 468
    iget-boolean v0, p0, Lcom/google/android/cameraview/b;->v:Z

    if-ne v0, p1, :cond_1

    .line 483
    :cond_0
    :goto_0
    return-void

    .line 471
    :cond_1
    iput-boolean p1, p0, Lcom/google/android/cameraview/b;->v:Z

    .line 472
    iget-object v0, p0, Lcom/google/android/cameraview/b;->n:Landroid/hardware/camera2/CaptureRequest$Builder;

    if-eqz v0, :cond_0

    .line 473
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->E()V

    .line 474
    iget-object v0, p0, Lcom/google/android/cameraview/b;->m:Landroid/hardware/camera2/CameraCaptureSession;

    if-eqz v0, :cond_0

    .line 476
    :try_start_0
    iget-object v0, p0, Lcom/google/android/cameraview/b;->m:Landroid/hardware/camera2/CameraCaptureSession;

    iget-object v1, p0, Lcom/google/android/cameraview/b;->n:Landroid/hardware/camera2/CaptureRequest$Builder;

    invoke-virtual {v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->build()Landroid/hardware/camera2/CaptureRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/cameraview/b;->h:Lcom/google/android/cameraview/f;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/camera2/CameraCaptureSession;->setRepeatingRequest(Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;Landroid/os/Handler;)I
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 478
    :catch_0
    move-exception v0

    .line 479
    iget-boolean v0, p0, Lcom/google/android/cameraview/b;->v:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/cameraview/b;->v:Z

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method a([Lcom/google/android/cameraview/a;)Z
    .locals 2

    .prologue
    .line 430
    iput-object p1, p0, Lcom/google/android/cameraview/b;->u:[Lcom/google/android/cameraview/a;

    .line 431
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->x()Lcom/google/android/cameraview/a;

    move-result-object v0

    .line 433
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/cameraview/b;->t:Lcom/google/android/cameraview/a;

    invoke-virtual {v0, v1}, Lcom/google/android/cameraview/a;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 434
    :cond_0
    const/4 v0, 0x0

    .line 447
    :goto_0
    return v0

    .line 438
    :cond_1
    iget-object v0, p0, Lcom/google/android/cameraview/b;->o:Landroid/media/ImageReader;

    if-eqz v0, :cond_2

    .line 439
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->y()V

    .line 441
    :cond_2
    iget-object v0, p0, Lcom/google/android/cameraview/b;->m:Landroid/hardware/camera2/CameraCaptureSession;

    if-eqz v0, :cond_3

    .line 442
    iget-object v0, p0, Lcom/google/android/cameraview/b;->m:Landroid/hardware/camera2/CameraCaptureSession;

    invoke-virtual {v0}, Landroid/hardware/camera2/CameraCaptureSession;->close()V

    .line 443
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/cameraview/b;->m:Landroid/hardware/camera2/CameraCaptureSession;

    .line 444
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->B()V

    .line 447
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method b()V
    .locals 1

    .prologue
    .line 301
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/cameraview/b;->H:Z

    .line 302
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->s()V

    .line 303
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->v()V

    .line 304
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->w()V

    .line 305
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->A()V

    .line 306
    return-void
.end method

.method b(I)V
    .locals 0

    .prologue
    .line 374
    iput p1, p0, Lcom/google/android/cameraview/b;->C:I

    .line 375
    return-void
.end method

.method c()V
    .locals 1

    .prologue
    .line 310
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/cameraview/b;->H:Z

    .line 311
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->s()V

    .line 312
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->v()V

    .line 313
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->w()V

    .line 314
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->y()V

    .line 315
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->A()V

    .line 316
    return-void
.end method

.method c(I)V
    .locals 0

    .prologue
    .line 384
    iput p1, p0, Lcom/google/android/cameraview/b;->D:I

    .line 385
    return-void
.end method

.method d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 320
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->u()V

    .line 321
    iget-object v0, p0, Lcom/google/android/cameraview/b;->l:Landroid/hardware/camera2/CameraDevice;

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p0, Lcom/google/android/cameraview/b;->l:Landroid/hardware/camera2/CameraDevice;

    invoke-virtual {v0}, Landroid/hardware/camera2/CameraDevice;->close()V

    .line 323
    iput-object v1, p0, Lcom/google/android/cameraview/b;->l:Landroid/hardware/camera2/CameraDevice;

    .line 325
    :cond_0
    iget-object v0, p0, Lcom/google/android/cameraview/b;->o:Landroid/media/ImageReader;

    if-eqz v0, :cond_1

    .line 326
    iget-object v0, p0, Lcom/google/android/cameraview/b;->o:Landroid/media/ImageReader;

    invoke-virtual {v0}, Landroid/media/ImageReader;->close()V

    .line 327
    iput-object v1, p0, Lcom/google/android/cameraview/b;->o:Landroid/media/ImageReader;

    .line 329
    :cond_1
    iget-object v0, p0, Lcom/google/android/cameraview/b;->d:Landroid/media/MediaRecorder;

    if-eqz v0, :cond_2

    .line 330
    iget-object v0, p0, Lcom/google/android/cameraview/b;->d:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->release()V

    .line 331
    iput-object v1, p0, Lcom/google/android/cameraview/b;->d:Landroid/media/MediaRecorder;

    .line 333
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/cameraview/b;->z:Z

    .line 335
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->t()V

    .line 336
    return-void
.end method

.method d(I)V
    .locals 0

    .prologue
    .line 394
    iput p1, p0, Lcom/google/android/cameraview/b;->E:I

    .line 395
    return-void
.end method

.method e(I)V
    .locals 1

    .prologue
    .line 404
    iget v0, p0, Lcom/google/android/cameraview/b;->s:I

    if-ne v0, p1, :cond_1

    .line 416
    :cond_0
    :goto_0
    return-void

    .line 407
    :cond_1
    iput p1, p0, Lcom/google/android/cameraview/b;->s:I

    .line 408
    invoke-virtual {p0}, Lcom/google/android/cameraview/b;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 409
    invoke-virtual {p0}, Lcom/google/android/cameraview/b;->d()V

    .line 410
    iget-boolean v0, p0, Lcom/google/android/cameraview/b;->H:Z

    if-eqz v0, :cond_2

    .line 411
    invoke-virtual {p0}, Lcom/google/android/cameraview/b;->b()V

    goto :goto_0

    .line 413
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/cameraview/b;->c()V

    goto :goto_0
.end method

.method e()Z
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/google/android/cameraview/b;->l:Landroid/hardware/camera2/CameraDevice;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method f()I
    .locals 1

    .prologue
    .line 369
    iget v0, p0, Lcom/google/android/cameraview/b;->B:I

    return v0
.end method

.method f(I)V
    .locals 0

    .prologue
    .line 463
    iput p1, p0, Lcom/google/android/cameraview/b;->I:I

    .line 464
    return-void
.end method

.method g()I
    .locals 1

    .prologue
    .line 379
    iget v0, p0, Lcom/google/android/cameraview/b;->C:I

    return v0
.end method

.method g(I)V
    .locals 5

    .prologue
    .line 492
    iget v0, p0, Lcom/google/android/cameraview/b;->w:I

    if-ne v0, p1, :cond_1

    .line 508
    :cond_0
    :goto_0
    return-void

    .line 495
    :cond_1
    iget v0, p0, Lcom/google/android/cameraview/b;->w:I

    .line 496
    iput p1, p0, Lcom/google/android/cameraview/b;->w:I

    .line 497
    iget-object v1, p0, Lcom/google/android/cameraview/b;->n:Landroid/hardware/camera2/CaptureRequest$Builder;

    if-eqz v1, :cond_0

    .line 498
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->F()V

    .line 499
    iget-object v1, p0, Lcom/google/android/cameraview/b;->m:Landroid/hardware/camera2/CameraCaptureSession;

    if-eqz v1, :cond_0

    .line 501
    :try_start_0
    iget-object v1, p0, Lcom/google/android/cameraview/b;->m:Landroid/hardware/camera2/CameraCaptureSession;

    iget-object v2, p0, Lcom/google/android/cameraview/b;->n:Landroid/hardware/camera2/CaptureRequest$Builder;

    invoke-virtual {v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->build()Landroid/hardware/camera2/CaptureRequest;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/cameraview/b;->h:Lcom/google/android/cameraview/f;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/hardware/camera2/CameraCaptureSession;->setRepeatingRequest(Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;Landroid/os/Handler;)I
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 503
    :catch_0
    move-exception v1

    .line 504
    iput v0, p0, Lcom/google/android/cameraview/b;->w:I

    goto :goto_0
.end method

.method h()I
    .locals 1

    .prologue
    .line 389
    iget v0, p0, Lcom/google/android/cameraview/b;->D:I

    return v0
.end method

.method h(I)V
    .locals 0

    .prologue
    .line 527
    iput p1, p0, Lcom/google/android/cameraview/b;->x:I

    .line 528
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->D()V

    .line 529
    return-void
.end method

.method i()I
    .locals 1

    .prologue
    .line 399
    iget v0, p0, Lcom/google/android/cameraview/b;->E:I

    return v0
.end method

.method j()I
    .locals 1

    .prologue
    .line 420
    iget v0, p0, Lcom/google/android/cameraview/b;->s:I

    return v0
.end method

.method k()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/cameraview/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 425
    iget-object v0, p0, Lcom/google/android/cameraview/b;->q:Lcom/google/android/cameraview/i;

    invoke-virtual {v0}, Lcom/google/android/cameraview/i;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method l()[Lcom/google/android/cameraview/a;
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/google/android/cameraview/b;->u:[Lcom/google/android/cameraview/a;

    return-object v0
.end method

.method m()Lcom/google/android/cameraview/a;
    .locals 1

    .prologue
    .line 458
    iget-object v0, p0, Lcom/google/android/cameraview/b;->t:Lcom/google/android/cameraview/a;

    return-object v0
.end method

.method n()Z
    .locals 1

    .prologue
    .line 487
    iget-boolean v0, p0, Lcom/google/android/cameraview/b;->v:Z

    return v0
.end method

.method o()I
    .locals 1

    .prologue
    .line 512
    iget v0, p0, Lcom/google/android/cameraview/b;->w:I

    return v0
.end method

.method p()V
    .locals 1

    .prologue
    .line 517
    iget-object v0, p0, Lcom/google/android/cameraview/b;->h:Lcom/google/android/cameraview/f;

    invoke-virtual {v0}, Lcom/google/android/cameraview/f;->d()V

    .line 518
    iget-boolean v0, p0, Lcom/google/android/cameraview/b;->v:Z

    if-eqz v0, :cond_0

    .line 519
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->G()V

    .line 523
    :goto_0
    return-void

    .line 521
    :cond_0
    invoke-direct {p0}, Lcom/google/android/cameraview/b;->H()V

    goto :goto_0
.end method

.method q()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 549
    iput-boolean v0, p0, Lcom/google/android/cameraview/b;->y:Z

    .line 551
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/cameraview/b;->z:Z

    .line 552
    iget-object v0, p0, Lcom/google/android/cameraview/b;->d:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->stop()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 558
    iget-object v0, p0, Lcom/google/android/cameraview/b;->d:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->reset()V

    .line 560
    :goto_0
    return-void

    .line 553
    :catch_0
    move-exception v0

    .line 554
    :try_start_1
    const-string v1, "Camera2"

    const-string v2, "Failed to stop video recording."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 556
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/cameraview/b;->A:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 558
    iget-object v0, p0, Lcom/google/android/cameraview/b;->d:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->reset()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/cameraview/b;->d:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->reset()V

    throw v0
.end method

.method r()Z
    .locals 1

    .prologue
    .line 564
    iget-boolean v0, p0, Lcom/google/android/cameraview/b;->z:Z

    return v0
.end method
