.class public Lcom/google/android/cameraview/CameraView;
.super Landroid/widget/FrameLayout;
.source "CameraView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/cameraview/CameraView$a;,
        Lcom/google/android/cameraview/CameraView$c;,
        Lcom/google/android/cameraview/CameraView$b;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcom/google/android/cameraview/c;

.field private final c:Lcom/google/android/cameraview/CameraView$b;

.field private d:Z

.field private final e:Landroid/view/TextureView;

.field private final f:Lcom/google/android/cameraview/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/google/android/cameraview/CameraView;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/cameraview/CameraView;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/cameraview/CameraView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 86
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/cameraview/CameraView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 94
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 96
    new-instance v0, Lcom/google/android/cameraview/CameraView$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/cameraview/CameraView$b;-><init>(Lcom/google/android/cameraview/CameraView;Lcom/google/android/cameraview/CameraView$1;)V

    iput-object v0, p0, Lcom/google/android/cameraview/CameraView;->c:Lcom/google/android/cameraview/CameraView$b;

    .line 97
    new-instance v0, Lcom/google/android/cameraview/b;

    iget-object v1, p0, Lcom/google/android/cameraview/CameraView;->c:Lcom/google/android/cameraview/CameraView$b;

    invoke-direct {v0, v1, p1}, Lcom/google/android/cameraview/b;-><init>(Lcom/google/android/cameraview/c$a;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    .line 99
    sget v0, Lcom/google/android/cameraview/g$b;->camera_view:I

    invoke-static {p1, v0, p0}, Lcom/google/android/cameraview/CameraView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 100
    sget v0, Lcom/google/android/cameraview/g$a;->texture_view:I

    invoke-virtual {p0, v0}, Lcom/google/android/cameraview/CameraView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/TextureView;

    iput-object v0, p0, Lcom/google/android/cameraview/CameraView;->e:Landroid/view/TextureView;

    .line 101
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->e:Landroid/view/TextureView;

    iget-object v1, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    invoke-virtual {v1}, Lcom/google/android/cameraview/c;->a()Landroid/view/TextureView$SurfaceTextureListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 103
    sget-object v0, Lcom/google/android/cameraview/g$d;->CameraView:[I

    sget v1, Lcom/google/android/cameraview/g$c;->Widget_CameraView:I

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 105
    sget v1, Lcom/google/android/cameraview/g$d;->CameraView_android_adjustViewBounds:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/cameraview/CameraView;->d:Z

    .line 106
    sget v1, Lcom/google/android/cameraview/g$d;->CameraView_facing:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/cameraview/CameraView;->setFacing(I)V

    .line 107
    sget v1, Lcom/google/android/cameraview/g$d;->CameraView_videoEncodingBitRate:I

    const v2, 0x4c4b40

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/cameraview/CameraView;->setVideoEncodingBitRate(I)V

    .line 108
    sget v1, Lcom/google/android/cameraview/g$d;->CameraView_videoFrameRate:I

    const/16 v2, 0x1e

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/cameraview/CameraView;->setVideoFrameRate(I)V

    .line 109
    sget v1, Lcom/google/android/cameraview/g$d;->CameraView_minVideoWidth:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/cameraview/CameraView;->setMinVideoWidth(I)V

    .line 110
    sget v1, Lcom/google/android/cameraview/g$d;->CameraView_minVideoHeight:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/cameraview/CameraView;->setMinVideoHeight(I)V

    .line 111
    sget v1, Lcom/google/android/cameraview/g$d;->CameraView_preferredAspectRatios:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 112
    if-eqz v1, :cond_0

    .line 113
    invoke-direct {p0, v1}, Lcom/google/android/cameraview/CameraView;->b(Ljava/lang/String;)[Lcom/google/android/cameraview/a;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/cameraview/CameraView;->setPreferredAspectRatio([Lcom/google/android/cameraview/a;)V

    .line 115
    :cond_0
    sget v1, Lcom/google/android/cameraview/g$d;->CameraView_autoFocus:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/cameraview/CameraView;->setAutoFocus(Z)V

    .line 116
    sget v1, Lcom/google/android/cameraview/g$d;->CameraView_flash:I

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/cameraview/CameraView;->setFlash(I)V

    .line 117
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 119
    new-instance v0, Lcom/google/android/cameraview/CameraView$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/cameraview/CameraView$1;-><init>(Lcom/google/android/cameraview/CameraView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/cameraview/CameraView;->f:Lcom/google/android/cameraview/d;

    .line 125
    return-void
.end method

.method static synthetic a(Lcom/google/android/cameraview/CameraView;)Lcom/google/android/cameraview/c;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/cameraview/CameraView;)Landroid/view/TextureView;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->e:Landroid/view/TextureView;

    return-object v0
.end method

.method private b(Ljava/lang/String;)[Lcom/google/android/cameraview/a;
    .locals 5

    .prologue
    .line 128
    const-string v0, "\\|"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 129
    new-instance v2, Ljava/util/ArrayList;

    array-length v0, v1

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 130
    array-length v3, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v1, v0

    .line 131
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/cameraview/a;->a(Ljava/lang/String;)Lcom/google/android/cameraview/a;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 133
    :cond_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/cameraview/a;

    .line 134
    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/cameraview/a;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    invoke-virtual {p0}, Lcom/google/android/cameraview/CameraView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v0, v1}, Lcom/google/android/cameraview/c;->f(I)V

    .line 239
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    invoke-virtual {v0}, Lcom/google/android/cameraview/c;->c()V

    .line 240
    return-void
.end method

.method public a(Lcom/google/android/cameraview/CameraView$a;)V
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->c:Lcom/google/android/cameraview/CameraView$b;

    invoke-virtual {v0, p1}, Lcom/google/android/cameraview/CameraView$b;->a(Lcom/google/android/cameraview/CameraView$a;)V

    .line 275
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    invoke-virtual {v0, p1}, Lcom/google/android/cameraview/c;->a(Ljava/lang/String;)V

    .line 480
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    invoke-virtual {p0}, Lcom/google/android/cameraview/CameraView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v0, v1}, Lcom/google/android/cameraview/c;->f(I)V

    .line 249
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    invoke-virtual {v0}, Lcom/google/android/cameraview/c;->b()V

    .line 250
    return-void
.end method

.method public b(Lcom/google/android/cameraview/CameraView$a;)V
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->c:Lcom/google/android/cameraview/CameraView$b;

    invoke-virtual {v0, p1}, Lcom/google/android/cameraview/CameraView$b;->b(Lcom/google/android/cameraview/CameraView$a;)V

    .line 285
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    invoke-virtual {v0}, Lcom/google/android/cameraview/c;->d()V

    .line 258
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    invoke-virtual {v0}, Lcom/google/android/cameraview/c;->e()Z

    move-result v0

    return v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    invoke-virtual {v0}, Lcom/google/android/cameraview/c;->q()V

    .line 487
    return-void
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 493
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    invoke-virtual {v0}, Lcom/google/android/cameraview/c;->r()Z

    move-result v0

    return v0
.end method

.method public g()V
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    invoke-virtual {v0}, Lcom/google/android/cameraview/c;->p()V

    .line 502
    return-void
.end method

.method public getAdjustViewBounds()Z
    .locals 1

    .prologue
    .line 305
    iget-boolean v0, p0, Lcom/google/android/cameraview/CameraView;->d:Z

    return v0
.end method

.method public getAspectRatio()Lcom/google/android/cameraview/a;
    .locals 1

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    invoke-virtual {v0}, Lcom/google/android/cameraview/c;->m()Lcom/google/android/cameraview/a;

    move-result-object v0

    return-object v0
.end method

.method public getAutoFocus()Z
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    invoke-virtual {v0}, Lcom/google/android/cameraview/c;->n()Z

    move-result v0

    return v0
.end method

.method public getFacing()I
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    invoke-virtual {v0}, Lcom/google/android/cameraview/c;->j()I

    move-result v0

    return v0
.end method

.method public getFlash()I
    .locals 1

    .prologue
    .line 470
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    invoke-virtual {v0}, Lcom/google/android/cameraview/c;->o()I

    move-result v0

    return v0
.end method

.method getMinVideoHeight()I
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    invoke-virtual {v0}, Lcom/google/android/cameraview/c;->i()I

    move-result v0

    return v0
.end method

.method getMinVideoWidth()I
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    invoke-virtual {v0}, Lcom/google/android/cameraview/c;->h()I

    move-result v0

    return v0
.end method

.method public getPreferredAspectRatios()[Lcom/google/android/cameraview/a;
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    invoke-virtual {v0}, Lcom/google/android/cameraview/c;->l()[Lcom/google/android/cameraview/a;

    move-result-object v0

    return-object v0
.end method

.method public getSupportedAspectRatios()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/cameraview/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 398
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    invoke-virtual {v0}, Lcom/google/android/cameraview/c;->k()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method getVideoEncodingBitRate()I
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    invoke-virtual {v0}, Lcom/google/android/cameraview/c;->f()I

    move-result v0

    return v0
.end method

.method getVideoFrameRate()I
    .locals 1

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    invoke-virtual {v0}, Lcom/google/android/cameraview/c;->g()I

    move-result v0

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 139
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 140
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->f:Lcom/google/android/cameraview/d;

    invoke-static {p0}, Lcom/google/android/cameraview/k;->a(Landroid/view/View;)Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/cameraview/d;->a(Landroid/view/Display;)V

    .line 141
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->f:Lcom/google/android/cameraview/d;

    invoke-virtual {v0}, Lcom/google/android/cameraview/d;->a()V

    .line 146
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 147
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/high16 v3, -0x80000000

    const/high16 v5, 0x40000000    # 2.0f

    .line 152
    iget-boolean v0, p0, Lcom/google/android/cameraview/CameraView;->d:Z

    if-eqz v0, :cond_9

    .line 153
    invoke-virtual {p0}, Lcom/google/android/cameraview/CameraView;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 154
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->c:Lcom/google/android/cameraview/CameraView$b;

    invoke-virtual {v0}, Lcom/google/android/cameraview/CameraView$b;->c()V

    .line 155
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 158
    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 159
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 160
    if-ne v1, v5, :cond_5

    if-eq v2, v5, :cond_5

    .line 161
    invoke-virtual {p0}, Lcom/google/android/cameraview/CameraView;->getAspectRatio()Lcom/google/android/cameraview/a;

    move-result-object v0

    .line 162
    sget-boolean v1, Lcom/google/android/cameraview/CameraView;->a:Z

    if-nez v1, :cond_2

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 163
    :cond_2
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0}, Lcom/google/android/cameraview/a;->c()F

    move-result v0

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 164
    if-ne v2, v3, :cond_3

    .line 165
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 168
    :cond_3
    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 167
    invoke-super {p0, p1, v0}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 185
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/cameraview/CameraView;->getMeasuredWidth()I

    move-result v1

    .line 186
    invoke-virtual {p0}, Lcom/google/android/cameraview/CameraView;->getMeasuredHeight()I

    move-result v2

    .line 187
    invoke-virtual {p0}, Lcom/google/android/cameraview/CameraView;->getAspectRatio()Lcom/google/android/cameraview/a;

    move-result-object v0

    .line 188
    if-eqz v0, :cond_0

    .line 192
    iget-object v3, p0, Lcom/google/android/cameraview/CameraView;->f:Lcom/google/android/cameraview/d;

    invoke-virtual {v3}, Lcom/google/android/cameraview/d;->b()I

    move-result v3

    rem-int/lit16 v3, v3, 0xb4

    if-nez v3, :cond_4

    .line 193
    invoke-virtual {v0}, Lcom/google/android/cameraview/a;->d()Lcom/google/android/cameraview/a;

    move-result-object v0

    .line 196
    :cond_4
    invoke-virtual {v0}, Lcom/google/android/cameraview/a;->b()I

    move-result v3

    mul-int/2addr v3, v1

    invoke-virtual {v0}, Lcom/google/android/cameraview/a;->a()I

    move-result v4

    div-int/2addr v3, v4

    if-ge v2, v3, :cond_a

    .line 197
    iget-object v2, p0, Lcom/google/android/cameraview/CameraView;->e:Landroid/view/TextureView;

    .line 198
    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 199
    invoke-virtual {v0}, Lcom/google/android/cameraview/a;->b()I

    move-result v4

    mul-int/2addr v1, v4

    invoke-virtual {v0}, Lcom/google/android/cameraview/a;->a()I

    move-result v0

    div-int v0, v1, v0

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 197
    invoke-virtual {v2, v3, v0}, Landroid/view/TextureView;->measure(II)V

    goto :goto_0

    .line 169
    :cond_5
    if-eq v1, v5, :cond_8

    if-ne v2, v5, :cond_8

    .line 170
    invoke-virtual {p0}, Lcom/google/android/cameraview/CameraView;->getAspectRatio()Lcom/google/android/cameraview/a;

    move-result-object v0

    .line 171
    sget-boolean v2, Lcom/google/android/cameraview/CameraView;->a:Z

    if-nez v2, :cond_6

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 172
    :cond_6
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0}, Lcom/google/android/cameraview/a;->c()F

    move-result v0

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 173
    if-ne v1, v3, :cond_7

    .line 174
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 176
    :cond_7
    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, v0, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    goto :goto_1

    .line 179
    :cond_8
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    goto :goto_1

    .line 182
    :cond_9
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    goto :goto_1

    .line 202
    :cond_a
    iget-object v1, p0, Lcom/google/android/cameraview/CameraView;->e:Landroid/view/TextureView;

    .line 203
    invoke-virtual {v0}, Lcom/google/android/cameraview/a;->a()I

    move-result v3

    mul-int/2addr v3, v2

    invoke-virtual {v0}, Lcom/google/android/cameraview/a;->b()I

    move-result v0

    div-int v0, v3, v0

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 205
    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 202
    invoke-virtual {v1, v0, v2}, Landroid/view/TextureView;->measure(II)V

    goto/16 :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 221
    instance-of v0, p1, Lcom/google/android/cameraview/CameraView$c;

    if-nez v0, :cond_0

    .line 222
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 231
    :goto_0
    return-void

    .line 225
    :cond_0
    check-cast p1, Lcom/google/android/cameraview/CameraView$c;

    .line 226
    invoke-virtual {p1}, Lcom/google/android/cameraview/CameraView$c;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 227
    iget v0, p1, Lcom/google/android/cameraview/CameraView$c;->a:I

    invoke-virtual {p0, v0}, Lcom/google/android/cameraview/CameraView;->setFacing(I)V

    .line 228
    iget-object v0, p1, Lcom/google/android/cameraview/CameraView$c;->b:[Lcom/google/android/cameraview/a;

    invoke-virtual {p0, v0}, Lcom/google/android/cameraview/CameraView;->setPreferredAspectRatio([Lcom/google/android/cameraview/a;)V

    .line 229
    iget-boolean v0, p1, Lcom/google/android/cameraview/CameraView$c;->c:Z

    invoke-virtual {p0, v0}, Lcom/google/android/cameraview/CameraView;->setAutoFocus(Z)V

    .line 230
    iget v0, p1, Lcom/google/android/cameraview/CameraView$c;->d:I

    invoke-virtual {p0, v0}, Lcom/google/android/cameraview/CameraView;->setFlash(I)V

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 211
    new-instance v0, Lcom/google/android/cameraview/CameraView$c;

    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/cameraview/CameraView$c;-><init>(Landroid/os/Parcelable;)V

    .line 212
    invoke-virtual {p0}, Lcom/google/android/cameraview/CameraView;->getFacing()I

    move-result v1

    iput v1, v0, Lcom/google/android/cameraview/CameraView$c;->a:I

    .line 213
    invoke-virtual {p0}, Lcom/google/android/cameraview/CameraView;->getPreferredAspectRatios()[Lcom/google/android/cameraview/a;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/cameraview/CameraView$c;->b:[Lcom/google/android/cameraview/a;

    .line 214
    invoke-virtual {p0}, Lcom/google/android/cameraview/CameraView;->getAutoFocus()Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/android/cameraview/CameraView$c;->c:Z

    .line 215
    invoke-virtual {p0}, Lcom/google/android/cameraview/CameraView;->getFlash()I

    move-result v1

    iput v1, v0, Lcom/google/android/cameraview/CameraView$c;->d:I

    .line 216
    return-object v0
.end method

.method public setAdjustViewBounds(Z)V
    .locals 1

    .prologue
    .line 293
    iget-boolean v0, p0, Lcom/google/android/cameraview/CameraView;->d:Z

    if-eq v0, p1, :cond_0

    .line 294
    iput-boolean p1, p0, Lcom/google/android/cameraview/CameraView;->d:Z

    .line 295
    invoke-virtual {p0}, Lcom/google/android/cameraview/CameraView;->requestLayout()V

    .line 297
    :cond_0
    return-void
.end method

.method public setAutoFocus(Z)V
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    invoke-virtual {v0, p1}, Lcom/google/android/cameraview/c;->a(Z)V

    .line 441
    return-void
.end method

.method public setFacing(I)V
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    invoke-virtual {v0, p1}, Lcom/google/android/cameraview/c;->e(I)V

    .line 316
    return-void
.end method

.method public setFlash(I)V
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    invoke-virtual {v0, p1}, Lcom/google/android/cameraview/c;->g(I)V

    .line 460
    return-void
.end method

.method setMinVideoHeight(I)V
    .locals 1

    .prologue
    .line 383
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    invoke-virtual {v0, p1}, Lcom/google/android/cameraview/c;->d(I)V

    .line 384
    return-void
.end method

.method setMinVideoWidth(I)V
    .locals 1

    .prologue
    .line 367
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    invoke-virtual {v0, p1}, Lcom/google/android/cameraview/c;->c(I)V

    .line 368
    return-void
.end method

.method public setPreferredAspectRatio([Lcom/google/android/cameraview/a;)V
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    invoke-virtual {v0, p1}, Lcom/google/android/cameraview/c;->a([Lcom/google/android/cameraview/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 412
    invoke-virtual {p0}, Lcom/google/android/cameraview/CameraView;->requestLayout()V

    .line 414
    :cond_0
    return-void
.end method

.method setVideoEncodingBitRate(I)V
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    invoke-virtual {v0, p1}, Lcom/google/android/cameraview/c;->a(I)V

    .line 336
    return-void
.end method

.method setVideoFrameRate(I)V
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lcom/google/android/cameraview/CameraView;->b:Lcom/google/android/cameraview/c;

    invoke-virtual {v0, p1}, Lcom/google/android/cameraview/c;->b(I)V

    .line 352
    return-void
.end method
