.class public Lcom/google/android/cameraview/a;
.super Ljava/lang/Object;
.source "AspectRatio.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/cameraview/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/cameraview/a;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:Landroid/support/v4/g/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/g/n",
            "<",
            "Landroid/support/v4/g/n",
            "<",
            "Lcom/google/android/cameraview/a;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:I

.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    new-instance v0, Landroid/support/v4/g/n;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Landroid/support/v4/g/n;-><init>(I)V

    sput-object v0, Lcom/google/android/cameraview/a;->a:Landroid/support/v4/g/n;

    .line 184
    new-instance v0, Lcom/google/android/cameraview/a$1;

    invoke-direct {v0}, Lcom/google/android/cameraview/a$1;-><init>()V

    sput-object v0, Lcom/google/android/cameraview/a;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(II)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput p1, p0, Lcom/google/android/cameraview/a;->b:I

    .line 88
    iput p2, p0, Lcom/google/android/cameraview/a;->c:I

    .line 89
    return-void
.end method

.method public static a(II)Lcom/google/android/cameraview/a;
    .locals 4

    .prologue
    .line 45
    invoke-static {p0, p1}, Lcom/google/android/cameraview/a;->b(II)I

    move-result v0

    .line 46
    div-int v2, p0, v0

    .line 47
    div-int v3, p1, v0

    .line 48
    sget-object v0, Lcom/google/android/cameraview/a;->a:Landroid/support/v4/g/n;

    invoke-virtual {v0, v2}, Landroid/support/v4/g/n;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/g/n;

    .line 49
    if-nez v0, :cond_1

    .line 50
    new-instance v1, Lcom/google/android/cameraview/a;

    invoke-direct {v1, v2, v3}, Lcom/google/android/cameraview/a;-><init>(II)V

    .line 51
    new-instance v0, Landroid/support/v4/g/n;

    invoke-direct {v0}, Landroid/support/v4/g/n;-><init>()V

    .line 52
    invoke-virtual {v0, v3, v1}, Landroid/support/v4/g/n;->b(ILjava/lang/Object;)V

    .line 53
    sget-object v3, Lcom/google/android/cameraview/a;->a:Landroid/support/v4/g/n;

    invoke-virtual {v3, v2, v0}, Landroid/support/v4/g/n;->b(ILjava/lang/Object;)V

    .line 61
    :cond_0
    :goto_0
    return-object v1

    .line 56
    :cond_1
    invoke-virtual {v0, v3}, Landroid/support/v4/g/n;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/cameraview/a;

    .line 57
    if-nez v1, :cond_0

    .line 58
    new-instance v1, Lcom/google/android/cameraview/a;

    invoke-direct {v1, v2, v3}, Lcom/google/android/cameraview/a;-><init>(II)V

    .line 59
    invoke-virtual {v0, v3, v1}, Landroid/support/v4/g/n;->b(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/cameraview/a;
    .locals 4

    .prologue
    .line 73
    const/16 v0, 0x3a

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 74
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 75
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Malformed aspect ratio: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 78
    :cond_0
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 79
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 80
    invoke-static {v1, v0}, Lcom/google/android/cameraview/a;->a(II)Lcom/google/android/cameraview/a;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 81
    :catch_0
    move-exception v0

    .line 82
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Malformed aspect ratio: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static b(II)I
    .locals 1

    .prologue
    .line 165
    :goto_0
    if-eqz p1, :cond_0

    .line 167
    rem-int v0, p0, p1

    move p0, p1

    move p1, v0

    .line 169
    goto :goto_0

    .line 170
    :cond_0
    return p0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lcom/google/android/cameraview/a;->b:I

    return v0
.end method

.method public a(Lcom/google/android/cameraview/a;)I
    .locals 2

    .prologue
    .line 148
    invoke-virtual {p0, p1}, Lcom/google/android/cameraview/a;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    const/4 v0, 0x0

    .line 153
    :goto_0
    return v0

    .line 150
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/cameraview/a;->c()F

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/cameraview/a;->c()F

    move-result v1

    sub-float/2addr v0, v1

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 151
    const/4 v0, 0x1

    goto :goto_0

    .line 153
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public a(I)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 107
    packed-switch p1, :pswitch_data_0

    move v0, v1

    .line 113
    :cond_0
    :goto_0
    return v0

    .line 109
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/cameraview/a;->c()F

    move-result v2

    cmpl-float v2, v2, v3

    if-gez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 111
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/cameraview/a;->c()F

    move-result v2

    cmpg-float v2, v2, v3

    if-lez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 107
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcom/google/android/cameraview/h;)Z
    .locals 3

    .prologue
    .line 100
    invoke-virtual {p1}, Lcom/google/android/cameraview/h;->a()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/cameraview/h;->b()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/cameraview/a;->b(II)I

    move-result v0

    .line 101
    invoke-virtual {p1}, Lcom/google/android/cameraview/h;->a()I

    move-result v1

    div-int/2addr v1, v0

    .line 102
    invoke-virtual {p1}, Lcom/google/android/cameraview/h;->b()I

    move-result v2

    div-int v0, v2, v0

    .line 103
    iget v2, p0, Lcom/google/android/cameraview/a;->b:I

    if-ne v2, v1, :cond_0

    iget v1, p0, Lcom/google/android/cameraview/a;->c:I

    if-ne v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/google/android/cameraview/a;->c:I

    return v0
.end method

.method public c()F
    .locals 2

    .prologue
    .line 137
    iget v0, p0, Lcom/google/android/cameraview/a;->b:I

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/cameraview/a;->c:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 28
    check-cast p1, Lcom/google/android/cameraview/a;

    invoke-virtual {p0, p1}, Lcom/google/android/cameraview/a;->a(Lcom/google/android/cameraview/a;)I

    move-result v0

    return v0
.end method

.method public d()Lcom/google/android/cameraview/a;
    .locals 2

    .prologue
    .line 161
    iget v0, p0, Lcom/google/android/cameraview/a;->c:I

    iget v1, p0, Lcom/google/android/cameraview/a;->b:I

    invoke-static {v0, v1}, Lcom/google/android/cameraview/a;->a(II)Lcom/google/android/cameraview/a;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 118
    if-nez p1, :cond_1

    .line 128
    :cond_0
    :goto_0
    return v1

    .line 121
    :cond_1
    if-ne p0, p1, :cond_2

    move v1, v0

    .line 122
    goto :goto_0

    .line 124
    :cond_2
    instance-of v2, p1, Lcom/google/android/cameraview/a;

    if-eqz v2, :cond_0

    .line 125
    check-cast p1, Lcom/google/android/cameraview/a;

    .line 126
    iget v2, p0, Lcom/google/android/cameraview/a;->b:I

    iget v3, p1, Lcom/google/android/cameraview/a;->b:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/cameraview/a;->c:I

    iget v3, p1, Lcom/google/android/cameraview/a;->c:I

    if-ne v2, v3, :cond_3

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 143
    iget v0, p0, Lcom/google/android/cameraview/a;->c:I

    iget v1, p0, Lcom/google/android/cameraview/a;->b:I

    shl-int/lit8 v1, v1, 0x10

    iget v2, p0, Lcom/google/android/cameraview/a;->b:I

    ushr-int/lit8 v2, v2, 0x10

    or-int/2addr v1, v2

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 133
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/google/android/cameraview/a;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/cameraview/a;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 180
    iget v0, p0, Lcom/google/android/cameraview/a;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 181
    iget v0, p0, Lcom/google/android/cameraview/a;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 182
    return-void
.end method
