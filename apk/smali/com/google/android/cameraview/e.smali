.class final Lcom/google/android/cameraview/e;
.super Ljava/lang/Object;
.source "FocusModeSelector.java"


# static fields
.field private static a:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/cameraview/e;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x4
        0x1
    .end array-data
.end method

.method static a(Landroid/hardware/camera2/CameraCharacteristics;)I
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 19
    sget-object v0, Landroid/hardware/camera2/CameraCharacteristics;->CONTROL_AF_AVAILABLE_MODES:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {p0, v0}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .line 21
    sget-object v1, Landroid/hardware/camera2/CameraCharacteristics;->INFO_SUPPORTED_HARDWARE_LEVEL:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {p0, v1}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 22
    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    const/4 v1, 0x1

    .line 23
    :goto_0
    sget-object v3, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v4, "Samsung"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    .line 24
    sget-object v6, Lcom/google/android/cameraview/e;->a:[I

    array-length v7, v6

    move v4, v2

    :goto_1
    if-ge v4, v7, :cond_3

    aget v3, v6, v4

    .line 27
    const/4 v8, 0x4

    if-ne v3, v8, :cond_2

    if-eqz v1, :cond_2

    if-eqz v5, :cond_2

    .line 24
    :cond_0
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    :cond_1
    move v1, v2

    .line 22
    goto :goto_0

    .line 30
    :cond_2
    invoke-static {v0, v3}, Lcom/google/android/cameraview/e;->a([II)Z

    move-result v8

    if-eqz v8, :cond_0

    move v2, v3

    .line 34
    :cond_3
    return v2
.end method

.method private static a([II)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 38
    if-nez p0, :cond_1

    .line 44
    :cond_0
    :goto_0
    return v0

    .line 39
    :cond_1
    array-length v2, p0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_0

    aget v3, p0, v1

    .line 40
    if-ne v3, p1, :cond_2

    .line 41
    const/4 v0, 0x1

    goto :goto_0

    .line 39
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method
