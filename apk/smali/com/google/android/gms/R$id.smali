.class public final Lcom/google/android/gms/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final adjust_height:I = 0x7f110089

.field public static final adjust_width:I = 0x7f11008a

.field public static final android_pay:I = 0x7f1100bb

.field public static final android_pay_dark:I = 0x7f1100b2

.field public static final android_pay_light:I = 0x7f1100b3

.field public static final android_pay_light_with_border:I = 0x7f1100b4

.field public static final auto:I = 0x7f11005c

.field public static final book_now:I = 0x7f1100ab

.field public static final button:I = 0x7f1100c0

.field public static final buyButton:I = 0x7f1100a8

.field public static final buy_now:I = 0x7f1100ac

.field public static final buy_with:I = 0x7f1100ad

.field public static final buy_with_google:I = 0x7f1100ae

.field public static final center:I = 0x7f110065

.field public static final classic:I = 0x7f1100b5

.field public static final dark:I = 0x7f110072

.field public static final date:I = 0x7f110144

.field public static final donate_with:I = 0x7f1100af

.field public static final donate_with_google:I = 0x7f1100b0

.field public static final google_wallet_classic:I = 0x7f1100b6

.field public static final google_wallet_grayscale:I = 0x7f1100b7

.field public static final google_wallet_monochrome:I = 0x7f1100b8

.field public static final grayscale:I = 0x7f1100b9

.field public static final holo_dark:I = 0x7f1100a2

.field public static final holo_light:I = 0x7f1100a3

.field public static final hybrid:I = 0x7f11008d

.field public static final icon_only:I = 0x7f11009b

.field public static final light:I = 0x7f110073

.field public static final logo_only:I = 0x7f1100b1

.field public static final match_parent:I = 0x7f1100aa

.field public static final monochrome:I = 0x7f1100ba

.field public static final none:I = 0x7f11003e

.field public static final normal:I = 0x7f110047

.field public static final production:I = 0x7f1100a4

.field public static final progressBar:I = 0x7f1101eb

.field public static final radio:I = 0x7f1100e4

.field public static final sandbox:I = 0x7f1100a5

.field public static final satellite:I = 0x7f11008e

.field public static final selectionDetails:I = 0x7f1100a9

.field public static final slide:I = 0x7f110071

.field public static final standard:I = 0x7f11009c

.field public static final strict_sandbox:I = 0x7f1100a6

.field public static final terrain:I = 0x7f11008f

.field public static final test:I = 0x7f1100a7

.field public static final text:I = 0x7f110023

.field public static final text2:I = 0x7f110024

.field public static final toolbar:I = 0x7f1100fe

.field public static final wide:I = 0x7f11009d

.field public static final wrap_content:I = 0x7f11005b


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
