.class public final Lcom/google/android/gms/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final common_google_signin_btn_text_dark:I = 0x7f0f0109

.field public static final common_google_signin_btn_text_dark_default:I = 0x7f0f004d

.field public static final common_google_signin_btn_text_dark_disabled:I = 0x7f0f004e

.field public static final common_google_signin_btn_text_dark_focused:I = 0x7f0f004f

.field public static final common_google_signin_btn_text_dark_pressed:I = 0x7f0f0050

.field public static final common_google_signin_btn_text_light:I = 0x7f0f010a

.field public static final common_google_signin_btn_text_light_default:I = 0x7f0f0051

.field public static final common_google_signin_btn_text_light_disabled:I = 0x7f0f0052

.field public static final common_google_signin_btn_text_light_focused:I = 0x7f0f0053

.field public static final common_google_signin_btn_text_light_pressed:I = 0x7f0f0054

.field public static final common_google_signin_btn_tint:I = 0x7f0f010b

.field public static final wallet_bright_foreground_disabled_holo_light:I = 0x7f0f00df

.field public static final wallet_bright_foreground_holo_dark:I = 0x7f0f00e0

.field public static final wallet_bright_foreground_holo_light:I = 0x7f0f00e1

.field public static final wallet_dim_foreground_disabled_holo_dark:I = 0x7f0f00e2

.field public static final wallet_dim_foreground_holo_dark:I = 0x7f0f00e3

.field public static final wallet_highlighted_text_holo_dark:I = 0x7f0f00e4

.field public static final wallet_highlighted_text_holo_light:I = 0x7f0f00e5

.field public static final wallet_hint_foreground_holo_dark:I = 0x7f0f00e6

.field public static final wallet_hint_foreground_holo_light:I = 0x7f0f00e7

.field public static final wallet_holo_blue_light:I = 0x7f0f00e8

.field public static final wallet_link_text_light:I = 0x7f0f00e9

.field public static final wallet_primary_text_holo_light:I = 0x7f0f0111

.field public static final wallet_secondary_text_holo_dark:I = 0x7f0f0112


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
