.class public final Lcom/google/android/gms/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final ambientEnabled:I = 0x7f0101b4

.field public static final appTheme:I = 0x7f010245

.field public static final buttonSize:I = 0x7f0101f2

.field public static final buyButtonAppearance:I = 0x7f01024c

.field public static final buyButtonHeight:I = 0x7f010249

.field public static final buyButtonText:I = 0x7f01024b

.field public static final buyButtonWidth:I = 0x7f01024a

.field public static final cameraBearing:I = 0x7f0101a5

.field public static final cameraMaxZoomPreference:I = 0x7f0101b6

.field public static final cameraMinZoomPreference:I = 0x7f0101b5

.field public static final cameraTargetLat:I = 0x7f0101a6

.field public static final cameraTargetLng:I = 0x7f0101a7

.field public static final cameraTilt:I = 0x7f0101a8

.field public static final cameraZoom:I = 0x7f0101a9

.field public static final circleCrop:I = 0x7f010197

.field public static final colorScheme:I = 0x7f0101f3

.field public static final environment:I = 0x7f010246

.field public static final fragmentMode:I = 0x7f010248

.field public static final fragmentStyle:I = 0x7f010247

.field public static final imageAspectRatio:I = 0x7f010196

.field public static final imageAspectRatioAdjust:I = 0x7f010195

.field public static final latLngBoundsNorthEastLatitude:I = 0x7f0101b9

.field public static final latLngBoundsNorthEastLongitude:I = 0x7f0101ba

.field public static final latLngBoundsSouthWestLatitude:I = 0x7f0101b7

.field public static final latLngBoundsSouthWestLongitude:I = 0x7f0101b8

.field public static final liteMode:I = 0x7f0101aa

.field public static final mapType:I = 0x7f0101a4

.field public static final maskedWalletDetailsBackground:I = 0x7f01024f

.field public static final maskedWalletDetailsButtonBackground:I = 0x7f010251

.field public static final maskedWalletDetailsButtonTextAppearance:I = 0x7f010250

.field public static final maskedWalletDetailsHeaderTextAppearance:I = 0x7f01024e

.field public static final maskedWalletDetailsLogoImageType:I = 0x7f010253

.field public static final maskedWalletDetailsLogoTextColor:I = 0x7f010252

.field public static final maskedWalletDetailsTextAppearance:I = 0x7f01024d

.field public static final scopeUris:I = 0x7f0101f4

.field public static final toolbarTextColorStyle:I = 0x7f010144

.field public static final uiCompass:I = 0x7f0101ab

.field public static final uiMapToolbar:I = 0x7f0101b3

.field public static final uiRotateGestures:I = 0x7f0101ac

.field public static final uiScrollGestures:I = 0x7f0101ad

.field public static final uiTiltGestures:I = 0x7f0101ae

.field public static final uiZoomControls:I = 0x7f0101af

.field public static final uiZoomGestures:I = 0x7f0101b0

.field public static final useViewLifecycle:I = 0x7f0101b1

.field public static final windowTransitionStyle:I = 0x7f010143

.field public static final zOrderOnTop:I = 0x7f0101b2


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
