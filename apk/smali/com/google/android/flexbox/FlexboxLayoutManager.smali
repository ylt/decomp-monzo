.class public Lcom/google/android/flexbox/FlexboxLayoutManager;
.super Landroid/support/v7/widget/RecyclerView$h;
.source "FlexboxLayoutManager.java"

# interfaces
.implements Landroid/support/v7/widget/RecyclerView$s$b;
.implements Lcom/google/android/flexbox/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/flexbox/FlexboxLayoutManager$d;,
        Lcom/google/android/flexbox/FlexboxLayoutManager$c;,
        Lcom/google/android/flexbox/FlexboxLayoutManager$a;,
        Lcom/google/android/flexbox/FlexboxLayoutManager$b;
    }
.end annotation


# static fields
.field static final synthetic a:Z

.field private static final b:Landroid/graphics/Rect;


# instance fields
.field private A:Lcom/google/android/flexbox/FlexboxLayoutManager$d;

.field private B:I

.field private C:I

.field private D:I

.field private E:I

.field private F:Z

.field private G:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final H:Landroid/content/Context;

.field private I:Landroid/view/View;

.field private J:I

.field private K:Lcom/google/android/flexbox/d$a;

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:Z

.field private h:Z

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/flexbox/c;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcom/google/android/flexbox/d;

.field private k:Landroid/support/v7/widget/RecyclerView$o;

.field private l:Landroid/support/v7/widget/RecyclerView$t;

.field private m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

.field private n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

.field private o:Landroid/support/v7/widget/ba;

.field private z:Landroid/support/v7/widget/ba;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/google/android/flexbox/FlexboxLayoutManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->a:Z

    .line 56
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->b:Landroid/graphics/Rect;

    return-void

    .line 46
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 199
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager;-><init>(Landroid/content/Context;II)V

    .line 200
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 208
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager;-><init>(Landroid/content/Context;II)V

    .line 209
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/high16 v2, -0x80000000

    .line 218
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$h;-><init>()V

    .line 101
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    .line 103
    new-instance v0, Lcom/google/android/flexbox/d;

    invoke-direct {v0, p0}, Lcom/google/android/flexbox/d;-><init>(Lcom/google/android/flexbox/a;)V

    iput-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    .line 123
    new-instance v0, Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;-><init>(Lcom/google/android/flexbox/FlexboxLayoutManager;Lcom/google/android/flexbox/FlexboxLayoutManager$1;)V

    iput-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    .line 145
    iput v3, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->B:I

    .line 150
    iput v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->C:I

    .line 155
    iput v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->D:I

    .line 160
    iput v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->E:I

    .line 174
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->G:Landroid/util/SparseArray;

    .line 187
    iput v3, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->J:I

    .line 193
    new-instance v0, Lcom/google/android/flexbox/d$a;

    invoke-direct {v0}, Lcom/google/android/flexbox/d$a;-><init>()V

    iput-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->K:Lcom/google/android/flexbox/d$a;

    .line 219
    invoke-virtual {p0, p2}, Lcom/google/android/flexbox/FlexboxLayoutManager;->f(I)V

    .line 220
    invoke-virtual {p0, p3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->m(I)V

    .line 221
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->n(I)V

    .line 222
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->c(Z)V

    .line 223
    iput-object p1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->H:Landroid/content/Context;

    .line 224
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    const/high16 v2, -0x80000000

    .line 241
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$h;-><init>()V

    .line 101
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    .line 103
    new-instance v0, Lcom/google/android/flexbox/d;

    invoke-direct {v0, p0}, Lcom/google/android/flexbox/d;-><init>(Lcom/google/android/flexbox/a;)V

    iput-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    .line 123
    new-instance v0, Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;-><init>(Lcom/google/android/flexbox/FlexboxLayoutManager;Lcom/google/android/flexbox/FlexboxLayoutManager$1;)V

    iput-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    .line 145
    iput v4, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->B:I

    .line 150
    iput v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->C:I

    .line 155
    iput v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->D:I

    .line 160
    iput v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->E:I

    .line 174
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->G:Landroid/util/SparseArray;

    .line 187
    iput v4, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->J:I

    .line 193
    new-instance v0, Lcom/google/android/flexbox/d$a;

    invoke-direct {v0}, Lcom/google/android/flexbox/d$a;-><init>()V

    iput-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->K:Lcom/google/android/flexbox/d$a;

    .line 242
    invoke-static {p1, p2, p3, p4}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/support/v7/widget/RecyclerView$h$b;

    move-result-object v0

    .line 243
    iget v1, v0, Landroid/support/v7/widget/RecyclerView$h$b;->a:I

    packed-switch v1, :pswitch_data_0

    .line 259
    :goto_0
    invoke-virtual {p0, v3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->m(I)V

    .line 260
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->n(I)V

    .line 261
    invoke-virtual {p0, v3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->c(Z)V

    .line 262
    iput-object p1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->H:Landroid/content/Context;

    .line 263
    return-void

    .line 245
    :pswitch_0
    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView$h$b;->c:Z

    if-eqz v0, :cond_0

    .line 246
    invoke-virtual {p0, v3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->f(I)V

    goto :goto_0

    .line 248
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->f(I)V

    goto :goto_0

    .line 252
    :pswitch_1
    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView$h$b;->c:Z

    if-eqz v0, :cond_1

    .line 253
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->f(I)V

    goto :goto_0

    .line 255
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->f(I)V

    goto :goto_0

    .line 243
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(ILandroid/support/v7/widget/RecyclerView$o;Landroid/support/v7/widget/RecyclerView$t;Z)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 775
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b()Z

    move-result v1

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->g:Z

    if-eqz v1, :cond_1

    .line 776
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v1}, Landroid/support/v7/widget/ba;->d()I

    move-result v1

    sub-int/2addr v1, p1

    .line 777
    if-lez v1, :cond_0

    .line 779
    neg-int v0, v1

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->c(ILandroid/support/v7/widget/RecyclerView$o;Landroid/support/v7/widget/RecyclerView$t;)I

    move-result v0

    .line 792
    :goto_0
    add-int v1, p1, v0

    .line 793
    if-eqz p4, :cond_0

    .line 795
    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v2}, Landroid/support/v7/widget/ba;->c()I

    move-result v2

    sub-int/2addr v1, v2

    .line 796
    if-lez v1, :cond_0

    .line 797
    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    neg-int v3, v1

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/ba;->a(I)V

    .line 798
    sub-int/2addr v0, v1

    .line 801
    :cond_0
    return v0

    .line 784
    :cond_1
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v1}, Landroid/support/v7/widget/ba;->c()I

    move-result v1

    sub-int v1, p1, v1

    .line 785
    if-lez v1, :cond_0

    .line 787
    invoke-direct {p0, v1, p2, p3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->c(ILandroid/support/v7/widget/RecyclerView$o;Landroid/support/v7/widget/RecyclerView$t;)I

    move-result v0

    neg-int v0, v0

    goto :goto_0
.end method

.method private a(Landroid/support/v7/widget/RecyclerView$o;Landroid/support/v7/widget/RecyclerView$t;Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I
    .locals 9

    .prologue
    const/high16 v8, -0x80000000

    .line 1259
    invoke-static {p3}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v0

    if-eq v0, v8, :cond_1

    .line 1260
    invoke-static {p3}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v0

    if-gez v0, :cond_0

    .line 1261
    invoke-static {p3}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v0

    invoke-static {p3}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v1

    add-int/2addr v0, v1

    invoke-static {p3, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->b(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 1263
    :cond_0
    invoke-direct {p0, p1, p3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(Landroid/support/v7/widget/RecyclerView$o;Lcom/google/android/flexbox/FlexboxLayoutManager$c;)V

    .line 1265
    :cond_1
    invoke-static {p3}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v3

    .line 1266
    invoke-static {p3}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v1

    .line 1267
    const/4 v0, 0x0

    .line 1268
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b()Z

    move-result v4

    move v2, v1

    move v1, v0

    .line 1269
    :goto_0
    if-gtz v2, :cond_2

    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->b(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    .line 1270
    invoke-static {p3, p2, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$c;Landroid/support/v7/widget/RecyclerView$t;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1271
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    invoke-static {p3}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->e(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v5

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/flexbox/c;

    .line 1272
    iget v5, v0, Lcom/google/android/flexbox/c;->o:I

    invoke-static {p3, v5}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 1273
    invoke-direct {p0, v0, p3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(Lcom/google/android/flexbox/c;Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v5

    add-int/2addr v1, v5

    .line 1275
    if-nez v4, :cond_3

    iget-boolean v5, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->g:Z

    if-eqz v5, :cond_3

    .line 1276
    invoke-static {p3}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/android/flexbox/c;->a()I

    move-result v6

    invoke-static {p3}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->f(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v7

    mul-int/2addr v6, v7

    sub-int/2addr v5, v6

    invoke-static {p3, v5}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 1281
    :goto_1
    invoke-virtual {v0}, Lcom/google/android/flexbox/c;->a()I

    move-result v0

    sub-int v0, v2, v0

    move v2, v0

    .line 1282
    goto :goto_0

    .line 1278
    :cond_3
    invoke-static {p3}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/android/flexbox/c;->a()I

    move-result v6

    invoke-static {p3}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->f(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v7

    mul-int/2addr v6, v7

    add-int/2addr v5, v6

    invoke-static {p3, v5}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    goto :goto_1

    .line 1283
    :cond_4
    invoke-static {p3}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v0

    sub-int/2addr v0, v1

    invoke-static {p3, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->e(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 1284
    invoke-static {p3}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v0

    if-eq v0, v8, :cond_6

    .line 1285
    invoke-static {p3}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v0

    add-int/2addr v0, v1

    invoke-static {p3, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->b(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 1286
    invoke-static {p3}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v0

    if-gez v0, :cond_5

    .line 1287
    invoke-static {p3}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v0

    invoke-static {p3}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v1

    add-int/2addr v0, v1

    invoke-static {p3, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->b(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 1289
    :cond_5
    invoke-direct {p0, p1, p3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(Landroid/support/v7/widget/RecyclerView$o;Lcom/google/android/flexbox/FlexboxLayoutManager$c;)V

    .line 1291
    :cond_6
    invoke-static {p3}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v0

    sub-int v0, v3, v0

    return v0
.end method

.method static synthetic a(Lcom/google/android/flexbox/FlexboxLayoutManager;)I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->d:I

    return v0
.end method

.method private a(Lcom/google/android/flexbox/c;Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I
    .locals 1

    .prologue
    .line 1420
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1421
    invoke-direct {p0, p1, p2}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b(Lcom/google/android/flexbox/c;Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v0

    .line 1423
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/flexbox/FlexboxLayoutManager;->c(Lcom/google/android/flexbox/c;Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v0

    goto :goto_0
.end method

.method private a(IIZ)Landroid/view/View;
    .locals 3

    .prologue
    .line 2475
    if-le p2, p1, :cond_0

    const/4 v0, 0x1

    .line 2476
    :goto_0
    if-eq p1, p2, :cond_2

    .line 2477
    invoke-virtual {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->i(I)Landroid/view/View;

    move-result-object v1

    .line 2478
    invoke-direct {p0, v1, p3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(Landroid/view/View;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    move-object v0, v1

    .line 2482
    :goto_1
    return-object v0

    .line 2475
    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    .line 2476
    :cond_1
    add-int/2addr p1, v0

    goto :goto_0

    .line 2482
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Landroid/view/View;Lcom/google/android/flexbox/c;)Landroid/view/View;
    .locals 7

    .prologue
    .line 2104
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b()Z

    move-result v3

    .line 2106
    const/4 v0, 0x1

    iget v4, p2, Lcom/google/android/flexbox/c;->h:I

    move v2, v0

    move-object v0, p1

    .line 2107
    :goto_0
    if-ge v2, v4, :cond_3

    .line 2108
    invoke-virtual {p0, v2}, Lcom/google/android/flexbox/FlexboxLayoutManager;->i(I)Landroid/view/View;

    move-result-object v1

    .line 2109
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v5

    const/16 v6, 0x8

    if-ne v5, v6, :cond_1

    .line 2107
    :cond_0
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2112
    :cond_1
    iget-boolean v5, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->g:Z

    if-eqz v5, :cond_2

    if-nez v3, :cond_2

    .line 2113
    iget-object v5, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v5, v0}, Landroid/support/v7/widget/ba;->b(Landroid/view/View;)I

    move-result v5

    iget-object v6, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 2114
    invoke-virtual {v6, v1}, Landroid/support/v7/widget/ba;->b(Landroid/view/View;)I

    move-result v6

    if-ge v5, v6, :cond_0

    move-object v0, v1

    .line 2115
    goto :goto_1

    .line 2118
    :cond_2
    iget-object v5, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v5, v0}, Landroid/support/v7/widget/ba;->a(Landroid/view/View;)I

    move-result v5

    iget-object v6, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 2119
    invoke-virtual {v6, v1}, Landroid/support/v7/widget/ba;->a(Landroid/view/View;)I

    move-result v6

    if-le v5, v6, :cond_0

    move-object v0, v1

    .line 2120
    goto :goto_1

    .line 2124
    :cond_3
    return-object v0
.end method

.method private a(II)V
    .locals 11

    .prologue
    const/4 v5, 0x1

    const/4 v10, -0x1

    const/4 v1, 0x0

    .line 1990
    sget-boolean v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v0, v0, Lcom/google/android/flexbox/d;->a:[I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1991
    :cond_0
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->g(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 1992
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b()Z

    move-result v6

    .line 1995
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->C()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->A()I

    move-result v2

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 1997
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->D()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->B()I

    move-result v3

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 1998
    if-nez v6, :cond_3

    iget-boolean v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->g:Z

    if-eqz v0, :cond_3

    move v4, v5

    .line 1999
    :goto_0
    if-ne p1, v5, :cond_8

    .line 2000
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->z()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->i(I)Landroid/view/View;

    move-result-object v7

    .line 2001
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    iget-object v8, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v8, v7}, Landroid/support/v7/widget/ba;->b(Landroid/view/View;)I

    move-result v8

    invoke-static {v0, v8}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 2002
    invoke-virtual {p0, v7}, Lcom/google/android/flexbox/FlexboxLayoutManager;->d(Landroid/view/View;)I

    move-result v8

    .line 2003
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v0, v0, Lcom/google/android/flexbox/d;->a:[I

    aget v0, v0, v8

    .line 2004
    iget-object v9, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/flexbox/c;

    .line 2008
    invoke-direct {p0, v7, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b(Landroid/view/View;Lcom/google/android/flexbox/c;)Landroid/view/View;

    move-result-object v0

    .line 2009
    iget-object v7, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v7, v5}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->f(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 2010
    iget-object v5, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    iget-object v7, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v7}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->k(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v7

    add-int/2addr v7, v8

    invoke-static {v5, v7}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 2011
    iget-object v5, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v5, v5, Lcom/google/android/flexbox/d;->a:[I

    array-length v5, v5

    iget-object v7, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v7}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->h(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v7

    if-gt v5, v7, :cond_4

    .line 2012
    iget-object v5, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v5, v10}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 2018
    :goto_1
    if-eqz v4, :cond_6

    .line 2019
    iget-object v4, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    iget-object v5, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v5, v0}, Landroid/support/v7/widget/ba;->a(Landroid/view/View;)I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 2020
    iget-object v4, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    iget-object v5, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v5, v0}, Landroid/support/v7/widget/ba;->a(Landroid/view/View;)I

    move-result v0

    neg-int v0, v0

    iget-object v5, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 2021
    invoke-virtual {v5}, Landroid/support/v7/widget/ba;->c()I

    move-result v5

    add-int/2addr v0, v5

    .line 2020
    invoke-static {v4, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->b(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 2022
    iget-object v4, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v0

    if-ltz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    .line 2023
    invoke-static {v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v0

    .line 2022
    :goto_2
    invoke-static {v4, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->b(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 2030
    :goto_3
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->e(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v0

    if-eq v0, v10, :cond_1

    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    .line 2031
    invoke-static {v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->e(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-le v0, v1, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    .line 2032
    invoke-static {v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->h(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->getFlexItemCount()I

    move-result v1

    if-gt v0, v1, :cond_2

    .line 2036
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v0

    sub-int v4, p2, v0

    .line 2037
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->K:Lcom/google/android/flexbox/d$a;

    invoke-virtual {v0}, Lcom/google/android/flexbox/d$a;->a()V

    .line 2038
    if-lez v4, :cond_2

    .line 2039
    if-eqz v6, :cond_7

    .line 2040
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->K:Lcom/google/android/flexbox/d$a;

    iget-object v5, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    .line 2042
    invoke-static {v5}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->h(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v5

    iget-object v6, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    .line 2040
    invoke-virtual/range {v0 .. v6}, Lcom/google/android/flexbox/d;->a(Lcom/google/android/flexbox/d$a;IIIILjava/util/List;)V

    .line 2048
    :goto_4
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    .line 2049
    invoke-static {v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->h(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v1

    .line 2048
    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/flexbox/d;->a(III)V

    .line 2050
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->h(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/flexbox/d;->a(I)V

    .line 2092
    :cond_2
    :goto_5
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v1

    sub-int v1, p2, v1

    invoke-static {v0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->e(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 2093
    return-void

    :cond_3
    move v4, v1

    .line 1998
    goto/16 :goto_0

    .line 2014
    :cond_4
    iget-object v5, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    iget-object v7, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v7, v7, Lcom/google/android/flexbox/d;->a:[I

    iget-object v8, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    .line 2015
    invoke-static {v8}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->h(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v8

    aget v7, v7, v8

    .line 2014
    invoke-static {v5, v7}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    goto/16 :goto_1

    :cond_5
    move v0, v1

    .line 2023
    goto/16 :goto_2

    .line 2025
    :cond_6
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    iget-object v4, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v4, v0}, Landroid/support/v7/widget/ba;->b(Landroid/view/View;)I

    move-result v4

    invoke-static {v1, v4}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 2026
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    iget-object v4, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v4, v0}, Landroid/support/v7/widget/ba;->b(Landroid/view/View;)I

    move-result v0

    iget-object v4, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 2027
    invoke-virtual {v4}, Landroid/support/v7/widget/ba;->d()I

    move-result v4

    sub-int/2addr v0, v4

    .line 2026
    invoke-static {v1, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->b(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    goto/16 :goto_3

    .line 2044
    :cond_7
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->K:Lcom/google/android/flexbox/d$a;

    iget-object v5, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    .line 2046
    invoke-static {v5}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->h(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v5

    iget-object v6, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    .line 2044
    invoke-virtual/range {v0 .. v6}, Lcom/google/android/flexbox/d;->c(Lcom/google/android/flexbox/d$a;IIIILjava/util/List;)V

    goto :goto_4

    .line 2054
    :cond_8
    invoke-virtual {p0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->i(I)Landroid/view/View;

    move-result-object v2

    .line 2056
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    iget-object v3, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v3, v2}, Landroid/support/v7/widget/ba;->a(Landroid/view/View;)I

    move-result v3

    invoke-static {v0, v3}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 2057
    invoke-virtual {p0, v2}, Lcom/google/android/flexbox/FlexboxLayoutManager;->d(Landroid/view/View;)I

    move-result v3

    .line 2058
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v0, v0, Lcom/google/android/flexbox/d;->a:[I

    aget v0, v0, v3

    .line 2059
    iget-object v6, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/flexbox/c;

    .line 2063
    invoke-direct {p0, v2, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(Landroid/view/View;Lcom/google/android/flexbox/c;)Landroid/view/View;

    move-result-object v6

    .line 2065
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v0, v5}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->f(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 2066
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v0, v0, Lcom/google/android/flexbox/d;->a:[I

    aget v0, v0, v3

    .line 2067
    if-ne v0, v10, :cond_d

    move v2, v1

    .line 2070
    :goto_6
    if-lez v2, :cond_a

    .line 2071
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    add-int/lit8 v5, v2, -0x1

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/flexbox/c;

    .line 2074
    iget-object v5, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-virtual {v0}, Lcom/google/android/flexbox/c;->b()I

    move-result v0

    sub-int v0, v3, v0

    invoke-static {v5, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 2078
    :goto_7
    iget-object v3, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    if-lez v2, :cond_b

    add-int/lit8 v0, v2, -0x1

    :goto_8
    invoke-static {v3, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 2080
    if-eqz v4, :cond_c

    .line 2081
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v2, v6}, Landroid/support/v7/widget/ba;->b(Landroid/view/View;)I

    move-result v2

    invoke-static {v0, v2}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 2082
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v2, v6}, Landroid/support/v7/widget/ba;->b(Landroid/view/View;)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 2083
    invoke-virtual {v3}, Landroid/support/v7/widget/ba;->d()I

    move-result v3

    sub-int/2addr v2, v3

    .line 2082
    invoke-static {v0, v2}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->b(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 2084
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v2}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v2

    if-ltz v2, :cond_9

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    .line 2085
    invoke-static {v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v1

    .line 2084
    :cond_9
    invoke-static {v0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->b(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    goto/16 :goto_5

    .line 2076
    :cond_a
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v0, v10}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    goto :goto_7

    :cond_b
    move v0, v1

    .line 2078
    goto :goto_8

    .line 2087
    :cond_c
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v1, v6}, Landroid/support/v7/widget/ba;->a(Landroid/view/View;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 2088
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v1, v6}, Landroid/support/v7/widget/ba;->a(Landroid/view/View;)I

    move-result v1

    neg-int v1, v1

    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 2089
    invoke-virtual {v2}, Landroid/support/v7/widget/ba;->c()I

    move-result v2

    add-int/2addr v1, v2

    .line 2088
    invoke-static {v0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->b(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    goto/16 :goto_5

    :cond_d
    move v2, v0

    goto :goto_6
.end method

.method private a(Landroid/support/v7/widget/RecyclerView$o;II)V
    .locals 0

    .prologue
    .line 1414
    :goto_0
    if-lt p3, p2, :cond_0

    .line 1415
    invoke-virtual {p0, p3, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(ILandroid/support/v7/widget/RecyclerView$o;)V

    .line 1414
    add-int/lit8 p3, p3, -0x1

    goto :goto_0

    .line 1417
    :cond_0
    return-void
.end method

.method private a(Landroid/support/v7/widget/RecyclerView$o;Lcom/google/android/flexbox/FlexboxLayoutManager$c;)V
    .locals 2

    .prologue
    .line 1295
    invoke-static {p2}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->g(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1304
    :goto_0
    return-void

    .line 1298
    :cond_0
    invoke-static {p2}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->f(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 1300
    invoke-direct {p0, p1, p2}, Lcom/google/android/flexbox/FlexboxLayoutManager;->c(Landroid/support/v7/widget/RecyclerView$o;Lcom/google/android/flexbox/FlexboxLayoutManager$c;)V

    goto :goto_0

    .line 1302
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b(Landroid/support/v7/widget/RecyclerView$o;Lcom/google/android/flexbox/FlexboxLayoutManager$c;)V

    goto :goto_0
.end method

.method private a(Landroid/support/v7/widget/RecyclerView$t;Lcom/google/android/flexbox/FlexboxLayoutManager$a;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1026
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->A:Lcom/google/android/flexbox/FlexboxLayoutManager$d;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(Landroid/support/v7/widget/RecyclerView$t;Lcom/google/android/flexbox/FlexboxLayoutManager$a;Lcom/google/android/flexbox/FlexboxLayoutManager$d;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1046
    :cond_0
    :goto_0
    return-void

    .line 1032
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b(Landroid/support/v7/widget/RecyclerView$t;Lcom/google/android/flexbox/FlexboxLayoutManager$a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1043
    invoke-static {p2}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->f(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)V

    .line 1044
    invoke-static {p2, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->b(Lcom/google/android/flexbox/FlexboxLayoutManager$a;I)I

    .line 1045
    invoke-static {p2, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$a;I)I

    goto :goto_0
.end method

.method private a(Lcom/google/android/flexbox/FlexboxLayoutManager$a;ZZ)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1679
    if-eqz p3, :cond_1

    .line 1680
    invoke-direct {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->k()V

    .line 1684
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->g:Z

    if-eqz v0, :cond_2

    .line 1685
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {p1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->h(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->e(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 1690
    :goto_1
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {p1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 1691
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v0, v3}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->f(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 1692
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v0, v3}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->g(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 1693
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {p1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->h(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 1694
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    const/high16 v1, -0x80000000

    invoke-static {v0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->b(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 1695
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {p1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->e(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 1697
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    .line 1698
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v3, :cond_0

    .line 1699
    invoke-static {p1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->e(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v0

    if-ltz v0, :cond_0

    .line 1700
    invoke-static {p1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->e(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 1701
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    invoke-static {p1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->e(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/flexbox/c;

    .line 1702
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->i(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    .line 1703
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->h(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/flexbox/c;->b()I

    move-result v0

    add-int/2addr v0, v2

    invoke-static {v1, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 1705
    :cond_0
    return-void

    .line 1682
    :cond_1
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->b(Lcom/google/android/flexbox/FlexboxLayoutManager$c;Z)Z

    goto/16 :goto_0

    .line 1687
    :cond_2
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 1688
    invoke-virtual {v1}, Landroid/support/v7/widget/ba;->d()I

    move-result v1

    invoke-static {p1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->h(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v2

    sub-int/2addr v1, v2

    .line 1687
    invoke-static {v0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->e(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    goto/16 :goto_1
.end method

.method private a(Landroid/support/v7/widget/RecyclerView$t;Lcom/google/android/flexbox/FlexboxLayoutManager$a;Lcom/google/android/flexbox/FlexboxLayoutManager$d;)Z
    .locals 6

    .prologue
    const/high16 v5, -0x80000000

    const/4 v4, -0x1

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1050
    sget-boolean v2, Lcom/google/android/flexbox/FlexboxLayoutManager;->a:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v2, v2, Lcom/google/android/flexbox/d;->a:[I

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1051
    :cond_0
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$t;->a()Z

    move-result v2

    if-nez v2, :cond_1

    iget v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->B:I

    if-ne v2, v4, :cond_2

    :cond_1
    move v1, v0

    .line 1118
    :goto_0
    return v1

    .line 1054
    :cond_2
    iget v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->B:I

    if-ltz v2, :cond_3

    iget v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->B:I

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$t;->e()I

    move-result v3

    if-lt v2, v3, :cond_4

    .line 1055
    :cond_3
    iput v4, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->B:I

    .line 1056
    iput v5, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->C:I

    move v1, v0

    .line 1060
    goto :goto_0

    .line 1063
    :cond_4
    iget v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->B:I

    invoke-static {p2, v2}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->b(Lcom/google/android/flexbox/FlexboxLayoutManager$a;I)I

    .line 1064
    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v2, v2, Lcom/google/android/flexbox/d;->a:[I

    invoke-static {p2}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v3

    aget v2, v2, v3

    invoke-static {p2, v2}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$a;I)I

    .line 1065
    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->A:Lcom/google/android/flexbox/FlexboxLayoutManager$d;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->A:Lcom/google/android/flexbox/FlexboxLayoutManager$d;

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$t;->e()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/android/flexbox/FlexboxLayoutManager$d;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$d;I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1066
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v0}, Landroid/support/v7/widget/ba;->c()I

    move-result v0

    .line 1067
    invoke-static {p3}, Lcom/google/android/flexbox/FlexboxLayoutManager$d;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$d;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1066
    invoke-static {p2, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$a;I)I

    .line 1068
    invoke-static {p2, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->b(Lcom/google/android/flexbox/FlexboxLayoutManager$a;Z)Z

    .line 1069
    invoke-static {p2, v4}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$a;I)I

    goto :goto_0

    .line 1073
    :cond_5
    iget v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->C:I

    if-ne v2, v5, :cond_d

    .line 1074
    iget v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->B:I

    invoke-virtual {p0, v2}, Lcom/google/android/flexbox/FlexboxLayoutManager;->c(I)Landroid/view/View;

    move-result-object v2

    .line 1075
    if-eqz v2, :cond_a

    .line 1076
    iget-object v3, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v3, v2}, Landroid/support/v7/widget/ba;->e(Landroid/view/View;)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 1077
    invoke-virtual {v4}, Landroid/support/v7/widget/ba;->f()I

    move-result v4

    if-le v3, v4, :cond_6

    .line 1078
    invoke-static {p2}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->f(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)V

    goto :goto_0

    .line 1081
    :cond_6
    iget-object v3, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v3, v2}, Landroid/support/v7/widget/ba;->a(Landroid/view/View;)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 1082
    invoke-virtual {v4}, Landroid/support/v7/widget/ba;->c()I

    move-result v4

    sub-int/2addr v3, v4

    .line 1083
    if-gez v3, :cond_7

    .line 1084
    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v2}, Landroid/support/v7/widget/ba;->c()I

    move-result v2

    invoke-static {p2, v2}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$a;I)I

    .line 1085
    invoke-static {p2, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$a;Z)Z

    goto/16 :goto_0

    .line 1089
    :cond_7
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v0}, Landroid/support/v7/widget/ba;->d()I

    move-result v0

    iget-object v3, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 1090
    invoke-virtual {v3, v2}, Landroid/support/v7/widget/ba;->b(Landroid/view/View;)I

    move-result v3

    sub-int/2addr v0, v3

    .line 1091
    if-gez v0, :cond_8

    .line 1092
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v0}, Landroid/support/v7/widget/ba;->d()I

    move-result v0

    invoke-static {p2, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$a;I)I

    .line 1093
    invoke-static {p2, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$a;Z)Z

    goto/16 :goto_0

    .line 1096
    :cond_8
    invoke-static {p2}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 1097
    invoke-virtual {v0, v2}, Landroid/support/v7/widget/ba;->b(Landroid/view/View;)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 1098
    invoke-virtual {v2}, Landroid/support/v7/widget/ba;->b()I

    move-result v2

    add-int/2addr v0, v2

    .line 1096
    :goto_1
    invoke-static {p2, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$a;I)I

    goto/16 :goto_0

    .line 1098
    :cond_9
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 1099
    invoke-virtual {v0, v2}, Landroid/support/v7/widget/ba;->a(Landroid/view/View;)I

    move-result v0

    goto :goto_1

    .line 1101
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->z()I

    move-result v2

    if-lez v2, :cond_c

    .line 1102
    invoke-virtual {p0, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->i(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/flexbox/FlexboxLayoutManager;->d(Landroid/view/View;)I

    move-result v2

    .line 1103
    iget v3, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->B:I

    if-ge v3, v2, :cond_b

    move v0, v1

    :cond_b
    invoke-static {p2, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$a;Z)Z

    .line 1105
    :cond_c
    invoke-static {p2}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->f(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)V

    goto/16 :goto_0

    .line 1111
    :cond_d
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b()Z

    move-result v0

    if-nez v0, :cond_e

    iget-boolean v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->g:Z

    if-eqz v0, :cond_e

    .line 1112
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->C:I

    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 1113
    invoke-virtual {v2}, Landroid/support/v7/widget/ba;->g()I

    move-result v2

    sub-int/2addr v0, v2

    .line 1112
    invoke-static {p2, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$a;I)I

    goto/16 :goto_0

    .line 1115
    :cond_e
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v0}, Landroid/support/v7/widget/ba;->c()I

    move-result v0

    iget v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->C:I

    add-int/2addr v0, v2

    invoke-static {p2, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$a;I)I

    goto/16 :goto_0
.end method

.method private a(Landroid/view/View;Z)Z
    .locals 11

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2361
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->getPaddingLeft()I

    move-result v2

    .line 2362
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->getPaddingTop()I

    move-result v6

    .line 2363
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->C()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    .line 2364
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->D()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->getPaddingBottom()I

    move-result v5

    sub-int v7, v4, v5

    .line 2365
    invoke-direct {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->p(Landroid/view/View;)I

    move-result v4

    .line 2366
    invoke-direct {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->r(Landroid/view/View;)I

    move-result v8

    .line 2367
    invoke-direct {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->q(Landroid/view/View;)I

    move-result v9

    .line 2368
    invoke-direct {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->s(Landroid/view/View;)I

    move-result v10

    .line 2374
    if-gt v2, v4, :cond_9

    if-lt v3, v9, :cond_9

    move v5, v0

    .line 2377
    :goto_0
    if-ge v4, v3, :cond_0

    if-lt v9, v2, :cond_8

    :cond_0
    move v4, v0

    .line 2381
    :goto_1
    if-gt v6, v8, :cond_7

    if-lt v7, v10, :cond_7

    move v3, v0

    .line 2384
    :goto_2
    if-ge v8, v7, :cond_1

    if-lt v10, v6, :cond_6

    :cond_1
    move v2, v0

    .line 2387
    :goto_3
    if-eqz p2, :cond_4

    .line 2388
    if-eqz v5, :cond_3

    if-eqz v3, :cond_3

    .line 2390
    :cond_2
    :goto_4
    return v0

    :cond_3
    move v0, v1

    .line 2388
    goto :goto_4

    .line 2390
    :cond_4
    if-eqz v4, :cond_5

    if-nez v2, :cond_2

    :cond_5
    move v0, v1

    goto :goto_4

    :cond_6
    move v2, v1

    goto :goto_3

    :cond_7
    move v3, v1

    goto :goto_2

    :cond_8
    move v4, v1

    goto :goto_1

    :cond_9
    move v5, v1

    goto :goto_0
.end method

.method private b(ILandroid/support/v7/widget/RecyclerView$o;Landroid/support/v7/widget/RecyclerView$t;Z)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 813
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b()Z

    move-result v1

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->g:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    .line 815
    :goto_0
    if-eqz v1, :cond_2

    .line 816
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v1}, Landroid/support/v7/widget/ba;->c()I

    move-result v1

    sub-int v1, p1, v1

    .line 817
    if-lez v1, :cond_0

    .line 818
    invoke-direct {p0, v1, p2, p3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->c(ILandroid/support/v7/widget/RecyclerView$o;Landroid/support/v7/widget/RecyclerView$t;)I

    move-result v0

    .line 832
    :goto_1
    add-int v1, p1, v0

    .line 833
    if-eqz p4, :cond_0

    .line 835
    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v2}, Landroid/support/v7/widget/ba;->d()I

    move-result v2

    sub-int v1, v2, v1

    .line 836
    if-lez v1, :cond_0

    .line 837
    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/ba;->a(I)V

    .line 838
    add-int/2addr v0, v1

    .line 841
    :cond_0
    return v0

    :cond_1
    move v1, v0

    .line 813
    goto :goto_0

    .line 823
    :cond_2
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v1}, Landroid/support/v7/widget/ba;->d()I

    move-result v1

    sub-int/2addr v1, p1

    .line 824
    if-lez v1, :cond_0

    .line 825
    neg-int v0, v1

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->c(ILandroid/support/v7/widget/RecyclerView$o;Landroid/support/v7/widget/RecyclerView$t;)I

    move-result v0

    neg-int v0, v0

    goto :goto_1
.end method

.method private b(Landroid/support/v7/widget/RecyclerView$t;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2180
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->z()I

    move-result v1

    if-nez v1, :cond_1

    .line 2193
    :cond_0
    :goto_0
    return v0

    .line 2183
    :cond_1
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$t;->e()I

    move-result v1

    .line 2184
    invoke-direct {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->l()V

    .line 2185
    invoke-direct {p0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->q(I)Landroid/view/View;

    move-result-object v2

    .line 2186
    invoke-direct {p0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->r(I)Landroid/view/View;

    move-result-object v1

    .line 2187
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$t;->e()I

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 2191
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ba;->b(Landroid/view/View;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 2192
    invoke-virtual {v1, v2}, Landroid/support/v7/widget/ba;->a(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    .line 2193
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v1}, Landroid/support/v7/widget/ba;->f()I

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/flexbox/FlexboxLayoutManager;)I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->c:I

    return v0
.end method

.method private b(Lcom/google/android/flexbox/c;Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I
    .locals 18

    .prologue
    .line 1428
    sget-boolean v2, Lcom/google/android/flexbox/FlexboxLayoutManager;->a:Z

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v2, v2, Lcom/google/android/flexbox/d;->b:[J

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 1430
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->getPaddingLeft()I

    move-result v3

    .line 1431
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->getPaddingRight()I

    move-result v5

    .line 1432
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->C()I

    move-result v6

    .line 1434
    invoke-static/range {p2 .. p2}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v2

    .line 1435
    invoke-static/range {p2 .. p2}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->f(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v4

    const/4 v7, -0x1

    if-ne v4, v7, :cond_8

    .line 1436
    move-object/from16 v0, p1

    iget v4, v0, Lcom/google/android/flexbox/c;->g:I

    sub-int/2addr v2, v4

    move v10, v2

    .line 1438
    :goto_0
    invoke-static/range {p2 .. p2}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->h(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v13

    .line 1444
    const/4 v2, 0x0

    .line 1445
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->e:I

    packed-switch v4, :pswitch_data_0

    .line 1473
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid justifyContent is set: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->e:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1447
    :pswitch_0
    int-to-float v4, v3

    .line 1448
    sub-int v3, v6, v5

    int-to-float v3, v3

    .line 1476
    :goto_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    invoke-static {v5}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->g(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v5

    int-to-float v5, v5

    sub-float v5, v4, v5

    .line 1477
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    invoke-static {v4}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->g(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v4

    int-to-float v4, v4

    sub-float v4, v3, v4

    .line 1478
    const/4 v3, 0x0

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v14

    .line 1482
    const/4 v2, 0x0

    .line 1483
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/flexbox/c;->b()I

    move-result v15

    move v12, v13

    .line 1484
    :goto_2
    add-int v3, v13, v15

    if-ge v12, v3, :cond_7

    .line 1485
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(I)Landroid/view/View;

    move-result-object v3

    .line 1486
    if-nez v3, :cond_3

    move v3, v4

    move v4, v5

    .line 1484
    :goto_3
    add-int/lit8 v5, v12, 0x1

    move v12, v5

    move v5, v4

    move v4, v3

    goto :goto_2

    .line 1451
    :pswitch_1
    move-object/from16 v0, p1

    iget v4, v0, Lcom/google/android/flexbox/c;->e:I

    sub-int v4, v6, v4

    add-int/2addr v4, v5

    int-to-float v4, v4

    .line 1452
    move-object/from16 v0, p1

    iget v5, v0, Lcom/google/android/flexbox/c;->e:I

    sub-int v3, v5, v3

    int-to-float v3, v3

    .line 1453
    goto :goto_1

    .line 1455
    :pswitch_2
    int-to-float v3, v3

    move-object/from16 v0, p1

    iget v4, v0, Lcom/google/android/flexbox/c;->e:I

    sub-int v4, v6, v4

    int-to-float v4, v4

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v4, v7

    add-float/2addr v4, v3

    .line 1456
    sub-int v3, v6, v5

    int-to-float v3, v3

    move-object/from16 v0, p1

    iget v5, v0, Lcom/google/android/flexbox/c;->e:I

    sub-int v5, v6, v5

    int-to-float v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    sub-float/2addr v3, v5

    .line 1457
    goto :goto_1

    .line 1459
    :pswitch_3
    move-object/from16 v0, p1

    iget v4, v0, Lcom/google/android/flexbox/c;->h:I

    if-eqz v4, :cond_1

    .line 1460
    move-object/from16 v0, p1

    iget v2, v0, Lcom/google/android/flexbox/c;->e:I

    sub-int v2, v6, v2

    int-to-float v2, v2

    move-object/from16 v0, p1

    iget v4, v0, Lcom/google/android/flexbox/c;->h:I

    int-to-float v4, v4

    div-float/2addr v2, v4

    .line 1463
    :cond_1
    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v2, v4

    add-float/2addr v4, v3

    .line 1464
    sub-int v3, v6, v5

    int-to-float v3, v3

    const/high16 v5, 0x40000000    # 2.0f

    div-float v5, v2, v5

    sub-float/2addr v3, v5

    .line 1465
    goto/16 :goto_1

    .line 1467
    :pswitch_4
    int-to-float v4, v3

    .line 1468
    move-object/from16 v0, p1

    iget v2, v0, Lcom/google/android/flexbox/c;->h:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_2

    move-object/from16 v0, p1

    iget v2, v0, Lcom/google/android/flexbox/c;->h:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    .line 1469
    :goto_4
    move-object/from16 v0, p1

    iget v3, v0, Lcom/google/android/flexbox/c;->e:I

    sub-int v3, v6, v3

    int-to-float v3, v3

    div-float v2, v3, v2

    .line 1470
    sub-int v3, v6, v5

    int-to-float v3, v3

    .line 1471
    goto/16 :goto_1

    .line 1468
    :cond_2
    const/high16 v2, 0x3f800000    # 1.0f

    goto :goto_4

    .line 1490
    :cond_3
    invoke-static/range {p2 .. p2}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->f(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_5

    .line 1491
    sget-object v6, Lcom/google/android/flexbox/FlexboxLayoutManager;->b:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v6}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1492
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b(Landroid/view/View;)V

    move v11, v2

    .line 1502
    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v2, v2, Lcom/google/android/flexbox/d;->b:[J

    aget-wide v6, v2, v12

    .line 1503
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    invoke-virtual {v2, v6, v7}, Lcom/google/android/flexbox/d;->a(J)I

    move-result v8

    .line 1504
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    invoke-virtual {v2, v6, v7}, Lcom/google/android/flexbox/d;->b(J)I

    move-result v6

    .line 1505
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Lcom/google/android/flexbox/FlexboxLayoutManager$b;

    .line 1506
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v8, v6, v9}, Lcom/google/android/flexbox/FlexboxLayoutManager;->c(Landroid/view/View;IILandroid/support/v7/widget/RecyclerView$i;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1507
    invoke-virtual {v3, v8, v6}, Landroid/view/View;->measure(II)V

    .line 1510
    :cond_4
    iget v2, v9, Lcom/google/android/flexbox/FlexboxLayoutManager$b;->leftMargin:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->n(Landroid/view/View;)I

    move-result v6

    add-int/2addr v2, v6

    int-to-float v2, v2

    add-float v16, v5, v2

    .line 1511
    iget v2, v9, Lcom/google/android/flexbox/FlexboxLayoutManager$b;->rightMargin:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->o(Landroid/view/View;)I

    move-result v5

    add-int/2addr v2, v5

    int-to-float v2, v2

    sub-float v17, v4, v2

    .line 1513
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->l(Landroid/view/View;)I

    move-result v2

    add-int v6, v10, v2

    .line 1514
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->g:Z

    if-eqz v2, :cond_6

    .line 1515
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    .line 1516
    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    sub-int v5, v4, v5

    .line 1517
    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 1518
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int v8, v6, v4

    move-object/from16 v4, p1

    .line 1515
    invoke-virtual/range {v2 .. v8}, Lcom/google/android/flexbox/d;->a(Landroid/view/View;Lcom/google/android/flexbox/c;IIII)V

    .line 1525
    :goto_6
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    iget v4, v9, Lcom/google/android/flexbox/FlexboxLayoutManager$b;->rightMargin:I

    add-int/2addr v2, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->o(Landroid/view/View;)I

    move-result v4

    add-int/2addr v2, v4

    int-to-float v2, v2

    add-float/2addr v2, v14

    add-float v4, v16, v2

    .line 1527
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    iget v5, v9, Lcom/google/android/flexbox/FlexboxLayoutManager$b;->leftMargin:I

    add-int/2addr v2, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->n(Landroid/view/View;)I

    move-result v3

    add-int/2addr v2, v3

    int-to-float v2, v2

    add-float/2addr v2, v14

    sub-float v2, v17, v2

    move v3, v2

    move v2, v11

    goto/16 :goto_3

    .line 1494
    :cond_5
    sget-object v6, Lcom/google/android/flexbox/FlexboxLayoutManager;->b:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v6}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1495
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b(Landroid/view/View;I)V

    .line 1496
    add-int/lit8 v2, v2, 0x1

    move v11, v2

    goto/16 :goto_5

    .line 1520
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    .line 1521
    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 1522
    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v7, v4

    .line 1523
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int v8, v6, v4

    move-object/from16 v4, p1

    .line 1520
    invoke-virtual/range {v2 .. v8}, Lcom/google/android/flexbox/d;->a(Landroid/view/View;Lcom/google/android/flexbox/c;IIII)V

    goto :goto_6

    .line 1530
    :cond_7
    invoke-static/range {p2 .. p2}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->e(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v3}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->f(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v3

    add-int/2addr v2, v3

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 1531
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/flexbox/c;->a()I

    move-result v2

    return v2

    :cond_8
    move v10, v2

    goto/16 :goto_0

    .line 1445
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method private b(Landroid/view/View;Lcom/google/android/flexbox/c;)Landroid/view/View;
    .locals 7

    .prologue
    .line 2136
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b()Z

    move-result v3

    .line 2138
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->z()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->z()I

    move-result v1

    iget v2, p2, Lcom/google/android/flexbox/c;->h:I

    sub-int/2addr v1, v2

    add-int/lit8 v4, v1, -0x1

    move v2, v0

    move-object v0, p1

    .line 2139
    :goto_0
    if-le v2, v4, :cond_3

    .line 2140
    invoke-virtual {p0, v2}, Lcom/google/android/flexbox/FlexboxLayoutManager;->i(I)Landroid/view/View;

    move-result-object v1

    .line 2141
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v5

    const/16 v6, 0x8

    if-ne v5, v6, :cond_1

    .line 2139
    :cond_0
    :goto_1
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_0

    .line 2144
    :cond_1
    iget-boolean v5, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->g:Z

    if-eqz v5, :cond_2

    if-nez v3, :cond_2

    .line 2147
    iget-object v5, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v5, v0}, Landroid/support/v7/widget/ba;->a(Landroid/view/View;)I

    move-result v5

    iget-object v6, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 2148
    invoke-virtual {v6, v1}, Landroid/support/v7/widget/ba;->a(Landroid/view/View;)I

    move-result v6

    if-le v5, v6, :cond_0

    move-object v0, v1

    .line 2149
    goto :goto_1

    .line 2152
    :cond_2
    iget-object v5, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v5, v0}, Landroid/support/v7/widget/ba;->b(Landroid/view/View;)I

    move-result v5

    iget-object v6, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 2153
    invoke-virtual {v6, v1}, Landroid/support/v7/widget/ba;->b(Landroid/view/View;)I

    move-result v6

    if-ge v5, v6, :cond_0

    move-object v0, v1

    .line 2154
    goto :goto_1

    .line 2158
    :cond_3
    return-object v0
.end method

.method private b(Landroid/support/v7/widget/RecyclerView$o;Lcom/google/android/flexbox/FlexboxLayoutManager$c;)V
    .locals 8

    .prologue
    const/4 v1, -0x1

    const/4 v4, 0x0

    .line 1308
    invoke-static {p2}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v0

    if-gez v0, :cond_1

    .line 1345
    :cond_0
    :goto_0
    return-void

    .line 1311
    :cond_1
    sget-boolean v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->a:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v0, v0, Lcom/google/android/flexbox/d;->a:[I

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1312
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->z()I

    move-result v5

    .line 1313
    if-eqz v5, :cond_0

    .line 1316
    invoke-virtual {p0, v4}, Lcom/google/android/flexbox/FlexboxLayoutManager;->i(I)Landroid/view/View;

    move-result-object v0

    .line 1318
    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v2, v2, Lcom/google/android/flexbox/d;->a:[I

    invoke-virtual {p0, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->d(Landroid/view/View;)I

    move-result v0

    aget v3, v2, v0

    .line 1319
    if-eq v3, v1, :cond_0

    .line 1322
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/flexbox/c;

    move-object v2, v0

    move v0, v1

    move v1, v4

    .line 1324
    :goto_1
    if-ge v1, v5, :cond_3

    .line 1325
    invoke-virtual {p0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->i(I)Landroid/view/View;

    move-result-object v6

    .line 1326
    invoke-static {p2}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v7

    invoke-direct {p0, v6, v7}, Lcom/google/android/flexbox/FlexboxLayoutManager;->e(Landroid/view/View;I)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1327
    iget v7, v2, Lcom/google/android/flexbox/c;->p:I

    invoke-virtual {p0, v6}, Lcom/google/android/flexbox/FlexboxLayoutManager;->d(Landroid/view/View;)I

    move-result v6

    if-ne v7, v6, :cond_5

    .line 1332
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-lt v3, v0, :cond_4

    move v0, v1

    .line 1344
    :cond_3
    invoke-direct {p0, p1, v4, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(Landroid/support/v7/widget/RecyclerView$o;II)V

    goto :goto_0

    .line 1336
    :cond_4
    invoke-static {p2}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->f(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v0

    add-int v2, v3, v0

    .line 1337
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/flexbox/c;

    move v3, v2

    move-object v2, v0

    move v0, v1

    .line 1324
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private b(Lcom/google/android/flexbox/FlexboxLayoutManager$a;ZZ)V
    .locals 3

    .prologue
    .line 1721
    if-eqz p3, :cond_1

    .line 1722
    invoke-direct {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->k()V

    .line 1726
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->g:Z

    if-eqz v0, :cond_2

    .line 1727
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->I:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-static {p1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->h(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 1728
    invoke-virtual {v2}, Landroid/support/v7/widget/ba;->c()I

    move-result v2

    sub-int/2addr v1, v2

    .line 1727
    invoke-static {v0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->e(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 1733
    :goto_1
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {p1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 1734
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->f(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 1735
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->g(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 1736
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {p1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->h(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 1737
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    const/high16 v1, -0x80000000

    invoke-static {v0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->b(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 1738
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {p1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->e(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 1740
    if-eqz p2, :cond_0

    invoke-static {p1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->e(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    .line 1741
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {p1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->e(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v1

    if-le v0, v1, :cond_0

    .line 1742
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    invoke-static {p1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->e(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/flexbox/c;

    .line 1743
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->j(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    .line 1744
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->h(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/flexbox/c;->b()I

    move-result v0

    sub-int v0, v2, v0

    invoke-static {v1, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 1746
    :cond_0
    return-void

    .line 1724
    :cond_1
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->b(Lcom/google/android/flexbox/FlexboxLayoutManager$c;Z)Z

    goto/16 :goto_0

    .line 1730
    :cond_2
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {p1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->h(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 1731
    invoke-virtual {v2}, Landroid/support/v7/widget/ba;->c()I

    move-result v2

    sub-int/2addr v1, v2

    .line 1730
    invoke-static {v0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->e(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    goto :goto_1
.end method

.method private b(Landroid/support/v7/widget/RecyclerView$t;Lcom/google/android/flexbox/FlexboxLayoutManager$a;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1127
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->z()I

    move-result v2

    if-nez v2, :cond_1

    .line 1154
    :cond_0
    :goto_0
    return v0

    .line 1132
    :cond_1
    invoke-static {p2}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1133
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$t;->e()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/google/android/flexbox/FlexboxLayoutManager;->r(I)Landroid/view/View;

    move-result-object v2

    .line 1135
    :goto_1
    if-eqz v2, :cond_0

    .line 1136
    invoke-static {p2, v2}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$a;Landroid/view/View;)V

    .line 1139
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$t;->a()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->d()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1141
    iget-object v3, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 1142
    invoke-virtual {v3, v2}, Landroid/support/v7/widget/ba;->a(Landroid/view/View;)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 1143
    invoke-virtual {v4}, Landroid/support/v7/widget/ba;->d()I

    move-result v4

    if-ge v3, v4, :cond_2

    iget-object v3, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 1144
    invoke-virtual {v3, v2}, Landroid/support/v7/widget/ba;->b(Landroid/view/View;)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 1145
    invoke-virtual {v3}, Landroid/support/v7/widget/ba;->c()I

    move-result v3

    if-ge v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1146
    :cond_3
    if-eqz v0, :cond_4

    .line 1147
    invoke-static {p2}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 1148
    invoke-virtual {v0}, Landroid/support/v7/widget/ba;->d()I

    move-result v0

    .line 1147
    :goto_2
    invoke-static {p2, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$a;I)I

    :cond_4
    move v0, v1

    .line 1152
    goto :goto_0

    .line 1134
    :cond_5
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$t;->e()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/google/android/flexbox/FlexboxLayoutManager;->q(I)Landroid/view/View;

    move-result-object v2

    goto :goto_1

    .line 1148
    :cond_6
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 1149
    invoke-virtual {v0}, Landroid/support/v7/widget/ba;->c()I

    move-result v0

    goto :goto_2
.end method

.method private c(ILandroid/support/v7/widget/RecyclerView$o;Landroid/support/v7/widget/RecyclerView$t;)I
    .locals 6

    .prologue
    const/4 v1, -0x1

    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 1916
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->z()I

    move-result v3

    if-eqz v3, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move p1, v2

    .line 1945
    :goto_0
    return p1

    .line 1919
    :cond_1
    invoke-direct {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->l()V

    .line 1920
    iget-object v3, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v3, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$c;Z)Z

    .line 1922
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b()Z

    move-result v3

    if-nez v3, :cond_3

    iget-boolean v3, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->g:Z

    if-eqz v3, :cond_3

    move v3, v0

    .line 1923
    :goto_1
    if-eqz v3, :cond_5

    .line 1924
    if-gez p1, :cond_4

    .line 1928
    :cond_2
    :goto_2
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 1930
    invoke-direct {p0, v0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(II)V

    .line 1932
    iget-object v4, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v4}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v4

    .line 1933
    iget-object v5, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-direct {p0, p2, p3, v5}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(Landroid/support/v7/widget/RecyclerView$o;Landroid/support/v7/widget/RecyclerView$t;Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1934
    if-gez v4, :cond_6

    move p1, v2

    .line 1935
    goto :goto_0

    :cond_3
    move v3, v2

    .line 1922
    goto :goto_1

    :cond_4
    move v0, v1

    .line 1924
    goto :goto_2

    .line 1926
    :cond_5
    if-gtz p1, :cond_2

    move v0, v1

    goto :goto_2

    .line 1938
    :cond_6
    if-eqz v3, :cond_8

    .line 1939
    if-le v1, v4, :cond_7

    neg-int v0, v0

    mul-int p1, v0, v4

    .line 1943
    :cond_7
    :goto_3
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    neg-int v1, p1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ba;->a(I)V

    .line 1944
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->h(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    goto :goto_0

    .line 1941
    :cond_8
    if-le v1, v4, :cond_7

    mul-int p1, v0, v4

    goto :goto_3
.end method

.method private c(Lcom/google/android/flexbox/c;Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I
    .locals 20

    .prologue
    .line 1535
    sget-boolean v2, Lcom/google/android/flexbox/FlexboxLayoutManager;->a:Z

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v2, v2, Lcom/google/android/flexbox/d;->b:[J

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 1537
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->getPaddingTop()I

    move-result v5

    .line 1538
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->getPaddingBottom()I

    move-result v6

    .line 1539
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->D()I

    move-result v7

    .line 1541
    invoke-static/range {p2 .. p2}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v3

    .line 1543
    invoke-static/range {p2 .. p2}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v2

    .line 1544
    invoke-static/range {p2 .. p2}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->f(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v4

    const/4 v8, -0x1

    if-ne v4, v8, :cond_a

    .line 1545
    move-object/from16 v0, p1

    iget v4, v0, Lcom/google/android/flexbox/c;->g:I

    sub-int/2addr v3, v4

    .line 1546
    move-object/from16 v0, p1

    iget v4, v0, Lcom/google/android/flexbox/c;->g:I

    add-int/2addr v2, v4

    move v11, v2

    move v12, v3

    .line 1548
    :goto_0
    invoke-static/range {p2 .. p2}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->h(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v15

    .line 1554
    const/4 v2, 0x0

    .line 1555
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->e:I

    packed-switch v3, :pswitch_data_0

    .line 1584
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid justifyContent is set: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->e:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1557
    :pswitch_0
    int-to-float v4, v5

    .line 1558
    sub-int v3, v7, v6

    int-to-float v3, v3

    .line 1587
    :goto_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    invoke-static {v5}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->g(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v5

    int-to-float v5, v5

    sub-float v6, v4, v5

    .line 1588
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    invoke-static {v4}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->g(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v4

    int-to-float v4, v4

    sub-float v5, v3, v4

    .line 1589
    const/4 v3, 0x0

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v16

    .line 1593
    const/4 v4, 0x0

    .line 1594
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/flexbox/c;->b()I

    move-result v17

    move v14, v15

    .line 1595
    :goto_2
    add-int v2, v15, v17

    if-ge v14, v2, :cond_9

    .line 1596
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(I)Landroid/view/View;

    move-result-object v3

    .line 1597
    if-nez v3, :cond_3

    move v2, v5

    move v3, v6

    .line 1595
    :goto_3
    add-int/lit8 v5, v14, 0x1

    move v14, v5

    move v6, v3

    move v5, v2

    goto :goto_2

    .line 1561
    :pswitch_1
    move-object/from16 v0, p1

    iget v3, v0, Lcom/google/android/flexbox/c;->e:I

    sub-int v3, v7, v3

    add-int/2addr v3, v6

    int-to-float v4, v3

    .line 1562
    move-object/from16 v0, p1

    iget v3, v0, Lcom/google/android/flexbox/c;->e:I

    sub-int/2addr v3, v5

    int-to-float v3, v3

    .line 1563
    goto :goto_1

    .line 1565
    :pswitch_2
    int-to-float v3, v5

    move-object/from16 v0, p1

    iget v4, v0, Lcom/google/android/flexbox/c;->e:I

    sub-int v4, v7, v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float/2addr v4, v3

    .line 1566
    sub-int v3, v7, v6

    int-to-float v3, v3

    move-object/from16 v0, p1

    iget v5, v0, Lcom/google/android/flexbox/c;->e:I

    sub-int v5, v7, v5

    int-to-float v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    sub-float/2addr v3, v5

    .line 1568
    goto :goto_1

    .line 1570
    :pswitch_3
    move-object/from16 v0, p1

    iget v3, v0, Lcom/google/android/flexbox/c;->h:I

    if-eqz v3, :cond_1

    .line 1571
    move-object/from16 v0, p1

    iget v2, v0, Lcom/google/android/flexbox/c;->e:I

    sub-int v2, v7, v2

    int-to-float v2, v2

    move-object/from16 v0, p1

    iget v3, v0, Lcom/google/android/flexbox/c;->h:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 1574
    :cond_1
    int-to-float v3, v5

    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v2, v4

    add-float/2addr v4, v3

    .line 1575
    sub-int v3, v7, v6

    int-to-float v3, v3

    const/high16 v5, 0x40000000    # 2.0f

    div-float v5, v2, v5

    sub-float/2addr v3, v5

    .line 1576
    goto :goto_1

    .line 1578
    :pswitch_4
    int-to-float v4, v5

    .line 1579
    move-object/from16 v0, p1

    iget v2, v0, Lcom/google/android/flexbox/c;->h:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_2

    move-object/from16 v0, p1

    iget v2, v0, Lcom/google/android/flexbox/c;->h:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    .line 1580
    :goto_4
    move-object/from16 v0, p1

    iget v3, v0, Lcom/google/android/flexbox/c;->e:I

    sub-int v3, v7, v3

    int-to-float v3, v3

    div-float v2, v3, v2

    .line 1581
    sub-int v3, v7, v6

    int-to-float v3, v3

    .line 1582
    goto/16 :goto_1

    .line 1579
    :cond_2
    const/high16 v2, 0x3f800000    # 1.0f

    goto :goto_4

    .line 1604
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v2, v2, Lcom/google/android/flexbox/d;->b:[J

    aget-wide v8, v2, v14

    .line 1605
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    invoke-virtual {v2, v8, v9}, Lcom/google/android/flexbox/d;->a(J)I

    move-result v7

    .line 1606
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    invoke-virtual {v2, v8, v9}, Lcom/google/android/flexbox/d;->b(J)I

    move-result v8

    .line 1607
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    move-object v10, v2

    check-cast v10, Lcom/google/android/flexbox/FlexboxLayoutManager$b;

    .line 1608
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v7, v8, v10}, Lcom/google/android/flexbox/FlexboxLayoutManager;->c(Landroid/view/View;IILandroid/support/v7/widget/RecyclerView$i;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1609
    invoke-virtual {v3, v7, v8}, Landroid/view/View;->measure(II)V

    .line 1612
    :cond_4
    iget v2, v10, Lcom/google/android/flexbox/FlexboxLayoutManager$b;->topMargin:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->l(Landroid/view/View;)I

    move-result v7

    add-int/2addr v2, v7

    int-to-float v2, v2

    add-float v18, v6, v2

    .line 1613
    iget v2, v10, Lcom/google/android/flexbox/FlexboxLayoutManager$b;->rightMargin:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->m(Landroid/view/View;)I

    move-result v6

    add-int/2addr v2, v6

    int-to-float v2, v2

    sub-float v19, v5, v2

    .line 1615
    invoke-static/range {p2 .. p2}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->f(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v2

    const/4 v5, 0x1

    if-ne v2, v5, :cond_5

    .line 1616
    sget-object v2, Lcom/google/android/flexbox/FlexboxLayoutManager;->b:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1617
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b(Landroid/view/View;)V

    move v13, v4

    .line 1624
    :goto_5
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->n(Landroid/view/View;)I

    move-result v2

    add-int v6, v12, v2

    .line 1625
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->o(Landroid/view/View;)I

    move-result v2

    sub-int v8, v11, v2

    .line 1626
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->g:Z

    if-eqz v2, :cond_7

    .line 1627
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->h:Z

    if-eqz v2, :cond_6

    .line 1628
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->g:Z

    .line 1629
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    sub-int v6, v8, v4

    .line 1630
    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    sub-int v7, v4, v7

    .line 1631
    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->round(F)I

    move-result v9

    move-object/from16 v4, p1

    .line 1628
    invoke-virtual/range {v2 .. v9}, Lcom/google/android/flexbox/d;->a(Landroid/view/View;Lcom/google/android/flexbox/c;ZIIII)V

    .line 1650
    :goto_6
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    iget v4, v10, Lcom/google/android/flexbox/FlexboxLayoutManager$b;->topMargin:I

    add-int/2addr v2, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->m(Landroid/view/View;)I

    move-result v4

    add-int/2addr v2, v4

    int-to-float v2, v2

    add-float v2, v2, v16

    add-float v4, v18, v2

    .line 1652
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    iget v5, v10, Lcom/google/android/flexbox/FlexboxLayoutManager$b;->bottomMargin:I

    add-int/2addr v2, v5

    .line 1653
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->l(Landroid/view/View;)I

    move-result v3

    add-int/2addr v2, v3

    int-to-float v2, v2

    add-float v2, v2, v16

    sub-float v2, v19, v2

    move v3, v4

    move v4, v13

    goto/16 :goto_3

    .line 1619
    :cond_5
    sget-object v2, Lcom/google/android/flexbox/FlexboxLayoutManager;->b:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1620
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b(Landroid/view/View;I)V

    .line 1621
    add-int/lit8 v2, v4, 0x1

    move v13, v2

    goto :goto_5

    .line 1633
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->g:Z

    .line 1634
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    sub-int v6, v8, v4

    .line 1635
    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 1636
    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v4

    move-object/from16 v4, p1

    .line 1633
    invoke-virtual/range {v2 .. v9}, Lcom/google/android/flexbox/d;->a(Landroid/view/View;Lcom/google/android/flexbox/c;ZIIII)V

    goto :goto_6

    .line 1639
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->h:Z

    if-eqz v2, :cond_8

    .line 1640
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->g:Z

    .line 1641
    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    sub-int v7, v4, v7

    .line 1642
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int v8, v6, v4

    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->round(F)I

    move-result v9

    move-object/from16 v4, p1

    .line 1640
    invoke-virtual/range {v2 .. v9}, Lcom/google/android/flexbox/d;->a(Landroid/view/View;Lcom/google/android/flexbox/c;ZIIII)V

    goto/16 :goto_6

    .line 1644
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->g:Z

    .line 1645
    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 1646
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int v8, v6, v4

    .line 1647
    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v4

    move-object/from16 v4, p1

    .line 1644
    invoke-virtual/range {v2 .. v9}, Lcom/google/android/flexbox/d;->a(Landroid/view/View;Lcom/google/android/flexbox/c;ZIIII)V

    goto/16 :goto_6

    .line 1655
    :cond_9
    invoke-static/range {p2 .. p2}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->e(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v3}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->f(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v3

    add-int/2addr v2, v3

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    .line 1656
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/flexbox/c;->a()I

    move-result v2

    return v2

    :cond_a
    move v11, v2

    move v12, v3

    goto/16 :goto_0

    .line 1555
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method private c(III)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1210
    invoke-direct {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->l()V

    .line 1211
    invoke-direct {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->n()V

    .line 1214
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v0}, Landroid/support/v7/widget/ba;->c()I

    move-result v5

    .line 1215
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v0}, Landroid/support/v7/widget/ba;->d()I

    move-result v6

    .line 1216
    if-le p2, p1, :cond_0

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    move-object v4, v2

    .line 1217
    :goto_1
    if-eq p1, p2, :cond_3

    .line 1218
    invoke-virtual {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->i(I)Landroid/view/View;

    move-result-object v3

    .line 1219
    invoke-virtual {p0, v3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->d(Landroid/view/View;)I

    move-result v0

    .line 1220
    if-ltz v0, :cond_6

    if-ge v0, p3, :cond_6

    .line 1221
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$i;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$i;->j_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1222
    if-nez v4, :cond_6

    move-object v0, v2

    .line 1217
    :goto_2
    add-int/2addr p1, v1

    move-object v2, v0

    move-object v4, v3

    goto :goto_1

    .line 1216
    :cond_0
    const/4 v0, -0x1

    move v1, v0

    goto :goto_0

    .line 1225
    :cond_1
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/ba;->a(Landroid/view/View;)I

    move-result v0

    if-lt v0, v5, :cond_2

    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 1226
    invoke-virtual {v0, v3}, Landroid/support/v7/widget/ba;->b(Landroid/view/View;)I

    move-result v0

    if-le v0, v6, :cond_4

    .line 1227
    :cond_2
    if-nez v2, :cond_6

    move-object v0, v3

    move-object v3, v4

    .line 1228
    goto :goto_2

    .line 1235
    :cond_3
    if-eqz v2, :cond_5

    :goto_3
    move-object v3, v2

    :cond_4
    return-object v3

    :cond_5
    move-object v2, v4

    goto :goto_3

    :cond_6
    move-object v0, v2

    move-object v3, v4

    goto :goto_2
.end method

.method private c(Landroid/support/v7/widget/RecyclerView$o;Lcom/google/android/flexbox/FlexboxLayoutManager$c;)V
    .locals 7

    .prologue
    .line 1357
    invoke-static {p2}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v0

    if-gez v0, :cond_1

    .line 1396
    :cond_0
    :goto_0
    return-void

    .line 1360
    :cond_1
    sget-boolean v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->a:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v0, v0, Lcom/google/android/flexbox/d;->a:[I

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1361
    :cond_2
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v0}, Landroid/support/v7/widget/ba;->e()I

    move-result v0

    invoke-static {p2}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v1

    sub-int/2addr v0, v1

    .line 1362
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->z()I

    move-result v1

    .line 1363
    if-eqz v1, :cond_0

    .line 1367
    add-int/lit8 v0, v1, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->i(I)Landroid/view/View;

    move-result-object v0

    .line 1368
    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v2, v2, Lcom/google/android/flexbox/d;->a:[I

    invoke-virtual {p0, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->d(Landroid/view/View;)I

    move-result v0

    aget v3, v2, v0

    .line 1369
    const/4 v0, -0x1

    if-eq v3, v0, :cond_0

    .line 1372
    add-int/lit8 v4, v1, -0x1

    .line 1374
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/flexbox/c;

    .line 1375
    add-int/lit8 v2, v1, -0x1

    :goto_1
    if-ltz v2, :cond_3

    .line 1376
    invoke-virtual {p0, v2}, Lcom/google/android/flexbox/FlexboxLayoutManager;->i(I)Landroid/view/View;

    move-result-object v5

    .line 1377
    invoke-static {p2}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v6

    invoke-direct {p0, v5, v6}, Lcom/google/android/flexbox/FlexboxLayoutManager;->f(Landroid/view/View;I)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1378
    iget v6, v0, Lcom/google/android/flexbox/c;->o:I

    invoke-virtual {p0, v5}, Lcom/google/android/flexbox/FlexboxLayoutManager;->d(Landroid/view/View;)I

    move-result v5

    if-ne v6, v5, :cond_5

    .line 1383
    if-gtz v3, :cond_4

    move v1, v2

    .line 1395
    :cond_3
    invoke-direct {p0, p1, v1, v4}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(Landroid/support/v7/widget/RecyclerView$o;II)V

    goto :goto_0

    .line 1387
    :cond_4
    invoke-static {p2}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->f(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v0

    add-int v1, v3, v0

    .line 1388
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/flexbox/c;

    move v3, v1

    move v1, v2

    .line 1375
    :cond_5
    add-int/lit8 v2, v2, -0x1

    goto :goto_1
.end method

.method private c(Landroid/view/View;IILandroid/support/v7/widget/RecyclerView$i;)Z
    .locals 2

    .prologue
    .line 2293
    invoke-virtual {p1}, Landroid/view/View;->isLayoutRequested()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2294
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->G()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2295
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    iget v1, p4, Landroid/support/v7/widget/RecyclerView$i;->width:I

    invoke-static {v0, p2, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->d(III)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2296
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    iget v1, p4, Landroid/support/v7/widget/RecyclerView$i;->height:I

    invoke-static {v0, p3, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->d(III)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 2293
    :goto_0
    return v0

    .line 2296
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/flexbox/FlexboxLayoutManager;)Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->g:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/flexbox/FlexboxLayoutManager;)Landroid/support/v7/widget/ba;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    return-object v0
.end method

.method private static d(III)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2305
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 2306
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 2307
    if-lez p2, :cond_1

    if-eq p0, p2, :cond_1

    move v0, v1

    .line 2318
    :cond_0
    :goto_0
    :sswitch_0
    return v0

    .line 2310
    :cond_1
    sparse-switch v2, :sswitch_data_0

    move v0, v1

    .line 2318
    goto :goto_0

    .line 2314
    :sswitch_1
    if-ge v3, p0, :cond_0

    move v0, v1

    goto :goto_0

    .line 2316
    :sswitch_2
    if-eq v3, p0, :cond_0

    move v0, v1

    goto :goto_0

    .line 2310
    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_0
        0x40000000 -> :sswitch_2
    .end sparse-switch
.end method

.method static synthetic e(Lcom/google/android/flexbox/FlexboxLayoutManager;)Lcom/google/android/flexbox/d;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    return-object v0
.end method

.method private e(Landroid/view/View;I)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1348
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b()Z

    move-result v2

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->g:Z

    if-eqz v2, :cond_2

    .line 1349
    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v2}, Landroid/support/v7/widget/ba;->e()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 1350
    invoke-virtual {v3, p1}, Landroid/support/v7/widget/ba;->a(Landroid/view/View;)I

    move-result v3

    sub-int/2addr v2, v3

    if-gt v2, p2, :cond_1

    .line 1352
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 1350
    goto :goto_0

    .line 1352
    :cond_2
    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v2, p1}, Landroid/support/v7/widget/ba;->b(Landroid/view/View;)I

    move-result v2

    if-le v2, p2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method static synthetic f(Lcom/google/android/flexbox/FlexboxLayoutManager;)Ljava/util/List;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    return-object v0
.end method

.method private f(Landroid/view/View;I)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1399
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b()Z

    move-result v2

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->g:Z

    if-eqz v2, :cond_2

    .line 1400
    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v2, p1}, Landroid/support/v7/widget/ba;->b(Landroid/view/View;)I

    move-result v2

    if-gt v2, p2, :cond_1

    .line 1402
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 1400
    goto :goto_0

    .line 1402
    :cond_2
    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v2, p1}, Landroid/support/v7/widget/ba;->a(Landroid/view/View;)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 1403
    invoke-virtual {v3}, Landroid/support/v7/widget/ba;->e()I

    move-result v3

    sub-int/2addr v3, p2

    if-ge v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private i(Landroid/support/v7/widget/RecyclerView$t;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2215
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->z()I

    move-result v1

    if-nez v1, :cond_1

    .line 2237
    :cond_0
    :goto_0
    return v0

    .line 2218
    :cond_1
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$t;->e()I

    move-result v1

    .line 2219
    invoke-direct {p0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->q(I)Landroid/view/View;

    move-result-object v2

    .line 2220
    invoke-direct {p0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->r(I)Landroid/view/View;

    move-result-object v1

    .line 2221
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$t;->e()I

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 2224
    sget-boolean v3, Lcom/google/android/flexbox/FlexboxLayoutManager;->a:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v3, v3, Lcom/google/android/flexbox/d;->a:[I

    if-nez v3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2225
    :cond_2
    invoke-virtual {p0, v2}, Lcom/google/android/flexbox/FlexboxLayoutManager;->d(Landroid/view/View;)I

    move-result v3

    .line 2226
    invoke-virtual {p0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->d(Landroid/view/View;)I

    move-result v4

    .line 2227
    iget-object v5, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v5, v1}, Landroid/support/v7/widget/ba;->b(Landroid/view/View;)I

    move-result v1

    iget-object v5, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 2228
    invoke-virtual {v5, v2}, Landroid/support/v7/widget/ba;->a(Landroid/view/View;)I

    move-result v5

    sub-int/2addr v1, v5

    .line 2227
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 2229
    iget-object v5, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v5, v5, Lcom/google/android/flexbox/d;->a:[I

    aget v3, v5, v3

    .line 2230
    if-eqz v3, :cond_0

    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    .line 2233
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v0, v0, Lcom/google/android/flexbox/d;->a:[I

    aget v0, v0, v4

    .line 2234
    sub-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x1

    .line 2235
    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    .line 2237
    int-to-float v1, v3

    mul-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 2238
    invoke-virtual {v1}, Landroid/support/v7/widget/ba;->c()I

    move-result v1

    iget-object v3, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 2239
    invoke-virtual {v3, v2}, Landroid/support/v7/widget/ba;->a(Landroid/view/View;)I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    add-float/2addr v0, v1

    .line 2237
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_0
.end method

.method private i()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 995
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->w()I

    move-result v0

    .line 996
    iget v3, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->c:I

    packed-switch v3, :pswitch_data_0

    .line 1020
    iput-boolean v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->g:Z

    .line 1021
    iput-boolean v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->h:Z

    .line 1023
    :goto_0
    return-void

    .line 998
    :pswitch_0
    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->g:Z

    .line 999
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->d:I

    if-ne v0, v4, :cond_1

    :goto_2
    iput-boolean v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->h:Z

    goto :goto_0

    :cond_0
    move v0, v2

    .line 998
    goto :goto_1

    :cond_1
    move v1, v2

    .line 999
    goto :goto_2

    .line 1002
    :pswitch_1
    if-eq v0, v1, :cond_2

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->g:Z

    .line 1003
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->d:I

    if-ne v0, v4, :cond_3

    :goto_4
    iput-boolean v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->h:Z

    goto :goto_0

    :cond_2
    move v0, v2

    .line 1002
    goto :goto_3

    :cond_3
    move v1, v2

    .line 1003
    goto :goto_4

    .line 1006
    :pswitch_2
    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->g:Z

    .line 1007
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->d:I

    if-ne v0, v4, :cond_4

    .line 1008
    iget-boolean v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->g:Z

    if-nez v0, :cond_6

    :goto_6
    iput-boolean v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->g:Z

    .line 1010
    :cond_4
    iput-boolean v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->h:Z

    goto :goto_0

    :cond_5
    move v0, v2

    .line 1006
    goto :goto_5

    :cond_6
    move v1, v2

    .line 1008
    goto :goto_6

    .line 1013
    :pswitch_3
    if-ne v0, v1, :cond_9

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->g:Z

    .line 1014
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->d:I

    if-ne v0, v4, :cond_8

    .line 1015
    iget-boolean v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->g:Z

    if-nez v0, :cond_7

    move v2, v1

    :cond_7
    iput-boolean v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->g:Z

    .line 1017
    :cond_8
    iput-boolean v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->h:Z

    goto :goto_0

    :cond_9
    move v0, v2

    .line 1013
    goto :goto_7

    .line 996
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private j(Landroid/support/v7/widget/RecyclerView$t;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2268
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->z()I

    move-result v1

    if-nez v1, :cond_1

    .line 2283
    :cond_0
    :goto_0
    return v0

    .line 2271
    :cond_1
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$t;->e()I

    move-result v1

    .line 2272
    invoke-direct {p0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->q(I)Landroid/view/View;

    move-result-object v2

    .line 2273
    invoke-direct {p0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->r(I)Landroid/view/View;

    move-result-object v1

    .line 2274
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$t;->e()I

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 2277
    sget-boolean v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->a:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v0, v0, Lcom/google/android/flexbox/d;->a:[I

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2278
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->c()I

    move-result v0

    .line 2279
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->h()I

    move-result v3

    .line 2280
    iget-object v4, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v4, v1}, Landroid/support/v7/widget/ba;->b(Landroid/view/View;)I

    move-result v1

    iget-object v4, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 2281
    invoke-virtual {v4, v2}, Landroid/support/v7/widget/ba;->a(Landroid/view/View;)I

    move-result v2

    sub-int/2addr v1, v2

    .line 2280
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 2282
    sub-int v0, v3, v0

    add-int/lit8 v0, v0, 0x1

    .line 2283
    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$t;->e()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0
.end method

.method private j()Landroid/view/View;
    .locals 1

    .prologue
    .line 1240
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->i(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private k()V
    .locals 3

    .prologue
    .line 1750
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1751
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->B()I

    move-result v0

    .line 1762
    :goto_0
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    if-eqz v0, :cond_0

    const/high16 v2, -0x80000000

    if-ne v0, v2, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_1
    invoke-static {v1, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->b(Lcom/google/android/flexbox/FlexboxLayoutManager$c;Z)Z

    .line 1764
    return-void

    .line 1753
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->A()I

    move-result v0

    goto :goto_0

    .line 1762
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private l()V
    .locals 1

    .prologue
    .line 1767
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    if-eqz v0, :cond_0

    .line 1795
    :goto_0
    return-void

    .line 1778
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1779
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->d:I

    if-nez v0, :cond_1

    .line 1780
    invoke-static {p0}, Landroid/support/v7/widget/ba;->a(Landroid/support/v7/widget/RecyclerView$h;)Landroid/support/v7/widget/ba;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 1781
    invoke-static {p0}, Landroid/support/v7/widget/ba;->b(Landroid/support/v7/widget/RecyclerView$h;)Landroid/support/v7/widget/ba;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->z:Landroid/support/v7/widget/ba;

    goto :goto_0

    .line 1783
    :cond_1
    invoke-static {p0}, Landroid/support/v7/widget/ba;->b(Landroid/support/v7/widget/RecyclerView$h;)Landroid/support/v7/widget/ba;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 1784
    invoke-static {p0}, Landroid/support/v7/widget/ba;->a(Landroid/support/v7/widget/RecyclerView$h;)Landroid/support/v7/widget/ba;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->z:Landroid/support/v7/widget/ba;

    goto :goto_0

    .line 1787
    :cond_2
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->d:I

    if-nez v0, :cond_3

    .line 1788
    invoke-static {p0}, Landroid/support/v7/widget/ba;->b(Landroid/support/v7/widget/RecyclerView$h;)Landroid/support/v7/widget/ba;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 1789
    invoke-static {p0}, Landroid/support/v7/widget/ba;->a(Landroid/support/v7/widget/RecyclerView$h;)Landroid/support/v7/widget/ba;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->z:Landroid/support/v7/widget/ba;

    goto :goto_0

    .line 1791
    :cond_3
    invoke-static {p0}, Landroid/support/v7/widget/ba;->a(Landroid/support/v7/widget/RecyclerView$h;)Landroid/support/v7/widget/ba;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 1792
    invoke-static {p0}, Landroid/support/v7/widget/ba;->b(Landroid/support/v7/widget/RecyclerView$h;)Landroid/support/v7/widget/ba;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->z:Landroid/support/v7/widget/ba;

    goto :goto_0
.end method

.method private n()V
    .locals 2

    .prologue
    .line 1798
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    if-nez v0, :cond_0

    .line 1799
    new-instance v0, Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;-><init>(Lcom/google/android/flexbox/FlexboxLayoutManager$1;)V

    iput-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    .line 1801
    :cond_0
    return-void
.end method

.method private o()V
    .locals 2

    .prologue
    .line 2322
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2323
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    invoke-static {v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->b(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)V

    .line 2324
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$a;I)I

    .line 2325
    return-void
.end method

.method private o(I)V
    .locals 4

    .prologue
    .line 617
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->c()I

    move-result v0

    .line 618
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->h()I

    move-result v1

    .line 619
    if-lt p1, v1, :cond_1

    .line 653
    :cond_0
    :goto_0
    return-void

    .line 622
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->z()I

    move-result v2

    .line 623
    iget-object v3, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    invoke-virtual {v3, v2}, Lcom/google/android/flexbox/d;->c(I)V

    .line 624
    iget-object v3, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    invoke-virtual {v3, v2}, Lcom/google/android/flexbox/d;->b(I)V

    .line 625
    iget-object v3, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    invoke-virtual {v3, v2}, Lcom/google/android/flexbox/d;->d(I)V

    .line 626
    sget-boolean v2, Lcom/google/android/flexbox/FlexboxLayoutManager;->a:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v2, v2, Lcom/google/android/flexbox/d;->a:[I

    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 628
    :cond_2
    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v2, v2, Lcom/google/android/flexbox/d;->a:[I

    array-length v2, v2

    if-ge p1, v2, :cond_0

    .line 632
    iput p1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->J:I

    .line 634
    invoke-direct {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->j()Landroid/view/View;

    move-result-object v2

    .line 635
    if-eqz v2, :cond_0

    .line 638
    if-gt v0, p1, :cond_3

    if-le p1, v1, :cond_0

    .line 644
    :cond_3
    invoke-virtual {p0, v2}, Lcom/google/android/flexbox/FlexboxLayoutManager;->d(Landroid/view/View;)I

    move-result v0

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->B:I

    .line 646
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b()Z

    move-result v0

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->g:Z

    if-eqz v0, :cond_4

    .line 647
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/ba;->b(Landroid/view/View;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 648
    invoke-virtual {v1}, Landroid/support/v7/widget/ba;->g()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->C:I

    goto :goto_0

    .line 650
    :cond_4
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/ba;->a(Landroid/view/View;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 651
    invoke-virtual {v1}, Landroid/support/v7/widget/ba;->c()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->C:I

    goto :goto_0
.end method

.method private p(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 2328
    .line 2329
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$i;

    .line 2330
    invoke-virtual {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->h(Landroid/view/View;)I

    move-result v1

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$i;->leftMargin:I

    sub-int v0, v1, v0

    return v0
.end method

.method private p(I)V
    .locals 14

    .prologue
    const/4 v0, 0x1

    const/high16 v4, -0x80000000

    const/4 v7, -0x1

    const/4 v11, 0x0

    .line 846
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->C()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->A()I

    move-result v2

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 848
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->D()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->B()I

    move-result v3

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 849
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->C()I

    move-result v5

    .line 850
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->D()I

    move-result v6

    .line 856
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 857
    iget v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->D:I

    if-eq v1, v4, :cond_1

    iget v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->D:I

    if-eq v1, v5, :cond_1

    .line 864
    :goto_0
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->b(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->H:Landroid/content/Context;

    .line 865
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    :goto_1
    move v4, v1

    .line 880
    :goto_2
    iput v5, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->D:I

    .line 881
    iput v6, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->E:I

    .line 883
    iget v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->J:I

    if-ne v1, v7, :cond_9

    iget v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->B:I

    if-ne v1, v7, :cond_0

    if-eqz v0, :cond_9

    .line 885
    :cond_0
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    invoke-static {v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 974
    :goto_3
    return-void

    :cond_1
    move v0, v11

    .line 857
    goto :goto_0

    .line 865
    :cond_2
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    .line 866
    invoke-static {v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v1

    goto :goto_1

    .line 868
    :cond_3
    iget v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->E:I

    if-eq v1, v4, :cond_4

    iget v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->E:I

    if-eq v1, v6, :cond_4

    .line 875
    :goto_4
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->b(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->H:Landroid/content/Context;

    .line 876
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    :goto_5
    move v4, v1

    .line 877
    goto :goto_2

    :cond_4
    move v0, v11

    .line 868
    goto :goto_4

    .line 876
    :cond_5
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    .line 877
    invoke-static {v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v1

    goto :goto_5

    .line 898
    :cond_6
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 899
    sget-boolean v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->a:Z

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v0, v0, Lcom/google/android/flexbox/d;->a:[I

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 900
    :cond_7
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->K:Lcom/google/android/flexbox/d$a;

    invoke-virtual {v0}, Lcom/google/android/flexbox/d$a;->a()V

    .line 901
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 902
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->K:Lcom/google/android/flexbox/d$a;

    iget-object v5, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    .line 905
    invoke-static {v5}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v5

    iget-object v6, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    .line 903
    invoke-virtual/range {v0 .. v6}, Lcom/google/android/flexbox/d;->b(Lcom/google/android/flexbox/d$a;IIIILjava/util/List;)V

    .line 912
    :goto_6
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->K:Lcom/google/android/flexbox/d$a;

    iget-object v0, v0, Lcom/google/android/flexbox/d$a;->a:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    .line 913
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/flexbox/d;->a(II)V

    .line 914
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    invoke-virtual {v0}, Lcom/google/android/flexbox/d;->a()V

    .line 915
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v1, v1, Lcom/google/android/flexbox/d;->a:[I

    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    .line 916
    invoke-static {v2}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v2

    aget v1, v1, v2

    .line 915
    invoke-static {v0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$a;I)I

    .line 917
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    invoke-static {v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->e(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$c;I)I

    goto/16 :goto_3

    .line 907
    :cond_8
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->K:Lcom/google/android/flexbox/d$a;

    iget-object v5, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    .line 910
    invoke-static {v5}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v5

    iget-object v6, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    .line 908
    invoke-virtual/range {v0 .. v6}, Lcom/google/android/flexbox/d;->d(Lcom/google/android/flexbox/d$a;IIIILjava/util/List;)V

    goto :goto_6

    .line 923
    :cond_9
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->J:I

    if-eq v0, v7, :cond_a

    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->J:I

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    .line 924
    invoke-static {v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 926
    :goto_7
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->K:Lcom/google/android/flexbox/d$a;

    invoke-virtual {v0}, Lcom/google/android/flexbox/d$a;->a()V

    .line 927
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 928
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_b

    .line 933
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    invoke-virtual {v0, v1, v5}, Lcom/google/android/flexbox/d;->a(Ljava/util/List;I)V

    .line 934
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->K:Lcom/google/android/flexbox/d$a;

    iget-object v6, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    .line 935
    invoke-static {v6}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v6

    iget-object v7, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    .line 934
    invoke-virtual/range {v0 .. v7}, Lcom/google/android/flexbox/d;->a(Lcom/google/android/flexbox/d$a;IIIIILjava/util/List;)V

    .line 961
    :goto_8
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->K:Lcom/google/android/flexbox/d$a;

    iget-object v0, v0, Lcom/google/android/flexbox/d$a;->a:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    .line 962
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    invoke-virtual {v0, v2, v3, v5}, Lcom/google/android/flexbox/d;->a(III)V

    .line 972
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    invoke-virtual {v0, v5}, Lcom/google/android/flexbox/d;->a(I)V

    goto/16 :goto_3

    .line 924
    :cond_a
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    invoke-static {v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v5

    goto :goto_7

    .line 938
    :cond_b
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    invoke-virtual {v0, p1}, Lcom/google/android/flexbox/d;->d(I)V

    .line 939
    iget-object v6, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v7, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->K:Lcom/google/android/flexbox/d$a;

    iget-object v12, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    move v8, v2

    move v9, v3

    move v10, v4

    .line 940
    invoke-virtual/range {v6 .. v12}, Lcom/google/android/flexbox/d;->a(Lcom/google/android/flexbox/d$a;IIIILjava/util/List;)V

    goto :goto_8

    .line 945
    :cond_c
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_d

    .line 950
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    invoke-virtual {v0, v1, v5}, Lcom/google/android/flexbox/d;->a(Ljava/util/List;I)V

    .line 951
    iget-object v6, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v7, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->K:Lcom/google/android/flexbox/d$a;

    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    .line 952
    invoke-static {v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v12

    iget-object v13, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    move v8, v3

    move v9, v2

    move v10, v4

    move v11, v5

    .line 951
    invoke-virtual/range {v6 .. v13}, Lcom/google/android/flexbox/d;->a(Lcom/google/android/flexbox/d$a;IIIIILjava/util/List;)V

    goto :goto_8

    .line 955
    :cond_d
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    invoke-virtual {v0, p1}, Lcom/google/android/flexbox/d;->d(I)V

    .line 956
    iget-object v6, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v7, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->K:Lcom/google/android/flexbox/d$a;

    iget-object v12, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    move v8, v2

    move v9, v3

    move v10, v4

    .line 957
    invoke-virtual/range {v6 .. v12}, Lcom/google/android/flexbox/d;->c(Lcom/google/android/flexbox/d$a;IIIILjava/util/List;)V

    goto :goto_8
.end method

.method private q(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 2334
    .line 2335
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$i;

    .line 2336
    invoke-virtual {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->j(Landroid/view/View;)I

    move-result v1

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$i;->rightMargin:I

    add-int/2addr v0, v1

    return v0
.end method

.method private q(I)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1166
    sget-boolean v1, Lcom/google/android/flexbox/FlexboxLayoutManager;->a:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v1, v1, Lcom/google/android/flexbox/d;->a:[I

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1167
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->z()I

    move-result v2

    invoke-direct {p0, v1, v2, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->c(III)Landroid/view/View;

    move-result-object v1

    .line 1168
    if-nez v1, :cond_2

    .line 1177
    :cond_1
    :goto_0
    return-object v0

    .line 1171
    :cond_2
    invoke-virtual {p0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->d(Landroid/view/View;)I

    move-result v2

    .line 1172
    iget-object v3, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v3, v3, Lcom/google/android/flexbox/d;->a:[I

    aget v2, v3, v2

    .line 1173
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    .line 1176
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/flexbox/c;

    .line 1177
    invoke-direct {p0, v1, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(Landroid/view/View;Lcom/google/android/flexbox/c;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private r(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 2340
    .line 2341
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$i;

    .line 2342
    invoke-virtual {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->i(Landroid/view/View;)I

    move-result v1

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$i;->topMargin:I

    sub-int v0, v1, v0

    return v0
.end method

.method private r(I)Landroid/view/View;
    .locals 3

    .prologue
    .line 1189
    sget-boolean v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v0, v0, Lcom/google/android/flexbox/d;->a:[I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1190
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->z()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->c(III)Landroid/view/View;

    move-result-object v1

    .line 1191
    if-nez v1, :cond_1

    .line 1192
    const/4 v0, 0x0

    .line 1197
    :goto_0
    return-object v0

    .line 1194
    :cond_1
    invoke-virtual {p0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->d(Landroid/view/View;)I

    move-result v0

    .line 1195
    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    iget-object v2, v2, Lcom/google/android/flexbox/d;->a:[I

    aget v0, v2, v0

    .line 1196
    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/flexbox/c;

    .line 1197
    invoke-direct {p0, v1, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b(Landroid/view/View;Lcom/google/android/flexbox/c;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private s(I)I
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1949
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->z()I

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_2

    :cond_0
    move p1, v1

    .line 1978
    :cond_1
    :goto_0
    return p1

    .line 1952
    :cond_2
    invoke-direct {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->l()V

    .line 1953
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b()Z

    move-result v4

    .line 1954
    if-eqz v4, :cond_4

    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->I:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    move v3, v0

    .line 1955
    :goto_1
    if-eqz v4, :cond_5

    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->C()I

    move-result v0

    .line 1957
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->w()I

    move-result v4

    if-ne v4, v2, :cond_3

    move v1, v2

    .line 1958
    :cond_3
    if-eqz v1, :cond_7

    .line 1959
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 1960
    if-gez p1, :cond_6

    .line 1961
    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    .line 1962
    invoke-static {v2}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->g(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v2

    add-int/2addr v0, v2

    sub-int/2addr v0, v3

    .line 1961
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1963
    neg-int p1, v0

    goto :goto_0

    .line 1954
    :cond_4
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->I:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    move v3, v0

    goto :goto_1

    .line 1955
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->D()I

    move-result v0

    goto :goto_2

    .line 1965
    :cond_6
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    invoke-static {v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->g(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v0

    add-int/2addr v0, p1

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    .line 1966
    invoke-static {v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->g(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v0

    neg-int p1, v0

    goto :goto_0

    .line 1970
    :cond_7
    if-lez p1, :cond_8

    .line 1971
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    .line 1972
    invoke-static {v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->g(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v1

    sub-int/2addr v0, v1

    sub-int/2addr v0, v3

    .line 1971
    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    goto :goto_0

    .line 1974
    :cond_8
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    invoke-static {v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->g(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v0

    add-int/2addr v0, p1

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    .line 1975
    invoke-static {v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->g(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v0

    neg-int p1, v0

    goto :goto_0
.end method

.method private s(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 2346
    .line 2347
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$i;

    .line 2348
    invoke-virtual {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->k(Landroid/view/View;)I

    move-result v1

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$i;->bottomMargin:I

    add-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public a(ILandroid/support/v7/widget/RecyclerView$o;Landroid/support/v7/widget/RecyclerView$t;)I
    .locals 3

    .prologue
    .line 1880
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1881
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->c(ILandroid/support/v7/widget/RecyclerView$o;Landroid/support/v7/widget/RecyclerView$t;)I

    move-result v0

    .line 1882
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->G:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->clear()V

    .line 1888
    :goto_0
    return v0

    .line 1885
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->s(I)I

    move-result v0

    .line 1886
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    invoke-static {v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->g(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v2

    add-int/2addr v2, v0

    invoke-static {v1, v2}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$a;I)I

    .line 1887
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->z:Landroid/support/v7/widget/ba;

    neg-int v2, v0

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/ba;->a(I)V

    goto :goto_0
.end method

.method public a(Landroid/view/View;II)I
    .locals 2

    .prologue
    .line 371
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372
    invoke-virtual {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->n(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->o(Landroid/view/View;)I

    move-result v1

    add-int/2addr v0, v1

    .line 374
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->l(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->m(Landroid/view/View;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public a()Landroid/support/v7/widget/RecyclerView$i;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 534
    new-instance v0, Lcom/google/android/flexbox/FlexboxLayoutManager$b;

    invoke-direct {v0, v1, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$b;-><init>(II)V

    return-object v0
.end method

.method public a(Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/support/v7/widget/RecyclerView$i;
    .locals 1

    .prologue
    .line 539
    new-instance v0, Lcom/google/android/flexbox/FlexboxLayoutManager$b;

    invoke-direct {v0, p1, p2}, Lcom/google/android/flexbox/FlexboxLayoutManager$b;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public a(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 435
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->G:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 436
    if-eqz v0, :cond_0

    .line 439
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->k:Landroid/support/v7/widget/RecyclerView$o;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView$o;->c(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public a(ILandroid/view/View;)V
    .locals 1

    .prologue
    .line 513
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->G:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 514
    return-void
.end method

.method public a(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 572
    instance-of v0, p1, Lcom/google/android/flexbox/FlexboxLayoutManager$d;

    if-eqz v0, :cond_0

    .line 573
    check-cast p1, Lcom/google/android/flexbox/FlexboxLayoutManager$d;

    iput-object p1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->A:Lcom/google/android/flexbox/FlexboxLayoutManager$d;

    .line 574
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->r()V

    .line 583
    :cond_0
    return-void
.end method

.method public a(Landroid/support/v7/widget/RecyclerView$a;Landroid/support/v7/widget/RecyclerView$a;)V
    .locals 0

    .prologue
    .line 549
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->x()V

    .line 550
    return-void
.end method

.method public a(Landroid/support/v7/widget/RecyclerView$t;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 978
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$h;->a(Landroid/support/v7/widget/RecyclerView$t;)V

    .line 982
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->A:Lcom/google/android/flexbox/FlexboxLayoutManager$d;

    .line 983
    iput v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->B:I

    .line 984
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->C:I

    .line 985
    iput v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->J:I

    .line 986
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    invoke-static {v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->b(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)V

    .line 987
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->G:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 988
    return-void
.end method

.method public a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 0

    .prologue
    .line 587
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView$h;->a(Landroid/support/v7/widget/RecyclerView;II)V

    .line 588
    invoke-direct {p0, p2}, Lcom/google/android/flexbox/FlexboxLayoutManager;->o(I)V

    .line 589
    return-void
.end method

.method public a(Landroid/support/v7/widget/RecyclerView;III)V
    .locals 1

    .prologue
    .line 612
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v7/widget/RecyclerView$h;->a(Landroid/support/v7/widget/RecyclerView;III)V

    .line 613
    invoke-static {p2, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->o(I)V

    .line 614
    return-void
.end method

.method public a(Landroid/support/v7/widget/RecyclerView;IILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 594
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v7/widget/RecyclerView$h;->a(Landroid/support/v7/widget/RecyclerView;IILjava/lang/Object;)V

    .line 595
    invoke-direct {p0, p2}, Lcom/google/android/flexbox/FlexboxLayoutManager;->o(I)V

    .line 596
    return-void
.end method

.method public a(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$o;)V
    .locals 1

    .prologue
    .line 1857
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView$h;->a(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$o;)V

    .line 1858
    iget-boolean v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->F:Z

    if-eqz v0, :cond_0

    .line 1862
    invoke-virtual {p0, p2}, Lcom/google/android/flexbox/FlexboxLayoutManager;->c(Landroid/support/v7/widget/RecyclerView$o;)V

    .line 1863
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView$o;->a()V

    .line 1865
    :cond_0
    return-void
.end method

.method public a(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$t;I)V
    .locals 2

    .prologue
    .line 1816
    new-instance v0, Landroid/support/v7/widget/au;

    .line 1817
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/widget/au;-><init>(Landroid/content/Context;)V

    .line 1818
    invoke-virtual {v0, p3}, Landroid/support/v7/widget/au;->d(I)V

    .line 1819
    invoke-virtual {p0, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(Landroid/support/v7/widget/RecyclerView$s;)V

    .line 1820
    return-void
.end method

.method public a(Landroid/view/View;IILcom/google/android/flexbox/c;)V
    .locals 2

    .prologue
    .line 392
    sget-object v0, Lcom/google/android/flexbox/FlexboxLayoutManager;->b:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 393
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 394
    invoke-virtual {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->n(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->o(Landroid/view/View;)I

    move-result v1

    add-int/2addr v0, v1

    .line 395
    iget v1, p4, Lcom/google/android/flexbox/c;->e:I

    add-int/2addr v1, v0

    iput v1, p4, Lcom/google/android/flexbox/c;->e:I

    .line 396
    iget v1, p4, Lcom/google/android/flexbox/c;->f:I

    add-int/2addr v0, v1

    iput v0, p4, Lcom/google/android/flexbox/c;->f:I

    .line 402
    :goto_0
    return-void

    .line 398
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->l(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->m(Landroid/view/View;)I

    move-result v1

    add-int/2addr v0, v1

    .line 399
    iget v1, p4, Lcom/google/android/flexbox/c;->e:I

    add-int/2addr v1, v0

    iput v1, p4, Lcom/google/android/flexbox/c;->e:I

    .line 400
    iget v1, p4, Lcom/google/android/flexbox/c;->f:I

    add-int/2addr v0, v1

    iput v0, p4, Lcom/google/android/flexbox/c;->f:I

    goto :goto_0
.end method

.method public a(Lcom/google/android/flexbox/c;)V
    .locals 0

    .prologue
    .line 463
    return-void
.end method

.method public a(Landroid/support/v7/widget/RecyclerView$i;)Z
    .locals 1

    .prologue
    .line 544
    instance-of v0, p1, Lcom/google/android/flexbox/FlexboxLayoutManager$b;

    return v0
.end method

.method public a_(III)I
    .locals 3

    .prologue
    .line 467
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->C()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->A()I

    move-result v1

    .line 468
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->f()Z

    move-result v2

    .line 467
    invoke-static {v0, v1, p2, p3, v2}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(IIIIZ)I

    move-result v0

    return v0
.end method

.method public a_(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 380
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 381
    invoke-virtual {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->l(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->m(Landroid/view/View;)I

    move-result v1

    add-int/2addr v0, v1

    .line 383
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->n(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->o(Landroid/view/View;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public b(III)I
    .locals 3

    .prologue
    .line 473
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->D()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->B()I

    move-result v1

    .line 474
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->g()Z

    move-result v2

    .line 473
    invoke-static {v0, v1, p2, p3, v2}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(IIIIZ)I

    move-result v0

    return v0
.end method

.method public b(ILandroid/support/v7/widget/RecyclerView$o;Landroid/support/v7/widget/RecyclerView$t;)I
    .locals 3

    .prologue
    .line 1895
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1896
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->c(ILandroid/support/v7/widget/RecyclerView$o;Landroid/support/v7/widget/RecyclerView$t;)I

    move-result v0

    .line 1897
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->G:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->clear()V

    .line 1903
    :goto_0
    return v0

    .line 1900
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->s(I)I

    move-result v0

    .line 1901
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    invoke-static {v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->g(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)I

    move-result v2

    add-int/2addr v2, v0

    invoke-static {v1, v2}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->d(Lcom/google/android/flexbox/FlexboxLayoutManager$a;I)I

    .line 1902
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->z:Landroid/support/v7/widget/ba;

    neg-int v2, v0

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/ba;->a(I)V

    goto :goto_0
.end method

.method public b(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 457
    invoke-virtual {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 0

    .prologue
    .line 606
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView$h;->b(Landroid/support/v7/widget/RecyclerView;II)V

    .line 607
    invoke-direct {p0, p2}, Lcom/google/android/flexbox/FlexboxLayoutManager;->o(I)V

    .line 608
    return-void
.end method

.method public b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1661
    iget v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->c:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->c:I

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2409
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->z()I

    move-result v0

    invoke-direct {p0, v1, v0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(IIZ)Landroid/view/View;

    move-result-object v0

    .line 2410
    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->d(Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method

.method public c(Landroid/support/v7/widget/RecyclerView$t;)I
    .locals 1

    .prologue
    .line 2198
    invoke-direct {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->i(Landroid/support/v7/widget/RecyclerView$t;)I

    .line 2202
    invoke-direct {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->i(Landroid/support/v7/widget/RecyclerView$t;)I

    move-result v0

    return v0
.end method

.method public c(Landroid/support/v7/widget/RecyclerView$o;Landroid/support/v7/widget/RecyclerView$t;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 677
    iput-object p1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->k:Landroid/support/v7/widget/RecyclerView$o;

    .line 678
    iput-object p2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->l:Landroid/support/v7/widget/RecyclerView$t;

    .line 679
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView$t;->e()I

    move-result v0

    .line 680
    if-nez v0, :cond_1

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView$t;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 765
    :cond_0
    :goto_0
    return-void

    .line 683
    :cond_1
    invoke-direct {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->i()V

    .line 684
    invoke-direct {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->l()V

    .line 685
    invoke-direct {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->n()V

    .line 686
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    invoke-virtual {v1, v0}, Lcom/google/android/flexbox/d;->c(I)V

    .line 687
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    invoke-virtual {v1, v0}, Lcom/google/android/flexbox/d;->b(I)V

    .line 689
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->j:Lcom/google/android/flexbox/d;

    invoke-virtual {v1, v0}, Lcom/google/android/flexbox/d;->d(I)V

    .line 691
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v1, v3}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$c;Z)Z

    .line 693
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->A:Lcom/google/android/flexbox/FlexboxLayoutManager$d;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->A:Lcom/google/android/flexbox/FlexboxLayoutManager$d;

    invoke-static {v1, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$d;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$d;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 694
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->A:Lcom/google/android/flexbox/FlexboxLayoutManager$d;

    invoke-static {v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$d;->b(Lcom/google/android/flexbox/FlexboxLayoutManager$d;)I

    move-result v1

    iput v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->B:I

    .line 697
    :cond_2
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    invoke-static {v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->B:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->A:Lcom/google/android/flexbox/FlexboxLayoutManager$d;

    if-eqz v1, :cond_4

    .line 699
    :cond_3
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    invoke-static {v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->b(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)V

    .line 700
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    invoke-direct {p0, p2, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(Landroid/support/v7/widget/RecyclerView$t;Lcom/google/android/flexbox/FlexboxLayoutManager$a;)V

    .line 701
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    invoke-static {v1, v4}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$a;Z)Z

    .line 703
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(Landroid/support/v7/widget/RecyclerView$o;)V

    .line 705
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    invoke-static {v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 706
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    invoke-direct {p0, v1, v3, v4}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b(Lcom/google/android/flexbox/FlexboxLayoutManager$a;ZZ)V

    .line 716
    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->p(I)V

    .line 728
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    invoke-static {v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 729
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(Landroid/support/v7/widget/RecyclerView$o;Landroid/support/v7/widget/RecyclerView$t;Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    .line 733
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v1

    .line 734
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    invoke-direct {p0, v0, v4, v3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$a;ZZ)V

    .line 735
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(Landroid/support/v7/widget/RecyclerView$o;Landroid/support/v7/widget/RecyclerView$t;Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    .line 739
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v0

    .line 754
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->z()I

    move-result v2

    if-lez v2, :cond_0

    .line 755
    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    invoke-static {v2}, Lcom/google/android/flexbox/FlexboxLayoutManager$a;->c(Lcom/google/android/flexbox/FlexboxLayoutManager$a;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 756
    invoke-direct {p0, v0, p1, p2, v4}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b(ILandroid/support/v7/widget/RecyclerView$o;Landroid/support/v7/widget/RecyclerView$t;Z)I

    move-result v0

    .line 757
    add-int/2addr v0, v1

    .line 758
    invoke-direct {p0, v0, p1, p2, v3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(ILandroid/support/v7/widget/RecyclerView$o;Landroid/support/v7/widget/RecyclerView$t;Z)I

    goto/16 :goto_0

    .line 708
    :cond_5
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    invoke-direct {p0, v1, v3, v4}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$a;ZZ)V

    goto :goto_1

    .line 741
    :cond_6
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(Landroid/support/v7/widget/RecyclerView$o;Landroid/support/v7/widget/RecyclerView$t;Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    .line 745
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v0

    .line 746
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->n:Lcom/google/android/flexbox/FlexboxLayoutManager$a;

    invoke-direct {p0, v1, v4, v3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b(Lcom/google/android/flexbox/FlexboxLayoutManager$a;ZZ)V

    .line 747
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(Landroid/support/v7/widget/RecyclerView$o;Landroid/support/v7/widget/RecyclerView$t;Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    .line 751
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->m:Lcom/google/android/flexbox/FlexboxLayoutManager$c;

    invoke-static {v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$c;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$c;)I

    move-result v1

    goto :goto_2

    .line 760
    :cond_7
    invoke-direct {p0, v1, p1, p2, v4}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(ILandroid/support/v7/widget/RecyclerView$o;Landroid/support/v7/widget/RecyclerView$t;Z)I

    move-result v1

    .line 761
    add-int/2addr v0, v1

    .line 762
    invoke-direct {p0, v0, p1, p2, v3}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b(ILandroid/support/v7/widget/RecyclerView$o;Landroid/support/v7/widget/RecyclerView$t;Z)I

    goto/16 :goto_0
.end method

.method public c(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 0

    .prologue
    .line 600
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView$h;->c(Landroid/support/v7/widget/RecyclerView;II)V

    .line 601
    invoke-direct {p0, p2}, Lcom/google/android/flexbox/FlexboxLayoutManager;->o(I)V

    .line 602
    return-void
.end method

.method public d(Landroid/support/v7/widget/RecyclerView$t;)I
    .locals 1

    .prologue
    .line 2207
    invoke-direct {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->i(Landroid/support/v7/widget/RecyclerView$t;)I

    move-result v0

    .line 2211
    return v0
.end method

.method public d(I)Landroid/graphics/PointF;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 520
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->z()I

    move-result v0

    if-nez v0, :cond_0

    .line 521
    const/4 v0, 0x0

    .line 528
    :goto_0
    return-object v0

    .line 523
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->i(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->d(Landroid/view/View;)I

    move-result v0

    .line 524
    if-ge p1, v0, :cond_1

    const/4 v0, -0x1

    .line 525
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 526
    new-instance v1, Landroid/graphics/PointF;

    int-to-float v0, v0

    invoke-direct {v1, v2, v0}, Landroid/graphics/PointF;-><init>(FF)V

    move-object v0, v1

    goto :goto_0

    .line 524
    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    .line 528
    :cond_2
    new-instance v1, Landroid/graphics/PointF;

    int-to-float v0, v0

    invoke-direct {v1, v0, v2}, Landroid/graphics/PointF;-><init>(FF)V

    move-object v0, v1

    goto :goto_0
.end method

.method public d(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 1851
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$h;->d(Landroid/support/v7/widget/RecyclerView;)V

    .line 1852
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->I:Landroid/view/View;

    .line 1853
    return-void
.end method

.method public e(Landroid/support/v7/widget/RecyclerView$t;)I
    .locals 1

    .prologue
    .line 2163
    invoke-direct {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b(Landroid/support/v7/widget/RecyclerView$t;)I

    move-result v0

    .line 2167
    return v0
.end method

.method public e()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 554
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->A:Lcom/google/android/flexbox/FlexboxLayoutManager$d;

    if-eqz v0, :cond_0

    .line 555
    new-instance v0, Lcom/google/android/flexbox/FlexboxLayoutManager$d;

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->A:Lcom/google/android/flexbox/FlexboxLayoutManager$d;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/flexbox/FlexboxLayoutManager$d;-><init>(Lcom/google/android/flexbox/FlexboxLayoutManager$d;Lcom/google/android/flexbox/FlexboxLayoutManager$1;)V

    .line 567
    :goto_0
    return-object v0

    .line 557
    :cond_0
    new-instance v0, Lcom/google/android/flexbox/FlexboxLayoutManager$d;

    invoke-direct {v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$d;-><init>()V

    .line 558
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->z()I

    move-result v1

    if-lez v1, :cond_1

    .line 560
    invoke-direct {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->j()Landroid/view/View;

    move-result-object v1

    .line 561
    invoke-virtual {p0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->d(Landroid/view/View;)I

    move-result v2

    invoke-static {v0, v2}, Lcom/google/android/flexbox/FlexboxLayoutManager$d;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$d;I)I

    .line 562
    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/ba;->a(Landroid/view/View;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 563
    invoke-virtual {v2}, Landroid/support/v7/widget/ba;->c()I

    move-result v2

    sub-int/2addr v1, v2

    .line 562
    invoke-static {v0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager$d;->b(Lcom/google/android/flexbox/FlexboxLayoutManager$d;I)I

    goto :goto_0

    .line 565
    :cond_1
    invoke-static {v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$d;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$d;)V

    goto :goto_0
.end method

.method public e(I)V
    .locals 1

    .prologue
    .line 1805
    iput p1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->B:I

    .line 1806
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->C:I

    .line 1807
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->A:Lcom/google/android/flexbox/FlexboxLayoutManager$d;

    if-eqz v0, :cond_0

    .line 1808
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->A:Lcom/google/android/flexbox/FlexboxLayoutManager$d;

    invoke-static {v0}, Lcom/google/android/flexbox/FlexboxLayoutManager$d;->a(Lcom/google/android/flexbox/FlexboxLayoutManager$d;)V

    .line 1810
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->r()V

    .line 1811
    return-void
.end method

.method public f(Landroid/support/v7/widget/RecyclerView$t;)I
    .locals 1

    .prologue
    .line 2172
    invoke-direct {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b(Landroid/support/v7/widget/RecyclerView$t;)I

    move-result v0

    .line 2176
    return v0
.end method

.method public f(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 274
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->c:I

    if-eq v0, p1, :cond_0

    .line 278
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->x()V

    .line 279
    iput p1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->c:I

    .line 280
    iput-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 281
    iput-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->z:Landroid/support/v7/widget/ba;

    .line 282
    invoke-direct {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->o()V

    .line 283
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->r()V

    .line 285
    :cond_0
    return-void
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 1869
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->C()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->I:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g(Landroid/support/v7/widget/RecyclerView$t;)I
    .locals 1

    .prologue
    .line 2244
    invoke-direct {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->j(Landroid/support/v7/widget/RecyclerView$t;)I

    move-result v0

    .line 2248
    return v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 1874
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->D()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->I:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAlignContent()I
    .locals 1

    .prologue
    .line 346
    const/4 v0, 0x5

    return v0
.end method

.method public getAlignItems()I
    .locals 1

    .prologue
    .line 328
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->f:I

    return v0
.end method

.method public getFlexDirection()I
    .locals 1

    .prologue
    .line 269
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->c:I

    return v0
.end method

.method public getFlexItemCount()I
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->l:Landroid/support/v7/widget/RecyclerView$t;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$t;->e()I

    move-result v0

    return v0
.end method

.method public getFlexLinesInternal()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/flexbox/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 508
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    return-object v0
.end method

.method public getFlexWrap()I
    .locals 1

    .prologue
    .line 290
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->d:I

    return v0
.end method

.method public getLargestMainSize()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 479
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 487
    :goto_0
    return v0

    .line 482
    :cond_0
    const/high16 v1, -0x80000000

    .line 483
    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_1

    .line 484
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/flexbox/c;

    .line 485
    iget v0, v0, Lcom/google/android/flexbox/c;->e:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 483
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move v0, v1

    .line 487
    goto :goto_0
.end method

.method public getSumOfCrossSize()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 492
    .line 493
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 494
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/flexbox/c;

    .line 496
    iget v0, v0, Lcom/google/android/flexbox/c;->g:I

    add-int/2addr v2, v0

    .line 493
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 498
    :cond_0
    return v2
.end method

.method public h()I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 2443
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->z()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x0

    invoke-direct {p0, v1, v0, v2}, Lcom/google/android/flexbox/FlexboxLayoutManager;->a(IIZ)Landroid/view/View;

    move-result-object v1

    .line 2444
    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->d(Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method

.method public h(Landroid/support/v7/widget/RecyclerView$t;)I
    .locals 1

    .prologue
    .line 2253
    invoke-direct {p0, p1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->j(Landroid/support/v7/widget/RecyclerView$t;)I

    move-result v0

    .line 2257
    return v0
.end method

.method public m(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 295
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 296
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "wrap_reverse is not supported in FlexboxLayoutManager"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 299
    :cond_0
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->d:I

    if-eq v0, p1, :cond_3

    .line 300
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->d:I

    if-eqz v0, :cond_1

    if-nez p1, :cond_2

    .line 301
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->x()V

    .line 302
    invoke-direct {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->o()V

    .line 304
    :cond_2
    iput p1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->d:I

    .line 305
    iput-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->o:Landroid/support/v7/widget/ba;

    .line 306
    iput-object v1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->z:Landroid/support/v7/widget/ba;

    .line 307
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->r()V

    .line 309
    :cond_3
    return-void
.end method

.method public n(I)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 333
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->f:I

    if-eq v0, p1, :cond_2

    .line 334
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->f:I

    if-eq v0, v1, :cond_0

    if-ne p1, v1, :cond_1

    .line 335
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->x()V

    .line 336
    invoke-direct {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->o()V

    .line 338
    :cond_1
    iput p1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->f:I

    .line 339
    invoke-virtual {p0}, Lcom/google/android/flexbox/FlexboxLayoutManager;->r()V

    .line 341
    :cond_2
    return-void
.end method

.method public setFlexLines(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/flexbox/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 503
    iput-object p1, p0, Lcom/google/android/flexbox/FlexboxLayoutManager;->i:Ljava/util/List;

    .line 504
    return-void
.end method
