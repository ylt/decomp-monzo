.class public final Lcom/google/android/flexbox/e$a;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/flexbox/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# static fields
.field public static final FlexboxLayout:[I

.field public static final FlexboxLayout_Layout:[I

.field public static final FlexboxLayout_Layout_layout_alignSelf:I = 0x4

.field public static final FlexboxLayout_Layout_layout_flexBasisPercent:I = 0x3

.field public static final FlexboxLayout_Layout_layout_flexGrow:I = 0x1

.field public static final FlexboxLayout_Layout_layout_flexShrink:I = 0x2

.field public static final FlexboxLayout_Layout_layout_maxHeight:I = 0x8

.field public static final FlexboxLayout_Layout_layout_maxWidth:I = 0x7

.field public static final FlexboxLayout_Layout_layout_minHeight:I = 0x6

.field public static final FlexboxLayout_Layout_layout_minWidth:I = 0x5

.field public static final FlexboxLayout_Layout_layout_order:I = 0x0

.field public static final FlexboxLayout_Layout_layout_wrapBefore:I = 0x9

.field public static final FlexboxLayout_alignContent:I = 0x4

.field public static final FlexboxLayout_alignItems:I = 0x3

.field public static final FlexboxLayout_dividerDrawable:I = 0x5

.field public static final FlexboxLayout_dividerDrawableHorizontal:I = 0x6

.field public static final FlexboxLayout_dividerDrawableVertical:I = 0x7

.field public static final FlexboxLayout_flexDirection:I = 0x0

.field public static final FlexboxLayout_flexWrap:I = 0x1

.field public static final FlexboxLayout_justifyContent:I = 0x2

.field public static final FlexboxLayout_showDivider:I = 0x8

.field public static final FlexboxLayout_showDividerHorizontal:I = 0x9

.field public static final FlexboxLayout_showDividerVertical:I = 0xa

.field public static final FontFamily:[I

.field public static final FontFamilyFont:[I

.field public static final FontFamilyFont_font:I = 0x1

.field public static final FontFamilyFont_fontStyle:I = 0x0

.field public static final FontFamilyFont_fontWeight:I = 0x2

.field public static final FontFamily_fontProviderAuthority:I = 0x0

.field public static final FontFamily_fontProviderCerts:I = 0x3

.field public static final FontFamily_fontProviderFetchStrategy:I = 0x4

.field public static final FontFamily_fontProviderFetchTimeout:I = 0x5

.field public static final FontFamily_fontProviderPackage:I = 0x1

.field public static final FontFamily_fontProviderQuery:I = 0x2

.field public static final RecyclerView:[I

.field public static final RecyclerView_android_descendantFocusability:I = 0x1

.field public static final RecyclerView_android_orientation:I = 0x0

.field public static final RecyclerView_fastScrollEnabled:I = 0x6

.field public static final RecyclerView_fastScrollHorizontalThumbDrawable:I = 0x9

.field public static final RecyclerView_fastScrollHorizontalTrackDrawable:I = 0xa

.field public static final RecyclerView_fastScrollVerticalThumbDrawable:I = 0x7

.field public static final RecyclerView_fastScrollVerticalTrackDrawable:I = 0x8

.field public static final RecyclerView_layoutManager:I = 0x2

.field public static final RecyclerView_reverseLayout:I = 0x4

.field public static final RecyclerView_spanCount:I = 0x3

.field public static final RecyclerView_stackFromEnd:I = 0x5


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0xb

    .line 173
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/flexbox/e$a;->FlexboxLayout:[I

    .line 185
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/flexbox/e$a;->FlexboxLayout_Layout:[I

    .line 196
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/flexbox/e$a;->FontFamily:[I

    .line 203
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/google/android/flexbox/e$a;->FontFamilyFont:[I

    .line 207
    new-array v0, v1, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/google/android/flexbox/e$a;->RecyclerView:[I

    return-void

    .line 173
    :array_0
    .array-data 4
        0x7f01015f
        0x7f010160
        0x7f010161
        0x7f010162
        0x7f010163
        0x7f010164
        0x7f010165
        0x7f010166
        0x7f010167
        0x7f010168
        0x7f010169
    .end array-data

    .line 185
    :array_1
    .array-data 4
        0x7f01016a
        0x7f01016b
        0x7f01016c
        0x7f01016d
        0x7f01016e
        0x7f01016f
        0x7f010170
        0x7f010171
        0x7f010172
        0x7f010173
    .end array-data

    .line 196
    :array_2
    .array-data 4
        0x7f01017a
        0x7f01017b
        0x7f01017c
        0x7f01017d
        0x7f01017e
        0x7f01017f
    .end array-data

    .line 203
    :array_3
    .array-data 4
        0x7f010180
        0x7f010181
        0x7f010182
    .end array-data

    .line 207
    :array_4
    .array-data 4
        0x10100c4
        0x10100f1
        0x7f0101da
        0x7f0101db
        0x7f0101dc
        0x7f0101dd
        0x7f0101de
        0x7f0101df
        0x7f0101e0
        0x7f0101e1
        0x7f0101e2
    .end array-data
.end method
