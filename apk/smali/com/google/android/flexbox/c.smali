.class public Lcom/google/android/flexbox/c;
.super Ljava/lang/Object;
.source "FlexLine.java"


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:I

.field f:I

.field g:I

.field h:I

.field i:I

.field j:F

.field k:F

.field l:I

.field m:I

.field n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field o:I

.field p:I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const v1, 0x7fffffff

    const/high16 v0, -0x80000000

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput v1, p0, Lcom/google/android/flexbox/c;->a:I

    .line 36
    iput v1, p0, Lcom/google/android/flexbox/c;->b:I

    .line 38
    iput v0, p0, Lcom/google/android/flexbox/c;->c:I

    .line 40
    iput v0, p0, Lcom/google/android/flexbox/c;->d:I

    .line 87
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/flexbox/c;->n:Ljava/util/List;

    .line 32
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lcom/google/android/flexbox/c;->g:I

    return v0
.end method

.method a(Landroid/view/View;IIII)V
    .locals 4

    .prologue
    .line 158
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/flexbox/b;

    .line 159
    iget v1, p0, Lcom/google/android/flexbox/c;->a:I

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-interface {v0}, Lcom/google/android/flexbox/b;->m()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int/2addr v2, p2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/flexbox/c;->a:I

    .line 160
    iget v1, p0, Lcom/google/android/flexbox/c;->b:I

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-interface {v0}, Lcom/google/android/flexbox/b;->n()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int/2addr v2, p3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/flexbox/c;->b:I

    .line 161
    iget v1, p0, Lcom/google/android/flexbox/c;->c:I

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v2

    invoke-interface {v0}, Lcom/google/android/flexbox/b;->o()I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v2, p4

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/flexbox/c;->c:I

    .line 162
    iget v1, p0, Lcom/google/android/flexbox/c;->d:I

    .line 163
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v2

    invoke-interface {v0}, Lcom/google/android/flexbox/b;->p()I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v0, p5

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/flexbox/c;->d:I

    .line 164
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/google/android/flexbox/c;->h:I

    return v0
.end method

.method public c()I
    .locals 2

    .prologue
    .line 121
    iget v0, p0, Lcom/google/android/flexbox/c;->h:I

    iget v1, p0, Lcom/google/android/flexbox/c;->i:I

    sub-int/2addr v0, v1

    return v0
.end method
