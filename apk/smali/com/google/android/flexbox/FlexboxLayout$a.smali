.class public Lcom/google/android/flexbox/FlexboxLayout$a;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source "FlexboxLayout.java"

# interfaces
.implements Lcom/google/android/flexbox/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/flexbox/FlexboxLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/flexbox/FlexboxLayout$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:I

.field private b:F

.field private c:F

.field private d:I

.field private e:F

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1822
    new-instance v0, Lcom/google/android/flexbox/FlexboxLayout$a$1;

    invoke-direct {v0}, Lcom/google/android/flexbox/FlexboxLayout$a$1;-><init>()V

    sput-object v0, Lcom/google/android/flexbox/FlexboxLayout$a;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/high16 v6, -0x40800000    # -1.0f

    const/4 v5, 0x0

    const v4, 0xffffff

    const/4 v3, 0x1

    .line 1581
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1533
    iput v3, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->a:I

    .line 1538
    iput v2, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->b:F

    .line 1543
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->c:F

    .line 1548
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->d:I

    .line 1553
    iput v6, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->e:F

    .line 1568
    iput v4, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->h:I

    .line 1573
    iput v4, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->i:I

    .line 1583
    sget-object v0, Lcom/google/android/flexbox/e$a;->FlexboxLayout_Layout:[I

    .line 1584
    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1585
    sget v1, Lcom/google/android/flexbox/e$a;->FlexboxLayout_Layout_layout_order:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->a:I

    .line 1586
    sget v1, Lcom/google/android/flexbox/e$a;->FlexboxLayout_Layout_layout_flexGrow:I

    .line 1587
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->b:F

    .line 1588
    sget v1, Lcom/google/android/flexbox/e$a;->FlexboxLayout_Layout_layout_flexShrink:I

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->c:F

    .line 1590
    sget v1, Lcom/google/android/flexbox/e$a;->FlexboxLayout_Layout_layout_alignSelf:I

    const/4 v2, -0x1

    .line 1591
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->d:I

    .line 1592
    sget v1, Lcom/google/android/flexbox/e$a;->FlexboxLayout_Layout_layout_flexBasisPercent:I

    .line 1593
    invoke-virtual {v0, v1, v3, v3, v6}, Landroid/content/res/TypedArray;->getFraction(IIIF)F

    move-result v1

    iput v1, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->e:F

    .line 1595
    sget v1, Lcom/google/android/flexbox/e$a;->FlexboxLayout_Layout_layout_minWidth:I

    .line 1596
    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->f:I

    .line 1597
    sget v1, Lcom/google/android/flexbox/e$a;->FlexboxLayout_Layout_layout_minHeight:I

    .line 1598
    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->g:I

    .line 1599
    sget v1, Lcom/google/android/flexbox/e$a;->FlexboxLayout_Layout_layout_maxWidth:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->h:I

    .line 1601
    sget v1, Lcom/google/android/flexbox/e$a;->FlexboxLayout_Layout_layout_maxHeight:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->i:I

    .line 1603
    sget v1, Lcom/google/android/flexbox/e$a;->FlexboxLayout_Layout_layout_wrapBefore:I

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->j:Z

    .line 1604
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1605
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const v3, 0xffffff

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1803
    invoke-direct {p0, v1, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 1533
    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->a:I

    .line 1538
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->b:F

    .line 1543
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->c:F

    .line 1548
    const/4 v2, -0x1

    iput v2, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->d:I

    .line 1553
    const/high16 v2, -0x40800000    # -1.0f

    iput v2, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->e:F

    .line 1568
    iput v3, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->h:I

    .line 1573
    iput v3, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->i:I

    .line 1804
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->a:I

    .line 1805
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v2

    iput v2, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->b:F

    .line 1806
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v2

    iput v2, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->c:F

    .line 1807
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->d:I

    .line 1808
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v2

    iput v2, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->e:F

    .line 1809
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->f:I

    .line 1810
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->g:I

    .line 1811
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->h:I

    .line 1812
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->i:I

    .line 1813
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->j:Z

    .line 1814
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->bottomMargin:I

    .line 1815
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->leftMargin:I

    .line 1816
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->rightMargin:I

    .line 1817
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->topMargin:I

    .line 1818
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->height:I

    .line 1819
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->width:I

    .line 1820
    return-void

    :cond_0
    move v0, v1

    .line 1813
    goto :goto_0
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    const v1, 0xffffff

    .line 1623
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1533
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->a:I

    .line 1538
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->b:F

    .line 1543
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->c:F

    .line 1548
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->d:I

    .line 1553
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->e:F

    .line 1568
    iput v1, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->h:I

    .line 1573
    iput v1, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->i:I

    .line 1624
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .locals 2

    .prologue
    const v1, 0xffffff

    .line 1631
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 1533
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->a:I

    .line 1538
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->b:F

    .line 1543
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->c:F

    .line 1548
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->d:I

    .line 1553
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->e:F

    .line 1568
    iput v1, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->h:I

    .line 1573
    iput v1, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->i:I

    .line 1632
    return-void
.end method

.method public constructor <init>(Lcom/google/android/flexbox/FlexboxLayout$a;)V
    .locals 2

    .prologue
    const v1, 0xffffff

    .line 1608
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 1533
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->a:I

    .line 1538
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->b:F

    .line 1543
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->c:F

    .line 1548
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->d:I

    .line 1553
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->e:F

    .line 1568
    iput v1, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->h:I

    .line 1573
    iput v1, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->i:I

    .line 1610
    iget v0, p1, Lcom/google/android/flexbox/FlexboxLayout$a;->a:I

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->a:I

    .line 1611
    iget v0, p1, Lcom/google/android/flexbox/FlexboxLayout$a;->b:F

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->b:F

    .line 1612
    iget v0, p1, Lcom/google/android/flexbox/FlexboxLayout$a;->c:F

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->c:F

    .line 1613
    iget v0, p1, Lcom/google/android/flexbox/FlexboxLayout$a;->d:I

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->d:I

    .line 1614
    iget v0, p1, Lcom/google/android/flexbox/FlexboxLayout$a;->e:F

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->e:F

    .line 1615
    iget v0, p1, Lcom/google/android/flexbox/FlexboxLayout$a;->f:I

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->f:I

    .line 1616
    iget v0, p1, Lcom/google/android/flexbox/FlexboxLayout$a;->g:I

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->g:I

    .line 1617
    iget v0, p1, Lcom/google/android/flexbox/FlexboxLayout$a;->h:I

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->h:I

    .line 1618
    iget v0, p1, Lcom/google/android/flexbox/FlexboxLayout$a;->i:I

    iput v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->i:I

    .line 1619
    iget-boolean v0, p1, Lcom/google/android/flexbox/FlexboxLayout$a;->j:Z

    iput-boolean v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->j:Z

    .line 1620
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 1636
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->width:I

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 1646
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->height:I

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 1656
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->a:I

    return v0
.end method

.method public d()F
    .locals 1

    .prologue
    .line 1666
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->b:F

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 1777
    const/4 v0, 0x0

    return v0
.end method

.method public e()F
    .locals 1

    .prologue
    .line 1676
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->c:F

    return v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 1687
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->d:I

    return v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 1697
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->f:I

    return v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 1707
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->g:I

    return v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 1717
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->h:I

    return v0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 1727
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->i:I

    return v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 1737
    iget-boolean v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->j:Z

    return v0
.end method

.method public l()F
    .locals 1

    .prologue
    .line 1747
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->e:F

    return v0
.end method

.method public m()I
    .locals 1

    .prologue
    .line 1757
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->leftMargin:I

    return v0
.end method

.method public n()I
    .locals 1

    .prologue
    .line 1762
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->topMargin:I

    return v0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 1767
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->rightMargin:I

    return v0
.end method

.method public p()I
    .locals 1

    .prologue
    .line 1772
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->bottomMargin:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1782
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1783
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->b:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1784
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->c:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1785
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1786
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->e:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1787
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1788
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1789
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1790
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->i:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1791
    iget-boolean v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->j:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1792
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->bottomMargin:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1793
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->leftMargin:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1794
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->rightMargin:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1795
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->topMargin:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1796
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->height:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1797
    iget v0, p0, Lcom/google/android/flexbox/FlexboxLayout$a;->width:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1798
    return-void

    .line 1791
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
