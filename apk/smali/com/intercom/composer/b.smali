.class public Lcom/intercom/composer/b;
.super Landroid/support/v4/app/Fragment;
.source "ComposerFragment.java"


# instance fields
.field a:Lcom/intercom/composer/c;

.field b:Lcom/intercom/composer/ComposerView;

.field c:Ljava/lang/String;

.field d:Lcom/intercom/composer/b/b;

.field private e:Lcom/intercom/composer/f;

.field private f:Z

.field private g:I

.field private h:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 42
    new-instance v0, Lcom/intercom/composer/b$1;

    invoke-direct {v0, p0}, Lcom/intercom/composer/b$1;-><init>(Lcom/intercom/composer/b;)V

    iput-object v0, p0, Lcom/intercom/composer/b;->h:Ljava/lang/Runnable;

    return-void
.end method

.method public static a(Ljava/lang/String;ZI)Lcom/intercom/composer/b;
    .locals 3

    .prologue
    .line 51
    new-instance v0, Lcom/intercom/composer/b;

    invoke-direct {v0}, Lcom/intercom/composer/b;-><init>()V

    .line 52
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 53
    const-string v2, "initial_input_identifier"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const-string v2, "show_keyboard_for_initial_input"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 55
    const-string v2, "theme_color"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 56
    invoke-virtual {v0, v1}, Lcom/intercom/composer/b;->setArguments(Landroid/os/Bundle;)V

    .line 57
    return-object v0
.end method

.method private a(Lcom/intercom/composer/b/b;)Z
    .locals 1

    .prologue
    .line 185
    instance-of v0, p1, Lcom/intercom/composer/b/c/b;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/intercom/composer/b/b;)Z
    .locals 1

    .prologue
    .line 189
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/intercom/composer/b/b;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 151
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 161
    :goto_0
    return-object v0

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/intercom/composer/b;->a:Lcom/intercom/composer/c;

    invoke-interface {v0}, Lcom/intercom/composer/c;->getInputs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intercom/composer/b/b;

    .line 156
    invoke-virtual {v0}, Lcom/intercom/composer/b/b;->getUniqueIdentifier()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 161
    goto :goto_0
.end method

.method a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 113
    iget-object v0, p0, Lcom/intercom/composer/b;->d:Lcom/intercom/composer/b/b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/intercom/composer/b;->b:Lcom/intercom/composer/ComposerView;

    iget-object v1, p0, Lcom/intercom/composer/b;->d:Lcom/intercom/composer/b/b;

    invoke-virtual {v0, v1, v2, v3}, Lcom/intercom/composer/ComposerView;->a(Lcom/intercom/composer/b/b;ZZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 125
    :cond_0
    :goto_0
    return-void

    .line 117
    :cond_1
    iget-object v0, p0, Lcom/intercom/composer/b;->a:Lcom/intercom/composer/c;

    invoke-interface {v0}, Lcom/intercom/composer/c;->getInputs()Ljava/util/List;

    move-result-object v1

    .line 118
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/intercom/composer/b;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/intercom/composer/b;->a(Ljava/lang/String;)Lcom/intercom/composer/b/b;

    move-result-object v0

    .line 120
    if-nez v0, :cond_2

    .line 121
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intercom/composer/b/b;

    .line 123
    :cond_2
    iget-object v1, p0, Lcom/intercom/composer/b;->b:Lcom/intercom/composer/ComposerView;

    iget-boolean v2, p0, Lcom/intercom/composer/b;->f:Z

    invoke-virtual {v1, v0, v2, v3}, Lcom/intercom/composer/ComposerView;->a(Lcom/intercom/composer/b/b;ZZ)Z

    goto :goto_0
.end method

.method public a(Lcom/intercom/composer/c;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/intercom/composer/b;->a:Lcom/intercom/composer/c;

    .line 62
    return-void
.end method

.method public a(Lcom/intercom/composer/f;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/intercom/composer/b;->e:Lcom/intercom/composer/f;

    .line 66
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 169
    invoke-virtual {p0, p1}, Lcom/intercom/composer/b;->a(Ljava/lang/String;)Lcom/intercom/composer/b/b;

    move-result-object v0

    .line 170
    if-eqz v0, :cond_0

    .line 171
    iget-object v1, p0, Lcom/intercom/composer/b;->b:Lcom/intercom/composer/ComposerView;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, p2, v2}, Lcom/intercom/composer/ComposerView;->a(Lcom/intercom/composer/b/b;ZZ)Z

    .line 173
    :cond_0
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/intercom/composer/b;->b:Lcom/intercom/composer/ComposerView;

    invoke-virtual {v0}, Lcom/intercom/composer/ComposerView;->a()Z

    move-result v0

    return v0
.end method

.method public c()Lcom/intercom/composer/b/b;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/intercom/composer/b;->b:Lcom/intercom/composer/ComposerView;

    invoke-virtual {v0}, Lcom/intercom/composer/ComposerView;->getSelectedInput()Lcom/intercom/composer/b/b;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/intercom/composer/b;->c()Lcom/intercom/composer/b/b;

    move-result-object v0

    .line 181
    invoke-direct {p0, v0}, Lcom/intercom/composer/b;->b(Lcom/intercom/composer/b/b;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/intercom/composer/b;->a(Lcom/intercom/composer/b/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 69
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/content/Context;)V

    .line 70
    instance-of v0, p1, Lcom/intercom/composer/c;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 71
    check-cast v0, Lcom/intercom/composer/c;

    iput-object v0, p0, Lcom/intercom/composer/b;->a:Lcom/intercom/composer/c;

    .line 73
    :cond_0
    instance-of v0, p1, Lcom/intercom/composer/f;

    if-eqz v0, :cond_1

    .line 74
    check-cast p1, Lcom/intercom/composer/f;

    iput-object p1, p0, Lcom/intercom/composer/b;->e:Lcom/intercom/composer/f;

    .line 76
    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 79
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 80
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/intercom/composer/b;->setRetainInstance(Z)V

    .line 81
    invoke-virtual {p0}, Lcom/intercom/composer/b;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "initial_input_identifier"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/intercom/composer/b;->c:Ljava/lang/String;

    .line 82
    invoke-virtual {p0}, Lcom/intercom/composer/b;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "show_keyboard_for_initial_input"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/intercom/composer/b;->f:Z

    .line 83
    invoke-virtual {p0}, Lcom/intercom/composer/b;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "theme_color"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/intercom/composer/b;->g:I

    .line 84
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 88
    sget v0, Lcom/intercom/composer/g$f;->intercom_composer_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/intercom/composer/ComposerView;

    iput-object v0, p0, Lcom/intercom/composer/b;->b:Lcom/intercom/composer/ComposerView;

    .line 89
    iget-object v0, p0, Lcom/intercom/composer/b;->b:Lcom/intercom/composer/ComposerView;

    invoke-virtual {p0}, Lcom/intercom/composer/b;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/intercom/composer/b;->g:I

    invoke-virtual {v0, v1, v2}, Lcom/intercom/composer/ComposerView;->a(Landroid/content/Context;I)V

    .line 90
    iget-object v0, p0, Lcom/intercom/composer/b;->b:Lcom/intercom/composer/ComposerView;

    invoke-virtual {p0}, Lcom/intercom/composer/b;->getChildFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/intercom/composer/ComposerView;->setFragmentManager(Landroid/support/v4/app/n;)V

    .line 91
    iget-object v0, p0, Lcom/intercom/composer/b;->b:Lcom/intercom/composer/ComposerView;

    iget-object v1, p0, Lcom/intercom/composer/b;->a:Lcom/intercom/composer/c;

    invoke-interface {v1}, Lcom/intercom/composer/c;->getInputs()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/intercom/composer/ComposerView;->setInputs(Ljava/util/List;)V

    .line 92
    iget-object v0, p0, Lcom/intercom/composer/b;->b:Lcom/intercom/composer/ComposerView;

    new-instance v1, Lcom/intercom/composer/b$2;

    invoke-direct {v1, p0}, Lcom/intercom/composer/b$2;-><init>(Lcom/intercom/composer/b;)V

    invoke-virtual {v0, v1}, Lcom/intercom/composer/ComposerView;->setOnSendButtonClickListener(Lcom/intercom/composer/b/c/a/a;)V

    .line 101
    iget-object v0, p0, Lcom/intercom/composer/b;->e:Lcom/intercom/composer/f;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/intercom/composer/b;->b:Lcom/intercom/composer/ComposerView;

    iget-object v1, p0, Lcom/intercom/composer/b;->e:Lcom/intercom/composer/f;

    invoke-virtual {v0, v1}, Lcom/intercom/composer/ComposerView;->setInputSelectedListener(Lcom/intercom/composer/f;)V

    .line 104
    :cond_0
    new-instance v0, Lcom/intercom/composer/pager/a;

    .line 105
    invoke-virtual {p0}, Lcom/intercom/composer/b;->getChildFragmentManager()Landroid/support/v4/app/n;

    move-result-object v1

    iget-object v2, p0, Lcom/intercom/composer/b;->b:Lcom/intercom/composer/ComposerView;

    invoke-virtual {v2}, Lcom/intercom/composer/ComposerView;->getInputs()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/intercom/composer/pager/a;-><init>(Landroid/support/v4/app/n;Ljava/util/List;)V

    .line 106
    iget-object v1, p0, Lcom/intercom/composer/b;->b:Lcom/intercom/composer/ComposerView;

    invoke-virtual {v1, v0}, Lcom/intercom/composer/ComposerView;->setComposerPagerAdapter(Lcom/intercom/composer/pager/a;)V

    .line 107
    iget-object v0, p0, Lcom/intercom/composer/b;->b:Lcom/intercom/composer/ComposerView;

    new-instance v1, Lcom/intercom/composer/a/c;

    invoke-virtual {p0}, Lcom/intercom/composer/b;->getActivity()Landroid/support/v4/app/j;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/intercom/composer/a/c;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0, v1}, Lcom/intercom/composer/ComposerView;->setEditTextLayoutAnimationListener(Lcom/intercom/composer/a/c;)V

    .line 108
    iget-object v0, p0, Lcom/intercom/composer/b;->b:Lcom/intercom/composer/ComposerView;

    iget-object v1, p0, Lcom/intercom/composer/b;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/intercom/composer/ComposerView;->post(Ljava/lang/Runnable;)Z

    .line 109
    iget-object v0, p0, Lcom/intercom/composer/b;->b:Lcom/intercom/composer/ComposerView;

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/intercom/composer/b;->b:Lcom/intercom/composer/ComposerView;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/intercom/composer/b;->b:Lcom/intercom/composer/ComposerView;

    invoke-virtual {v0}, Lcom/intercom/composer/ComposerView;->b()V

    .line 136
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 137
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/intercom/composer/b;->b:Lcom/intercom/composer/ComposerView;

    invoke-virtual {v0}, Lcom/intercom/composer/ComposerView;->getSelectedInput()Lcom/intercom/composer/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/intercom/composer/b;->d:Lcom/intercom/composer/b/b;

    .line 129
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 130
    return-void
.end method
