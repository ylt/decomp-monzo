.class public Lcom/intercom/composer/c/a;
.super Ljava/lang/Object;
.source "KeyboardHelper.java"

# interfaces
.implements Lcom/intercom/composer/c/c;


# instance fields
.field private final a:Lcom/intercom/composer/c/b;

.field private final b:Landroid/view/Window;

.field private final c:Landroid/view/View;

.field private final d:Landroid/view/View;

.field private final e:Lcom/intercom/composer/c/d;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/intercom/composer/c/d;Landroid/view/View;Landroid/view/View;)V
    .locals 6

    .prologue
    .line 22
    new-instance v1, Lcom/intercom/composer/c/b;

    invoke-direct {v1, p1, p2}, Lcom/intercom/composer/c/b;-><init>(Landroid/app/Activity;Lcom/intercom/composer/c/d;)V

    .line 23
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    move-object v0, p0

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    .line 22
    invoke-direct/range {v0 .. v5}, Lcom/intercom/composer/c/a;-><init>(Lcom/intercom/composer/c/b;Lcom/intercom/composer/c/d;Landroid/view/Window;Landroid/view/View;Landroid/view/View;)V

    .line 24
    return-void
.end method

.method constructor <init>(Lcom/intercom/composer/c/b;Lcom/intercom/composer/c/d;Landroid/view/Window;Landroid/view/View;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    if-nez p5, :cond_0

    .line 29
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "behindKeyboardView can not be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 32
    :cond_0
    if-nez p4, :cond_1

    .line 33
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "editText can not be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :cond_1
    iput-object p4, p0, Lcom/intercom/composer/c/a;->c:Landroid/view/View;

    .line 36
    iput-object p5, p0, Lcom/intercom/composer/c/a;->d:Landroid/view/View;

    .line 37
    iput-object p3, p0, Lcom/intercom/composer/c/a;->b:Landroid/view/Window;

    .line 38
    iget-object v0, p0, Lcom/intercom/composer/c/a;->b:Landroid/view/Window;

    const/16 v1, 0x13

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 39
    iput-object p2, p0, Lcom/intercom/composer/c/a;->e:Lcom/intercom/composer/c/d;

    .line 40
    iput-object p1, p0, Lcom/intercom/composer/c/a;->a:Lcom/intercom/composer/c/b;

    .line 41
    iget-object v0, p0, Lcom/intercom/composer/c/a;->a:Lcom/intercom/composer/c/b;

    invoke-virtual {v0, p0}, Lcom/intercom/composer/c/b;->a(Lcom/intercom/composer/c/c;)V

    .line 42
    return-void
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/intercom/composer/c/a;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/16 v3, 0x20

    .line 70
    iget-object v0, p0, Lcom/intercom/composer/c/a;->e:Lcom/intercom/composer/c/d;

    invoke-virtual {v0}, Lcom/intercom/composer/c/d;->a()I

    move-result v0

    .line 71
    iget-object v1, p0, Lcom/intercom/composer/c/a;->a:Lcom/intercom/composer/c/b;

    invoke-virtual {v1}, Lcom/intercom/composer/c/b;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 73
    iget-object v1, p0, Lcom/intercom/composer/c/a;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget-object v2, p0, Lcom/intercom/composer/c/a;->a:Lcom/intercom/composer/c/b;

    invoke-virtual {v2, v0}, Lcom/intercom/composer/c/b;->a(I)I

    move-result v0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 74
    iget-object v0, p0, Lcom/intercom/composer/c/a;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 75
    iget-object v0, p0, Lcom/intercom/composer/c/a;->b:Landroid/view/Window;

    invoke-virtual {v0, v3}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 76
    iget-object v0, p0, Lcom/intercom/composer/c/a;->a:Lcom/intercom/composer/c/b;

    invoke-virtual {v0}, Lcom/intercom/composer/c/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/intercom/composer/c/a;->a:Lcom/intercom/composer/c/b;

    iget-object v1, p0, Lcom/intercom/composer/c/a;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/intercom/composer/c/b;->a(Landroid/view/View;)V

    .line 85
    :cond_0
    :goto_0
    return-void

    .line 79
    :cond_1
    invoke-direct {p0}, Lcom/intercom/composer/c/a;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 81
    iget-object v1, p0, Lcom/intercom/composer/c/a;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget-object v2, p0, Lcom/intercom/composer/c/a;->a:Lcom/intercom/composer/c/b;

    invoke-virtual {v2, v0}, Lcom/intercom/composer/c/b;->a(I)I

    move-result v0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 82
    iget-object v0, p0, Lcom/intercom/composer/c/a;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 83
    iget-object v0, p0, Lcom/intercom/composer/c/a;->b:Landroid/view/Window;

    invoke-virtual {v0, v3}, Landroid/view/Window;->setSoftInputMode(I)V

    goto :goto_0
.end method

.method public a(ZI)V
    .locals 2

    .prologue
    const/16 v1, 0x10

    .line 45
    if-eqz p1, :cond_1

    .line 47
    iget-object v0, p0, Lcom/intercom/composer/c/a;->b:Landroid/view/Window;

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 48
    invoke-direct {p0}, Lcom/intercom/composer/c/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/intercom/composer/c/a;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, 0x0

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 51
    iget-object v0, p0, Lcom/intercom/composer/c/a;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 63
    :cond_0
    :goto_0
    return-void

    .line 55
    :cond_1
    invoke-direct {p0}, Lcom/intercom/composer/c/a;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 57
    iget-object v0, p0, Lcom/intercom/composer/c/a;->b:Landroid/view/Window;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    goto :goto_0

    .line 60
    :cond_2
    iget-object v0, p0, Lcom/intercom/composer/c/a;->b:Landroid/view/Window;

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    goto :goto_0
.end method

.method public b()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 91
    invoke-direct {p0}, Lcom/intercom/composer/c/a;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 92
    iget-object v1, p0, Lcom/intercom/composer/c/a;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 93
    iget-object v0, p0, Lcom/intercom/composer/c/a;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 94
    iget-object v0, p0, Lcom/intercom/composer/c/a;->b:Landroid/view/Window;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 95
    const/4 v0, 0x1

    .line 97
    :cond_0
    return v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/intercom/composer/c/a;->a:Lcom/intercom/composer/c/b;

    invoke-virtual {v0}, Lcom/intercom/composer/c/b;->b()V

    .line 102
    return-void
.end method
