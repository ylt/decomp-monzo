.class Lcom/intercom/composer/c/b;
.super Ljava/lang/Object;
.source "KeyboardManager.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Lcom/intercom/composer/c/c;


# instance fields
.field a:Z

.field b:I

.field private final c:Landroid/view/Window;

.field private final d:Landroid/view/WindowManager;

.field private final e:Landroid/view/inputmethod/InputMethodManager;

.field private final f:Landroid/content/SharedPreferences;

.field private final g:Lcom/intercom/composer/c/d;

.field private h:Lcom/intercom/composer/c/c;


# direct methods
.method constructor <init>(Landroid/app/Activity;Lcom/intercom/composer/c/d;)V
    .locals 6

    .prologue
    .line 46
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    const-string v0, "input_method"

    .line 47
    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/inputmethod/InputMethodManager;

    const-string v0, "keyboard"

    const/4 v4, 0x0

    .line 48
    invoke-virtual {p1, v0, v4}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    move-object v0, p0

    move-object v5, p2

    .line 46
    invoke-direct/range {v0 .. v5}, Lcom/intercom/composer/c/b;-><init>(Landroid/view/Window;Landroid/view/WindowManager;Landroid/view/inputmethod/InputMethodManager;Landroid/content/SharedPreferences;Lcom/intercom/composer/c/d;)V

    .line 50
    return-void
.end method

.method constructor <init>(Landroid/view/Window;Landroid/view/WindowManager;Landroid/view/inputmethod/InputMethodManager;Landroid/content/SharedPreferences;Lcom/intercom/composer/c/d;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/intercom/composer/c/b;->c:Landroid/view/Window;

    .line 57
    iput-object p2, p0, Lcom/intercom/composer/c/b;->d:Landroid/view/WindowManager;

    .line 58
    iput-object p3, p0, Lcom/intercom/composer/c/b;->e:Landroid/view/inputmethod/InputMethodManager;

    .line 59
    iput-object p4, p0, Lcom/intercom/composer/c/b;->f:Landroid/content/SharedPreferences;

    .line 60
    iput-object p5, p0, Lcom/intercom/composer/c/b;->g:Lcom/intercom/composer/c/d;

    .line 61
    iget-object v0, p0, Lcom/intercom/composer/c/b;->c:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 62
    return-void
.end method

.method private c()I
    .locals 3

    .prologue
    .line 80
    iget-object v0, p0, Lcom/intercom/composer/c/b;->c:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 82
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 83
    invoke-virtual {v0, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 85
    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 86
    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v1

    .line 87
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_0

    .line 89
    invoke-direct {p0}, Lcom/intercom/composer/c/b;->d()I

    move-result v1

    sub-int/2addr v0, v1

    .line 91
    :cond_0
    return v0
.end method

.method private d()I
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 111
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 112
    iget-object v1, p0, Lcom/intercom/composer/c/b;->d:Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 113
    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 114
    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 116
    invoke-virtual {v1, v0}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V

    .line 117
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 119
    if-le v0, v2, :cond_0

    .line 120
    sub-int/2addr v0, v2

    .line 122
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a(I)I
    .locals 3

    .prologue
    .line 69
    iget-object v0, p0, Lcom/intercom/composer/c/b;->c:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 70
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 72
    sget v1, Lcom/intercom/composer/g$c;->intercom_composer_keyboard_portrait_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 73
    iget-object v1, p0, Lcom/intercom/composer/c/b;->f:Landroid/content/SharedPreferences;

    const-string v2, "keyboard_height_portrait"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 75
    :goto_0
    return v0

    :cond_0
    sget v1, Lcom/intercom/composer/g$c;->intercom_composer_keyboard_landscape_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0
.end method

.method a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 65
    iget-object v0, p0, Lcom/intercom/composer/c/b;->e:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 66
    return-void
.end method

.method a(Lcom/intercom/composer/c/c;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/intercom/composer/c/b;->h:Lcom/intercom/composer/c/c;

    .line 128
    return-void
.end method

.method public a(ZI)V
    .locals 2

    .prologue
    .line 148
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/intercom/composer/c/b;->g:Lcom/intercom/composer/c/d;

    invoke-virtual {v0}, Lcom/intercom/composer/c/d;->a()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 149
    iget-object v0, p0, Lcom/intercom/composer/c/b;->f:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "keyboard_height_portrait"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 151
    :cond_0
    return-void
.end method

.method a()Z
    .locals 1

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/intercom/composer/c/b;->c()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b()V
    .locals 3

    .prologue
    .line 99
    iget-object v0, p0, Lcom/intercom/composer/c/b;->c:Landroid/view/Window;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/intercom/composer/c/b;->c:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 101
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_1

    .line 102
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 107
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method

.method public onGlobalLayout()V
    .locals 4

    .prologue
    .line 131
    invoke-direct {p0}, Lcom/intercom/composer/c/b;->c()I

    move-result v1

    .line 132
    if-lez v1, :cond_1

    const/4 v0, 0x1

    .line 133
    :goto_0
    iget-object v2, p0, Lcom/intercom/composer/c/b;->g:Lcom/intercom/composer/c/d;

    invoke-virtual {v2}, Lcom/intercom/composer/c/d;->a()I

    move-result v2

    .line 135
    iget-boolean v3, p0, Lcom/intercom/composer/c/b;->a:Z

    if-ne v0, v3, :cond_2

    iget v3, p0, Lcom/intercom/composer/c/b;->b:I

    if-ne v2, v3, :cond_2

    .line 145
    :cond_0
    :goto_1
    return-void

    .line 132
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 139
    :cond_2
    iput-boolean v0, p0, Lcom/intercom/composer/c/b;->a:Z

    .line 140
    iput v2, p0, Lcom/intercom/composer/c/b;->b:I

    .line 141
    invoke-virtual {p0, v0, v1}, Lcom/intercom/composer/c/b;->a(ZI)V

    .line 142
    iget-object v2, p0, Lcom/intercom/composer/c/b;->h:Lcom/intercom/composer/c/c;

    if-eqz v2, :cond_0

    .line 143
    iget-object v2, p0, Lcom/intercom/composer/c/b;->h:Lcom/intercom/composer/c/c;

    invoke-interface {v2, v0, v1}, Lcom/intercom/composer/c/c;->a(ZI)V

    goto :goto_1
.end method
