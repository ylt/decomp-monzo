.class public Lcom/intercom/composer/a/e;
.super Lcom/intercom/composer/a/g;
.source "HideSendButtonAnimatorListener.java"


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/intercom/composer/pager/a;Landroid/support/v7/widget/RecyclerView$a;Lcom/intercom/composer/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/intercom/composer/b/b;",
            ">;",
            "Lcom/intercom/composer/pager/a;",
            "Landroid/support/v7/widget/RecyclerView$a;",
            "Lcom/intercom/composer/a;",
            ")V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intercom/composer/a/g;-><init>(Ljava/util/List;Lcom/intercom/composer/pager/a;Landroid/support/v7/widget/RecyclerView$a;Lcom/intercom/composer/a;)V

    .line 23
    return-void
.end method


# virtual methods
.method public bridge synthetic onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 16
    invoke-super {p0, p1}, Lcom/intercom/composer/a/g;->onAnimationCancel(Landroid/animation/Animator;)V

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 26
    invoke-super {p0, p1}, Lcom/intercom/composer/a/g;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 27
    iget-object v0, p0, Lcom/intercom/composer/a/e;->d:Lcom/intercom/composer/a;

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Lcom/intercom/composer/a;->setSendButtonVisibility(I)V

    .line 28
    iget-boolean v0, p0, Lcom/intercom/composer/a/e;->e:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/intercom/composer/a/e;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29
    iget-object v0, p0, Lcom/intercom/composer/a/e;->d:Lcom/intercom/composer/a;

    sget-object v1, Lcom/intercom/composer/a/a;->c:Lcom/intercom/composer/a/a;

    invoke-interface {v0, v1}, Lcom/intercom/composer/a;->a(Lcom/intercom/composer/a/a;)V

    .line 30
    iget-object v0, p0, Lcom/intercom/composer/a/e;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/intercom/composer/a/e;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 31
    iget-object v0, p0, Lcom/intercom/composer/a/e;->b:Lcom/intercom/composer/pager/a;

    invoke-virtual {v0}, Lcom/intercom/composer/pager/a;->c()V

    .line 32
    iget-object v0, p0, Lcom/intercom/composer/a/e;->c:Landroid/support/v7/widget/RecyclerView$a;

    iget-object v1, p0, Lcom/intercom/composer/a/e;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$a;->notifyItemRemoved(I)V

    .line 34
    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/intercom/composer/a/g;->onAnimationStart(Landroid/animation/Animator;)V

    .line 38
    iget-object v0, p0, Lcom/intercom/composer/a/e;->d:Lcom/intercom/composer/a;

    sget-object v1, Lcom/intercom/composer/a/a;->a:Lcom/intercom/composer/a/a;

    invoke-interface {v0, v1}, Lcom/intercom/composer/a;->a(Lcom/intercom/composer/a/a;)V

    .line 39
    return-void
.end method
