.class public Lcom/intercom/composer/a/b;
.super Ljava/lang/Object;
.source "EditTextLayoutAnimator.java"


# instance fields
.field a:Lcom/intercom/composer/a/a;

.field final b:Landroid/animation/Animator$AnimatorListener;

.field final c:Landroid/animation/Animator$AnimatorListener;

.field final d:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

.field private final e:Landroid/view/View;

.field private f:Lcom/intercom/composer/a/c;

.field private g:Landroid/animation/ObjectAnimator;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    sget-object v0, Lcom/intercom/composer/a/a;->d:Lcom/intercom/composer/a/a;

    iput-object v0, p0, Lcom/intercom/composer/a/b;->a:Lcom/intercom/composer/a/a;

    .line 52
    new-instance v0, Lcom/intercom/composer/a/b$1;

    invoke-direct {v0, p0}, Lcom/intercom/composer/a/b$1;-><init>(Lcom/intercom/composer/a/b;)V

    iput-object v0, p0, Lcom/intercom/composer/a/b;->b:Landroid/animation/Animator$AnimatorListener;

    .line 85
    new-instance v0, Lcom/intercom/composer/a/b$2;

    invoke-direct {v0, p0}, Lcom/intercom/composer/a/b$2;-><init>(Lcom/intercom/composer/a/b;)V

    iput-object v0, p0, Lcom/intercom/composer/a/b;->c:Landroid/animation/Animator$AnimatorListener;

    .line 102
    new-instance v0, Lcom/intercom/composer/a/b$3;

    invoke-direct {v0, p0}, Lcom/intercom/composer/a/b$3;-><init>(Lcom/intercom/composer/a/b;)V

    iput-object v0, p0, Lcom/intercom/composer/a/b;->d:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 29
    iput-object p1, p0, Lcom/intercom/composer/a/b;->e:Landroid/view/View;

    .line 30
    return-void
.end method

.method static synthetic a(Lcom/intercom/composer/a/b;)Lcom/intercom/composer/a/c;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/intercom/composer/a/b;->f:Lcom/intercom/composer/a/c;

    return-object v0
.end method

.method static synthetic b(Lcom/intercom/composer/a/b;)Landroid/view/View;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/intercom/composer/a/b;->e:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    .line 71
    iget-object v0, p0, Lcom/intercom/composer/a/b;->a:Lcom/intercom/composer/a/a;

    sget-object v1, Lcom/intercom/composer/a/a;->b:Lcom/intercom/composer/a/a;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/intercom/composer/a/b;->g:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/intercom/composer/a/b;->g:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/intercom/composer/a/b;->a:Lcom/intercom/composer/a/a;

    sget-object v1, Lcom/intercom/composer/a/a;->d:Lcom/intercom/composer/a/a;

    if-ne v0, v1, :cond_1

    .line 76
    iget-object v0, p0, Lcom/intercom/composer/a/b;->e:Landroid/view/View;

    const-string v1, "layout_marginBottom"

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/intercom/composer/a/b;->e:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    neg-int v4, v4

    int-to-float v4, v4

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/intercom/composer/a/b;->g:Landroid/animation/ObjectAnimator;

    .line 77
    iget-object v0, p0, Lcom/intercom/composer/a/b;->g:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x15e

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 78
    iget-object v0, p0, Lcom/intercom/composer/a/b;->g:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/support/v4/view/b/b;

    invoke-direct {v1}, Landroid/support/v4/view/b/b;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 79
    iget-object v0, p0, Lcom/intercom/composer/a/b;->g:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/intercom/composer/a/b;->d:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 80
    iget-object v0, p0, Lcom/intercom/composer/a/b;->g:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/intercom/composer/a/b;->c:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 81
    iget-object v0, p0, Lcom/intercom/composer/a/b;->g:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 83
    :cond_1
    return-void
.end method

.method public a(Lcom/intercom/composer/a/c;)V
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/intercom/composer/a/b;->f:Lcom/intercom/composer/a/c;

    .line 34
    return-void
.end method

.method public a(Z)V
    .locals 5

    .prologue
    .line 38
    iget-object v0, p0, Lcom/intercom/composer/a/b;->a:Lcom/intercom/composer/a/a;

    sget-object v1, Lcom/intercom/composer/a/a;->a:Lcom/intercom/composer/a/a;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/intercom/composer/a/b;->g:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/intercom/composer/a/b;->g:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/intercom/composer/a/b;->a:Lcom/intercom/composer/a/a;

    sget-object v1, Lcom/intercom/composer/a/a;->c:Lcom/intercom/composer/a/a;

    if-ne v0, v1, :cond_1

    .line 43
    iget-object v0, p0, Lcom/intercom/composer/a/b;->e:Landroid/view/View;

    const-string v1, "layout_marginBottom"

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/intercom/composer/a/b;->e:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    neg-int v4, v4

    int-to-float v4, v4

    aput v4, v2, v3

    const/4 v3, 0x1

    const/4 v4, 0x0

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/intercom/composer/a/b;->g:Landroid/animation/ObjectAnimator;

    .line 44
    iget-object v2, p0, Lcom/intercom/composer/a/b;->g:Landroid/animation/ObjectAnimator;

    if-eqz p1, :cond_2

    const-wide/16 v0, 0x15e

    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 45
    iget-object v0, p0, Lcom/intercom/composer/a/b;->g:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/support/v4/view/b/b;

    invoke-direct {v1}, Landroid/support/v4/view/b/b;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 46
    iget-object v0, p0, Lcom/intercom/composer/a/b;->g:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/intercom/composer/a/b;->d:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 47
    iget-object v0, p0, Lcom/intercom/composer/a/b;->g:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/intercom/composer/a/b;->b:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 48
    iget-object v0, p0, Lcom/intercom/composer/a/b;->g:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 50
    :cond_1
    return-void

    .line 44
    :cond_2
    const-wide/16 v0, 0x0

    goto :goto_0
.end method
