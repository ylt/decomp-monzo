.class public Lcom/intercom/composer/a/h;
.super Lcom/intercom/composer/a/g;
.source "ShowSendButtonAnimatorListener.java"


# instance fields
.field private final f:Landroid/support/v7/widget/LinearLayoutManager;


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/intercom/composer/pager/a;Landroid/support/v7/widget/RecyclerView$a;Landroid/support/v7/widget/LinearLayoutManager;Lcom/intercom/composer/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/intercom/composer/b/b;",
            ">;",
            "Lcom/intercom/composer/pager/a;",
            "Landroid/support/v7/widget/RecyclerView$a;",
            "Landroid/support/v7/widget/LinearLayoutManager;",
            "Lcom/intercom/composer/a;",
            ")V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3, p5}, Lcom/intercom/composer/a/g;-><init>(Ljava/util/List;Lcom/intercom/composer/pager/a;Landroid/support/v7/widget/RecyclerView$a;Lcom/intercom/composer/a;)V

    .line 28
    iput-object p4, p0, Lcom/intercom/composer/a/h;->f:Landroid/support/v7/widget/LinearLayoutManager;

    .line 29
    return-void
.end method


# virtual methods
.method public bridge synthetic onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/intercom/composer/a/g;->onAnimationCancel(Landroid/animation/Animator;)V

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    .line 32
    invoke-super {p0, p1}, Lcom/intercom/composer/a/g;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 33
    iget-boolean v0, p0, Lcom/intercom/composer/a/h;->e:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/intercom/composer/a/h;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/intercom/composer/a/h;->d:Lcom/intercom/composer/a;

    sget-object v1, Lcom/intercom/composer/a/a;->d:Lcom/intercom/composer/a/a;

    invoke-interface {v0, v1}, Lcom/intercom/composer/a;->a(Lcom/intercom/composer/a/a;)V

    .line 35
    iget-object v0, p0, Lcom/intercom/composer/a/h;->a:Ljava/util/List;

    new-instance v1, Lcom/intercom/composer/b/a/b;

    invoke-direct {v1}, Lcom/intercom/composer/b/a/b;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    iget-object v0, p0, Lcom/intercom/composer/a/h;->b:Lcom/intercom/composer/pager/a;

    invoke-virtual {v0}, Lcom/intercom/composer/pager/a;->c()V

    .line 38
    iget-object v0, p0, Lcom/intercom/composer/a/h;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 39
    iget-object v1, p0, Lcom/intercom/composer/a/h;->c:Landroid/support/v7/widget/RecyclerView$a;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView$a;->notifyItemInserted(I)V

    .line 41
    iget-object v1, p0, Lcom/intercom/composer/a/h;->f:Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {v1}, Landroid/support/v7/widget/LinearLayoutManager;->p()I

    move-result v1

    add-int/lit8 v2, v0, -0x1

    if-ne v1, v2, :cond_0

    .line 42
    iget-object v1, p0, Lcom/intercom/composer/a/h;->f:Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/LinearLayoutManager;->e(I)V

    .line 45
    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 48
    invoke-super {p0, p1}, Lcom/intercom/composer/a/g;->onAnimationStart(Landroid/animation/Animator;)V

    .line 49
    iget-object v0, p0, Lcom/intercom/composer/a/h;->d:Lcom/intercom/composer/a;

    sget-object v1, Lcom/intercom/composer/a/a;->b:Lcom/intercom/composer/a/a;

    invoke-interface {v0, v1}, Lcom/intercom/composer/a;->a(Lcom/intercom/composer/a/a;)V

    .line 50
    iget-object v0, p0, Lcom/intercom/composer/a/h;->d:Lcom/intercom/composer/a;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/intercom/composer/a;->setSendButtonVisibility(I)V

    .line 51
    return-void
.end method
