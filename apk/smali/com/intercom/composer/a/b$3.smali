.class Lcom/intercom/composer/a/b$3;
.super Ljava/lang/Object;
.source "EditTextLayoutAnimator.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intercom/composer/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/intercom/composer/a/b;


# direct methods
.method constructor <init>(Lcom/intercom/composer/a/b;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/intercom/composer/a/b$3;->a:Lcom/intercom/composer/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/intercom/composer/a/b$3;->a:Lcom/intercom/composer/a/b;

    invoke-static {v0}, Lcom/intercom/composer/a/b;->b(Lcom/intercom/composer/a/b;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 105
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->intValue()I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 106
    iget-object v0, p0, Lcom/intercom/composer/a/b$3;->a:Lcom/intercom/composer/a/b;

    invoke-static {v0}, Lcom/intercom/composer/a/b;->b(Lcom/intercom/composer/a/b;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 107
    return-void
.end method
