.class public final enum Lcom/intercom/composer/a/a;
.super Ljava/lang/Enum;
.source "AnimationStatus.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/intercom/composer/a/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/intercom/composer/a/a;

.field public static final enum b:Lcom/intercom/composer/a/a;

.field public static final enum c:Lcom/intercom/composer/a/a;

.field public static final enum d:Lcom/intercom/composer/a/a;

.field public static final enum e:Lcom/intercom/composer/a/a;

.field private static final synthetic f:[Lcom/intercom/composer/a/a;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5
    new-instance v0, Lcom/intercom/composer/a/a;

    const-string v1, "HIDING"

    invoke-direct {v0, v1, v2}, Lcom/intercom/composer/a/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/intercom/composer/a/a;->a:Lcom/intercom/composer/a/a;

    new-instance v0, Lcom/intercom/composer/a/a;

    const-string v1, "SHOWING"

    invoke-direct {v0, v1, v3}, Lcom/intercom/composer/a/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/intercom/composer/a/a;->b:Lcom/intercom/composer/a/a;

    new-instance v0, Lcom/intercom/composer/a/a;

    const-string v1, "HIDDEN"

    invoke-direct {v0, v1, v4}, Lcom/intercom/composer/a/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/intercom/composer/a/a;->c:Lcom/intercom/composer/a/a;

    new-instance v0, Lcom/intercom/composer/a/a;

    const-string v1, "SHOWN"

    invoke-direct {v0, v1, v5}, Lcom/intercom/composer/a/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/intercom/composer/a/a;->d:Lcom/intercom/composer/a/a;

    new-instance v0, Lcom/intercom/composer/a/a;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6}, Lcom/intercom/composer/a/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/intercom/composer/a/a;->e:Lcom/intercom/composer/a/a;

    .line 3
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/intercom/composer/a/a;

    sget-object v1, Lcom/intercom/composer/a/a;->a:Lcom/intercom/composer/a/a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/intercom/composer/a/a;->b:Lcom/intercom/composer/a/a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/intercom/composer/a/a;->c:Lcom/intercom/composer/a/a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/intercom/composer/a/a;->d:Lcom/intercom/composer/a/a;

    aput-object v1, v0, v5

    sget-object v1, Lcom/intercom/composer/a/a;->e:Lcom/intercom/composer/a/a;

    aput-object v1, v0, v6

    sput-object v0, Lcom/intercom/composer/a/a;->f:[Lcom/intercom/composer/a/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intercom/composer/a/a;
    .locals 1

    .prologue
    .line 3
    const-class v0, Lcom/intercom/composer/a/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/intercom/composer/a/a;

    return-object v0
.end method

.method public static values()[Lcom/intercom/composer/a/a;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/intercom/composer/a/a;->f:[Lcom/intercom/composer/a/a;

    invoke-virtual {v0}, [Lcom/intercom/composer/a/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/intercom/composer/a/a;

    return-object v0
.end method
