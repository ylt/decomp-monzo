.class Lcom/intercom/composer/a/f$1;
.super Ljava/lang/Object;
.source "SendButtonAnimator.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intercom/composer/a/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/intercom/composer/a/f;


# direct methods
.method constructor <init>(Lcom/intercom/composer/a/f;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/intercom/composer/a/f$1;->a:Lcom/intercom/composer/a/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/intercom/composer/a/f$1;->a:Lcom/intercom/composer/a/f;

    iget-object v1, v0, Lcom/intercom/composer/a/f;->a:Landroid/view/View;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 89
    return-void
.end method
