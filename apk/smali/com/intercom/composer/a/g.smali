.class abstract Lcom/intercom/composer/a/g;
.super Landroid/animation/AnimatorListenerAdapter;
.source "SendButtonAnimatorListener.java"


# instance fields
.field protected final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/intercom/composer/b/b;",
            ">;"
        }
    .end annotation
.end field

.field final b:Lcom/intercom/composer/pager/a;

.field final c:Landroid/support/v7/widget/RecyclerView$a;

.field final d:Lcom/intercom/composer/a;

.field e:Z


# direct methods
.method constructor <init>(Ljava/util/List;Lcom/intercom/composer/pager/a;Landroid/support/v7/widget/RecyclerView$a;Lcom/intercom/composer/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/intercom/composer/b/b;",
            ">;",
            "Lcom/intercom/composer/pager/a;",
            "Landroid/support/v7/widget/RecyclerView$a;",
            "Lcom/intercom/composer/a;",
            ")V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/intercom/composer/a/g;->a:Ljava/util/List;

    .line 28
    iput-object p2, p0, Lcom/intercom/composer/a/g;->b:Lcom/intercom/composer/pager/a;

    .line 29
    iput-object p3, p0, Lcom/intercom/composer/a/g;->c:Landroid/support/v7/widget/RecyclerView$a;

    .line 30
    iput-object p4, p0, Lcom/intercom/composer/a/g;->d:Lcom/intercom/composer/a;

    .line 31
    return-void
.end method


# virtual methods
.method a()Z
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/intercom/composer/a/g;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intercom/composer/b/b;

    .line 35
    instance-of v0, v0, Lcom/intercom/composer/b/a/b;

    if-eqz v0, :cond_0

    .line 36
    const/4 v0, 0x1

    .line 39
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 43
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationCancel(Landroid/animation/Animator;)V

    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/intercom/composer/a/g;->e:Z

    .line 45
    return-void
.end method
