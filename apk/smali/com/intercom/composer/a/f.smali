.class public Lcom/intercom/composer/a/f;
.super Ljava/lang/Object;
.source "SendButtonAnimator.java"


# instance fields
.field final a:Landroid/view/View;

.field final b:Landroid/widget/ImageView;

.field c:Landroid/animation/AnimatorSet;

.field final d:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

.field private final e:Lcom/intercom/composer/a/h;

.field private final f:Lcom/intercom/composer/a/e;

.field private g:Landroid/animation/ObjectAnimator;

.field private h:Landroid/animation/ObjectAnimator;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/widget/ImageView;Lcom/intercom/composer/a/h;Lcom/intercom/composer/a/e;)V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    new-instance v0, Lcom/intercom/composer/a/f$1;

    invoke-direct {v0, p0}, Lcom/intercom/composer/a/f$1;-><init>(Lcom/intercom/composer/a/f;)V

    iput-object v0, p0, Lcom/intercom/composer/a/f;->d:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 44
    iput-object p1, p0, Lcom/intercom/composer/a/f;->a:Landroid/view/View;

    .line 45
    iput-object p2, p0, Lcom/intercom/composer/a/f;->b:Landroid/widget/ImageView;

    .line 46
    iput-object p3, p0, Lcom/intercom/composer/a/f;->e:Lcom/intercom/composer/a/h;

    .line 47
    iput-object p4, p0, Lcom/intercom/composer/a/f;->f:Lcom/intercom/composer/a/e;

    .line 49
    new-array v0, v7, [Landroid/animation/PropertyValuesHolder;

    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v2, v4, [F

    fill-array-data v2, :array_0

    .line 50
    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    aput-object v1, v0, v5

    const-string v1, "scaleX"

    new-array v2, v4, [F

    fill-array-data v2, :array_1

    .line 51
    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    aput-object v1, v0, v6

    const-string v1, "scaleY"

    new-array v2, v4, [F

    fill-array-data v2, :array_2

    .line 52
    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    aput-object v1, v0, v4

    .line 49
    invoke-static {p2, v0}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/intercom/composer/a/f;->g:Landroid/animation/ObjectAnimator;

    .line 53
    iget-object v0, p0, Lcom/intercom/composer/a/f;->g:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 55
    new-array v0, v7, [Landroid/animation/PropertyValuesHolder;

    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v2, v4, [F

    fill-array-data v2, :array_3

    .line 56
    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    aput-object v1, v0, v5

    const-string v1, "scaleX"

    new-array v2, v4, [F

    fill-array-data v2, :array_4

    .line 57
    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    aput-object v1, v0, v6

    const-string v1, "scaleY"

    new-array v2, v4, [F

    fill-array-data v2, :array_5

    .line 58
    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    aput-object v1, v0, v4

    .line 55
    invoke-static {p2, v0}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/intercom/composer/a/f;->h:Landroid/animation/ObjectAnimator;

    .line 59
    return-void

    .line 49
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 50
    :array_1
    .array-data 4
        0x3f19999a    # 0.6f
        0x3f800000    # 1.0f
    .end array-data

    .line 51
    :array_2
    .array-data 4
        0x3f19999a    # 0.6f
        0x3f800000    # 1.0f
    .end array-data

    .line 55
    :array_3
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 56
    :array_4
    .array-data 4
        0x3f800000    # 1.0f
        0x3f19999a    # 0.6f
    .end array-data

    .line 57
    :array_5
    .array-data 4
        0x3f800000    # 1.0f
        0x3f19999a    # 0.6f
    .end array-data
.end method


# virtual methods
.method public a(ZLcom/intercom/composer/a/a;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 63
    if-eqz p1, :cond_3

    sget-object v0, Lcom/intercom/composer/a/a;->a:Lcom/intercom/composer/a/a;

    if-eq p2, v0, :cond_0

    sget-object v0, Lcom/intercom/composer/a/a;->c:Lcom/intercom/composer/a/a;

    if-eq p2, v0, :cond_0

    sget-object v0, Lcom/intercom/composer/a/a;->e:Lcom/intercom/composer/a/a;

    if-ne p2, v0, :cond_3

    :cond_0
    move v0, v1

    .line 71
    :goto_0
    iget-object v3, p0, Lcom/intercom/composer/a/f;->c:Landroid/animation/AnimatorSet;

    if-eqz v3, :cond_1

    .line 72
    iget-object v3, p0, Lcom/intercom/composer/a/f;->c:Landroid/animation/AnimatorSet;

    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->cancel()V

    .line 74
    :cond_1
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v3, p0, Lcom/intercom/composer/a/f;->c:Landroid/animation/AnimatorSet;

    .line 76
    if-eqz v0, :cond_5

    const/high16 v3, 0x3f800000    # 1.0f

    .line 77
    :goto_1
    iget-object v4, p0, Lcom/intercom/composer/a/f;->a:Landroid/view/View;

    const-string v5, "alpha"

    new-array v6, v1, [F

    aput v3, v6, v2

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 78
    iget-object v4, p0, Lcom/intercom/composer/a/f;->d:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v3, v4}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 80
    iget-object v4, p0, Lcom/intercom/composer/a/f;->c:Landroid/animation/AnimatorSet;

    const/4 v5, 0x2

    new-array v5, v5, [Landroid/animation/Animator;

    aput-object v3, v5, v2

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/intercom/composer/a/f;->g:Landroid/animation/ObjectAnimator;

    :goto_2
    aput-object v0, v5, v1

    invoke-virtual {v4, v5}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 81
    iget-object v0, p0, Lcom/intercom/composer/a/f;->c:Landroid/animation/AnimatorSet;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 82
    iget-object v1, p0, Lcom/intercom/composer/a/f;->c:Landroid/animation/AnimatorSet;

    if-eqz p1, :cond_7

    iget-object v0, p0, Lcom/intercom/composer/a/f;->e:Lcom/intercom/composer/a/h;

    :goto_3
    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 83
    iget-object v0, p0, Lcom/intercom/composer/a/f;->c:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 84
    :cond_2
    return-void

    .line 65
    :cond_3
    if-nez p1, :cond_2

    sget-object v0, Lcom/intercom/composer/a/a;->b:Lcom/intercom/composer/a/a;

    if-eq p2, v0, :cond_4

    sget-object v0, Lcom/intercom/composer/a/a;->d:Lcom/intercom/composer/a/a;

    if-eq p2, v0, :cond_4

    sget-object v0, Lcom/intercom/composer/a/a;->e:Lcom/intercom/composer/a/a;

    if-ne p2, v0, :cond_2

    :cond_4
    move v0, v2

    .line 66
    goto :goto_0

    .line 76
    :cond_5
    const/4 v3, 0x0

    goto :goto_1

    .line 80
    :cond_6
    iget-object v0, p0, Lcom/intercom/composer/a/f;->h:Landroid/animation/ObjectAnimator;

    goto :goto_2

    .line 82
    :cond_7
    iget-object v0, p0, Lcom/intercom/composer/a/f;->f:Lcom/intercom/composer/a/e;

    goto :goto_3
.end method
