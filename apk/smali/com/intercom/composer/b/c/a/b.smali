.class public Lcom/intercom/composer/b/c/a/b;
.super Ljava/lang/Object;
.source "SendButtonClickListener.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Lcom/intercom/composer/b/c/a/a;

.field private final b:Landroid/widget/EditText;


# direct methods
.method public constructor <init>(Lcom/intercom/composer/b/c/a/a;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/intercom/composer/b/c/a/b;->a:Lcom/intercom/composer/b/c/a/a;

    .line 13
    iput-object p2, p0, Lcom/intercom/composer/b/c/a/b;->b:Landroid/widget/EditText;

    .line 14
    return-void
.end method


# virtual methods
.method a()V
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/intercom/composer/b/c/a/b;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->clear()V

    .line 23
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 17
    iget-object v0, p0, Lcom/intercom/composer/b/c/a/b;->a:Lcom/intercom/composer/b/c/a/a;

    iget-object v1, p0, Lcom/intercom/composer/b/c/a/b;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/intercom/composer/b/c/a/a;->a(Ljava/lang/CharSequence;)V

    .line 18
    invoke-virtual {p0}, Lcom/intercom/composer/b/c/a/b;->a()V

    .line 19
    return-void
.end method
