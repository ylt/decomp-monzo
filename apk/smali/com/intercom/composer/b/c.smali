.class public abstract Lcom/intercom/composer/b/c;
.super Landroid/support/v4/app/Fragment;
.source "InputFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 11
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 12
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/intercom/composer/b/c;->setRetainInstance(Z)V

    .line 13
    return-void
.end method

.method public abstract onInputDeselected()V
.end method

.method public abstract onInputReselected()V
.end method

.method public abstract onInputSelected()V
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 43
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 44
    invoke-virtual {p0}, Lcom/intercom/composer/b/c;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/intercom/composer/b/c;->passDataOnViewCreated(Landroid/os/Bundle;)V

    .line 45
    return-void
.end method

.method public passData(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/intercom/composer/b/c;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 18
    invoke-virtual {p0, p1}, Lcom/intercom/composer/b/c;->passDataOnViewCreated(Landroid/os/Bundle;)V

    .line 23
    :goto_0
    return-void

    .line 21
    :cond_0
    invoke-virtual {p0, p1}, Lcom/intercom/composer/b/c;->setArguments(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method protected abstract passDataOnViewCreated(Landroid/os/Bundle;)V
.end method

.method public setArguments(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/intercom/composer/b/c;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 34
    if-nez v0, :cond_0

    .line 35
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 38
    :cond_0
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 39
    invoke-super {p0, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 40
    return-void
.end method
