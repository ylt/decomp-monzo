.class public Lcom/intercom/composer/b/b/e;
.super Landroid/support/v7/widget/RecyclerView$a;
.source "InputIconsRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$a",
        "<",
        "Landroid/support/v7/widget/RecyclerView$w;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/intercom/composer/b/b/f;

.field private final b:Landroid/view/LayoutInflater;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/intercom/composer/b/b;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/intercom/composer/b/b;

.field private final e:Lcom/intercom/composer/b/b/b;

.field private final f:Landroid/support/v4/app/n;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Ljava/util/List;Lcom/intercom/composer/b/b/f;Lcom/intercom/composer/b/b/b;Landroid/support/v4/app/n;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/LayoutInflater;",
            "Ljava/util/List",
            "<",
            "Lcom/intercom/composer/b/b;",
            ">;",
            "Lcom/intercom/composer/b/b/f;",
            "Lcom/intercom/composer/b/b/b;",
            "Landroid/support/v4/app/n;",
            ")V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$a;-><init>()V

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/intercom/composer/b/b/e;->c:Ljava/util/List;

    .line 35
    iput-object p2, p0, Lcom/intercom/composer/b/b/e;->c:Ljava/util/List;

    .line 36
    iput-object p3, p0, Lcom/intercom/composer/b/b/e;->a:Lcom/intercom/composer/b/b/f;

    .line 37
    iput-object p1, p0, Lcom/intercom/composer/b/b/e;->b:Landroid/view/LayoutInflater;

    .line 38
    iput-object p4, p0, Lcom/intercom/composer/b/b/e;->e:Lcom/intercom/composer/b/b/b;

    .line 39
    iput-object p5, p0, Lcom/intercom/composer/b/b/e;->f:Landroid/support/v4/app/n;

    .line 40
    return-void
.end method

.method private a(Lcom/intercom/composer/b/b;)V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/intercom/composer/b/b/e;->d:Lcom/intercom/composer/b/b;

    if-ne p1, v0, :cond_1

    .line 105
    iget-object v0, p0, Lcom/intercom/composer/b/b/e;->f:Landroid/support/v4/app/n;

    invoke-virtual {p1, v0}, Lcom/intercom/composer/b/b;->findFragment(Landroid/support/v4/app/n;)Lcom/intercom/composer/b/c;

    move-result-object v0

    .line 106
    if-eqz v0, :cond_0

    .line 107
    invoke-virtual {v0}, Lcom/intercom/composer/b/c;->onInputReselected()V

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    iget-object v0, p0, Lcom/intercom/composer/b/b/e;->d:Lcom/intercom/composer/b/b;

    if-eqz v0, :cond_2

    .line 111
    iget-object v0, p0, Lcom/intercom/composer/b/b/e;->d:Lcom/intercom/composer/b/b;

    iget-object v1, p0, Lcom/intercom/composer/b/b/e;->f:Landroid/support/v4/app/n;

    invoke-virtual {v0, v1}, Lcom/intercom/composer/b/b;->findFragment(Landroid/support/v4/app/n;)Lcom/intercom/composer/b/c;

    move-result-object v0

    .line 112
    if-eqz v0, :cond_2

    .line 113
    invoke-virtual {v0}, Lcom/intercom/composer/b/c;->onInputDeselected()V

    .line 116
    :cond_2
    iget-object v0, p0, Lcom/intercom/composer/b/b/e;->f:Landroid/support/v4/app/n;

    invoke-virtual {p1, v0}, Lcom/intercom/composer/b/b;->findFragment(Landroid/support/v4/app/n;)Lcom/intercom/composer/b/c;

    move-result-object v0

    .line 117
    if-eqz v0, :cond_0

    .line 118
    invoke-virtual {v0}, Lcom/intercom/composer/b/c;->onInputSelected()V

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/intercom/composer/b/b;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/intercom/composer/b/b/e;->d:Lcom/intercom/composer/b/b;

    return-object v0
.end method

.method public a(Lcom/intercom/composer/b/b;ZZ)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 85
    iget-object v1, p0, Lcom/intercom/composer/b/b/e;->c:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 100
    :cond_0
    :goto_0
    return v0

    .line 89
    :cond_1
    invoke-direct {p0, p1}, Lcom/intercom/composer/b/b/e;->a(Lcom/intercom/composer/b/b;)V

    .line 91
    iget-object v1, p0, Lcom/intercom/composer/b/b/e;->d:Lcom/intercom/composer/b/b;

    if-eq p1, v1, :cond_0

    .line 95
    iput-object p1, p0, Lcom/intercom/composer/b/b/e;->d:Lcom/intercom/composer/b/b;

    .line 96
    invoke-virtual {p0}, Lcom/intercom/composer/b/b/e;->notifyDataSetChanged()V

    .line 98
    iget-object v0, p0, Lcom/intercom/composer/b/b/e;->a:Lcom/intercom/composer/b/b/f;

    iget-object v1, p0, Lcom/intercom/composer/b/b/e;->c:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-interface {v0, p1, v1, p2, p3}, Lcom/intercom/composer/b/b/f;->a(Lcom/intercom/composer/b/b;IZZ)V

    .line 100
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 124
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/intercom/composer/b/b/e;->d:Lcom/intercom/composer/b/b;

    .line 125
    invoke-virtual {p0}, Lcom/intercom/composer/b/b/e;->notifyDataSetChanged()V

    .line 126
    return-void
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/intercom/composer/b/b/e;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/intercom/composer/b/b/e;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intercom/composer/b/b;

    .line 44
    instance-of v0, v0, Lcom/intercom/composer/b/a/b;

    if-eqz v0, :cond_0

    .line 45
    const/4 v0, 0x2

    .line 47
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$w;I)V
    .locals 3

    .prologue
    .line 61
    iget-object v0, p0, Lcom/intercom/composer/b/b/e;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intercom/composer/b/b;

    .line 62
    instance-of v1, p1, Lcom/intercom/composer/b/b/d;

    if-eqz v1, :cond_0

    .line 63
    check-cast p1, Lcom/intercom/composer/b/b/d;

    .line 64
    iget-object v1, p0, Lcom/intercom/composer/b/b/e;->d:Lcom/intercom/composer/b/b;

    if-eqz v1, :cond_1

    .line 65
    invoke-virtual {v0}, Lcom/intercom/composer/b/b;->getUniqueIdentifier()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/intercom/composer/b/b/e;->d:Lcom/intercom/composer/b/b;

    invoke-virtual {v2}, Lcom/intercom/composer/b/b;->getUniqueIdentifier()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    .line 66
    :goto_0
    invoke-virtual {p1, v0, v1}, Lcom/intercom/composer/b/b/d;->a(Lcom/intercom/composer/b/b;Z)V

    .line 68
    :cond_0
    return-void

    .line 65
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$w;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 52
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 53
    new-instance v0, Lcom/intercom/composer/b/b/a;

    iget-object v1, p0, Lcom/intercom/composer/b/b/e;->b:Landroid/view/LayoutInflater;

    sget v2, Lcom/intercom/composer/g$f;->intercom_composer_empty_view_layout:I

    .line 54
    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/intercom/composer/b/b/a;-><init>(Landroid/view/View;)V

    .line 56
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/intercom/composer/b/b/d;

    iget-object v1, p0, Lcom/intercom/composer/b/b/e;->b:Landroid/view/LayoutInflater;

    sget v2, Lcom/intercom/composer/g$f;->intercom_composer_input_icon_view_layout:I

    .line 57
    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/intercom/composer/b/b/e;->e:Lcom/intercom/composer/b/b/b;

    invoke-direct {v0, v1, v2}, Lcom/intercom/composer/b/b/d;-><init>(Landroid/view/View;Lcom/intercom/composer/b/b/b;)V

    goto :goto_0
.end method
