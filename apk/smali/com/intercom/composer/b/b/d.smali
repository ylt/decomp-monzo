.class Lcom/intercom/composer/b/b/d;
.super Landroid/support/v7/widget/RecyclerView$w;
.source "InputIconViewHolder.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final a:Landroid/widget/ImageView;

.field final b:Lcom/intercom/composer/b/b/b;


# direct methods
.method constructor <init>(Landroid/view/View;Lcom/intercom/composer/b/b/b;)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$w;-><init>(Landroid/view/View;)V

    .line 17
    iput-object p2, p0, Lcom/intercom/composer/b/b/d;->b:Lcom/intercom/composer/b/b/b;

    .line 18
    sget v0, Lcom/intercom/composer/g$e;->input_icon_image_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/intercom/composer/b/b/d;->a:Landroid/widget/ImageView;

    .line 19
    iget-object v0, p0, Lcom/intercom/composer/b/b/d;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 20
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 21
    return-void
.end method


# virtual methods
.method a(Lcom/intercom/composer/b/b;Z)V
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/intercom/composer/b/b/d;->a:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/intercom/composer/b/b/d;->a:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/intercom/composer/b/b;->getIconDrawable(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 29
    iget-object v0, p0, Lcom/intercom/composer/b/b/d;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 30
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/intercom/composer/b/b/d;->b:Lcom/intercom/composer/b/b/b;

    invoke-interface {v0, p0}, Lcom/intercom/composer/b/b/b;->a(Landroid/support/v7/widget/RecyclerView$w;)V

    .line 25
    return-void
.end method
