.class public Lcom/intercom/composer/pager/a;
.super Landroid/support/v4/app/r;
.source "ComposerPagerAdapter.java"


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Lcom/intercom/composer/b/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/support/v4/app/n;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/n;",
            "Ljava/util/List",
            "<+",
            "Lcom/intercom/composer/b/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1}, Landroid/support/v4/app/r;-><init>(Landroid/support/v4/app/n;)V

    .line 18
    iput-object p2, p0, Lcom/intercom/composer/pager/a;->a:Ljava/util/List;

    .line 19
    return-void
.end method


# virtual methods
.method public a(I)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/intercom/composer/pager/a;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intercom/composer/b/b;

    invoke-virtual {v0}, Lcom/intercom/composer/b/b;->createFragment()Lcom/intercom/composer/b/c;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 30
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/r;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 31
    iget-object v1, p0, Lcom/intercom/composer/pager/a;->a:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/intercom/composer/b/b;

    .line 32
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getTag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/intercom/composer/b/b;->setFragmentTag(Ljava/lang/String;)V

    .line 33
    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/intercom/composer/pager/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
