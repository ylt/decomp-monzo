.class public Lcom/intercom/composer/ComposerView;
.super Landroid/widget/LinearLayout;
.source "ComposerView.java"

# interfaces
.implements Lcom/intercom/composer/a;
.implements Lcom/intercom/composer/b/b/b;
.implements Lcom/intercom/composer/b/b/f;
.implements Lcom/intercom/composer/b/c/a/a;
.implements Lcom/intercom/composer/d/a;


# instance fields
.field a:Landroid/support/v7/widget/RecyclerView;

.field b:Landroid/widget/LinearLayout;

.field c:Landroid/support/v4/view/ViewPager;

.field d:Landroid/widget/ImageView;

.field e:Landroid/view/View;

.field f:Landroid/view/View;

.field g:Landroid/view/View;

.field h:Lcom/intercom/composer/b/b/e;

.field i:Lcom/intercom/composer/c/a;

.field j:Lcom/intercom/composer/a/b;

.field k:Lcom/intercom/composer/f;

.field l:Lcom/intercom/composer/a/f;

.field m:Lcom/intercom/composer/a/a;

.field final n:Lcom/intercom/composer/d/b;

.field private final o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/intercom/composer/b/b;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Landroid/support/v7/widget/LinearLayoutManager;

.field private final q:Lcom/intercom/composer/c/d;

.field private r:Lcom/intercom/composer/b/c/a/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/intercom/composer/ComposerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 87
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/intercom/composer/ComposerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 91
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 94
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/intercom/composer/ComposerView;->o:Ljava/util/List;

    .line 82
    sget-object v0, Lcom/intercom/composer/a/a;->e:Lcom/intercom/composer/a/a;

    iput-object v0, p0, Lcom/intercom/composer/ComposerView;->m:Lcom/intercom/composer/a/a;

    .line 95
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/intercom/composer/ComposerView;->setOrientation(I)V

    .line 96
    sget v0, Lcom/intercom/composer/g$f;->intercom_composer_view_layout:I

    invoke-static {p1, v0, p0}, Lcom/intercom/composer/ComposerView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 98
    sget v0, Lcom/intercom/composer/g$e;->composer_upper_border:I

    invoke-virtual {p0, v0}, Lcom/intercom/composer/ComposerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/intercom/composer/ComposerView;->f:Landroid/view/View;

    .line 99
    sget v0, Lcom/intercom/composer/g$e;->composer_lower_border:I

    invoke-virtual {p0, v0}, Lcom/intercom/composer/ComposerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/intercom/composer/ComposerView;->g:Landroid/view/View;

    .line 101
    sget v0, Lcom/intercom/composer/g$e;->composer_edit_text_layout:I

    invoke-virtual {p0, v0}, Lcom/intercom/composer/ComposerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/intercom/composer/ComposerView;->b:Landroid/widget/LinearLayout;

    .line 102
    sget v0, Lcom/intercom/composer/g$e;->composer_input_icons_recycler_view:I

    invoke-virtual {p0, v0}, Lcom/intercom/composer/ComposerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/intercom/composer/ComposerView;->a:Landroid/support/v7/widget/RecyclerView;

    .line 103
    sget v0, Lcom/intercom/composer/g$e;->composer_view_pager:I

    invoke-virtual {p0, v0}, Lcom/intercom/composer/ComposerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/intercom/composer/ComposerView;->c:Landroid/support/v4/view/ViewPager;

    .line 104
    sget v0, Lcom/intercom/composer/g$e;->send_button:I

    invoke-virtual {p0, v0}, Lcom/intercom/composer/ComposerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/intercom/composer/ComposerView;->d:Landroid/widget/ImageView;

    .line 106
    sget v0, Lcom/intercom/composer/g$e;->send_button_fading_background:I

    invoke-virtual {p0, v0}, Lcom/intercom/composer/ComposerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/intercom/composer/ComposerView;->e:Landroid/view/View;

    .line 108
    new-instance v0, Lcom/intercom/composer/c/d;

    invoke-direct {v0, p1}, Lcom/intercom/composer/c/d;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/intercom/composer/ComposerView;->q:Lcom/intercom/composer/c/d;

    .line 109
    new-instance v1, Lcom/intercom/composer/c/a;

    move-object v0, p1

    check-cast v0, Landroid/app/Activity;

    iget-object v2, p0, Lcom/intercom/composer/ComposerView;->q:Lcom/intercom/composer/c/d;

    iget-object v3, p0, Lcom/intercom/composer/ComposerView;->b:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/intercom/composer/ComposerView;->c:Landroid/support/v4/view/ViewPager;

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/intercom/composer/c/a;-><init>(Landroid/app/Activity;Lcom/intercom/composer/c/d;Landroid/view/View;Landroid/view/View;)V

    iput-object v1, p0, Lcom/intercom/composer/ComposerView;->i:Lcom/intercom/composer/c/a;

    .line 110
    new-instance v0, Lcom/intercom/composer/a/b;

    iget-object v1, p0, Lcom/intercom/composer/ComposerView;->b:Landroid/widget/LinearLayout;

    invoke-direct {v0, v1}, Lcom/intercom/composer/a/b;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/intercom/composer/ComposerView;->j:Lcom/intercom/composer/a/b;

    .line 111
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-direct {v0, p1, v5, v5}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    iput-object v0, p0, Lcom/intercom/composer/ComposerView;->p:Landroid/support/v7/widget/LinearLayoutManager;

    .line 112
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/intercom/composer/ComposerView;->p:Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$h;)V

    .line 113
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->a:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lcom/intercom/composer/b/b/c;

    invoke-direct {v1, p1}, Lcom/intercom/composer/b/b/c;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$g;)V

    .line 114
    new-instance v0, Lcom/intercom/composer/d/b;

    invoke-direct {v0, p0}, Lcom/intercom/composer/d/b;-><init>(Lcom/intercom/composer/d/a;)V

    iput-object v0, p0, Lcom/intercom/composer/ComposerView;->n:Lcom/intercom/composer/d/b;

    .line 115
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 351
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 352
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 353
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 355
    :cond_0
    return-void
.end method

.method private a(Landroid/widget/EditText;)V
    .locals 2

    .prologue
    .line 295
    invoke-virtual {p1}, Landroid/widget/EditText;->requestFocus()Z

    .line 296
    invoke-virtual {p1}, Landroid/widget/EditText;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    .line 297
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 298
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 300
    invoke-virtual {p1}, Landroid/widget/EditText;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/intercom/composer/g$a;->intercom_composer_keyboard_takes_full_screen_in_landscape:I

    .line 301
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 302
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->q:Lcom/intercom/composer/c/d;

    invoke-virtual {v0}, Lcom/intercom/composer/c/d;->a()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 303
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->i:Lcom/intercom/composer/c/a;

    invoke-virtual {v0}, Lcom/intercom/composer/c/a;->b()Z

    .line 305
    :cond_0
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 274
    invoke-direct {p0}, Lcom/intercom/composer/ComposerView;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 276
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 277
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 283
    :goto_0
    return-void

    .line 279
    :cond_0
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 280
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 281
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private e()Z
    .locals 2

    .prologue
    .line 286
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intercom/composer/b/b;

    .line 287
    instance-of v0, v0, Lcom/intercom/composer/b/c/b;

    if-eqz v0, :cond_0

    .line 288
    const/4 v0, 0x1

    .line 291
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a(Lcom/intercom/composer/b/c/b;)Landroid/widget/EditText;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 315
    invoke-virtual {p1}, Lcom/intercom/composer/b/c/b;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    .line 316
    invoke-virtual {p1}, Lcom/intercom/composer/b/c/b;->getOptions()Ljava/util/List;

    move-result-object v2

    .line 318
    iget-object v3, p0, Lcom/intercom/composer/ComposerView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 320
    invoke-direct {p0, v1}, Lcom/intercom/composer/ComposerView;->a(Landroid/view/View;)V

    .line 321
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x2

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct {v3, v0, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 322
    iget-object v4, p0, Lcom/intercom/composer/ComposerView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 324
    iget-object v3, p0, Lcom/intercom/composer/ComposerView;->d:Landroid/widget/ImageView;

    new-instance v4, Lcom/intercom/composer/b/c/a/b;

    invoke-direct {v4, p0, v1}, Lcom/intercom/composer/b/c/a/b;-><init>(Lcom/intercom/composer/b/c/a/a;Landroid/widget/EditText;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 326
    iget-object v3, p0, Lcom/intercom/composer/ComposerView;->n:Lcom/intercom/composer/d/b;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 330
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/intercom/composer/ComposerView;->a(Z)V

    .line 332
    if-eqz v2, :cond_1

    .line 333
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intercom/composer/b/c/b/c;

    .line 334
    invoke-virtual {p0}, Lcom/intercom/composer/ComposerView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/intercom/composer/g$c;->intercom_composer_editable_text_input_option_padding:I

    .line 335
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 336
    invoke-virtual {p0}, Lcom/intercom/composer/ComposerView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/intercom/composer/g$c;->intercom_composer_editable_text_input_option_padding_bottom:I

    .line 337
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 338
    new-instance v5, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/intercom/composer/ComposerView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 339
    invoke-virtual {v0}, Lcom/intercom/composer/b/c/b/c;->a()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 340
    invoke-virtual {v5, v3, v3, v3, v4}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 341
    new-instance v3, Lcom/intercom/composer/b/c/b/b;

    invoke-direct {v3, v0}, Lcom/intercom/composer/b/c/b/b;-><init>(Lcom/intercom/composer/b/c/b/c;)V

    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 343
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 347
    :cond_1
    return-object v1
.end method

.method public a(II)V
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 223
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->setBackgroundResource(I)V

    .line 224
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->e:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 225
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->f:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 226
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->g:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 227
    return-void
.end method

.method a(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 119
    sget v0, Lcom/intercom/composer/g$d;->intercom_composer_send_background:I

    invoke-static {p1, v0}, Landroid/support/v4/content/a;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 120
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, p2, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 121
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    .line 122
    iget-object v1, p0, Lcom/intercom/composer/ComposerView;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 126
    :goto_0
    return-void

    .line 124
    :cond_0
    iget-object v1, p0, Lcom/intercom/composer/ComposerView;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public a(Landroid/support/v7/widget/RecyclerView$w;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 129
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$w;->getAdapterPosition()I

    move-result v0

    .line 130
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/intercom/composer/ComposerView;->o:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 131
    iget-object v1, p0, Lcom/intercom/composer/ComposerView;->o:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intercom/composer/b/b;

    invoke-virtual {p0, v0, v2, v2}, Lcom/intercom/composer/ComposerView;->a(Lcom/intercom/composer/b/b;ZZ)Z

    .line 133
    :cond_0
    return-void
.end method

.method public a(Lcom/intercom/composer/a/a;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/intercom/composer/ComposerView;->m:Lcom/intercom/composer/a/a;

    .line 157
    return-void
.end method

.method public a(Lcom/intercom/composer/b/b;IZZ)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 136
    instance-of v0, p1, Lcom/intercom/composer/b/c/b;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 137
    check-cast v0, Lcom/intercom/composer/b/c/b;

    invoke-virtual {p0, v0}, Lcom/intercom/composer/ComposerView;->a(Lcom/intercom/composer/b/c/b;)Landroid/widget/EditText;

    move-result-object v0

    .line 138
    iget-object v2, p0, Lcom/intercom/composer/ComposerView;->j:Lcom/intercom/composer/a/b;

    invoke-virtual {v2, p4}, Lcom/intercom/composer/a/b;->a(Z)V

    .line 140
    if-eqz p3, :cond_0

    .line 141
    invoke-direct {p0, v0}, Lcom/intercom/composer/ComposerView;->a(Landroid/widget/EditText;)V

    .line 143
    :cond_0
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/intercom/composer/ComposerView;->a(Z)V

    .line 151
    :goto_1
    invoke-virtual {p1}, Lcom/intercom/composer/b/b;->getBackgroundColor()I

    move-result v0

    invoke-virtual {p1}, Lcom/intercom/composer/b/b;->getBorderColor()I

    move-result v2

    invoke-virtual {p0, v0, v2}, Lcom/intercom/composer/ComposerView;->a(II)V

    .line 152
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p2, v1}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 153
    return-void

    :cond_1
    move v0, v1

    .line 143
    goto :goto_0

    .line 145
    :cond_2
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->i:Lcom/intercom/composer/c/a;

    invoke-virtual {v0}, Lcom/intercom/composer/c/a;->a()V

    .line 146
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->clearFocus()V

    .line 147
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->j:Lcom/intercom/composer/a/b;

    invoke-virtual {v0}, Lcom/intercom/composer/a/b;->a()V

    .line 148
    invoke-virtual {p0, v1}, Lcom/intercom/composer/ComposerView;->a(Z)V

    goto :goto_1
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->r:Lcom/intercom/composer/b/c/a/a;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->r:Lcom/intercom/composer/b/c/a/a;

    invoke-interface {v0, p1}, Lcom/intercom/composer/b/c/a/a;->a(Ljava/lang/CharSequence;)V

    .line 173
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 164
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->l:Lcom/intercom/composer/a/f;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->l:Lcom/intercom/composer/a/f;

    iget-object v1, p0, Lcom/intercom/composer/ComposerView;->m:Lcom/intercom/composer/a/a;

    invoke-virtual {v0, p1, v1}, Lcom/intercom/composer/a/f;->a(ZLcom/intercom/composer/a/a;)V

    .line 167
    :cond_0
    return-void
.end method

.method public a()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 243
    invoke-virtual {p0}, Lcom/intercom/composer/ComposerView;->getSelectedInput()Lcom/intercom/composer/b/b;

    move-result-object v0

    .line 245
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/intercom/composer/ComposerView;->o:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->o:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intercom/composer/b/b;

    invoke-virtual {p0, v0, v2, v2}, Lcom/intercom/composer/ComposerView;->a(Lcom/intercom/composer/b/b;ZZ)Z

    .line 250
    :goto_0
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->i:Lcom/intercom/composer/c/a;

    invoke-virtual {v0}, Lcom/intercom/composer/c/a;->b()Z

    move-result v0

    return v0

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->h:Lcom/intercom/composer/b/b/e;

    invoke-virtual {v0}, Lcom/intercom/composer/b/b/e;->b()V

    goto :goto_0
.end method

.method public a(Lcom/intercom/composer/b/b;ZZ)Z
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->k:Lcom/intercom/composer/f;

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->k:Lcom/intercom/composer/f;

    invoke-interface {v0, p1}, Lcom/intercom/composer/f;->onInputSelected(Lcom/intercom/composer/b/b;)V

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->h:Lcom/intercom/composer/b/b/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/intercom/composer/b/b/e;->a(Lcom/intercom/composer/b/b;ZZ)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 254
    invoke-virtual {p0}, Lcom/intercom/composer/ComposerView;->c()V

    .line 255
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->i:Lcom/intercom/composer/c/a;

    invoke-virtual {v0}, Lcom/intercom/composer/c/a;->c()V

    .line 256
    return-void
.end method

.method c()V
    .locals 4

    .prologue
    .line 262
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    .line 263
    if-lez v2, :cond_1

    .line 264
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 265
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 266
    instance-of v3, v0, Landroid/widget/EditText;

    if-eqz v3, :cond_0

    .line 267
    check-cast v0, Landroid/widget/EditText;

    iget-object v3, p0, Lcom/intercom/composer/ComposerView;->n:Lcom/intercom/composer/d/b;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 264
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 271
    :cond_1
    return-void
.end method

.method public getInputs()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/intercom/composer/b/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 366
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->o:Ljava/util/List;

    return-object v0
.end method

.method public getSelectedInput()Lcom/intercom/composer/b/b;
    .locals 1

    .prologue
    .line 358
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->h:Lcom/intercom/composer/b/b/e;

    invoke-virtual {v0}, Lcom/intercom/composer/b/b/e;->a()Lcom/intercom/composer/b/b;

    move-result-object v0

    return-object v0
.end method

.method public getTextInputHeight()I
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    return v0
.end method

.method public setComposerPagerAdapter(Lcom/intercom/composer/pager/a;)V
    .locals 6

    .prologue
    .line 195
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/p;)V

    .line 196
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->c:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/intercom/composer/ComposerView;->o:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 198
    new-instance v0, Lcom/intercom/composer/a/h;

    iget-object v1, p0, Lcom/intercom/composer/ComposerView;->o:Ljava/util/List;

    iget-object v3, p0, Lcom/intercom/composer/ComposerView;->h:Lcom/intercom/composer/b/b/e;

    iget-object v4, p0, Lcom/intercom/composer/ComposerView;->p:Landroid/support/v7/widget/LinearLayoutManager;

    move-object v2, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/intercom/composer/a/h;-><init>(Ljava/util/List;Lcom/intercom/composer/pager/a;Landroid/support/v7/widget/RecyclerView$a;Landroid/support/v7/widget/LinearLayoutManager;Lcom/intercom/composer/a;)V

    .line 202
    new-instance v1, Lcom/intercom/composer/a/e;

    iget-object v2, p0, Lcom/intercom/composer/ComposerView;->o:Ljava/util/List;

    iget-object v3, p0, Lcom/intercom/composer/ComposerView;->h:Lcom/intercom/composer/b/b/e;

    invoke-direct {v1, v2, p1, v3, p0}, Lcom/intercom/composer/a/e;-><init>(Ljava/util/List;Lcom/intercom/composer/pager/a;Landroid/support/v7/widget/RecyclerView$a;Lcom/intercom/composer/a;)V

    .line 206
    new-instance v2, Lcom/intercom/composer/a/f;

    iget-object v3, p0, Lcom/intercom/composer/ComposerView;->e:Landroid/view/View;

    iget-object v4, p0, Lcom/intercom/composer/ComposerView;->d:Landroid/widget/ImageView;

    invoke-direct {v2, v3, v4, v0, v1}, Lcom/intercom/composer/a/f;-><init>(Landroid/view/View;Landroid/widget/ImageView;Lcom/intercom/composer/a/h;Lcom/intercom/composer/a/e;)V

    iput-object v2, p0, Lcom/intercom/composer/ComposerView;->l:Lcom/intercom/composer/a/f;

    .line 208
    return-void
.end method

.method public setEditTextLayoutAnimationListener(Lcom/intercom/composer/a/c;)V
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->j:Lcom/intercom/composer/a/b;

    invoke-virtual {v0, p1}, Lcom/intercom/composer/a/b;->a(Lcom/intercom/composer/a/c;)V

    .line 186
    return-void
.end method

.method public setFragmentManager(Landroid/support/v4/app/n;)V
    .locals 6

    .prologue
    .line 189
    new-instance v0, Lcom/intercom/composer/b/b/e;

    invoke-virtual {p0}, Lcom/intercom/composer/ComposerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v2, p0, Lcom/intercom/composer/ComposerView;->o:Ljava/util/List;

    move-object v3, p0

    move-object v4, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/intercom/composer/b/b/e;-><init>(Landroid/view/LayoutInflater;Ljava/util/List;Lcom/intercom/composer/b/b/f;Lcom/intercom/composer/b/b/b;Landroid/support/v4/app/n;)V

    iput-object v0, p0, Lcom/intercom/composer/ComposerView;->h:Lcom/intercom/composer/b/b/e;

    .line 191
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/intercom/composer/ComposerView;->h:Lcom/intercom/composer/b/b/e;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 192
    return-void
.end method

.method public setInputSelectedListener(Lcom/intercom/composer/f;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcom/intercom/composer/ComposerView;->k:Lcom/intercom/composer/f;

    .line 182
    return-void
.end method

.method public setInputs(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/intercom/composer/b/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 211
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->h:Lcom/intercom/composer/b/b/e;

    if-nez v0, :cond_0

    .line 212
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Fragment manager should be set!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 215
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->o:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 216
    invoke-direct {p0}, Lcom/intercom/composer/ComposerView;->d()V

    .line 217
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->h:Lcom/intercom/composer/b/b/e;

    invoke-virtual {v0}, Lcom/intercom/composer/b/b/e;->notifyDataSetChanged()V

    .line 218
    return-void
.end method

.method public setOnSendButtonClickListener(Lcom/intercom/composer/b/c/a/a;)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lcom/intercom/composer/ComposerView;->r:Lcom/intercom/composer/b/c/a/a;

    .line 178
    return-void
.end method

.method public setSendButtonVisibility(I)V
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/intercom/composer/ComposerView;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 161
    return-void
.end method
