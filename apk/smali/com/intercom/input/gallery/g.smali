.class public abstract Lcom/intercom/input/gallery/g;
.super Lcom/intercom/composer/b/c;
.source "GalleryInputFragment.java"

# interfaces
.implements Lcom/intercom/input/gallery/adapter/b;
.implements Lcom/intercom/input/gallery/adapter/e;
.implements Lcom/intercom/input/gallery/h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intercom/input/gallery/g$a;
    }
.end annotation


# static fields
.field private static final ARG_EXPANDED:Ljava/lang/String; = "expanded"

.field public static final GALLERY_FULL_SCREEN_REQUEST_CODE:I = 0xe


# instance fields
.field contentLayout:Landroid/widget/FrameLayout;

.field final dataListener:Lcom/intercom/input/gallery/e$a;

.field dataSource:Lcom/intercom/input/gallery/e;

.field emptyLayout:Lcom/intercom/input/gallery/EmptyView;

.field endlessRecyclerScrollListener:Lcom/intercom/input/gallery/adapter/a;

.field expanded:Z

.field private final expanderClickListener:Landroid/view/View$OnClickListener;

.field final galleryImages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/intercom/input/gallery/c;",
            ">;"
        }
    .end annotation
.end field

.field galleryInputExpandedListener:Lcom/intercom/input/gallery/f;

.field galleryOutputListener:Lcom/intercom/input/gallery/j;

.field private imageLoader:Lcom/intercom/composer/e;

.field injector:Lcom/intercom/input/gallery/g$a;

.field layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

.field recyclerAdapter:Lcom/intercom/input/gallery/adapter/c;

.field recyclerView:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/intercom/composer/b/c;-><init>()V

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/intercom/input/gallery/g;->galleryImages:Ljava/util/List;

    .line 70
    new-instance v0, Lcom/intercom/input/gallery/g$1;

    invoke-direct {v0, p0}, Lcom/intercom/input/gallery/g$1;-><init>(Lcom/intercom/input/gallery/g;)V

    iput-object v0, p0, Lcom/intercom/input/gallery/g;->dataListener:Lcom/intercom/input/gallery/e$a;

    .line 90
    new-instance v0, Lcom/intercom/input/gallery/g$2;

    invoke-direct {v0, p0}, Lcom/intercom/input/gallery/g$2;-><init>(Lcom/intercom/input/gallery/g;)V

    iput-object v0, p0, Lcom/intercom/input/gallery/g;->expanderClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/intercom/input/gallery/g;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/intercom/input/gallery/g;->showPermissionPermanentlyDeniedDialog()V

    return-void
.end method

.method public static createArguments(Z)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 104
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 105
    const-string v1, "expanded"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 106
    return-object v0
.end method

.method private setUpAppBar(Lcom/intercom/input/gallery/g$a;Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 182
    invoke-interface {p1, p2}, Lcom/intercom/input/gallery/g$a;->getToolbar(Landroid/view/ViewGroup;)Landroid/support/v7/widget/Toolbar;

    move-result-object v1

    .line 183
    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 184
    invoke-virtual {p0}, Lcom/intercom/input/gallery/g;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/e;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/e;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 186
    invoke-virtual {p0}, Lcom/intercom/input/gallery/g;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/e;

    invoke-virtual {v0}, Landroid/support/v7/app/e;->getSupportActionBar()Landroid/support/v7/app/a;

    move-result-object v0

    .line 187
    if-eqz v0, :cond_0

    .line 188
    invoke-virtual {v0, v2}, Landroid/support/v7/app/a;->b(Z)V

    .line 189
    invoke-virtual {v0, v2}, Landroid/support/v7/app/a;->a(Z)V

    .line 190
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->c(Z)V

    .line 192
    :cond_0
    return-void
.end method

.method private setUpPreviewViews(Lcom/intercom/input/gallery/g$a;Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->contentLayout:Landroid/widget/FrameLayout;

    invoke-interface {p1, v0}, Lcom/intercom/input/gallery/g$a;->getExpanderButton(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 196
    if-eqz v0, :cond_0

    .line 197
    iget-object v1, p0, Lcom/intercom/input/gallery/g;->contentLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 198
    iget-object v1, p0, Lcom/intercom/input/gallery/g;->expanderClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->contentLayout:Landroid/widget/FrameLayout;

    invoke-interface {p1, v0}, Lcom/intercom/input/gallery/g$a;->getSearchView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 203
    if-eqz v0, :cond_1

    .line 204
    iget-object v1, p0, Lcom/intercom/input/gallery/g;->expanderClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 205
    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 207
    :cond_1
    return-void
.end method

.method private showPermissionPermanentlyDeniedDialog()V
    .locals 3

    .prologue
    .line 332
    new-instance v0, Landroid/support/v7/app/d$a;

    invoke-virtual {p0}, Lcom/intercom/input/gallery/g;->getActivity()Landroid/support/v4/app/j;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/d$a;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/intercom/input/gallery/o$g;->intercom_photo_access_denied:I

    .line 333
    invoke-virtual {v0, v1}, Landroid/support/v7/app/d$a;->a(I)Landroid/support/v7/app/d$a;

    move-result-object v0

    sget v1, Lcom/intercom/input/gallery/o$g;->intercom_go_to_device_settings:I

    .line 334
    invoke-virtual {v0, v1}, Landroid/support/v7/app/d$a;->b(I)Landroid/support/v7/app/d$a;

    move-result-object v0

    sget v1, Lcom/intercom/input/gallery/o$g;->intercom_app_settings:I

    new-instance v2, Lcom/intercom/input/gallery/g$4;

    invoke-direct {v2, p0}, Lcom/intercom/input/gallery/g$4;-><init>(Lcom/intercom/input/gallery/g;)V

    .line 335
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/d$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/d$a;

    move-result-object v0

    sget v1, Lcom/intercom/input/gallery/o$g;->intercom_not_now:I

    const/4 v2, 0x0

    .line 341
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/d$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/d$a;

    move-result-object v0

    .line 342
    invoke-virtual {v0}, Landroid/support/v7/app/d$a;->c()Landroid/support/v7/app/d;

    .line 343
    return-void
.end method


# virtual methods
.method checkPermissionAndFetchImages(Z)V
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->dataSource:Lcom/intercom/input/gallery/e;

    invoke-interface {v0}, Lcom/intercom/input/gallery/e;->getPermissionStatus()I

    move-result v0

    .line 310
    packed-switch v0, :pswitch_data_0

    .line 326
    invoke-virtual {p0}, Lcom/intercom/input/gallery/g;->fetchImagesIfNotFetched()V

    .line 329
    :cond_0
    :goto_0
    return-void

    .line 313
    :pswitch_0
    invoke-virtual {p0, v0}, Lcom/intercom/input/gallery/g;->showEmptyOrPermissionScreen(I)V

    .line 314
    if-eqz p1, :cond_0

    .line 315
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->dataSource:Lcom/intercom/input/gallery/e;

    invoke-interface {v0}, Lcom/intercom/input/gallery/e;->requestPermission()V

    goto :goto_0

    .line 319
    :pswitch_1
    invoke-virtual {p0, v0}, Lcom/intercom/input/gallery/g;->showEmptyOrPermissionScreen(I)V

    .line 320
    if-eqz p1, :cond_0

    .line 321
    invoke-direct {p0}, Lcom/intercom/input/gallery/g;->showPermissionPermanentlyDeniedDialog()V

    goto :goto_0

    .line 310
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method fetchImagesIfNotFetched()V
    .locals 3

    .prologue
    .line 358
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->galleryImages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 359
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->dataSource:Lcom/intercom/input/gallery/e;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/intercom/input/gallery/e;->getImages(ILjava/lang/String;)V

    .line 361
    :cond_0
    return-void
.end method

.method protected abstract getInjector(Lcom/intercom/input/gallery/g;)Lcom/intercom/input/gallery/g$a;
.end method

.method getLayoutRes()I
    .locals 1

    .prologue
    .line 210
    iget-boolean v0, p0, Lcom/intercom/input/gallery/g;->expanded:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/intercom/input/gallery/o$f;->intercom_composer_fragment_composer_gallery_expanded:I

    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/intercom/input/gallery/o$f;->intercom_composer_fragment_composer_gallery:I

    goto :goto_0
.end method

.method launchLightBoxActivity(Lcom/intercom/input/gallery/c;)V
    .locals 4

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/intercom/input/gallery/g;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    .line 250
    invoke-virtual {p0, p0}, Lcom/intercom/input/gallery/g;->getInjector(Lcom/intercom/input/gallery/g;)Lcom/intercom/input/gallery/g$a;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/intercom/input/gallery/g$a;->getLightBoxFragmentClass(Lcom/intercom/input/gallery/g;)Ljava/lang/Class;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/intercom/input/gallery/GalleryLightBoxActivity;->a(Landroid/content/Context;Lcom/intercom/input/gallery/c;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    .line 251
    sget v2, Lcom/intercom/input/gallery/o$a;->intercom_composer_slide_up:I

    sget v3, Lcom/intercom/input/gallery/o$a;->intercom_composer_stay:I

    .line 252
    invoke-static {v0, v2, v3}, Landroid/support/v4/app/b;->a(Landroid/content/Context;II)Landroid/support/v4/app/b;

    move-result-object v0

    .line 253
    invoke-virtual {v0}, Landroid/support/v4/app/b;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 254
    const/16 v2, 0xe

    invoke-virtual {p0, v1, v2, v0}, Lcom/intercom/input/gallery/g;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 255
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 364
    const/16 v0, 0xe

    if-ne p1, v0, :cond_1

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 365
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->galleryOutputListener:Lcom/intercom/input/gallery/j;

    if-eqz v0, :cond_0

    .line 366
    const-string v0, "gallery_image"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/intercom/input/gallery/c;

    .line 367
    iget-object v1, p0, Lcom/intercom/input/gallery/g;->galleryOutputListener:Lcom/intercom/input/gallery/j;

    invoke-interface {v1, v0}, Lcom/intercom/input/gallery/j;->onGalleryOutputReceived(Lcom/intercom/input/gallery/c;)V

    .line 372
    :cond_0
    :goto_0
    return-void

    .line 370
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/intercom/composer/b/c;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 110
    invoke-super {p0, p1}, Lcom/intercom/composer/b/c;->onAttach(Landroid/content/Context;)V

    .line 111
    instance-of v0, p1, Lcom/intercom/input/gallery/j;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 112
    check-cast v0, Lcom/intercom/input/gallery/j;

    iput-object v0, p0, Lcom/intercom/input/gallery/g;->galleryOutputListener:Lcom/intercom/input/gallery/j;

    .line 114
    :cond_0
    instance-of v0, p1, Lcom/intercom/input/gallery/f;

    if-eqz v0, :cond_1

    .line 115
    check-cast p1, Lcom/intercom/input/gallery/f;

    iput-object p1, p0, Lcom/intercom/input/gallery/g;->galleryInputExpandedListener:Lcom/intercom/input/gallery/f;

    .line 117
    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 120
    invoke-super {p0, p1}, Lcom/intercom/composer/b/c;->onCreate(Landroid/os/Bundle;)V

    .line 122
    invoke-virtual {p0}, Lcom/intercom/input/gallery/g;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 123
    if-eqz v0, :cond_0

    .line 124
    const-string v1, "expanded"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/intercom/input/gallery/g;->expanded:Z

    .line 127
    :cond_0
    iget-boolean v0, p0, Lcom/intercom/input/gallery/g;->expanded:Z

    if-eqz v0, :cond_1

    .line 128
    invoke-virtual {p0}, Lcom/intercom/input/gallery/g;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/intercom/input/gallery/o$e;->intercom_composer_expanded_column_count:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 129
    new-instance v1, Landroid/support/v7/widget/GridLayoutManager;

    invoke-virtual {p0}, Lcom/intercom/input/gallery/g;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/support/v7/widget/GridLayoutManager;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/intercom/input/gallery/g;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    .line 134
    :goto_0
    invoke-virtual {p0, p0}, Lcom/intercom/input/gallery/g;->getInjector(Lcom/intercom/input/gallery/g;)Lcom/intercom/input/gallery/g$a;

    move-result-object v0

    iput-object v0, p0, Lcom/intercom/input/gallery/g;->injector:Lcom/intercom/input/gallery/g$a;

    .line 136
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->injector:Lcom/intercom/input/gallery/g$a;

    invoke-interface {v0, p0}, Lcom/intercom/input/gallery/g$a;->getDataSource(Lcom/intercom/input/gallery/g;)Lcom/intercom/input/gallery/e;

    move-result-object v0

    iput-object v0, p0, Lcom/intercom/input/gallery/g;->dataSource:Lcom/intercom/input/gallery/e;

    .line 137
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->dataSource:Lcom/intercom/input/gallery/e;

    iget-object v1, p0, Lcom/intercom/input/gallery/g;->dataListener:Lcom/intercom/input/gallery/e$a;

    invoke-interface {v0, v1}, Lcom/intercom/input/gallery/e;->setListener(Lcom/intercom/input/gallery/e$a;)V

    .line 139
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->injector:Lcom/intercom/input/gallery/g$a;

    invoke-interface {v0, p0}, Lcom/intercom/input/gallery/g$a;->getImageLoader(Lcom/intercom/input/gallery/g;)Lcom/intercom/composer/e;

    move-result-object v0

    iput-object v0, p0, Lcom/intercom/input/gallery/g;->imageLoader:Lcom/intercom/composer/e;

    .line 141
    new-instance v0, Lcom/intercom/input/gallery/adapter/a;

    iget-object v1, p0, Lcom/intercom/input/gallery/g;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    invoke-direct {v0, v1, p0}, Lcom/intercom/input/gallery/adapter/a;-><init>(Landroid/support/v7/widget/LinearLayoutManager;Lcom/intercom/input/gallery/adapter/b;)V

    iput-object v0, p0, Lcom/intercom/input/gallery/g;->endlessRecyclerScrollListener:Lcom/intercom/input/gallery/adapter/a;

    .line 142
    new-instance v0, Lcom/intercom/input/gallery/adapter/c;

    invoke-virtual {p0}, Lcom/intercom/input/gallery/g;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v2, p0, Lcom/intercom/input/gallery/g;->galleryImages:Ljava/util/List;

    iget-boolean v3, p0, Lcom/intercom/input/gallery/g;->expanded:Z

    iget-object v5, p0, Lcom/intercom/input/gallery/g;->imageLoader:Lcom/intercom/composer/e;

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/intercom/input/gallery/adapter/c;-><init>(Landroid/view/LayoutInflater;Ljava/util/List;ZLcom/intercom/input/gallery/adapter/e;Lcom/intercom/composer/e;)V

    iput-object v0, p0, Lcom/intercom/input/gallery/g;->recyclerAdapter:Lcom/intercom/input/gallery/adapter/c;

    .line 144
    return-void

    .line 131
    :cond_1
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/intercom/input/gallery/g;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, v2, v2}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    iput-object v0, p0, Lcom/intercom/input/gallery/g;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/intercom/input/gallery/g;->getLayoutRes()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 148
    sget v0, Lcom/intercom/input/gallery/o$d;->gallery_root_view:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 149
    sget v1, Lcom/intercom/input/gallery/o$d;->gallery_recycler_view:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView;

    iput-object v1, p0, Lcom/intercom/input/gallery/g;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 150
    sget v1, Lcom/intercom/input/gallery/o$d;->gallery_empty_view:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/intercom/input/gallery/EmptyView;

    iput-object v1, p0, Lcom/intercom/input/gallery/g;->emptyLayout:Lcom/intercom/input/gallery/EmptyView;

    .line 151
    sget v1, Lcom/intercom/input/gallery/o$d;->gallery_content_layout:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/intercom/input/gallery/g;->contentLayout:Landroid/widget/FrameLayout;

    .line 153
    iget-boolean v1, p0, Lcom/intercom/input/gallery/g;->expanded:Z

    if-eqz v1, :cond_0

    .line 154
    iget-object v1, p0, Lcom/intercom/input/gallery/g;->injector:Lcom/intercom/input/gallery/g$a;

    invoke-direct {p0, v1, v0}, Lcom/intercom/input/gallery/g;->setUpAppBar(Lcom/intercom/input/gallery/g$a;Landroid/view/ViewGroup;)V

    .line 155
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lcom/intercom/input/gallery/k;

    sget v3, Lcom/intercom/input/gallery/o$c;->intercom_composer_toolbar_height:I

    invoke-direct {v1, v3}, Lcom/intercom/input/gallery/k;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$g;)V

    .line 160
    :goto_0
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->emptyLayout:Lcom/intercom/input/gallery/EmptyView;

    new-instance v1, Lcom/intercom/input/gallery/g$3;

    invoke-direct {v1, p0}, Lcom/intercom/input/gallery/g$3;-><init>(Lcom/intercom/input/gallery/g;)V

    invoke-virtual {v0, v1}, Lcom/intercom/input/gallery/EmptyView;->setActionButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 176
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->emptyLayout:Lcom/intercom/input/gallery/EmptyView;

    iget-object v1, p0, Lcom/intercom/input/gallery/g;->injector:Lcom/intercom/input/gallery/g$a;

    invoke-virtual {p0}, Lcom/intercom/input/gallery/g;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v1, v3}, Lcom/intercom/input/gallery/g$a;->getThemeColor(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/intercom/input/gallery/EmptyView;->setThemeColor(I)V

    .line 178
    return-object v2

    .line 157
    :cond_0
    iget-object v1, p0, Lcom/intercom/input/gallery/g;->injector:Lcom/intercom/input/gallery/g$a;

    invoke-direct {p0, v1, v0}, Lcom/intercom/input/gallery/g;->setUpPreviewViews(Lcom/intercom/input/gallery/g$a;Landroid/view/ViewGroup;)V

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 229
    invoke-super {p0}, Lcom/intercom/composer/b/c;->onDestroyView()V

    .line 230
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/intercom/input/gallery/g;->endlessRecyclerScrollListener:Lcom/intercom/input/gallery/adapter/a;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/support/v7/widget/RecyclerView$m;)V

    .line 231
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$h;)V

    .line 232
    return-void
.end method

.method public onImageClicked(Landroid/support/v7/widget/RecyclerView$w;)V
    .locals 2

    .prologue
    .line 241
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$w;->getAdapterPosition()I

    move-result v0

    .line 242
    const/4 v1, -0x1

    if-le v0, v1, :cond_0

    iget-object v1, p0, Lcom/intercom/input/gallery/g;->recyclerAdapter:Lcom/intercom/input/gallery/adapter/c;

    invoke-virtual {v1}, Lcom/intercom/input/gallery/adapter/c;->getItemCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 243
    iget-object v1, p0, Lcom/intercom/input/gallery/g;->recyclerAdapter:Lcom/intercom/input/gallery/adapter/c;

    invoke-virtual {v1, v0}, Lcom/intercom/input/gallery/adapter/c;->a(I)Lcom/intercom/input/gallery/c;

    move-result-object v0

    .line 244
    invoke-virtual {p0, v0}, Lcom/intercom/input/gallery/g;->launchLightBoxActivity(Lcom/intercom/input/gallery/c;)V

    .line 246
    :cond_0
    return-void
.end method

.method public onInputDeselected()V
    .locals 0

    .prologue
    .line 347
    return-void
.end method

.method public onInputReselected()V
    .locals 0

    .prologue
    .line 345
    return-void
.end method

.method public onInputSelected()V
    .locals 1

    .prologue
    .line 305
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/intercom/input/gallery/g;->checkPermissionAndFetchImages(Z)V

    .line 306
    return-void
.end method

.method public onLoadMore()V
    .locals 3

    .prologue
    .line 235
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->galleryImages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/intercom/input/gallery/g;->dataSource:Lcom/intercom/input/gallery/e;

    invoke-interface {v0}, Lcom/intercom/input/gallery/e;->isLoading()Z

    move-result v0

    if-nez v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->dataSource:Lcom/intercom/input/gallery/e;

    iget-object v1, p0, Lcom/intercom/input/gallery/g;->galleryImages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/intercom/input/gallery/e;->getImages(ILjava/lang/String;)V

    .line 238
    :cond_0
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 1

    .prologue
    .line 259
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/intercom/input/gallery/g;->checkPermissionAndFetchImages(Z)V

    .line 260
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 215
    invoke-super {p0, p1, p2}, Lcom/intercom/composer/b/c;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 217
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/intercom/input/gallery/g;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$h;)V

    .line 218
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/intercom/input/gallery/g;->recyclerAdapter:Lcom/intercom/input/gallery/adapter/c;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 219
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/intercom/input/gallery/g;->endlessRecyclerScrollListener:Lcom/intercom/input/gallery/adapter/a;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$m;)V

    .line 221
    iget-boolean v0, p0, Lcom/intercom/input/gallery/g;->expanded:Z

    if-eqz v0, :cond_0

    .line 222
    invoke-virtual {p0}, Lcom/intercom/input/gallery/g;->onInputSelected()V

    .line 225
    :cond_0
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->endlessRecyclerScrollListener:Lcom/intercom/input/gallery/adapter/a;

    iget-object v1, p0, Lcom/intercom/input/gallery/g;->dataSource:Lcom/intercom/input/gallery/e;

    invoke-interface {v1}, Lcom/intercom/input/gallery/e;->getCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/intercom/input/gallery/adapter/a;->a(I)V

    .line 226
    return-void
.end method

.method protected passDataOnViewCreated(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 302
    return-void
.end method

.method public setGalleryExpandedListener(Lcom/intercom/input/gallery/f;)V
    .locals 0

    .prologue
    .line 354
    iput-object p1, p0, Lcom/intercom/input/gallery/g;->galleryInputExpandedListener:Lcom/intercom/input/gallery/f;

    .line 355
    return-void
.end method

.method public setGalleryListener(Lcom/intercom/input/gallery/j;)V
    .locals 0

    .prologue
    .line 350
    iput-object p1, p0, Lcom/intercom/input/gallery/g;->galleryOutputListener:Lcom/intercom/input/gallery/j;

    .line 351
    return-void
.end method

.method showEmptyOrPermissionScreen(I)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/16 v3, 0x8

    .line 263
    packed-switch p1, :pswitch_data_0

    .line 292
    :goto_0
    return-void

    .line 266
    :pswitch_0
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->emptyLayout:Lcom/intercom/input/gallery/EmptyView;

    invoke-virtual {v0, v2}, Lcom/intercom/input/gallery/EmptyView;->setVisibility(I)V

    .line 267
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->emptyLayout:Lcom/intercom/input/gallery/EmptyView;

    sget v1, Lcom/intercom/input/gallery/o$g;->intercom_photo_access_denied:I

    invoke-virtual {v0, v1}, Lcom/intercom/input/gallery/EmptyView;->setTitle(I)V

    .line 268
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->emptyLayout:Lcom/intercom/input/gallery/EmptyView;

    sget v1, Lcom/intercom/input/gallery/o$g;->intercom_allow_storage_access:I

    invoke-virtual {v0, v1}, Lcom/intercom/input/gallery/EmptyView;->setSubtitle(I)V

    .line 269
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->emptyLayout:Lcom/intercom/input/gallery/EmptyView;

    invoke-virtual {v0, v2}, Lcom/intercom/input/gallery/EmptyView;->setActionButtonVisibility(I)V

    .line 270
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->contentLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0

    .line 273
    :pswitch_1
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->emptyLayout:Lcom/intercom/input/gallery/EmptyView;

    invoke-virtual {v0, v2}, Lcom/intercom/input/gallery/EmptyView;->setVisibility(I)V

    .line 274
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->emptyLayout:Lcom/intercom/input/gallery/EmptyView;

    sget v1, Lcom/intercom/input/gallery/o$g;->intercom_access_photos:I

    invoke-virtual {v0, v1}, Lcom/intercom/input/gallery/EmptyView;->setTitle(I)V

    .line 275
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->emptyLayout:Lcom/intercom/input/gallery/EmptyView;

    sget v1, Lcom/intercom/input/gallery/o$g;->intercom_storage_access_request:I

    invoke-virtual {v0, v1}, Lcom/intercom/input/gallery/EmptyView;->setSubtitle(I)V

    .line 276
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->emptyLayout:Lcom/intercom/input/gallery/EmptyView;

    invoke-virtual {v0, v3}, Lcom/intercom/input/gallery/EmptyView;->setActionButtonVisibility(I)V

    .line 277
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->contentLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0

    .line 280
    :pswitch_2
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->galleryImages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->emptyLayout:Lcom/intercom/input/gallery/EmptyView;

    invoke-virtual {v0, v2}, Lcom/intercom/input/gallery/EmptyView;->setVisibility(I)V

    .line 282
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->emptyLayout:Lcom/intercom/input/gallery/EmptyView;

    invoke-virtual {v0, v3}, Lcom/intercom/input/gallery/EmptyView;->setActionButtonVisibility(I)V

    .line 283
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->emptyLayout:Lcom/intercom/input/gallery/EmptyView;

    iget-object v1, p0, Lcom/intercom/input/gallery/g;->injector:Lcom/intercom/input/gallery/g$a;

    invoke-virtual {p0}, Lcom/intercom/input/gallery/g;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/intercom/input/gallery/g$a;->getEmptyViewTitle(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/intercom/input/gallery/EmptyView;->setTitle(Ljava/lang/CharSequence;)V

    .line 284
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->emptyLayout:Lcom/intercom/input/gallery/EmptyView;

    iget-object v1, p0, Lcom/intercom/input/gallery/g;->injector:Lcom/intercom/input/gallery/g$a;

    invoke-virtual {p0}, Lcom/intercom/input/gallery/g;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/intercom/input/gallery/g$a;->getEmptyViewSubtitle(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/intercom/input/gallery/EmptyView;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 285
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->contentLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0

    .line 287
    :cond_0
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->emptyLayout:Lcom/intercom/input/gallery/EmptyView;

    invoke-virtual {v0, v3}, Lcom/intercom/input/gallery/EmptyView;->setVisibility(I)V

    .line 288
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->contentLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0

    .line 263
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method showErrorScreen()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 295
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->emptyLayout:Lcom/intercom/input/gallery/EmptyView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/intercom/input/gallery/EmptyView;->setVisibility(I)V

    .line 296
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->emptyLayout:Lcom/intercom/input/gallery/EmptyView;

    invoke-virtual {v0, v3}, Lcom/intercom/input/gallery/EmptyView;->setActionButtonVisibility(I)V

    .line 297
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->emptyLayout:Lcom/intercom/input/gallery/EmptyView;

    iget-object v1, p0, Lcom/intercom/input/gallery/g;->injector:Lcom/intercom/input/gallery/g$a;

    invoke-virtual {p0}, Lcom/intercom/input/gallery/g;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/intercom/input/gallery/g$a;->getErrorViewTitle(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/intercom/input/gallery/EmptyView;->setTitle(Ljava/lang/CharSequence;)V

    .line 298
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->emptyLayout:Lcom/intercom/input/gallery/EmptyView;

    iget-object v1, p0, Lcom/intercom/input/gallery/g;->injector:Lcom/intercom/input/gallery/g$a;

    invoke-virtual {p0}, Lcom/intercom/input/gallery/g;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/intercom/input/gallery/g$a;->getErrorViewSubtitle(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/intercom/input/gallery/EmptyView;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 299
    iget-object v0, p0, Lcom/intercom/input/gallery/g;->contentLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 300
    return-void
.end method
