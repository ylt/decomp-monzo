.class public Lcom/intercom/input/gallery/GalleryLightBoxActivity;
.super Landroid/support/v7/app/e;
.source "GalleryLightBoxActivity.java"


# instance fields
.field a:Lcom/intercom/input/gallery/c;

.field b:Landroid/support/v4/app/n;

.field private c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/intercom/input/gallery/i;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/support/v7/app/e;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/intercom/input/gallery/c;Ljava/lang/Class;)Landroid/content/Intent;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/intercom/input/gallery/c;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/intercom/input/gallery/i;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 24
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/intercom/input/gallery/GalleryLightBoxActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "gallery_image"

    .line 25
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "fragment_class"

    .line 26
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    .line 24
    return-object v0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 2

    .prologue
    .line 51
    invoke-super {p0}, Landroid/support/v7/app/e;->onBackPressed()V

    .line 52
    sget v0, Lcom/intercom/input/gallery/o$a;->intercom_composer_stay:I

    sget v1, Lcom/intercom/input/gallery/o$a;->intercom_composer_slide_down:I

    invoke-virtual {p0, v0, v1}, Lcom/intercom/input/gallery/GalleryLightBoxActivity;->overridePendingTransition(II)V

    .line 53
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/intercom/input/gallery/GalleryLightBoxActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 32
    invoke-super {p0, p1}, Landroid/support/v7/app/e;->onCreate(Landroid/os/Bundle;)V

    .line 33
    sget v1, Lcom/intercom/input/gallery/o$f;->intercom_composer_activity_gallery_lightbox:I

    invoke-virtual {p0, v1}, Lcom/intercom/input/gallery/GalleryLightBoxActivity;->setContentView(I)V

    .line 34
    sget v1, Lcom/intercom/input/gallery/o$b;->intercom_composer_status_bar:I

    invoke-static {v0, v1}, Lcom/intercom/composer/h;->a(Landroid/view/Window;I)V

    .line 36
    invoke-virtual {p0}, Lcom/intercom/input/gallery/GalleryLightBoxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 37
    const-string v0, "gallery_image"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/intercom/input/gallery/c;

    iput-object v0, p0, Lcom/intercom/input/gallery/GalleryLightBoxActivity;->a:Lcom/intercom/input/gallery/c;

    .line 38
    const-string v0, "fragment_class"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    invoke-static {v0}, Lcom/intercom/input/gallery/b;->a(Ljava/io/Serializable;)Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lcom/intercom/input/gallery/GalleryLightBoxActivity;->c:Ljava/lang/Class;

    .line 40
    invoke-virtual {p0}, Lcom/intercom/input/gallery/GalleryLightBoxActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v0

    iput-object v0, p0, Lcom/intercom/input/gallery/GalleryLightBoxActivity;->b:Landroid/support/v4/app/n;

    .line 41
    iget-object v0, p0, Lcom/intercom/input/gallery/GalleryLightBoxActivity;->b:Landroid/support/v4/app/n;

    sget v1, Lcom/intercom/input/gallery/o$d;->fragment_container:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/n;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/intercom/input/gallery/GalleryLightBoxActivity;->c:Ljava/lang/Class;

    invoke-static {v0}, Lcom/intercom/input/gallery/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intercom/input/gallery/i;

    .line 43
    iget-object v1, p0, Lcom/intercom/input/gallery/GalleryLightBoxActivity;->a:Lcom/intercom/input/gallery/c;

    invoke-static {v1}, Lcom/intercom/input/gallery/i;->createArgs(Lcom/intercom/input/gallery/c;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/intercom/input/gallery/i;->setArguments(Landroid/os/Bundle;)V

    .line 44
    iget-object v1, p0, Lcom/intercom/input/gallery/GalleryLightBoxActivity;->b:Landroid/support/v4/app/n;

    invoke-virtual {v1}, Landroid/support/v4/app/n;->a()Landroid/support/v4/app/t;

    move-result-object v1

    sget v2, Lcom/intercom/input/gallery/o$d;->fragment_container:I

    .line 45
    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/t;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/t;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Landroid/support/v4/app/t;->c()I

    .line 48
    :cond_0
    return-void
.end method
