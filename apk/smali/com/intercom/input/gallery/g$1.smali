.class Lcom/intercom/input/gallery/g$1;
.super Ljava/lang/Object;
.source "GalleryInputFragment.java"

# interfaces
.implements Lcom/intercom/input/gallery/e$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intercom/input/gallery/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/intercom/input/gallery/g;


# direct methods
.method constructor <init>(Lcom/intercom/input/gallery/g;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/intercom/input/gallery/g$1;->a:Lcom/intercom/input/gallery/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/intercom/input/gallery/g$1;->a:Lcom/intercom/input/gallery/g;

    invoke-virtual {v0}, Lcom/intercom/input/gallery/g;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/intercom/input/gallery/g$1;->a:Lcom/intercom/input/gallery/g;

    invoke-virtual {v0}, Lcom/intercom/input/gallery/g;->showErrorScreen()V

    .line 87
    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/intercom/input/gallery/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lcom/intercom/input/gallery/g$1;->a:Lcom/intercom/input/gallery/g;

    iget-object v0, v0, Lcom/intercom/input/gallery/g;->galleryImages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 73
    iget-object v0, p0, Lcom/intercom/input/gallery/g$1;->a:Lcom/intercom/input/gallery/g;

    iget-object v0, v0, Lcom/intercom/input/gallery/g;->galleryImages:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 74
    iget-object v0, p0, Lcom/intercom/input/gallery/g$1;->a:Lcom/intercom/input/gallery/g;

    iget-object v0, v0, Lcom/intercom/input/gallery/g;->endlessRecyclerScrollListener:Lcom/intercom/input/gallery/adapter/a;

    iget-object v1, p0, Lcom/intercom/input/gallery/g$1;->a:Lcom/intercom/input/gallery/g;

    iget-object v1, v1, Lcom/intercom/input/gallery/g;->dataSource:Lcom/intercom/input/gallery/e;

    invoke-interface {v1}, Lcom/intercom/input/gallery/e;->getCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/intercom/input/gallery/adapter/a;->a(I)V

    .line 75
    iget-object v0, p0, Lcom/intercom/input/gallery/g$1;->a:Lcom/intercom/input/gallery/g;

    iget-object v0, v0, Lcom/intercom/input/gallery/g;->recyclerAdapter:Lcom/intercom/input/gallery/adapter/c;

    invoke-virtual {v0}, Lcom/intercom/input/gallery/adapter/c;->notifyDataSetChanged()V

    .line 77
    iget-object v0, p0, Lcom/intercom/input/gallery/g$1;->a:Lcom/intercom/input/gallery/g;

    invoke-virtual {v0}, Lcom/intercom/input/gallery/g;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/intercom/input/gallery/g$1;->a:Lcom/intercom/input/gallery/g;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/intercom/input/gallery/g;->showEmptyOrPermissionScreen(I)V

    .line 80
    :cond_0
    return-void
.end method
