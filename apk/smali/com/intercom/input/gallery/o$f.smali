.class public final Lcom/intercom/input/gallery/o$f;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intercom/input/gallery/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "f"
.end annotation


# static fields
.field public static final abc_action_bar_title_item:I = 0x7f050000

.field public static final abc_action_bar_up_container:I = 0x7f050001

.field public static final abc_action_bar_view_list_nav_layout:I = 0x7f050002

.field public static final abc_action_menu_item_layout:I = 0x7f050003

.field public static final abc_action_menu_layout:I = 0x7f050004

.field public static final abc_action_mode_bar:I = 0x7f050005

.field public static final abc_action_mode_close_item_material:I = 0x7f050006

.field public static final abc_activity_chooser_view:I = 0x7f050007

.field public static final abc_activity_chooser_view_list_item:I = 0x7f050008

.field public static final abc_alert_dialog_button_bar_material:I = 0x7f050009

.field public static final abc_alert_dialog_material:I = 0x7f05000a

.field public static final abc_alert_dialog_title_material:I = 0x7f05000b

.field public static final abc_dialog_title_material:I = 0x7f05000c

.field public static final abc_expanded_menu_layout:I = 0x7f05000d

.field public static final abc_list_menu_item_checkbox:I = 0x7f05000e

.field public static final abc_list_menu_item_icon:I = 0x7f05000f

.field public static final abc_list_menu_item_layout:I = 0x7f050010

.field public static final abc_list_menu_item_radio:I = 0x7f050011

.field public static final abc_popup_menu_header_item_layout:I = 0x7f050012

.field public static final abc_popup_menu_item_layout:I = 0x7f050013

.field public static final abc_screen_content_include:I = 0x7f050014

.field public static final abc_screen_simple:I = 0x7f050015

.field public static final abc_screen_simple_overlay_action_mode:I = 0x7f050016

.field public static final abc_screen_toolbar:I = 0x7f050017

.field public static final abc_search_dropdown_item_icons_2line:I = 0x7f050018

.field public static final abc_search_view:I = 0x7f050019

.field public static final abc_select_dialog_material:I = 0x7f05001a

.field public static final intercom_composer_activity_gallery_lightbox:I = 0x7f0500c1

.field public static final intercom_composer_activity_input_full_screen:I = 0x7f0500c2

.field public static final intercom_composer_empty_view:I = 0x7f0500c4

.field public static final intercom_composer_expanded_image_list_item:I = 0x7f0500c6

.field public static final intercom_composer_fragment_composer_gallery:I = 0x7f0500c7

.field public static final intercom_composer_fragment_composer_gallery_expanded:I = 0x7f0500c8

.field public static final intercom_composer_gallery_lightbox_fragment:I = 0x7f0500ca

.field public static final intercom_composer_image_list_item:I = 0x7f0500cc

.field public static final intercom_composer_loading_view:I = 0x7f0500cf

.field public static final notification_action:I = 0x7f050114

.field public static final notification_action_tombstone:I = 0x7f050115

.field public static final notification_media_action:I = 0x7f050116

.field public static final notification_media_cancel_action:I = 0x7f050117

.field public static final notification_template_big_media:I = 0x7f050118

.field public static final notification_template_big_media_custom:I = 0x7f050119

.field public static final notification_template_big_media_narrow:I = 0x7f05011a

.field public static final notification_template_big_media_narrow_custom:I = 0x7f05011b

.field public static final notification_template_custom_big:I = 0x7f05011c

.field public static final notification_template_icon_group:I = 0x7f05011d

.field public static final notification_template_lines_media:I = 0x7f05011e

.field public static final notification_template_media:I = 0x7f05011f

.field public static final notification_template_media_custom:I = 0x7f050120

.field public static final notification_template_part_chronometer:I = 0x7f050121

.field public static final notification_template_part_time:I = 0x7f050122

.field public static final select_dialog_item_material:I = 0x7f05012c

.field public static final select_dialog_multichoice_material:I = 0x7f05012d

.field public static final select_dialog_singlechoice_material:I = 0x7f05012e

.field public static final support_simple_spinner_dropdown_item:I = 0x7f05012f
