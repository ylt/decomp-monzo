.class public Lcom/intercom/input/gallery/adapter/a;
.super Landroid/support/v7/widget/RecyclerView$m;
.source "EndlessRecyclerScrollListener.java"


# instance fields
.field private final a:Landroid/support/v7/widget/LinearLayoutManager;

.field private final b:Lcom/intercom/input/gallery/adapter/b;

.field private c:I


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/LinearLayoutManager;Lcom/intercom/input/gallery/adapter/b;)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$m;-><init>()V

    .line 12
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/intercom/input/gallery/adapter/a;->c:I

    .line 16
    iput-object p1, p0, Lcom/intercom/input/gallery/adapter/a;->a:Landroid/support/v7/widget/LinearLayoutManager;

    .line 17
    iput-object p2, p0, Lcom/intercom/input/gallery/adapter/a;->b:Lcom/intercom/input/gallery/adapter/b;

    .line 18
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 34
    iput p1, p0, Lcom/intercom/input/gallery/adapter/a;->c:I

    .line 35
    return-void
.end method

.method public onScrolled(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 3

    .prologue
    .line 21
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView$m;->onScrolled(Landroid/support/v7/widget/RecyclerView;II)V

    .line 23
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v0

    .line 24
    iget-object v1, p0, Lcom/intercom/input/gallery/adapter/a;->a:Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {v1}, Landroid/support/v7/widget/LinearLayoutManager;->F()I

    move-result v1

    .line 25
    iget-object v2, p0, Lcom/intercom/input/gallery/adapter/a;->a:Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {v2}, Landroid/support/v7/widget/LinearLayoutManager;->n()I

    move-result v2

    .line 27
    sub-int v0, v1, v0

    if-gt v0, v2, :cond_0

    iget v0, p0, Lcom/intercom/input/gallery/adapter/a;->c:I

    if-ge v1, v0, :cond_0

    .line 29
    iget-object v0, p0, Lcom/intercom/input/gallery/adapter/a;->b:Lcom/intercom/input/gallery/adapter/b;

    invoke-interface {v0}, Lcom/intercom/input/gallery/adapter/b;->onLoadMore()V

    .line 31
    :cond_0
    return-void
.end method
