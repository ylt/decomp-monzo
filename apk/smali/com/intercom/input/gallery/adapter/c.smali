.class public Lcom/intercom/input/gallery/adapter/c;
.super Landroid/support/v7/widget/RecyclerView$a;
.source "GalleryRecyclerViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$a",
        "<",
        "Lcom/intercom/input/gallery/adapter/d;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:Z

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/intercom/input/gallery/c;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/intercom/input/gallery/adapter/e;

.field private final e:Lcom/intercom/composer/e;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Ljava/util/List;ZLcom/intercom/input/gallery/adapter/e;Lcom/intercom/composer/e;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/LayoutInflater;",
            "Ljava/util/List",
            "<",
            "Lcom/intercom/input/gallery/c;",
            ">;Z",
            "Lcom/intercom/input/gallery/adapter/e;",
            "Lcom/intercom/composer/e;",
            ")V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$a;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/intercom/input/gallery/adapter/c;->a:Landroid/view/LayoutInflater;

    .line 29
    iput-object p2, p0, Lcom/intercom/input/gallery/adapter/c;->c:Ljava/util/List;

    .line 30
    iput-boolean p3, p0, Lcom/intercom/input/gallery/adapter/c;->b:Z

    .line 31
    iput-object p4, p0, Lcom/intercom/input/gallery/adapter/c;->d:Lcom/intercom/input/gallery/adapter/e;

    .line 32
    iput-object p5, p0, Lcom/intercom/input/gallery/adapter/c;->e:Lcom/intercom/composer/e;

    .line 33
    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;I)Lcom/intercom/input/gallery/adapter/d;
    .locals 3

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/intercom/input/gallery/adapter/c;->b:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/intercom/input/gallery/o$f;->intercom_composer_expanded_image_list_item:I

    .line 38
    :goto_0
    iget-object v1, p0, Lcom/intercom/input/gallery/adapter/c;->a:Landroid/view/LayoutInflater;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 39
    new-instance v1, Lcom/intercom/input/gallery/adapter/d;

    iget-object v2, p0, Lcom/intercom/input/gallery/adapter/c;->d:Lcom/intercom/input/gallery/adapter/e;

    invoke-direct {v1, v0, v2}, Lcom/intercom/input/gallery/adapter/d;-><init>(Landroid/view/View;Lcom/intercom/input/gallery/adapter/e;)V

    return-object v1

    .line 36
    :cond_0
    sget v0, Lcom/intercom/input/gallery/o$f;->intercom_composer_image_list_item:I

    goto :goto_0
.end method

.method public a(I)Lcom/intercom/input/gallery/c;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/intercom/input/gallery/adapter/c;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intercom/input/gallery/c;

    return-object v0
.end method

.method public a(Lcom/intercom/input/gallery/adapter/d;)V
    .locals 2

    .prologue
    .line 51
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$a;->onViewRecycled(Landroid/support/v7/widget/RecyclerView$w;)V

    .line 52
    iget-object v0, p0, Lcom/intercom/input/gallery/adapter/c;->e:Lcom/intercom/composer/e;

    invoke-virtual {p1}, Lcom/intercom/input/gallery/adapter/d;->a()Landroid/widget/ImageView;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/intercom/composer/e;->clear(Landroid/widget/ImageView;)V

    .line 53
    return-void
.end method

.method public a(Lcom/intercom/input/gallery/adapter/d;I)V
    .locals 3

    .prologue
    .line 43
    iget-object v1, p0, Lcom/intercom/input/gallery/adapter/c;->e:Lcom/intercom/composer/e;

    iget-object v0, p0, Lcom/intercom/input/gallery/adapter/c;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intercom/input/gallery/c;

    invoke-virtual {p1}, Lcom/intercom/input/gallery/adapter/d;->a()Landroid/widget/ImageView;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/intercom/composer/e;->loadImageIntoView(Lcom/intercom/input/gallery/c;Landroid/widget/ImageView;)V

    .line 44
    return-void
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/intercom/input/gallery/adapter/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$w;I)V
    .locals 0

    .prologue
    .line 15
    check-cast p1, Lcom/intercom/input/gallery/adapter/d;

    invoke-virtual {p0, p1, p2}, Lcom/intercom/input/gallery/adapter/c;->a(Lcom/intercom/input/gallery/adapter/d;I)V

    return-void
.end method

.method public synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$w;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0, p1, p2}, Lcom/intercom/input/gallery/adapter/c;->a(Landroid/view/ViewGroup;I)Lcom/intercom/input/gallery/adapter/d;

    move-result-object v0

    return-object v0
.end method

.method public synthetic onViewRecycled(Landroid/support/v7/widget/RecyclerView$w;)V
    .locals 0

    .prologue
    .line 15
    check-cast p1, Lcom/intercom/input/gallery/adapter/d;

    invoke-virtual {p0, p1}, Lcom/intercom/input/gallery/adapter/c;->a(Lcom/intercom/input/gallery/adapter/d;)V

    return-void
.end method
