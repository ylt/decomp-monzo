.class public Lcom/intercom/input/gallery/AnnotatedImageView;
.super Landroid/support/v7/widget/AppCompatImageView;
.source "AnnotatedImageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intercom/input/gallery/AnnotatedImageView$a;
    }
.end annotation


# instance fields
.field private a:Landroid/graphics/Path;

.field private b:Landroid/graphics/Paint;

.field private c:Landroid/graphics/Canvas;

.field private d:Landroid/graphics/Bitmap;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Path;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Paint;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private h:Z

.field private i:Landroid/graphics/Rect;

.field private j:Lcom/intercom/input/gallery/AnnotatedImageView$a;

.field private final k:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/AppCompatImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->e:Ljava/util/List;

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->f:Ljava/util/List;

    .line 33
    const/high16 v0, -0x9a0000

    iput v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->g:I

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->h:Z

    .line 35
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->i:Landroid/graphics/Rect;

    .line 85
    new-instance v0, Lcom/intercom/input/gallery/AnnotatedImageView$1;

    invoke-direct {v0, p0}, Lcom/intercom/input/gallery/AnnotatedImageView$1;-><init>(Lcom/intercom/input/gallery/AnnotatedImageView;)V

    iput-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->k:Ljava/lang/Runnable;

    .line 41
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->a:Landroid/graphics/Path;

    .line 42
    invoke-direct {p0}, Lcom/intercom/input/gallery/AnnotatedImageView;->c()V

    .line 43
    return-void
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 63
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_0

    .line 64
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Path;

    iget-object v1, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->f:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 63
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 66
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/intercom/input/gallery/AnnotatedImageView;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/intercom/input/gallery/AnnotatedImageView;->d()V

    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 46
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->b:Landroid/graphics/Paint;

    .line 47
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->b:Landroid/graphics/Paint;

    iget v1, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->g:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 48
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->b:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 49
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->b:Landroid/graphics/Paint;

    const/high16 v1, 0x42000000    # 32.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 50
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 51
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 52
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 53
    return-void
.end method

.method private d()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 92
    iget-boolean v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->h:Z

    if-nez v0, :cond_1

    .line 135
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    invoke-virtual {p0}, Lcom/intercom/input/gallery/AnnotatedImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 96
    if-eqz v0, :cond_0

    .line 99
    invoke-virtual {p0}, Lcom/intercom/input/gallery/AnnotatedImageView;->getHeight()I

    move-result v1

    .line 100
    invoke-virtual {p0}, Lcom/intercom/input/gallery/AnnotatedImageView;->getWidth()I

    move-result v2

    .line 101
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    .line 102
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 103
    mul-int v4, v1, v0

    mul-int v5, v2, v3

    if-gt v4, v5, :cond_2

    .line 114
    mul-int/2addr v0, v1

    div-int/2addr v0, v3

    .line 115
    sub-int/2addr v2, v0

    div-int/lit8 v2, v2, 0x2

    .line 116
    iget-object v3, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->i:Landroid/graphics/Rect;

    add-int/2addr v0, v2

    invoke-virtual {v3, v2, v6, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 132
    :goto_1
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->i:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget-object v1, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->i:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->d:Landroid/graphics/Bitmap;

    .line 133
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->d:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->c:Landroid/graphics/Canvas;

    .line 134
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->c:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->i:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    neg-int v1, v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->i:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_0

    .line 128
    :cond_2
    mul-int/2addr v3, v2

    div-int v0, v3, v0

    .line 129
    sub-int/2addr v1, v0

    div-int/lit8 v1, v1, 0x2

    .line 130
    iget-object v3, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->i:Landroid/graphics/Rect;

    add-int/2addr v0, v1

    invoke-virtual {v3, v6, v1, v2, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_1
.end method

.method private e()V
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->j:Lcom/intercom/input/gallery/AnnotatedImageView$a;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->j:Lcom/intercom/input/gallery/AnnotatedImageView$a;

    iget-object v1, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/intercom/input/gallery/AnnotatedImageView$a;->a(I)V

    .line 171
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 174
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 175
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 178
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->c:Landroid/graphics/Canvas;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->c:Landroid/graphics/Canvas;

    const/4 v1, 0x0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 181
    :cond_0
    invoke-virtual {p0}, Lcom/intercom/input/gallery/AnnotatedImageView;->invalidate()V

    .line 182
    invoke-direct {p0}, Lcom/intercom/input/gallery/AnnotatedImageView;->e()V

    .line 183
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    :goto_0
    return-void

    .line 207
    :cond_0
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->e:Ljava/util/List;

    iget-object v1, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 208
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->f:Ljava/util/List;

    iget-object v1, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 209
    invoke-virtual {p0}, Lcom/intercom/input/gallery/AnnotatedImageView;->invalidate()V

    .line 210
    invoke-direct {p0}, Lcom/intercom/input/gallery/AnnotatedImageView;->e()V

    goto :goto_0
.end method

.method public getAnnotationsBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->c:Landroid/graphics/Canvas;

    invoke-direct {p0, v0}, Lcom/intercom/input/gallery/AnnotatedImageView;->a(Landroid/graphics/Canvas;)V

    .line 192
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->d:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getPathCount()I
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 56
    invoke-super {p0, p1}, Landroid/support/v7/widget/AppCompatImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 57
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->i:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 58
    invoke-direct {p0, p1}, Lcom/intercom/input/gallery/AnnotatedImageView;->a(Landroid/graphics/Canvas;)V

    .line 59
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->a:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 60
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    .prologue
    .line 79
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v7/widget/AppCompatImageView;->onSizeChanged(IIII)V

    .line 80
    if-lez p1, :cond_0

    if-lez p2, :cond_0

    .line 81
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->k:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/intercom/input/gallery/AnnotatedImageView;->post(Ljava/lang/Runnable;)Z

    .line 83
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 138
    iget-boolean v1, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->h:Z

    if-nez v1, :cond_0

    .line 164
    :goto_0
    return v0

    .line 142
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 143
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 144
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 146
    :pswitch_0
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->a:Landroid/graphics/Path;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 163
    :goto_1
    invoke-virtual {p0}, Lcom/intercom/input/gallery/AnnotatedImageView;->invalidate()V

    .line 164
    const/4 v0, 0x1

    goto :goto_0

    .line 149
    :pswitch_1
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->a:Landroid/graphics/Path;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_1

    .line 152
    :pswitch_2
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->a:Landroid/graphics/Path;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 153
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->e:Ljava/util/List;

    iget-object v1, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->a:Landroid/graphics/Path;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 154
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->f:Ljava/util/List;

    iget-object v1, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->b:Landroid/graphics/Paint;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    invoke-direct {p0}, Lcom/intercom/input/gallery/AnnotatedImageView;->e()V

    .line 156
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->a:Landroid/graphics/Path;

    .line 157
    invoke-direct {p0}, Lcom/intercom/input/gallery/AnnotatedImageView;->c()V

    goto :goto_1

    .line 144
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setAnnotationEnabled(Z)V
    .locals 0

    .prologue
    .line 196
    iput-boolean p1, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->h:Z

    .line 197
    return-void
.end method

.method public setColor(I)V
    .locals 2

    .prologue
    .line 186
    iput p1, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->g:I

    .line 187
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->b:Landroid/graphics/Paint;

    iget v1, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->g:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 188
    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 74
    invoke-super {p0, p1}, Landroid/support/v7/widget/AppCompatImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 75
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->k:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/intercom/input/gallery/AnnotatedImageView;->post(Ljava/lang/Runnable;)Z

    .line 76
    return-void
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 69
    invoke-super {p0, p1}, Landroid/support/v7/widget/AppCompatImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 70
    iget-object v0, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->k:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/intercom/input/gallery/AnnotatedImageView;->post(Ljava/lang/Runnable;)Z

    .line 71
    return-void
.end method

.method public setListener(Lcom/intercom/input/gallery/AnnotatedImageView$a;)V
    .locals 0

    .prologue
    .line 214
    iput-object p1, p0, Lcom/intercom/input/gallery/AnnotatedImageView;->j:Lcom/intercom/input/gallery/AnnotatedImageView$a;

    .line 215
    return-void
.end method
