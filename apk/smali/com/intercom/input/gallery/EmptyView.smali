.class public Lcom/intercom/input/gallery/EmptyView;
.super Landroid/widget/LinearLayout;
.source "EmptyView.java"


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/Button;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:I

.field private h:I

.field private i:I

.field private j:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/intercom/input/gallery/EmptyView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 38
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/intercom/input/gallery/o$f;->intercom_composer_empty_view:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 40
    sget v0, Lcom/intercom/input/gallery/o$b;->intercom_composer_white:I

    invoke-virtual {p0, v0}, Lcom/intercom/input/gallery/EmptyView;->setBackgroundResource(I)V

    .line 41
    sget-object v0, Lcom/intercom/input/gallery/o$h;->intercom_composer_empty_view:[I

    invoke-virtual {p1, p2, v0, v3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 43
    :try_start_0
    sget v0, Lcom/intercom/input/gallery/o$h;->intercom_composer_empty_view_intercom_composer_titleText:I

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/intercom/input/gallery/EmptyView;->d:Ljava/lang/String;

    .line 44
    sget v0, Lcom/intercom/input/gallery/o$h;->intercom_composer_empty_view_intercom_composer_subtitleText:I

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/intercom/input/gallery/EmptyView;->e:Ljava/lang/String;

    .line 45
    sget v0, Lcom/intercom/input/gallery/o$h;->intercom_composer_empty_view_intercom_composer_actionButtonText:I

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/intercom/input/gallery/EmptyView;->f:Ljava/lang/String;

    .line 46
    sget v0, Lcom/intercom/input/gallery/o$h;->intercom_composer_empty_view_intercom_composer_internalPaddingTop:I

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/intercom/input/gallery/EmptyView;->g:I

    .line 47
    sget v0, Lcom/intercom/input/gallery/o$h;->intercom_composer_empty_view_intercom_composer_internalPaddingBottom:I

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/intercom/input/gallery/EmptyView;->h:I

    .line 48
    sget v0, Lcom/intercom/input/gallery/o$h;->intercom_composer_empty_view_intercom_composer_internalPaddingLeft:I

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/intercom/input/gallery/EmptyView;->i:I

    .line 49
    sget v0, Lcom/intercom/input/gallery/o$h;->intercom_composer_empty_view_intercom_composer_internalPaddingRight:I

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/intercom/input/gallery/EmptyView;->j:I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 61
    :goto_0
    return-void

    .line 50
    :catch_0
    move-exception v0

    .line 51
    :try_start_1
    const-string v0, ""

    iput-object v0, p0, Lcom/intercom/input/gallery/EmptyView;->d:Ljava/lang/String;

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/intercom/input/gallery/EmptyView;->e:Ljava/lang/String;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/intercom/input/gallery/EmptyView;->f:Ljava/lang/String;

    .line 54
    const/4 v0, 0x0

    iput v0, p0, Lcom/intercom/input/gallery/EmptyView;->g:I

    .line 55
    const/4 v0, 0x0

    iput v0, p0, Lcom/intercom/input/gallery/EmptyView;->h:I

    .line 56
    const/4 v0, 0x0

    iput v0, p0, Lcom/intercom/input/gallery/EmptyView;->i:I

    .line 57
    const/4 v0, 0x0

    iput v0, p0, Lcom/intercom/input/gallery/EmptyView;->j:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 59
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 5

    .prologue
    .line 64
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 65
    sget v0, Lcom/intercom/input/gallery/o$d;->empty_text_title:I

    invoke-virtual {p0, v0}, Lcom/intercom/input/gallery/EmptyView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/intercom/input/gallery/EmptyView;->a:Landroid/widget/TextView;

    .line 66
    sget v0, Lcom/intercom/input/gallery/o$d;->empty_text_subtitle:I

    invoke-virtual {p0, v0}, Lcom/intercom/input/gallery/EmptyView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/intercom/input/gallery/EmptyView;->b:Landroid/widget/TextView;

    .line 67
    sget v0, Lcom/intercom/input/gallery/o$d;->empty_action_button:I

    invoke-virtual {p0, v0}, Lcom/intercom/input/gallery/EmptyView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/intercom/input/gallery/EmptyView;->c:Landroid/widget/Button;

    .line 69
    iget-object v0, p0, Lcom/intercom/input/gallery/EmptyView;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/intercom/input/gallery/EmptyView;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    iget-object v0, p0, Lcom/intercom/input/gallery/EmptyView;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/intercom/input/gallery/EmptyView;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    iget-object v0, p0, Lcom/intercom/input/gallery/EmptyView;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/intercom/input/gallery/EmptyView;->c:Landroid/widget/Button;

    iget-object v1, p0, Lcom/intercom/input/gallery/EmptyView;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 77
    :goto_0
    sget v0, Lcom/intercom/input/gallery/o$d;->empty_view_layout:I

    invoke-virtual {p0, v0}, Lcom/intercom/input/gallery/EmptyView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 78
    iget v1, p0, Lcom/intercom/input/gallery/EmptyView;->i:I

    iget v2, p0, Lcom/intercom/input/gallery/EmptyView;->g:I

    iget v3, p0, Lcom/intercom/input/gallery/EmptyView;->j:I

    iget v4, p0, Lcom/intercom/input/gallery/EmptyView;->h:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 79
    return-void

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/intercom/input/gallery/EmptyView;->c:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public setActionButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/intercom/input/gallery/EmptyView;->c:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    return-void
.end method

.method public setActionButtonVisibility(I)V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/intercom/input/gallery/EmptyView;->c:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setVisibility(I)V

    .line 103
    return-void
.end method

.method public setSubtitle(I)V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/intercom/input/gallery/EmptyView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 99
    return-void
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/intercom/input/gallery/EmptyView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    return-void
.end method

.method public setThemeColor(I)V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/intercom/input/gallery/EmptyView;->c:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setBackgroundColor(I)V

    .line 107
    return-void
.end method

.method public setTitle(I)V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/intercom/input/gallery/EmptyView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 95
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/intercom/input/gallery/EmptyView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    return-void
.end method
