.class public Lcom/intercom/input/gallery/m;
.super Ljava/lang/Object;
.source "LocalImagesDataSource.java"

# interfaces
.implements Lcom/intercom/input/gallery/e;


# instance fields
.field private a:Lcom/intercom/input/gallery/e$a;

.field private b:Lcom/intercom/input/gallery/n;

.field private c:Landroid/content/Context;

.field private d:Lcom/intercom/input/gallery/h;

.field private e:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/intercom/input/gallery/n;Lcom/intercom/input/gallery/h;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/intercom/input/gallery/m;->c:Landroid/content/Context;

    .line 47
    iput-object p3, p0, Lcom/intercom/input/gallery/m;->d:Lcom/intercom/input/gallery/h;

    .line 48
    iput-object p2, p0, Lcom/intercom/input/gallery/m;->b:Lcom/intercom/input/gallery/n;

    .line 49
    return-void
.end method

.method private a(Landroid/database/Cursor;Ljava/lang/String;)Landroid/graphics/Point;
    .locals 3

    .prologue
    .line 138
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 139
    const-string v0, "height"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 140
    const-string v0, "width"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 150
    :goto_0
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    return-object v2

    .line 142
    :cond_0
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 144
    const/4 v0, 0x1

    iput-boolean v0, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 145
    invoke-static {p2, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 146
    iget v0, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 147
    iget v1, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    goto :goto_0
.end method

.method public static a(Lcom/intercom/input/gallery/g;)Lcom/intercom/input/gallery/e;
    .locals 3

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/intercom/input/gallery/g;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    .line 40
    invoke-static {v0}, Lcom/intercom/input/gallery/n;->a(Landroid/app/Activity;)Lcom/intercom/input/gallery/n;

    move-result-object v1

    .line 41
    new-instance v2, Lcom/intercom/input/gallery/m;

    invoke-direct {v2, v0, v1, p0}, Lcom/intercom/input/gallery/m;-><init>(Landroid/content/Context;Lcom/intercom/input/gallery/n;Lcom/intercom/input/gallery/h;)V

    return-object v2
.end method


# virtual methods
.method a(Landroid/database/Cursor;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/intercom/input/gallery/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 109
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 111
    :cond_0
    const-string v1, "_data"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 112
    const-string v2, "mime_type"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 113
    const-string v3, "title"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 114
    const-string v4, "_size"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 116
    if-eqz v1, :cond_1

    if-eqz v3, :cond_1

    if-eqz v2, :cond_1

    .line 117
    invoke-direct {p0, p1, v1}, Lcom/intercom/input/gallery/m;->a(Landroid/database/Cursor;Ljava/lang/String;)Landroid/graphics/Point;

    move-result-object v5

    .line 118
    new-instance v6, Lcom/intercom/input/gallery/c$a;

    invoke-direct {v6}, Lcom/intercom/input/gallery/c$a;-><init>()V

    .line 119
    invoke-virtual {v6, v3}, Lcom/intercom/input/gallery/c$a;->a(Ljava/lang/String;)Lcom/intercom/input/gallery/c$a;

    move-result-object v3

    .line 120
    invoke-virtual {v3, v1}, Lcom/intercom/input/gallery/c$a;->c(Ljava/lang/String;)Lcom/intercom/input/gallery/c$a;

    move-result-object v1

    .line 121
    invoke-virtual {v1, v2}, Lcom/intercom/input/gallery/c$a;->b(Ljava/lang/String;)Lcom/intercom/input/gallery/c$a;

    move-result-object v1

    iget v2, v5, Landroid/graphics/Point;->x:I

    .line 122
    invoke-virtual {v1, v2}, Lcom/intercom/input/gallery/c$a;->a(I)Lcom/intercom/input/gallery/c$a;

    move-result-object v1

    iget v2, v5, Landroid/graphics/Point;->y:I

    .line 123
    invoke-virtual {v1, v2}, Lcom/intercom/input/gallery/c$a;->b(I)Lcom/intercom/input/gallery/c$a;

    move-result-object v1

    .line 124
    invoke-virtual {v1, v4}, Lcom/intercom/input/gallery/c$a;->c(I)Lcom/intercom/input/gallery/c$a;

    move-result-object v1

    .line 125
    invoke-virtual {v1}, Lcom/intercom/input/gallery/c$a;->a()Lcom/intercom/input/gallery/c;

    move-result-object v1

    .line 126
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 128
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 130
    :cond_2
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 131
    return-object v0
.end method

.method public getCount()I
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 75
    invoke-virtual {p0}, Lcom/intercom/input/gallery/m;->getPermissionStatus()I

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    :goto_0
    return v6

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/intercom/input/gallery/m;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 82
    if-eqz v1, :cond_1

    .line 83
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 84
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_1
    move v6, v0

    .line 86
    goto :goto_0

    :cond_1
    move v0, v6

    goto :goto_1
.end method

.method public getImages(ILjava/lang/String;)V
    .locals 8

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 90
    iput-boolean v3, p0, Lcom/intercom/input/gallery/m;->e:Z

    .line 92
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x7

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_data"

    aput-object v0, v2, v7

    const-string v0, "date_added"

    aput-object v0, v2, v3

    const-string v0, "mime_type"

    aput-object v0, v2, v4

    const-string v0, "title"

    aput-object v0, v2, v5

    const-string v0, "height"

    aput-object v0, v2, v6

    const/4 v0, 0x5

    const-string v1, "width"

    aput-object v1, v2, v0

    const/4 v0, 0x6

    const-string v1, "_size"

    aput-object v1, v2, v0

    .line 96
    :goto_0
    iget-object v0, p0, Lcom/intercom/input/gallery/m;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "date_added DESC LIMIT 50 OFFSET "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 99
    iput-boolean v7, p0, Lcom/intercom/input/gallery/m;->e:Z

    .line 100
    if-nez v0, :cond_1

    .line 101
    iget-object v0, p0, Lcom/intercom/input/gallery/m;->a:Lcom/intercom/input/gallery/e$a;

    invoke-interface {v0}, Lcom/intercom/input/gallery/e$a;->a()V

    .line 105
    :goto_1
    return-void

    .line 92
    :cond_0
    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_data"

    aput-object v0, v2, v7

    const-string v0, "date_added"

    aput-object v0, v2, v3

    const-string v0, "mime_type"

    aput-object v0, v2, v4

    const-string v0, "title"

    aput-object v0, v2, v5

    const-string v0, "_size"

    aput-object v0, v2, v6

    goto :goto_0

    .line 103
    :cond_1
    iget-object v1, p0, Lcom/intercom/input/gallery/m;->a:Lcom/intercom/input/gallery/e$a;

    invoke-virtual {p0, v0}, Lcom/intercom/input/gallery/m;->a(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/intercom/input/gallery/e$a;->a(Ljava/util/List;)V

    goto :goto_1
.end method

.method public getPermissionStatus()I
    .locals 2

    .prologue
    .line 53
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    .line 54
    iget-object v0, p0, Lcom/intercom/input/gallery/m;->b:Lcom/intercom/input/gallery/n;

    const-string v1, "android.permission.READ_EXTERNAL_STORAGE"

    invoke-virtual {v0, v1}, Lcom/intercom/input/gallery/n;->a(Ljava/lang/String;)I

    move-result v0

    .line 57
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLoading()Z
    .locals 1

    .prologue
    .line 154
    iget-boolean v0, p0, Lcom/intercom/input/gallery/m;->e:Z

    return v0
.end method

.method public requestPermission()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 61
    iget-object v0, p0, Lcom/intercom/input/gallery/m;->b:Lcom/intercom/input/gallery/n;

    invoke-virtual {v0, v4}, Lcom/intercom/input/gallery/n;->a(Z)V

    .line 64
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    .line 65
    iget-object v0, p0, Lcom/intercom/input/gallery/m;->d:Lcom/intercom/input/gallery/h;

    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v3, v1, v2

    invoke-interface {v0, v1, v4}, Lcom/intercom/input/gallery/h;->requestPermissions([Ljava/lang/String;I)V

    .line 68
    :cond_0
    return-void
.end method

.method public setListener(Lcom/intercom/input/gallery/e$a;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/intercom/input/gallery/m;->a:Lcom/intercom/input/gallery/e$a;

    .line 72
    return-void
.end method
