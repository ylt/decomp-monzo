.class public final Lcom/intercom/input/gallery/c$a;
.super Ljava/lang/Object;
.source "GalleryImage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intercom/input/gallery/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:I

.field private h:I

.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static f(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 216
    if-nez p0, :cond_0

    const-string p0, ""

    :cond_0
    return-object p0
.end method


# virtual methods
.method public a(I)Lcom/intercom/input/gallery/c$a;
    .locals 0

    .prologue
    .line 183
    iput p1, p0, Lcom/intercom/input/gallery/c$a;->f:I

    .line 184
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/intercom/input/gallery/c$a;
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lcom/intercom/input/gallery/c$a;->a:Ljava/lang/String;

    .line 159
    return-object p0
.end method

.method public a(Z)Lcom/intercom/input/gallery/c$a;
    .locals 0

    .prologue
    .line 198
    iput-boolean p1, p0, Lcom/intercom/input/gallery/c$a;->i:Z

    .line 199
    return-object p0
.end method

.method public a()Lcom/intercom/input/gallery/c;
    .locals 10

    .prologue
    .line 203
    new-instance v0, Lcom/intercom/input/gallery/c;

    iget-object v1, p0, Lcom/intercom/input/gallery/c$a;->a:Ljava/lang/String;

    .line 204
    invoke-static {v1}, Lcom/intercom/input/gallery/c$a;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/intercom/input/gallery/c$a;->b:Ljava/lang/String;

    .line 205
    invoke-static {v2}, Lcom/intercom/input/gallery/c$a;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/intercom/input/gallery/c$a;->c:Ljava/lang/String;

    .line 206
    invoke-static {v3}, Lcom/intercom/input/gallery/c$a;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/intercom/input/gallery/c$a;->d:Ljava/lang/String;

    .line 207
    invoke-static {v4}, Lcom/intercom/input/gallery/c$a;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/intercom/input/gallery/c$a;->e:Ljava/lang/String;

    .line 208
    invoke-static {v5}, Lcom/intercom/input/gallery/c$a;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget v6, p0, Lcom/intercom/input/gallery/c$a;->f:I

    iget v7, p0, Lcom/intercom/input/gallery/c$a;->g:I

    iget v8, p0, Lcom/intercom/input/gallery/c$a;->h:I

    iget-boolean v9, p0, Lcom/intercom/input/gallery/c$a;->i:Z

    invoke-direct/range {v0 .. v9}, Lcom/intercom/input/gallery/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIZ)V

    .line 203
    return-object v0
.end method

.method public b(I)Lcom/intercom/input/gallery/c$a;
    .locals 0

    .prologue
    .line 188
    iput p1, p0, Lcom/intercom/input/gallery/c$a;->g:I

    .line 189
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/intercom/input/gallery/c$a;
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lcom/intercom/input/gallery/c$a;->b:Ljava/lang/String;

    .line 164
    return-object p0
.end method

.method public c(I)Lcom/intercom/input/gallery/c$a;
    .locals 0

    .prologue
    .line 193
    iput p1, p0, Lcom/intercom/input/gallery/c$a;->h:I

    .line 194
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/intercom/input/gallery/c$a;
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, Lcom/intercom/input/gallery/c$a;->c:Ljava/lang/String;

    .line 169
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/intercom/input/gallery/c$a;
    .locals 0

    .prologue
    .line 173
    iput-object p1, p0, Lcom/intercom/input/gallery/c$a;->d:Ljava/lang/String;

    .line 174
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/intercom/input/gallery/c$a;
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lcom/intercom/input/gallery/c$a;->e:Ljava/lang/String;

    .line 179
    return-object p0
.end method
