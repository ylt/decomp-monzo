.class Lcom/intercom/input/gallery/n;
.super Ljava/lang/Object;
.source "PermissionHelper.java"


# instance fields
.field private final a:Landroid/content/SharedPreferences;

.field private final b:Landroid/app/Activity;


# direct methods
.method constructor <init>(Landroid/app/Activity;Landroid/content/SharedPreferences;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/intercom/input/gallery/n;->b:Landroid/app/Activity;

    .line 29
    iput-object p2, p0, Lcom/intercom/input/gallery/n;->a:Landroid/content/SharedPreferences;

    .line 30
    return-void
.end method

.method static a(Landroid/app/Activity;)Lcom/intercom/input/gallery/n;
    .locals 2

    .prologue
    .line 23
    const-string v0, "intercom_composer_permission_status_prefs"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 24
    new-instance v1, Lcom/intercom/input/gallery/n;

    invoke-direct {v1, p0, v0}, Lcom/intercom/input/gallery/n;-><init>(Landroid/app/Activity;Landroid/content/SharedPreferences;)V

    return-object v1
.end method


# virtual methods
.method a(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 33
    iget-object v1, p0, Lcom/intercom/input/gallery/n;->b:Landroid/app/Activity;

    invoke-static {v1, p1}, Landroid/support/v4/content/a;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 44
    :goto_0
    return v0

    .line 37
    :cond_0
    iget-object v1, p0, Lcom/intercom/input/gallery/n;->b:Landroid/app/Activity;

    invoke-static {v1, p1}, Landroid/support/v4/app/a;->a(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 38
    const/4 v0, 0x1

    goto :goto_0

    .line 41
    :cond_1
    iget-object v1, p0, Lcom/intercom/input/gallery/n;->a:Landroid/content/SharedPreferences;

    const-string v2, "asked_for_permission"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 42
    const/4 v0, 0x2

    goto :goto_0

    .line 44
    :cond_2
    const/4 v0, 0x3

    goto :goto_0
.end method

.method a(Z)V
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/intercom/input/gallery/n;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "asked_for_permission"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 50
    return-void
.end method
