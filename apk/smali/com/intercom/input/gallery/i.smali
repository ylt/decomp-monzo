.class public abstract Lcom/intercom/input/gallery/i;
.super Landroid/support/v4/app/Fragment;
.source "GalleryLightBoxFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intercom/input/gallery/i$a;
    }
.end annotation


# static fields
.field private static final COLOR_PICKER_DIAMETER_DP:I = 0x30

.field private static final COLOR_PICKER_PADDING_DP:I = 0xc

.field private static final HIGHLIGHTED_COLOR_SCALE:F = 2.5f

.field private static final INITIAL_UNDO_BUTTON_SCALE:F = 0.9f

.field private static final INTRO_ANIMATION_DURATION_MS:I = 0x320

.field private static final INTRO_ANIMATION_STAGGER_DELAY_MS:I = 0x32

.field private static final UNSELECTED_COLOR_ALPHA:F = 0.3f


# instance fields
.field private annotatedImageView:Lcom/intercom/input/gallery/AnnotatedImageView;

.field private annotationColors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field galleryImage:Lcom/intercom/input/gallery/c;

.field private imageLoader:Lcom/intercom/composer/e;

.field private isAnnotationEnabled:Z

.field private selectedColorView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/intercom/input/gallery/i;)Lcom/intercom/input/gallery/AnnotatedImageView;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/intercom/input/gallery/i;->annotatedImageView:Lcom/intercom/input/gallery/AnnotatedImageView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/intercom/input/gallery/i;)Landroid/view/View;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/intercom/input/gallery/i;->selectedColorView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$102(Lcom/intercom/input/gallery/i;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/intercom/input/gallery/i;->selectedColorView:Landroid/view/View;

    return-object p1
.end method

.method public static createArgs(Lcom/intercom/input/gallery/c;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 47
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 48
    const-string v1, "gallery_image"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 49
    return-object v0
.end method

.method private dpToPx(I)I
    .locals 2

    .prologue
    .line 226
    int-to-float v0, p1

    invoke-virtual {p0}, Lcom/intercom/input/gallery/i;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private setupAnnotations(Landroid/view/View;ZLjava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/16 v0, 0x8

    const v4, 0x3f666666    # 0.9f

    .line 83
    sget v1, Lcom/intercom/input/gallery/o$d;->lightbox_undo_button:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 84
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 85
    if-eqz p2, :cond_0

    .line 86
    iget-object v2, p0, Lcom/intercom/input/gallery/i;->annotatedImageView:Lcom/intercom/input/gallery/AnnotatedImageView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/intercom/input/gallery/AnnotatedImageView;->setAnnotationEnabled(Z)V

    .line 87
    new-instance v2, Lcom/intercom/input/gallery/i$1;

    invoke-direct {v2, p0}, Lcom/intercom/input/gallery/i$1;-><init>(Lcom/intercom/input/gallery/i;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 93
    invoke-virtual {v1, v4}, Landroid/view/View;->setScaleX(F)V

    .line 94
    invoke-virtual {v1, v4}, Landroid/view/View;->setScaleY(F)V

    .line 95
    iget-object v2, p0, Lcom/intercom/input/gallery/i;->annotatedImageView:Lcom/intercom/input/gallery/AnnotatedImageView;

    new-instance v3, Lcom/intercom/input/gallery/i$2;

    invoke-direct {v3, p0, v1}, Lcom/intercom/input/gallery/i$2;-><init>(Lcom/intercom/input/gallery/i;Landroid/view/View;)V

    invoke-virtual {v2, v3}, Lcom/intercom/input/gallery/AnnotatedImageView;->setListener(Lcom/intercom/input/gallery/AnnotatedImageView$a;)V

    .line 100
    iget-object v2, p0, Lcom/intercom/input/gallery/i;->annotatedImageView:Lcom/intercom/input/gallery/AnnotatedImageView;

    invoke-direct {p0, p1, p3, v2, v1}, Lcom/intercom/input/gallery/i;->setupColorPickers(Landroid/view/View;Ljava/util/List;Lcom/intercom/input/gallery/AnnotatedImageView;Landroid/view/View;)V

    .line 103
    :cond_0
    sget v1, Lcom/intercom/input/gallery/o$d;->lightbox_annotation_controls_space:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz p2, :cond_1

    const/4 v0, 0x0

    .line 104
    :cond_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 105
    return-void
.end method

.method private setupColorPickers(Landroid/view/View;Ljava/util/List;Lcom/intercom/input/gallery/AnnotatedImageView;Landroid/view/View;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/intercom/input/gallery/AnnotatedImageView;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 109
    sget v2, Lcom/intercom/input/gallery/o$d;->lightbox_button_layout:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v10, v2

    check-cast v10, Landroid/view/ViewGroup;

    .line 110
    move-object/from16 v0, p4

    invoke-virtual {v10, v0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v12

    .line 111
    const/16 v2, 0x30

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/intercom/input/gallery/i;->dpToPx(I)I

    move-result v13

    .line 112
    const/16 v2, 0xc

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/intercom/input/gallery/i;->dpToPx(I)I

    move-result v14

    .line 114
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v2

    new-array v7, v2, [Landroid/view/View;

    .line 115
    new-instance v4, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v4}, Landroid/view/animation/OvershootInterpolator;-><init>()V

    .line 116
    new-instance v8, Landroid/support/v4/view/b/b;

    invoke-direct {v8}, Landroid/support/v4/view/b/b;-><init>()V

    .line 118
    const/4 v2, 0x0

    move v11, v2

    :goto_0
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v11, v2, :cond_1

    .line 119
    new-instance v15, Landroid/widget/ImageView;

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v15, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 120
    aput-object v15, v7, v11

    .line 121
    invoke-virtual {v15, v14, v14, v14, v14}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 122
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v13, v13}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v15, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 124
    move-object/from16 v0, p2

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 125
    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    .line 127
    new-instance v2, Lcom/intercom/input/gallery/i$3;

    move-object/from16 v3, p0

    move-object/from16 v5, p3

    invoke-direct/range {v2 .. v9}, Lcom/intercom/input/gallery/i$3;-><init>(Lcom/intercom/input/gallery/i;Landroid/view/animation/OvershootInterpolator;Lcom/intercom/input/gallery/AnnotatedImageView;I[Landroid/view/View;Landroid/support/v4/view/b/b;Landroid/graphics/Rect;)V

    invoke-virtual {v15, v2}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 181
    new-instance v2, Lcom/intercom/input/gallery/a;

    invoke-direct {v2, v6}, Lcom/intercom/input/gallery/a;-><init>(I)V

    invoke-virtual {v15, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 182
    const/4 v2, 0x0

    invoke-virtual {v15, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 183
    invoke-virtual {v15}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    if-nez v11, :cond_0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 184
    :goto_1
    invoke-virtual {v3, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const-wide/16 v16, 0x320

    .line 185
    move-wide/from16 v0, v16

    invoke-virtual {v2, v0, v1}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    mul-int/lit8 v3, v11, 0x32

    int-to-long v0, v3

    move-wide/from16 v16, v0

    .line 186
    move-wide/from16 v0, v16

    invoke-virtual {v2, v0, v1}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    .line 187
    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 189
    add-int v2, v12, v11

    .line 190
    invoke-virtual {v10, v15, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 118
    add-int/lit8 v2, v11, 0x1

    move v11, v2

    goto :goto_0

    .line 183
    :cond_0
    const v2, 0x3e99999a    # 0.3f

    goto :goto_1

    .line 192
    :cond_1
    const/4 v2, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/intercom/input/gallery/AnnotatedImageView;->setColor(I)V

    .line 193
    const/4 v2, 0x0

    aget-object v2, v7, v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/intercom/input/gallery/i;->selectedColorView:Landroid/view/View;

    .line 194
    return-void
.end method


# virtual methods
.method protected abstract getInjector(Lcom/intercom/input/gallery/i;)Lcom/intercom/input/gallery/i$a;
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 197
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/intercom/input/gallery/o$d;->lightbox_send_button:I

    if-ne v0, v1, :cond_1

    .line 198
    iget-boolean v0, p0, Lcom/intercom/input/gallery/i;->isAnnotationEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/intercom/input/gallery/i;->annotatedImageView:Lcom/intercom/input/gallery/AnnotatedImageView;

    invoke-virtual {v0}, Lcom/intercom/input/gallery/AnnotatedImageView;->getPathCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 199
    new-instance v0, Lcom/intercom/input/gallery/l;

    invoke-virtual {p0}, Lcom/intercom/input/gallery/i;->getActivity()Landroid/support/v4/app/j;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/j;->getCacheDir()Ljava/io/File;

    move-result-object v1

    iget-object v2, p0, Lcom/intercom/input/gallery/i;->imageLoader:Lcom/intercom/composer/e;

    invoke-direct {v0, v1, v2}, Lcom/intercom/input/gallery/l;-><init>(Ljava/io/File;Lcom/intercom/composer/e;)V

    iget-object v1, p0, Lcom/intercom/input/gallery/i;->annotatedImageView:Lcom/intercom/input/gallery/AnnotatedImageView;

    iget-object v2, p0, Lcom/intercom/input/gallery/i;->galleryImage:Lcom/intercom/input/gallery/c;

    .line 200
    invoke-virtual {v0, v1, v2}, Lcom/intercom/input/gallery/l;->a(Lcom/intercom/input/gallery/AnnotatedImageView;Lcom/intercom/input/gallery/c;)Lcom/intercom/input/gallery/c;

    move-result-object v0

    iput-object v0, p0, Lcom/intercom/input/gallery/i;->galleryImage:Lcom/intercom/input/gallery/c;

    .line 202
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 203
    const-string v1, "gallery_image"

    iget-object v2, p0, Lcom/intercom/input/gallery/i;->galleryImage:Lcom/intercom/input/gallery/c;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 204
    invoke-virtual {p0}, Lcom/intercom/input/gallery/i;->getActivity()Landroid/support/v4/app/j;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/j;->setResult(ILandroid/content/Intent;)V

    .line 207
    :cond_1
    invoke-virtual {p0}, Lcom/intercom/input/gallery/i;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/j;->onBackPressed()V

    .line 208
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 219
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 220
    iget-object v0, p0, Lcom/intercom/input/gallery/i;->annotatedImageView:Lcom/intercom/input/gallery/AnnotatedImageView;

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/intercom/input/gallery/i;->annotatedImageView:Lcom/intercom/input/gallery/AnnotatedImageView;

    invoke-virtual {v0}, Lcom/intercom/input/gallery/AnnotatedImageView;->a()V

    .line 223
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 53
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 54
    invoke-virtual {p0}, Lcom/intercom/input/gallery/i;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 55
    const-string v1, "gallery_image"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/intercom/input/gallery/c;

    iput-object v0, p0, Lcom/intercom/input/gallery/i;->galleryImage:Lcom/intercom/input/gallery/c;

    .line 57
    invoke-virtual {p0, p0}, Lcom/intercom/input/gallery/i;->getInjector(Lcom/intercom/input/gallery/i;)Lcom/intercom/input/gallery/i$a;

    move-result-object v0

    .line 58
    invoke-interface {v0, p0}, Lcom/intercom/input/gallery/i$a;->getImageLoader(Lcom/intercom/input/gallery/i;)Lcom/intercom/composer/e;

    move-result-object v1

    iput-object v1, p0, Lcom/intercom/input/gallery/i;->imageLoader:Lcom/intercom/composer/e;

    .line 59
    invoke-interface {v0, p0}, Lcom/intercom/input/gallery/i$a;->getAnnotationColors(Lcom/intercom/input/gallery/i;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/intercom/input/gallery/i;->annotationColors:Ljava/util/List;

    .line 60
    invoke-interface {v0, p0}, Lcom/intercom/input/gallery/i$a;->isAnnotationEnabled(Lcom/intercom/input/gallery/i;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/intercom/input/gallery/i;->annotationColors:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/intercom/input/gallery/i;->isAnnotationEnabled:Z

    .line 61
    return-void

    .line 60
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 67
    sget v0, Lcom/intercom/input/gallery/o$f;->intercom_composer_gallery_lightbox_fragment:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 71
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 73
    sget v0, Lcom/intercom/input/gallery/o$d;->lightbox_image:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/intercom/input/gallery/AnnotatedImageView;

    iput-object v0, p0, Lcom/intercom/input/gallery/i;->annotatedImageView:Lcom/intercom/input/gallery/AnnotatedImageView;

    .line 75
    iget-object v0, p0, Lcom/intercom/input/gallery/i;->imageLoader:Lcom/intercom/composer/e;

    iget-object v1, p0, Lcom/intercom/input/gallery/i;->galleryImage:Lcom/intercom/input/gallery/c;

    iget-object v2, p0, Lcom/intercom/input/gallery/i;->annotatedImageView:Lcom/intercom/input/gallery/AnnotatedImageView;

    invoke-interface {v0, v1, v2}, Lcom/intercom/composer/e;->loadImageIntoView(Lcom/intercom/input/gallery/c;Landroid/widget/ImageView;)V

    .line 77
    sget v0, Lcom/intercom/input/gallery/o$d;->lightbox_send_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    sget v0, Lcom/intercom/input/gallery/o$d;->lightbox_close_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    iget-boolean v0, p0, Lcom/intercom/input/gallery/i;->isAnnotationEnabled:Z

    iget-object v1, p0, Lcom/intercom/input/gallery/i;->annotationColors:Ljava/util/List;

    invoke-direct {p0, p1, v0, v1}, Lcom/intercom/input/gallery/i;->setupAnnotations(Landroid/view/View;ZLjava/util/List;)V

    .line 80
    return-void
.end method
