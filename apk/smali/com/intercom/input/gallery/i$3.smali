.class Lcom/intercom/input/gallery/i$3;
.super Ljava/lang/Object;
.source "GalleryLightBoxFragment.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/intercom/input/gallery/i;->setupColorPickers(Landroid/view/View;Ljava/util/List;Lcom/intercom/input/gallery/AnnotatedImageView;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/view/animation/OvershootInterpolator;

.field final synthetic b:Lcom/intercom/input/gallery/AnnotatedImageView;

.field final synthetic c:I

.field final synthetic d:[Landroid/view/View;

.field final synthetic e:Landroid/support/v4/view/b/b;

.field final synthetic f:Landroid/graphics/Rect;

.field final synthetic g:Lcom/intercom/input/gallery/i;


# direct methods
.method constructor <init>(Lcom/intercom/input/gallery/i;Landroid/view/animation/OvershootInterpolator;Lcom/intercom/input/gallery/AnnotatedImageView;I[Landroid/view/View;Landroid/support/v4/view/b/b;Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/intercom/input/gallery/i$3;->g:Lcom/intercom/input/gallery/i;

    iput-object p2, p0, Lcom/intercom/input/gallery/i$3;->a:Landroid/view/animation/OvershootInterpolator;

    iput-object p3, p0, Lcom/intercom/input/gallery/i$3;->b:Lcom/intercom/input/gallery/AnnotatedImageView;

    iput p4, p0, Lcom/intercom/input/gallery/i$3;->c:I

    iput-object p5, p0, Lcom/intercom/input/gallery/i$3;->d:[Landroid/view/View;

    iput-object p6, p0, Lcom/intercom/input/gallery/i$3;->e:Landroid/support/v4/view/b/b;

    iput-object p7, p0, Lcom/intercom/input/gallery/i$3;->f:Landroid/graphics/Rect;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/view/View;II)Z
    .locals 3

    .prologue
    .line 176
    iget-object v0, p0, Lcom/intercom/input/gallery/i$3;->f:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 177
    iget-object v0, p0, Lcom/intercom/input/gallery/i$3;->f:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/intercom/input/gallery/i$3;->f:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    neg-int v1, v1

    iget-object v2, p0, Lcom/intercom/input/gallery/i$3;->f:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    neg-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 178
    iget-object v0, p0, Lcom/intercom/input/gallery/i$3;->f:Landroid/graphics/Rect;

    invoke-virtual {v0, p2, p3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 12

    .prologue
    const/high16 v4, 0x40200000    # 2.5f

    const v2, 0x3e99999a    # 0.3f

    const/4 v3, 0x1

    const-wide/16 v10, 0xc8

    const/high16 v1, 0x3f800000    # 1.0f

    .line 129
    invoke-static {p2}, Landroid/support/v4/view/h;->a(Landroid/view/MotionEvent;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 171
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 131
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 132
    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 133
    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 134
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 135
    invoke-virtual {v0, v10, v11}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/intercom/input/gallery/i$3;->a:Landroid/view/animation/OvershootInterpolator;

    .line 136
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 137
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    move v0, v3

    .line 138
    goto :goto_0

    .line 140
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 141
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 142
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 143
    invoke-virtual {v0, v10, v11}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v4, p0, Lcom/intercom/input/gallery/i$3;->a:Landroid/view/animation/OvershootInterpolator;

    .line 144
    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 145
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 147
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-direct {p0, p1, v0, v4}, Lcom/intercom/input/gallery/i$3;->a(Landroid/view/View;II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/intercom/input/gallery/i$3;->b:Lcom/intercom/input/gallery/AnnotatedImageView;

    iget v4, p0, Lcom/intercom/input/gallery/i$3;->c:I

    invoke-virtual {v0, v4}, Lcom/intercom/input/gallery/AnnotatedImageView;->setColor(I)V

    .line 149
    iget-object v0, p0, Lcom/intercom/input/gallery/i$3;->g:Lcom/intercom/input/gallery/i;

    invoke-static {v0, p1}, Lcom/intercom/input/gallery/i;->access$102(Lcom/intercom/input/gallery/i;Landroid/view/View;)Landroid/view/View;

    .line 150
    invoke-virtual {p1}, Landroid/view/View;->performClick()Z

    .line 153
    :cond_0
    iget-object v5, p0, Lcom/intercom/input/gallery/i$3;->d:[Landroid/view/View;

    array-length v6, v5

    const/4 v0, 0x0

    move v4, v0

    :goto_1
    if-ge v4, v6, :cond_2

    aget-object v0, v5, v4

    .line 154
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    iget-object v8, p0, Lcom/intercom/input/gallery/i$3;->g:Lcom/intercom/input/gallery/i;

    .line 155
    invoke-static {v8}, Lcom/intercom/input/gallery/i;->access$100(Lcom/intercom/input/gallery/i;)Landroid/view/View;

    move-result-object v8

    if-ne v0, v8, :cond_1

    move v0, v1

    :goto_2
    invoke-virtual {v7, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v7, p0, Lcom/intercom/input/gallery/i$3;->e:Landroid/support/v4/view/b/b;

    .line 156
    invoke-virtual {v0, v7}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 157
    invoke-virtual {v0, v10, v11}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 158
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 153
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_1
    move v0, v2

    .line 155
    goto :goto_2

    :cond_2
    move v0, v3

    .line 160
    goto/16 :goto_0

    .line 162
    :pswitch_3
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 163
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 164
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v4, p0, Lcom/intercom/input/gallery/i$3;->g:Lcom/intercom/input/gallery/i;

    .line 165
    invoke-static {v4}, Lcom/intercom/input/gallery/i;->access$100(Lcom/intercom/input/gallery/i;)Landroid/view/View;

    move-result-object v4

    if-ne p1, v4, :cond_3

    :goto_3
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/intercom/input/gallery/i$3;->a:Landroid/view/animation/OvershootInterpolator;

    .line 166
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 167
    invoke-virtual {v0, v10, v11}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 168
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    move v0, v3

    .line 169
    goto/16 :goto_0

    :cond_3
    move v1, v2

    .line 165
    goto :goto_3

    .line 129
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
