.class Lcom/intercom/input/gallery/l;
.super Ljava/lang/Object;
.source "ImageCompositor.java"


# instance fields
.field private final a:Ljava/io/File;

.field private final b:Lcom/intercom/composer/e;


# direct methods
.method constructor <init>(Ljava/io/File;Lcom/intercom/composer/e;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/intercom/input/gallery/l;->a:Ljava/io/File;

    .line 21
    iput-object p2, p0, Lcom/intercom/input/gallery/l;->b:Lcom/intercom/composer/e;

    .line 22
    return-void
.end method

.method private static a(Landroid/graphics/Bitmap;)Landroid/graphics/Rect;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 78
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method


# virtual methods
.method a(Lcom/intercom/input/gallery/AnnotatedImageView;Lcom/intercom/input/gallery/c;)Lcom/intercom/input/gallery/c;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 25
    invoke-virtual {p1}, Lcom/intercom/input/gallery/AnnotatedImageView;->getAnnotationsBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 26
    iget-object v1, p0, Lcom/intercom/input/gallery/l;->b:Lcom/intercom/composer/e;

    invoke-interface {v1, p1}, Lcom/intercom/composer/e;->getBitmapFromView(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 28
    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/intercom/input/gallery/AnnotatedImageView;->getPathCount()I

    move-result v3

    if-nez v3, :cond_1

    .line 74
    :cond_0
    :goto_0
    return-object p2

    .line 32
    :cond_1
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v3

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 33
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 35
    invoke-static {v0}, Lcom/intercom/input/gallery/l;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Rect;

    move-result-object v5

    .line 36
    invoke-static {v1}, Lcom/intercom/input/gallery/l;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Rect;

    move-result-object v1

    .line 33
    invoke-virtual {v4, v0, v5, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 38
    invoke-virtual {p1}, Lcom/intercom/input/gallery/AnnotatedImageView;->a()V

    .line 46
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/intercom/input/gallery/c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_anno_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 47
    const-string v1, ".jpg"

    iget-object v4, p0, Lcom/intercom/input/gallery/l;->a:Ljava/io/File;

    invoke-static {v0, v1, v4}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v4

    .line 48
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 49
    :try_start_1
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x5a

    invoke-virtual {v3, v2, v5, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 51
    new-instance v2, Lcom/intercom/input/gallery/c$a;

    invoke-direct {v2}, Lcom/intercom/input/gallery/c$a;-><init>()V

    .line 52
    invoke-virtual {p2}, Lcom/intercom/input/gallery/c;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/intercom/input/gallery/c$a;->e(Ljava/lang/String;)Lcom/intercom/input/gallery/c$a;

    move-result-object v2

    .line 53
    invoke-virtual {p2}, Lcom/intercom/input/gallery/c;->f()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/intercom/input/gallery/c$a;->a(I)Lcom/intercom/input/gallery/c$a;

    move-result-object v2

    .line 54
    invoke-virtual {p2}, Lcom/intercom/input/gallery/c;->g()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/intercom/input/gallery/c$a;->b(I)Lcom/intercom/input/gallery/c$a;

    move-result-object v2

    .line 55
    invoke-virtual {p2}, Lcom/intercom/input/gallery/c;->h()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/intercom/input/gallery/c$a;->c(I)Lcom/intercom/input/gallery/c$a;

    move-result-object v2

    .line 56
    invoke-virtual {p2}, Lcom/intercom/input/gallery/c;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/intercom/input/gallery/c$a;->b(Ljava/lang/String;)Lcom/intercom/input/gallery/c$a;

    move-result-object v2

    .line 57
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/intercom/input/gallery/c$a;->c(Ljava/lang/String;)Lcom/intercom/input/gallery/c$a;

    move-result-object v2

    .line 58
    invoke-virtual {v2, v0}, Lcom/intercom/input/gallery/c$a;->a(Ljava/lang/String;)Lcom/intercom/input/gallery/c$a;

    move-result-object v0

    .line 59
    invoke-virtual {p2}, Lcom/intercom/input/gallery/c;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/intercom/input/gallery/c$a;->d(Ljava/lang/String;)Lcom/intercom/input/gallery/c$a;

    move-result-object v0

    const/4 v2, 0x1

    .line 60
    invoke-virtual {v0, v2}, Lcom/intercom/input/gallery/c$a;->a(Z)Lcom/intercom/input/gallery/c$a;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Lcom/intercom/input/gallery/c$a;->a()Lcom/intercom/input/gallery/c;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object p2

    .line 66
    if-eqz v1, :cond_0

    .line 67
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 69
    :catch_0
    move-exception v0

    .line 70
    const-string v1, "Intercom"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t close stream: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 62
    :catch_1
    move-exception v0

    move-object v1, v2

    .line 63
    :goto_1
    :try_start_3
    const-string v2, "Intercom"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Couldn\'t composite images: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 66
    if-eqz v1, :cond_0

    .line 67
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_0

    .line 69
    :catch_2
    move-exception v0

    .line 70
    const-string v1, "Intercom"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t close stream: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 65
    :catchall_0
    move-exception v0

    move-object v1, v2

    .line 66
    :goto_2
    if-eqz v1, :cond_2

    .line 67
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 71
    :cond_2
    :goto_3
    throw v0

    .line 69
    :catch_3
    move-exception v1

    .line 70
    const-string v2, "Intercom"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Couldn\'t close stream: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 65
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 62
    :catch_4
    move-exception v0

    goto :goto_1
.end method
