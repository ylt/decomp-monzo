.class public Lcom/intercom/input/gallery/c;
.super Ljava/lang/Object;
.source "GalleryImage.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intercom/input/gallery/c$a;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/intercom/input/gallery/c;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 102
    new-instance v0, Lcom/intercom/input/gallery/c$1;

    invoke-direct {v0}, Lcom/intercom/input/gallery/c$1;-><init>()V

    sput-object v0, Lcom/intercom/input/gallery/c;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/intercom/input/gallery/c;->a:Ljava/lang/String;

    .line 80
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/intercom/input/gallery/c;->b:Ljava/lang/String;

    .line 81
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/intercom/input/gallery/c;->c:Ljava/lang/String;

    .line 82
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/intercom/input/gallery/c;->d:Ljava/lang/String;

    .line 83
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/intercom/input/gallery/c;->e:Ljava/lang/String;

    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/intercom/input/gallery/c;->f:I

    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/intercom/input/gallery/c;->g:I

    .line 86
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/intercom/input/gallery/c;->h:I

    .line 87
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/intercom/input/gallery/c;->i:Z

    .line 88
    return-void

    .line 87
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIZ)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/intercom/input/gallery/c;->a:Ljava/lang/String;

    .line 24
    iput-object p2, p0, Lcom/intercom/input/gallery/c;->b:Ljava/lang/String;

    .line 25
    iput-object p3, p0, Lcom/intercom/input/gallery/c;->c:Ljava/lang/String;

    .line 26
    iput-object p4, p0, Lcom/intercom/input/gallery/c;->d:Ljava/lang/String;

    .line 27
    iput-object p5, p0, Lcom/intercom/input/gallery/c;->e:Ljava/lang/String;

    .line 28
    iput p6, p0, Lcom/intercom/input/gallery/c;->f:I

    .line 29
    iput p7, p0, Lcom/intercom/input/gallery/c;->g:I

    .line 30
    iput p8, p0, Lcom/intercom/input/gallery/c;->h:I

    .line 31
    iput-boolean p9, p0, Lcom/intercom/input/gallery/c;->i:Z

    .line 32
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/intercom/input/gallery/c;->c:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/intercom/input/gallery/c;->a:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/intercom/input/gallery/c;->b:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/intercom/input/gallery/c;->d:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/intercom/input/gallery/c;->e:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 117
    if-ne p0, p1, :cond_1

    const/4 v0, 0x1

    .line 130
    :cond_0
    :goto_0
    return v0

    .line 118
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 120
    check-cast p1, Lcom/intercom/input/gallery/c;

    .line 122
    iget v1, p0, Lcom/intercom/input/gallery/c;->f:I

    iget v2, p1, Lcom/intercom/input/gallery/c;->f:I

    if-ne v1, v2, :cond_0

    .line 123
    iget v1, p0, Lcom/intercom/input/gallery/c;->g:I

    iget v2, p1, Lcom/intercom/input/gallery/c;->g:I

    if-ne v1, v2, :cond_0

    .line 124
    iget v1, p0, Lcom/intercom/input/gallery/c;->h:I

    iget v2, p1, Lcom/intercom/input/gallery/c;->h:I

    if-ne v1, v2, :cond_0

    .line 125
    iget-boolean v1, p0, Lcom/intercom/input/gallery/c;->i:Z

    iget-boolean v2, p1, Lcom/intercom/input/gallery/c;->i:Z

    if-ne v1, v2, :cond_0

    .line 126
    iget-object v1, p0, Lcom/intercom/input/gallery/c;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/intercom/input/gallery/c;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 127
    iget-object v1, p0, Lcom/intercom/input/gallery/c;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/intercom/input/gallery/c;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 128
    iget-object v1, p0, Lcom/intercom/input/gallery/c;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/intercom/input/gallery/c;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 129
    iget-object v1, p0, Lcom/intercom/input/gallery/c;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/intercom/input/gallery/c;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 130
    iget-object v0, p0, Lcom/intercom/input/gallery/c;->e:Ljava/lang/String;

    iget-object v1, p1, Lcom/intercom/input/gallery/c;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/intercom/input/gallery/c;->f:I

    return v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/intercom/input/gallery/c;->g:I

    return v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/intercom/input/gallery/c;->h:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/intercom/input/gallery/c;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 135
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/intercom/input/gallery/c;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 136
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/intercom/input/gallery/c;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 137
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/intercom/input/gallery/c;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 138
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/intercom/input/gallery/c;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 139
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/intercom/input/gallery/c;->f:I

    add-int/2addr v0, v1

    .line 140
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/intercom/input/gallery/c;->g:I

    add-int/2addr v0, v1

    .line 141
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/intercom/input/gallery/c;->h:I

    add-int/2addr v0, v1

    .line 142
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/intercom/input/gallery/c;->i:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    .line 143
    return v0

    .line 142
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/intercom/input/gallery/c;->i:Z

    return v0
.end method

.method public j()Ljava/io/File;
    .locals 2

    .prologue
    .line 75
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/intercom/input/gallery/c;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/intercom/input/gallery/c;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/intercom/input/gallery/c;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lcom/intercom/input/gallery/c;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/intercom/input/gallery/c;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/intercom/input/gallery/c;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 96
    iget v0, p0, Lcom/intercom/input/gallery/c;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 97
    iget v0, p0, Lcom/intercom/input/gallery/c;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 98
    iget v0, p0, Lcom/intercom/input/gallery/c;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 99
    iget-boolean v0, p0, Lcom/intercom/input/gallery/c;->i:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 100
    return-void

    .line 99
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
