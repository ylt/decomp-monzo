.class Lcom/intercom/input/gallery/g$2;
.super Ljava/lang/Object;
.source "GalleryInputFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intercom/input/gallery/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/intercom/input/gallery/g;


# direct methods
.method constructor <init>(Lcom/intercom/input/gallery/g;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/intercom/input/gallery/g$2;->a:Lcom/intercom/input/gallery/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 92
    iget-object v0, p0, Lcom/intercom/input/gallery/g$2;->a:Lcom/intercom/input/gallery/g;

    iget-object v0, v0, Lcom/intercom/input/gallery/g;->galleryInputExpandedListener:Lcom/intercom/input/gallery/f;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/intercom/input/gallery/g$2;->a:Lcom/intercom/input/gallery/g;

    iget-object v0, v0, Lcom/intercom/input/gallery/g;->galleryInputExpandedListener:Lcom/intercom/input/gallery/f;

    invoke-interface {v0}, Lcom/intercom/input/gallery/f;->onInputExpanded()V

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/intercom/input/gallery/g$2;->a:Lcom/intercom/input/gallery/g;

    invoke-virtual {v0}, Lcom/intercom/input/gallery/g;->getActivity()Landroid/support/v4/app/j;

    move-result-object v0

    iget-object v1, p0, Lcom/intercom/input/gallery/g$2;->a:Lcom/intercom/input/gallery/g;

    .line 96
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    iget-object v2, p0, Lcom/intercom/input/gallery/g$2;->a:Lcom/intercom/input/gallery/g;

    invoke-virtual {v2}, Lcom/intercom/input/gallery/g;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    .line 95
    invoke-static {v0, v1, v2}, Lcom/intercom/input/gallery/GalleryInputFullScreenActivity;->a(Landroid/content/Context;Ljava/lang/Class;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 97
    iget-object v1, p0, Lcom/intercom/input/gallery/g$2;->a:Lcom/intercom/input/gallery/g;

    invoke-virtual {v1}, Lcom/intercom/input/gallery/g;->getActivity()Landroid/support/v4/app/j;

    move-result-object v1

    sget v2, Lcom/intercom/input/gallery/o$a;->intercom_composer_slide_up:I

    sget v3, Lcom/intercom/input/gallery/o$a;->intercom_composer_stay:I

    invoke-static {v1, v2, v3}, Landroid/support/v4/app/b;->a(Landroid/content/Context;II)Landroid/support/v4/app/b;

    move-result-object v1

    .line 98
    invoke-virtual {v1}, Landroid/support/v4/app/b;->a()Landroid/os/Bundle;

    move-result-object v1

    .line 99
    iget-object v2, p0, Lcom/intercom/input/gallery/g$2;->a:Lcom/intercom/input/gallery/g;

    const/16 v3, 0xe

    invoke-virtual {v2, v0, v3, v1}, Lcom/intercom/input/gallery/g;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 100
    return-void
.end method
