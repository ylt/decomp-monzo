.class public Lcom/intercom/input/gallery/d;
.super Lcom/intercom/composer/b/b;
.source "GalleryInput.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/intercom/composer/b/b",
        "<",
        "Lcom/intercom/input/gallery/g;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/intercom/input/gallery/j;

.field private final b:Lcom/intercom/input/gallery/f;

.field private final c:Lcom/intercom/composer/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/intercom/composer/d",
            "<",
            "Lcom/intercom/input/gallery/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/intercom/composer/b/a;Lcom/intercom/input/gallery/j;Lcom/intercom/input/gallery/f;Lcom/intercom/composer/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/intercom/composer/b/a;",
            "Lcom/intercom/input/gallery/j;",
            "Lcom/intercom/input/gallery/f;",
            "Lcom/intercom/composer/d",
            "<",
            "Lcom/intercom/input/gallery/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lcom/intercom/composer/b/b;-><init>(Ljava/lang/String;Lcom/intercom/composer/b/a;)V

    .line 19
    iput-object p3, p0, Lcom/intercom/input/gallery/d;->a:Lcom/intercom/input/gallery/j;

    .line 20
    iput-object p4, p0, Lcom/intercom/input/gallery/d;->b:Lcom/intercom/input/gallery/f;

    .line 21
    iput-object p5, p0, Lcom/intercom/input/gallery/d;->c:Lcom/intercom/composer/d;

    .line 22
    return-void
.end method


# virtual methods
.method public a()Lcom/intercom/input/gallery/g;
    .locals 2

    .prologue
    .line 25
    iget-object v0, p0, Lcom/intercom/input/gallery/d;->c:Lcom/intercom/composer/d;

    invoke-interface {v0}, Lcom/intercom/composer/d;->create()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intercom/input/gallery/g;

    .line 26
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/intercom/input/gallery/g;->createArguments(Z)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/intercom/input/gallery/g;->setArguments(Landroid/os/Bundle;)V

    .line 27
    iget-object v1, p0, Lcom/intercom/input/gallery/d;->a:Lcom/intercom/input/gallery/j;

    invoke-virtual {v0, v1}, Lcom/intercom/input/gallery/g;->setGalleryListener(Lcom/intercom/input/gallery/j;)V

    .line 28
    iget-object v1, p0, Lcom/intercom/input/gallery/d;->b:Lcom/intercom/input/gallery/f;

    invoke-virtual {v0, v1}, Lcom/intercom/input/gallery/g;->setGalleryExpandedListener(Lcom/intercom/input/gallery/f;)V

    .line 29
    return-object v0
.end method

.method public synthetic createFragment()Lcom/intercom/composer/b/c;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0}, Lcom/intercom/input/gallery/d;->a()Lcom/intercom/input/gallery/g;

    move-result-object v0

    return-object v0
.end method
