.class public Lcom/intercom/input/gallery/GalleryInputFullScreenActivity;
.super Landroid/support/v7/app/e;
.source "GalleryInputFullScreenActivity.java"

# interfaces
.implements Lcom/intercom/input/gallery/j;


# instance fields
.field private a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/intercom/input/gallery/g;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/support/v7/app/e;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Class;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/intercom/input/gallery/g;",
            ">;",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 26
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/intercom/input/gallery/GalleryInputFullScreenActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "fragment_class"

    .line 27
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "fragment_args"

    .line 28
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 26
    return-object v0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 3

    .prologue
    .line 69
    invoke-super {p0}, Landroid/support/v7/app/e;->onBackPressed()V

    .line 70
    invoke-virtual {p0}, Lcom/intercom/input/gallery/GalleryInputFullScreenActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    .line 71
    if-eqz v1, :cond_0

    .line 72
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/intercom/input/gallery/GalleryInputFullScreenActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 73
    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 75
    :cond_0
    sget v0, Lcom/intercom/input/gallery/o$a;->intercom_composer_stay:I

    sget v1, Lcom/intercom/input/gallery/o$a;->intercom_composer_slide_down:I

    invoke-virtual {p0, v0, v1}, Lcom/intercom/input/gallery/GalleryInputFullScreenActivity;->overridePendingTransition(II)V

    .line 76
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 32
    invoke-super {p0, p1}, Landroid/support/v7/app/e;->onCreate(Landroid/os/Bundle;)V

    .line 33
    sget v0, Lcom/intercom/input/gallery/o$f;->intercom_composer_activity_input_full_screen:I

    invoke-virtual {p0, v0}, Lcom/intercom/input/gallery/GalleryInputFullScreenActivity;->setContentView(I)V

    .line 34
    invoke-virtual {p0}, Lcom/intercom/input/gallery/GalleryInputFullScreenActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    sget v1, Lcom/intercom/input/gallery/o$b;->intercom_composer_status_bar:I

    invoke-static {v0, v1}, Lcom/intercom/composer/h;->a(Landroid/view/Window;I)V

    .line 36
    invoke-virtual {p0}, Lcom/intercom/input/gallery/GalleryInputFullScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 37
    const-string v1, "fragment_class"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    invoke-static {v1}, Lcom/intercom/input/gallery/b;->a(Ljava/io/Serializable;)Ljava/lang/Class;

    move-result-object v1

    iput-object v1, p0, Lcom/intercom/input/gallery/GalleryInputFullScreenActivity;->a:Ljava/lang/Class;

    .line 38
    const-string v1, "fragment_args"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/intercom/input/gallery/GalleryInputFullScreenActivity;->b:Landroid/os/Bundle;

    .line 39
    return-void
.end method

.method public onGalleryOutputReceived(Lcom/intercom/input/gallery/c;)V
    .locals 2

    .prologue
    .line 79
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 80
    const-string v1, "gallery_image"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 81
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/intercom/input/gallery/GalleryInputFullScreenActivity;->setResult(ILandroid/content/Intent;)V

    .line 82
    invoke-virtual {p0}, Lcom/intercom/input/gallery/GalleryInputFullScreenActivity;->onBackPressed()V

    .line 83
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 61
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 62
    invoke-virtual {p0}, Lcom/intercom/input/gallery/GalleryInputFullScreenActivity;->onBackPressed()V

    .line 63
    const/4 v0, 0x1

    .line 65
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v7/app/e;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 42
    invoke-super {p0, p1}, Landroid/support/v7/app/e;->onPostCreate(Landroid/os/Bundle;)V

    .line 45
    invoke-virtual {p0}, Lcom/intercom/input/gallery/GalleryInputFullScreenActivity;->getSupportFragmentManager()Landroid/support/v4/app/n;

    move-result-object v2

    .line 46
    const-class v0, Lcom/intercom/input/gallery/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    .line 47
    invoke-virtual {v2, v3}, Landroid/support/v4/app/n;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 48
    if-nez v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/intercom/input/gallery/GalleryInputFullScreenActivity;->a:Ljava/lang/Class;

    invoke-static {v0}, Lcom/intercom/input/gallery/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intercom/input/gallery/g;

    .line 50
    iget-object v1, p0, Lcom/intercom/input/gallery/GalleryInputFullScreenActivity;->b:Landroid/os/Bundle;

    if-nez v1, :cond_1

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 51
    :goto_0
    const/4 v4, 0x1

    invoke-static {v4}, Lcom/intercom/input/gallery/g;->createArguments(Z)Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 52
    invoke-virtual {v0, v1}, Lcom/intercom/input/gallery/g;->setArguments(Landroid/os/Bundle;)V

    .line 53
    invoke-virtual {v0, p0}, Lcom/intercom/input/gallery/g;->setGalleryListener(Lcom/intercom/input/gallery/j;)V

    .line 54
    invoke-virtual {v2}, Landroid/support/v4/app/n;->a()Landroid/support/v4/app/t;

    move-result-object v1

    sget v2, Lcom/intercom/input/gallery/o$d;->expanded_fragment:I

    .line 55
    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/t;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/t;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Landroid/support/v4/app/t;->c()I

    .line 58
    :cond_0
    return-void

    .line 50
    :cond_1
    new-instance v1, Landroid/os/Bundle;

    iget-object v4, p0, Lcom/intercom/input/gallery/GalleryInputFullScreenActivity;->b:Landroid/os/Bundle;

    invoke-direct {v1, v4}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    goto :goto_0
.end method
