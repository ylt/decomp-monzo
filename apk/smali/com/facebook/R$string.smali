.class public final Lcom/facebook/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f0a0000

.field public static final abc_action_bar_home_description_format:I = 0x7f0a0001

.field public static final abc_action_bar_home_subtitle_description_format:I = 0x7f0a0002

.field public static final abc_action_bar_up_description:I = 0x7f0a0003

.field public static final abc_action_menu_overflow_description:I = 0x7f0a0004

.field public static final abc_action_mode_done:I = 0x7f0a0005

.field public static final abc_activity_chooser_view_see_all:I = 0x7f0a0006

.field public static final abc_activitychooserview_choose_application:I = 0x7f0a0007

.field public static final abc_capital_off:I = 0x7f0a0008

.field public static final abc_capital_on:I = 0x7f0a0009

.field public static final abc_font_family_body_1_material:I = 0x7f0a00b5

.field public static final abc_font_family_body_2_material:I = 0x7f0a00b6

.field public static final abc_font_family_button_material:I = 0x7f0a00b7

.field public static final abc_font_family_caption_material:I = 0x7f0a00b8

.field public static final abc_font_family_display_1_material:I = 0x7f0a00b9

.field public static final abc_font_family_display_2_material:I = 0x7f0a00ba

.field public static final abc_font_family_display_3_material:I = 0x7f0a00bb

.field public static final abc_font_family_display_4_material:I = 0x7f0a00bc

.field public static final abc_font_family_headline_material:I = 0x7f0a00bd

.field public static final abc_font_family_menu_material:I = 0x7f0a00be

.field public static final abc_font_family_subhead_material:I = 0x7f0a00bf

.field public static final abc_font_family_title_material:I = 0x7f0a00c0

.field public static final abc_search_hint:I = 0x7f0a000a

.field public static final abc_searchview_description_clear:I = 0x7f0a000b

.field public static final abc_searchview_description_query:I = 0x7f0a000c

.field public static final abc_searchview_description_search:I = 0x7f0a000d

.field public static final abc_searchview_description_submit:I = 0x7f0a000e

.field public static final abc_searchview_description_voice:I = 0x7f0a000f

.field public static final abc_shareactionprovider_share_with:I = 0x7f0a0010

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f0a0011

.field public static final abc_toolbar_collapse_description:I = 0x7f0a0012

.field public static final com_facebook_device_auth_instructions:I = 0x7f0a0013

.field public static final com_facebook_image_download_unknown_error:I = 0x7f0a0014

.field public static final com_facebook_internet_permission_error_message:I = 0x7f0a0015

.field public static final com_facebook_internet_permission_error_title:I = 0x7f0a0016

.field public static final com_facebook_like_button_liked:I = 0x7f0a0017

.field public static final com_facebook_like_button_not_liked:I = 0x7f0a0018

.field public static final com_facebook_loading:I = 0x7f0a0019

.field public static final com_facebook_loginview_cancel_action:I = 0x7f0a001a

.field public static final com_facebook_loginview_log_in_button:I = 0x7f0a001b

.field public static final com_facebook_loginview_log_in_button_continue:I = 0x7f0a001c

.field public static final com_facebook_loginview_log_in_button_continue_f1gender:I = 0x7f0a00a1

.field public static final com_facebook_loginview_log_in_button_continue_m2gender:I = 0x7f0a00a2

.field public static final com_facebook_loginview_log_in_button_long:I = 0x7f0a001d

.field public static final com_facebook_loginview_log_out_action:I = 0x7f0a001e

.field public static final com_facebook_loginview_log_out_action_f1gender:I = 0x7f0a00a3

.field public static final com_facebook_loginview_log_out_action_m2gender:I = 0x7f0a00a4

.field public static final com_facebook_loginview_log_out_button:I = 0x7f0a001f

.field public static final com_facebook_loginview_log_out_button_f1gender:I = 0x7f0a00a5

.field public static final com_facebook_loginview_log_out_button_m2gender:I = 0x7f0a00a6

.field public static final com_facebook_loginview_logged_in_as:I = 0x7f0a0020

.field public static final com_facebook_loginview_logged_in_as_f1gender:I = 0x7f0a00a7

.field public static final com_facebook_loginview_logged_in_as_m2gender:I = 0x7f0a00a8

.field public static final com_facebook_loginview_logged_in_using_facebook:I = 0x7f0a0021

.field public static final com_facebook_loginview_logged_in_using_facebook_f1gender:I = 0x7f0a009f

.field public static final com_facebook_loginview_logged_in_using_facebook_m2gender:I = 0x7f0a00a0

.field public static final com_facebook_send_button_text:I = 0x7f0a0022

.field public static final com_facebook_send_button_text_f1gender:I = 0x7f0a00a9

.field public static final com_facebook_send_button_text_m2gender:I = 0x7f0a00aa

.field public static final com_facebook_share_button_text:I = 0x7f0a0023

.field public static final com_facebook_share_button_text_f1gender:I = 0x7f0a00ab

.field public static final com_facebook_share_button_text_m2gender:I = 0x7f0a00ac

.field public static final com_facebook_smart_device_instructions:I = 0x7f0a0024

.field public static final com_facebook_smart_device_instructions_or:I = 0x7f0a0025

.field public static final com_facebook_smart_login_confirmation_cancel:I = 0x7f0a0026

.field public static final com_facebook_smart_login_confirmation_cancel_f1gender:I = 0x7f0a00ad

.field public static final com_facebook_smart_login_confirmation_cancel_m2gender:I = 0x7f0a00ae

.field public static final com_facebook_smart_login_confirmation_continue_as:I = 0x7f0a0027

.field public static final com_facebook_smart_login_confirmation_continue_as_f1gender:I = 0x7f0a00af

.field public static final com_facebook_smart_login_confirmation_continue_as_m2gender:I = 0x7f0a00b0

.field public static final com_facebook_smart_login_confirmation_title:I = 0x7f0a0028

.field public static final com_facebook_smart_login_confirmation_title_f1gender:I = 0x7f0a00b1

.field public static final com_facebook_smart_login_confirmation_title_m2gender:I = 0x7f0a00b2

.field public static final com_facebook_tooltip_default:I = 0x7f0a0029

.field public static final com_facebook_tooltip_default_f1gender:I = 0x7f0a00b3

.field public static final com_facebook_tooltip_default_m2gender:I = 0x7f0a00b4

.field public static final messenger_send_button_text:I = 0x7f0a0047

.field public static final search_menu_title:I = 0x7f0a0048

.field public static final status_bar_notification_info_overflow:I = 0x7f0a0049


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 856
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
