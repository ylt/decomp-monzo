.class public abstract Lcom/c/b/b;
.super Ljava/lang/Object;
.source "Optional.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/c/b/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/c/b/a",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/c/b/a;

    invoke-direct {v0}, Lcom/c/b/a;-><init>()V

    sput-object v0, Lcom/c/b/b;->a:Lcom/c/b/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(Ljava/lang/Object;)Lcom/c/b/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lcom/c/b/b",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 34
    if-nez p0, :cond_0

    .line 35
    invoke-static {}, Lcom/c/b/b;->c()Lcom/c/b/b;

    move-result-object v0

    .line 37
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/c/b/c;

    invoke-direct {v0, p0}, Lcom/c/b/c;-><init>(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static c()Lcom/c/b/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/c/b/b",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 49
    sget-object v0, Lcom/c/b/b;->a:Lcom/c/b/a;

    return-object v0
.end method


# virtual methods
.method public abstract a(Lcom/c/a/a;)Lcom/c/b/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/c/a/a",
            "<TT;>;)",
            "Lcom/c/b/b",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract a()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)TT;"
        }
    .end annotation
.end method

.method public abstract b()Z
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/c/b/b;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
