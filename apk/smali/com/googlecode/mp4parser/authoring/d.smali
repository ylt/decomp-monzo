.class public Lcom/googlecode/mp4parser/authoring/d;
.super Lcom/googlecode/mp4parser/authoring/a;
.source "Mp4TrackImpl.java"


# static fields
.field static final synthetic f:Z


# instance fields
.field d:Lcom/coremedia/iso/boxes/TrackBox;

.field e:[Lcom/coremedia/iso/d;

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/mp4parser/authoring/e;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/coremedia/iso/boxes/SampleDescriptionBox;

.field private i:[J

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/coremedia/iso/boxes/CompositionTimeToSample$a;",
            ">;"
        }
    .end annotation
.end field

.field private k:[J

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/coremedia/iso/boxes/SampleDependencyTypeBox$a;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcom/googlecode/mp4parser/authoring/g;

.field private n:Ljava/lang/String;

.field private o:Lcom/coremedia/iso/boxes/SubSampleInformationBox;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/googlecode/mp4parser/authoring/d;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/googlecode/mp4parser/authoring/d;->f:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public varargs constructor <init>(Ljava/lang/String;Lcom/coremedia/iso/boxes/TrackBox;[Lcom/coremedia/iso/d;)V
    .locals 30

    .prologue
    .line 61
    invoke-direct/range {p0 .. p1}, Lcom/googlecode/mp4parser/authoring/a;-><init>(Ljava/lang/String;)V

    .line 46
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/googlecode/mp4parser/authoring/d;->k:[J

    .line 48
    new-instance v4, Lcom/googlecode/mp4parser/authoring/g;

    invoke-direct {v4}, Lcom/googlecode/mp4parser/authoring/g;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/googlecode/mp4parser/authoring/d;->m:Lcom/googlecode/mp4parser/authoring/g;

    .line 50
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/googlecode/mp4parser/authoring/d;->o:Lcom/coremedia/iso/boxes/SubSampleInformationBox;

    .line 62
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/googlecode/mp4parser/authoring/d;->d:Lcom/coremedia/iso/boxes/TrackBox;

    .line 63
    invoke-virtual/range {p2 .. p2}, Lcom/coremedia/iso/boxes/TrackBox;->getTrackHeaderBox()Lcom/coremedia/iso/boxes/TrackHeaderBox;

    move-result-object v4

    invoke-virtual {v4}, Lcom/coremedia/iso/boxes/TrackHeaderBox;->getTrackId()J

    move-result-wide v16

    .line 64
    new-instance v4, Lcom/coremedia/iso/boxes/mdat/a;

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-direct {v4, v0, v1}, Lcom/coremedia/iso/boxes/mdat/a;-><init>(Lcom/coremedia/iso/boxes/TrackBox;[Lcom/coremedia/iso/d;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/googlecode/mp4parser/authoring/d;->g:Ljava/util/List;

    .line 65
    invoke-virtual/range {p2 .. p2}, Lcom/coremedia/iso/boxes/TrackBox;->getMediaBox()Lcom/coremedia/iso/boxes/MediaBox;

    move-result-object v4

    invoke-virtual {v4}, Lcom/coremedia/iso/boxes/MediaBox;->getMediaInformationBox()Lcom/coremedia/iso/boxes/MediaInformationBox;

    move-result-object v4

    invoke-virtual {v4}, Lcom/coremedia/iso/boxes/MediaInformationBox;->getSampleTableBox()Lcom/coremedia/iso/boxes/SampleTableBox;

    move-result-object v13

    .line 67
    invoke-virtual/range {p2 .. p2}, Lcom/coremedia/iso/boxes/TrackBox;->getMediaBox()Lcom/coremedia/iso/boxes/MediaBox;

    move-result-object v4

    invoke-virtual {v4}, Lcom/coremedia/iso/boxes/MediaBox;->getHandlerBox()Lcom/coremedia/iso/boxes/HandlerBox;

    move-result-object v4

    invoke-virtual {v4}, Lcom/coremedia/iso/boxes/HandlerBox;->getHandlerType()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/googlecode/mp4parser/authoring/d;->n:Ljava/lang/String;

    .line 69
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 70
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/googlecode/mp4parser/authoring/d;->j:Ljava/util/List;

    .line 71
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/googlecode/mp4parser/authoring/d;->l:Ljava/util/List;

    .line 73
    invoke-virtual {v13}, Lcom/coremedia/iso/boxes/SampleTableBox;->getTimeToSampleBox()Lcom/coremedia/iso/boxes/TimeToSampleBox;

    move-result-object v4

    invoke-virtual {v4}, Lcom/coremedia/iso/boxes/TimeToSampleBox;->getEntries()Ljava/util/List;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 74
    invoke-virtual {v13}, Lcom/coremedia/iso/boxes/SampleTableBox;->getCompositionTimeToSample()Lcom/coremedia/iso/boxes/CompositionTimeToSample;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 75
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/googlecode/mp4parser/authoring/d;->j:Ljava/util/List;

    invoke-virtual {v13}, Lcom/coremedia/iso/boxes/SampleTableBox;->getCompositionTimeToSample()Lcom/coremedia/iso/boxes/CompositionTimeToSample;

    move-result-object v5

    invoke-virtual {v5}, Lcom/coremedia/iso/boxes/CompositionTimeToSample;->getEntries()Ljava/util/List;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 77
    :cond_0
    invoke-virtual {v13}, Lcom/coremedia/iso/boxes/SampleTableBox;->getSampleDependencyTypeBox()Lcom/coremedia/iso/boxes/SampleDependencyTypeBox;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 78
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/googlecode/mp4parser/authoring/d;->l:Ljava/util/List;

    invoke-virtual {v13}, Lcom/coremedia/iso/boxes/SampleTableBox;->getSampleDependencyTypeBox()Lcom/coremedia/iso/boxes/SampleDependencyTypeBox;

    move-result-object v5

    invoke-virtual {v5}, Lcom/coremedia/iso/boxes/SampleDependencyTypeBox;->getEntries()Ljava/util/List;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 80
    :cond_1
    invoke-virtual {v13}, Lcom/coremedia/iso/boxes/SampleTableBox;->getSyncSampleBox()Lcom/coremedia/iso/boxes/SyncSampleBox;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 81
    invoke-virtual {v13}, Lcom/coremedia/iso/boxes/SampleTableBox;->getSyncSampleBox()Lcom/coremedia/iso/boxes/SyncSampleBox;

    move-result-object v4

    invoke-virtual {v4}, Lcom/coremedia/iso/boxes/SyncSampleBox;->getSampleNumber()[J

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/googlecode/mp4parser/authoring/d;->k:[J

    .line 83
    :cond_2
    const-string v4, "subs"

    invoke-static {v13, v4}, Lcom/googlecode/mp4parser/c/j;->a(Lcom/googlecode/mp4parser/AbstractContainerBox;Ljava/lang/String;)Lcom/coremedia/iso/boxes/a;

    move-result-object v4

    check-cast v4, Lcom/coremedia/iso/boxes/SubSampleInformationBox;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/googlecode/mp4parser/authoring/d;->o:Lcom/coremedia/iso/boxes/SubSampleInformationBox;

    .line 86
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 87
    invoke-virtual/range {p2 .. p2}, Lcom/coremedia/iso/boxes/TrackBox;->getParent()Lcom/coremedia/iso/boxes/b;

    move-result-object v4

    check-cast v4, Lcom/coremedia/iso/boxes/a;

    invoke-interface {v4}, Lcom/coremedia/iso/boxes/a;->getParent()Lcom/coremedia/iso/boxes/b;

    move-result-object v4

    const-class v5, Lcom/coremedia/iso/boxes/fragment/MovieFragmentBox;

    invoke-interface {v4, v5}, Lcom/coremedia/iso/boxes/b;->getBoxes(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 88
    move-object/from16 v0, p3

    array-length v5, v0

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v5, :cond_5

    .line 92
    invoke-virtual {v13}, Lcom/coremedia/iso/boxes/SampleTableBox;->getSampleDescriptionBox()Lcom/coremedia/iso/boxes/SampleDescriptionBox;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/googlecode/mp4parser/authoring/d;->h:Lcom/coremedia/iso/boxes/SampleDescriptionBox;

    .line 93
    const/16 v20, 0x0

    .line 94
    invoke-virtual/range {p2 .. p2}, Lcom/coremedia/iso/boxes/TrackBox;->getParent()Lcom/coremedia/iso/boxes/b;

    move-result-object v4

    const-class v5, Lcom/coremedia/iso/boxes/fragment/MovieExtendsBox;

    invoke-interface {v4, v5}, Lcom/coremedia/iso/boxes/b;->getBoxes(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v4

    .line 95
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_1a

    .line 96
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :cond_3
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_6

    .line 196
    :goto_1
    invoke-static/range {v18 .. v18}, Lcom/coremedia/iso/boxes/TimeToSampleBox;->blowupTimeToSamples(Ljava/util/List;)[J

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/googlecode/mp4parser/authoring/d;->i:[J

    .line 198
    invoke-virtual/range {p2 .. p2}, Lcom/coremedia/iso/boxes/TrackBox;->getMediaBox()Lcom/coremedia/iso/boxes/MediaBox;

    move-result-object v4

    invoke-virtual {v4}, Lcom/coremedia/iso/boxes/MediaBox;->getMediaHeaderBox()Lcom/coremedia/iso/boxes/MediaHeaderBox;

    move-result-object v15

    .line 199
    invoke-virtual/range {p2 .. p2}, Lcom/coremedia/iso/boxes/TrackBox;->getTrackHeaderBox()Lcom/coremedia/iso/boxes/TrackHeaderBox;

    move-result-object v4

    .line 201
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/googlecode/mp4parser/authoring/d;->m:Lcom/googlecode/mp4parser/authoring/g;

    invoke-virtual {v4}, Lcom/coremedia/iso/boxes/TrackHeaderBox;->getTrackId()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/googlecode/mp4parser/authoring/g;->b(J)V

    .line 202
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/googlecode/mp4parser/authoring/d;->m:Lcom/googlecode/mp4parser/authoring/g;

    invoke-virtual {v15}, Lcom/coremedia/iso/boxes/MediaHeaderBox;->getCreationTime()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/googlecode/mp4parser/authoring/g;->b(Ljava/util/Date;)V

    .line 203
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/googlecode/mp4parser/authoring/d;->m:Lcom/googlecode/mp4parser/authoring/g;

    invoke-virtual {v15}, Lcom/coremedia/iso/boxes/MediaHeaderBox;->getLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/googlecode/mp4parser/authoring/g;->a(Ljava/lang/String;)V

    .line 205
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/googlecode/mp4parser/authoring/d;->m:Lcom/googlecode/mp4parser/authoring/g;

    invoke-virtual {v15}, Lcom/coremedia/iso/boxes/MediaHeaderBox;->getModificationTime()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/googlecode/mp4parser/authoring/g;->a(Ljava/util/Date;)V

    .line 206
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/googlecode/mp4parser/authoring/d;->m:Lcom/googlecode/mp4parser/authoring/g;

    invoke-virtual {v15}, Lcom/coremedia/iso/boxes/MediaHeaderBox;->getTimescale()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/googlecode/mp4parser/authoring/g;->a(J)V

    .line 207
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/googlecode/mp4parser/authoring/d;->m:Lcom/googlecode/mp4parser/authoring/g;

    invoke-virtual {v4}, Lcom/coremedia/iso/boxes/TrackHeaderBox;->getHeight()D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/googlecode/mp4parser/authoring/g;->b(D)V

    .line 208
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/googlecode/mp4parser/authoring/d;->m:Lcom/googlecode/mp4parser/authoring/g;

    invoke-virtual {v4}, Lcom/coremedia/iso/boxes/TrackHeaderBox;->getWidth()D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/googlecode/mp4parser/authoring/g;->a(D)V

    .line 209
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/googlecode/mp4parser/authoring/d;->m:Lcom/googlecode/mp4parser/authoring/g;

    invoke-virtual {v4}, Lcom/coremedia/iso/boxes/TrackHeaderBox;->getLayer()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/googlecode/mp4parser/authoring/g;->a(I)V

    .line 210
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/googlecode/mp4parser/authoring/d;->m:Lcom/googlecode/mp4parser/authoring/g;

    invoke-virtual {v4}, Lcom/coremedia/iso/boxes/TrackHeaderBox;->getMatrix()Lcom/googlecode/mp4parser/c/h;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/googlecode/mp4parser/authoring/g;->a(Lcom/googlecode/mp4parser/c/h;)V

    .line 211
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/googlecode/mp4parser/authoring/d;->m:Lcom/googlecode/mp4parser/authoring/g;

    invoke-virtual {v4}, Lcom/coremedia/iso/boxes/TrackHeaderBox;->getVolume()F

    move-result v4

    invoke-virtual {v5, v4}, Lcom/googlecode/mp4parser/authoring/g;->a(F)V

    .line 212
    const-string v4, "edts/elst"

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Lcom/googlecode/mp4parser/c/j;->a(Lcom/googlecode/mp4parser/AbstractContainerBox;Ljava/lang/String;)Lcom/coremedia/iso/boxes/a;

    move-result-object v4

    check-cast v4, Lcom/coremedia/iso/boxes/EditListBox;

    .line 213
    const-string v5, "../mvhd"

    move-object/from16 v0, p2

    invoke-static {v0, v5}, Lcom/googlecode/mp4parser/c/j;->a(Lcom/googlecode/mp4parser/AbstractContainerBox;Ljava/lang/String;)Lcom/coremedia/iso/boxes/a;

    move-result-object v5

    move-object v14, v5

    check-cast v14, Lcom/coremedia/iso/boxes/MovieHeaderBox;

    .line 214
    if-eqz v4, :cond_4

    .line 215
    invoke-virtual {v4}, Lcom/coremedia/iso/boxes/EditListBox;->getEntries()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_2
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1b

    .line 220
    :cond_4
    return-void

    .line 88
    :cond_5
    aget-object v6, p3, v4

    .line 89
    const-class v7, Lcom/coremedia/iso/boxes/fragment/MovieFragmentBox;

    invoke-virtual {v6, v7}, Lcom/coremedia/iso/d;->getBoxes(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v6

    move-object/from16 v0, v19

    invoke-interface {v0, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 88
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 96
    :cond_6
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/coremedia/iso/boxes/fragment/MovieExtendsBox;

    .line 97
    const-class v5, Lcom/coremedia/iso/boxes/fragment/TrackExtendsBox;

    invoke-virtual {v4, v5}, Lcom/coremedia/iso/boxes/fragment/MovieExtendsBox;->getBoxes(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v4

    .line 98
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :cond_7
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/coremedia/iso/boxes/fragment/TrackExtendsBox;

    .line 99
    invoke-virtual {v4}, Lcom/coremedia/iso/boxes/fragment/TrackExtendsBox;->getTrackId()J

    move-result-wide v6

    cmp-long v5, v6, v16

    if-nez v5, :cond_7

    .line 100
    invoke-virtual/range {p2 .. p2}, Lcom/coremedia/iso/boxes/TrackBox;->getParent()Lcom/coremedia/iso/boxes/b;

    move-result-object v5

    check-cast v5, Lcom/coremedia/iso/boxes/a;

    invoke-interface {v5}, Lcom/coremedia/iso/boxes/a;->getParent()Lcom/coremedia/iso/boxes/b;

    move-result-object v5

    const-string v6, "/moof/traf/subs"

    invoke-static {v5, v6}, Lcom/googlecode/mp4parser/c/j;->a(Lcom/coremedia/iso/boxes/b;Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    .line 101
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_8

    .line 102
    new-instance v5, Lcom/coremedia/iso/boxes/SubSampleInformationBox;

    invoke-direct {v5}, Lcom/coremedia/iso/boxes/SubSampleInformationBox;-><init>()V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/googlecode/mp4parser/authoring/d;->o:Lcom/coremedia/iso/boxes/SubSampleInformationBox;

    .line 105
    :cond_8
    const-wide/16 v6, 0x1

    .line 106
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v23

    :goto_3
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/coremedia/iso/boxes/fragment/MovieFragmentBox;

    .line 107
    const-class v8, Lcom/coremedia/iso/boxes/fragment/TrackFragmentBox;

    invoke-virtual {v5, v8}, Lcom/coremedia/iso/boxes/fragment/MovieFragmentBox;->getBoxes(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v5

    .line 108
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v24

    move-wide v14, v6

    :cond_9
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_a

    move-wide v6, v14

    goto :goto_3

    :cond_a
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v12, v5

    check-cast v12, Lcom/coremedia/iso/boxes/fragment/TrackFragmentBox;

    .line 109
    invoke-virtual {v12}, Lcom/coremedia/iso/boxes/fragment/TrackFragmentBox;->getTrackFragmentHeaderBox()Lcom/coremedia/iso/boxes/fragment/TrackFragmentHeaderBox;

    move-result-object v5

    invoke-virtual {v5}, Lcom/coremedia/iso/boxes/fragment/TrackFragmentHeaderBox;->getTrackId()J

    move-result-wide v6

    cmp-long v5, v6, v16

    if-nez v5, :cond_9

    .line 111
    const-class v5, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleGroupDescriptionBox;

    invoke-virtual {v13, v5}, Lcom/coremedia/iso/boxes/SampleTableBox;->getBoxes(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v6

    .line 112
    const-string v5, "sgpd"

    invoke-static {v12, v5}, Lcom/googlecode/mp4parser/c/j;->a(Lcom/coremedia/iso/boxes/b;Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    .line 113
    const-string v5, "sbgp"

    invoke-static {v12, v5}, Lcom/googlecode/mp4parser/c/j;->a(Lcom/coremedia/iso/boxes/b;Ljava/lang/String;)Ljava/util/List;

    move-result-object v8

    .line 114
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/googlecode/mp4parser/authoring/d;->c:Ljava/util/Map;

    const-wide/16 v10, 0x1

    sub-long v10, v14, v10

    move-object/from16 v5, p0

    .line 110
    invoke-direct/range {v5 .. v11}, Lcom/googlecode/mp4parser/authoring/d;->a(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/Map;J)Ljava/util/Map;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/googlecode/mp4parser/authoring/d;->c:Ljava/util/Map;

    .line 116
    const-string v5, "subs"

    invoke-static {v12, v5}, Lcom/googlecode/mp4parser/c/j;->a(Lcom/googlecode/mp4parser/AbstractContainerBox;Ljava/lang/String;)Lcom/coremedia/iso/boxes/a;

    move-result-object v5

    check-cast v5, Lcom/coremedia/iso/boxes/SubSampleInformationBox;

    .line 117
    if-eqz v5, :cond_b

    .line 118
    move/from16 v0, v20

    int-to-long v6, v0

    sub-long v6, v14, v6

    const-wide/16 v8, 0x1

    sub-long/2addr v6, v8

    .line 119
    invoke-virtual {v5}, Lcom/coremedia/iso/boxes/SubSampleInformationBox;->getEntries()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_11

    .line 133
    :cond_b
    const-class v5, Lcom/coremedia/iso/boxes/fragment/TrackRunBox;

    invoke-virtual {v12, v5}, Lcom/coremedia/iso/boxes/fragment/TrackFragmentBox;->getBoxes(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v5

    .line 134
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_c
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/coremedia/iso/boxes/fragment/TrackRunBox;

    .line 135
    invoke-virtual {v5}, Lcom/coremedia/iso/boxes/fragment/TrackRunBox;->getParent()Lcom/coremedia/iso/boxes/b;

    move-result-object v6

    check-cast v6, Lcom/coremedia/iso/boxes/fragment/TrackFragmentBox;

    invoke-virtual {v6}, Lcom/coremedia/iso/boxes/fragment/TrackFragmentBox;->getTrackFragmentHeaderBox()Lcom/coremedia/iso/boxes/fragment/TrackFragmentHeaderBox;

    move-result-object v10

    .line 136
    const/4 v6, 0x1

    .line 137
    invoke-virtual {v5}, Lcom/coremedia/iso/boxes/fragment/TrackRunBox;->getEntries()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    move v8, v6

    :goto_5
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_c

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/coremedia/iso/boxes/fragment/TrackRunBox$a;

    .line 138
    invoke-virtual {v5}, Lcom/coremedia/iso/boxes/fragment/TrackRunBox;->isSampleDurationPresent()Z

    move-result v7

    if-eqz v7, :cond_14

    .line 139
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v7

    if-eqz v7, :cond_d

    .line 140
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    move-object/from16 v0, v18

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/coremedia/iso/boxes/TimeToSampleBox$a;

    invoke-virtual {v7}, Lcom/coremedia/iso/boxes/TimeToSampleBox$a;->b()J

    move-result-wide v26

    invoke-virtual {v6}, Lcom/coremedia/iso/boxes/fragment/TrackRunBox$a;->a()J

    move-result-wide v28

    cmp-long v7, v26, v28

    if-eqz v7, :cond_13

    .line 141
    :cond_d
    new-instance v7, Lcom/coremedia/iso/boxes/TimeToSampleBox$a;

    const-wide/16 v26, 0x1

    invoke-virtual {v6}, Lcom/coremedia/iso/boxes/fragment/TrackRunBox$a;->a()J

    move-result-wide v28

    move-wide/from16 v0, v26

    move-wide/from16 v2, v28

    invoke-direct {v7, v0, v1, v2, v3}, Lcom/coremedia/iso/boxes/TimeToSampleBox$a;-><init>(JJ)V

    move-object/from16 v0, v18

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 154
    :goto_6
    invoke-virtual {v5}, Lcom/coremedia/iso/boxes/fragment/TrackRunBox;->isSampleCompositionTimeOffsetPresent()Z

    move-result v7

    if-eqz v7, :cond_f

    .line 155
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/googlecode/mp4parser/authoring/d;->j:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-eqz v7, :cond_e

    .line 156
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/googlecode/mp4parser/authoring/d;->j:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/googlecode/mp4parser/authoring/d;->j:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    invoke-interface {v7, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/coremedia/iso/boxes/CompositionTimeToSample$a;

    invoke-virtual {v7}, Lcom/coremedia/iso/boxes/CompositionTimeToSample$a;->b()I

    move-result v7

    int-to-long v0, v7

    move-wide/from16 v26, v0

    invoke-virtual {v6}, Lcom/coremedia/iso/boxes/fragment/TrackRunBox$a;->d()J

    move-result-wide v28

    cmp-long v7, v26, v28

    if-eqz v7, :cond_16

    .line 157
    :cond_e
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/googlecode/mp4parser/authoring/d;->j:Ljava/util/List;

    new-instance v12, Lcom/coremedia/iso/boxes/CompositionTimeToSample$a;

    const/16 v25, 0x1

    invoke-virtual {v6}, Lcom/coremedia/iso/boxes/fragment/TrackRunBox$a;->d()J

    move-result-wide v26

    invoke-static/range {v26 .. v27}, Lcom/googlecode/mp4parser/c/b;->a(J)I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-direct {v12, v0, v1}, Lcom/coremedia/iso/boxes/CompositionTimeToSample$a;-><init>(II)V

    invoke-interface {v7, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    :cond_f
    :goto_7
    invoke-virtual {v5}, Lcom/coremedia/iso/boxes/fragment/TrackRunBox;->isSampleFlagsPresent()Z

    move-result v7

    if-eqz v7, :cond_17

    .line 165
    invoke-virtual {v6}, Lcom/coremedia/iso/boxes/fragment/TrackRunBox$a;->c()Lcom/coremedia/iso/boxes/fragment/a;

    move-result-object v6

    .line 177
    :goto_8
    if-eqz v6, :cond_10

    invoke-virtual {v6}, Lcom/coremedia/iso/boxes/fragment/a;->a()Z

    move-result v6

    if-nez v6, :cond_10

    .line 179
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/googlecode/mp4parser/authoring/d;->k:[J

    const/4 v7, 0x1

    new-array v7, v7, [J

    const/4 v8, 0x0

    aput-wide v14, v7, v8

    invoke-static {v6, v7}, Lcom/googlecode/mp4parser/c/i;->a([J[J)[J

    move-result-object v6

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/googlecode/mp4parser/authoring/d;->k:[J

    .line 181
    :cond_10
    const-wide/16 v6, 0x1

    add-long/2addr v14, v6

    .line 182
    const/4 v6, 0x0

    move v8, v6

    goto/16 :goto_5

    .line 119
    :cond_11
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/coremedia/iso/boxes/SubSampleInformationBox$a;

    .line 120
    new-instance v9, Lcom/coremedia/iso/boxes/SubSampleInformationBox$a;

    invoke-direct {v9}, Lcom/coremedia/iso/boxes/SubSampleInformationBox$a;-><init>()V

    .line 121
    invoke-virtual {v9}, Lcom/coremedia/iso/boxes/SubSampleInformationBox$a;->c()Ljava/util/List;

    move-result-object v10

    invoke-virtual {v5}, Lcom/coremedia/iso/boxes/SubSampleInformationBox$a;->c()Ljava/util/List;

    move-result-object v11

    invoke-interface {v10, v11}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 122
    const-wide/16 v10, 0x0

    cmp-long v10, v6, v10

    if-eqz v10, :cond_12

    .line 123
    invoke-virtual {v5}, Lcom/coremedia/iso/boxes/SubSampleInformationBox$a;->a()J

    move-result-wide v10

    add-long/2addr v6, v10

    invoke-virtual {v9, v6, v7}, Lcom/coremedia/iso/boxes/SubSampleInformationBox$a;->a(J)V

    .line 124
    const-wide/16 v6, 0x0

    .line 128
    :goto_9
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/googlecode/mp4parser/authoring/d;->o:Lcom/coremedia/iso/boxes/SubSampleInformationBox;

    invoke-virtual {v5}, Lcom/coremedia/iso/boxes/SubSampleInformationBox;->getEntries()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 126
    :cond_12
    invoke-virtual {v5}, Lcom/coremedia/iso/boxes/SubSampleInformationBox$a;->a()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Lcom/coremedia/iso/boxes/SubSampleInformationBox$a;->a(J)V

    goto :goto_9

    .line 143
    :cond_13
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    move-object/from16 v0, v18

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/coremedia/iso/boxes/TimeToSampleBox$a;

    .line 144
    invoke-virtual {v7}, Lcom/coremedia/iso/boxes/TimeToSampleBox$a;->a()J

    move-result-wide v26

    const-wide/16 v28, 0x1

    add-long v26, v26, v28

    move-wide/from16 v0, v26

    invoke-virtual {v7, v0, v1}, Lcom/coremedia/iso/boxes/TimeToSampleBox$a;->a(J)V

    goto/16 :goto_6

    .line 147
    :cond_14
    invoke-virtual {v10}, Lcom/coremedia/iso/boxes/fragment/TrackFragmentHeaderBox;->hasDefaultSampleDuration()Z

    move-result v7

    if-eqz v7, :cond_15

    .line 148
    new-instance v7, Lcom/coremedia/iso/boxes/TimeToSampleBox$a;

    const-wide/16 v26, 0x1

    invoke-virtual {v10}, Lcom/coremedia/iso/boxes/fragment/TrackFragmentHeaderBox;->getDefaultSampleDuration()J

    move-result-wide v28

    move-wide/from16 v0, v26

    move-wide/from16 v2, v28

    invoke-direct {v7, v0, v1, v2, v3}, Lcom/coremedia/iso/boxes/TimeToSampleBox$a;-><init>(JJ)V

    move-object/from16 v0, v18

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    .line 150
    :cond_15
    new-instance v7, Lcom/coremedia/iso/boxes/TimeToSampleBox$a;

    const-wide/16 v26, 0x1

    invoke-virtual {v4}, Lcom/coremedia/iso/boxes/fragment/TrackExtendsBox;->getDefaultSampleDuration()J

    move-result-wide v28

    move-wide/from16 v0, v26

    move-wide/from16 v2, v28

    invoke-direct {v7, v0, v1, v2, v3}, Lcom/coremedia/iso/boxes/TimeToSampleBox$a;-><init>(JJ)V

    move-object/from16 v0, v18

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    .line 159
    :cond_16
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/googlecode/mp4parser/authoring/d;->j:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/googlecode/mp4parser/authoring/d;->j:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    invoke-interface {v7, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/coremedia/iso/boxes/CompositionTimeToSample$a;

    .line 160
    invoke-virtual {v7}, Lcom/coremedia/iso/boxes/CompositionTimeToSample$a;->a()I

    move-result v12

    add-int/lit8 v12, v12, 0x1

    invoke-virtual {v7, v12}, Lcom/coremedia/iso/boxes/CompositionTimeToSample$a;->a(I)V

    goto/16 :goto_7

    .line 167
    :cond_17
    if-eqz v8, :cond_18

    invoke-virtual {v5}, Lcom/coremedia/iso/boxes/fragment/TrackRunBox;->isFirstSampleFlagsPresent()Z

    move-result v6

    if-eqz v6, :cond_18

    .line 168
    invoke-virtual {v5}, Lcom/coremedia/iso/boxes/fragment/TrackRunBox;->getFirstSampleFlags()Lcom/coremedia/iso/boxes/fragment/a;

    move-result-object v6

    goto/16 :goto_8

    .line 170
    :cond_18
    invoke-virtual {v10}, Lcom/coremedia/iso/boxes/fragment/TrackFragmentHeaderBox;->hasDefaultSampleFlags()Z

    move-result v6

    if-eqz v6, :cond_19

    .line 171
    invoke-virtual {v10}, Lcom/coremedia/iso/boxes/fragment/TrackFragmentHeaderBox;->getDefaultSampleFlags()Lcom/coremedia/iso/boxes/fragment/a;

    move-result-object v6

    goto/16 :goto_8

    .line 173
    :cond_19
    invoke-virtual {v4}, Lcom/coremedia/iso/boxes/fragment/TrackExtendsBox;->getDefaultSampleFlags()Lcom/coremedia/iso/boxes/fragment/a;

    move-result-object v6

    goto/16 :goto_8

    .line 193
    :cond_1a
    const-class v4, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleGroupDescriptionBox;

    invoke-virtual {v13, v4}, Lcom/coremedia/iso/boxes/SampleTableBox;->getBoxes(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v6

    const/4 v7, 0x0

    const-class v4, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleToGroupBox;

    invoke-virtual {v13, v4}, Lcom/coremedia/iso/boxes/SampleTableBox;->getBoxes(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/googlecode/mp4parser/authoring/d;->c:Ljava/util/Map;

    const-wide/16 v10, 0x0

    move-object/from16 v5, p0

    invoke-direct/range {v5 .. v11}, Lcom/googlecode/mp4parser/authoring/d;->a(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/Map;J)Ljava/util/Map;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/googlecode/mp4parser/authoring/d;->c:Ljava/util/Map;

    goto/16 :goto_1

    .line 215
    :cond_1b
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/coremedia/iso/boxes/EditListBox$a;

    .line 216
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/googlecode/mp4parser/authoring/d;->b:Ljava/util/List;

    move-object/from16 v17, v0

    new-instance v5, Lcom/googlecode/mp4parser/authoring/b;

    invoke-virtual {v4}, Lcom/coremedia/iso/boxes/EditListBox$a;->b()J

    move-result-wide v6

    invoke-virtual {v15}, Lcom/coremedia/iso/boxes/MediaHeaderBox;->getTimescale()J

    move-result-wide v8

    invoke-virtual {v4}, Lcom/coremedia/iso/boxes/EditListBox$a;->c()D

    move-result-wide v10

    invoke-virtual {v4}, Lcom/coremedia/iso/boxes/EditListBox$a;->a()J

    move-result-wide v12

    long-to-double v12, v12

    invoke-virtual {v14}, Lcom/coremedia/iso/boxes/MovieHeaderBox;->getTimescale()J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-double v0, v0

    move-wide/from16 v18, v0

    div-double v12, v12, v18

    invoke-direct/range {v5 .. v13}, Lcom/googlecode/mp4parser/authoring/b;-><init>(JJDD)V

    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2
.end method

.method private a(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/Map;J)Ljava/util/Map;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleGroupDescriptionBox;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleGroupDescriptionBox;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleToGroupBox;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/b;",
            "[J>;J)",
            "Ljava/util/Map",
            "<",
            "Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/b;",
            "[J>;"
        }
    .end annotation

    .prologue
    .line 225
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 262
    return-object p4

    .line 225
    :cond_1
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleToGroupBox;

    .line 226
    const/4 v3, 0x0

    .line 227
    invoke-virtual {v2}, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleToGroupBox;->getEntries()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v5, v3

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleToGroupBox$a;

    .line 228
    invoke-virtual {v3}, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleToGroupBox$a;->b()I

    move-result v4

    if-lez v4, :cond_9

    .line 229
    const/4 v4, 0x0

    .line 230
    invoke-virtual {v3}, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleToGroupBox$a;->b()I

    move-result v6

    const v7, 0xffff

    if-le v6, v7, :cond_5

    .line 231
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v6, v4

    :cond_2
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_4

    .line 243
    :cond_3
    sget-boolean v4, Lcom/googlecode/mp4parser/authoring/d;->f:Z

    if-nez v4, :cond_7

    if-nez v6, :cond_7

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 231
    :cond_4
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleGroupDescriptionBox;

    .line 232
    invoke-virtual {v4}, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleGroupDescriptionBox;->getGroupingType()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2}, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleToGroupBox;->getGroupingType()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 233
    invoke-virtual {v4}, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleGroupDescriptionBox;->getGroupEntries()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v3}, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleToGroupBox$a;->b()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    const v10, 0xffff

    and-int/2addr v6, v10

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/b;

    move-object v6, v4

    goto :goto_1

    .line 237
    :cond_5
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v6, v4

    :cond_6
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleGroupDescriptionBox;

    .line 238
    invoke-virtual {v4}, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleGroupDescriptionBox;->getGroupingType()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2}, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleToGroupBox;->getGroupingType()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 239
    invoke-virtual {v4}, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleGroupDescriptionBox;->getGroupEntries()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v3}, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleToGroupBox$a;->b()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/b;

    move-object v6, v4

    goto :goto_2

    .line 244
    :cond_7
    move-object/from16 v0, p4

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [J

    .line 245
    if-nez v4, :cond_8

    .line 246
    const/4 v4, 0x0

    new-array v4, v4, [J

    .line 249
    :cond_8
    invoke-virtual {v3}, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleToGroupBox$a;->a()J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/googlecode/mp4parser/c/b;->a(J)I

    move-result v7

    array-length v10, v4

    add-int/2addr v7, v10

    new-array v10, v7, [J

    .line 250
    const/4 v7, 0x0

    const/4 v11, 0x0

    array-length v12, v4

    invoke-static {v4, v7, v10, v11, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 251
    const/4 v7, 0x0

    :goto_3
    int-to-long v12, v7

    invoke-virtual {v3}, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleToGroupBox$a;->a()J

    move-result-wide v14

    cmp-long v11, v12, v14

    if-ltz v11, :cond_a

    .line 254
    move-object/from16 v0, p4

    invoke-interface {v0, v6, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    :cond_9
    int-to-long v4, v5

    invoke-virtual {v3}, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleToGroupBox$a;->a()J

    move-result-wide v6

    add-long/2addr v4, v6

    long-to-int v3, v4

    move v5, v3

    goto/16 :goto_0

    .line 252
    :cond_a
    array-length v11, v4

    add-int/2addr v11, v7

    int-to-long v12, v5

    add-long v12, v12, p5

    int-to-long v14, v7

    add-long/2addr v12, v14

    aput-wide v12, v10, v11

    .line 251
    add-int/lit8 v7, v7, 0x1

    goto :goto_3
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/coremedia/iso/boxes/CompositionTimeToSample$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 292
    iget-object v0, p0, Lcom/googlecode/mp4parser/authoring/d;->j:Ljava/util/List;

    return-object v0
.end method

.method public b()[J
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Lcom/googlecode/mp4parser/authoring/d;->k:[J

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/googlecode/mp4parser/authoring/d;->k:[J

    array-length v0, v0

    iget-object v1, p0, Lcom/googlecode/mp4parser/authoring/d;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 297
    :cond_0
    const/4 v0, 0x0

    .line 299
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/googlecode/mp4parser/authoring/d;->k:[J

    goto :goto_0
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/coremedia/iso/boxes/SampleDependencyTypeBox$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 304
    iget-object v0, p0, Lcom/googlecode/mp4parser/authoring/d;->l:Ljava/util/List;

    return-object v0
.end method

.method public close()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 266
    iget-object v0, p0, Lcom/googlecode/mp4parser/authoring/d;->d:Lcom/coremedia/iso/boxes/TrackBox;

    invoke-virtual {v0}, Lcom/coremedia/iso/boxes/TrackBox;->getParent()Lcom/coremedia/iso/boxes/b;

    move-result-object v0

    .line 267
    instance-of v1, v0, Lcom/googlecode/mp4parser/a;

    if-eqz v1, :cond_0

    .line 268
    check-cast v0, Lcom/googlecode/mp4parser/a;

    invoke-virtual {v0}, Lcom/googlecode/mp4parser/a;->close()V

    .line 270
    :cond_0
    iget-object v0, p0, Lcom/googlecode/mp4parser/authoring/d;->e:[Lcom/coremedia/iso/d;

    if-eqz v0, :cond_1

    .line 271
    iget-object v1, p0, Lcom/googlecode/mp4parser/authoring/d;->e:[Lcom/coremedia/iso/d;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v2, :cond_2

    .line 277
    :cond_1
    return-void

    .line 271
    :cond_2
    aget-object v3, v1, v0

    .line 272
    invoke-virtual {v3}, Lcom/coremedia/iso/d;->close()V

    .line 271
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public d()Lcom/coremedia/iso/boxes/SubSampleInformationBox;
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/googlecode/mp4parser/authoring/d;->o:Lcom/coremedia/iso/boxes/SubSampleInformationBox;

    return-object v0
.end method

.method public h()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/mp4parser/authoring/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 280
    iget-object v0, p0, Lcom/googlecode/mp4parser/authoring/d;->g:Ljava/util/List;

    return-object v0
.end method

.method public declared-synchronized i()[J
    .locals 1

    .prologue
    .line 284
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/googlecode/mp4parser/authoring/d;->i:[J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public j()Lcom/coremedia/iso/boxes/SampleDescriptionBox;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/googlecode/mp4parser/authoring/d;->h:Lcom/coremedia/iso/boxes/SampleDescriptionBox;

    return-object v0
.end method

.method public k()Lcom/googlecode/mp4parser/authoring/g;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/googlecode/mp4parser/authoring/d;->m:Lcom/googlecode/mp4parser/authoring/g;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lcom/googlecode/mp4parser/authoring/d;->n:Ljava/lang/String;

    return-object v0
.end method
