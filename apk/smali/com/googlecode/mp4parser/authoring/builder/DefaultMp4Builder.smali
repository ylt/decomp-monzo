.class public Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;
.super Ljava/lang/Object;
.source "DefaultMp4Builder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder$InterleaveChunkMdat;
    }
.end annotation


# static fields
.field static final synthetic e:Z

.field private static f:Lcom/googlecode/mp4parser/c/f;


# instance fields
.field a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/googlecode/mp4parser/authoring/f;",
            "Lcom/coremedia/iso/boxes/StaticChunkOffsetBox;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/mp4parser/iso14496/part12/SampleAuxiliaryInformationOffsetsBox;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/googlecode/mp4parser/authoring/f;",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/mp4parser/authoring/e;",
            ">;>;"
        }
    .end annotation
.end field

.field d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/googlecode/mp4parser/authoring/f;",
            "[J>;"
        }
    .end annotation
.end field

.field private g:Lcom/googlecode/mp4parser/authoring/builder/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->e:Z

    .line 53
    const-class v0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;

    invoke-static {v0}, Lcom/googlecode/mp4parser/c/f;->a(Ljava/lang/Class;)Lcom/googlecode/mp4parser/c/f;

    move-result-object v0

    sput-object v0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->f:Lcom/googlecode/mp4parser/c/f;

    return-void

    .line 51
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->a:Ljava/util/Map;

    .line 55
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->b:Ljava/util/Set;

    .line 56
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->c:Ljava/util/HashMap;

    .line 57
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->d:Ljava/util/HashMap;

    .line 51
    return-void
.end method

.method private static a([I)J
    .locals 6

    .prologue
    .line 61
    const-wide/16 v2, 0x0

    .line 62
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v1, :cond_0

    .line 65
    return-wide v2

    .line 62
    :cond_0
    aget v4, p0, v0

    int-to-long v4, v4

    .line 63
    add-long/2addr v2, v4

    .line 62
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static a([J)J
    .locals 6

    .prologue
    .line 69
    const-wide/16 v2, 0x0

    .line 70
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v1, :cond_0

    .line 73
    return-wide v2

    .line 70
    :cond_0
    aget-wide v4, p0, v0

    .line 71
    add-long/2addr v2, v4

    .line 70
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method static synthetic a()Lcom/googlecode/mp4parser/c/f;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->f:Lcom/googlecode/mp4parser/c/f;

    return-object v0
.end method


# virtual methods
.method protected a(Lcom/googlecode/mp4parser/authoring/c;Ljava/util/Map;)Lcom/coremedia/iso/boxes/MovieBox;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/googlecode/mp4parser/authoring/c;",
            "Ljava/util/Map",
            "<",
            "Lcom/googlecode/mp4parser/authoring/f;",
            "[I>;)",
            "Lcom/coremedia/iso/boxes/MovieBox;"
        }
    .end annotation

    .prologue
    .line 186
    new-instance v6, Lcom/coremedia/iso/boxes/MovieBox;

    invoke-direct {v6}, Lcom/coremedia/iso/boxes/MovieBox;-><init>()V

    .line 187
    new-instance v7, Lcom/coremedia/iso/boxes/MovieHeaderBox;

    invoke-direct {v7}, Lcom/coremedia/iso/boxes/MovieHeaderBox;-><init>()V

    .line 189
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v7, v0}, Lcom/coremedia/iso/boxes/MovieHeaderBox;->setCreationTime(Ljava/util/Date;)V

    .line 190
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v7, v0}, Lcom/coremedia/iso/boxes/MovieHeaderBox;->setModificationTime(Ljava/util/Date;)V

    .line 191
    invoke-virtual {p1}, Lcom/googlecode/mp4parser/authoring/c;->d()Lcom/googlecode/mp4parser/c/h;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/coremedia/iso/boxes/MovieHeaderBox;->setMatrix(Lcom/googlecode/mp4parser/c/h;)V

    .line 192
    invoke-virtual {p0, p1}, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->d(Lcom/googlecode/mp4parser/authoring/c;)J

    move-result-wide v8

    .line 193
    const-wide/16 v0, 0x0

    .line 195
    invoke-virtual {p1}, Lcom/googlecode/mp4parser/authoring/c;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move-wide v2, v0

    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 216
    invoke-virtual {v7, v2, v3}, Lcom/coremedia/iso/boxes/MovieHeaderBox;->setDuration(J)V

    .line 217
    invoke-virtual {v7, v8, v9}, Lcom/coremedia/iso/boxes/MovieHeaderBox;->setTimescale(J)V

    .line 219
    const-wide/16 v0, 0x0

    .line 220
    invoke-virtual {p1}, Lcom/googlecode/mp4parser/authoring/c;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_6

    .line 223
    const-wide/16 v0, 0x1

    add-long/2addr v0, v2

    invoke-virtual {v7, v0, v1}, Lcom/coremedia/iso/boxes/MovieHeaderBox;->setNextTrackId(J)V

    .line 225
    invoke-virtual {v6, v7}, Lcom/coremedia/iso/boxes/MovieBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 226
    invoke-virtual {p1}, Lcom/googlecode/mp4parser/authoring/c;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_8

    .line 230
    invoke-virtual {p0, p1}, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->c(Lcom/googlecode/mp4parser/authoring/c;)Lcom/coremedia/iso/boxes/a;

    move-result-object v0

    .line 231
    if-eqz v0, :cond_1

    .line 232
    invoke-virtual {v6, v0}, Lcom/coremedia/iso/boxes/MovieBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 234
    :cond_1
    return-object v6

    .line 195
    :cond_2
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/googlecode/mp4parser/authoring/f;

    .line 198
    invoke-interface {v0}, Lcom/googlecode/mp4parser/authoring/f;->f()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Lcom/googlecode/mp4parser/authoring/f;->f()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 199
    :cond_3
    invoke-interface {v0}, Lcom/googlecode/mp4parser/authoring/f;->e()J

    move-result-wide v4

    mul-long/2addr v4, v8

    invoke-interface {v0}, Lcom/googlecode/mp4parser/authoring/f;->k()Lcom/googlecode/mp4parser/authoring/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/googlecode/mp4parser/authoring/g;->b()J

    move-result-wide v0

    div-long v0, v4, v0

    .line 209
    :goto_3
    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    move-wide v2, v0

    .line 210
    goto :goto_0

    .line 201
    :cond_4
    const-wide/16 v4, 0x0

    .line 202
    invoke-interface {v0}, Lcom/googlecode/mp4parser/authoring/f;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_4
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_5

    .line 205
    long-to-double v0, v8

    mul-double/2addr v0, v4

    double-to-long v0, v0

    goto :goto_3

    .line 202
    :cond_5
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/googlecode/mp4parser/authoring/b;

    .line 203
    invoke-virtual {v0}, Lcom/googlecode/mp4parser/authoring/b;->b()D

    move-result-wide v0

    double-to-long v0, v0

    long-to-double v0, v0

    add-double/2addr v0, v4

    move-wide v4, v0

    goto :goto_4

    .line 220
    :cond_6
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/googlecode/mp4parser/authoring/f;

    .line 221
    invoke-interface {v0}, Lcom/googlecode/mp4parser/authoring/f;->k()Lcom/googlecode/mp4parser/authoring/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/googlecode/mp4parser/authoring/g;->f()J

    move-result-wide v8

    cmp-long v1, v2, v8

    if-gez v1, :cond_7

    invoke-interface {v0}, Lcom/googlecode/mp4parser/authoring/f;->k()Lcom/googlecode/mp4parser/authoring/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/googlecode/mp4parser/authoring/g;->f()J

    move-result-wide v0

    :goto_5
    move-wide v2, v0

    goto/16 :goto_1

    :cond_7
    move-wide v0, v2

    goto :goto_5

    .line 226
    :cond_8
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/googlecode/mp4parser/authoring/f;

    .line 227
    invoke-virtual {p0, v0, p1, p2}, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->a(Lcom/googlecode/mp4parser/authoring/f;Lcom/googlecode/mp4parser/authoring/c;Ljava/util/Map;)Lcom/coremedia/iso/boxes/TrackBox;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/coremedia/iso/boxes/MovieBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    goto/16 :goto_2
.end method

.method protected a(Lcom/googlecode/mp4parser/authoring/f;Lcom/googlecode/mp4parser/authoring/c;Ljava/util/Map;)Lcom/coremedia/iso/boxes/TrackBox;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/googlecode/mp4parser/authoring/f;",
            "Lcom/googlecode/mp4parser/authoring/c;",
            "Ljava/util/Map",
            "<",
            "Lcom/googlecode/mp4parser/authoring/f;",
            "[I>;)",
            "Lcom/coremedia/iso/boxes/TrackBox;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 250
    new-instance v4, Lcom/coremedia/iso/boxes/TrackBox;

    invoke-direct {v4}, Lcom/coremedia/iso/boxes/TrackBox;-><init>()V

    .line 251
    new-instance v5, Lcom/coremedia/iso/boxes/TrackHeaderBox;

    invoke-direct {v5}, Lcom/coremedia/iso/boxes/TrackHeaderBox;-><init>()V

    .line 253
    invoke-virtual {v5, v7}, Lcom/coremedia/iso/boxes/TrackHeaderBox;->setEnabled(Z)V

    .line 254
    invoke-virtual {v5, v7}, Lcom/coremedia/iso/boxes/TrackHeaderBox;->setInMovie(Z)V

    .line 257
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->k()Lcom/googlecode/mp4parser/authoring/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/googlecode/mp4parser/authoring/g;->j()Lcom/googlecode/mp4parser/c/h;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/coremedia/iso/boxes/TrackHeaderBox;->setMatrix(Lcom/googlecode/mp4parser/c/h;)V

    .line 259
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->k()Lcom/googlecode/mp4parser/authoring/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/googlecode/mp4parser/authoring/g;->i()I

    move-result v0

    invoke-virtual {v5, v0}, Lcom/coremedia/iso/boxes/TrackHeaderBox;->setAlternateGroup(I)V

    .line 260
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->k()Lcom/googlecode/mp4parser/authoring/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/googlecode/mp4parser/authoring/g;->c()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/coremedia/iso/boxes/TrackHeaderBox;->setCreationTime(Ljava/util/Date;)V

    .line 262
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->f()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 263
    :cond_0
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->e()J

    move-result-wide v0

    invoke-virtual {p0, p2}, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->d(Lcom/googlecode/mp4parser/authoring/c;)J

    move-result-wide v2

    mul-long/2addr v0, v2

    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->k()Lcom/googlecode/mp4parser/authoring/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/googlecode/mp4parser/authoring/g;->b()J

    move-result-wide v2

    div-long/2addr v0, v2

    invoke-virtual {v5, v0, v1}, Lcom/coremedia/iso/boxes/TrackHeaderBox;->setDuration(J)V

    .line 273
    :goto_0
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->k()Lcom/googlecode/mp4parser/authoring/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/googlecode/mp4parser/authoring/g;->e()D

    move-result-wide v0

    invoke-virtual {v5, v0, v1}, Lcom/coremedia/iso/boxes/TrackHeaderBox;->setHeight(D)V

    .line 274
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->k()Lcom/googlecode/mp4parser/authoring/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/googlecode/mp4parser/authoring/g;->d()D

    move-result-wide v0

    invoke-virtual {v5, v0, v1}, Lcom/coremedia/iso/boxes/TrackHeaderBox;->setWidth(D)V

    .line 275
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->k()Lcom/googlecode/mp4parser/authoring/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/googlecode/mp4parser/authoring/g;->g()I

    move-result v0

    invoke-virtual {v5, v0}, Lcom/coremedia/iso/boxes/TrackHeaderBox;->setLayer(I)V

    .line 276
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v5, v0}, Lcom/coremedia/iso/boxes/TrackHeaderBox;->setModificationTime(Ljava/util/Date;)V

    .line 277
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->k()Lcom/googlecode/mp4parser/authoring/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/googlecode/mp4parser/authoring/g;->f()J

    move-result-wide v0

    invoke-virtual {v5, v0, v1}, Lcom/coremedia/iso/boxes/TrackHeaderBox;->setTrackId(J)V

    .line 278
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->k()Lcom/googlecode/mp4parser/authoring/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/googlecode/mp4parser/authoring/g;->h()F

    move-result v0

    invoke-virtual {v5, v0}, Lcom/coremedia/iso/boxes/TrackHeaderBox;->setVolume(F)V

    .line 280
    invoke-virtual {v4, v5}, Lcom/coremedia/iso/boxes/TrackBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 282
    invoke-virtual {p0, p1, p2}, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->a(Lcom/googlecode/mp4parser/authoring/f;Lcom/googlecode/mp4parser/authoring/c;)Lcom/coremedia/iso/boxes/a;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/coremedia/iso/boxes/TrackBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 284
    new-instance v0, Lcom/coremedia/iso/boxes/MediaBox;

    invoke-direct {v0}, Lcom/coremedia/iso/boxes/MediaBox;-><init>()V

    .line 285
    invoke-virtual {v4, v0}, Lcom/coremedia/iso/boxes/TrackBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 286
    new-instance v1, Lcom/coremedia/iso/boxes/MediaHeaderBox;

    invoke-direct {v1}, Lcom/coremedia/iso/boxes/MediaHeaderBox;-><init>()V

    .line 287
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->k()Lcom/googlecode/mp4parser/authoring/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/googlecode/mp4parser/authoring/g;->c()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/coremedia/iso/boxes/MediaHeaderBox;->setCreationTime(Ljava/util/Date;)V

    .line 288
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->e()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/coremedia/iso/boxes/MediaHeaderBox;->setDuration(J)V

    .line 289
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->k()Lcom/googlecode/mp4parser/authoring/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/googlecode/mp4parser/authoring/g;->b()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/coremedia/iso/boxes/MediaHeaderBox;->setTimescale(J)V

    .line 290
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->k()Lcom/googlecode/mp4parser/authoring/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/googlecode/mp4parser/authoring/g;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/coremedia/iso/boxes/MediaHeaderBox;->setLanguage(Ljava/lang/String;)V

    .line 291
    invoke-virtual {v0, v1}, Lcom/coremedia/iso/boxes/MediaBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 292
    new-instance v1, Lcom/coremedia/iso/boxes/HandlerBox;

    invoke-direct {v1}, Lcom/coremedia/iso/boxes/HandlerBox;-><init>()V

    .line 293
    invoke-virtual {v0, v1}, Lcom/coremedia/iso/boxes/MediaBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 295
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/coremedia/iso/boxes/HandlerBox;->setHandlerType(Ljava/lang/String;)V

    .line 297
    new-instance v1, Lcom/coremedia/iso/boxes/MediaInformationBox;

    invoke-direct {v1}, Lcom/coremedia/iso/boxes/MediaInformationBox;-><init>()V

    .line 298
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->l()Ljava/lang/String;

    move-result-object v2

    const-string v3, "vide"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 299
    new-instance v2, Lcom/coremedia/iso/boxes/VideoMediaHeaderBox;

    invoke-direct {v2}, Lcom/coremedia/iso/boxes/VideoMediaHeaderBox;-><init>()V

    invoke-virtual {v1, v2}, Lcom/coremedia/iso/boxes/MediaInformationBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 314
    :cond_1
    :goto_1
    new-instance v2, Lcom/coremedia/iso/boxes/DataInformationBox;

    invoke-direct {v2}, Lcom/coremedia/iso/boxes/DataInformationBox;-><init>()V

    .line 315
    new-instance v3, Lcom/coremedia/iso/boxes/DataReferenceBox;

    invoke-direct {v3}, Lcom/coremedia/iso/boxes/DataReferenceBox;-><init>()V

    .line 316
    invoke-virtual {v2, v3}, Lcom/coremedia/iso/boxes/DataInformationBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 317
    new-instance v5, Lcom/coremedia/iso/boxes/DataEntryUrlBox;

    invoke-direct {v5}, Lcom/coremedia/iso/boxes/DataEntryUrlBox;-><init>()V

    .line 318
    invoke-virtual {v5, v7}, Lcom/coremedia/iso/boxes/DataEntryUrlBox;->setFlags(I)V

    .line 319
    invoke-virtual {v3, v5}, Lcom/coremedia/iso/boxes/DataReferenceBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 320
    invoke-virtual {v1, v2}, Lcom/coremedia/iso/boxes/MediaInformationBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 323
    invoke-virtual {p0, p1, p2, p3}, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->b(Lcom/googlecode/mp4parser/authoring/f;Lcom/googlecode/mp4parser/authoring/c;Ljava/util/Map;)Lcom/coremedia/iso/boxes/a;

    move-result-object v2

    .line 324
    invoke-virtual {v1, v2}, Lcom/coremedia/iso/boxes/MediaInformationBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 325
    invoke-virtual {v0, v1}, Lcom/coremedia/iso/boxes/MediaBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 326
    sget-object v0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->f:Lcom/googlecode/mp4parser/c/f;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "done with trak for track_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->k()Lcom/googlecode/mp4parser/authoring/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/googlecode/mp4parser/authoring/g;->f()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/googlecode/mp4parser/c/f;->a(Ljava/lang/String;)V

    .line 327
    return-object v4

    .line 265
    :cond_2
    const-wide/16 v0, 0x0

    .line 266
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->f()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-wide v2, v0

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 269
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->k()Lcom/googlecode/mp4parser/authoring/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/googlecode/mp4parser/authoring/g;->b()J

    move-result-wide v0

    mul-long/2addr v0, v2

    invoke-virtual {v5, v0, v1}, Lcom/coremedia/iso/boxes/TrackHeaderBox;->setDuration(J)V

    goto/16 :goto_0

    .line 266
    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/googlecode/mp4parser/authoring/b;

    .line 267
    invoke-virtual {v0}, Lcom/googlecode/mp4parser/authoring/b;->b()D

    move-result-wide v0

    double-to-long v0, v0

    add-long/2addr v0, v2

    move-wide v2, v0

    goto :goto_2

    .line 300
    :cond_4
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->l()Ljava/lang/String;

    move-result-object v2

    const-string v3, "soun"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 301
    new-instance v2, Lcom/coremedia/iso/boxes/SoundMediaHeaderBox;

    invoke-direct {v2}, Lcom/coremedia/iso/boxes/SoundMediaHeaderBox;-><init>()V

    invoke-virtual {v1, v2}, Lcom/coremedia/iso/boxes/MediaInformationBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    goto/16 :goto_1

    .line 302
    :cond_5
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->l()Ljava/lang/String;

    move-result-object v2

    const-string v3, "text"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 303
    new-instance v2, Lcom/coremedia/iso/boxes/NullMediaHeaderBox;

    invoke-direct {v2}, Lcom/coremedia/iso/boxes/NullMediaHeaderBox;-><init>()V

    invoke-virtual {v1, v2}, Lcom/coremedia/iso/boxes/MediaInformationBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    goto/16 :goto_1

    .line 304
    :cond_6
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->l()Ljava/lang/String;

    move-result-object v2

    const-string v3, "subt"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 305
    new-instance v2, Lcom/coremedia/iso/boxes/SubtitleMediaHeaderBox;

    invoke-direct {v2}, Lcom/coremedia/iso/boxes/SubtitleMediaHeaderBox;-><init>()V

    invoke-virtual {v1, v2}, Lcom/coremedia/iso/boxes/MediaInformationBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    goto/16 :goto_1

    .line 306
    :cond_7
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->l()Ljava/lang/String;

    move-result-object v2

    const-string v3, "hint"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 307
    new-instance v2, Lcom/coremedia/iso/boxes/HintMediaHeaderBox;

    invoke-direct {v2}, Lcom/coremedia/iso/boxes/HintMediaHeaderBox;-><init>()V

    invoke-virtual {v1, v2}, Lcom/coremedia/iso/boxes/MediaInformationBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    goto/16 :goto_1

    .line 308
    :cond_8
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->l()Ljava/lang/String;

    move-result-object v2

    const-string v3, "sbtl"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 309
    new-instance v2, Lcom/coremedia/iso/boxes/NullMediaHeaderBox;

    invoke-direct {v2}, Lcom/coremedia/iso/boxes/NullMediaHeaderBox;-><init>()V

    invoke-virtual {v1, v2}, Lcom/coremedia/iso/boxes/MediaInformationBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    goto/16 :goto_1
.end method

.method protected a(Lcom/googlecode/mp4parser/authoring/f;Lcom/googlecode/mp4parser/authoring/c;)Lcom/coremedia/iso/boxes/a;
    .locals 12

    .prologue
    .line 331
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->f()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 332
    new-instance v1, Lcom/coremedia/iso/boxes/EditListBox;

    invoke-direct {v1}, Lcom/coremedia/iso/boxes/EditListBox;-><init>()V

    .line 333
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/coremedia/iso/boxes/EditListBox;->setVersion(I)V

    .line 334
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 336
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 343
    invoke-virtual {v1, v8}, Lcom/coremedia/iso/boxes/EditListBox;->setEntries(Ljava/util/List;)V

    .line 344
    new-instance v0, Lcom/coremedia/iso/boxes/EditBox;

    invoke-direct {v0}, Lcom/coremedia/iso/boxes/EditBox;-><init>()V

    .line 345
    invoke-virtual {v0, v1}, Lcom/coremedia/iso/boxes/EditBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 348
    :goto_1
    return-object v0

    .line 336
    :cond_0
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/googlecode/mp4parser/authoring/b;

    .line 337
    new-instance v0, Lcom/coremedia/iso/boxes/EditListBox$a;

    .line 338
    invoke-virtual {v6}, Lcom/googlecode/mp4parser/authoring/b;->b()D

    move-result-wide v2

    invoke-virtual {p2}, Lcom/googlecode/mp4parser/authoring/c;->c()J

    move-result-wide v4

    long-to-double v4, v4

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    .line 339
    invoke-virtual {v6}, Lcom/googlecode/mp4parser/authoring/b;->c()J

    move-result-wide v4

    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->k()Lcom/googlecode/mp4parser/authoring/g;

    move-result-object v7

    invoke-virtual {v7}, Lcom/googlecode/mp4parser/authoring/g;->b()J

    move-result-wide v10

    mul-long/2addr v4, v10

    invoke-virtual {v6}, Lcom/googlecode/mp4parser/authoring/b;->a()J

    move-result-wide v10

    div-long/2addr v4, v10

    .line 340
    invoke-virtual {v6}, Lcom/googlecode/mp4parser/authoring/b;->d()D

    move-result-wide v6

    invoke-direct/range {v0 .. v7}, Lcom/coremedia/iso/boxes/EditListBox$a;-><init>(Lcom/coremedia/iso/boxes/EditListBox;JJD)V

    .line 337
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 348
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Lcom/googlecode/mp4parser/authoring/c;)Lcom/coremedia/iso/boxes/b;
    .locals 12

    .prologue
    const/4 v7, 0x0

    .line 91
    iget-object v0, p0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->g:Lcom/googlecode/mp4parser/authoring/builder/c;

    if-nez v0, :cond_0

    .line 92
    new-instance v0, Lcom/googlecode/mp4parser/authoring/builder/a;

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-direct {v0, v2, v3}, Lcom/googlecode/mp4parser/authoring/builder/a;-><init>(D)V

    iput-object v0, p0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->g:Lcom/googlecode/mp4parser/authoring/builder/c;

    .line 94
    :cond_0
    sget-object v0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->f:Lcom/googlecode/mp4parser/c/f;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Creating movie "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/googlecode/mp4parser/c/f;->a(Ljava/lang/String;)V

    .line 95
    invoke-virtual {p1}, Lcom/googlecode/mp4parser/authoring/c;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 108
    new-instance v8, Lcom/googlecode/mp4parser/a;

    invoke-direct {v8}, Lcom/googlecode/mp4parser/a;-><init>()V

    .line 110
    invoke-virtual {p0, p1}, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->b(Lcom/googlecode/mp4parser/authoring/c;)Lcom/coremedia/iso/boxes/FileTypeBox;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/googlecode/mp4parser/a;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 112
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 113
    invoke-virtual {p1}, Lcom/googlecode/mp4parser/authoring/c;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_4

    .line 116
    invoke-virtual {p0, p1, v3}, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->a(Lcom/googlecode/mp4parser/authoring/c;Ljava/util/Map;)Lcom/coremedia/iso/boxes/MovieBox;

    move-result-object v0

    .line 117
    invoke-virtual {v8, v0}, Lcom/googlecode/mp4parser/a;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 118
    const-string v1, "trak/mdia/minf/stbl/stsz"

    invoke-static {v0, v1}, Lcom/googlecode/mp4parser/c/j;->a(Lcom/coremedia/iso/boxes/a;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 120
    const-wide/16 v4, 0x0

    .line 121
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_5

    .line 125
    sget-object v0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->f:Lcom/googlecode/mp4parser/c/f;

    const-string v1, "About to create mdat"

    invoke-virtual {v0, v1}, Lcom/googlecode/mp4parser/c/f;->a(Ljava/lang/String;)V

    .line 126
    new-instance v0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder$InterleaveChunkMdat;

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder$InterleaveChunkMdat;-><init>(Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;Lcom/googlecode/mp4parser/authoring/c;Ljava/util/Map;JLcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder$InterleaveChunkMdat;)V

    .line 127
    invoke-virtual {v8, v0}, Lcom/googlecode/mp4parser/a;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 128
    sget-object v1, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->f:Lcom/googlecode/mp4parser/c/f;

    const-string v2, "mdat crated"

    invoke-virtual {v1, v2}, Lcom/googlecode/mp4parser/c/f;->a(Ljava/lang/String;)V

    .line 134
    invoke-virtual {v0}, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder$InterleaveChunkMdat;->getDataOffset()J

    move-result-wide v2

    .line 135
    iget-object v0, p0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_6

    .line 141
    iget-object v0, p0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_7

    .line 168
    return-object v8

    .line 95
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/googlecode/mp4parser/authoring/f;

    .line 97
    invoke-interface {v0}, Lcom/googlecode/mp4parser/authoring/f;->h()Ljava/util/List;

    move-result-object v4

    .line 98
    invoke-virtual {p0, v0, v4}, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->a(Lcom/googlecode/mp4parser/authoring/f;Ljava/util/List;)Ljava/util/List;

    .line 99
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    new-array v5, v1, [J

    move v2, v7

    .line 100
    :goto_4
    array-length v1, v5

    if-lt v2, v1, :cond_3

    .line 104
    iget-object v1, p0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->d:Ljava/util/HashMap;

    invoke-virtual {v1, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 101
    :cond_3
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/googlecode/mp4parser/authoring/e;

    .line 102
    invoke-interface {v1}, Lcom/googlecode/mp4parser/authoring/e;->a()J

    move-result-wide v8

    aput-wide v8, v5, v2

    .line 100
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_4

    .line 113
    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/googlecode/mp4parser/authoring/f;

    .line 114
    invoke-virtual {p0, v0}, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->a(Lcom/googlecode/mp4parser/authoring/f;)[I

    move-result-object v2

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 121
    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/coremedia/iso/boxes/SampleSizeBox;

    .line 122
    invoke-virtual {v0}, Lcom/coremedia/iso/boxes/SampleSizeBox;->getSampleSizes()[J

    move-result-object v0

    invoke-static {v0}, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->a([J)J

    move-result-wide v10

    add-long/2addr v4, v10

    goto/16 :goto_2

    .line 135
    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/coremedia/iso/boxes/StaticChunkOffsetBox;

    .line 136
    invoke-virtual {v0}, Lcom/coremedia/iso/boxes/StaticChunkOffsetBox;->getChunkOffsets()[J

    move-result-object v4

    move v0, v7

    .line 137
    :goto_5
    array-length v5, v4

    if-ge v0, v5, :cond_1

    .line 138
    aget-wide v10, v4, v0

    add-long/2addr v10, v2

    aput-wide v10, v4, v0

    .line 137
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 141
    :cond_7
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mp4parser/iso14496/part12/SampleAuxiliaryInformationOffsetsBox;

    .line 142
    invoke-virtual {v0}, Lcom/mp4parser/iso14496/part12/SampleAuxiliaryInformationOffsetsBox;->getSize()J

    move-result-wide v2

    .line 143
    const-wide/16 v4, 0x2c

    add-long/2addr v2, v4

    move-wide v4, v2

    move-object v2, v0

    :goto_6
    move-object v1, v2

    .line 148
    check-cast v1, Lcom/coremedia/iso/boxes/a;

    invoke-interface {v1}, Lcom/coremedia/iso/boxes/a;->getParent()Lcom/coremedia/iso/boxes/b;

    move-result-object v3

    move-object v1, v3

    .line 150
    check-cast v1, Lcom/coremedia/iso/boxes/b;

    invoke-interface {v1}, Lcom/coremedia/iso/boxes/b;->getBoxes()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_7
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_9

    .line 157
    :cond_8
    instance-of v1, v3, Lcom/coremedia/iso/boxes/a;

    if-nez v1, :cond_b

    .line 159
    invoke-virtual {v0}, Lcom/mp4parser/iso14496/part12/SampleAuxiliaryInformationOffsetsBox;->getOffsets()[J

    move-result-object v2

    move v1, v7

    .line 160
    :goto_8
    array-length v3, v2

    if-lt v1, v3, :cond_a

    .line 164
    invoke-virtual {v0, v2}, Lcom/mp4parser/iso14496/part12/SampleAuxiliaryInformationOffsetsBox;->setOffsets([J)V

    goto/16 :goto_3

    .line 150
    :cond_9
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/coremedia/iso/boxes/a;

    .line 151
    if-eq v1, v2, :cond_8

    .line 154
    invoke-interface {v1}, Lcom/coremedia/iso/boxes/a;->getSize()J

    move-result-wide v10

    add-long/2addr v4, v10

    goto :goto_7

    .line 161
    :cond_a
    aget-wide v10, v2, v1

    add-long/2addr v10, v4

    aput-wide v10, v2, v1

    .line 160
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    :cond_b
    move-object v2, v3

    goto :goto_6
.end method

.method protected a(Lcom/googlecode/mp4parser/authoring/f;Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/googlecode/mp4parser/authoring/f;",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/mp4parser/authoring/e;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/mp4parser/authoring/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 172
    iget-object v0, p0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method protected a(Lcom/googlecode/mp4parser/authoring/f;Lcom/coremedia/iso/boxes/SampleTableBox;)V
    .locals 1

    .prologue
    .line 412
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->d()Lcom/coremedia/iso/boxes/SubSampleInformationBox;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 413
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->d()Lcom/coremedia/iso/boxes/SubSampleInformationBox;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/coremedia/iso/boxes/SampleTableBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 415
    :cond_0
    return-void
.end method

.method protected a(Lcom/googlecode/mp4parser/authoring/f;Lcom/googlecode/mp4parser/authoring/c;Ljava/util/Map;Lcom/coremedia/iso/boxes/SampleTableBox;)V
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/googlecode/mp4parser/authoring/f;",
            "Lcom/googlecode/mp4parser/authoring/c;",
            "Ljava/util/Map",
            "<",
            "Lcom/googlecode/mp4parser/authoring/f;",
            "[I>;",
            "Lcom/coremedia/iso/boxes/SampleTableBox;",
            ")V"
        }
    .end annotation

    .prologue
    .line 466
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->a:Ljava/util/Map;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_1

    .line 472
    const-wide/16 v4, 0x0

    .line 474
    sget-object v2, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->f:Lcom/googlecode/mp4parser/c/f;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "Calculating chunk offsets for track_"

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {p1 .. p1}, Lcom/googlecode/mp4parser/authoring/f;->k()Lcom/googlecode/mp4parser/authoring/g;

    move-result-object v6

    invoke-virtual {v6}, Lcom/googlecode/mp4parser/authoring/g;->f()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/googlecode/mp4parser/c/f;->a(Ljava/lang/String;)V

    .line 476
    new-instance v12, Ljava/util/ArrayList;

    invoke-interface/range {p3 .. p3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v12, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 477
    new-instance v2, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder$1;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder$1;-><init>(Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;)V

    invoke-static {v12, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 482
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 483
    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    .line 484
    new-instance v15, Ljava/util/HashMap;

    invoke-direct {v15}, Ljava/util/HashMap;-><init>()V

    .line 485
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    move-wide v6, v4

    .line 493
    :goto_1
    const/4 v2, 0x0

    .line 494
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v4, v2

    :cond_0
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 503
    if-nez v4, :cond_5

    .line 528
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->a:Ljava/util/Map;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/coremedia/iso/boxes/a;

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Lcom/coremedia/iso/boxes/SampleTableBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 529
    return-void

    .line 485
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/googlecode/mp4parser/authoring/f;

    .line 486
    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v13, v2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 487
    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v14, v2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 488
    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-interface {v15, v2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 489
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->a:Ljava/util/Map;

    new-instance v7, Lcom/coremedia/iso/boxes/StaticChunkOffsetBox;

    invoke-direct {v7}, Lcom/coremedia/iso/boxes/StaticChunkOffsetBox;-><init>()V

    invoke-interface {v6, v2, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 494
    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/googlecode/mp4parser/authoring/f;

    .line 496
    if-eqz v4, :cond_4

    invoke-interface {v15, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    invoke-interface {v15, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    cmpg-double v3, v8, v10

    if-gez v3, :cond_0

    .line 499
    :cond_4
    invoke-interface {v13, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v8

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [I

    array-length v3, v3

    if-ge v8, v3, :cond_0

    move-object v4, v2

    .line 500
    goto :goto_2

    .line 507
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->a:Ljava/util/Map;

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/coremedia/iso/boxes/ChunkOffsetBox;

    .line 508
    invoke-virtual {v2}, Lcom/coremedia/iso/boxes/ChunkOffsetBox;->getChunkOffsets()[J

    move-result-object v3

    const/4 v5, 0x1

    new-array v5, v5, [J

    const/4 v8, 0x0

    aput-wide v6, v5, v8

    invoke-static {v3, v5}, Lcom/googlecode/mp4parser/c/i;->a([J[J)[J

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/coremedia/iso/boxes/ChunkOffsetBox;->setChunkOffsets([J)V

    .line 510
    invoke-interface {v13, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v16

    .line 512
    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [I

    aget v17, v2, v16

    .line 513
    invoke-interface {v14, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 514
    invoke-interface {v15, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    .line 516
    invoke-interface {v4}, Lcom/googlecode/mp4parser/authoring/f;->i()[J

    move-result-object v18

    move-wide v8, v2

    move v3, v5

    .line 517
    :goto_3
    add-int v2, v5, v17

    if-lt v3, v2, :cond_6

    .line 521
    add-int/lit8 v2, v16, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v13, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 522
    add-int v2, v5, v17

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v14, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 523
    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-interface {v15, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 518
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->d:Ljava/util/HashMap;

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [J

    aget-wide v10, v2, v3

    add-long/2addr v10, v6

    .line 519
    aget-wide v6, v18, v3

    long-to-double v6, v6

    invoke-interface {v4}, Lcom/googlecode/mp4parser/authoring/f;->k()Lcom/googlecode/mp4parser/authoring/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/googlecode/mp4parser/authoring/g;->b()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-double v0, v0

    move-wide/from16 v20, v0

    div-double v6, v6, v20

    add-double/2addr v6, v8

    .line 517
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-wide v8, v6

    move-wide v6, v10

    goto :goto_3
.end method

.method protected a(Lcom/googlecode/mp4parser/authoring/f;Ljava/util/Map;Lcom/coremedia/iso/boxes/SampleTableBox;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/googlecode/mp4parser/authoring/f;",
            "Ljava/util/Map",
            "<",
            "Lcom/googlecode/mp4parser/authoring/f;",
            "[I>;",
            "Lcom/coremedia/iso/boxes/SampleTableBox;",
            ")V"
        }
    .end annotation

    .prologue
    .line 539
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .line 541
    new-instance v9, Lcom/coremedia/iso/boxes/SampleToChunkBox;

    invoke-direct {v9}, Lcom/coremedia/iso/boxes/SampleToChunkBox;-><init>()V

    .line 542
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    invoke-virtual {v9, v1}, Lcom/coremedia/iso/boxes/SampleToChunkBox;->setEntries(Ljava/util/List;)V

    .line 543
    const-wide/32 v2, -0x80000000

    .line 544
    const/4 v1, 0x0

    move v8, v1

    :goto_0
    array-length v1, v0

    if-lt v8, v1, :cond_0

    .line 554
    invoke-virtual {p3, v9}, Lcom/coremedia/iso/boxes/SampleTableBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 555
    return-void

    .line 549
    :cond_0
    aget v1, v0, v8

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 550
    invoke-virtual {v9}, Lcom/coremedia/iso/boxes/SampleToChunkBox;->getEntries()Ljava/util/List;

    move-result-object v10

    new-instance v1, Lcom/coremedia/iso/boxes/SampleToChunkBox$a;

    add-int/lit8 v2, v8, 0x1

    int-to-long v2, v2

    aget v4, v0, v8

    int-to-long v4, v4

    const-wide/16 v6, 0x1

    invoke-direct/range {v1 .. v7}, Lcom/coremedia/iso/boxes/SampleToChunkBox$a;-><init>(JJJ)V

    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 551
    aget v1, v0, v8

    int-to-long v2, v1

    .line 544
    :cond_1
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto :goto_0
.end method

.method protected a(Lcom/googlecode/mp4parser/authoring/tracks/a;Lcom/coremedia/iso/boxes/SampleTableBox;[I)V
    .locals 17

    .prologue
    .line 419
    new-instance v9, Lcom/mp4parser/iso14496/part12/SampleAuxiliaryInformationSizesBox;

    invoke-direct {v9}, Lcom/mp4parser/iso14496/part12/SampleAuxiliaryInformationSizesBox;-><init>()V

    .line 421
    const-string v2, "cenc"

    invoke-virtual {v9, v2}, Lcom/mp4parser/iso14496/part12/SampleAuxiliaryInformationSizesBox;->setAuxInfoType(Ljava/lang/String;)V

    .line 422
    const/4 v2, 0x1

    invoke-virtual {v9, v2}, Lcom/mp4parser/iso14496/part12/SampleAuxiliaryInformationSizesBox;->setFlags(I)V

    .line 423
    invoke-interface/range {p1 .. p1}, Lcom/googlecode/mp4parser/authoring/tracks/a;->m()Ljava/util/List;

    move-result-object v10

    .line 424
    invoke-interface/range {p1 .. p1}, Lcom/googlecode/mp4parser/authoring/tracks/a;->n()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 425
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v2

    new-array v4, v2, [S

    .line 426
    const/4 v2, 0x0

    move v3, v2

    :goto_0
    array-length v2, v4

    if-lt v3, v2, :cond_0

    .line 429
    invoke-virtual {v9, v4}, Lcom/mp4parser/iso14496/part12/SampleAuxiliaryInformationSizesBox;->setSampleInfoSizes([S)V

    .line 435
    :goto_1
    new-instance v11, Lcom/mp4parser/iso14496/part12/SampleAuxiliaryInformationOffsetsBox;

    invoke-direct {v11}, Lcom/mp4parser/iso14496/part12/SampleAuxiliaryInformationOffsetsBox;-><init>()V

    .line 436
    new-instance v12, Lcom/googlecode/mp4parser/boxes/dece/SampleEncryptionBox;

    invoke-direct {v12}, Lcom/googlecode/mp4parser/boxes/dece/SampleEncryptionBox;-><init>()V

    .line 437
    invoke-interface/range {p1 .. p1}, Lcom/googlecode/mp4parser/authoring/tracks/a;->n()Z

    move-result v2

    invoke-virtual {v12, v2}, Lcom/googlecode/mp4parser/boxes/dece/SampleEncryptionBox;->setSubSampleEncryption(Z)V

    .line 438
    invoke-virtual {v12, v10}, Lcom/googlecode/mp4parser/boxes/dece/SampleEncryptionBox;->setEntries(Ljava/util/List;)V

    .line 440
    invoke-virtual {v12}, Lcom/googlecode/mp4parser/boxes/dece/SampleEncryptionBox;->getOffsetToFirstIV()I

    move-result v2

    int-to-long v4, v2

    .line 441
    const/4 v3, 0x0

    .line 442
    move-object/from16 v0, p3

    array-length v2, v0

    new-array v13, v2, [J

    .line 445
    const/4 v2, 0x0

    move/from16 v16, v2

    move v2, v3

    move/from16 v3, v16

    :goto_2
    move-object/from16 v0, p3

    array-length v6, v0

    if-lt v3, v6, :cond_2

    .line 451
    invoke-virtual {v11, v13}, Lcom/mp4parser/iso14496/part12/SampleAuxiliaryInformationOffsetsBox;->setOffsets([J)V

    .line 453
    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Lcom/coremedia/iso/boxes/SampleTableBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 454
    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lcom/coremedia/iso/boxes/SampleTableBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 455
    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Lcom/coremedia/iso/boxes/SampleTableBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 456
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->b:Ljava/util/Set;

    invoke-interface {v2, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 459
    return-void

    .line 427
    :cond_0
    invoke-interface {v10, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mp4parser/iso23001/part7/a;

    invoke-virtual {v2}, Lcom/mp4parser/iso23001/part7/a;->a()I

    move-result v2

    int-to-short v2, v2

    aput-short v2, v4, v3

    .line 426
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 431
    :cond_1
    const/16 v2, 0x8

    invoke-virtual {v9, v2}, Lcom/mp4parser/iso14496/part12/SampleAuxiliaryInformationSizesBox;->setDefaultSampleInfoSize(I)V

    .line 432
    invoke-interface/range {p1 .. p1}, Lcom/googlecode/mp4parser/authoring/tracks/a;->h()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v9, v2}, Lcom/mp4parser/iso14496/part12/SampleAuxiliaryInformationSizesBox;->setSampleCount(I)V

    goto :goto_1

    .line 446
    :cond_2
    aput-wide v4, v13, v3

    .line 447
    const/4 v6, 0x0

    move/from16 v16, v6

    move-wide v6, v4

    move v4, v2

    move/from16 v5, v16

    :goto_3
    aget v2, p3, v3

    if-lt v5, v2, :cond_3

    .line 445
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v4

    move-wide v4, v6

    goto :goto_2

    .line 448
    :cond_3
    add-int/lit8 v8, v4, 0x1

    invoke-interface {v10, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mp4parser/iso23001/part7/a;

    invoke-virtual {v2}, Lcom/mp4parser/iso23001/part7/a;->a()I

    move-result v2

    int-to-long v14, v2

    add-long/2addr v6, v14

    .line 447
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v4, v8

    goto :goto_3
.end method

.method a(Lcom/googlecode/mp4parser/authoring/f;)[I
    .locals 10

    .prologue
    const-wide/16 v8, 0x1

    .line 609
    iget-object v0, p0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->g:Lcom/googlecode/mp4parser/authoring/builder/c;

    invoke-interface {v0, p1}, Lcom/googlecode/mp4parser/authoring/builder/c;->a(Lcom/googlecode/mp4parser/authoring/f;)[J

    move-result-object v1

    .line 610
    array-length v0, v1

    new-array v4, v0, [I

    .line 613
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-lt v0, v2, :cond_0

    .line 624
    sget-boolean v0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->e:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v4}, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->a([I)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "The number of samples and the sum of all chunk lengths must be equal"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 614
    :cond_0
    aget-wide v2, v1, v0

    sub-long v6, v2, v8

    .line 616
    array-length v2, v1

    add-int/lit8 v3, v0, 0x1

    if-ne v2, v3, :cond_1

    .line 617
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->h()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    int-to-long v2, v2

    .line 622
    :goto_1
    sub-long/2addr v2, v6

    invoke-static {v2, v3}, Lcom/googlecode/mp4parser/c/b;->a(J)I

    move-result v2

    aput v2, v4, v0

    .line 613
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 619
    :cond_1
    add-int/lit8 v2, v0, 0x1

    aget-wide v2, v1, v2

    sub-long/2addr v2, v8

    goto :goto_1

    .line 625
    :cond_2
    return-object v4
.end method

.method protected b(Lcom/googlecode/mp4parser/authoring/c;)Lcom/coremedia/iso/boxes/FileTypeBox;
    .locals 6

    .prologue
    .line 176
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 178
    const-string v1, "mp42"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 179
    const-string v1, "iso6"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 180
    const-string v1, "avc1"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 181
    const-string v1, "isom"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 182
    new-instance v1, Lcom/coremedia/iso/boxes/FileTypeBox;

    const-string v2, "iso6"

    const-wide/16 v4, 0x1

    invoke-direct {v1, v2, v4, v5, v0}, Lcom/coremedia/iso/boxes/FileTypeBox;-><init>(Ljava/lang/String;JLjava/util/List;)V

    return-object v1
.end method

.method protected b(Lcom/googlecode/mp4parser/authoring/f;Lcom/googlecode/mp4parser/authoring/c;Ljava/util/Map;)Lcom/coremedia/iso/boxes/a;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/googlecode/mp4parser/authoring/f;",
            "Lcom/googlecode/mp4parser/authoring/c;",
            "Ljava/util/Map",
            "<",
            "Lcom/googlecode/mp4parser/authoring/f;",
            "[I>;)",
            "Lcom/coremedia/iso/boxes/a;"
        }
    .end annotation

    .prologue
    .line 353
    new-instance v6, Lcom/coremedia/iso/boxes/SampleTableBox;

    invoke-direct {v6}, Lcom/coremedia/iso/boxes/SampleTableBox;-><init>()V

    .line 355
    invoke-virtual {p0, p1, v6}, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->b(Lcom/googlecode/mp4parser/authoring/f;Lcom/coremedia/iso/boxes/SampleTableBox;)V

    .line 356
    invoke-virtual {p0, p1, v6}, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->g(Lcom/googlecode/mp4parser/authoring/f;Lcom/coremedia/iso/boxes/SampleTableBox;)V

    .line 357
    invoke-virtual {p0, p1, v6}, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->f(Lcom/googlecode/mp4parser/authoring/f;Lcom/coremedia/iso/boxes/SampleTableBox;)V

    .line 358
    invoke-virtual {p0, p1, v6}, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->e(Lcom/googlecode/mp4parser/authoring/f;Lcom/coremedia/iso/boxes/SampleTableBox;)V

    .line 359
    invoke-virtual {p0, p1, v6}, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->d(Lcom/googlecode/mp4parser/authoring/f;Lcom/coremedia/iso/boxes/SampleTableBox;)V

    .line 360
    invoke-virtual {p0, p1, p3, v6}, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->a(Lcom/googlecode/mp4parser/authoring/f;Ljava/util/Map;Lcom/coremedia/iso/boxes/SampleTableBox;)V

    .line 361
    invoke-virtual {p0, p1, v6}, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->c(Lcom/googlecode/mp4parser/authoring/f;Lcom/coremedia/iso/boxes/SampleTableBox;)V

    .line 362
    invoke-virtual {p0, p1, p2, p3, v6}, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->a(Lcom/googlecode/mp4parser/authoring/f;Lcom/googlecode/mp4parser/authoring/c;Ljava/util/Map;Lcom/coremedia/iso/boxes/SampleTableBox;)V

    .line 365
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 366
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->g()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 375
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 403
    instance-of v0, p1, Lcom/googlecode/mp4parser/authoring/tracks/a;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 404
    check-cast v0, Lcom/googlecode/mp4parser/authoring/tracks/a;

    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    invoke-virtual {p0, v0, v6, v1}, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->a(Lcom/googlecode/mp4parser/authoring/tracks/a;Lcom/coremedia/iso/boxes/SampleTableBox;[I)V

    .line 406
    :cond_0
    invoke-virtual {p0, p1, v6}, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->a(Lcom/googlecode/mp4parser/authoring/f;Lcom/coremedia/iso/boxes/SampleTableBox;)V

    .line 407
    sget-object v0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->f:Lcom/googlecode/mp4parser/c/f;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "done with stbl for track_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->k()Lcom/googlecode/mp4parser/authoring/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/googlecode/mp4parser/authoring/g;->f()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/googlecode/mp4parser/c/f;->a(Ljava/lang/String;)V

    .line 408
    return-object v6

    .line 366
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 367
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/b;

    invoke-virtual {v1}, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/b;->a()Ljava/lang/String;

    move-result-object v4

    .line 368
    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 369
    if-nez v1, :cond_2

    .line 370
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 371
    invoke-interface {v2, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 373
    :cond_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/b;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 375
    :cond_3
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 376
    new-instance v8, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleGroupDescriptionBox;

    invoke-direct {v8}, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleGroupDescriptionBox;-><init>()V

    .line 377
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 378
    invoke-virtual {v8, v1}, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleGroupDescriptionBox;->setGroupingType(Ljava/lang/String;)V

    .line 379
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-virtual {v8, v2}, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleGroupDescriptionBox;->setGroupEntries(Ljava/util/List;)V

    .line 380
    new-instance v9, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleToGroupBox;

    invoke-direct {v9}, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleToGroupBox;-><init>()V

    .line 381
    invoke-virtual {v9, v1}, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleToGroupBox;->setGroupingType(Ljava/lang/String;)V

    .line 382
    const/4 v2, 0x0

    .line 383
    const/4 v1, 0x0

    move v3, v1

    :goto_2
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->h()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v3, v1, :cond_4

    .line 399
    invoke-virtual {v6, v8}, Lcom/coremedia/iso/boxes/SampleTableBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 400
    invoke-virtual {v6, v9}, Lcom/coremedia/iso/boxes/SampleTableBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    goto/16 :goto_1

    .line 384
    :cond_4
    const/4 v4, 0x0

    .line 385
    const/4 v1, 0x0

    move v5, v4

    move v4, v1

    :goto_3
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v4, v1, :cond_6

    .line 392
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleToGroupBox$a;->b()I

    move-result v1

    if-eq v1, v5, :cond_8

    .line 393
    :cond_5
    new-instance v1, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleToGroupBox$a;

    const-wide/16 v10, 0x1

    invoke-direct {v1, v10, v11, v5}, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleToGroupBox$a;-><init>(JI)V

    .line 394
    invoke-virtual {v9}, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleToGroupBox;->getEntries()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 383
    :goto_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v1

    goto :goto_2

    .line 386
    :cond_6
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/b;

    .line 387
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->g()Ljava/util/Map;

    move-result-object v10

    invoke-interface {v10, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [J

    .line 388
    int-to-long v10, v3

    invoke-static {v1, v10, v11}, Ljava/util/Arrays;->binarySearch([JJ)I

    move-result v1

    if-ltz v1, :cond_7

    .line 389
    add-int/lit8 v5, v4, 0x1

    .line 385
    :cond_7
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_3

    .line 396
    :cond_8
    invoke-virtual {v2}, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleToGroupBox$a;->a()J

    move-result-wide v4

    const-wide/16 v10, 0x1

    add-long/2addr v4, v10

    invoke-virtual {v2, v4, v5}, Lcom/googlecode/mp4parser/boxes/mp4/samplegrouping/SampleToGroupBox$a;->a(J)V

    move-object v1, v2

    goto :goto_4
.end method

.method protected b(Lcom/googlecode/mp4parser/authoring/f;Lcom/coremedia/iso/boxes/SampleTableBox;)V
    .locals 1

    .prologue
    .line 462
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->j()Lcom/coremedia/iso/boxes/SampleDescriptionBox;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/coremedia/iso/boxes/SampleTableBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 463
    return-void
.end method

.method protected c(Lcom/googlecode/mp4parser/authoring/c;)Lcom/coremedia/iso/boxes/a;
    .locals 1

    .prologue
    .line 245
    const/4 v0, 0x0

    return-object v0
.end method

.method protected c(Lcom/googlecode/mp4parser/authoring/f;Lcom/coremedia/iso/boxes/SampleTableBox;)V
    .locals 2

    .prologue
    .line 532
    new-instance v1, Lcom/coremedia/iso/boxes/SampleSizeBox;

    invoke-direct {v1}, Lcom/coremedia/iso/boxes/SampleSizeBox;-><init>()V

    .line 533
    iget-object v0, p0, Lcom/googlecode/mp4parser/authoring/builder/DefaultMp4Builder;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [J

    invoke-virtual {v1, v0}, Lcom/coremedia/iso/boxes/SampleSizeBox;->setSampleSizes([J)V

    .line 535
    invoke-virtual {p2, v1}, Lcom/coremedia/iso/boxes/SampleTableBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 536
    return-void
.end method

.method public d(Lcom/googlecode/mp4parser/authoring/c;)J
    .locals 5

    .prologue
    .line 632
    invoke-virtual {p1}, Lcom/googlecode/mp4parser/authoring/c;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/googlecode/mp4parser/authoring/f;

    invoke-interface {v0}, Lcom/googlecode/mp4parser/authoring/f;->k()Lcom/googlecode/mp4parser/authoring/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/googlecode/mp4parser/authoring/g;->b()J

    move-result-wide v0

    .line 633
    invoke-virtual {p1}, Lcom/googlecode/mp4parser/authoring/c;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 636
    return-wide v2

    .line 633
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/googlecode/mp4parser/authoring/f;

    .line 634
    invoke-interface {v0}, Lcom/googlecode/mp4parser/authoring/f;->k()Lcom/googlecode/mp4parser/authoring/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/googlecode/mp4parser/authoring/g;->b()J

    move-result-wide v0

    invoke-static {v2, v3, v0, v1}, Lcom/googlecode/mp4parser/c/g;->b(JJ)J

    move-result-wide v0

    move-wide v2, v0

    goto :goto_0
.end method

.method protected d(Lcom/googlecode/mp4parser/authoring/f;Lcom/coremedia/iso/boxes/SampleTableBox;)V
    .locals 2

    .prologue
    .line 558
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->c()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 559
    new-instance v0, Lcom/coremedia/iso/boxes/SampleDependencyTypeBox;

    invoke-direct {v0}, Lcom/coremedia/iso/boxes/SampleDependencyTypeBox;-><init>()V

    .line 560
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->c()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/coremedia/iso/boxes/SampleDependencyTypeBox;->setEntries(Ljava/util/List;)V

    .line 561
    invoke-virtual {p2, v0}, Lcom/coremedia/iso/boxes/SampleTableBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 563
    :cond_0
    return-void
.end method

.method protected e(Lcom/googlecode/mp4parser/authoring/f;Lcom/coremedia/iso/boxes/SampleTableBox;)V
    .locals 2

    .prologue
    .line 566
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->b()[J

    move-result-object v0

    .line 567
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 568
    new-instance v1, Lcom/coremedia/iso/boxes/SyncSampleBox;

    invoke-direct {v1}, Lcom/coremedia/iso/boxes/SyncSampleBox;-><init>()V

    .line 569
    invoke-virtual {v1, v0}, Lcom/coremedia/iso/boxes/SyncSampleBox;->setSampleNumber([J)V

    .line 570
    invoke-virtual {p2, v1}, Lcom/coremedia/iso/boxes/SampleTableBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 572
    :cond_0
    return-void
.end method

.method protected f(Lcom/googlecode/mp4parser/authoring/f;Lcom/coremedia/iso/boxes/SampleTableBox;)V
    .locals 2

    .prologue
    .line 575
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->a()Ljava/util/List;

    move-result-object v0

    .line 576
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 577
    new-instance v1, Lcom/coremedia/iso/boxes/CompositionTimeToSample;

    invoke-direct {v1}, Lcom/coremedia/iso/boxes/CompositionTimeToSample;-><init>()V

    .line 578
    invoke-virtual {v1, v0}, Lcom/coremedia/iso/boxes/CompositionTimeToSample;->setEntries(Ljava/util/List;)V

    .line 579
    invoke-virtual {p2, v1}, Lcom/coremedia/iso/boxes/SampleTableBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 581
    :cond_0
    return-void
.end method

.method protected g(Lcom/googlecode/mp4parser/authoring/f;Lcom/coremedia/iso/boxes/SampleTableBox;)V
    .locals 13

    .prologue
    const-wide/16 v10, 0x1

    .line 584
    const/4 v1, 0x0

    .line 585
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 587
    invoke-interface {p1}, Lcom/googlecode/mp4parser/authoring/f;->i()[J

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v12, v0

    move-object v0, v1

    move v1, v12

    :goto_0
    if-lt v1, v4, :cond_0

    .line 596
    new-instance v0, Lcom/coremedia/iso/boxes/TimeToSampleBox;

    invoke-direct {v0}, Lcom/coremedia/iso/boxes/TimeToSampleBox;-><init>()V

    .line 597
    invoke-virtual {v0, v2}, Lcom/coremedia/iso/boxes/TimeToSampleBox;->setEntries(Ljava/util/List;)V

    .line 598
    invoke-virtual {p2, v0}, Lcom/coremedia/iso/boxes/SampleTableBox;->addBox(Lcom/coremedia/iso/boxes/a;)V

    .line 599
    return-void

    .line 587
    :cond_0
    aget-wide v6, v3, v1

    .line 588
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/coremedia/iso/boxes/TimeToSampleBox$a;->b()J

    move-result-wide v8

    cmp-long v5, v8, v6

    if-nez v5, :cond_1

    .line 589
    invoke-virtual {v0}, Lcom/coremedia/iso/boxes/TimeToSampleBox$a;->a()J

    move-result-wide v6

    add-long/2addr v6, v10

    invoke-virtual {v0, v6, v7}, Lcom/coremedia/iso/boxes/TimeToSampleBox$a;->a(J)V

    .line 587
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 591
    :cond_1
    new-instance v0, Lcom/coremedia/iso/boxes/TimeToSampleBox$a;

    invoke-direct {v0, v10, v11, v6, v7}, Lcom/coremedia/iso/boxes/TimeToSampleBox$a;-><init>(JJ)V

    .line 592
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
