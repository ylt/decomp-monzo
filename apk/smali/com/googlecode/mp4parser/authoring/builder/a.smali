.class public Lcom/googlecode/mp4parser/authoring/builder/a;
.super Ljava/lang/Object;
.source "BetterFragmenter.java"

# interfaces
.implements Lcom/googlecode/mp4parser/authoring/builder/c;


# instance fields
.field private a:D


# direct methods
.method public constructor <init>(D)V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-wide p1, p0, Lcom/googlecode/mp4parser/authoring/builder/a;->a:D

    .line 16
    return-void
.end method


# virtual methods
.method public a(Lcom/googlecode/mp4parser/authoring/f;)[J
    .locals 18

    .prologue
    .line 19
    invoke-interface/range {p1 .. p1}, Lcom/googlecode/mp4parser/authoring/f;->k()Lcom/googlecode/mp4parser/authoring/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/googlecode/mp4parser/authoring/g;->b()J

    move-result-wide v6

    .line 20
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/googlecode/mp4parser/authoring/builder/a;->a:D

    long-to-double v4, v6

    mul-double/2addr v2, v4

    double-to-long v8, v2

    .line 21
    const/4 v2, 0x0

    new-array v3, v2, [J

    .line 22
    invoke-interface/range {p1 .. p1}, Lcom/googlecode/mp4parser/authoring/f;->b()[J

    move-result-object v10

    .line 23
    invoke-interface/range {p1 .. p1}, Lcom/googlecode/mp4parser/authoring/f;->i()[J

    move-result-object v11

    .line 24
    if-eqz v10, :cond_5

    .line 25
    array-length v2, v10

    new-array v6, v2, [J

    .line 26
    const-wide/16 v4, 0x0

    .line 27
    invoke-interface/range {p1 .. p1}, Lcom/googlecode/mp4parser/authoring/f;->e()J

    move-result-wide v12

    .line 29
    const/4 v2, 0x0

    :goto_0
    array-length v7, v11

    if-lt v2, v7, :cond_1

    .line 36
    const-wide/16 v4, 0x0

    .line 38
    const/4 v2, 0x0

    :goto_1
    array-length v7, v6

    add-int/lit8 v7, v7, -0x1

    if-lt v2, v7, :cond_3

    .line 48
    array-length v2, v6

    add-int/lit8 v2, v2, -0x1

    aget-wide v4, v6, v2

    sub-long v4, v12, v4

    const-wide/16 v12, 0x2

    div-long/2addr v8, v12

    cmp-long v2, v4, v8

    if-lez v2, :cond_0

    .line 49
    const/4 v2, 0x1

    new-array v2, v2, [J

    const/4 v4, 0x0

    array-length v5, v6

    add-int/lit8 v5, v5, -0x1

    aget-wide v6, v10, v5

    aput-wide v6, v2, v4

    invoke-static {v3, v2}, Lcom/googlecode/mp4parser/c/i;->a([J[J)[J

    move-result-object v3

    .line 72
    :cond_0
    :goto_2
    return-object v3

    .line 30
    :cond_1
    int-to-long v14, v2

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    invoke-static {v10, v14, v15}, Ljava/util/Arrays;->binarySearch([JJ)I

    move-result v7

    .line 31
    if-ltz v7, :cond_2

    .line 32
    aput-wide v4, v6, v7

    .line 34
    :cond_2
    aget-wide v14, v11, v2

    add-long/2addr v4, v14

    .line 29
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 39
    :cond_3
    aget-wide v14, v6, v2

    .line 40
    add-int/lit8 v7, v2, 0x1

    aget-wide v16, v6, v7

    .line 41
    cmp-long v7, v4, v16

    if-gtz v7, :cond_4

    .line 42
    sub-long/2addr v14, v4

    invoke-static {v14, v15}, Ljava/lang/Math;->abs(J)J

    move-result-wide v14

    sub-long v16, v16, v4

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->abs(J)J

    move-result-wide v16

    cmp-long v7, v14, v16

    if-gez v7, :cond_4

    .line 43
    const/4 v4, 0x1

    new-array v4, v4, [J

    const/4 v5, 0x0

    aget-wide v14, v10, v2

    aput-wide v14, v4, v5

    invoke-static {v3, v4}, Lcom/googlecode/mp4parser/c/i;->a([J[J)[J

    move-result-object v3

    .line 44
    aget-wide v4, v6, v2

    add-long/2addr v4, v8

    .line 38
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 53
    :cond_5
    const-wide/16 v4, 0x0

    .line 54
    const/4 v2, 0x1

    new-array v3, v2, [J

    const/4 v2, 0x0

    const-wide/16 v8, 0x1

    aput-wide v8, v3, v2

    .line 55
    const/4 v2, 0x1

    :goto_3
    array-length v8, v11

    if-lt v2, v8, :cond_6

    .line 66
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/googlecode/mp4parser/authoring/builder/a;->a:D

    cmpg-double v2, v4, v6

    if-gez v2, :cond_0

    array-length v2, v3

    const/4 v4, 0x1

    if-le v2, v4, :cond_0

    .line 67
    array-length v2, v11

    add-int/lit8 v2, v2, 0x1

    int-to-long v4, v2

    array-length v2, v3

    add-int/lit8 v2, v2, -0x2

    aget-wide v6, v3, v2

    sub-long/2addr v4, v6

    .line 68
    array-length v2, v3

    add-int/lit8 v2, v2, -0x1

    array-length v6, v3

    add-int/lit8 v6, v6, -0x2

    aget-wide v6, v3, v6

    const-wide/16 v8, 0x2

    div-long/2addr v4, v8

    add-long/2addr v4, v6

    aput-wide v4, v3, v2

    goto :goto_2

    .line 56
    :cond_6
    aget-wide v8, v11, v2

    long-to-double v8, v8

    long-to-double v12, v6

    div-double/2addr v8, v12

    add-double/2addr v4, v8

    .line 57
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/googlecode/mp4parser/authoring/builder/a;->a:D

    cmpl-double v8, v4, v8

    if-ltz v8, :cond_8

    .line 58
    if-lez v2, :cond_7

    .line 59
    const/4 v4, 0x1

    new-array v4, v4, [J

    const/4 v5, 0x0

    add-int/lit8 v8, v2, 0x1

    int-to-long v8, v8

    aput-wide v8, v4, v5

    invoke-static {v3, v4}, Lcom/googlecode/mp4parser/c/i;->a([J[J)[J

    move-result-object v3

    .line 62
    :cond_7
    const-wide/16 v4, 0x0

    .line 55
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method
