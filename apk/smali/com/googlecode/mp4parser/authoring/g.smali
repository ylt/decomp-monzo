.class public Lcom/googlecode/mp4parser/authoring/g;
.super Ljava/lang/Object;
.source "TrackMetaData.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field a:I

.field private b:Ljava/lang/String;

.field private c:J

.field private d:Ljava/util/Date;

.field private e:Ljava/util/Date;

.field private f:Lcom/googlecode/mp4parser/c/h;

.field private g:D

.field private h:D

.field private i:F

.field private j:J

.field private k:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const-string v0, "eng"

    iput-object v0, p0, Lcom/googlecode/mp4parser/authoring/g;->b:Ljava/lang/String;

    .line 28
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/googlecode/mp4parser/authoring/g;->d:Ljava/util/Date;

    .line 29
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/googlecode/mp4parser/authoring/g;->e:Ljava/util/Date;

    .line 30
    sget-object v0, Lcom/googlecode/mp4parser/c/h;->j:Lcom/googlecode/mp4parser/c/h;

    iput-object v0, p0, Lcom/googlecode/mp4parser/authoring/g;->f:Lcom/googlecode/mp4parser/c/h;

    .line 34
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/googlecode/mp4parser/authoring/g;->j:J

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lcom/googlecode/mp4parser/authoring/g;->k:I

    .line 25
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/googlecode/mp4parser/authoring/g;->b:Ljava/lang/String;

    return-object v0
.end method

.method public a(D)V
    .locals 1

    .prologue
    .line 82
    iput-wide p1, p0, Lcom/googlecode/mp4parser/authoring/g;->g:D

    .line 83
    return-void
.end method

.method public a(F)V
    .locals 0

    .prologue
    .line 114
    iput p1, p0, Lcom/googlecode/mp4parser/authoring/g;->i:F

    .line 115
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 106
    iput p1, p0, Lcom/googlecode/mp4parser/authoring/g;->a:I

    .line 107
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 58
    iput-wide p1, p0, Lcom/googlecode/mp4parser/authoring/g;->c:J

    .line 59
    return-void
.end method

.method public a(Lcom/googlecode/mp4parser/c/h;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/googlecode/mp4parser/authoring/g;->f:Lcom/googlecode/mp4parser/c/h;

    .line 131
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/googlecode/mp4parser/authoring/g;->b:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public a(Ljava/util/Date;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/googlecode/mp4parser/authoring/g;->d:Ljava/util/Date;

    .line 67
    return-void
.end method

.method public b()J
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/googlecode/mp4parser/authoring/g;->c:J

    return-wide v0
.end method

.method public b(D)V
    .locals 1

    .prologue
    .line 90
    iput-wide p1, p0, Lcom/googlecode/mp4parser/authoring/g;->h:D

    .line 91
    return-void
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 98
    iput-wide p1, p0, Lcom/googlecode/mp4parser/authoring/g;->j:J

    .line 99
    return-void
.end method

.method public b(Ljava/util/Date;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/googlecode/mp4parser/authoring/g;->e:Ljava/util/Date;

    .line 75
    return-void
.end method

.method public c()Ljava/util/Date;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/googlecode/mp4parser/authoring/g;->e:Ljava/util/Date;

    return-object v0
.end method

.method public clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 135
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 137
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()D
    .locals 2

    .prologue
    .line 78
    iget-wide v0, p0, Lcom/googlecode/mp4parser/authoring/g;->g:D

    return-wide v0
.end method

.method public e()D
    .locals 2

    .prologue
    .line 86
    iget-wide v0, p0, Lcom/googlecode/mp4parser/authoring/g;->h:D

    return-wide v0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 94
    iget-wide v0, p0, Lcom/googlecode/mp4parser/authoring/g;->j:J

    return-wide v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lcom/googlecode/mp4parser/authoring/g;->a:I

    return v0
.end method

.method public h()F
    .locals 1

    .prologue
    .line 110
    iget v0, p0, Lcom/googlecode/mp4parser/authoring/g;->i:F

    return v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lcom/googlecode/mp4parser/authoring/g;->k:I

    return v0
.end method

.method public j()Lcom/googlecode/mp4parser/c/h;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/googlecode/mp4parser/authoring/g;->f:Lcom/googlecode/mp4parser/c/h;

    return-object v0
.end method
