.class public Lcom/googlecode/mp4parser/authoring/a/a;
.super Ljava/util/AbstractList;
.source "DefaultMp4SampleList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/googlecode/mp4parser/authoring/a/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractList",
        "<",
        "Lcom/googlecode/mp4parser/authoring/e;",
        ">;"
    }
.end annotation


# static fields
.field private static final j:Lcom/googlecode/mp4parser/c/f;


# instance fields
.field a:Lcom/coremedia/iso/boxes/b;

.field b:Lcom/coremedia/iso/boxes/TrackBox;

.field c:[Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/ref/SoftReference",
            "<",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field d:[I

.field e:[J

.field f:[J

.field g:[[J

.field h:Lcom/coremedia/iso/boxes/SampleSizeBox;

.field i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/googlecode/mp4parser/authoring/a/a;

    invoke-static {v0}, Lcom/googlecode/mp4parser/c/f;->a(Ljava/lang/Class;)Lcom/googlecode/mp4parser/c/f;

    move-result-object v0

    sput-object v0, Lcom/googlecode/mp4parser/authoring/a/a;->j:Lcom/googlecode/mp4parser/c/f;

    return-void
.end method

.method public constructor <init>(JLcom/coremedia/iso/boxes/b;)V
    .locals 17

    .prologue
    .line 34
    invoke-direct/range {p0 .. p0}, Ljava/util/AbstractList;-><init>()V

    .line 24
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/googlecode/mp4parser/authoring/a/a;->b:Lcom/coremedia/iso/boxes/TrackBox;

    .line 25
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/googlecode/mp4parser/authoring/a/a;->c:[Ljava/lang/ref/SoftReference;

    .line 31
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/googlecode/mp4parser/authoring/a/a;->i:I

    .line 35
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/googlecode/mp4parser/authoring/a/a;->a:Lcom/coremedia/iso/boxes/b;

    .line 36
    const-class v2, Lcom/coremedia/iso/boxes/MovieBox;

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Lcom/coremedia/iso/boxes/b;->getBoxes(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/coremedia/iso/boxes/MovieBox;

    .line 37
    const-class v3, Lcom/coremedia/iso/boxes/TrackBox;

    invoke-virtual {v2, v3}, Lcom/coremedia/iso/boxes/MovieBox;->getBoxes(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v2

    .line 39
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 44
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/googlecode/mp4parser/authoring/a/a;->b:Lcom/coremedia/iso/boxes/TrackBox;

    if-nez v2, :cond_2

    .line 45
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "This MP4 does not contain track "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 39
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/coremedia/iso/boxes/TrackBox;

    .line 40
    invoke-virtual {v2}, Lcom/coremedia/iso/boxes/TrackBox;->getTrackHeaderBox()Lcom/coremedia/iso/boxes/TrackHeaderBox;

    move-result-object v4

    invoke-virtual {v4}, Lcom/coremedia/iso/boxes/TrackHeaderBox;->getTrackId()J

    move-result-wide v4

    cmp-long v4, v4, p1

    if-nez v4, :cond_0

    .line 41
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/googlecode/mp4parser/authoring/a/a;->b:Lcom/coremedia/iso/boxes/TrackBox;

    goto :goto_0

    .line 47
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/googlecode/mp4parser/authoring/a/a;->b:Lcom/coremedia/iso/boxes/TrackBox;

    invoke-virtual {v2}, Lcom/coremedia/iso/boxes/TrackBox;->getSampleTableBox()Lcom/coremedia/iso/boxes/SampleTableBox;

    move-result-object v2

    invoke-virtual {v2}, Lcom/coremedia/iso/boxes/SampleTableBox;->getChunkOffsetBox()Lcom/coremedia/iso/boxes/ChunkOffsetBox;

    move-result-object v2

    invoke-virtual {v2}, Lcom/coremedia/iso/boxes/ChunkOffsetBox;->getChunkOffsets()[J

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/googlecode/mp4parser/authoring/a/a;->e:[J

    .line 48
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/googlecode/mp4parser/authoring/a/a;->e:[J

    array-length v2, v2

    new-array v2, v2, [J

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/googlecode/mp4parser/authoring/a/a;->f:[J

    .line 50
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/googlecode/mp4parser/authoring/a/a;->e:[J

    array-length v2, v2

    new-array v2, v2, [Ljava/lang/ref/SoftReference;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/googlecode/mp4parser/authoring/a/a;->c:[Ljava/lang/ref/SoftReference;

    .line 51
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/googlecode/mp4parser/authoring/a/a;->c:[Ljava/lang/ref/SoftReference;

    new-instance v3, Ljava/lang/ref/SoftReference;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-static {v2, v3}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 53
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/googlecode/mp4parser/authoring/a/a;->e:[J

    array-length v2, v2

    new-array v2, v2, [[J

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/googlecode/mp4parser/authoring/a/a;->g:[[J

    .line 54
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/googlecode/mp4parser/authoring/a/a;->b:Lcom/coremedia/iso/boxes/TrackBox;

    invoke-virtual {v2}, Lcom/coremedia/iso/boxes/TrackBox;->getSampleTableBox()Lcom/coremedia/iso/boxes/SampleTableBox;

    move-result-object v2

    invoke-virtual {v2}, Lcom/coremedia/iso/boxes/SampleTableBox;->getSampleSizeBox()Lcom/coremedia/iso/boxes/SampleSizeBox;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/googlecode/mp4parser/authoring/a/a;->h:Lcom/coremedia/iso/boxes/SampleSizeBox;

    .line 55
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/googlecode/mp4parser/authoring/a/a;->b:Lcom/coremedia/iso/boxes/TrackBox;

    invoke-virtual {v2}, Lcom/coremedia/iso/boxes/TrackBox;->getSampleTableBox()Lcom/coremedia/iso/boxes/SampleTableBox;

    move-result-object v2

    invoke-virtual {v2}, Lcom/coremedia/iso/boxes/SampleTableBox;->getSampleToChunkBox()Lcom/coremedia/iso/boxes/SampleToChunkBox;

    move-result-object v2

    invoke-virtual {v2}, Lcom/coremedia/iso/boxes/SampleToChunkBox;->getEntries()Ljava/util/List;

    move-result-object v2

    .line 56
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Lcom/coremedia/iso/boxes/SampleToChunkBox$a;

    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/coremedia/iso/boxes/SampleToChunkBox$a;

    .line 59
    const/4 v3, 0x0

    .line 60
    const/4 v9, 0x1

    aget-object v3, v2, v3

    .line 61
    const/4 v8, 0x0

    .line 62
    const/4 v5, 0x0

    .line 64
    invoke-virtual {v3}, Lcom/coremedia/iso/boxes/SampleToChunkBox$a;->a()J

    move-result-wide v6

    .line 65
    invoke-virtual {v3}, Lcom/coremedia/iso/boxes/SampleToChunkBox$a;->b()J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/googlecode/mp4parser/c/b;->a(J)I

    move-result v4

    .line 67
    const/4 v3, 0x1

    .line 68
    invoke-virtual/range {p0 .. p0}, Lcom/googlecode/mp4parser/authoring/a/a;->size()I

    move-result v11

    move v14, v3

    move v3, v5

    move v5, v8

    move v8, v9

    move v9, v14

    .line 73
    :goto_1
    add-int/lit8 v10, v5, 0x1

    .line 74
    int-to-long v12, v10

    cmp-long v5, v12, v6

    if-nez v5, :cond_a

    .line 76
    array-length v3, v2

    if-le v3, v8, :cond_3

    .line 77
    add-int/lit8 v5, v8, 0x1

    aget-object v6, v2, v8

    .line 78
    invoke-virtual {v6}, Lcom/coremedia/iso/boxes/SampleToChunkBox$a;->b()J

    move-result-wide v12

    invoke-static {v12, v13}, Lcom/googlecode/mp4parser/c/b;->a(J)I

    move-result v3

    .line 79
    invoke-virtual {v6}, Lcom/coremedia/iso/boxes/SampleToChunkBox$a;->a()J

    move-result-wide v6

    move-wide v14, v6

    move v6, v4

    move v7, v5

    move-wide v4, v14

    .line 85
    :goto_2
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/googlecode/mp4parser/authoring/a/a;->g:[[J

    add-int/lit8 v12, v10, -0x1

    new-array v13, v6, [J

    aput-object v13, v8, v12

    .line 87
    add-int v8, v9, v6

    .line 71
    if-le v8, v11, :cond_9

    .line 88
    add-int/lit8 v3, v10, 0x1

    new-array v3, v3, [I

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/googlecode/mp4parser/authoring/a/a;->d:[I

    .line 90
    const/4 v3, 0x0

    .line 91
    const/4 v9, 0x1

    aget-object v3, v2, v3

    .line 92
    const/4 v8, 0x0

    .line 93
    const/4 v5, 0x0

    .line 95
    invoke-virtual {v3}, Lcom/coremedia/iso/boxes/SampleToChunkBox$a;->a()J

    move-result-wide v6

    .line 96
    invoke-virtual {v3}, Lcom/coremedia/iso/boxes/SampleToChunkBox$a;->b()J

    move-result-wide v12

    invoke-static {v12, v13}, Lcom/googlecode/mp4parser/c/b;->a(J)I

    move-result v4

    .line 98
    const/4 v3, 0x1

    move v10, v9

    .line 100
    :goto_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/googlecode/mp4parser/authoring/a/a;->d:[I

    add-int/lit8 v9, v8, 0x1

    aput v3, v12, v8

    .line 101
    int-to-long v12, v9

    cmp-long v8, v12, v6

    if-nez v8, :cond_8

    .line 103
    array-length v5, v2

    if-le v5, v10, :cond_4

    .line 104
    add-int/lit8 v8, v10, 0x1

    aget-object v6, v2, v10

    .line 105
    invoke-virtual {v6}, Lcom/coremedia/iso/boxes/SampleToChunkBox$a;->b()J

    move-result-wide v12

    invoke-static {v12, v13}, Lcom/googlecode/mp4parser/c/b;->a(J)I

    move-result v5

    .line 106
    invoke-virtual {v6}, Lcom/coremedia/iso/boxes/SampleToChunkBox$a;->a()J

    move-result-wide v6

    move v14, v5

    move v5, v4

    move v4, v14

    .line 113
    :goto_4
    add-int/2addr v3, v5

    .line 99
    if-le v3, v11, :cond_7

    .line 114
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/googlecode/mp4parser/authoring/a/a;->d:[I

    const v3, 0x7fffffff

    aput v3, v2, v9

    .line 116
    const/4 v3, 0x0

    .line 117
    const-wide/16 v4, 0x0

    .line 118
    const/4 v2, 0x1

    :goto_5
    int-to-long v6, v2

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/googlecode/mp4parser/authoring/a/a;->h:Lcom/coremedia/iso/boxes/SampleSizeBox;

    invoke-virtual {v8}, Lcom/coremedia/iso/boxes/SampleSizeBox;->getSampleCount()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-lez v6, :cond_6

    .line 131
    return-void

    .line 81
    :cond_3
    const/4 v3, -0x1

    .line 82
    const-wide v6, 0x7fffffffffffffffL

    move-wide v14, v6

    move v6, v4

    move v7, v8

    move-wide v4, v14

    goto :goto_2

    .line 108
    :cond_4
    const/4 v5, -0x1

    .line 109
    const-wide v6, 0x7fffffffffffffffL

    move v8, v10

    move v14, v4

    move v4, v5

    move v5, v14

    goto :goto_4

    .line 121
    :cond_5
    add-int/lit8 v3, v3, 0x1

    .line 122
    const-wide/16 v4, 0x0

    .line 119
    :cond_6
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/googlecode/mp4parser/authoring/a/a;->d:[I

    aget v6, v6, v3

    if-eq v2, v6, :cond_5

    .line 124
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/googlecode/mp4parser/authoring/a/a;->f:[J

    add-int/lit8 v7, v3, -0x1

    aget-wide v8, v6, v7

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/googlecode/mp4parser/authoring/a/a;->h:Lcom/coremedia/iso/boxes/SampleSizeBox;

    add-int/lit8 v11, v2, -0x1

    invoke-virtual {v10, v11}, Lcom/coremedia/iso/boxes/SampleSizeBox;->getSampleSizeAtIndex(I)J

    move-result-wide v10

    add-long/2addr v8, v10

    aput-wide v8, v6, v7

    .line 125
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/googlecode/mp4parser/authoring/a/a;->g:[[J

    add-int/lit8 v7, v3, -0x1

    aget-object v6, v6, v7

    .line 126
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/googlecode/mp4parser/authoring/a/a;->d:[I

    add-int/lit8 v8, v3, -0x1

    aget v7, v7, v8

    .line 127
    sub-int v7, v2, v7

    aput-wide v4, v6, v7

    .line 128
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/googlecode/mp4parser/authoring/a/a;->h:Lcom/coremedia/iso/boxes/SampleSizeBox;

    add-int/lit8 v7, v2, -0x1

    invoke-virtual {v6, v7}, Lcom/coremedia/iso/boxes/SampleSizeBox;->getSampleSizeAtIndex(I)J

    move-result-wide v6

    add-long/2addr v4, v6

    .line 118
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_7
    move v10, v8

    move v8, v9

    goto/16 :goto_3

    :cond_8
    move v8, v10

    goto :goto_4

    :cond_9
    move v9, v8

    move v8, v7

    move-wide v14, v4

    move v5, v10

    move v4, v3

    move v3, v6

    move-wide v6, v14

    goto/16 :goto_1

    :cond_a
    move v14, v4

    move-wide v4, v6

    move v6, v3

    move v7, v8

    move v3, v14

    goto/16 :goto_2
.end method

.method static synthetic a()Lcom/googlecode/mp4parser/c/f;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/googlecode/mp4parser/authoring/a/a;->j:Lcom/googlecode/mp4parser/c/f;

    return-object v0
.end method


# virtual methods
.method declared-synchronized a(I)I
    .locals 3

    .prologue
    .line 134
    monitor-enter p0

    add-int/lit8 v0, p1, 0x1

    .line 136
    :try_start_0
    iget-object v1, p0, Lcom/googlecode/mp4parser/authoring/a/a;->d:[I

    iget v2, p0, Lcom/googlecode/mp4parser/authoring/a/a;->i:I

    aget v1, v1, v2

    if-lt v0, v1, :cond_0

    iget-object v1, p0, Lcom/googlecode/mp4parser/authoring/a/a;->d:[I

    iget v2, p0, Lcom/googlecode/mp4parser/authoring/a/a;->i:I

    add-int/lit8 v2, v2, 0x1

    aget v1, v1, v2

    if-ge v0, v1, :cond_0

    .line 137
    iget v0, p0, Lcom/googlecode/mp4parser/authoring/a/a;->i:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154
    :goto_0
    monitor-exit p0

    return v0

    .line 138
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/googlecode/mp4parser/authoring/a/a;->d:[I

    iget v2, p0, Lcom/googlecode/mp4parser/authoring/a/a;->i:I

    aget v1, v1, v2

    if-ge v0, v1, :cond_2

    .line 141
    const/4 v1, 0x0

    iput v1, p0, Lcom/googlecode/mp4parser/authoring/a/a;->i:I

    .line 143
    :goto_1
    iget-object v1, p0, Lcom/googlecode/mp4parser/authoring/a/a;->d:[I

    iget v2, p0, Lcom/googlecode/mp4parser/authoring/a/a;->i:I

    add-int/lit8 v2, v2, 0x1

    aget v1, v1, v2

    if-le v1, v0, :cond_1

    .line 146
    iget v0, p0, Lcom/googlecode/mp4parser/authoring/a/a;->i:I

    goto :goto_0

    .line 144
    :cond_1
    iget v1, p0, Lcom/googlecode/mp4parser/authoring/a/a;->i:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/googlecode/mp4parser/authoring/a/a;->i:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 134
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 149
    :cond_2
    :try_start_2
    iget v1, p0, Lcom/googlecode/mp4parser/authoring/a/a;->i:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/googlecode/mp4parser/authoring/a/a;->i:I

    .line 151
    :goto_2
    iget-object v1, p0, Lcom/googlecode/mp4parser/authoring/a/a;->d:[I

    iget v2, p0, Lcom/googlecode/mp4parser/authoring/a/a;->i:I

    add-int/lit8 v2, v2, 0x1

    aget v1, v1, v2

    if-le v1, v0, :cond_3

    .line 154
    iget v0, p0, Lcom/googlecode/mp4parser/authoring/a/a;->i:I

    goto :goto_0

    .line 152
    :cond_3
    iget v1, p0, Lcom/googlecode/mp4parser/authoring/a/a;->i:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/googlecode/mp4parser/authoring/a/a;->i:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public b(I)Lcom/googlecode/mp4parser/authoring/e;
    .locals 4

    .prologue
    .line 161
    int-to-long v0, p1

    iget-object v2, p0, Lcom/googlecode/mp4parser/authoring/a/a;->h:Lcom/coremedia/iso/boxes/SampleSizeBox;

    invoke-virtual {v2}, Lcom/coremedia/iso/boxes/SampleSizeBox;->getSampleCount()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 162
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 164
    :cond_0
    new-instance v0, Lcom/googlecode/mp4parser/authoring/a/a$a;

    invoke-direct {v0, p0, p1}, Lcom/googlecode/mp4parser/authoring/a/a$a;-><init>(Lcom/googlecode/mp4parser/authoring/a/a;I)V

    return-object v0
.end method

.method public synthetic get(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/googlecode/mp4parser/authoring/a/a;->b(I)Lcom/googlecode/mp4parser/authoring/e;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 2

    .prologue
    .line 169
    iget-object v0, p0, Lcom/googlecode/mp4parser/authoring/a/a;->b:Lcom/coremedia/iso/boxes/TrackBox;

    invoke-virtual {v0}, Lcom/coremedia/iso/boxes/TrackBox;->getSampleTableBox()Lcom/coremedia/iso/boxes/SampleTableBox;

    move-result-object v0

    invoke-virtual {v0}, Lcom/coremedia/iso/boxes/SampleTableBox;->getSampleSizeBox()Lcom/coremedia/iso/boxes/SampleSizeBox;

    move-result-object v0

    invoke-virtual {v0}, Lcom/coremedia/iso/boxes/SampleSizeBox;->getSampleCount()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/googlecode/mp4parser/c/b;->a(J)I

    move-result v0

    return v0
.end method
