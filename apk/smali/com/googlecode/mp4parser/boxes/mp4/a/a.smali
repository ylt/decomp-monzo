.class public Lcom/googlecode/mp4parser/boxes/mp4/a/a;
.super Lcom/googlecode/mp4parser/boxes/mp4/a/b;
.source "AudioSpecificConfig.java"


# annotations
.annotation runtime Lcom/googlecode/mp4parser/boxes/mp4/a/g;
    a = {
        0x5
    }
    b = 0x40
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/googlecode/mp4parser/boxes/mp4/a/a$a;,
        Lcom/googlecode/mp4parser/boxes/mp4/a/a$b;
    }
.end annotation


# static fields
.field public static a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public A:I

.field public B:I

.field public C:Z

.field public D:Z

.field public E:Z

.field public F:I

.field public G:Z

.field public H:I

.field public I:I

.field public J:I

.field public K:I

.field public L:I

.field public M:I

.field public N:I

.field public O:I

.field public P:I

.field public Q:I

.field public R:I

.field public S:I

.field public T:I

.field public U:I

.field public V:Z

.field W:[B

.field X:Z

.field public c:Lcom/googlecode/mp4parser/boxes/mp4/a/a$a;

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:Z

.field public k:Z

.field public l:I

.field public m:I

.field public n:I

.field public o:I

.field public p:I

.field public q:I

.field public r:I

.field public s:I

.field public t:I

.field public u:I

.field public v:I

.field public w:I

.field public x:I

.field public y:I

.field public z:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 269
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->a:Ljava/util/Map;

    .line 270
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    .line 290
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->a:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x17700

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->a:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x15888

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 292
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->a:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0xfa00

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 293
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->a:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0xbb80

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->a:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0xac44

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->a:Ljava/util/Map;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x7d00

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->a:Ljava/util/Map;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x5dc0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->a:Ljava/util/Map;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x5622

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->a:Ljava/util/Map;

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x3e80

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->a:Ljava/util/Map;

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2ee0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->a:Ljava/util/Map;

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2b11

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->a:Ljava/util/Map;

    const/16 v1, 0xb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x1f40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 347
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "AAC main"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 348
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "AAC LC"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 349
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "AAC SSR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "AAC LTP"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 351
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "SBR"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 352
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "AAC Scalable"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 353
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "TwinVQ"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 354
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "CELP"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "HVXC"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 356
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(reserved)"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 357
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0xb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(reserved)"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 358
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "TTSI"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 359
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "Main synthetic"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 360
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0xe

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "Wavetable synthesis"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 361
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0xf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "General MIDI"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 362
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0x10

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "Algorithmic Synthesis and Audio FX"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 363
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0x11

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "ER AAC LC"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 364
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0x12

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(reserved)"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 365
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0x13

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "ER AAC LTP"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 366
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0x14

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "ER AAC Scalable"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 367
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0x15

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "ER TwinVQ"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 368
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0x16

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "ER BSAC"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 369
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0x17

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "ER AAC LD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 370
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0x18

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "ER CELP"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 371
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0x19

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "ER HVXC"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 372
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0x1a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "ER HILN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 373
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0x1b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "ER Parametric"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 374
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0x1c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "SSC"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0x1d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "PS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 376
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0x1e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "MPEG Surround"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0x1f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(escape)"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 378
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0x20

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "Layer-1"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 379
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0x21

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "Layer-2"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 380
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0x22

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "Layer-3"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 381
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0x23

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "DST"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 382
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0x24

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "ALS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 383
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0x25

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "SLS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 384
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0x26

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "SLS non-core"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 385
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0x27

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "ER AAC ELD"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 386
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0x28

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "SMR Simple"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 387
    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    const/16 v1, 0x29

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "SMR Main"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 467
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 520
    invoke-direct {p0}, Lcom/googlecode/mp4parser/boxes/mp4/a/b;-><init>()V

    .line 479
    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->l:I

    .line 486
    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->s:I

    .line 487
    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->t:I

    .line 488
    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->u:I

    .line 524
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->X:Z

    .line 521
    const/4 v0, 0x5

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->Y:I

    .line 522
    return-void
.end method

.method private a(Lcom/googlecode/mp4parser/boxes/mp4/a/c;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1007
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    .line 1008
    const/16 v1, 0x1f

    if-ne v0, v1, :cond_0

    .line 1009
    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x20

    .line 1011
    :cond_0
    return v0
.end method

.method private a(IIILcom/googlecode/mp4parser/boxes/mp4/a/c;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v2, 0x14

    const/4 v1, 0x1

    .line 1019
    invoke-virtual {p4, v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->v:I

    .line 1020
    invoke-virtual {p4, v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->w:I

    .line 1021
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->w:I

    if-ne v0, v1, :cond_0

    .line 1022
    const/16 v0, 0xe

    invoke-virtual {p4, v0}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->x:I

    .line 1024
    :cond_0
    invoke-virtual {p4, v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->y:I

    .line 1025
    if-nez p2, :cond_1

    .line 1026
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t parse program_config_element yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1029
    :cond_1
    const/4 v0, 0x6

    if-eq p3, v0, :cond_2

    if-ne p3, v2, :cond_3

    .line 1030
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p4, v0}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->z:I

    .line 1032
    :cond_3
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->y:I

    if-ne v0, v1, :cond_7

    .line 1033
    const/16 v0, 0x16

    if-ne p3, v0, :cond_4

    .line 1034
    const/4 v0, 0x5

    invoke-virtual {p4, v0}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->A:I

    .line 1035
    const/16 v0, 0xb

    invoke-virtual {p4, v0}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->B:I

    .line 1037
    :cond_4
    const/16 v0, 0x11

    if-eq p3, v0, :cond_5

    const/16 v0, 0x13

    if-eq p3, v0, :cond_5

    .line 1038
    if-eq p3, v2, :cond_5

    const/16 v0, 0x17

    if-ne p3, v0, :cond_6

    .line 1039
    :cond_5
    invoke-virtual {p4}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->C:Z

    .line 1040
    invoke-virtual {p4}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->D:Z

    .line 1041
    invoke-virtual {p4}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->E:Z

    .line 1043
    :cond_6
    invoke-virtual {p4, v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->F:I

    .line 1044
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->F:I

    if-ne v0, v1, :cond_7

    .line 1045
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "not yet implemented"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049
    :cond_7
    iput-boolean v1, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->G:Z

    .line 1050
    return-void
.end method

.method private a(ILcom/googlecode/mp4parser/boxes/mp4/a/d;)V
    .locals 2

    .prologue
    const/4 v1, 0x5

    .line 998
    const/16 v0, 0x20

    if-lt p1, v0, :cond_0

    .line 999
    const/16 v0, 0x1f

    invoke-virtual {p2, v0, v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(II)V

    .line 1000
    add-int/lit8 v0, p1, -0x20

    const/4 v1, 0x6

    invoke-virtual {p2, v0, v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(II)V

    .line 1004
    :goto_0
    return-void

    .line 1002
    :cond_0
    invoke-virtual {p2, p1, v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(II)V

    goto :goto_0
.end method

.method private a(Lcom/googlecode/mp4parser/boxes/mp4/a/d;)V
    .locals 4

    .prologue
    const/16 v3, 0x14

    const/4 v2, 0x1

    .line 1057
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->v:I

    invoke-virtual {p1, v0, v2}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(II)V

    .line 1058
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->w:I

    invoke-virtual {p1, v0, v2}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(II)V

    .line 1059
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->w:I

    if-ne v0, v2, :cond_0

    .line 1060
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->x:I

    const/16 v1, 0xe

    invoke-virtual {p1, v0, v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(II)V

    .line 1062
    :cond_0
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->y:I

    invoke-virtual {p1, v0, v2}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(II)V

    .line 1063
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->h:I

    if-nez v0, :cond_1

    .line 1064
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t parse program_config_element yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1067
    :cond_1
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    if-ne v0, v3, :cond_3

    .line 1068
    :cond_2
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->z:I

    const/4 v1, 0x3

    invoke-virtual {p1, v0, v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(II)V

    .line 1070
    :cond_3
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->y:I

    if-ne v0, v2, :cond_7

    .line 1071
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    const/16 v1, 0x16

    if-ne v0, v1, :cond_4

    .line 1072
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->A:I

    const/4 v1, 0x5

    invoke-virtual {p1, v0, v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(II)V

    .line 1073
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->B:I

    const/16 v1, 0xb

    invoke-virtual {p1, v0, v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(II)V

    .line 1075
    :cond_4
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    const/16 v1, 0x11

    if-eq v0, v1, :cond_5

    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    const/16 v1, 0x13

    if-eq v0, v1, :cond_5

    .line 1076
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    if-eq v0, v3, :cond_5

    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    const/16 v1, 0x17

    if-ne v0, v1, :cond_6

    .line 1077
    :cond_5
    iget-boolean v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->C:Z

    invoke-virtual {p1, v0}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(Z)V

    .line 1078
    iget-boolean v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->D:Z

    invoke-virtual {p1, v0}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(Z)V

    .line 1079
    iget-boolean v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->E:Z

    invoke-virtual {p1, v0}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(Z)V

    .line 1081
    :cond_6
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->F:I

    invoke-virtual {p1, v0, v2}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(II)V

    .line 1082
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->F:I

    if-ne v0, v2, :cond_7

    .line 1083
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "not yet implemented"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1086
    :cond_7
    return-void
.end method

.method private b(IIILcom/googlecode/mp4parser/boxes/mp4/a/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1099
    invoke-virtual {p4, v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->H:I

    .line 1100
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->H:I

    if-ne v0, v1, :cond_0

    .line 1101
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->c(IIILcom/googlecode/mp4parser/boxes/mp4/a/c;)V

    .line 1105
    :goto_0
    return-void

    .line 1103
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->f(IIILcom/googlecode/mp4parser/boxes/mp4/a/c;)V

    goto :goto_0
.end method

.method private c(IIILcom/googlecode/mp4parser/boxes/mp4/a/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1124
    const/4 v0, 0x2

    invoke-virtual {p4, v0}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->I:I

    .line 1126
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->I:I

    if-eq v0, v1, :cond_0

    .line 1127
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d(IIILcom/googlecode/mp4parser/boxes/mp4/a/c;)V

    .line 1129
    :cond_0
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->I:I

    if-eqz v0, :cond_1

    .line 1130
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->e(IIILcom/googlecode/mp4parser/boxes/mp4/a/c;)V

    .line 1133
    :cond_1
    invoke-virtual {p4, v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->J:I

    .line 1134
    iput-boolean v1, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->V:Z

    .line 1135
    return-void
.end method

.method private d(IIILcom/googlecode/mp4parser/boxes/mp4/a/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1149
    invoke-virtual {p4, v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->K:I

    .line 1150
    const/4 v0, 0x2

    invoke-virtual {p4, v0}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->L:I

    .line 1151
    invoke-virtual {p4, v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->M:I

    .line 1153
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->M:I

    if-ne v0, v1, :cond_0

    .line 1154
    invoke-virtual {p4, v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->N:I

    .line 1156
    :cond_0
    return-void
.end method

.method private e()I
    .locals 5

    .prologue
    const/16 v4, 0x14

    const/4 v3, 0x1

    .line 714
    .line 716
    const/4 v0, 0x2

    .line 717
    iget v1, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->w:I

    if-ne v1, v3, :cond_0

    .line 718
    const/16 v0, 0x10

    .line 720
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 721
    iget v1, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->h:I

    if-nez v1, :cond_1

    .line 722
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t parse program_config_element yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 725
    :cond_1
    iget v1, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    const/4 v2, 0x6

    if-eq v1, v2, :cond_2

    iget v1, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    if-ne v1, v4, :cond_3

    .line 726
    :cond_2
    add-int/lit8 v0, v0, 0x3

    .line 728
    :cond_3
    iget v1, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->y:I

    if-ne v1, v3, :cond_7

    .line 729
    iget v1, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    const/16 v2, 0x16

    if-ne v1, v2, :cond_4

    .line 730
    add-int/lit8 v0, v0, 0x5

    .line 731
    add-int/lit8 v0, v0, 0xb

    .line 733
    :cond_4
    iget v1, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    const/16 v2, 0x11

    if-eq v1, v2, :cond_5

    iget v1, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    const/16 v2, 0x13

    if-eq v1, v2, :cond_5

    .line 734
    iget v1, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    if-eq v1, v4, :cond_5

    iget v1, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    const/16 v2, 0x17

    if-ne v1, v2, :cond_6

    .line 735
    :cond_5
    add-int/lit8 v0, v0, 0x1

    .line 736
    add-int/lit8 v0, v0, 0x1

    .line 737
    add-int/lit8 v0, v0, 0x1

    .line 739
    :cond_6
    add-int/lit8 v0, v0, 0x1

    .line 740
    iget v1, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->F:I

    if-ne v1, v3, :cond_7

    .line 741
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 744
    :cond_7
    return v0
.end method

.method private e(IIILcom/googlecode/mp4parser/boxes/mp4/a/c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1169
    const/4 v0, 0x1

    invoke-virtual {p4, v0}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->O:I

    .line 1170
    const/16 v0, 0x8

    invoke-virtual {p4, v0}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->P:I

    .line 1171
    const/4 v0, 0x4

    invoke-virtual {p4, v0}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->Q:I

    .line 1172
    const/16 v0, 0xc

    invoke-virtual {p4, v0}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->R:I

    .line 1173
    const/4 v0, 0x2

    invoke-virtual {p4, v0}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->S:I

    .line 1174
    return-void
.end method

.method private f()Ljava/nio/ByteBuffer;
    .locals 9

    .prologue
    const/16 v8, 0x18

    const/16 v7, 0xf

    const/4 v6, 0x5

    const/4 v5, 0x1

    const/4 v4, 0x4

    .line 820
    invoke-virtual {p0}, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->a()I

    move-result v0

    new-array v0, v0, [B

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 821
    new-instance v1, Lcom/googlecode/mp4parser/boxes/mp4/a/d;

    invoke-direct {v1, v0}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;-><init>(Ljava/nio/ByteBuffer;)V

    .line 822
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->e:I

    invoke-direct {p0, v2, v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->a(ILcom/googlecode/mp4parser/boxes/mp4/a/d;)V

    .line 823
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->f:I

    invoke-virtual {v1, v2, v4}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(II)V

    .line 825
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->f:I

    if-ne v2, v7, :cond_0

    .line 826
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->g:I

    invoke-virtual {v1, v2, v8}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(II)V

    .line 829
    :cond_0
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->h:I

    invoke-virtual {v1, v2, v4}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(II)V

    .line 831
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    if-eq v2, v6, :cond_1

    .line 832
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    const/16 v3, 0x1d

    if-ne v2, v3, :cond_4

    .line 833
    :cond_1
    iput v6, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->i:I

    .line 834
    iput-boolean v5, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->j:Z

    .line 835
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    const/16 v3, 0x1d

    if-ne v2, v3, :cond_2

    .line 836
    iput-boolean v5, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->k:Z

    .line 838
    :cond_2
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->l:I

    invoke-virtual {v1, v2, v4}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(II)V

    .line 839
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->l:I

    if-ne v2, v7, :cond_3

    .line 840
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->m:I

    invoke-virtual {v1, v2, v8}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(II)V

    .line 841
    :cond_3
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    invoke-direct {p0, v2, v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->a(ILcom/googlecode/mp4parser/boxes/mp4/a/d;)V

    .line 842
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    const/16 v3, 0x16

    if-ne v2, v3, :cond_4

    .line 843
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->n:I

    invoke-virtual {v1, v2, v4}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(II)V

    .line 845
    :cond_4
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    packed-switch v2, :pswitch_data_0

    .line 936
    :goto_0
    :pswitch_0
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    packed-switch v2, :pswitch_data_1

    .line 962
    :cond_5
    :pswitch_1
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->u:I

    if-ltz v2, :cond_9

    .line 963
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->u:I

    const/16 v3, 0xb

    invoke-virtual {v1, v2, v3}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(II)V

    .line 964
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->u:I

    const/16 v3, 0x2b7

    if-ne v2, v3, :cond_9

    .line 965
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->i:I

    invoke-direct {p0, v2, v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->a(ILcom/googlecode/mp4parser/boxes/mp4/a/d;)V

    .line 966
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->i:I

    if-ne v2, v6, :cond_7

    .line 967
    iget-boolean v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->j:Z

    invoke-virtual {v1, v2}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(Z)V

    .line 968
    iget-boolean v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->j:Z

    if-eqz v2, :cond_7

    .line 969
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->l:I

    invoke-virtual {v1, v2, v4}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(II)V

    .line 970
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->l:I

    if-ne v2, v7, :cond_6

    .line 971
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->m:I

    invoke-virtual {v1, v2, v8}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(II)V

    .line 973
    :cond_6
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->t:I

    if-ltz v2, :cond_7

    .line 974
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->t:I

    const/16 v3, 0xb

    invoke-virtual {v1, v2, v3}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(II)V

    .line 975
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->s:I

    const/16 v3, 0x548

    if-ne v2, v3, :cond_7

    .line 976
    iget-boolean v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->k:Z

    invoke-virtual {v1, v2}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(Z)V

    .line 981
    :cond_7
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->i:I

    const/16 v3, 0x16

    if-ne v2, v3, :cond_9

    .line 982
    iget-boolean v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->j:Z

    invoke-virtual {v1, v2}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(Z)V

    .line 983
    iget-boolean v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->j:Z

    if-eqz v2, :cond_8

    .line 984
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->l:I

    invoke-virtual {v1, v2, v4}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(II)V

    .line 985
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->l:I

    if-ne v2, v7, :cond_8

    .line 986
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->m:I

    invoke-virtual {v1, v2, v8}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(II)V

    .line 989
    :cond_8
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->n:I

    invoke-virtual {v1, v2, v4}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(II)V

    .line 994
    :cond_9
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    return-object v0

    .line 858
    :pswitch_2
    invoke-direct {p0, v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->a(Lcom/googlecode/mp4parser/boxes/mp4/a/d;)V

    goto :goto_0

    .line 862
    :pswitch_3
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t write CelpSpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 866
    :pswitch_4
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t write HvxcSpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 870
    :pswitch_5
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t write TTSSpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 877
    :pswitch_6
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t write StructuredAudioSpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 881
    :pswitch_7
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t write ErrorResilientCelpSpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 885
    :pswitch_8
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t write ErrorResilientHvxcSpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 890
    :pswitch_9
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t write parseParametricSpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 895
    :pswitch_a
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t write SSCSpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 899
    :pswitch_b
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->o:I

    invoke-virtual {v1, v0, v5}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(II)V

    .line 900
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t write SpatialSpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 906
    :pswitch_c
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t write MPEG_1_2_SpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 910
    :pswitch_d
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t write DSTSpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 914
    :pswitch_e
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->p:I

    invoke-virtual {v1, v0, v6}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(II)V

    .line 915
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t write ALSSpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 920
    :pswitch_f
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t write SLSSpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 924
    :pswitch_10
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t write ELDSpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 929
    :pswitch_11
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t parse SymbolicMusicSpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 948
    :pswitch_12
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->q:I

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(II)V

    .line 949
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->q:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_a

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->q:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_b

    .line 950
    :cond_a
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t parse ErrorProtectionSpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 953
    :cond_b
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->q:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_5

    .line 954
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->r:I

    invoke-virtual {v1, v2, v5}, Lcom/googlecode/mp4parser/boxes/mp4/a/d;->a(II)V

    .line 955
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->r:I

    if-nez v2, :cond_5

    .line 957
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "not implemented"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 845
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_0
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_11
    .end packed-switch

    .line 936
    :pswitch_data_1
    .packed-switch 0x11
        :pswitch_12
        :pswitch_1
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_12
    .end packed-switch
.end method

.method private f(IIILcom/googlecode/mp4parser/boxes/mp4/a/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1186
    invoke-virtual {p4, v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->T:I

    .line 1187
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->T:I

    if-ne v0, v1, :cond_0

    .line 1188
    const/4 v0, 0x2

    invoke-virtual {p4, v0}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->U:I

    .line 1190
    :cond_0
    return-void
.end method


# virtual methods
.method a()I
    .locals 7

    .prologue
    const/16 v6, 0x1e

    const/16 v5, 0x16

    const/4 v1, 0x5

    const/16 v4, 0xf

    .line 748
    .line 749
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->e:I

    if-le v0, v6, :cond_a

    .line 750
    const/16 v0, 0xb

    .line 752
    :goto_0
    add-int/lit8 v0, v0, 0x4

    .line 753
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->f:I

    if-ne v2, v4, :cond_0

    .line 754
    add-int/lit8 v0, v0, 0x18

    .line 756
    :cond_0
    add-int/lit8 v0, v0, 0x4

    .line 757
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    if-eq v2, v1, :cond_1

    .line 758
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    const/16 v3, 0x1d

    if-ne v2, v3, :cond_2

    .line 759
    :cond_1
    add-int/lit8 v0, v0, 0x4

    .line 760
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->l:I

    if-ne v2, v4, :cond_2

    .line 762
    add-int/lit8 v0, v0, 0x18

    .line 766
    :cond_2
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    if-ne v2, v5, :cond_3

    .line 767
    add-int/lit8 v0, v0, 0x4

    .line 771
    :cond_3
    iget-boolean v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->G:Z

    if-eqz v2, :cond_4

    .line 772
    invoke-direct {p0}, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->e()I

    move-result v2

    add-int/2addr v0, v2

    .line 774
    :cond_4
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->u:I

    if-ltz v2, :cond_9

    .line 775
    add-int/lit8 v0, v0, 0xb

    .line 776
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->u:I

    const/16 v3, 0x2b7

    if-ne v2, v3, :cond_9

    .line 777
    add-int/lit8 v0, v0, 0x5

    .line 778
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->i:I

    if-le v2, v6, :cond_5

    .line 779
    add-int/lit8 v0, v0, 0x6

    .line 781
    :cond_5
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->i:I

    if-ne v2, v1, :cond_7

    .line 782
    add-int/lit8 v0, v0, 0x1

    .line 783
    iget-boolean v1, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->j:Z

    if-eqz v1, :cond_7

    .line 784
    add-int/lit8 v0, v0, 0x4

    .line 785
    iget v1, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->l:I

    if-ne v1, v4, :cond_6

    .line 786
    add-int/lit8 v0, v0, 0x18

    .line 788
    :cond_6
    iget v1, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->t:I

    if-ltz v1, :cond_7

    .line 789
    add-int/lit8 v0, v0, 0xb

    .line 790
    iget v1, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->t:I

    const/16 v2, 0x548

    if-ne v1, v2, :cond_7

    .line 791
    add-int/lit8 v0, v0, 0x1

    .line 796
    :cond_7
    iget v1, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->i:I

    if-ne v1, v5, :cond_9

    .line 797
    add-int/lit8 v0, v0, 0x1

    .line 798
    iget-boolean v1, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->j:Z

    if-eqz v1, :cond_8

    .line 799
    add-int/lit8 v0, v0, 0x4

    .line 800
    iget v1, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->l:I

    if-ne v1, v4, :cond_8

    .line 801
    add-int/lit8 v0, v0, 0x18

    .line 804
    :cond_8
    add-int/lit8 v0, v0, 0x4

    .line 808
    :cond_9
    int-to-double v0, v0

    const-wide/high16 v2, 0x4020000000000000L    # 8.0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0

    :cond_a
    move v0, v1

    goto :goto_0
.end method

.method public a(Ljava/nio/ByteBuffer;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v8, 0x18

    const/16 v7, 0xf

    const/4 v6, 0x5

    const/4 v5, 0x1

    const/4 v4, 0x4

    .line 528
    iput-boolean v5, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->X:Z

    .line 529
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->slice()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 530
    iget v1, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->Z:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 531
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->Z:I

    add-int/2addr v1, v2

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 534
    iget v1, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->Z:I

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->W:[B

    .line 535
    iget-object v1, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->W:[B

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 536
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 538
    new-instance v1, Lcom/googlecode/mp4parser/boxes/mp4/a/c;

    invoke-direct {v1, v0}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;-><init>(Ljava/nio/ByteBuffer;)V

    .line 539
    invoke-direct {p0, v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->a(Lcom/googlecode/mp4parser/boxes/mp4/a/c;)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->e:I

    .line 540
    invoke-virtual {v1, v4}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->f:I

    .line 542
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->f:I

    if-ne v0, v7, :cond_0

    .line 543
    invoke-virtual {v1, v8}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->g:I

    .line 546
    :cond_0
    invoke-virtual {v1, v4}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->h:I

    .line 548
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    if-eq v0, v6, :cond_1

    .line 549
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    const/16 v2, 0x1d

    if-ne v0, v2, :cond_a

    .line 550
    :cond_1
    iput v6, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->i:I

    .line 551
    iput-boolean v5, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->j:Z

    .line 552
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    const/16 v2, 0x1d

    if-ne v0, v2, :cond_2

    .line 553
    iput-boolean v5, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->k:Z

    .line 555
    :cond_2
    invoke-virtual {v1, v4}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->l:I

    .line 556
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->l:I

    if-ne v0, v7, :cond_3

    .line 557
    invoke-virtual {v1, v8}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->m:I

    .line 558
    :cond_3
    invoke-direct {p0, v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->a(Lcom/googlecode/mp4parser/boxes/mp4/a/c;)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    .line 559
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    const/16 v2, 0x16

    if-ne v0, v2, :cond_4

    .line 560
    invoke-virtual {v1, v4}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->n:I

    .line 565
    :cond_4
    :goto_0
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    packed-switch v0, :pswitch_data_0

    .line 654
    :goto_1
    :pswitch_0
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    packed-switch v0, :pswitch_data_1

    .line 680
    :cond_5
    :pswitch_1
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->i:I

    if-eq v0, v6, :cond_9

    invoke-virtual {v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->b()I

    move-result v0

    const/16 v2, 0x10

    if-lt v0, v2, :cond_9

    .line 681
    const/16 v0, 0xb

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->s:I

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->u:I

    .line 682
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->s:I

    const/16 v2, 0x2b7

    if-ne v0, v2, :cond_9

    .line 683
    invoke-direct {p0, v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->a(Lcom/googlecode/mp4parser/boxes/mp4/a/c;)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->i:I

    .line 684
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->i:I

    if-ne v0, v6, :cond_7

    .line 685
    invoke-virtual {v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->j:Z

    .line 686
    iget-boolean v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->j:Z

    if-eqz v0, :cond_7

    .line 687
    invoke-virtual {v1, v4}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->l:I

    .line 688
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->l:I

    if-ne v0, v7, :cond_6

    .line 689
    invoke-virtual {v1, v8}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->m:I

    .line 691
    :cond_6
    invoke-virtual {v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->b()I

    move-result v0

    const/16 v2, 0xc

    if-lt v0, v2, :cond_7

    .line 692
    const/16 v0, 0xb

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->s:I

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->t:I

    .line 693
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->s:I

    const/16 v2, 0x548

    if-ne v0, v2, :cond_7

    .line 694
    invoke-virtual {v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->k:Z

    .line 699
    :cond_7
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->i:I

    const/16 v2, 0x16

    if-ne v0, v2, :cond_9

    .line 700
    invoke-virtual {v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->j:Z

    .line 701
    iget-boolean v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->j:Z

    if-eqz v0, :cond_8

    .line 702
    invoke-virtual {v1, v4}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->l:I

    .line 703
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->l:I

    if-ne v0, v7, :cond_8

    .line 704
    invoke-virtual {v1, v8}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->m:I

    .line 707
    :cond_8
    invoke-virtual {v1, v4}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->n:I

    .line 711
    :cond_9
    return-void

    .line 562
    :cond_a
    const/4 v0, 0x0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->i:I

    goto/16 :goto_0

    .line 578
    :pswitch_2
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->f:I

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->h:I

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->a(IIILcom/googlecode/mp4parser/boxes/mp4/a/c;)V

    goto/16 :goto_1

    .line 582
    :pswitch_3
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t parse CelpSpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 586
    :pswitch_4
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t parse HvxcSpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 590
    :pswitch_5
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t parse TTSSpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 597
    :pswitch_6
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t parse StructuredAudioSpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 601
    :pswitch_7
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t parse ErrorResilientCelpSpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 605
    :pswitch_8
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t parse ErrorResilientHvxcSpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 610
    :pswitch_9
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->f:I

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->h:I

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b(IIILcom/googlecode/mp4parser/boxes/mp4/a/c;)V

    goto/16 :goto_1

    .line 614
    :pswitch_a
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t parse SSCSpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 618
    :pswitch_b
    invoke-virtual {v1, v5}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->o:I

    .line 619
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t parse SpatialSpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 625
    :pswitch_c
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t parse MPEG_1_2_SpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 629
    :pswitch_d
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t parse DSTSpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 633
    :pswitch_e
    invoke-virtual {v1, v6}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->p:I

    .line 634
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t parse ALSSpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 639
    :pswitch_f
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t parse SLSSpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 643
    :pswitch_10
    new-instance v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a$a;

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->h:I

    invoke-direct {v0, p0, v2, v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/a$a;-><init>(Lcom/googlecode/mp4parser/boxes/mp4/a/a;ILcom/googlecode/mp4parser/boxes/mp4/a/c;)V

    iput-object v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->c:Lcom/googlecode/mp4parser/boxes/mp4/a/a$a;

    goto/16 :goto_1

    .line 647
    :pswitch_11
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t parse SymbolicMusicSpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 666
    :pswitch_12
    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->q:I

    .line 667
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->q:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_b

    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->q:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_c

    .line 668
    :cond_b
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "can\'t parse ErrorProtectionSpecificConfig yet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 671
    :cond_c
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->q:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_5

    .line 672
    invoke-virtual {v1, v5}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    iput v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->r:I

    .line 673
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->r:I

    if-nez v0, :cond_5

    .line 675
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "not implemented"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 565
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_0
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_11
    .end packed-switch

    .line 654
    :pswitch_data_1
    .packed-switch 0x11
        :pswitch_12
        :pswitch_1
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_12
    .end packed-switch
.end method

.method public b()Ljava/nio/ByteBuffer;
    .locals 2

    .prologue
    .line 812
    invoke-virtual {p0}, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d()I

    move-result v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 813
    iget v1, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->Y:I

    invoke-static {v0, v1}, Lcom/coremedia/iso/g;->c(Ljava/nio/ByteBuffer;I)V

    .line 814
    invoke-virtual {p0}, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->a()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->a(Ljava/nio/ByteBuffer;I)V

    .line 815
    invoke-direct {p0}, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->f()Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 816
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1293
    if-ne p0, p1, :cond_1

    .line 1432
    :cond_0
    :goto_0
    return v0

    .line 1296
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1297
    goto :goto_0

    .line 1300
    :cond_3
    check-cast p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;

    .line 1302
    iget-boolean v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->D:Z

    iget-boolean v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->D:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1303
    goto :goto_0

    .line 1305
    :cond_4
    iget-boolean v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->C:Z

    iget-boolean v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->C:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 1306
    goto :goto_0

    .line 1308
    :cond_5
    iget-boolean v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->E:Z

    iget-boolean v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->E:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 1309
    goto :goto_0

    .line 1311
    :cond_6
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1312
    goto :goto_0

    .line 1314
    :cond_7
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->h:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->h:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 1315
    goto :goto_0

    .line 1317
    :cond_8
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->x:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->x:I

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 1318
    goto :goto_0

    .line 1320
    :cond_9
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->w:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->w:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1321
    goto :goto_0

    .line 1323
    :cond_a
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->r:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->r:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 1324
    goto :goto_0

    .line 1326
    :cond_b
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->q:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->q:I

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 1327
    goto :goto_0

    .line 1329
    :cond_c
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->M:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->M:I

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 1330
    goto :goto_0

    .line 1332
    :cond_d
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->i:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->i:I

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 1333
    goto :goto_0

    .line 1335
    :cond_e
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->n:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->n:I

    if-eq v2, v3, :cond_f

    move v0, v1

    .line 1336
    goto :goto_0

    .line 1338
    :cond_f
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->y:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->y:I

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 1339
    goto :goto_0

    .line 1341
    :cond_10
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->F:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->F:I

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 1342
    goto :goto_0

    .line 1344
    :cond_11
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->m:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->m:I

    if-eq v2, v3, :cond_12

    move v0, v1

    .line 1345
    goto/16 :goto_0

    .line 1347
    :cond_12
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->l:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->l:I

    if-eq v2, v3, :cond_13

    move v0, v1

    .line 1348
    goto/16 :goto_0

    .line 1350
    :cond_13
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->p:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->p:I

    if-eq v2, v3, :cond_14

    move v0, v1

    .line 1351
    goto/16 :goto_0

    .line 1353
    :cond_14
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->v:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->v:I

    if-eq v2, v3, :cond_15

    move v0, v1

    .line 1354
    goto/16 :goto_0

    .line 1356
    :cond_15
    iget-boolean v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->G:Z

    iget-boolean v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->G:Z

    if-eq v2, v3, :cond_16

    move v0, v1

    .line 1357
    goto/16 :goto_0

    .line 1359
    :cond_16
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->S:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->S:I

    if-eq v2, v3, :cond_17

    move v0, v1

    .line 1360
    goto/16 :goto_0

    .line 1362
    :cond_17
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->T:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->T:I

    if-eq v2, v3, :cond_18

    move v0, v1

    .line 1363
    goto/16 :goto_0

    .line 1365
    :cond_18
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->U:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->U:I

    if-eq v2, v3, :cond_19

    move v0, v1

    .line 1366
    goto/16 :goto_0

    .line 1368
    :cond_19
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->R:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->R:I

    if-eq v2, v3, :cond_1a

    move v0, v1

    .line 1369
    goto/16 :goto_0

    .line 1371
    :cond_1a
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->P:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->P:I

    if-eq v2, v3, :cond_1b

    move v0, v1

    .line 1372
    goto/16 :goto_0

    .line 1374
    :cond_1b
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->O:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->O:I

    if-eq v2, v3, :cond_1c

    move v0, v1

    .line 1375
    goto/16 :goto_0

    .line 1377
    :cond_1c
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->Q:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->Q:I

    if-eq v2, v3, :cond_1d

    move v0, v1

    .line 1378
    goto/16 :goto_0

    .line 1380
    :cond_1d
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->L:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->L:I

    if-eq v2, v3, :cond_1e

    move v0, v1

    .line 1381
    goto/16 :goto_0

    .line 1383
    :cond_1e
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->K:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->K:I

    if-eq v2, v3, :cond_1f

    move v0, v1

    .line 1384
    goto/16 :goto_0

    .line 1386
    :cond_1f
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->H:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->H:I

    if-eq v2, v3, :cond_20

    move v0, v1

    .line 1387
    goto/16 :goto_0

    .line 1389
    :cond_20
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->z:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->z:I

    if-eq v2, v3, :cond_21

    move v0, v1

    .line 1390
    goto/16 :goto_0

    .line 1392
    :cond_21
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->B:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->B:I

    if-eq v2, v3, :cond_22

    move v0, v1

    .line 1393
    goto/16 :goto_0

    .line 1395
    :cond_22
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->A:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->A:I

    if-eq v2, v3, :cond_23

    move v0, v1

    .line 1396
    goto/16 :goto_0

    .line 1398
    :cond_23
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->J:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->J:I

    if-eq v2, v3, :cond_24

    move v0, v1

    .line 1399
    goto/16 :goto_0

    .line 1401
    :cond_24
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->I:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->I:I

    if-eq v2, v3, :cond_25

    move v0, v1

    .line 1402
    goto/16 :goto_0

    .line 1404
    :cond_25
    iget-boolean v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->V:Z

    iget-boolean v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->V:Z

    if-eq v2, v3, :cond_26

    move v0, v1

    .line 1405
    goto/16 :goto_0

    .line 1407
    :cond_26
    iget-boolean v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->k:Z

    iget-boolean v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->k:Z

    if-eq v2, v3, :cond_27

    move v0, v1

    .line 1408
    goto/16 :goto_0

    .line 1410
    :cond_27
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->o:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->o:I

    if-eq v2, v3, :cond_28

    move v0, v1

    .line 1411
    goto/16 :goto_0

    .line 1413
    :cond_28
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->g:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->g:I

    if-eq v2, v3, :cond_29

    move v0, v1

    .line 1414
    goto/16 :goto_0

    .line 1416
    :cond_29
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->f:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->f:I

    if-eq v2, v3, :cond_2a

    move v0, v1

    .line 1417
    goto/16 :goto_0

    .line 1419
    :cond_2a
    iget-boolean v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->j:Z

    iget-boolean v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->j:Z

    if-eq v2, v3, :cond_2b

    move v0, v1

    .line 1420
    goto/16 :goto_0

    .line 1422
    :cond_2b
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->s:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->s:I

    if-eq v2, v3, :cond_2c

    move v0, v1

    .line 1423
    goto/16 :goto_0

    .line 1425
    :cond_2c
    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->N:I

    iget v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->N:I

    if-eq v2, v3, :cond_2d

    move v0, v1

    .line 1426
    goto/16 :goto_0

    .line 1428
    :cond_2d
    iget-object v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->W:[B

    iget-object v3, p1, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->W:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1429
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1437
    iget-object v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->W:[B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->W:[B

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    .line 1438
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    add-int/2addr v0, v3

    .line 1439
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->f:I

    add-int/2addr v0, v3

    .line 1440
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->g:I

    add-int/2addr v0, v3

    .line 1441
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->h:I

    add-int/2addr v0, v3

    .line 1442
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->i:I

    add-int/2addr v0, v3

    .line 1443
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->j:Z

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    add-int/2addr v0, v3

    .line 1444
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->k:Z

    if-eqz v0, :cond_2

    move v0, v2

    :goto_2
    add-int/2addr v0, v3

    .line 1445
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->l:I

    add-int/2addr v0, v3

    .line 1446
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->m:I

    add-int/2addr v0, v3

    .line 1447
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->n:I

    add-int/2addr v0, v3

    .line 1448
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->o:I

    add-int/2addr v0, v3

    .line 1449
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->p:I

    add-int/2addr v0, v3

    .line 1450
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->q:I

    add-int/2addr v0, v3

    .line 1451
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->r:I

    add-int/2addr v0, v3

    .line 1452
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->s:I

    add-int/2addr v0, v3

    .line 1453
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->v:I

    add-int/2addr v0, v3

    .line 1454
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->w:I

    add-int/2addr v0, v3

    .line 1455
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->x:I

    add-int/2addr v0, v3

    .line 1456
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->y:I

    add-int/2addr v0, v3

    .line 1457
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->z:I

    add-int/2addr v0, v3

    .line 1458
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->A:I

    add-int/2addr v0, v3

    .line 1459
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->B:I

    add-int/2addr v0, v3

    .line 1460
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->C:Z

    if-eqz v0, :cond_3

    move v0, v2

    :goto_3
    add-int/2addr v0, v3

    .line 1461
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->D:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_4
    add-int/2addr v0, v3

    .line 1462
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->E:Z

    if-eqz v0, :cond_5

    move v0, v2

    :goto_5
    add-int/2addr v0, v3

    .line 1463
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->F:I

    add-int/2addr v0, v3

    .line 1464
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->G:Z

    if-eqz v0, :cond_6

    move v0, v2

    :goto_6
    add-int/2addr v0, v3

    .line 1465
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->H:I

    add-int/2addr v0, v3

    .line 1466
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->I:I

    add-int/2addr v0, v3

    .line 1467
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->J:I

    add-int/2addr v0, v3

    .line 1468
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->K:I

    add-int/2addr v0, v3

    .line 1469
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->L:I

    add-int/2addr v0, v3

    .line 1470
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->M:I

    add-int/2addr v0, v3

    .line 1471
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->N:I

    add-int/2addr v0, v3

    .line 1472
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->O:I

    add-int/2addr v0, v3

    .line 1473
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->P:I

    add-int/2addr v0, v3

    .line 1474
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->Q:I

    add-int/2addr v0, v3

    .line 1475
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->R:I

    add-int/2addr v0, v3

    .line 1476
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->S:I

    add-int/2addr v0, v3

    .line 1477
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->T:I

    add-int/2addr v0, v3

    .line 1478
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->U:I

    add-int/2addr v0, v3

    .line 1479
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->V:Z

    if-eqz v3, :cond_7

    :goto_7
    add-int/2addr v0, v2

    .line 1480
    return v0

    :cond_0
    move v0, v1

    .line 1437
    goto/16 :goto_0

    :cond_1
    move v0, v1

    .line 1443
    goto/16 :goto_1

    :cond_2
    move v0, v1

    .line 1444
    goto/16 :goto_2

    :cond_3
    move v0, v1

    .line 1460
    goto :goto_3

    :cond_4
    move v0, v1

    .line 1461
    goto :goto_4

    :cond_5
    move v0, v1

    .line 1462
    goto :goto_5

    :cond_6
    move v0, v1

    .line 1464
    goto :goto_6

    :cond_7
    move v2, v1

    .line 1479
    goto :goto_7
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1218
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1219
    const-string v0, "AudioSpecificConfig"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1220
    const-string v0, "{configBytes="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->W:[B

    invoke-static {v2}, Lcom/coremedia/iso/c;->a([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1221
    const-string v0, ", audioObjectType="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1222
    const-string v0, ", samplingFrequencyIndex="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->f:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->a:Ljava/util/Map;

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->f:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1223
    const-string v0, ", samplingFrequency="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->g:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1224
    const-string v0, ", channelConfiguration="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->h:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1225
    iget v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->i:I

    if-lez v0, :cond_0

    .line 1226
    const-string v0, ", extensionAudioObjectType="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->i:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->b:Ljava/util/Map;

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->i:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1227
    const-string v0, ", sbrPresentFlag="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->j:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1228
    const-string v0, ", psPresentFlag="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->k:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1229
    const-string v0, ", extensionSamplingFrequencyIndex="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->l:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->a:Ljava/util/Map;

    iget v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->l:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1230
    const-string v0, ", extensionSamplingFrequency="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->m:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1231
    const-string v0, ", extensionChannelConfiguration="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->n:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1237
    :cond_0
    const-string v0, ", syncExtensionType="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->s:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1238
    iget-boolean v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->G:Z

    if-eqz v0, :cond_1

    .line 1239
    const-string v0, ", frameLengthFlag="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->v:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1240
    const-string v0, ", dependsOnCoreCoder="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->w:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1241
    const-string v0, ", coreCoderDelay="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->x:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1242
    const-string v0, ", extensionFlag="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->y:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1243
    const-string v0, ", layerNr="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->z:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1244
    const-string v0, ", numOfSubFrame="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->A:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1245
    const-string v0, ", layer_length="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->B:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1246
    const-string v0, ", aacSectionDataResilienceFlag="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->C:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1247
    const-string v0, ", aacScalefactorDataResilienceFlag="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->D:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1248
    const-string v0, ", aacSpectralDataResilienceFlag="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->E:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1249
    const-string v0, ", extensionFlag3="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->F:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1251
    :cond_1
    iget-boolean v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->V:Z

    if-eqz v0, :cond_2

    .line 1252
    const-string v0, ", isBaseLayer="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->H:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1253
    const-string v0, ", paraMode="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->I:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1254
    const-string v0, ", paraExtensionFlag="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->J:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1255
    const-string v0, ", hvxcVarMode="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->K:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1256
    const-string v0, ", hvxcRateMode="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->L:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1257
    const-string v0, ", erHvxcExtensionFlag="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->M:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1258
    const-string v0, ", var_ScalableFlag="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->N:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1259
    const-string v0, ", hilnQuantMode="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->O:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1260
    const-string v0, ", hilnMaxNumLine="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->P:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1261
    const-string v0, ", hilnSampleRateCode="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->Q:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1262
    const-string v0, ", hilnFrameLength="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->R:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1263
    const-string v0, ", hilnContMode="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->S:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1264
    const-string v0, ", hilnEnhaLayer="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->T:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1265
    const-string v0, ", hilnEnhaQuantMode="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a;->U:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1267
    :cond_2
    const/16 v0, 0x7d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1268
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
