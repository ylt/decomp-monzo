.class public Lcom/googlecode/mp4parser/boxes/mp4/a/a$a;
.super Ljava/lang/Object;
.source "AudioSpecificConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/googlecode/mp4parser/boxes/mp4/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Z

.field final synthetic h:Lcom/googlecode/mp4parser/boxes/mp4/a/a;


# direct methods
.method public constructor <init>(Lcom/googlecode/mp4parser/boxes/mp4/a/a;ILcom/googlecode/mp4parser/boxes/mp4/a/c;)V
    .locals 7

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v2, 0x0

    .line 1493
    iput-object p1, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a$a;->h:Lcom/googlecode/mp4parser/boxes/mp4/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1495
    invoke-virtual {p3}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a$a;->a:Z

    .line 1496
    invoke-virtual {p3}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a$a;->b:Z

    .line 1497
    invoke-virtual {p3}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a$a;->c:Z

    .line 1498
    invoke-virtual {p3}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a$a;->d:Z

    .line 1500
    invoke-virtual {p3}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a$a;->e:Z

    .line 1501
    iget-boolean v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a$a;->e:Z

    if-eqz v0, :cond_0

    .line 1502
    invoke-virtual {p3}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a$a;->f:Z

    .line 1503
    invoke-virtual {p3}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a$a;->g:Z

    .line 1504
    invoke-virtual {p0, p2, p3}, Lcom/googlecode/mp4parser/boxes/mp4/a/a$a;->a(ILcom/googlecode/mp4parser/boxes/mp4/a/c;)V

    .line 1507
    :cond_0
    invoke-virtual {p3, v4}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    if-nez v0, :cond_1

    .line 1529
    return-void

    .line 1508
    :cond_1
    invoke-virtual {p3, v4}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v1

    .line 1511
    const/16 v0, 0xf

    if-ne v1, v0, :cond_3

    .line 1512
    invoke-virtual {p3, v5}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v0

    .line 1513
    add-int/2addr v1, v0

    move v6, v0

    move v0, v1

    move v1, v6

    .line 1515
    :goto_0
    const/16 v3, 0xff

    if-ne v1, v3, :cond_2

    .line 1516
    const/16 v1, 0x10

    invoke-virtual {p3, v1}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    move-result v1

    .line 1517
    add-int/2addr v0, v1

    :cond_2
    move v1, v2

    .line 1522
    :goto_1
    if-ge v1, v0, :cond_0

    .line 1523
    invoke-virtual {p3, v5}, Lcom/googlecode/mp4parser/boxes/mp4/a/c;->a(I)I

    .line 1522
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    move v1, v2

    goto :goto_0
.end method


# virtual methods
.method public a(ILcom/googlecode/mp4parser/boxes/mp4/a/c;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1533
    packed-switch p1, :pswitch_data_0

    move v0, v1

    .line 1553
    :goto_0
    if-lt v1, v0, :cond_0

    .line 1556
    return-void

    .line 1536
    :pswitch_0
    const/4 v0, 0x1

    .line 1537
    goto :goto_0

    .line 1539
    :pswitch_1
    const/4 v0, 0x2

    .line 1540
    goto :goto_0

    .line 1544
    :pswitch_2
    const/4 v0, 0x3

    .line 1545
    goto :goto_0

    .line 1547
    :pswitch_3
    const/4 v0, 0x4

    .line 1548
    goto :goto_0

    .line 1554
    :cond_0
    new-instance v2, Lcom/googlecode/mp4parser/boxes/mp4/a/a$b;

    iget-object v3, p0, Lcom/googlecode/mp4parser/boxes/mp4/a/a$a;->h:Lcom/googlecode/mp4parser/boxes/mp4/a/a;

    invoke-direct {v2, v3, p2}, Lcom/googlecode/mp4parser/boxes/mp4/a/a$b;-><init>(Lcom/googlecode/mp4parser/boxes/mp4/a/a;Lcom/googlecode/mp4parser/boxes/mp4/a/c;)V

    .line 1553
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1533
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
