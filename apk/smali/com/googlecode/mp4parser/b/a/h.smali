.class public Lcom/googlecode/mp4parser/b/a/h;
.super Lcom/googlecode/mp4parser/b/a/b;
.source "SeqParameterSet.java"


# instance fields
.field public A:Z

.field public B:I

.field public C:I

.field public D:I

.field public E:Z

.field public F:Z

.field public G:Z

.field public H:I

.field public I:I

.field public J:I

.field public K:I

.field public L:[I

.field public M:Lcom/googlecode/mp4parser/b/a/i;

.field public N:Lcom/googlecode/mp4parser/b/a/g;

.field public O:I

.field public a:I

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:I

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Lcom/googlecode/mp4parser/b/a/c;

.field public j:I

.field public k:I

.field public l:I

.field public m:I

.field public n:I

.field public o:I

.field public p:Z

.field public q:I

.field public r:J

.field public s:Z

.field public t:Z

.field public u:Z

.field public v:Z

.field public w:Z

.field public x:Z

.field public y:I

.field public z:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/googlecode/mp4parser/b/a/b;-><init>()V

    return-void
.end method

.method public static a(Ljava/io/InputStream;)Lcom/googlecode/mp4parser/b/a/h;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x8

    .line 87
    new-instance v1, Lcom/googlecode/mp4parser/b/b/b;

    invoke-direct {v1, p0}, Lcom/googlecode/mp4parser/b/b/b;-><init>(Ljava/io/InputStream;)V

    .line 88
    new-instance v2, Lcom/googlecode/mp4parser/b/a/h;

    invoke-direct {v2}, Lcom/googlecode/mp4parser/b/a/h;-><init>()V

    .line 90
    const-string v0, "SPS: profile_idc"

    invoke-virtual {v1, v6, v0}, Lcom/googlecode/mp4parser/b/b/b;->a(ILjava/lang/String;)J

    move-result-wide v4

    long-to-int v0, v4

    iput v0, v2, Lcom/googlecode/mp4parser/b/a/h;->q:I

    .line 92
    const-string v0, "SPS: constraint_set_0_flag"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v0

    .line 91
    iput-boolean v0, v2, Lcom/googlecode/mp4parser/b/a/h;->s:Z

    .line 94
    const-string v0, "SPS: constraint_set_1_flag"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v0

    .line 93
    iput-boolean v0, v2, Lcom/googlecode/mp4parser/b/a/h;->t:Z

    .line 96
    const-string v0, "SPS: constraint_set_2_flag"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v0

    .line 95
    iput-boolean v0, v2, Lcom/googlecode/mp4parser/b/a/h;->u:Z

    .line 98
    const-string v0, "SPS: constraint_set_3_flag"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v0

    .line 97
    iput-boolean v0, v2, Lcom/googlecode/mp4parser/b/a/h;->v:Z

    .line 100
    const-string v0, "SPS: constraint_set_4_flag"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v0

    .line 99
    iput-boolean v0, v2, Lcom/googlecode/mp4parser/b/a/h;->w:Z

    .line 102
    const-string v0, "SPS: constraint_set_5_flag"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v0

    .line 101
    iput-boolean v0, v2, Lcom/googlecode/mp4parser/b/a/h;->x:Z

    .line 104
    const/4 v0, 0x2

    const-string v3, "SPS: reserved_zero_2bits"

    invoke-virtual {v1, v0, v3}, Lcom/googlecode/mp4parser/b/b/b;->a(ILjava/lang/String;)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/googlecode/mp4parser/b/a/h;->r:J

    .line 105
    const-string v0, "SPS: level_idc"

    invoke-virtual {v1, v6, v0}, Lcom/googlecode/mp4parser/b/b/b;->a(ILjava/lang/String;)J

    move-result-wide v4

    long-to-int v0, v4

    iput v0, v2, Lcom/googlecode/mp4parser/b/a/h;->y:I

    .line 106
    const-string v0, "SPS: seq_parameter_set_id"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, v2, Lcom/googlecode/mp4parser/b/a/h;->z:I

    .line 108
    iget v0, v2, Lcom/googlecode/mp4parser/b/a/h;->q:I

    const/16 v3, 0x64

    if-eq v0, v3, :cond_0

    iget v0, v2, Lcom/googlecode/mp4parser/b/a/h;->q:I

    const/16 v3, 0x6e

    if-eq v0, v3, :cond_0

    .line 109
    iget v0, v2, Lcom/googlecode/mp4parser/b/a/h;->q:I

    const/16 v3, 0x7a

    if-eq v0, v3, :cond_0

    iget v0, v2, Lcom/googlecode/mp4parser/b/a/h;->q:I

    const/16 v3, 0x90

    if-ne v0, v3, :cond_7

    .line 111
    :cond_0
    const-string v0, "SPS: chroma_format_idc"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->a(Ljava/lang/String;)I

    move-result v0

    .line 110
    invoke-static {v0}, Lcom/googlecode/mp4parser/b/a/c;->a(I)Lcom/googlecode/mp4parser/b/a/c;

    move-result-object v0

    iput-object v0, v2, Lcom/googlecode/mp4parser/b/a/h;->i:Lcom/googlecode/mp4parser/b/a/c;

    .line 112
    iget-object v0, v2, Lcom/googlecode/mp4parser/b/a/h;->i:Lcom/googlecode/mp4parser/b/a/c;

    sget-object v3, Lcom/googlecode/mp4parser/b/a/c;->d:Lcom/googlecode/mp4parser/b/a/c;

    if-ne v0, v3, :cond_1

    .line 114
    const-string v0, "SPS: residual_color_transform_flag"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v0

    .line 113
    iput-boolean v0, v2, Lcom/googlecode/mp4parser/b/a/h;->A:Z

    .line 117
    :cond_1
    const-string v0, "SPS: bit_depth_luma_minus8"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->a(Ljava/lang/String;)I

    move-result v0

    .line 116
    iput v0, v2, Lcom/googlecode/mp4parser/b/a/h;->n:I

    .line 119
    const-string v0, "SPS: bit_depth_chroma_minus8"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->a(Ljava/lang/String;)I

    move-result v0

    .line 118
    iput v0, v2, Lcom/googlecode/mp4parser/b/a/h;->o:I

    .line 121
    const-string v0, "SPS: qpprime_y_zero_transform_bypass_flag"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v0

    .line 120
    iput-boolean v0, v2, Lcom/googlecode/mp4parser/b/a/h;->p:Z

    .line 123
    const-string v0, "SPS: seq_scaling_matrix_present_lag"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v0

    .line 124
    if-eqz v0, :cond_2

    .line 125
    invoke-static {v1, v2}, Lcom/googlecode/mp4parser/b/a/h;->a(Lcom/googlecode/mp4parser/b/b/b;Lcom/googlecode/mp4parser/b/a/h;)V

    .line 131
    :cond_2
    :goto_0
    const-string v0, "SPS: log2_max_frame_num_minus4"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->a(Ljava/lang/String;)I

    move-result v0

    .line 130
    iput v0, v2, Lcom/googlecode/mp4parser/b/a/h;->j:I

    .line 132
    const-string v0, "SPS: pic_order_cnt_type"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, v2, Lcom/googlecode/mp4parser/b/a/h;->a:I

    .line 133
    iget v0, v2, Lcom/googlecode/mp4parser/b/a/h;->a:I

    if-nez v0, :cond_8

    .line 135
    const-string v0, "SPS: log2_max_pic_order_cnt_lsb_minus4"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->a(Ljava/lang/String;)I

    move-result v0

    .line 134
    iput v0, v2, Lcom/googlecode/mp4parser/b/a/h;->k:I

    .line 151
    :cond_3
    const-string v0, "SPS: num_ref_frames"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, v2, Lcom/googlecode/mp4parser/b/a/h;->D:I

    .line 153
    const-string v0, "SPS: gaps_in_frame_num_value_allowed_flag"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v0

    .line 152
    iput-boolean v0, v2, Lcom/googlecode/mp4parser/b/a/h;->E:Z

    .line 155
    const-string v0, "SPS: pic_width_in_mbs_minus1"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->a(Ljava/lang/String;)I

    move-result v0

    .line 154
    iput v0, v2, Lcom/googlecode/mp4parser/b/a/h;->m:I

    .line 157
    const-string v0, "SPS: pic_height_in_map_units_minus1"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->a(Ljava/lang/String;)I

    move-result v0

    .line 156
    iput v0, v2, Lcom/googlecode/mp4parser/b/a/h;->l:I

    .line 158
    const-string v0, "SPS: frame_mbs_only_flag"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, v2, Lcom/googlecode/mp4parser/b/a/h;->F:Z

    .line 159
    iget-boolean v0, v2, Lcom/googlecode/mp4parser/b/a/h;->F:Z

    if-nez v0, :cond_4

    .line 161
    const-string v0, "SPS: mb_adaptive_frame_field_flag"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v0

    .line 160
    iput-boolean v0, v2, Lcom/googlecode/mp4parser/b/a/h;->g:Z

    .line 164
    :cond_4
    const-string v0, "SPS: direct_8x8_inference_flag"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v0

    .line 163
    iput-boolean v0, v2, Lcom/googlecode/mp4parser/b/a/h;->h:Z

    .line 165
    const-string v0, "SPS: frame_cropping_flag"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, v2, Lcom/googlecode/mp4parser/b/a/h;->G:Z

    .line 166
    iget-boolean v0, v2, Lcom/googlecode/mp4parser/b/a/h;->G:Z

    if-eqz v0, :cond_5

    .line 168
    const-string v0, "SPS: frame_crop_left_offset"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->a(Ljava/lang/String;)I

    move-result v0

    .line 167
    iput v0, v2, Lcom/googlecode/mp4parser/b/a/h;->H:I

    .line 170
    const-string v0, "SPS: frame_crop_right_offset"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->a(Ljava/lang/String;)I

    move-result v0

    .line 169
    iput v0, v2, Lcom/googlecode/mp4parser/b/a/h;->I:I

    .line 172
    const-string v0, "SPS: frame_crop_top_offset"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->a(Ljava/lang/String;)I

    move-result v0

    .line 171
    iput v0, v2, Lcom/googlecode/mp4parser/b/a/h;->J:I

    .line 174
    const-string v0, "SPS: frame_crop_bottom_offset"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->a(Ljava/lang/String;)I

    move-result v0

    .line 173
    iput v0, v2, Lcom/googlecode/mp4parser/b/a/h;->K:I

    .line 177
    :cond_5
    const-string v0, "SPS: vui_parameters_present_flag"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v0

    .line 178
    if-eqz v0, :cond_6

    .line 179
    invoke-static {v1}, Lcom/googlecode/mp4parser/b/a/h;->a(Lcom/googlecode/mp4parser/b/b/b;)Lcom/googlecode/mp4parser/b/a/i;

    move-result-object v0

    iput-object v0, v2, Lcom/googlecode/mp4parser/b/a/h;->M:Lcom/googlecode/mp4parser/b/a/i;

    .line 181
    :cond_6
    invoke-virtual {v1}, Lcom/googlecode/mp4parser/b/b/b;->d()V

    .line 183
    return-object v2

    .line 128
    :cond_7
    sget-object v0, Lcom/googlecode/mp4parser/b/a/c;->b:Lcom/googlecode/mp4parser/b/a/c;

    iput-object v0, v2, Lcom/googlecode/mp4parser/b/a/h;->i:Lcom/googlecode/mp4parser/b/a/c;

    goto/16 :goto_0

    .line 136
    :cond_8
    iget v0, v2, Lcom/googlecode/mp4parser/b/a/h;->a:I

    const/4 v3, 0x1

    if-ne v0, v3, :cond_3

    .line 138
    const-string v0, "SPS: delta_pic_order_always_zero_flag"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v0

    .line 137
    iput-boolean v0, v2, Lcom/googlecode/mp4parser/b/a/h;->c:Z

    .line 140
    const-string v0, "SPS: offset_for_non_ref_pic"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->b(Ljava/lang/String;)I

    move-result v0

    .line 139
    iput v0, v2, Lcom/googlecode/mp4parser/b/a/h;->B:I

    .line 142
    const-string v0, "SPS: offset_for_top_to_bottom_field"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->b(Ljava/lang/String;)I

    move-result v0

    .line 141
    iput v0, v2, Lcom/googlecode/mp4parser/b/a/h;->C:I

    .line 144
    const-string v0, "SPS: num_ref_frames_in_pic_order_cnt_cycle"

    invoke-virtual {v1, v0}, Lcom/googlecode/mp4parser/b/b/b;->a(Ljava/lang/String;)I

    move-result v0

    .line 143
    iput v0, v2, Lcom/googlecode/mp4parser/b/a/h;->O:I

    .line 145
    iget v0, v2, Lcom/googlecode/mp4parser/b/a/h;->O:I

    new-array v0, v0, [I

    iput-object v0, v2, Lcom/googlecode/mp4parser/b/a/h;->L:[I

    .line 146
    const/4 v0, 0x0

    :goto_1
    iget v3, v2, Lcom/googlecode/mp4parser/b/a/h;->O:I

    if-ge v0, v3, :cond_3

    .line 147
    iget-object v3, v2, Lcom/googlecode/mp4parser/b/a/h;->L:[I

    .line 148
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SPS: offsetForRefFrame ["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/googlecode/mp4parser/b/b/b;->b(Ljava/lang/String;)I

    move-result v4

    .line 147
    aput v4, v3, v0

    .line 146
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private static a(Lcom/googlecode/mp4parser/b/b/b;)Lcom/googlecode/mp4parser/b/a/i;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x20

    const/16 v5, 0x10

    const/16 v4, 0x8

    .line 208
    new-instance v0, Lcom/googlecode/mp4parser/b/a/i;

    invoke-direct {v0}, Lcom/googlecode/mp4parser/b/a/i;-><init>()V

    .line 210
    const-string v1, "VUI: aspect_ratio_info_present_flag"

    invoke-virtual {p0, v1}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v1

    .line 209
    iput-boolean v1, v0, Lcom/googlecode/mp4parser/b/a/i;->a:Z

    .line 211
    iget-boolean v1, v0, Lcom/googlecode/mp4parser/b/a/i;->a:Z

    if-eqz v1, :cond_0

    .line 213
    const-string v1, "VUI: aspect_ratio"

    .line 212
    invoke-virtual {p0, v4, v1}, Lcom/googlecode/mp4parser/b/b/b;->a(ILjava/lang/String;)J

    move-result-wide v2

    long-to-int v1, v2

    invoke-static {v1}, Lcom/googlecode/mp4parser/b/a/a;->a(I)Lcom/googlecode/mp4parser/b/a/a;

    move-result-object v1

    iput-object v1, v0, Lcom/googlecode/mp4parser/b/a/i;->y:Lcom/googlecode/mp4parser/b/a/a;

    .line 214
    iget-object v1, v0, Lcom/googlecode/mp4parser/b/a/i;->y:Lcom/googlecode/mp4parser/b/a/a;

    sget-object v2, Lcom/googlecode/mp4parser/b/a/a;->a:Lcom/googlecode/mp4parser/b/a/a;

    if-ne v1, v2, :cond_0

    .line 215
    const-string v1, "VUI: sar_width"

    invoke-virtual {p0, v5, v1}, Lcom/googlecode/mp4parser/b/b/b;->a(ILjava/lang/String;)J

    move-result-wide v2

    long-to-int v1, v2

    iput v1, v0, Lcom/googlecode/mp4parser/b/a/i;->b:I

    .line 216
    const-string v1, "VUI: sar_height"

    invoke-virtual {p0, v5, v1}, Lcom/googlecode/mp4parser/b/b/b;->a(ILjava/lang/String;)J

    move-result-wide v2

    long-to-int v1, v2

    iput v1, v0, Lcom/googlecode/mp4parser/b/a/i;->c:I

    .line 220
    :cond_0
    const-string v1, "VUI: overscan_info_present_flag"

    invoke-virtual {p0, v1}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v1

    .line 219
    iput-boolean v1, v0, Lcom/googlecode/mp4parser/b/a/i;->d:Z

    .line 221
    iget-boolean v1, v0, Lcom/googlecode/mp4parser/b/a/i;->d:Z

    if-eqz v1, :cond_1

    .line 223
    const-string v1, "VUI: overscan_appropriate_flag"

    invoke-virtual {p0, v1}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v1

    .line 222
    iput-boolean v1, v0, Lcom/googlecode/mp4parser/b/a/i;->e:Z

    .line 226
    :cond_1
    const-string v1, "VUI: video_signal_type_present_flag"

    invoke-virtual {p0, v1}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v1

    .line 225
    iput-boolean v1, v0, Lcom/googlecode/mp4parser/b/a/i;->f:Z

    .line 227
    iget-boolean v1, v0, Lcom/googlecode/mp4parser/b/a/i;->f:Z

    if-eqz v1, :cond_2

    .line 228
    const/4 v1, 0x3

    const-string v2, "VUI: video_format"

    invoke-virtual {p0, v1, v2}, Lcom/googlecode/mp4parser/b/b/b;->a(ILjava/lang/String;)J

    move-result-wide v2

    long-to-int v1, v2

    iput v1, v0, Lcom/googlecode/mp4parser/b/a/i;->g:I

    .line 230
    const-string v1, "VUI: video_full_range_flag"

    invoke-virtual {p0, v1}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v1

    .line 229
    iput-boolean v1, v0, Lcom/googlecode/mp4parser/b/a/i;->h:Z

    .line 232
    const-string v1, "VUI: colour_description_present_flag"

    invoke-virtual {p0, v1}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v1

    .line 231
    iput-boolean v1, v0, Lcom/googlecode/mp4parser/b/a/i;->i:Z

    .line 233
    iget-boolean v1, v0, Lcom/googlecode/mp4parser/b/a/i;->i:Z

    if-eqz v1, :cond_2

    .line 235
    const-string v1, "VUI: colour_primaries"

    .line 234
    invoke-virtual {p0, v4, v1}, Lcom/googlecode/mp4parser/b/b/b;->a(ILjava/lang/String;)J

    move-result-wide v2

    long-to-int v1, v2

    iput v1, v0, Lcom/googlecode/mp4parser/b/a/i;->j:I

    .line 237
    const-string v1, "VUI: transfer_characteristics"

    .line 236
    invoke-virtual {p0, v4, v1}, Lcom/googlecode/mp4parser/b/b/b;->a(ILjava/lang/String;)J

    move-result-wide v2

    long-to-int v1, v2

    iput v1, v0, Lcom/googlecode/mp4parser/b/a/i;->k:I

    .line 239
    const-string v1, "VUI: matrix_coefficients"

    .line 238
    invoke-virtual {p0, v4, v1}, Lcom/googlecode/mp4parser/b/b/b;->a(ILjava/lang/String;)J

    move-result-wide v2

    long-to-int v1, v2

    iput v1, v0, Lcom/googlecode/mp4parser/b/a/i;->l:I

    .line 243
    :cond_2
    const-string v1, "VUI: chroma_loc_info_present_flag"

    invoke-virtual {p0, v1}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v1

    .line 242
    iput-boolean v1, v0, Lcom/googlecode/mp4parser/b/a/i;->m:Z

    .line 244
    iget-boolean v1, v0, Lcom/googlecode/mp4parser/b/a/i;->m:Z

    if-eqz v1, :cond_3

    .line 246
    const-string v1, "VUI chroma_sample_loc_type_top_field"

    invoke-virtual {p0, v1}, Lcom/googlecode/mp4parser/b/b/b;->a(Ljava/lang/String;)I

    move-result v1

    .line 245
    iput v1, v0, Lcom/googlecode/mp4parser/b/a/i;->n:I

    .line 248
    const-string v1, "VUI chroma_sample_loc_type_bottom_field"

    invoke-virtual {p0, v1}, Lcom/googlecode/mp4parser/b/b/b;->a(Ljava/lang/String;)I

    move-result v1

    .line 247
    iput v1, v0, Lcom/googlecode/mp4parser/b/a/i;->o:I

    .line 251
    :cond_3
    const-string v1, "VUI: timing_info_present_flag"

    invoke-virtual {p0, v1}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v1

    .line 250
    iput-boolean v1, v0, Lcom/googlecode/mp4parser/b/a/i;->p:Z

    .line 252
    iget-boolean v1, v0, Lcom/googlecode/mp4parser/b/a/i;->p:Z

    if-eqz v1, :cond_4

    .line 254
    const-string v1, "VUI: num_units_in_tick"

    .line 253
    invoke-virtual {p0, v6, v1}, Lcom/googlecode/mp4parser/b/b/b;->a(ILjava/lang/String;)J

    move-result-wide v2

    long-to-int v1, v2

    iput v1, v0, Lcom/googlecode/mp4parser/b/a/i;->q:I

    .line 255
    const-string v1, "VUI: time_scale"

    invoke-virtual {p0, v6, v1}, Lcom/googlecode/mp4parser/b/b/b;->a(ILjava/lang/String;)J

    move-result-wide v2

    long-to-int v1, v2

    iput v1, v0, Lcom/googlecode/mp4parser/b/a/i;->r:I

    .line 257
    const-string v1, "VUI: fixed_frame_rate_flag"

    invoke-virtual {p0, v1}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v1

    .line 256
    iput-boolean v1, v0, Lcom/googlecode/mp4parser/b/a/i;->s:Z

    .line 260
    :cond_4
    const-string v1, "VUI: nal_hrd_parameters_present_flag"

    invoke-virtual {p0, v1}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v1

    .line 261
    if-eqz v1, :cond_5

    .line 262
    invoke-static {p0}, Lcom/googlecode/mp4parser/b/a/h;->b(Lcom/googlecode/mp4parser/b/b/b;)Lcom/googlecode/mp4parser/b/a/d;

    move-result-object v2

    iput-object v2, v0, Lcom/googlecode/mp4parser/b/a/i;->v:Lcom/googlecode/mp4parser/b/a/d;

    .line 264
    :cond_5
    const-string v2, "VUI: vcl_hrd_parameters_present_flag"

    invoke-virtual {p0, v2}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v2

    .line 265
    if-eqz v2, :cond_6

    .line 266
    invoke-static {p0}, Lcom/googlecode/mp4parser/b/a/h;->b(Lcom/googlecode/mp4parser/b/b/b;)Lcom/googlecode/mp4parser/b/a/d;

    move-result-object v3

    iput-object v3, v0, Lcom/googlecode/mp4parser/b/a/i;->w:Lcom/googlecode/mp4parser/b/a/d;

    .line 267
    :cond_6
    if-nez v1, :cond_7

    if-eqz v2, :cond_8

    .line 269
    :cond_7
    const-string v1, "VUI: low_delay_hrd_flag"

    invoke-virtual {p0, v1}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v1

    .line 268
    iput-boolean v1, v0, Lcom/googlecode/mp4parser/b/a/i;->t:Z

    .line 272
    :cond_8
    const-string v1, "VUI: pic_struct_present_flag"

    invoke-virtual {p0, v1}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v1

    .line 271
    iput-boolean v1, v0, Lcom/googlecode/mp4parser/b/a/i;->u:Z

    .line 274
    const-string v1, "VUI: bitstream_restriction_flag"

    invoke-virtual {p0, v1}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v1

    .line 275
    if-eqz v1, :cond_9

    .line 276
    new-instance v1, Lcom/googlecode/mp4parser/b/a/i$a;

    invoke-direct {v1}, Lcom/googlecode/mp4parser/b/a/i$a;-><init>()V

    iput-object v1, v0, Lcom/googlecode/mp4parser/b/a/i;->x:Lcom/googlecode/mp4parser/b/a/i$a;

    .line 277
    iget-object v1, v0, Lcom/googlecode/mp4parser/b/a/i;->x:Lcom/googlecode/mp4parser/b/a/i$a;

    .line 278
    const-string v2, "VUI: motion_vectors_over_pic_boundaries_flag"

    invoke-virtual {p0, v2}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v2

    .line 277
    iput-boolean v2, v1, Lcom/googlecode/mp4parser/b/a/i$a;->a:Z

    .line 279
    iget-object v1, v0, Lcom/googlecode/mp4parser/b/a/i;->x:Lcom/googlecode/mp4parser/b/a/i$a;

    .line 280
    const-string v2, "VUI max_bytes_per_pic_denom"

    invoke-virtual {p0, v2}, Lcom/googlecode/mp4parser/b/b/b;->a(Ljava/lang/String;)I

    move-result v2

    .line 279
    iput v2, v1, Lcom/googlecode/mp4parser/b/a/i$a;->b:I

    .line 281
    iget-object v1, v0, Lcom/googlecode/mp4parser/b/a/i;->x:Lcom/googlecode/mp4parser/b/a/i$a;

    .line 282
    const-string v2, "VUI max_bits_per_mb_denom"

    invoke-virtual {p0, v2}, Lcom/googlecode/mp4parser/b/b/b;->a(Ljava/lang/String;)I

    move-result v2

    .line 281
    iput v2, v1, Lcom/googlecode/mp4parser/b/a/i$a;->c:I

    .line 283
    iget-object v1, v0, Lcom/googlecode/mp4parser/b/a/i;->x:Lcom/googlecode/mp4parser/b/a/i$a;

    .line 284
    const-string v2, "VUI log2_max_mv_length_horizontal"

    invoke-virtual {p0, v2}, Lcom/googlecode/mp4parser/b/b/b;->a(Ljava/lang/String;)I

    move-result v2

    .line 283
    iput v2, v1, Lcom/googlecode/mp4parser/b/a/i$a;->d:I

    .line 285
    iget-object v1, v0, Lcom/googlecode/mp4parser/b/a/i;->x:Lcom/googlecode/mp4parser/b/a/i$a;

    .line 286
    const-string v2, "VUI log2_max_mv_length_vertical"

    invoke-virtual {p0, v2}, Lcom/googlecode/mp4parser/b/b/b;->a(Ljava/lang/String;)I

    move-result v2

    .line 285
    iput v2, v1, Lcom/googlecode/mp4parser/b/a/i$a;->e:I

    .line 287
    iget-object v1, v0, Lcom/googlecode/mp4parser/b/a/i;->x:Lcom/googlecode/mp4parser/b/a/i$a;

    .line 288
    const-string v2, "VUI num_reorder_frames"

    invoke-virtual {p0, v2}, Lcom/googlecode/mp4parser/b/b/b;->a(Ljava/lang/String;)I

    move-result v2

    .line 287
    iput v2, v1, Lcom/googlecode/mp4parser/b/a/i$a;->f:I

    .line 289
    iget-object v1, v0, Lcom/googlecode/mp4parser/b/a/i;->x:Lcom/googlecode/mp4parser/b/a/i$a;

    .line 290
    const-string v2, "VUI max_dec_frame_buffering"

    invoke-virtual {p0, v2}, Lcom/googlecode/mp4parser/b/b/b;->a(Ljava/lang/String;)I

    move-result v2

    .line 289
    iput v2, v1, Lcom/googlecode/mp4parser/b/a/i$a;->g:I

    .line 293
    :cond_9
    return-object v0
.end method

.method private static a(Lcom/googlecode/mp4parser/b/b/b;Lcom/googlecode/mp4parser/b/a/h;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x8

    .line 188
    new-instance v0, Lcom/googlecode/mp4parser/b/a/g;

    invoke-direct {v0}, Lcom/googlecode/mp4parser/b/a/g;-><init>()V

    iput-object v0, p1, Lcom/googlecode/mp4parser/b/a/h;->N:Lcom/googlecode/mp4parser/b/a/g;

    .line 189
    const/4 v0, 0x0

    :goto_0
    if-lt v0, v4, :cond_0

    .line 204
    return-void

    .line 191
    :cond_0
    const-string v1, "SPS: seqScalingListPresentFlag"

    invoke-virtual {p0, v1}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v1

    .line 192
    if-eqz v1, :cond_1

    .line 193
    iget-object v1, p1, Lcom/googlecode/mp4parser/b/a/h;->N:Lcom/googlecode/mp4parser/b/a/g;

    new-array v2, v4, [Lcom/googlecode/mp4parser/b/a/f;

    iput-object v2, v1, Lcom/googlecode/mp4parser/b/a/g;->a:[Lcom/googlecode/mp4parser/b/a/f;

    .line 194
    iget-object v1, p1, Lcom/googlecode/mp4parser/b/a/h;->N:Lcom/googlecode/mp4parser/b/a/g;

    new-array v2, v4, [Lcom/googlecode/mp4parser/b/a/f;

    iput-object v2, v1, Lcom/googlecode/mp4parser/b/a/g;->b:[Lcom/googlecode/mp4parser/b/a/f;

    .line 195
    const/4 v1, 0x6

    if-ge v0, v1, :cond_2

    .line 196
    iget-object v1, p1, Lcom/googlecode/mp4parser/b/a/h;->N:Lcom/googlecode/mp4parser/b/a/g;

    iget-object v1, v1, Lcom/googlecode/mp4parser/b/a/g;->a:[Lcom/googlecode/mp4parser/b/a/f;

    .line 197
    const/16 v2, 0x10

    .line 196
    invoke-static {p0, v2}, Lcom/googlecode/mp4parser/b/a/f;->a(Lcom/googlecode/mp4parser/b/b/b;I)Lcom/googlecode/mp4parser/b/a/f;

    move-result-object v2

    aput-object v2, v1, v0

    .line 189
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 199
    :cond_2
    iget-object v1, p1, Lcom/googlecode/mp4parser/b/a/h;->N:Lcom/googlecode/mp4parser/b/a/g;

    iget-object v1, v1, Lcom/googlecode/mp4parser/b/a/g;->b:[Lcom/googlecode/mp4parser/b/a/f;

    add-int/lit8 v2, v0, -0x6

    .line 200
    const/16 v3, 0x40

    .line 199
    invoke-static {p0, v3}, Lcom/googlecode/mp4parser/b/a/f;->a(Lcom/googlecode/mp4parser/b/b/b;I)Lcom/googlecode/mp4parser/b/a/f;

    move-result-object v3

    aput-object v3, v1, v2

    goto :goto_1
.end method

.method private static b(Lcom/googlecode/mp4parser/b/b/b;)Lcom/googlecode/mp4parser/b/a/d;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x5

    .line 298
    new-instance v1, Lcom/googlecode/mp4parser/b/a/d;

    invoke-direct {v1}, Lcom/googlecode/mp4parser/b/a/d;-><init>()V

    .line 299
    const-string v0, "SPS: cpb_cnt_minus1"

    invoke-virtual {p0, v0}, Lcom/googlecode/mp4parser/b/b/b;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, v1, Lcom/googlecode/mp4parser/b/a/d;->a:I

    .line 300
    const-string v0, "HRD: bit_rate_scale"

    invoke-virtual {p0, v5, v0}, Lcom/googlecode/mp4parser/b/b/b;->a(ILjava/lang/String;)J

    move-result-wide v2

    long-to-int v0, v2

    iput v0, v1, Lcom/googlecode/mp4parser/b/a/d;->b:I

    .line 301
    const-string v0, "HRD: cpb_size_scale"

    invoke-virtual {p0, v5, v0}, Lcom/googlecode/mp4parser/b/b/b;->a(ILjava/lang/String;)J

    move-result-wide v2

    long-to-int v0, v2

    iput v0, v1, Lcom/googlecode/mp4parser/b/a/d;->c:I

    .line 302
    iget v0, v1, Lcom/googlecode/mp4parser/b/a/d;->a:I

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [I

    iput-object v0, v1, Lcom/googlecode/mp4parser/b/a/d;->d:[I

    .line 303
    iget v0, v1, Lcom/googlecode/mp4parser/b/a/d;->a:I

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [I

    iput-object v0, v1, Lcom/googlecode/mp4parser/b/a/d;->e:[I

    .line 304
    iget v0, v1, Lcom/googlecode/mp4parser/b/a/d;->a:I

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Z

    iput-object v0, v1, Lcom/googlecode/mp4parser/b/a/d;->f:[Z

    .line 306
    const/4 v0, 0x0

    :goto_0
    iget v2, v1, Lcom/googlecode/mp4parser/b/a/d;->a:I

    if-le v0, v2, :cond_0

    .line 314
    const-string v0, "HRD: initial_cpb_removal_delay_length_minus1"

    .line 313
    invoke-virtual {p0, v4, v0}, Lcom/googlecode/mp4parser/b/b/b;->a(ILjava/lang/String;)J

    move-result-wide v2

    long-to-int v0, v2

    iput v0, v1, Lcom/googlecode/mp4parser/b/a/d;->g:I

    .line 316
    const-string v0, "HRD: cpb_removal_delay_length_minus1"

    .line 315
    invoke-virtual {p0, v4, v0}, Lcom/googlecode/mp4parser/b/b/b;->a(ILjava/lang/String;)J

    move-result-wide v2

    long-to-int v0, v2

    iput v0, v1, Lcom/googlecode/mp4parser/b/a/d;->h:I

    .line 318
    const-string v0, "HRD: dpb_output_delay_length_minus1"

    .line 317
    invoke-virtual {p0, v4, v0}, Lcom/googlecode/mp4parser/b/b/b;->a(ILjava/lang/String;)J

    move-result-wide v2

    long-to-int v0, v2

    iput v0, v1, Lcom/googlecode/mp4parser/b/a/d;->i:I

    .line 320
    const-string v0, "HRD: time_offset_length"

    .line 319
    invoke-virtual {p0, v4, v0}, Lcom/googlecode/mp4parser/b/b/b;->a(ILjava/lang/String;)J

    move-result-wide v2

    long-to-int v0, v2

    iput v0, v1, Lcom/googlecode/mp4parser/b/a/d;->j:I

    .line 321
    return-object v1

    .line 307
    :cond_0
    iget-object v2, v1, Lcom/googlecode/mp4parser/b/a/d;->d:[I

    .line 308
    const-string v3, "HRD: bit_rate_value_minus1"

    invoke-virtual {p0, v3}, Lcom/googlecode/mp4parser/b/b/b;->a(Ljava/lang/String;)I

    move-result v3

    .line 307
    aput v3, v2, v0

    .line 309
    iget-object v2, v1, Lcom/googlecode/mp4parser/b/a/d;->e:[I

    .line 310
    const-string v3, "HRD: cpb_size_value_minus1"

    invoke-virtual {p0, v3}, Lcom/googlecode/mp4parser/b/b/b;->a(Ljava/lang/String;)I

    move-result v3

    .line 309
    aput v3, v2, v0

    .line 311
    iget-object v2, v1, Lcom/googlecode/mp4parser/b/a/d;->f:[Z

    const-string v3, "HRD: cbr_flag"

    invoke-virtual {p0, v3}, Lcom/googlecode/mp4parser/b/b/b;->c(Ljava/lang/String;)Z

    move-result v3

    aput-boolean v3, v2, v0

    .line 306
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 529
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SeqParameterSet{ \n        pic_order_cnt_type="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 530
    iget v1, p0, Lcom/googlecode/mp4parser/b/a/h;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 531
    const-string v1, ", \n        field_pic_flag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/googlecode/mp4parser/b/a/h;->b:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 532
    const-string v1, ", \n        delta_pic_order_always_zero_flag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/googlecode/mp4parser/b/a/h;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 533
    const-string v1, ", \n        weighted_pred_flag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/googlecode/mp4parser/b/a/h;->d:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 534
    const-string v1, ", \n        weighted_bipred_idc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/googlecode/mp4parser/b/a/h;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 535
    const-string v1, ", \n        entropy_coding_mode_flag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/googlecode/mp4parser/b/a/h;->f:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 536
    const-string v1, ", \n        mb_adaptive_frame_field_flag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/googlecode/mp4parser/b/a/h;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 537
    const-string v1, ", \n        direct_8x8_inference_flag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/googlecode/mp4parser/b/a/h;->h:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 538
    const-string v1, ", \n        chroma_format_idc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/googlecode/mp4parser/b/a/h;->i:Lcom/googlecode/mp4parser/b/a/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 539
    const-string v1, ", \n        log2_max_frame_num_minus4="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/googlecode/mp4parser/b/a/h;->j:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 540
    const-string v1, ", \n        log2_max_pic_order_cnt_lsb_minus4="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/googlecode/mp4parser/b/a/h;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 541
    const-string v1, ", \n        pic_height_in_map_units_minus1="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/googlecode/mp4parser/b/a/h;->l:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 542
    const-string v1, ", \n        pic_width_in_mbs_minus1="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/googlecode/mp4parser/b/a/h;->m:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 543
    const-string v1, ", \n        bit_depth_luma_minus8="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/googlecode/mp4parser/b/a/h;->n:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 544
    const-string v1, ", \n        bit_depth_chroma_minus8="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/googlecode/mp4parser/b/a/h;->o:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 545
    const-string v1, ", \n        qpprime_y_zero_transform_bypass_flag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/googlecode/mp4parser/b/a/h;->p:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 546
    const-string v1, ", \n        profile_idc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/googlecode/mp4parser/b/a/h;->q:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 547
    const-string v1, ", \n        constraint_set_0_flag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/googlecode/mp4parser/b/a/h;->s:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 548
    const-string v1, ", \n        constraint_set_1_flag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/googlecode/mp4parser/b/a/h;->t:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 549
    const-string v1, ", \n        constraint_set_2_flag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/googlecode/mp4parser/b/a/h;->u:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 550
    const-string v1, ", \n        constraint_set_3_flag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/googlecode/mp4parser/b/a/h;->v:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 551
    const-string v1, ", \n        constraint_set_4_flag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/googlecode/mp4parser/b/a/h;->w:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 552
    const-string v1, ", \n        constraint_set_5_flag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/googlecode/mp4parser/b/a/h;->x:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 553
    const-string v1, ", \n        level_idc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/googlecode/mp4parser/b/a/h;->y:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 554
    const-string v1, ", \n        seq_parameter_set_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/googlecode/mp4parser/b/a/h;->z:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 555
    const-string v1, ", \n        residual_color_transform_flag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/googlecode/mp4parser/b/a/h;->A:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 556
    const-string v1, ", \n        offset_for_non_ref_pic="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/googlecode/mp4parser/b/a/h;->B:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 557
    const-string v1, ", \n        offset_for_top_to_bottom_field="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/googlecode/mp4parser/b/a/h;->C:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 558
    const-string v1, ", \n        num_ref_frames="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/googlecode/mp4parser/b/a/h;->D:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 559
    const-string v1, ", \n        gaps_in_frame_num_value_allowed_flag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/googlecode/mp4parser/b/a/h;->E:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 560
    const-string v1, ", \n        frame_mbs_only_flag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/googlecode/mp4parser/b/a/h;->F:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 561
    const-string v1, ", \n        frame_cropping_flag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/googlecode/mp4parser/b/a/h;->G:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 562
    const-string v1, ", \n        frame_crop_left_offset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/googlecode/mp4parser/b/a/h;->H:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 563
    const-string v1, ", \n        frame_crop_right_offset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/googlecode/mp4parser/b/a/h;->I:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 564
    const-string v1, ", \n        frame_crop_top_offset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/googlecode/mp4parser/b/a/h;->J:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 565
    const-string v1, ", \n        frame_crop_bottom_offset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/googlecode/mp4parser/b/a/h;->K:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 566
    const-string v1, ", \n        offsetForRefFrame="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/googlecode/mp4parser/b/a/h;->L:[I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 567
    const-string v1, ", \n        vuiParams="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/googlecode/mp4parser/b/a/h;->M:Lcom/googlecode/mp4parser/b/a/i;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 568
    const-string v1, ", \n        scalingMatrix="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/googlecode/mp4parser/b/a/h;->N:Lcom/googlecode/mp4parser/b/a/g;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 569
    const-string v1, ", \n        num_ref_frames_in_pic_order_cnt_cycle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/googlecode/mp4parser/b/a/h;->O:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 570
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 529
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
