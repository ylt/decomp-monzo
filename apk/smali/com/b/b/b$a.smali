.class final Lcom/b/b/b$a;
.super Ljava/lang/Object;
.source "BehaviorRelay.java"

# interfaces
.implements Lcom/b/b/a$a;
.implements Lio/reactivex/b/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/b/b/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/b/b/a$a",
        "<TT;>;",
        "Lio/reactivex/b/b;"
    }
.end annotation


# instance fields
.field final a:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<-TT;>;"
        }
    .end annotation
.end field

.field final b:Lcom/b/b/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/b",
            "<TT;>;"
        }
    .end annotation
.end field

.field c:Z

.field d:Z

.field e:Lcom/b/b/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/b/a",
            "<TT;>;"
        }
    .end annotation
.end field

.field f:Z

.field volatile g:Z

.field h:J


# direct methods
.method constructor <init>(Lio/reactivex/t;Lcom/b/b/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;",
            "Lcom/b/b/b",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 273
    iput-object p1, p0, Lcom/b/b/b$a;->a:Lio/reactivex/t;

    .line 274
    iput-object p2, p0, Lcom/b/b/b$a;->b:Lcom/b/b/b;

    .line 275
    return-void
.end method


# virtual methods
.method a()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 292
    iget-boolean v1, p0, Lcom/b/b/b$a;->g:Z

    if-eqz v1, :cond_1

    .line 320
    :cond_0
    :goto_0
    return-void

    .line 296
    :cond_1
    monitor-enter p0

    .line 297
    :try_start_0
    iget-boolean v1, p0, Lcom/b/b/b$a;->g:Z

    if-eqz v1, :cond_2

    .line 298
    monitor-exit p0

    goto :goto_0

    .line 314
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 300
    :cond_2
    :try_start_1
    iget-boolean v1, p0, Lcom/b/b/b$a;->c:Z

    if-eqz v1, :cond_3

    .line 301
    monitor-exit p0

    goto :goto_0

    .line 304
    :cond_3
    iget-object v1, p0, Lcom/b/b/b$a;->b:Lcom/b/b/b;

    .line 305
    iget-object v2, v1, Lcom/b/b/b;->b:Ljava/util/concurrent/locks/Lock;

    .line 307
    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 308
    iget-wide v4, v1, Lcom/b/b/b;->c:J

    iput-wide v4, p0, Lcom/b/b/b$a;->h:J

    .line 309
    iget-object v1, v1, Lcom/b/b/b;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    .line 310
    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 312
    if-eqz v1, :cond_4

    :goto_1
    iput-boolean v0, p0, Lcom/b/b/b$a;->d:Z

    .line 313
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/b/b/b$a;->c:Z

    .line 314
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 316
    if-eqz v1, :cond_0

    .line 317
    invoke-virtual {p0, v1}, Lcom/b/b/b$a;->a(Ljava/lang/Object;)Z

    .line 318
    invoke-virtual {p0}, Lcom/b/b/b$a;->b()V

    goto :goto_0

    .line 312
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method a(Ljava/lang/Object;J)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;J)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 323
    iget-boolean v0, p0, Lcom/b/b/b$a;->g:Z

    if-eqz v0, :cond_0

    .line 349
    :goto_0
    return-void

    .line 326
    :cond_0
    iget-boolean v0, p0, Lcom/b/b/b$a;->f:Z

    if-nez v0, :cond_5

    .line 327
    monitor-enter p0

    .line 328
    :try_start_0
    iget-boolean v0, p0, Lcom/b/b/b$a;->g:Z

    if-eqz v0, :cond_1

    .line 329
    monitor-exit p0

    goto :goto_0

    .line 344
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 331
    :cond_1
    :try_start_1
    iget-wide v0, p0, Lcom/b/b/b$a;->h:J

    cmp-long v0, v0, p2

    if-nez v0, :cond_2

    .line 332
    monitor-exit p0

    goto :goto_0

    .line 334
    :cond_2
    iget-boolean v0, p0, Lcom/b/b/b$a;->d:Z

    if-eqz v0, :cond_4

    .line 335
    iget-object v0, p0, Lcom/b/b/b$a;->e:Lcom/b/b/a;

    .line 336
    if-nez v0, :cond_3

    .line 337
    new-instance v0, Lcom/b/b/a;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/b/b/a;-><init>(I)V

    .line 338
    iput-object v0, p0, Lcom/b/b/b$a;->e:Lcom/b/b/a;

    .line 340
    :cond_3
    invoke-virtual {v0, p1}, Lcom/b/b/a;->a(Ljava/lang/Object;)V

    .line 341
    monitor-exit p0

    goto :goto_0

    .line 343
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/b/b/b$a;->c:Z

    .line 344
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 345
    iput-boolean v2, p0, Lcom/b/b/b$a;->f:Z

    .line 348
    :cond_5
    invoke-virtual {p0, p1}, Lcom/b/b/b$a;->a(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 353
    iget-boolean v0, p0, Lcom/b/b/b$a;->g:Z

    if-nez v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/b/b/b$a;->a:Lio/reactivex/t;

    invoke-interface {v0, p1}, Lio/reactivex/t;->onNext(Ljava/lang/Object;)V

    .line 356
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method b()V
    .locals 2

    .prologue
    .line 361
    :goto_0
    iget-boolean v0, p0, Lcom/b/b/b$a;->g:Z

    if-eqz v0, :cond_0

    .line 369
    :goto_1
    return-void

    .line 365
    :cond_0
    monitor-enter p0

    .line 366
    :try_start_0
    iget-object v0, p0, Lcom/b/b/b$a;->e:Lcom/b/b/a;

    .line 367
    if-nez v0, :cond_1

    .line 368
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/b/b/b$a;->d:Z

    .line 369
    monitor-exit p0

    goto :goto_1

    .line 372
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 371
    :cond_1
    const/4 v1, 0x0

    :try_start_1
    iput-object v1, p0, Lcom/b/b/b$a;->e:Lcom/b/b/a;

    .line 372
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 374
    invoke-virtual {v0, p0}, Lcom/b/b/a;->a(Lcom/b/b/a$a;)V

    goto :goto_0
.end method

.method public dispose()V
    .locals 1

    .prologue
    .line 279
    iget-boolean v0, p0, Lcom/b/b/b$a;->g:Z

    if-nez v0, :cond_0

    .line 280
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/b/b/b$a;->g:Z

    .line 282
    iget-object v0, p0, Lcom/b/b/b$a;->b:Lcom/b/b/b;

    invoke-virtual {v0, p0}, Lcom/b/b/b;->a(Lcom/b/b/b$a;)V

    .line 284
    :cond_0
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 288
    iget-boolean v0, p0, Lcom/b/b/b$a;->g:Z

    return v0
.end method
