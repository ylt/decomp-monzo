.class public final Lcom/b/b/b;
.super Lcom/b/b/d;
.source "BehaviorRelay.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/b/b/b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/b/b/d",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final d:[Ljava/lang/Object;

.field private static final f:[Lcom/b/b/b$a;


# instance fields
.field final a:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<TT;>;"
        }
    .end annotation
.end field

.field final b:Ljava/util/concurrent/locks/Lock;

.field c:J

.field private final e:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<[",
            "Lcom/b/b/b$a",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/concurrent/locks/Lock;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54
    new-array v0, v1, [Ljava/lang/Object;

    sput-object v0, Lcom/b/b/b;->d:[Ljava/lang/Object;

    .line 61
    new-array v0, v1, [Lcom/b/b/b$a;

    sput-object v0, Lcom/b/b/b;->f:[Lcom/b/b/b$a;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/b/b/d;-><init>()V

    .line 91
    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>()V

    .line 92
    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    iput-object v1, p0, Lcom/b/b/b;->b:Ljava/util/concurrent/locks/Lock;

    .line 93
    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    iput-object v0, p0, Lcom/b/b/b;->g:Ljava/util/concurrent/locks/Lock;

    .line 94
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, Lcom/b/b/b;->f:[Lcom/b/b/b$a;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/b/b/b;->e:Ljava/util/concurrent/atomic/AtomicReference;

    .line 95
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/b/b/b;->a:Ljava/util/concurrent/atomic/AtomicReference;

    .line 96
    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/b/b/b;-><init>()V

    .line 105
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "defaultValue == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/b/b/b;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->lazySet(Ljava/lang/Object;)V

    .line 107
    return-void
.end method

.method public static a()Lcom/b/b/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/b/b/b",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 72
    new-instance v0, Lcom/b/b/b;

    invoke-direct {v0}, Lcom/b/b/b;-><init>()V

    return-object v0
.end method

.method public static b(Ljava/lang/Object;)Lcom/b/b/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lcom/b/b/b",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 84
    new-instance v0, Lcom/b/b/b;

    invoke-direct {v0, p0}, Lcom/b/b/b;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method private b(Lcom/b/b/b$a;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/b/b/b$a",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/b/b/b;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/b/b/b$a;

    .line 203
    array-length v1, v0

    .line 205
    add-int/lit8 v2, v1, 0x1

    new-array v2, v2, [Lcom/b/b/b$a;

    .line 206
    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 207
    aput-object p1, v2, v1

    .line 208
    iget-object v1, p0, Lcom/b/b/b;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1, v0, v2}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    return-void
.end method

.method private c(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 248
    iget-object v0, p0, Lcom/b/b/b;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 250
    :try_start_0
    iget-wide v0, p0, Lcom/b/b/b;->c:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/b/b/b;->c:J

    .line 251
    iget-object v0, p0, Lcom/b/b/b;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->lazySet(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 253
    iget-object v0, p0, Lcom/b/b/b;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 255
    return-void

    .line 253
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/b/b/b;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method


# virtual methods
.method a(Lcom/b/b/b$a;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/b/b/b$a",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 217
    :cond_0
    iget-object v0, p0, Lcom/b/b/b;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/b/b/b$a;

    .line 218
    sget-object v1, Lcom/b/b/b;->f:[Lcom/b/b/b$a;

    if-ne v0, v1, :cond_2

    .line 242
    :cond_1
    :goto_0
    return-void

    .line 221
    :cond_2
    array-length v4, v0

    .line 222
    const/4 v2, -0x1

    move v1, v3

    .line 223
    :goto_1
    if-ge v1, v4, :cond_3

    .line 224
    aget-object v5, v0, v1

    if-ne v5, p1, :cond_4

    move v2, v1

    .line 230
    :cond_3
    if-ltz v2, :cond_1

    .line 234
    const/4 v1, 0x1

    if-ne v4, v1, :cond_5

    .line 235
    sget-object v1, Lcom/b/b/b;->f:[Lcom/b/b/b$a;

    .line 241
    :goto_2
    iget-object v2, p0, Lcom/b/b/b;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 223
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 237
    :cond_5
    add-int/lit8 v1, v4, -0x1

    new-array v1, v1, [Lcom/b/b/b$a;

    .line 238
    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 239
    add-int/lit8 v5, v2, 0x1

    sub-int/2addr v4, v2

    add-int/lit8 v4, v4, -0x1

    invoke-static {v0, v5, v1, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_2
.end method

.method public a(Ljava/lang/Object;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 123
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "value == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125
    :cond_0
    invoke-direct {p0, p1}, Lcom/b/b/b;->c(Ljava/lang/Object;)V

    .line 126
    iget-object v0, p0, Lcom/b/b/b;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/b/b/b$a;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 127
    iget-wide v4, p0, Lcom/b/b/b;->c:J

    invoke-virtual {v3, p1, v4, v5}, Lcom/b/b/b$a;->a(Ljava/lang/Object;J)V

    .line 126
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 129
    :cond_1
    return-void
.end method

.method public b()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 145
    iget-object v0, p0, Lcom/b/b/b;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected subscribeActual(Lio/reactivex/t;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 111
    new-instance v0, Lcom/b/b/b$a;

    invoke-direct {v0, p1, p0}, Lcom/b/b/b$a;-><init>(Lio/reactivex/t;Lcom/b/b/b;)V

    .line 112
    invoke-interface {p1, v0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 113
    invoke-direct {p0, v0}, Lcom/b/b/b;->b(Lcom/b/b/b$a;)V

    .line 114
    iget-boolean v1, v0, Lcom/b/b/b$a;->g:Z

    if-eqz v1, :cond_0

    .line 115
    invoke-virtual {p0, v0}, Lcom/b/b/b;->a(Lcom/b/b/b$a;)V

    .line 119
    :goto_0
    return-void

    .line 117
    :cond_0
    invoke-virtual {v0}, Lcom/b/b/b$a;->a()V

    goto :goto_0
.end method
