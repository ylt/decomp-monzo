.class public final Lcom/b/b/c;
.super Lcom/b/b/d;
.source "PublishRelay.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/b/b/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/b/b/d",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final a:[Lcom/b/b/c$a;


# instance fields
.field private final b:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<[",
            "Lcom/b/b/c$a",
            "<TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/b/b/c$a;

    sput-object v0, Lcom/b/b/c;->a:[Lcom/b/b/c$a;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/b/b/d;-><init>()V

    .line 61
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, Lcom/b/b/c;->a:[Lcom/b/b/c$a;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/b/b/c;->b:Ljava/util/concurrent/atomic/AtomicReference;

    .line 62
    return-void
.end method

.method public static a()Lcom/b/b/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/b/b/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 54
    new-instance v0, Lcom/b/b/c;

    invoke-direct {v0}, Lcom/b/b/c;-><init>()V

    return-object v0
.end method

.method private b(Lcom/b/b/c$a;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/b/b/c$a",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/b/b/c;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/b/b/c$a;

    .line 85
    array-length v1, v0

    .line 87
    add-int/lit8 v2, v1, 0x1

    new-array v2, v2, [Lcom/b/b/c$a;

    .line 88
    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 89
    aput-object p1, v2, v1

    .line 91
    iget-object v1, p0, Lcom/b/b/c;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1, v0, v2}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    return-void
.end method


# virtual methods
.method a(Lcom/b/b/c$a;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/b/b/c$a",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/b/b/c;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/b/b/c$a;

    .line 105
    sget-object v1, Lcom/b/b/c;->a:[Lcom/b/b/c$a;

    if-ne v0, v1, :cond_2

    .line 132
    :cond_1
    :goto_0
    return-void

    .line 109
    :cond_2
    array-length v4, v0

    .line 110
    const/4 v2, -0x1

    move v1, v3

    .line 111
    :goto_1
    if-ge v1, v4, :cond_3

    .line 112
    aget-object v5, v0, v1

    if-ne v5, p1, :cond_4

    move v2, v1

    .line 118
    :cond_3
    if-ltz v2, :cond_1

    .line 124
    const/4 v1, 0x1

    if-ne v4, v1, :cond_5

    .line 125
    sget-object v1, Lcom/b/b/c;->a:[Lcom/b/b/c$a;

    .line 131
    :goto_2
    iget-object v2, p0, Lcom/b/b/c;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 111
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 127
    :cond_5
    add-int/lit8 v1, v4, -0x1

    new-array v1, v1, [Lcom/b/b/c$a;

    .line 128
    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 129
    add-int/lit8 v5, v2, 0x1

    sub-int/2addr v4, v2

    add-int/lit8 v4, v4, -0x1

    invoke-static {v0, v5, v1, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_2
.end method

.method public a(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 139
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "value == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/b/b/c;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/b/b/c$a;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 141
    invoke-virtual {v3, p1}, Lcom/b/b/c$a;->a(Ljava/lang/Object;)V

    .line 140
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 143
    :cond_1
    return-void
.end method

.method public subscribeActual(Lio/reactivex/t;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 67
    new-instance v0, Lcom/b/b/c$a;

    invoke-direct {v0, p1, p0}, Lcom/b/b/c$a;-><init>(Lio/reactivex/t;Lcom/b/b/c;)V

    .line 68
    invoke-interface {p1, v0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 69
    invoke-direct {p0, v0}, Lcom/b/b/c;->b(Lcom/b/b/c$a;)V

    .line 72
    invoke-virtual {v0}, Lcom/b/b/c$a;->isDisposed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 73
    invoke-virtual {p0, v0}, Lcom/b/b/c;->a(Lcom/b/b/c$a;)V

    .line 75
    :cond_0
    return-void
.end method
