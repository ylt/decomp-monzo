.class Lcom/b/b/a;
.super Ljava/lang/Object;
.source "AppendOnlyLinkedArrayList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/b/b/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final b:[Ljava/lang/Object;

.field private c:[Ljava/lang/Object;

.field private d:I


# direct methods
.method constructor <init>(I)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput p1, p0, Lcom/b/b/a;->a:I

    .line 35
    add-int/lit8 v0, p1, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/b/b/a;->b:[Ljava/lang/Object;

    .line 36
    iget-object v0, p0, Lcom/b/b/a;->b:[Ljava/lang/Object;

    iput-object v0, p0, Lcom/b/b/a;->c:[Ljava/lang/Object;

    .line 37
    return-void
.end method


# virtual methods
.method a(Lcom/b/b/a$a;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/b/b/a$a",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcom/b/b/a;->b:[Ljava/lang/Object;

    .line 75
    iget v2, p0, Lcom/b/b/a;->a:I

    move-object v1, v0

    .line 76
    :goto_0
    if-eqz v1, :cond_2

    .line 77
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_0

    .line 78
    aget-object v3, v1, v0

    .line 79
    if-nez v3, :cond_1

    .line 86
    :cond_0
    aget-object v0, v1, v2

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    move-object v1, v0

    goto :goto_0

    .line 82
    :cond_1
    invoke-interface {p1, v3}, Lcom/b/b/a$a;->a(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 77
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 88
    :cond_2
    return-void
.end method

.method a(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 45
    iget v1, p0, Lcom/b/b/a;->a:I

    .line 46
    iget v0, p0, Lcom/b/b/a;->d:I

    .line 47
    if-ne v0, v1, :cond_0

    .line 48
    add-int/lit8 v0, v1, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 49
    iget-object v2, p0, Lcom/b/b/a;->c:[Ljava/lang/Object;

    aput-object v0, v2, v1

    .line 50
    iput-object v0, p0, Lcom/b/b/a;->c:[Ljava/lang/Object;

    .line 51
    const/4 v0, 0x0

    .line 53
    :cond_0
    iget-object v1, p0, Lcom/b/b/a;->c:[Ljava/lang/Object;

    aput-object p1, v1, v0

    .line 54
    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/b/b/a;->d:I

    .line 55
    return-void
.end method
