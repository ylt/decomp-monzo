.class final Lcom/b/a/d/c;
.super Lcom/b/a/a;
.source "CompoundButtonCheckedChangeObservable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/b/a/d/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/b/a/a",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/widget/CompoundButton;


# direct methods
.method constructor <init>(Landroid/widget/CompoundButton;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/b/a/a;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/b/a/d/c;->a:Landroid/widget/CompoundButton;

    .line 16
    return-void
.end method


# virtual methods
.method protected synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/b/a/d/c;->c()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lio/reactivex/t;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-static {p1}, Lcom/b/a/a/c;->a(Lio/reactivex/t;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 25
    :goto_0
    return-void

    .line 22
    :cond_0
    new-instance v0, Lcom/b/a/d/c$a;

    iget-object v1, p0, Lcom/b/a/d/c;->a:Landroid/widget/CompoundButton;

    invoke-direct {v0, v1, p1}, Lcom/b/a/d/c$a;-><init>(Landroid/widget/CompoundButton;Lio/reactivex/t;)V

    .line 23
    invoke-interface {p1, v0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 24
    iget-object v1, p0, Lcom/b/a/d/c;->a:Landroid/widget/CompoundButton;

    invoke-virtual {v1, v0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0
.end method

.method protected c()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/b/a/d/c;->a:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
