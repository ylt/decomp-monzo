.class final Lcom/b/a/d/a;
.super Lcom/b/a/d/f;
.source "AutoValue_TextViewAfterTextChangeEvent.java"


# instance fields
.field private final a:Landroid/widget/TextView;

.field private final b:Landroid/text/Editable;


# direct methods
.method constructor <init>(Landroid/widget/TextView;Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/b/a/d/f;-><init>()V

    .line 17
    if-nez p1, :cond_0

    .line 18
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null view"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 20
    :cond_0
    iput-object p1, p0, Lcom/b/a/d/a;->a:Landroid/widget/TextView;

    .line 21
    iput-object p2, p0, Lcom/b/a/d/a;->b:Landroid/text/Editable;

    .line 22
    return-void
.end method


# virtual methods
.method public a()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/b/a/d/a;->a:Landroid/widget/TextView;

    return-object v0
.end method

.method public b()Landroid/text/Editable;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/b/a/d/a;->b:Landroid/text/Editable;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 46
    if-ne p1, p0, :cond_1

    .line 54
    :cond_0
    :goto_0
    return v0

    .line 49
    :cond_1
    instance-of v2, p1, Lcom/b/a/d/f;

    if-eqz v2, :cond_4

    .line 50
    check-cast p1, Lcom/b/a/d/f;

    .line 51
    iget-object v2, p0, Lcom/b/a/d/a;->a:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/b/a/d/f;->a()Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/b/a/d/a;->b:Landroid/text/Editable;

    if-nez v2, :cond_3

    .line 52
    invoke-virtual {p1}, Lcom/b/a/d/f;->b()Landroid/text/Editable;

    move-result-object v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/b/a/d/a;->b:Landroid/text/Editable;

    invoke-virtual {p1}, Lcom/b/a/d/f;->b()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    :cond_4
    move v0, v1

    .line 54
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    const v1, 0xf4243

    .line 59
    .line 61
    iget-object v0, p0, Lcom/b/a/d/a;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    xor-int/2addr v0, v1

    .line 62
    mul-int/2addr v1, v0

    .line 63
    iget-object v0, p0, Lcom/b/a/d/a;->b:Landroid/text/Editable;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    xor-int/2addr v0, v1

    .line 64
    return v0

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/b/a/d/a;->b:Landroid/text/Editable;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TextViewAfterTextChangeEvent{view="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/b/a/d/a;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", editable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/b/a/d/a;->b:Landroid/text/Editable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
