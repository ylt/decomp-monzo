.class final Lcom/b/a/d/i$a;
.super Lio/reactivex/a/a;
.source "TextViewEditorActionEventObservable.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/b/a/d/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/widget/TextView;

.field private final b:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<-",
            "Lcom/b/a/d/h;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lio/reactivex/c/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/q",
            "<-",
            "Lcom/b/a/d/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/widget/TextView;Lio/reactivex/t;Lio/reactivex/c/q;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            "Lio/reactivex/t",
            "<-",
            "Lcom/b/a/d/h;",
            ">;",
            "Lio/reactivex/c/q",
            "<-",
            "Lcom/b/a/d/h;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0}, Lio/reactivex/a/a;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/b/a/d/i$a;->a:Landroid/widget/TextView;

    .line 41
    iput-object p2, p0, Lcom/b/a/d/i$a;->b:Lio/reactivex/t;

    .line 42
    iput-object p3, p0, Lcom/b/a/d/i$a;->c:Lio/reactivex/c/q;

    .line 43
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/b/a/d/i$a;->a:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 63
    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/b/a/d/i$a;->a:Landroid/widget/TextView;

    invoke-static {v0, p2, p3}, Lcom/b/a/d/h;->a(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Lcom/b/a/d/h;

    move-result-object v0

    .line 49
    :try_start_0
    invoke-virtual {p0}, Lcom/b/a/d/i$a;->isDisposed()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/b/a/d/i$a;->c:Lio/reactivex/c/q;

    invoke-interface {v1, v0}, Lio/reactivex/c/q;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 50
    iget-object v1, p0, Lcom/b/a/d/i$a;->b:Lio/reactivex/t;

    invoke-interface {v1, v0}, Lio/reactivex/t;->onNext(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    const/4 v0, 0x1

    .line 57
    :goto_0
    return v0

    .line 53
    :catch_0
    move-exception v0

    .line 54
    iget-object v1, p0, Lcom/b/a/d/i$a;->b:Lio/reactivex/t;

    invoke-interface {v1, v0}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    .line 55
    invoke-virtual {p0}, Lcom/b/a/d/i$a;->dispose()V

    .line 57
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
