.class public final Lcom/b/a/d/e;
.super Ljava/lang/Object;
.source "RxTextView.java"


# direct methods
.method public static a(Landroid/widget/TextView;)Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            ")",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    const-string v0, "view == null"

    invoke-static {p0, v0}, Lcom/b/a/a/c;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    sget-object v0, Lcom/b/a/a/a;->b:Lio/reactivex/c/q;

    invoke-static {p0, v0}, Lcom/b/a/d/e;->a(Landroid/widget/TextView;Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/widget/TextView;Lio/reactivex/c/q;)Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            "Lio/reactivex/c/q",
            "<-",
            "Ljava/lang/Integer;",
            ">;)",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    const-string v0, "view == null"

    invoke-static {p0, v0}, Lcom/b/a/a/c;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    const-string v0, "handled == null"

    invoke-static {p1, v0}, Lcom/b/a/a/c;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    new-instance v0, Lcom/b/a/d/j;

    invoke-direct {v0, p0, p1}, Lcom/b/a/d/j;-><init>(Landroid/widget/TextView;Lio/reactivex/c/q;)V

    return-object v0
.end method

.method public static b(Landroid/widget/TextView;)Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            ")",
            "Lio/reactivex/n",
            "<",
            "Lcom/b/a/d/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    const-string v0, "view == null"

    invoke-static {p0, v0}, Lcom/b/a/a/c;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    sget-object v0, Lcom/b/a/a/a;->b:Lio/reactivex/c/q;

    invoke-static {p0, v0}, Lcom/b/a/d/e;->b(Landroid/widget/TextView;Lio/reactivex/c/q;)Lio/reactivex/n;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/widget/TextView;Lio/reactivex/c/q;)Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            "Lio/reactivex/c/q",
            "<-",
            "Lcom/b/a/d/h;",
            ">;)",
            "Lio/reactivex/n",
            "<",
            "Lcom/b/a/d/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    const-string v0, "view == null"

    invoke-static {p0, v0}, Lcom/b/a/a/c;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    const-string v0, "handled == null"

    invoke-static {p1, v0}, Lcom/b/a/a/c;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    new-instance v0, Lcom/b/a/d/i;

    invoke-direct {v0, p0, p1}, Lcom/b/a/d/i;-><init>(Landroid/widget/TextView;Lio/reactivex/c/q;)V

    return-object v0
.end method

.method public static c(Landroid/widget/TextView;)Lcom/b/a/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            ")",
            "Lcom/b/a/a",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    const-string v0, "view == null"

    invoke-static {p0, v0}, Lcom/b/a/a/c;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    new-instance v0, Lcom/b/a/d/k;

    invoke-direct {v0, p0}, Lcom/b/a/d/k;-><init>(Landroid/widget/TextView;)V

    return-object v0
.end method

.method public static d(Landroid/widget/TextView;)Lcom/b/a/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            ")",
            "Lcom/b/a/a",
            "<",
            "Lcom/b/a/d/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 159
    const-string v0, "view == null"

    invoke-static {p0, v0}, Lcom/b/a/a/c;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 160
    new-instance v0, Lcom/b/a/d/g;

    invoke-direct {v0, p0}, Lcom/b/a/d/g;-><init>(Landroid/widget/TextView;)V

    return-object v0
.end method
