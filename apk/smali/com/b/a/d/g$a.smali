.class final Lcom/b/a/d/g$a;
.super Lio/reactivex/a/a;
.source "TextViewAfterTextChangeEventObservable.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/b/a/d/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/widget/TextView;

.field private final b:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<-",
            "Lcom/b/a/d/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/widget/TextView;Lio/reactivex/t;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            "Lio/reactivex/t",
            "<-",
            "Lcom/b/a/d/f;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0}, Lio/reactivex/a/a;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/b/a/d/g$a;->a:Landroid/widget/TextView;

    .line 35
    iput-object p2, p0, Lcom/b/a/d/g$a;->b:Lio/reactivex/t;

    .line 36
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/b/a/d/g$a;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 54
    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/b/a/d/g$a;->b:Lio/reactivex/t;

    iget-object v1, p0, Lcom/b/a/d/g$a;->a:Landroid/widget/TextView;

    invoke-static {v1, p1}, Lcom/b/a/d/f;->a(Landroid/widget/TextView;Landroid/text/Editable;)Lcom/b/a/d/f;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/reactivex/t;->onNext(Ljava/lang/Object;)V

    .line 49
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 40
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 44
    return-void
.end method
