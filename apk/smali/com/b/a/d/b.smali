.class final Lcom/b/a/d/b;
.super Lcom/b/a/d/h;
.source "AutoValue_TextViewEditorActionEvent.java"


# instance fields
.field private final a:Landroid/widget/TextView;

.field private final b:I

.field private final c:Landroid/view/KeyEvent;


# direct methods
.method constructor <init>(Landroid/widget/TextView;ILandroid/view/KeyEvent;)V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/b/a/d/h;-><init>()V

    .line 19
    if-nez p1, :cond_0

    .line 20
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null view"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 22
    :cond_0
    iput-object p1, p0, Lcom/b/a/d/b;->a:Landroid/widget/TextView;

    .line 23
    iput p2, p0, Lcom/b/a/d/b;->b:I

    .line 24
    iput-object p3, p0, Lcom/b/a/d/b;->c:Landroid/view/KeyEvent;

    .line 25
    return-void
.end method


# virtual methods
.method public a()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/b/a/d/b;->a:Landroid/widget/TextView;

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/b/a/d/b;->b:I

    return v0
.end method

.method public c()Landroid/view/KeyEvent;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/b/a/d/b;->c:Landroid/view/KeyEvent;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 55
    if-ne p1, p0, :cond_1

    .line 64
    :cond_0
    :goto_0
    return v0

    .line 58
    :cond_1
    instance-of v2, p1, Lcom/b/a/d/h;

    if-eqz v2, :cond_4

    .line 59
    check-cast p1, Lcom/b/a/d/h;

    .line 60
    iget-object v2, p0, Lcom/b/a/d/b;->a:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/b/a/d/h;->a()Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/b/a/d/b;->b:I

    .line 61
    invoke-virtual {p1}, Lcom/b/a/d/h;->b()I

    move-result v3

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/b/a/d/b;->c:Landroid/view/KeyEvent;

    if-nez v2, :cond_3

    .line 62
    invoke-virtual {p1}, Lcom/b/a/d/h;->c()Landroid/view/KeyEvent;

    move-result-object v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/b/a/d/b;->c:Landroid/view/KeyEvent;

    invoke-virtual {p1}, Lcom/b/a/d/h;->c()Landroid/view/KeyEvent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    :cond_4
    move v0, v1

    .line 64
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 69
    .line 71
    iget-object v0, p0, Lcom/b/a/d/b;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    xor-int/2addr v0, v2

    .line 72
    mul-int/2addr v0, v2

    .line 73
    iget v1, p0, Lcom/b/a/d/b;->b:I

    xor-int/2addr v0, v1

    .line 74
    mul-int v1, v0, v2

    .line 75
    iget-object v0, p0, Lcom/b/a/d/b;->c:Landroid/view/KeyEvent;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    xor-int/2addr v0, v1

    .line 76
    return v0

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/b/a/d/b;->c:Landroid/view/KeyEvent;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TextViewEditorActionEvent{view="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/b/a/d/b;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", actionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/b/a/d/b;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", keyEvent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/b/a/d/b;->c:Landroid/view/KeyEvent;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
