.class final Lcom/b/a/d/k;
.super Lcom/b/a/a;
.source "TextViewTextObservable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/b/a/d/k$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/b/a/a",
        "<",
        "Ljava/lang/CharSequence;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/b/a/a;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/b/a/d/k;->a:Landroid/widget/TextView;

    .line 15
    return-void
.end method


# virtual methods
.method protected synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0}, Lcom/b/a/d/k;->c()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lio/reactivex/t;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 19
    new-instance v0, Lcom/b/a/d/k$a;

    iget-object v1, p0, Lcom/b/a/d/k;->a:Landroid/widget/TextView;

    invoke-direct {v0, v1, p1}, Lcom/b/a/d/k$a;-><init>(Landroid/widget/TextView;Lio/reactivex/t;)V

    .line 20
    invoke-interface {p1, v0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 21
    iget-object v1, p0, Lcom/b/a/d/k;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 22
    return-void
.end method

.method protected c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/b/a/d/k;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method
