.class final Lcom/b/a/d/g;
.super Lcom/b/a/a;
.source "TextViewAfterTextChangeEventObservable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/b/a/d/g$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/b/a/a",
        "<",
        "Lcom/b/a/d/f;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/b/a/a;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/b/a/d/g;->a:Landroid/widget/TextView;

    .line 16
    return-void
.end method


# virtual methods
.method protected synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0}, Lcom/b/a/d/g;->c()Lcom/b/a/d/f;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lio/reactivex/t;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-",
            "Lcom/b/a/d/f;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 20
    new-instance v0, Lcom/b/a/d/g$a;

    iget-object v1, p0, Lcom/b/a/d/g;->a:Landroid/widget/TextView;

    invoke-direct {v0, v1, p1}, Lcom/b/a/d/g$a;-><init>(Landroid/widget/TextView;Lio/reactivex/t;)V

    .line 21
    invoke-interface {p1, v0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 22
    iget-object v1, p0, Lcom/b/a/d/g;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 23
    return-void
.end method

.method protected c()Lcom/b/a/d/f;
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Lcom/b/a/d/g;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/b/a/d/g;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getEditableText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/b/a/d/f;->a(Landroid/widget/TextView;Landroid/text/Editable;)Lcom/b/a/d/f;

    move-result-object v0

    return-object v0
.end method
