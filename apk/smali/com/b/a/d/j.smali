.class final Lcom/b/a/d/j;
.super Lio/reactivex/n;
.source "TextViewEditorActionObservable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/b/a/d/j$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/reactivex/n",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/widget/TextView;

.field private final b:Lio/reactivex/c/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/q",
            "<-",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/widget/TextView;Lio/reactivex/c/q;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            "Lio/reactivex/c/q",
            "<-",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0}, Lio/reactivex/n;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/b/a/d/j;->a:Landroid/widget/TextView;

    .line 19
    iput-object p2, p0, Lcom/b/a/d/j;->b:Lio/reactivex/c/q;

    .line 20
    return-void
.end method


# virtual methods
.method protected subscribeActual(Lio/reactivex/t;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-static {p1}, Lcom/b/a/a/c;->a(Lio/reactivex/t;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 30
    :goto_0
    return-void

    .line 27
    :cond_0
    new-instance v0, Lcom/b/a/d/j$a;

    iget-object v1, p0, Lcom/b/a/d/j;->a:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/b/a/d/j;->b:Lio/reactivex/c/q;

    invoke-direct {v0, v1, p1, v2}, Lcom/b/a/d/j$a;-><init>(Landroid/widget/TextView;Lio/reactivex/t;Lio/reactivex/c/q;)V

    .line 28
    invoke-interface {p1, v0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 29
    iget-object v1, p0, Lcom/b/a/d/j;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    goto :goto_0
.end method
