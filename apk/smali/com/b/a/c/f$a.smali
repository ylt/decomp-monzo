.class final Lcom/b/a/c/f$a;
.super Lio/reactivex/a/a;
.source "ViewTouchObservable.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/b/a/c/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Lio/reactivex/c/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/q",
            "<-",
            "Landroid/view/MotionEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<-",
            "Landroid/view/MotionEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/view/View;Lio/reactivex/c/q;Lio/reactivex/t;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lio/reactivex/c/q",
            "<-",
            "Landroid/view/MotionEvent;",
            ">;",
            "Lio/reactivex/t",
            "<-",
            "Landroid/view/MotionEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0}, Lio/reactivex/a/a;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/b/a/c/f$a;->a:Landroid/view/View;

    .line 39
    iput-object p2, p0, Lcom/b/a/c/f$a;->b:Lio/reactivex/c/q;

    .line 40
    iput-object p3, p0, Lcom/b/a/c/f$a;->c:Lio/reactivex/t;

    .line 41
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/b/a/c/f$a;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 60
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/b/a/c/f$a;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 46
    :try_start_0
    iget-object v0, p0, Lcom/b/a/c/f$a;->b:Lio/reactivex/c/q;

    invoke-interface {v0, p2}, Lio/reactivex/c/q;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/b/a/c/f$a;->c:Lio/reactivex/t;

    invoke-interface {v0, p2}, Lio/reactivex/t;->onNext(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    const/4 v0, 0x1

    .line 55
    :goto_0
    return v0

    .line 50
    :catch_0
    move-exception v0

    .line 51
    iget-object v1, p0, Lcom/b/a/c/f$a;->c:Lio/reactivex/t;

    invoke-interface {v1, v0}, Lio/reactivex/t;->onError(Ljava/lang/Throwable;)V

    .line 52
    invoke-virtual {p0}, Lcom/b/a/c/f$a;->dispose()V

    .line 55
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
