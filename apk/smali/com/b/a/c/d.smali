.class final Lcom/b/a/c/d;
.super Lio/reactivex/n;
.source "ViewClickObservable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/b/a/c/d$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/reactivex/n",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lio/reactivex/n;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/b/a/c/d;->a:Landroid/view/View;

    .line 17
    return-void
.end method


# virtual methods
.method protected subscribeActual(Lio/reactivex/t;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-static {p1}, Lcom/b/a/a/c;->a(Lio/reactivex/t;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 26
    :goto_0
    return-void

    .line 23
    :cond_0
    new-instance v0, Lcom/b/a/c/d$a;

    iget-object v1, p0, Lcom/b/a/c/d;->a:Landroid/view/View;

    invoke-direct {v0, v1, p1}, Lcom/b/a/c/d$a;-><init>(Landroid/view/View;Lio/reactivex/t;)V

    .line 24
    invoke-interface {p1, v0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 25
    iget-object v1, p0, Lcom/b/a/c/d;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
