.class public final Lcom/b/a/c/c;
.super Ljava/lang/Object;
.source "RxView.java"


# direct methods
.method public static a(Landroid/view/View;)Lio/reactivex/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Lio/reactivex/n",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    const-string v0, "view == null"

    invoke-static {p0, v0}, Lcom/b/a/a/c;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    new-instance v0, Lcom/b/a/c/d;

    invoke-direct {v0, p0}, Lcom/b/a/c/d;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public static b(Landroid/view/View;)Lcom/b/a/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Lcom/b/a/a",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147
    const-string v0, "view == null"

    invoke-static {p0, v0}, Lcom/b/a/a/c;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    new-instance v0, Lcom/b/a/c/e;

    invoke-direct {v0, p0}, Lcom/b/a/c/e;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public static c(Landroid/view/View;)Lio/reactivex/n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Lio/reactivex/n",
            "<",
            "Landroid/view/MotionEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 340
    const-string v0, "view == null"

    invoke-static {p0, v0}, Lcom/b/a/a/c;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 341
    new-instance v0, Lcom/b/a/c/f;

    sget-object v1, Lcom/b/a/a/a;->b:Lio/reactivex/c/q;

    invoke-direct {v0, p0, v1}, Lcom/b/a/c/f;-><init>(Landroid/view/View;Lio/reactivex/c/q;)V

    return-object v0
.end method
