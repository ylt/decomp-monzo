.class final Lcom/b/a/c/a;
.super Lio/reactivex/n;
.source "MenuItemClickOnSubscribe.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/b/a/c/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/reactivex/n",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/MenuItem;

.field private final b:Lio/reactivex/c/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/c/q",
            "<-",
            "Landroid/view/MenuItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/view/MenuItem;Lio/reactivex/c/q;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/MenuItem;",
            "Lio/reactivex/c/q",
            "<-",
            "Landroid/view/MenuItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0}, Lio/reactivex/n;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/b/a/c/a;->a:Landroid/view/MenuItem;

    .line 19
    iput-object p2, p0, Lcom/b/a/c/a;->b:Lio/reactivex/c/q;

    .line 20
    return-void
.end method


# virtual methods
.method protected subscribeActual(Lio/reactivex/t;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-static {p1}, Lcom/b/a/a/c;->a(Lio/reactivex/t;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 29
    :goto_0
    return-void

    .line 26
    :cond_0
    new-instance v0, Lcom/b/a/c/a$a;

    iget-object v1, p0, Lcom/b/a/c/a;->a:Landroid/view/MenuItem;

    iget-object v2, p0, Lcom/b/a/c/a;->b:Lio/reactivex/c/q;

    invoke-direct {v0, v1, v2, p1}, Lcom/b/a/c/a$a;-><init>(Landroid/view/MenuItem;Lio/reactivex/c/q;Lio/reactivex/t;)V

    .line 27
    invoke-interface {p1, v0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 28
    iget-object v1, p0, Lcom/b/a/c/a;->a:Landroid/view/MenuItem;

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0
.end method
