.class final Lcom/b/a/b/a/a/a$a;
.super Lio/reactivex/a/a;
.source "BottomNavigationViewItemSelectionsObservable.java"

# interfaces
.implements Landroid/support/design/widget/BottomNavigationView$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/b/a/b/a/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/support/design/widget/BottomNavigationView;

.field private final b:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<-",
            "Landroid/view/MenuItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/support/design/widget/BottomNavigationView;Lio/reactivex/t;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/design/widget/BottomNavigationView;",
            "Lio/reactivex/t",
            "<-",
            "Landroid/view/MenuItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0}, Lio/reactivex/a/a;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/b/a/b/a/a/a$a;->a:Landroid/support/design/widget/BottomNavigationView;

    .line 47
    iput-object p2, p0, Lcom/b/a/b/a/a/a$a;->b:Lio/reactivex/t;

    .line 48
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/b/a/b/a/a/a$a;->a:Landroid/support/design/widget/BottomNavigationView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/BottomNavigationView;->setOnNavigationItemSelectedListener(Landroid/support/design/widget/BottomNavigationView$b;)V

    .line 59
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/b/a/b/a/a/a$a;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/b/a/b/a/a/a$a;->b:Lio/reactivex/t;

    invoke-interface {v0, p1}, Lio/reactivex/t;->onNext(Ljava/lang/Object;)V

    .line 54
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
