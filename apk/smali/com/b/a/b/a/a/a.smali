.class final Lcom/b/a/b/a/a/a;
.super Lio/reactivex/n;
.source "BottomNavigationViewItemSelectionsObservable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/b/a/b/a/a/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/reactivex/n",
        "<",
        "Landroid/view/MenuItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/support/design/widget/BottomNavigationView;


# direct methods
.method constructor <init>(Landroid/support/design/widget/BottomNavigationView;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lio/reactivex/n;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/b/a/b/a/a/a;->a:Landroid/support/design/widget/BottomNavigationView;

    .line 19
    return-void
.end method


# virtual methods
.method protected subscribeActual(Lio/reactivex/t;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-",
            "Landroid/view/MenuItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-static {p1}, Lcom/b/a/a/c;->a(Lio/reactivex/t;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 38
    :cond_0
    :goto_0
    return-void

    .line 25
    :cond_1
    new-instance v0, Lcom/b/a/b/a/a/a$a;

    iget-object v1, p0, Lcom/b/a/b/a/a/a;->a:Landroid/support/design/widget/BottomNavigationView;

    invoke-direct {v0, v1, p1}, Lcom/b/a/b/a/a/a$a;-><init>(Landroid/support/design/widget/BottomNavigationView;Lio/reactivex/t;)V

    .line 26
    invoke-interface {p1, v0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 27
    iget-object v1, p0, Lcom/b/a/b/a/a/a;->a:Landroid/support/design/widget/BottomNavigationView;

    invoke-virtual {v1, v0}, Landroid/support/design/widget/BottomNavigationView;->setOnNavigationItemSelectedListener(Landroid/support/design/widget/BottomNavigationView$b;)V

    .line 30
    iget-object v0, p0, Lcom/b/a/b/a/a/a;->a:Landroid/support/design/widget/BottomNavigationView;

    invoke-virtual {v0}, Landroid/support/design/widget/BottomNavigationView;->getMenu()Landroid/view/Menu;

    move-result-object v1

    .line 31
    const/4 v0, 0x0

    invoke-interface {v1}, Landroid/view/Menu;->size()I

    move-result v2

    :goto_1
    if-ge v0, v2, :cond_0

    .line 32
    invoke-interface {v1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 33
    invoke-interface {v3}, Landroid/view/MenuItem;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 34
    invoke-interface {p1, v3}, Lio/reactivex/t;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    .line 31
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
