.class final Lcom/b/a/b/b/a/b;
.super Lcom/b/a/a;
.source "ViewPagerPageSelectedObservable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/b/a/b/b/a/b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/b/a/a",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/support/v4/view/ViewPager;


# direct methods
.method constructor <init>(Landroid/support/v4/view/ViewPager;)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/b/a/a;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/b/a/b/b/a/b;->a:Landroid/support/v4/view/ViewPager;

    .line 14
    return-void
.end method


# virtual methods
.method protected synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0}, Lcom/b/a/b/b/a/b;->c()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lio/reactivex/t;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/t",
            "<-",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 17
    new-instance v0, Lcom/b/a/b/b/a/b$a;

    iget-object v1, p0, Lcom/b/a/b/b/a/b;->a:Landroid/support/v4/view/ViewPager;

    invoke-direct {v0, v1, p1}, Lcom/b/a/b/b/a/b$a;-><init>(Landroid/support/v4/view/ViewPager;Lio/reactivex/t;)V

    .line 18
    invoke-interface {p1, v0}, Lio/reactivex/t;->onSubscribe(Lio/reactivex/b/b;)V

    .line 19
    iget-object v1, p0, Lcom/b/a/b/b/a/b;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->a(Landroid/support/v4/view/ViewPager$f;)V

    .line 20
    return-void
.end method

.method protected c()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/b/a/b/b/a/b;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
