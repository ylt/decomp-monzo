.class final Lcom/b/a/b/b/a/b$a;
.super Lio/reactivex/a/a;
.source "ViewPagerPageSelectedObservable.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$f;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/b/a/b/b/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/support/v4/view/ViewPager;

.field private final b:Lio/reactivex/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/t",
            "<-",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/support/v4/view/ViewPager;Lio/reactivex/t;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/view/ViewPager;",
            "Lio/reactivex/t",
            "<-",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Lio/reactivex/a/a;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/b/a/b/b/a/b$a;->a:Landroid/support/v4/view/ViewPager;

    .line 32
    iput-object p2, p0, Lcom/b/a/b/b/a/b$a;->b:Lio/reactivex/t;

    .line 33
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/b/a/b/b/a/b$a;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->b(Landroid/support/v4/view/ViewPager$f;)V

    .line 52
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 48
    return-void
.end method

.method public a(IFI)V
    .locals 0

    .prologue
    .line 38
    return-void
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/b/a/b/b/a/b$a;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/b/a/b/b/a/b$a;->b:Lio/reactivex/t;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/reactivex/t;->onNext(Ljava/lang/Object;)V

    .line 44
    :cond_0
    return-void
.end method
