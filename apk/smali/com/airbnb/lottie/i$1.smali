.class Lcom/airbnb/lottie/i$1;
.super Ljava/lang/Object;
.source "PerformanceTracker.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/airbnb/lottie/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/support/v4/g/j",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Float;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/airbnb/lottie/i;


# direct methods
.method constructor <init>(Lcom/airbnb/lottie/i;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcom/airbnb/lottie/i$1;->a:Lcom/airbnb/lottie/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v4/g/j;Landroid/support/v4/g/j;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/g/j",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;",
            "Landroid/support/v4/g/j",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p1, Landroid/support/v4/g/j;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 30
    iget-object v0, p2, Landroid/support/v4/g/j;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 31
    cmpl-float v2, v0, v1

    if-lez v2, :cond_0

    .line 32
    const/4 v0, 0x1

    .line 36
    :goto_0
    return v0

    .line 33
    :cond_0
    cmpl-float v0, v1, v0

    if-lez v0, :cond_1

    .line 34
    const/4 v0, -0x1

    goto :goto_0

    .line 36
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 27
    check-cast p1, Landroid/support/v4/g/j;

    check-cast p2, Landroid/support/v4/g/j;

    invoke-virtual {p0, p1, p2}, Lcom/airbnb/lottie/i$1;->a(Landroid/support/v4/g/j;Landroid/support/v4/g/j;)I

    move-result v0

    return v0
.end method
